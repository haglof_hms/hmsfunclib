/*
//////////////////////////////////////////////////////////////////////////
COPYRIGHT NOTICE, DISCLAIMER, and LICENSE:
//////////////////////////////////////////////////////////////////////////

CUtil : Copyright (C) 2008, Kazi Shupantha Imam (shupantha@yahoo.com)

//////////////////////////////////////////////////////////////////////////
Covered code is provided under this license on an "as is" basis, without
warranty of any kind, either expressed or implied, including, without
limitation, warranties that the covered code is free of defects,
merchantable, fit for a particular purpose or non-infringing. The entire
risk as to the quality and performance of the covered code is with you.
Should any covered code prove defective in any respect, you (not the
initial developer or any other contributor) assume the cost of any
necessary servicing, repair or correction. This disclaimer of warranty
constitutes an essential part of this license. No use of any covered code
is authorized hereunder except under this disclaimer.

Permission is hereby granted to use, copy, modify, and distribute this
source code, or portions hereof, for any purpose, including commercial
applications, freely and without fee, subject to the following
restrictions: 

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
//////////////////////////////////////////////////////////////////////////
*/

#include "stdafx.h"

#include "Util.h"

#include <stdio.h>
#include <malloc.h>
#include <tchar.h>
#include <wchar.h>
#include <ctime>
#include <direct.h>
#include <math.h>
#include <algorithm>

#include <sstream>
#include <cstring>
#include <iostream>
#include <iomanip>

CUtil::CUtil(void)
{
}

CUtil::~CUtil(void)
{
}

bool CUtil::ReadFile(const stlString& strFilePath, stlString& strFileData)
{
	FILE* pFile = _tfsopen(strFilePath.c_str(), _T("r"), _SH_DENYNO);

	if(pFile == NULL)
	{
		return false;
	}

	// Get the file size
	fseek(pFile, 0, SEEK_END);
	long lFileSize = ftell(pFile);
	rewind(pFile);

	// Allocate memory to contain the whole file
	char* strBuffer = (char*) malloc(sizeof(char) * lFileSize + 1);

	if(strBuffer == NULL)
	{
		return false;
	}

	// Copy the file into the buffer
	(long) fread(strBuffer, 1, lFileSize, pFile);
	strBuffer[lFileSize] = '\0';

	strFileData = GetStringW(string(strBuffer));

	// The text data in the file is now loaded in the memory buffer
	// Close the file handle and free the buffer
	fclose(pFile);
	free(strBuffer);

	return true;
}

bool CUtil::WriteFile(const stlString& strFilePath, const stlString& strFileData)
{
	FILE* pFile = _tfsopen(strFilePath.c_str(), _T("w"), _SH_DENYWR);

	if(pFile == NULL)
	{
		return false;
	}

	// Get the number of elements
	long lElementCount = (long) strFileData.length();

	// Write the data into file
	long lFileSize = 0;

	lFileSize = (long) fwrite(GetStringA(strFileData).c_str(), sizeof(char), lElementCount, pFile);;

	if(lElementCount != lFileSize)
	{
		return false;
	}

	// Close the file handle and free the buffer
	fclose(pFile);

	return true;
}

long CUtil::GetFileSize(const stlString& strFilePath)
{
	// Get the the file size using better/faster method
	long lFileSize = (long) GetFileSize64(strFilePath);

	if(lFileSize > 0)
	{
		return lFileSize;
	}

	// If the previous method fails, try again the old fashioned way
	FILE* pFile = _tfsopen(strFilePath.c_str(), _T("r"), _SH_DENYNO);
		
	if(pFile == NULL)
	{
		// File does not exist
		return -1;
	}
	else
	{
		// Get the file size
		fseek(pFile, 0, SEEK_END);
		lFileSize = ftell(pFile);
		rewind(pFile);

		// Close the file handle and free the buffer
		fclose(pFile);

		return lFileSize;
	}
}

__int64 CUtil::GetFileSize64(const stlString& strFilePath)
{ 
	struct __stat64 fileStat;
	int iError = 0;

	iError = _stat64(GetStringA(strFilePath).c_str(), &fileStat);
	
	if(iError != 0)
	{
		return 0;
	}

	return fileStat.st_size;
}

bool CUtil::IsFile(const stlString& strFilePath)
{
	// For most cases, it is safe to assume that if the file size is reported as 0 or -ve, its not a valid file
	if(GetFileSize(strFilePath) <= 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

long CUtil::FindReplace(stlString& strSource, const stlString& strFind, const stlString& strReplace)
{
	long lCount = 0;

	stlString::size_type nPos = 0;

	while(stlString::npos != (nPos = strSource.find(strFind, nPos)))
	{
		strSource.replace(nPos, strFind.length(), strReplace);
		nPos += strReplace.length();

		lCount++;
	}

	return lCount;
}

long CUtil::GetTokenCount(const stlString& strSource, const stlString& strDeliminator)
{
	if(strSource.compare(_T("")) == 0)
	{
		return 0;
	}

	long lIndex = 0;
	long lTokenCount = 0;

	while(true)
	{
		lIndex = (long) strSource.find(strDeliminator, lIndex);
		lTokenCount++;

		if(lIndex >= 0)
		{
			lIndex++;
		}
		else
		{
			break;
		}
	}

	return lTokenCount;
}

stlString CUtil::GetToken(const stlString& strSource, const stlString& strDeliminator, long lTokenIndex)
{
	if(strSource.compare(_T("")) == 0 || lTokenIndex < 0)
	{
		return stlString(_T(""));
	}

	stlString strText = strSource;
	long lDeliminatorLength = (long) strDeliminator.length();

	while(true)
	{
		long lIndex = 0;
		stlString strTemp = _T("");

		lIndex = (long) strText.find(strDeliminator, lIndex);

		if(lIndex >= 0)
		{
			strTemp = strText.substr(0, lIndex);
		}
		else
		{
			strTemp = strText;
		}

		strText.erase(0, lIndex + lDeliminatorLength);

		lTokenIndex--;

		if(lTokenIndex < 0)
		{
			return strTemp;
		}
	}
}

string CUtil::Long2StringA(unsigned long lNumber, unsigned long lPrecision)
{
	string strNumber = "";

	unsigned long lNumberOfDigits = (unsigned long) log10((long double) lNumber) + 1;

	for(unsigned long lIndex = 0; lIndex < (lPrecision - lNumberOfDigits); lIndex++)
	{
		strNumber += "0";
	}

	strNumber += Long2StringA(lNumber);

	return strNumber;
}

string CUtil::Long2StringA(long long lNumber)
{
	std::ostringstream strStream;
	strStream << lNumber;

	return strStream.str();
}

string CUtil::Double2StringA(long double dNumber, unsigned long lPrecision)
{
	std::ostringstream strStream;
	
	if(lPrecision <= 0)
	{
		strStream << dNumber;
	}
	else
	{
		strStream << std::fixed;
		strStream << std::setprecision(lPrecision) << dNumber;
	}

	return strStream.str();
}

wstring CUtil::Long2StringW(unsigned long lNumber, unsigned long lPrecision)
{
	return GetStringW(Long2StringA(lNumber, lPrecision));
}

wstring CUtil::Long2StringW(long long lNumber)
{
	return GetStringW(Long2StringA(lNumber));
}

wstring CUtil::Double2StringW(long double dNumber, unsigned long lPrecision)
{
	return GetStringW(Double2StringA(dNumber, lPrecision));
}

stlString CUtil::ToUpper(const stlString& str)
{
	stlString strUpper = str;
	std::transform(strUpper.begin(), strUpper.end(), strUpper.begin(), toupper);
	return strUpper;
}

stlString CUtil::ToLower(const stlString& str)
{
	stlString strLower = str;
	std::transform(strLower.begin(), strLower.end(), strLower.begin(), tolower);
	return strLower;
}

int CUtil::CompareNoCase(const stlString& str1, const stlString& str2)
{
	return ToLower(str1).compare(ToLower(str2));
}

stlString CUtil::GetSystemTime(const stlString& strDateFormat)
{
	string strTime = "";
	string strFormat = "";

	strFormat = GetStringA(strDateFormat.c_str());

	time_t CurrentTime;
	tm tmCurrent;

	time(&CurrentTime);

	if(localtime_s(&tmCurrent, &CurrentTime) == 0)
	{
		char strBuffer[_MAX_PATH];

		strftime(strBuffer, sizeof(strBuffer), strFormat.c_str(), &tmCurrent);

		strTime = strBuffer;
	}

	return GetStringW(strTime.c_str());
}

stlString CUtil::GetTime(long lSeconds)
{
	char strBuffer[16];
	sprintf_s(strBuffer, 16, "%02d:%02d:%02d\0", lSeconds / 3600, (lSeconds % 3600) / 60 , lSeconds % 60);

	return GetStringW(string(strBuffer));
}

long CUtil::GetTime(const stlString& strTime)
{
	if(GetTokenCount(strTime, _T(":")) != 3)
	{
		return 0;
	}

	stlString strHour = GetToken(strTime, _T(":"), 0);
	stlString strMinute = GetToken(strTime, _T(":"), 1);
	stlString strSecond = GetToken(strTime, _T(":"), 2);

	long lHour = _tstol(strHour.c_str());
	long lMinute = _tstol(strMinute.c_str());
	long lSecond = _tstol(strSecond.c_str());

	return ((lHour * 60 * 60) + (lMinute * 60) + lSecond);
}

stlString CUtil::GetWorkingDirectory()
{
	TCHAR strBuffer[_MAX_PATH];

	// Get the current working directory
	_tgetcwd(strBuffer, _MAX_PATH);

	return stlString(strBuffer);
}

#ifdef WIN32
stlString CUtil::GetProgramDirectory()
{
	TCHAR strFilePath[_MAX_PATH];

	stlString strProgramDirectory = _T("");

	// Get the file path of the current executable
	if(::GetModuleFileName(NULL, strFilePath, _MAX_PATH) != 0)
	{
		strProgramDirectory = GetFileDirectory(strFilePath);
	}

	return strProgramDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetProgramFilesDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion"), _T("ProgramFilesDir"));

	// Check if a path was found in the registry
	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetWindowsDirectory()
{
	stlString strDirectory = _T("");

	TCHAR strPath[MAX_PATH];

	// Use the API to get windows installation directory path
	HRESULT hr = SHGetFolderPath(NULL, CSIDL_WINDOWS | CSIDL_FLAG_CREATE, NULL, 0, strPath);

	if(SUCCEEDED(hr)) 
	{
		strDirectory = strPath;
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetSystemDirectory()
{
	stlString strDirectory = _T("");

	TCHAR strPath[MAX_PATH];

	// Use the API to get windows system directory path
	HRESULT hr = SHGetFolderPath(NULL, CSIDL_SYSTEM | CSIDL_FLAG_CREATE, NULL, 0, strPath);

	if(SUCCEEDED(hr)) 
	{
		strDirectory = strPath;
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetMyDocumentsDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("Personal"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetMyMusicDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("My Music"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_MYMUSIC | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetMyPicturesDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("My Pictures"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_MYPICTURES | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetMyVideosDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("My Video"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_MYVIDEO | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetAppDataDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("AppData"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetLocalAppDataDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("Local AppData"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetDesktopDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("Desktop"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
stlString CUtil::GetStartupDirectory()
{
	// For backward compatibility, check specific registry location for the path
	stlString strDirectory = GetRegistryInfo(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T("Startup"));

	if(strDirectory.compare(_T("")) == 0)
	{
		TCHAR strPath[MAX_PATH];

		// Since there was no path found in the registry, use the API
		HRESULT hr = SHGetFolderPath(NULL, CSIDL_STARTUP | CSIDL_FLAG_CREATE, NULL, 0, strPath);

		if(SUCCEEDED(hr)) 
		{
			strDirectory = strPath;
		}
	}

	return strDirectory;
}
#endif

#ifdef WIN32
bool CUtil::CopyFile2Directory(const stlString& strSourceFilePath, const stlString& strDestinationDirectory, bool bDeleteSource /* = false*/)
{
	if(strSourceFilePath.compare(_T("")) == 0)
	{
		return false;
	}

	if(strDestinationDirectory.compare(_T("")) == 0)
	{
		return false;
	}

	// Create the entire destination directory path, if not present
	MakeDirectory(strDestinationDirectory);

	stlString strSourceFileName = GetFileName(strSourceFilePath, true);

	// Build the destination file path
	stlString strDestinationFilePath = strDestinationDirectory;
	strDestinationFilePath = CUtil::AddDirectoryEnding(strDestinationFilePath);
	strDestinationFilePath += strSourceFileName;

	// Copy from source to destination
	if(!CopyFile(strSourceFilePath.c_str(), strDestinationFilePath.c_str(), FALSE))
	{
		return false;
	}
	else
	{
		if(bDeleteSource)
		{
			_tunlink(strSourceFilePath.c_str());
		}

		return true;
	}
}
#endif

#ifdef WIN32
void CUtil::GetFileList(const stlString& strTargetDirectoryPath, bool bLookInSubdirectories, vector<stlString>& vecstrFileList)
{
	// Check whether target directory string is empty
	if(strTargetDirectoryPath.compare(_T("")) == 0)
	{
		return;
	}

	// Remove "\\" if present at the end of the target directory
	// Then make a copy of it and use as the current search directory
	stlString strCurrentDirectory = RemoveDirectoryEnding(strTargetDirectoryPath);

	// This data structure stores information about the file/folder that is found by any of these Win32 API functions:
	// FindFirstFile, FindFirstFileEx, or FindNextFile function
	WIN32_FIND_DATA fdDesktop = {0};

	TCHAR strDesktopPath[_MAX_PATH];

	// Format and copy the current directory to the character array
	// Note the addition of the wildcard *.*, which represents all files
	// 
	// Below is a list of wildcards that you can use
	// * (asterisk)			- represents zero or more characters at the current character position
	// ? (question mark)	- represents a single character
	//
	// Modify this function so that the function can take in a search pattern with wildcards and use it in the line below to find for e.g. only *.mpg files
	_stprintf_s(strDesktopPath, _MAX_PATH, _T("%s\\*.*"), strCurrentDirectory.c_str());

	// Finds the first file and populates the WIN32_FIND_DATA data structure with its information
	// The return value is a search handle used in subsequent calls to FindNextFile or FindClose functions
	HANDLE hDesktop = ::FindFirstFile(strDesktopPath, &fdDesktop);	

	// If an invalid handle is returned by FindFirstFile function, then the directory is empty, so nothing to do but quit
	if(hDesktop == INVALID_HANDLE_VALUE)
	{
		return;
	}

	// Do this on the first file found and repeat for every next file found until all the required files that match the search pattern are found
	do 
	{
		// Check if a directory was found
		if(fdDesktop.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// Reconstruct the directory path
			_stprintf_s(strDesktopPath, _MAX_PATH, _T("%s\\%s"), strCurrentDirectory.c_str(), fdDesktop.cFileName);

			// Get the name of the directory
			stlString strCurrentDirectoryName = GetDirectoryName(strDesktopPath);

			// If its a current (.) or previous (..) directory indicator, just skip it
			if((strCurrentDirectoryName.compare(_T(".")) == 0) || (strCurrentDirectoryName.compare(_T("..")) == 0))
			{
				continue;
			}
			// Other wise this is a sub-directory
			else
			{
				// Check whether function was called to include sub-directories in the search
				if(bLookInSubdirectories)
				{
					// If sub-directories are to be searched as well, recursively call the function again, with the target directory as the sub-directory
					GetFileList(strDesktopPath, bLookInSubdirectories, vecstrFileList);
				}
			}
		}
		// A file was found
		else
		// if(fdDesktop.dwFileAttributes & FILE_ATTRIBUTE_NORMAL)
		{
			// Reconstruct the file path
			_stprintf_s(strDesktopPath, _MAX_PATH, _T("%s\\%s"), strCurrentDirectory.c_str(), fdDesktop.cFileName);

			// Add the string to the vector
			vecstrFileList.push_back(stlString(strDesktopPath));
		}
	}
	// Search for the next file that matches the search pattern
	while(::FindNextFile(hDesktop, &fdDesktop) == TRUE);

	// Close the search handle
	::FindClose(hDesktop);
}
#endif

#ifdef WIN32
void CUtil::MakeDirectory(const stlString& strDirectoryPath)
{
	if(strDirectoryPath.compare(_T("")) == 0)
	{
		return;
	}

	stlString strDirectoryPathCopy = RemoveDirectoryEnding(strDirectoryPath);

	long lIndex = 0;
	long lCount = GetTokenCount(strDirectoryPathCopy, DIRECTORY_SEPARATOR);

	stlString strDirectory2Make = GetToken(strDirectoryPathCopy, DIRECTORY_SEPARATOR, 0);

	for(lIndex = 1; lIndex < lCount; lIndex++)
	{
		strDirectory2Make += DIRECTORY_SEPARATOR;
		strDirectory2Make += GetToken(strDirectoryPathCopy, DIRECTORY_SEPARATOR, lIndex);

		CreateDirectory(strDirectory2Make.c_str(), NULL);
		SetFileAttributes(strDirectory2Make.c_str(), FILE_ATTRIBUTE_NORMAL);
	}
}
#endif

#ifdef WIN32
void CUtil::DeleteDirectory(const stlString& strTargetDirectoryPath)
{
	// Check whether target directory string is empty
	if(strTargetDirectoryPath.compare(_T("")) == 0)
	{
		return;
	}

	// Remove "\\" if present at the end of the target directory
	// Then make a copy of it and use as the current search directory
	stlString strCurrentDirectory = RemoveDirectoryEnding(strTargetDirectoryPath);

	// This data structure stores information about the file/folder that is found by any of these Win32 API functions:
	// FindFirstFile, FindFirstFileEx, or FindNextFile function
	WIN32_FIND_DATA fdDesktop = {0};

	TCHAR strDesktopPath[_MAX_PATH];

	// Format and copy the current directory to the character array
	// Note the addition of the wildcard *.*, which represents all files
	// 
	// Below is a list of wildcards that you can use
	// * (asterisk)			- represents zero or more characters at the current character position
	// ? (question mark)	- represents a single character
	//
	// Modify this function so that the function can take in a search pattern with wildcards and use it in the line below to find for e.g. only *.mpg files
	_stprintf_s(strDesktopPath, _MAX_PATH, _T("%s\\*.*"), strCurrentDirectory.c_str());

	// Finds the first file and populates the WIN32_FIND_DATA data structure with its information
	// The return value is a search handle used in subsequent calls to FindNextFile or FindClose functions
	HANDLE hDesktop = ::FindFirstFile(strDesktopPath, &fdDesktop);	

	// If an invalid handle is returned by FindFirstFile function, then the directory is empty, so delete directory and quit
	if(hDesktop == INVALID_HANDLE_VALUE)
	{
		RemoveDirectory(strCurrentDirectory.c_str());

		return;
	}

	// Do this on the first file found and repeat for every next file found until all the required files that match the search pattern are found
	do 
	{
		// Check if a directory was found
		if(fdDesktop.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// Reconstruct the directory path
			_stprintf_s(strDesktopPath, _MAX_PATH, _T("%s\\%s"), strCurrentDirectory.c_str(), fdDesktop.cFileName);

			// Get the name of the directory
			stlString strCurrentDirectoryName = GetDirectoryName(strDesktopPath);

			// If its a current (.) or previous (..) directory indicator, just skip it
			if((strCurrentDirectoryName.compare(_T(".")) == 0) || (strCurrentDirectoryName.compare(_T("..")) == 0))
			{
				continue;
			}
			// Other wise this is a sub-directory
			else
			{
				// Recursively call the function again, with the target directory as the sub-directory
				DeleteDirectory(strDesktopPath);
			}
		}
		// A file was found
		else
		// if(fdDesktop.dwFileAttributes & FILE_ATTRIBUTE_NORMAL)
		{
			// Reconstruct the file path
			_stprintf_s(strDesktopPath, _MAX_PATH, _T("%s\\%s"), strCurrentDirectory.c_str(), fdDesktop.cFileName);

			// Delete the file
			_tunlink(strDesktopPath);
		}
	}
	// Search for the next file that matches the search pattern
	while(::FindNextFile(hDesktop, &fdDesktop) == TRUE);

	// Remove the directory
	RemoveDirectory(strCurrentDirectory.c_str());

	// Close the search handle
	::FindClose(hDesktop);
}
#endif

#ifdef WIN32
bool CUtil::IsDirectory(const stlString& strDirectoryPath)
{
	if(strDirectoryPath.compare(_T("")) == 0)
	{
		return false;
	}

	CFileFind FileFinder;

	bool bIsDirectory = false;

	BOOL bWorking = FileFinder.FindFile(strDirectoryPath.c_str());
	
	while(bWorking)
	{
		bWorking = FileFinder.FindNextFile();

		stlString strCurrentFileName = (LPCTSTR) FileFinder.GetFileName().GetBuffer();

		if(CompareNoCase(strDirectoryPath, strCurrentFileName) == 0)
		{
			bIsDirectory = (FileFinder.IsDirectory() == TRUE);

			break;
		}
	}

	return bIsDirectory;
}
#endif

stlString CUtil::AddDirectoryEnding(const stlString& strDirectoryPath)
{
	if(strDirectoryPath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strDirectory = strDirectoryPath;

	if(strDirectory[strDirectory.length() - 1] != DIRECTORY_SEPARATOR_C)
	{
		strDirectory += DIRECTORY_SEPARATOR;
	}

	return strDirectory;
}

stlString CUtil::RemoveDirectoryEnding(const stlString& strDirectoryPath)
{
	if(strDirectoryPath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strDirectory = strDirectoryPath;

	if(strDirectory[strDirectory.length() - 1] == DIRECTORY_SEPARATOR_C)
	{
		strDirectory.erase(strDirectory.length() - 1);
	}

	return strDirectory;
}

stlString CUtil::GetDirectoryName(const stlString& strDirectoryPath)
{
	if(strDirectoryPath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strDirectoryName = RemoveDirectoryEnding(strDirectoryPath);

	size_t i64Index = strDirectoryName.find_last_of(DIRECTORY_SEPARATOR);

	if(i64Index == stlString::npos)
	{
		return stlString(_T(""));
	}

	strDirectoryName.erase(0, i64Index + 1);

	return strDirectoryName;
}

stlString CUtil::GetFileDirectory(const stlString& strFilePath)
{
	if(strFilePath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strDirectory = RemoveDirectoryEnding(strFilePath);

	size_t i64Index = strDirectory.find_last_of(DIRECTORY_SEPARATOR);

	if(i64Index == stlString::npos)
	{
		return stlString(_T(""));
	}

	strDirectory.erase(i64Index);

	return strDirectory;
}

stlString CUtil::GetRootDirectory(const stlString& strDirectoryPath)
{
	if(strDirectoryPath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strRootDirectory = RemoveDirectoryEnding(strDirectoryPath);

	size_t i64Index = strRootDirectory.find_last_of(DIRECTORY_SEPARATOR);

	if(i64Index == stlString::npos)
	{
		return stlString(_T(""));
	}

	strRootDirectory.erase(i64Index);

	return strRootDirectory;
}

stlString CUtil::GetFileName(const stlString& strFilePath, bool bIncludeExtension /* = false*/)
{
	if(strFilePath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strFileName = strFilePath;

	size_t i64Index = strFileName.find_last_of(DIRECTORY_SEPARATOR);

	if(i64Index != stlString::npos)
	{
		strFileName.erase(0, i64Index + 1);
	}

	if(!bIncludeExtension)
	{
		i64Index = strFileName.find_last_of(_T("."));

		if(i64Index == stlString::npos)
		{
			return strFileName;
		}

		strFileName.erase(i64Index);
	}

	return strFileName;
}

stlString CUtil::GetFileExtension(const stlString& strFilePath, bool bIncludeDot /* = false*/)
{
	if(strFilePath.compare(_T("")) == 0)
	{
		return stlString(_T(""));
	}

	stlString strFileExtension = strFilePath;

	size_t i64Index = strFileExtension.find_last_of(_T("."));

	if(i64Index == stlString::npos)
	{
		return stlString(_T(""));
	}

	if(!bIncludeDot)
	{
		strFileExtension.erase(0, i64Index + 1);
	}
	else
	{
		strFileExtension.erase(0, i64Index);
	}

	if(strFileExtension.compare(strFilePath) == 0)
	{
		strFileExtension = _T("");
	}

	return strFileExtension;
}

char CUtil::Wide2Narrow(wchar_t wch)
{
	// Simple type cast works because UNICODE incorporates ASCII into itself
	char ch = (char) wch;
	return ch;
}

wchar_t CUtil::Narrow2Wide(char ch)
{
	// Simple type cast works because UNICODE incorporates ASCII into itself
	wchar_t wch = (wchar_t) ch;
	return wch;
}

wstring CUtil::GetStringW(const string& strA)
{
	/*
	wstring strW = L"";
	strW.resize(strA.length(), L'\0');

	std::transform(strA.begin(), strA.end(), strW.begin(), Narrow2Wide);

	return strW;
	*/

	BSTR bstrW = NULL;
	wstring strW = L"";

	long lStringLengthA = (long) strA.length();
	long lStringLengthW = ::MultiByteToWideChar(CP_ACP, 0, strA.c_str(), lStringLengthA, 0, 0);
	
	// Check whether conversion is successful
	if(lStringLengthW > 0)
	{
		bstrW = ::SysAllocStringLen(0, lStringLengthW);
		::MultiByteToWideChar(CP_ACP, 0, strA.c_str(), lStringLengthA, bstrW, lStringLengthW);

		strW = bstrW;
	}

	// free the BSTR
	::SysFreeString(bstrW);

	return strW;
}

string CUtil::GetStringA(const wstring& strW)
{
	/*
	string strA = "";
	strA.resize(strW.length(), '\0');

	std::transform(strW.begin(), strW.end(), strA.begin(), Wide2Narrow);

	return strA;
	*/

	char* pstrA = NULL;
	string strA = "";

	long lStringLengthW = (long) strW.length();
	long lStringLengthA = ::WideCharToMultiByte(CP_ACP, 0, strW.c_str(), lStringLengthW, 0, 0, NULL, NULL);

	// Check whether conversion is successful
	if(lStringLengthA > 0)
	{
		pstrA = new char[lStringLengthA + 1];	// allocate a final null terminator as well
		::WideCharToMultiByte(CP_ACP, 0, strW.c_str(), lStringLengthW, pstrA, lStringLengthA, NULL, NULL);
		pstrA[lStringLengthA] = 0;	// Set the null terminator

		strA = pstrA;
	}

	// free the memory
	delete[] pstrA;

	return strA;
}

#ifdef WIN32
stlString CUtil::GetRegistryInfo(HKEY hkey, const stlString& strRegistryPath, const stlString& strValueName)
{
	CRegKey reg;
	stlString strKey = _T("");

	if(reg.Open(hkey, strRegistryPath.c_str(), KEY_READ) == ERROR_SUCCESS)
	{
		TCHAR strKeyBuffer[MAX_PATH * 4];
		unsigned long ulKeyBufferSize = MAX_PATH * 4;

		if(reg.QueryStringValue(strValueName.c_str(), strKeyBuffer, &ulKeyBufferSize) == ERROR_SUCCESS)
		{
			strKey = strKeyBuffer;
		}

		reg.Close();
	}

	return strKey;
}

bool CUtil::SetRegistryInfo(HKEY hkey, const stlString& strRegistryPath, const stlString& strValueName, const stlString& strValue)
{
	CRegKey reg;
	bool bSuccess = false;

	if(reg.Open(hkey, strRegistryPath.c_str(), KEY_WRITE) == ERROR_SUCCESS)
	{
		if(reg.SetStringValue(strValueName.c_str(), strValue.c_str(), REG_SZ) == ERROR_SUCCESS)
		{
			bSuccess = true;
		}

		reg.Close();
	}

	return bSuccess;
}
#endif

#ifdef WIN32
LONG CUtil::GetRegKey(HKEY key, LPCTSTR subkey, LPTSTR regdata)
{
	HKEY hkey;
	long lResult = 0;

	lResult = RegOpenKeyEx(key, subkey, 0, KEY_QUERY_VALUE, &hkey);

	if(lResult == ERROR_SUCCESS)
	{
		TCHAR strDataBuffer[MAX_PATH * 4];
		long lDataSize = MAX_PATH * 4;

		RegQueryValue(hkey, NULL, strDataBuffer, &lDataSize);
		
		lstrcpy(regdata, strDataBuffer);
		
		RegCloseKey(hkey);
	}

	return lResult;
}
#endif

#ifdef WIN32
HINSTANCE CUtil::OpenURL(LPCTSTR strURL)
{
	TCHAR key[MAX_PATH + MAX_PATH];

	// First try ShellExecute()
	HINSTANCE result = ShellExecute(NULL, _T("open"), strURL, NULL, NULL, SW_SHOW);

	// If it failed, get the .htm regkey and lookup the program
	if((long long) result <= HINSTANCE_ERROR)
	{
		if(GetRegKey(HKEY_CLASSES_ROOT, _T(".htm"), key) == ERROR_SUCCESS)
		{
			lstrcat(key, _T("\\shell\\open\\command"));

			if(GetRegKey(HKEY_CLASSES_ROOT, key, key) == ERROR_SUCCESS)
			{
				TCHAR *pos;
				
				pos = _tcsstr(key, _T("\"%1\""));
				
				if(pos == NULL)		// No quotes found
				{
					pos = _tcsstr(key, _T("%1"));		// Check for %1, without quotes 
					
					if(pos == NULL)						// No parameter at all...
					{
						pos = key + lstrlen(key) - 1;
					}
					else
					{
						*pos = _T('\0');				// Remove the parameter
					}
				}
				else
				{
					*pos = _T('\0');					// Remove the parameter
				}

				lstrcat(pos, _T(" "));
				lstrcat(pos, strURL);

				USES_CONVERSION;
				result = (HINSTANCE) (long long) WinExec(T2A(key), SW_SHOW);
			}
		}
	}

	return result;
}
#endif

#ifdef WIN32
bool CUtil::Execute(const stlString& strFilePath, const stlString& strParameters /* = _T("")*/, bool bShow /* = true*/, bool bWait /* = false*/)
{
	SHELLEXECUTEINFO info;

	info.cbSize = sizeof(SHELLEXECUTEINFO);
	info.fMask = SEE_MASK_NOCLOSEPROCESS;
	info.hwnd = NULL;
	info.lpVerb = _T("");

	info.lpFile = strFilePath.c_str();
	info.lpParameters = strParameters.c_str();
	info.lpDirectory = NULL;

	if(bShow)
	{
		info.nShow = SW_SHOWNORMAL;
	}
	else
	{
		info.nShow = SW_HIDE;
	}

	info.hInstApp = NULL;

	if(!::ShellExecuteEx(&info))
	{
		return false;
	}
	else
	{
		if(bWait)
		{
			::WaitForSingleObject(info.hProcess, INFINITE);
		}
	}

	return true;
}
#endif

stlString CUtil::GenerateRandomString(long lMaxLength, bool bIncludeAlpha /* = true*/, bool bIncludeNumbers /* = true*/)
{
	string strRamdom = "";

	long lLength = lMaxLength + 1;

	while(lLength > lMaxLength)
	{
		lLength = rand() + 1;
	}

	long lIndex = 0;
	long lCount = lLength;

	for(lIndex = 0; lIndex < lCount; lIndex++)
	{
		while(true)
		{
			char cRandom = rand() / 128;

			if(bIncludeAlpha && bIncludeNumbers)
			{
				if(isalnum(cRandom))
				{
					strRamdom += cRandom;

					break;
				}
			}
			else
			if(bIncludeAlpha)
			{
				if(isalpha(cRandom))
				{
					strRamdom += cRandom;

					break;
				}
			}
			else
			if(bIncludeNumbers)
			{
				if(isdigit(cRandom))
				{
					strRamdom += cRandom;

					break;
				}
			}
			else
			{
				strRamdom += cRandom;

				break;
			}
		}
	}

	return GetStringW(strRamdom);
}
