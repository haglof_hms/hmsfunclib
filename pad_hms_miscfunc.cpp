#include "StdAfx.h"
#include "pad_hms_miscfunc.h"

#include "memdc.h"

#include "net.h"

#include "math.h"

#include "messageDlg.h"

#include "fileversioninfo.h"

#include <string.h>

#include <winsock2.h>
#include <ws2tcpip.h>

#include <atlbase.h>

#include <time.h>

#include <sys\types.h> 
#include <sys\stat.h> 

#include <fstream>
#include <Aclapi.h>	// filesystem permissions

#pragma comment(lib, "Ws2_32.lib")

HINSTANCE g_hInstanceDLL = NULL;

BOOL CALLBACK EnumLocalesProc(LPTSTR lpLocaleString);

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

//////////////////////////////////////////////////////////////////////////
// CMyReportCtrl; derived from CXTPReportControl
IMPLEMENT_DYNCREATE(CMyReportCtrl, CXTPReportControl)

BEGIN_MESSAGE_MAP(CMyReportCtrl, CXTPReportControl)
	ON_WM_CHAR()
	ON_WM_KEYUP()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


CMyReportCtrl::CMyReportCtrl()
	: CXTPReportControl()
{
	bIsDirty = FALSE;
	m_bDoValidateCol = FALSE;
	m_fValidateValue = 0.0;
	m_bDoEditFirstColumn = TRUE;
	m_bRunMetrics = TRUE;
}

// Copy constructor
CMyReportCtrl::CMyReportCtrl(const CMyReportCtrl &c)
{
	*this = c;
}

BOOL CMyReportCtrl::Create(CWnd* pParentWnd,UINT nID,BOOL add_hscroll,BOOL run_metrics)
{

	CXTPReportControl::Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,
														CRect(0,0,0,0),
														pParentWnd,
														nID);

	if (add_hscroll)
	{
		GetReportHeader()->SetAutoColumnSizing( FALSE );
		EnableScrollBar(SB_HORZ, TRUE );
	}
	else
	{
		GetReportHeader()->SetAutoColumnSizing( TRUE );
		EnableScrollBar(SB_HORZ, FALSE );
	}

	EnableScrollBar(SB_VERT, TRUE );
	m_bRunMetrics = run_metrics;
	m_nID = nID;
	return TRUE;
}

// virtual method; 070205 p�d
void CMyReportCtrl::GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
{
	CXTPReportControl::GetItemMetrics(pDrawArgs, pItemMetrics);

	CXTPReportControl *rep = pDrawArgs->pControl;
	if (rep != NULL)
	{
		if (m_bRunMetrics)
		{
			CDC *pDC = pDrawArgs->pDC; //GetDC();
			CXTPReportRecords *pRecs = NULL;
			CString sMsg;
			int nCol = -1;
			double fValue = 0.0;
			CXTPReportColumn *pCol = rep->GetFocusedColumn();
	
			nCol = pDrawArgs->pColumn->GetIndex();
			if (nCol == 0)
			{
				pDrawArgs->pColumn->SetEditable( m_bDoEditFirstColumn );
			}

			if (nCol > 0 && isDoValidate())
			{

				pRecs = rep->GetRecords();
				if (pRecs != NULL)
				{

					fValue = 0.0;
					for (int i = 0;i < pRecs->GetCount();i++)
					{
						CXTPReportRecord *pRecord = pRecs->GetAt(i);
						if (pRecord != NULL)
						{
							CXTPReportRecordItem *pRec = pRecord->GetItem(pDrawArgs->pColumn);
							fValue += _tstof(pRec->GetCaption(pDrawArgs->pColumn));
						}	// if (pRecord != NULL)
					}	// for (int i = 0;i < pRecs->GetCount();i++)
#ifndef UNICODE
					sMsg.Format(_T("%.1f"),fValue);
					if (pDrawArgs->pColumn != NULL)
					{
						pDrawArgs->pColumn->SetFooterText((sMsg));
					}
#endif
				}	// if (pRecs != NULL)

				if (fValue != getValidateValue())
				{
					pItemMetrics->clrForeground = RGB(255,0,0);
					pItemMetrics->clrBackground = RGB(255,255,255);
				}
				else
				{
					pItemMetrics->clrBackground = RGB(255,255,255);
				}

			}	// if (nCol > 0 && isDoValidate())
		}	// if (m_bRunMetrics)
	}

}

void CMyReportCtrl::OnDraw(CDC* pDC)
{
	CXTPReportControl::OnDraw(pDC);
}

void CMyReportCtrl::OnSelectionChanged()
{
/*
	CString S;
	CXTPReportControl *rep = this;
	CXTPReportColumns *cols = NULL;
	if (rep->GetSafeHwnd() != NULL)
	{
		if (m_bRunMetrics)
		{
			cols = rep->GetColumns();
			if (cols != NULL)
			{
				CXTPReportRecords *pRecs = NULL;
				CString sMsg;
				int nCol = -1;
				double fValue = 0.0;
				CXTPReportColumn *pCol = rep->GetFocusedColumn();
		
				nCol = pCol->GetIndex();
				if (nCol == 0)
				{
					pCol->SetEditable( m_bDoEditFirstColumn );
					rep->GetPaintManager()->m_clrHighlightText = RGB(0,0,255);
				}
				for	(int i = 1;i < cols->GetCount();i++)
				{
					if (isDoValidate())
					{
						pRecs = GetRecords();
						if (pRecs != NULL)
						{
							fValue = 0.0;
							for (int i = 0;i < pRecs->GetCount();i++)
							{
								CXTPReportRecord *pRecord = pRecs->GetAt(i);
								if (pRecord != NULL)
								{
									CXTPReportRecordItem *pRec = pRecord->GetItem(pCol);
									fValue += _tstof(pRec->GetCaption(pCol));
								}	// if (pRecord != NULL)
							}	// for (int i = 0;i < pRecs->GetCount();i++)
						}	// if (pRecs != NULL)
						sMsg.Format("%.1f",fValue);
						pCol->SetFooterText(sMsg);
						if (i == nCol)
						{
							if (fValue != getValidateValue())
							{
								
								rep->GetPaintManager()->m_clrHighlightText = RGB(255,0,0);
							}
							else
							{
								rep->GetPaintManager()->m_clrHighlightText = RGB(255,255,255);
							}
						}	// if (nCol == i)
					}	// if (nCol > 0 && isDoValidate())
				}
			}
		}
	}

*/
	CXTPReportControl::OnSelectionChanged();
}


BOOL CMyReportCtrl::isDirty(void)
{
	isDataValid();
	return bIsDirty;
}

void CMyReportCtrl::setIsDirty(BOOL val)
{
	bIsDirty = val;
}

BOOL CMyReportCtrl::ClearReport(void)
{
	CXTPReportControl *rep = this;
	if (rep->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords* pRecords = rep->GetRecords();

		if (pRecords)
		{
			pRecords->RemoveAll();
			return TRUE;
		}
	}
	return FALSE;
}

// Check if data in Report grid is valid; 070205 p�d
BOOL CMyReportCtrl::isDataValid(void)
{
	double fValue;
	CString sValue;
	if (isDoValidate())
	{
		CXTPReportControl *rep = this;
		if (rep->GetSafeHwnd() != NULL)
		{
			CXTPReportRecords *pRecs = rep->GetRecords();
			CXTPReportColumns* pCols = rep->GetColumns();

			if (pCols != NULL && pRecs != NULL)
			{
				for (int col = 0;col < pCols->GetCount();col++)
				{
					if (col > 0)
					{
						fValue = 0.0;
						for (int i = 0;i < pRecs->GetCount();i++)
						{
							CXTPReportRecord *pRecord = pRecs->GetAt(i);
							if (pRecord != NULL)
							{
								CXTPReportRecordItem *pRec = pRecord->GetItem(pCols->GetAt(col));
								sValue = pRec->GetCaption(pCols->GetAt(col));
                fValue += _tstof(sValue);
							}	// if (pRecord != NULL)
						}	// for (int i = 0;i < pRecs->GetCount();i++)
						if (fValue != getValidateValue())
							return FALSE;
					}	// if (col >= 0)
				}	// for (int col = 0;col < pCols->GetCount();col++)
			}	// if (pCols != NULL && pRecs != NULL)
		}	// if (rep->GetSafeHwnd() != NULL)
	}	// if (isDoValidate())

	return TRUE;

}

void CMyReportCtrl::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	bIsDirty = TRUE;
//	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
//	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
	CXTPReportControl::OnChar(nChar,nRepCnt,nFlags);
}

void CMyReportCtrl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	bIsDirty = TRUE;
//	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
//	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
	CXTPReportControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

void CMyReportCtrl::OnLButtonUp(UINT flag,CPoint p)
{
	CXTPReportControl::OnLButtonUp(flag,p);
}

//////////////////////////////////////////////////////////////////////////////////
// 	CMyReportView

IMPLEMENT_DYNCREATE(CMyReportView, CXTPReportView)

BEGIN_MESSAGE_MAP(CMyReportView, CXTPReportView)
	ON_WM_CHAR()
	ON_WM_KEYUP()
END_MESSAGE_MAP()

CMyReportView::CMyReportView()
	: CXTPReportView()
{
	m_bIsDirty = FALSE;
}

void CMyReportView::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	m_bIsDirty = TRUE;
	CXTPReportView::OnChar(nChar,nRepCnt,nFlags);
}

void CMyReportView::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	m_bIsDirty = TRUE;
	CXTPReportView::OnKeyUp(nChar,nRepCnt,nFlags);
}


//////////////////////////////////////////////////////////////////////////////////
// 	CMyPropertyGrid

IMPLEMENT_DYNCREATE(CMyPropertyGrid, CXTPPropertyGrid)

BEGIN_MESSAGE_MAP(CMyPropertyGrid, CXTPPropertyGrid)
	ON_WM_CHAR()
	ON_WM_KEYUP()
END_MESSAGE_MAP()

CMyPropertyGrid::CMyPropertyGrid()
	: CXTPPropertyGrid()
{
	m_bIsDirty = FALSE;
}

void CMyPropertyGrid::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	m_bIsDirty = TRUE;
	CXTPPropertyGrid::OnChar(nChar,nRepCnt,nFlags);
}

void CMyPropertyGrid::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	m_bIsDirty = TRUE;
	CXTPPropertyGrid::OnKeyUp(nChar,nRepCnt,nFlags);
}

void CMyPropertyGrid::OnSelectionChanged(CXTPPropertyGridItem* pItem)
{
	m_bIsDirty = TRUE;
	CXTPPropertyGrid::OnSelectionChanged(pItem);
}

void CMyPropertyGrid::OnNavigate(XTPPropertyGridUI nUIElement,BOOL bForward,CXTPPropertyGridItem* pItem)
{
	CXTPPropertyGrid::OnNavigate(nUIElement,bForward,pItem);
}


//////////////////////////////////////////////////////////////////////////////////
// 	CMyTabControl

CMyTabControl::CMyTabControl()
	: CXTPTabControl()
{
}

void CMyTabControl::RedrawControl(LPCRECT lpRect,BOOL bAnimate)
{
	CXTPTabControl::RedrawControl(lpRect,bAnimate);
}

void CMyTabControl::OnItemClick(CXTPTabManagerItem* pItem)
{
	CXTPTabControl::OnItemClick(pItem);

	CXTPTabManager *pTabManager = pItem->GetTabManager();
	CXTPTabManagerItem *pTabPage = NULL;
	if (pTabManager)
	{
		pTabPage = GetSelectedItem();
		if (pTabPage)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, 
										 ID_WPARAM_VALUE_FROM + 0xFA,
										 pTabPage->GetIndex());
		}	// if (pTabPage)
	}	// if (pTabManager)
	
}

CXTPTabManagerItem *CMyTabControl::getSelectedTabPage(void)
{

	CXTPTabManagerItem *pTabPage = NULL;
	pTabPage = GetSelectedItem();
	if (pTabPage)
	{
		return pTabPage;
	}

	return NULL;
}

CXTPTabManagerItem *CMyTabControl::getTabPage(int idx)
{
	// Make sure the index (idx) is less than tabs; 060405 p�d
	if (idx < GetItemCount())
	{
		CXTPTabManagerItem *pTabPage = NULL;
		pTabPage = GetItem(idx);
		if (pTabPage)
		{
			return pTabPage;
		}
	}
	return NULL;
}

int CMyTabControl::getNumOfTabPages(void)
{
	return GetItemCount();
}


//////////////////////////////////////////////////////////////////////////////////
// 	CMyExcelTabControl

IMPLEMENT_DYNCREATE(CMyExcelTabControl, CXTExcelTabCtrl)

BEGIN_MESSAGE_MAP(CMyExcelTabControl, CXTExcelTabCtrl)
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

CMyExcelTabControl::CMyExcelTabControl()
	: CXTExcelTabCtrl()
{
}

void CMyExcelTabControl::OnRButtonUp(UINT nFlags,CPoint point)
{
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, 
								 ID_WPARAM_VALUE_FROM + 0xFB,
								 GetCurSel());

	CXTExcelTabCtrl::OnRButtonUp(nFlags,point);
}


/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit

//	Defines
//
#define BLANK_ENTRY		-1

BEGIN_MESSAGE_MAP(CMyExtEdit, CXTEdit)
	//{{AFX_MSG_MAP(CMyExtEdit)
	ON_WM_KEYUP()
	ON_WM_CTLCOLOR_REFLECT()
	ON_CONTROL_REFLECT(EN_UPDATE, OnUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyExtEdit::CMyExtEdit()

{
  // defaults
  m_crFGEnabled = ENABLED_FG;
  m_crBGEnabled = ENABLED_BG;
  m_crFGDisabled = DISABLED_FG;
  m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
  m_pbrushEnabled = new CBrush;
  m_pbrushDisabled = new CBrush;
  m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);

	m_bModified = FALSE;
	m_bIsNumeric = FALSE;
	m_bIsAsH100	= FALSE;
	m_bIsValidate = FALSE;

	m_nItemData = -1;
	m_sItemData = _T("");

	m_nMin  = 0;		//	Initialize member variables to zero
	m_nMax	= 99999;
	m_nLastValidValue = 0;

	szValue[0] = '\0';
	szIdentifer[0] = '\0';
}

CMyExtEdit::~CMyExtEdit()
{
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}

BOOL CMyExtEdit::PreTranslateMessage(MSG *pMSG)
{
	UINT  nKeyCode = pMSG->wParam; // virtual key code of the key pressed
	if (pMSG->message == WM_KEYDOWN)
	{
		if (nKeyCode < 65)
		{
			return 0;
		}

	  // CTRL+C, CTRL+X, or, CTRL+V?
	  if ( nKeyCode == _T('C') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Copy();
	  }
	  else if( nKeyCode == _T('X') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Cut();
	  }
	  else if( nKeyCode == _T('V') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Paste();
	  }

	  // Only accept numberic input?
	  if (m_bIsNumeric)
	  {
		if (isNumeric(nKeyCode))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	  }
	  else
	  {
			return 0;
	  }
	}

	return CXTEdit::PreTranslateMessage(pMSG);
}

void CMyExtEdit::RemoveLeadingZeros ( void )
{
	if (m_bIsNumeric && m_bIsValidate)
	{
		//	Local Variables
		CString strWindowText;
		SHORT index;
		//	Initialize Variables
		strWindowText.Empty ( );
		index = 0;
		//	Copy the contents of the edit control
		//		to a string
		GetWindowText ( strWindowText );
		//	Check if the length of the text in the edit
		//		control is greater than 1
		//
		//	If the length is 1 then there can not be any
		//		leading zeroes
		//
		if ( strWindowText.GetLength ( ) > 1 )
		{
			//	Get the index of the first
			//		non-zero character
			//
			index = strWindowText.FindOneOf ( _T("123456789") );	
			//	Check if there are any leading zeroes
			//		if the first non-zero character
			//		is at any position but 0, then
			//		there are leading zeros
			//
			if ( index > 0 )
			{
				//	Leading zeroes were found
				//		Remove them
				//
				strWindowText = strWindowText.Mid ( index );
			}
			//	Check if no non-zero characters were found
			//		That means there are only zeros in the
			//		string
			//
			else  if ( index == -1 )
			{
				//	There is only zeroes in the string
				//		change the string to only one zero
				//
				strWindowText = "0";
			}
			//	Check if the text was formatted, the index is not 0
			//
			if ( index != 0 )
			{
				//	The text was formatted, update the edit controls text
				//
				SetWindowText ( strWindowText );
				//	Set the cursor position
				//
				SetSel ( 0, 0 );
			}
		}
	} // if (m_bIsNumeric)
}

/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit message handlers

void CMyExtEdit::PreSubclassWindow() 
{
	CXTEdit::PreSubclassWindow();
}

void CMyExtEdit::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	char tmp[120];
	if (nChar == VK_TAB && !m_bModified)
		m_bModified = FALSE;
	else
		m_bModified = TRUE;
	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	CXTEdit::OnKeyUp(nChar,nRepCnt,nFlags);
}


void CMyExtEdit::SetAsNumeric(void)
{
	m_bIsNumeric = TRUE;
}

void CMyExtEdit::SetAsH100(void)
{
	m_bIsAsH100 = TRUE;
}

HBRUSH CMyExtEdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(GetStyle() & ES_READONLY)
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
}

void CMyExtEdit::OnUpdate() 
{
	if (m_bIsNumeric && m_bIsValidate)
	{
		//	Local Variables
		CString strWindowText;
		//	Initialize Variables
		strWindowText.Empty ( );
		LONG ValidationNumber;
		ValidationNumber = 0;
		//	Remove the leading zeros from the	edit control
		RemoveLeadingZeros();
		//	Copy the contents of the edit control to a string
		GetWindowText ( strWindowText );
		//	Check if the edit control is blank
		if ( strWindowText.IsEmpty ( ) )
		{
			//	The operator erased the last number.  Thus the last valid value
			//		entered by the operator is NULL.
			m_nLastValidValue = BLANK_ENTRY;
		}
		else
		{
			//	There is a number to validate in the string
			//	Convert the string to a LONG
			ValidationNumber = _tstol ( strWindowText );
			//	Check if the number falls within the specified range
			if ( ( ValidationNumber < m_nMin ) || ( ValidationNumber > m_nMax ) )
			{
				//	The number is invalid, it is out of the specified range
				//	Check what type of value the last Valid Value is:
				//		1.	A number:	which is indicated by a value greater than
				//						or equal to 0.
				//
				//		2.	NULL:		which is indicated by BLANK_ENTRY (-1).
				//
				if ( m_nLastValidValue >= 0 )
				{
					//	The last value is a number
					//
					strWindowText.Format ( _T("%d"), m_nLastValidValue );
				}
				else
				{
					//	The last value was a NULL
					//
					strWindowText.Empty ( );
				}
				//	Set the Edit control with the Last Valid Value
				//
				SetWindowText ( strWindowText );
				//	Place the cursor at the end of the string
				//
				SetSel ( strWindowText.GetLength ( ), strWindowText.GetLength ( ) );
				//	Alert the operator of the error
				//
				MessageBeep ( 0xFFFFFFFF );
			}
			else
			{
				//	The number is valid, that is, it is in the specified range
				//
				//	Set the last valid value to the current number 
				//		in the edit control
				//
				m_nLastValidValue = ValidationNumber;
			}
		}
	}	// if (m_bIsNumeric)
	this->RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
}

void CMyExtEdit::SetReadOnly(BOOL flag)
{
		if (flag == TRUE)
		{
			ModifyStyle(WS_TABSTOP,0);
		}
		else
		{
			ModifyStyle(0,WS_TABSTOP);
		}
		Invalidate(TRUE);

		CXTEdit::SetReadOnly( flag );
}

void CMyExtEdit::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyExtEdit::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
  // Setting the colors and the brush
  m_crFGDisabled = crFG;
  m_crBGDisabled = crBG;
  delete m_pbrushDisabled;
  m_pbrushDisabled = new CBrush;
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

void CMyExtEdit::SetFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}






/////////////////////////////////////////////////////////////////////////////
// CMyMaskExtEdit

BEGIN_MESSAGE_MAP(CMyMaskExtEdit, CXTMaskEdit)
	//{{AFX_MSG_MAP(CMyMaskExtEdit)
	ON_WM_KEYUP()
	ON_WM_CTLCOLOR_REFLECT()
	ON_CONTROL_REFLECT(EN_UPDATE, OnUpdate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyMaskExtEdit::CMyMaskExtEdit()

{
  // defaults
  m_crFGEnabled = ENABLED_FG;
  m_crBGEnabled = ENABLED_BG;
  m_crFGDisabled = DISABLED_FG;
  m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
  m_pbrushEnabled = new CBrush;
  m_pbrushDisabled = new CBrush;
  m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);

	m_bModified = FALSE;
	m_bIsNumeric = FALSE;
	m_bIsAsH100	= FALSE;
	m_bIsValidate = FALSE;

	m_nItemData = -1;
	m_sItemData = _T("");

	m_nMin  = 0;		//	Initialize member variables to zero
	m_nMax	= 99999;
	m_nLastValidValue = 0;

	szValue[0] = '\0';
	szIdentifer[0] = '\0';


}

CMyMaskExtEdit::~CMyMaskExtEdit()
{
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}

BOOL CMyMaskExtEdit::PreTranslateMessage(MSG *pMSG)
{
	UINT  nKeyCode = pMSG->wParam; // virtual key code of the key pressed
	if (pMSG->message == WM_KEYDOWN)
	{
		if (nKeyCode < 65)
		{
			return 0;
		}

	  // CTRL+C, CTRL+X, or, CTRL+V?
	  if ( nKeyCode == _T('C') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Copy();
	  }
	  else if( nKeyCode == _T('X') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Cut();
	  }
	  else if( nKeyCode == _T('V') && (::GetKeyState(VK_CONTROL) & 0x8000) )
	  {
		  Paste();
	  }

	  // Only accept numberic input?
	  if (m_bIsNumeric)
	  {
		if (isNumeric(nKeyCode))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	  }
	  else
	  {
			return 0;
	  }
	}

	return CXTMaskEdit::PreTranslateMessage(pMSG);
}

void CMyMaskExtEdit::RemoveLeadingZeros ( void )
{
	if (m_bIsNumeric && m_bIsValidate)
	{
		//	Local Variables
		CString strWindowText;
		SHORT index;
		//	Initialize Variables
		strWindowText.Empty ( );
		index = 0;
		//	Copy the contents of the edit control
		//		to a string
		GetWindowText ( strWindowText );
		//	Check if the length of the text in the edit
		//		control is greater than 1
		//
		//	If the length is 1 then there can not be any
		//		leading zeroes
		//
		if ( strWindowText.GetLength ( ) > 1 )
		{
			//	Get the index of the first
			//		non-zero character
			//
			index = strWindowText.FindOneOf ( _T("123456789") );	
			//	Check if there are any leading zeroes
			//		if the first non-zero character
			//		is at any position but 0, then
			//		there are leading zeros
			//
			if ( index > 0 )
			{
				//	Leading zeroes were found
				//		Remove them
				//
				strWindowText = strWindowText.Mid ( index );
			}
			//	Check if no non-zero characters were found
			//		That means there are only zeros in the
			//		string
			//
			else  if ( index == -1 )
			{
				//	There is only zeroes in the string
				//		change the string to only one zero
				//
				strWindowText = "0";
			}
			//	Check if the text was formatted, the index is not 0
			//
			if ( index != 0 )
			{
				//	The text was formatted, update the edit controls text
				//
				SetWindowText ( strWindowText );
				//	Set the cursor position
				//
				SetSel ( 0, 0 );
			}
		}
	} // if (m_bIsNumeric)
}

/////////////////////////////////////////////////////////////////////////////
// CMyMaskExtEdit message handlers

void CMyMaskExtEdit::PreSubclassWindow() 
{
	CXTMaskEdit::PreSubclassWindow();
}

void CMyMaskExtEdit::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	char tmp[120];
	if (nChar == VK_TAB && !m_bModified)
		m_bModified = FALSE;
	else
		m_bModified = TRUE;
	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	CXTMaskEdit::OnKeyUp(nChar,nRepCnt,nFlags);
}


void CMyMaskExtEdit::SetAsNumeric(void)
{
	m_bIsNumeric = TRUE;
}

void CMyMaskExtEdit::SetAsH100(void)
{
	m_bIsAsH100 = TRUE;
}

HBRUSH CMyMaskExtEdit::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(GetStyle() & ES_READONLY)
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
}

void CMyMaskExtEdit::OnUpdate() 
{
	if (m_bIsNumeric && m_bIsValidate)
	{
		//	Local Variables
		CString strWindowText;
		//	Initialize Variables
		strWindowText.Empty ( );
		LONG ValidationNumber;
		ValidationNumber = 0;
		//	Remove the leading zeros from the	edit control
		RemoveLeadingZeros();
		//	Copy the contents of the edit control to a string
		GetWindowText ( strWindowText );
		//	Check if the edit control is blank
		if ( strWindowText.IsEmpty ( ) )
		{
			//	The operator erased the last number.  Thus the last valid value
			//		entered by the operator is NULL.
			m_nLastValidValue = BLANK_ENTRY;
		}
		else
		{
			//	There is a number to validate in the string
			//	Convert the string to a LONG
			ValidationNumber = _tstol ( strWindowText );
			//	Check if the number falls within the specified range
			if ( ( ValidationNumber < m_nMin ) || ( ValidationNumber > m_nMax ) )
			{
				//	The number is invalid, it is out of the specified range
				//	Check what type of value the last Valid Value is:
				//		1.	A number:	which is indicated by a value greater than
				//						or equal to 0.
				//
				//		2.	NULL:		which is indicated by BLANK_ENTRY (-1).
				//
				if ( m_nLastValidValue >= 0 )
				{
					//	The last value is a number
					//
					strWindowText.Format ( _T("%d"), m_nLastValidValue );
				}
				else
				{
					//	The last value was a NULL
					//
					strWindowText.Empty ( );
				}
				//	Set the Edit control with the Last Valid Value
				//
				SetWindowText ( strWindowText );
				//	Place the cursor at the end of the string
				//
				SetSel ( strWindowText.GetLength ( ), strWindowText.GetLength ( ) );
				//	Alert the operator of the error
				//
				MessageBeep ( 0xFFFFFFFF );
			}
			else
			{
				//	The number is valid, that is, it is in the specified range
				//
				//	Set the last valid value to the current number 
				//		in the edit control
				//
				m_nLastValidValue = ValidationNumber;
			}
		}
	}	// if (m_bIsNumeric)
	this->RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
}

void CMyMaskExtEdit::SetReadOnly(BOOL flag)
{
		if (flag == TRUE)
		{
			ModifyStyle(WS_TABSTOP,0);
		}
		else
		{
			ModifyStyle(0,WS_TABSTOP);
		}
		Invalidate(TRUE);

		CXTMaskEdit::SetReadOnly( flag );
}

void CMyMaskExtEdit::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyMaskExtEdit::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
  // Setting the colors and the brush
  m_crFGDisabled = crFG;
  m_crBGDisabled = crBG;
  delete m_pbrushDisabled;
  m_pbrushDisabled = new CBrush;
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

void CMyMaskExtEdit::SetFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}



/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic

CMyExtStatic::CMyExtStatic()
{
	m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing background color to the system face color.
	m_crTextColor = BLACK; // Initializing text color to black
	m_brBkgnd.CreateSolidBrush(m_crBkColor); // Creating the Brush Color For the Edit Box Background
	m_fnt1 = new CFont();
}

CMyExtStatic::~CMyExtStatic()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
}


BEGIN_MESSAGE_MAP(CMyExtStatic, CStatic)
	//{{AFX_MSG_MAP(CMyExtStatic)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic message handlers

void CMyExtStatic::SetTextColor(COLORREF crColor)
{
	m_crTextColor = crColor; // Passing the value passed by the dialog to the member varaible for Text Color
	RedrawWindow();
}

void CMyExtStatic::SetBkColor(COLORREF crColor)
{
	m_crBkColor = crColor; // Passing the value passed by the dialog to the member varaible for Backgound Color
	m_brBkgnd.DeleteObject(); // Deleting any Previous Brush Colors if any existed.
	m_brBkgnd.CreateSolidBrush(crColor); // Creating the Brush Color For the Edit Box Background
	RedrawWindow();
}

HBRUSH CMyExtStatic::CtlColor(CDC* pDC, UINT nCtlColor)
{
	HBRUSH hbr;
	hbr = (HBRUSH)m_brBkgnd; // Passing a Handle to the Brush
	pDC->SetBkColor(m_crBkColor); // Setting the Color of the Text Background to the one passed by the Dialog
	pDC->SetTextColor(m_crTextColor); // Setting the Text Color to the one Passed by the Dialog

	if (nCtlColor)       // To get rid of compiler warning
      nCtlColor += 0;

	return hbr;
}

void CMyExtStatic::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;

	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

void CMyExtStatic::SetLblFontEx(int size,int weight,BOOL underline,BOOL italic)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	CFont* cf = GetFont();
	if (cf)
		cf->GetObject(sizeof(lf), &lf);
	else
		GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

	if (size > -1) lf.lfHeight = size;
	lf.lfWeight = weight;
	lf.lfUnderline = underline;
	lf.lfItalic = italic;

	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}


////////////////////////////////////////////////////////////////////////////
// CMyExtTranspStatic

IMPLEMENT_DYNAMIC(CMyExtTranspStatic, CStatic)

BEGIN_MESSAGE_MAP(CMyExtTranspStatic, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()

CMyExtTranspStatic::CMyExtTranspStatic()
{
	m_brBkGnd.CreateSolidBrush(RGB(255,0,150));
}

CMyExtTranspStatic::~CMyExtTranspStatic()
{
}

// CMyExtTranspStatic message handlers

void CMyExtTranspStatic::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	// Where to draw text
	CRect client_rect;
	GetClientRect(client_rect);

	// Get the caption
	CString szText;
	GetWindowText(szText);

	// Get the font
	CFont *pFont, *pOldFont;
	pFont = GetFont();
	pOldFont = dc.SelectObject(pFont);

	// Map "Static Styles" to "Text Styles"
#define MAP_STYLE(src, dest) if (dwStyle & (src)) dwText |= (dest)
#define NMAP_STYLE(src, dest) if (!(dwStyle & (src))) dwText |= (dest)

	DWORD dwStyle = GetStyle(), dwText = 0;

	MAP_STYLE(	SS_RIGHT,			DT_RIGHT					);
	MAP_STYLE(	SS_CENTER,			DT_CENTER					);
	MAP_STYLE(	SS_CENTERIMAGE,		DT_VCENTER | DT_SINGLELINE	);
	MAP_STYLE(	SS_NOPREFIX,		DT_NOPREFIX					);
	MAP_STYLE(	SS_WORDELLIPSIS,	DT_WORD_ELLIPSIS			);
	MAP_STYLE(	SS_ENDELLIPSIS,		DT_END_ELLIPSIS				);
	MAP_STYLE(	SS_PATHELLIPSIS,	DT_PATH_ELLIPSIS			);

	NMAP_STYLE(	SS_LEFTNOWORDWRAP |
				SS_CENTERIMAGE |
				SS_WORDELLIPSIS |
				SS_ENDELLIPSIS |
				SS_PATHELLIPSIS,	DT_WORDBREAK				);

	// Set transparent background
	dc.SetBkMode(TRANSPARENT);

	dc.SelectObject((HBRUSH)&m_brBkGnd);

	// Draw the text
	dc.DrawText(szText, client_rect, dwText);

	// Select old font
	dc.SelectObject(pOldFont);
}


////////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CMyExtCBox, CComboBox)
	//{{AFX_MSG_MAP(CMyExtCBox)
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_CONTROL_REFLECT_EX(CBN_EDITCHANGE, OnCBoxChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyExtCBox::CMyExtCBox()
{
	m_bIsDirty = FALSE;
	m_bRO = FALSE;
	m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing background color to the system face color.
	m_crTextColor = BLACK; // Initializing text color to black
	m_brBkgnd.CreateSolidBrush(m_crBkColor); // Creating the Brush Color For the Edit Box Background
	m_fnt1 = new CFont();
}

CMyExtCBox::~CMyExtCBox()
{
}

void CMyExtCBox::OnDestroy()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
	if (m_wndEdit.GetSafeHwnd() != NULL)
			m_wndEdit.UnsubclassWindow();
  if (m_wndLB.GetSafeHwnd() != NULL)
		m_wndLB.UnsubclassWindow();
  
	CComboBox::OnDestroy();
}

BOOL CMyExtCBox::OnCBoxChange()
{
	m_bIsDirty = TRUE;

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CMyExtCBox message handlers

void CMyExtCBox::SetTextColor(COLORREF crColor)
{
	m_crTextColor = crColor; // Passing the value passed by the dialog to the member varaible for Text Color
	RedrawWindow();
}

void CMyExtCBox::SetBkColor(COLORREF crColor)
{
	m_crBkColor = crColor; // Passing the value passed by the dialog to the member varaible for Backgound Color
	m_brBkgnd.DeleteObject(); // Deleting any Previous Brush Colors if any existed.
	m_brBkgnd.CreateSolidBrush(crColor); // Creating the Brush Color For the Edit Box Background
	RedrawWindow();
}



//HBRUSH CMyExtCBox::CtlColor(CDC* pDC, UINT nCtlColor)
HBRUSH CMyExtCBox::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{

	HBRUSH hbr;
	hbr = (HBRUSH)m_brBkgnd; // Passing a Handle to the Brush
	pDC->SetBkColor(m_crBkColor); // Setting the Color of the Text Background to the one passed by the Dialog
	pDC->SetTextColor(m_crTextColor); // Setting the Text Color to the one Passed by the Dialog
	pDC->SetBkMode( TRANSPARENT );

	if (nCtlColor == CTLCOLOR_EDIT && pWnd != NULL)
	{
			// Edit control
      if (m_wndEdit.GetSafeHwnd() == NULL)
				m_wndEdit.SubclassWindow(pWnd->GetSafeHwnd());
      if (m_wndEdit.GetSafeHwnd() != NULL)
			{
				RECT rc;
				m_wndEdit.GetClientRect(&rc);
				m_wndEdit.GetDC()->FillRect(&rc,&m_brBkgnd);
				m_wndEdit.SetReadOnly( TRUE );
			}
	}
  else if (nCtlColor == CTLCOLOR_LISTBOX)
  {
		//ListBox control
    if (m_wndLB.GetSafeHwnd() == NULL)
         m_wndLB.SubclassWindow(pWnd->GetSafeHwnd());
    if (m_wndLB.GetSafeHwnd() != NULL)
		{
			if (m_bRO)
			{
				m_wndLB.ShowWindow( SW_HIDE );
			}
			else
			{
				m_wndLB.ShowWindow( SW_SHOW );
			}
		}
  }

	
	CComboBox::OnCtlColor(pDC,pWnd,nCtlColor);
	
	return hbr;
}

void CMyExtCBox::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

////////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CMyExtListBox, CListBox)
	//{{AFX_MSG_MAP(CMyExtListBox)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyExtListBox::CMyExtListBox()
{
	m_fnt1 = new CFont();
}

CMyExtListBox::~CMyExtListBox()
{
}

void CMyExtListBox::OnDestroy()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
  
	CListBox::OnDestroy();
}

void CMyExtListBox::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}


//////////////////////////////////////////////////////////////////////////


// CMyComboBox

BEGIN_MESSAGE_MAP(CMyComboBox, CComboBox)
  //{{AFX_MSG_MAP(CMyComboBox)
  ON_WM_CTLCOLOR()
  ON_WM_DESTROY()
  ON_WM_CTLCOLOR_REFLECT()
  ON_WM_ENABLE()
	ON_CONTROL_REFLECT_EX(CBN_EDITCHANGE, OnCBoxChange)
	ON_CONTROL_REFLECT_EX(CBN_DROPDOWN, OnCBoxDropDown)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyComboBox::CMyComboBox(BOOL bEditable)
{
	// defaults
	m_bIsDirty = FALSE;
  m_bEditable = bEditable;
  m_crFGEnabled = ENABLED_FG;
  m_crBGEnabled = ENABLED_BG;
  m_crFGDisabled = DISABLED_FG;
  m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
  m_pbrushEnabled = new CBrush;
  m_pbrushDisabled = new CBrush;
  m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);

}

CMyComboBox::~CMyComboBox()
{
	m_font.DeleteObject();
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}

// CMyComboBox message handlers

HBRUSH CMyComboBox::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(IsWindowEnabled())
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
}

HBRUSH CMyComboBox::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(IsWindowEnabled())
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   return NULL;
}

void CMyComboBox::OnEnable(BOOL bEnable)
{
   CComboBox::OnEnable(bEnable);

   // TODO: Add your message handler code here

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);
	 if (pComboEdit != NULL)
	 {
	   // Setting the edit ctrl always to enable
		 pComboEdit->EnableWindow(TRUE);
		pComboEdit->SetReadOnly(!(m_bEditable && bEnable));
		Invalidate();
	 }
}

void CMyComboBox::SetReadOnly(BOOL bReadOnly)
{
   m_bEditable = !bReadOnly;
   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);
	 if (pComboEdit != NULL)
	 {
		 //EnableWindow( !bReadOnly );
		 // Setting the edit ctrl always to enable
		pComboEdit->EnableWindow(TRUE);
		pComboEdit->SetReadOnly(!(m_bEditable && IsWindowEnabled()));
		Invalidate();
	 }

}

void CMyComboBox::SetReadOnlyEx(BOOL bReadOnly)
{
   m_bEditable = !bReadOnly;

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);
	 if (pComboEdit != NULL)
	 {
	   // Setting the edit ctrl always to enable
		 pComboEdit->EnableWindow(TRUE);
	   pComboEdit->SetReadOnly(!m_bEditable);
		 Invalidate();
	 }
}

void CMyComboBox::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyComboBox::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGDisabled = crFG;
   m_crBGDisabled = crBG;
   delete m_pbrushDisabled;
   m_pbrushDisabled = new CBrush;
   m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

CString CMyComboBox::getText(void)
{
	int nIndex = GetCurSel();
	int nLength;
	CString sBuffer;
	if (nIndex != CB_ERR)	
	{
		nLength = GetLBTextLen(nIndex);
		GetLBText(nIndex,sBuffer.GetBuffer(nLength));
		sBuffer.ReleaseBuffer();
		return sBuffer;
	}
	else
	{
		GetWindowText(sBuffer);
		return sBuffer;
	}
		return _T("");
}

void CMyComboBox::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_font.CreateFontIndirect(&lf);

	SetFont(&m_font);
}

// Events
BOOL CMyComboBox::OnCBoxChange()
{
	m_bIsDirty = TRUE;

	return FALSE;
}

BOOL CMyComboBox::OnCBoxDropDown()
{
	m_bIsDirty = TRUE;

	return FALSE;
}

// CMyComboBoxHistory

BEGIN_MESSAGE_MAP(CMyComboBoxHistory, CHistoryCombo) //CComboBox)
  //{{AFX_MSG_MAP(CMyComboBoxHistory)
  ON_WM_CTLCOLOR()
  ON_WM_DESTROY()
  ON_WM_CTLCOLOR_REFLECT()
  ON_WM_ENABLE()
	ON_CONTROL_REFLECT_EX(CBN_EDITCHANGE, OnCBoxChange)
	ON_CONTROL_REFLECT_EX(CBN_DROPDOWN, OnCBoxDropDown)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyComboBoxHistory::CMyComboBoxHistory(BOOL bEditable)
{
	// defaults
	m_bIsDirty = FALSE;
  m_bEditable = bEditable;
  m_crFGEnabled = ENABLED_FG;
  m_crBGEnabled = ENABLED_BG;
  m_crFGDisabled = DISABLED_FG;
  m_crBGDisabled = DISABLED_BG;

  // to changes the colors dynamic
  m_pbrushEnabled = new CBrush;
  m_pbrushDisabled = new CBrush;
  m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
  m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);

}

CMyComboBoxHistory::~CMyComboBoxHistory()
{
	m_font.DeleteObject();
  delete m_pbrushEnabled;
  delete m_pbrushDisabled;
}

// CMyComboBoxHistory message handlers

HBRUSH CMyComboBoxHistory::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(IsWindowEnabled())
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
}

HBRUSH CMyComboBoxHistory::CtlColor(CDC* pDC, UINT nCtlColor)
{
   // Setting color in en- disabled mode
   if(IsWindowEnabled())
   {
      pDC->SetTextColor(m_crFGEnabled);
      pDC->SetBkColor(m_crBGEnabled);
      return *m_pbrushEnabled;
   }
   else
   {
      pDC->SetTextColor(m_crFGDisabled);
      pDC->SetBkColor(m_crBGDisabled);
      return *m_pbrushDisabled;
   }
   return NULL;
}

void CMyComboBoxHistory::OnEnable(BOOL bEnable)
{
   CComboBox::OnEnable(bEnable);

   // TODO: Add your message handler code here

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);

   // Setting the edit ctrl always to enable
   pComboEdit->EnableWindow(TRUE);
   pComboEdit->SetReadOnly(!(m_bEditable && bEnable));
   Invalidate();
}

void CMyComboBoxHistory::SetReadOnly(BOOL bReadOnly)
{
   m_bEditable = !bReadOnly;

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);

	 EnableWindow( !bReadOnly );
   // Setting the edit ctrl always to enable
   pComboEdit->EnableWindow(TRUE);
   pComboEdit->SetReadOnly(!(m_bEditable && IsWindowEnabled()));
   Invalidate();
}

void CMyComboBoxHistory::SetReadOnlyEx(BOOL bReadOnly)
{
   m_bEditable = !bReadOnly;

   // The first child is the CEdit
   CEdit* pComboEdit=(CEdit*)GetWindow(GW_CHILD);

   // Setting the edit ctrl always to enable
   pComboEdit->EnableWindow(TRUE);
   pComboEdit->SetReadOnly(!m_bEditable);
   Invalidate();
}

void CMyComboBoxHistory::SetEnabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGEnabled = crFG;
   m_crBGEnabled = crBG;
   delete m_pbrushEnabled;
   m_pbrushEnabled = new CBrush;
   m_pbrushEnabled->CreateSolidBrush(m_crBGEnabled);
}

void CMyComboBoxHistory::SetDisabledColor(COLORREF crFG, COLORREF crBG)
{
   // Setting the colors and the brush
   m_crFGDisabled = crFG;
   m_crBGDisabled = crBG;
   delete m_pbrushDisabled;
   m_pbrushDisabled = new CBrush;
   m_pbrushDisabled->CreateSolidBrush(m_crBGDisabled);
}

CString CMyComboBoxHistory::getText(void)
{
	int nIndex = GetCurSel();
	int nLength;
	CString sBuffer;
	if (nIndex != CB_ERR)	
	{
		nLength = GetLBTextLen(nIndex);
		GetLBText(nIndex,sBuffer.GetBuffer(nLength));
		sBuffer.ReleaseBuffer();
		return sBuffer;
	}
	else
	{
		GetWindowText(sBuffer);
		return sBuffer;
	}
		return _T("");
}

void CMyComboBoxHistory::SetLblFont(int size,int weight,LPTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_font.CreateFontIndirect(&lf);

	SetFont(&m_font);
}

// Events
BOOL CMyComboBoxHistory::OnCBoxChange()
{
	m_bIsDirty = TRUE;

	return FALSE;
}

BOOL CMyComboBoxHistory::OnCBoxDropDown()
{
	m_bIsDirty = TRUE;

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
// CMyListBox

BEGIN_MESSAGE_MAP(CMyListBox, CListBox)
	//{{AFX_MSG_MAP(CXHTMLStatic)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMyListBox::CMyListBox()
{
	m_bInitialized = FALSE;
	m_bDrawFirstRow = FALSE;
	m_vecColumns.clear();
	m_vecColumnsColor.clear();
	m_colBackGnd = ::GetSysColor(COLOR_BTNFACE);
	m_colText = RGB(0,0,0);	// Black
	m_nColStart = 0;
}

CMyListBox::~CMyListBox()
{
	if (m_font.GetSafeHandle())
		m_font.DeleteObject();
}

BOOL CMyListBox::OnEraseBkgnd(CDC* pDC)
{
//	if (!m_bInitialized)
//	{
		// Set brush to desired background color
		CBrush backBrush(m_colBackGnd);

		// Save old brush
		CBrush* pOldBrush = pDC->SelectObject(&backBrush);

		CRect rect;
		GetClientRect(&rect);

		pDC->FillRect(rect,&backBrush);
		pDC->SelectObject(pOldBrush);
		m_bInitialized = TRUE;
//	}
	return TRUE;

}

// PUBLIC
void CMyListBox::Init(CWnd *parent,CRect rect,UINT id)
{
	Create(WS_CHILD|WS_VISIBLE|WS_BORDER|WS_VSCROLL|LBS_USETABSTOPS|LBS_OWNERDRAWVARIABLE,
					rect, parent, id);
}

void CMyListBox::setColumn(int column_start,int align,
													 BOOL is_bold,BOOL is_underline,
													 BOOL is_italic,BOOL is_strike_through)
{
	m_vecColumns.push_back(COLUMN(column_start,align,is_bold,is_underline,is_italic,is_strike_through));
}

void CMyListBox::setColumnColor(COLORREF text,COLORREF bkgnd)
{
	m_vecColumnsColor.push_back(_column_color(text,bkgnd));
}

void CMyListBox::setBkColor(COLORREF col)
{
	m_colBackGnd = col;
}

void CMyListBox::setColStart(int col_start)
{
	m_nColStart = col_start;
}

void CMyListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	BOOL bIsColumnColor = FALSE;
	UINT nNumOfColumnColors;

	if (lpDrawItemStruct->CtlType == ODT_LISTBOX) 
	{
		if (lpDrawItemStruct->itemAction & ODA_DRAWENTIRE)
		{
			this->SetRedraw(FALSE);
			int nLen = 	CListBox::GetTextLen(lpDrawItemStruct->itemID);
			if (nLen != LB_ERR)
			{
				GetText(lpDrawItemStruct->itemID,m_sText);
				// Add an extra \t at the end of the string
				if (m_sText.Right(1) != "\t")
				{
					m_sText += "\t";
				}
				CStringArray arrTabText;
				// Check out if there is arguments; 070207 p�d
				//	splitInfo(m_sArguments.GetBuffer(),&nItems,&nLen,';');
				BOOL bIsText = (SplitString(m_sText,_T("\t"),arrTabText) > 0);

				// Make sure theres data and that defined columns are equal to or greater
				// than number of arguments; 071129 p�d
				if (arrTabText.GetCount() > 0 && m_vecColumns.size() > 0 && 
						m_vecColumns.size() >= arrTabText.GetCount()  )
				{
					RECT rect = lpDrawItemStruct->rcItem;
					// Make sure theres enough information in Column colors
					// If theres less than text, use deafult background color; 071129 p�d
					bIsColumnColor = m_vecColumnsColor.size() >= arrTabText.GetCount();
					nNumOfColumnColors = m_vecColumnsColor.size();

					LOGFONT lf;
					CFont* cf = GetFont();
					if (cf)
						cf->GetObject(sizeof(lf), &lf);
					else
						GetObject(GetStockObject(SYSTEM_FONT), sizeof(lf), &lf);

					m_font.DeleteObject();
					VERIFY(m_font.CreateFontIndirect(&lf));
					CDC dc;
					dc.Attach(lpDrawItemStruct->hDC);

					// Get rectangle for row; 071129 p�d
					CRect rectDraw;
					UINT ii = 0;
					int nPrevCol = m_nColStart;
					for (int i = 0;i < arrTabText.GetCount();i++)
					{
						COLUMN col = m_vecColumns[ii];
						if (ii < nNumOfColumnColors)
						{
							COLUMN_COLOR col_col = m_vecColumnsColor[ii];
							dc.SetBkColor(col_col._bkgnd_color);
							dc.SetTextColor(col_col._text_color);
						}
						else
						{
							dc.SetBkColor(m_colBackGnd);
							dc.SetTextColor(m_colText);
						}
						// Add text to listbox; 071129 p�d
						rectDraw = CRect(nPrevCol,rect.top,col._col_width+nPrevCol,rect.bottom);
						lf.lfWeight    = (col._is_bold ? FW_BOLD : FW_NORMAL);
						lf.lfUnderline = (BYTE) col._is_underline;
						lf.lfItalic    = (BYTE) col._is_italic;
						lf.lfStrikeOut = (BYTE) col._is_strike_through;
						m_font.DeleteObject();
						VERIFY(m_font.CreateFontIndirect(&lf));
						// Draw the backgound color for text (same as window background); 071129 p�d
						dc.SelectObject(&m_font);
						dc.DrawText(arrTabText[i],rectDraw,DT_SINGLELINE | DT_VCENTER | col._align);
						nPrevCol += col._col_width;
						ii++;
					}	// for (int i = 0;i < arrTabText.GetCount();i++)
					dc.Detach();
				}	// if (arrTabText.GetCount() > 0 && m_vecColumns.size() > 0 && 
			}	// if (nLen != LB_ERR)
			this->SetRedraw(TRUE);
		}
	}	// if (lpDrawItemStruct->CtlType == ODT_LISTBOX) 
}

void CMyListBox::ResetContent()
{
	if (!::IsWindow(m_hWnd))
	{
		ASSERT(FALSE);
		return;
	}

	CListBox::ResetContent();
}
// PRIVATE
int CMyListBox::SplitString(const CString& input,const CString& delimiter, CStringArray& results)
{
  int iPos = 0;
  int newPos = -1;
  int sizeS2 = delimiter.GetLength();
  int isize = input.GetLength();

  CArray<INT, int> positions;

  newPos = input.Find (delimiter, 0);

  if( newPos < 0 ) { return 0; }

  int numFound = 0;

  while( newPos >= iPos )
  {
    numFound++;
    positions.Add(newPos);
    iPos = newPos;
    newPos = input.Find (delimiter, iPos+sizeS2); //+1);
  }

  for( int i = 0; i < positions.GetSize(); i++ )
  {
    CString s;
    if( i == 0 )
      s = input.Mid( i, positions[i] );
    else
    {
      int offset = positions[i-1] + sizeS2;
      if( offset < isize )
      {
        if( i == positions.GetSize() )
          s = input.Mid(offset);
        else if( i > 0 )
          s = input.Mid( positions[i-1] + sizeS2, 
                 positions[i] - positions[i-1] - sizeS2 );
      }
    }
    if( s.GetLength() >= 0 )
      results.Add(s);
  }
  return numFound;
}

//////////////////////////////////////////////////////////////////////////
// CMyXTButton
CMyXTButton::CMyXTButton()
{
}

// This example implements the DrawItem method for a CButton-derived 
// class that draws the button's text using the color red.
void CMyXTButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
   UINT uStyle = DFCS_BUTTONPUSH;

   // This code only works with buttons.
   ASSERT(lpDrawItemStruct->CtlType == ODT_BUTTON);

   // If drawing selected, add the pushed style to DrawFrameControl.
   if (lpDrawItemStruct->itemState & ODS_SELECTED)
      uStyle |= DFCS_PUSHED;
/*
   // Draw the button frame.
   ::DrawFrameControl(lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, 
      DFC_BUTTON, uStyle);
*/
   // Get the button's text.
   CString strText;
   GetWindowText(strText);

   // Draw the button text using the text color red.
   //COLORREF crOldColor = ::SetTextColor(lpDrawItemStruct->hDC, RGB(255,0,0));
   ::DrawText(lpDrawItemStruct->hDC, strText, strText.GetLength(), 
      &lpDrawItemStruct->rcItem, DT_WORDBREAK |DT_VCENTER); //|DT_CENTER);
   //::SetTextColor(lpDrawItemStruct->hDC, crOldColor);
}

////////////////////////////////////////////////////////////////////////////////////
// CMyHtmlView

BEGIN_MESSAGE_MAP(CMyHtmlView, CXTHtmlView)
	//{{AFX_MSG_MAP(CMyHtmlView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CMyHtmlView::DoDataExchange(CDataExchange* pDX)
{
	CXTHtmlView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

CMyHtmlView::~CMyHtmlView()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMyHtmlView diagnostics

#ifdef _DEBUG
void CMyHtmlView::AssertValid() const
{
	CXTHtmlView::AssertValid();
}

void CMyHtmlView::Dump(CDumpContext& dc) const
{
	CXTHtmlView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyHtmlView message handlers

void CMyHtmlView::OnInitialUpdate()
{
}

void CMyHtmlView::printOutHTM()
{
	ExecWB(OLECMDID_PRINT,OLECMDEXECOPT_PROMPTUSER,NULL,NULL);
}

void CMyHtmlView::setHTMFileName(LPCTSTR html_filename)
{
	m_sHTML_FileName = html_filename;
}

CString CMyHtmlView::getHTM(void)
{
	return m_sHTM;
}

void CMyHtmlView::startHTM(void)
{
	CString sTmp;
	sTmp = _T("<html><body>");
	m_sarrHTMText.Add(sTmp);
}

void CMyHtmlView::endHTM(void)
{
	CString sTmp;
	sTmp = _T("</body></html>");
	m_sarrHTMText.Add(sTmp);
}

void CMyHtmlView::setHTM_text(LPCTSTR txt,HTM_TYPES type,short font_size,LPCTSTR font_name,HTM_COLORS color,bool bold,bool italic,bool uline)
{
	CString sTmp;
	CString sIsBold_start,sIsBold_end;
	CString sIsItalic_start,sIsItalic_end;
	CString sIsULine_start,sIsULine_end;
	CString sColor;
	if (bold)
	{
		sIsBold_start = _T("<b>");
		sIsBold_end		= _T("</b>");
	}
	if (italic)
	{
		sIsItalic_start = _T("<i>");
		sIsItalic_end		=	_T("</i>");
	}
	if (uline)
	{
		sIsULine_start = _T("<u>");
		sIsULine_end	 = _T("</u>");
	}
	// Color
	if (color == HTM_COLOR_BLACK) sColor = _T("#000000");
	else if (color == HTM_COLOR_RED) sColor = _T("#ff0000");
	else if (color == HTM_COLOR_GREEN) sColor = _T("#00ff00");
	else if (color == HTM_COLOR_BLUE) sColor = _T("#0000ff");
	else if (color == HTM_COLOR_YELLOW) sColor = _T("#ffff00");
	else if (color == HTM_COLOR_CYAN) sColor = _T("#00ffff");
	else if (color == HTM_COLOR_MAGENTA) sColor = _T("#ff00ff");
	else if (color == HTM_COLOR_GRAY) sColor = _T("#c0c0c0");
	else if (color == HTM_COLOR_WHITE) sColor = _T("#ffffff");

	switch (type)
	{
		case HTM_FREE_FMT :
			sTmp.Format(_T("<font face=\"%s\" size=\"%d\" color=\"%s\">%s%s%s %s %s%s%s</font>"),
				font_name,font_size,sColor,sIsBold_start,sIsItalic_start,sIsULine_start,txt,sIsBold_end,sIsItalic_end,sIsULine_end);
			m_sarrHTMText.Add(sTmp);
		break;
		case HTM_HEAD1 :
			sTmp.Format(_T("<h1>%s</h1>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_HEAD2 :
			sTmp.Format(_T("<h2>%s</h2>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_HEAD3 :
			sTmp.Format(_T("<h3>%s</h3>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_HEAD4 :
			sTmp.Format(_T("<h4>%s</h4>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_PLAIN :
			sTmp.Format(_T("%s"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_BOLD :
			sTmp.Format(_T("<b>%s</b>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_ULINE :
			sTmp.Format(_T("<u>%s</u>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_ITALIC :
			sTmp.Format(_T("<i>%s</i>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_BOLD_ULINE :
			sTmp.Format(_T("<b><u>%s</u></b>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_BOLD_ULINE_ITALIC :
			sTmp.Format(_T("<b><u><i>%s</i></u></b>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
		case HTM_ULINE_ITALIC :
			sTmp.Format(_T("<u><i>%s</i></u>"),txt);
			m_sarrHTMText.Add(sTmp);
		break;	
	};
}

void CMyHtmlView::setHTML_linefeed(short numof)
{
	for (short i = 0;i < numof;i++)
		m_sarrHTMText.Add(_T("</br>"));
}

void CMyHtmlView::setHTML_line(HTM_HR_LAYOUT hr_layout,short hr_size)
{
	CString sTmp;
	if (hr_layout == HTM_HR_SHADE)
		sTmp.Format(_T("<hr/>"));
	else if (hr_layout == HTM_HR_NOSHADE)
		sTmp.Format(_T("<hr noshade size=%d/>"),hr_size);
	m_sarrHTMText.Add(sTmp);
}

void CMyHtmlView::setHTM_table(void)
{
	CString sTmp;
	sTmp.Format(_T("<table border=\"0\" style='table-layout:auto'>"));
	m_sarrHTMText.Add(sTmp);
}

void CMyHtmlView::setHTM_start_table_column(void)
{
	m_sarrHTMText.Add(_T("<tr>"));
}

void CMyHtmlView::setHTM_table_column(LPCTSTR txt,short width,short font_size,LPCTSTR font_name,HTM_COLORS color,HTM_ALIGN align,bool bold,bool italic,bool uline)
{
	CString sTmp;
	CString sIsBold_start,sIsBold_end;
	CString sIsItalic_start,sIsItalic_end;
	CString sIsULine_start,sIsULine_end;
	CString sColor;
	if (bold)
	{
		sIsBold_start = _T("<b>");
		sIsBold_end		= _T("</b>");
	}
	if (italic)
	{
		sIsItalic_start = _T("<i>");
		sIsItalic_end		=	_T("</i>");
	}
	if (uline)
	{
		sIsULine_start = _T("<u>");
		sIsULine_end	 = _T("</u>");
	}

	// Color
	if (color == HTM_COLOR_BLACK) sColor = _T("#000000");
	else if (color == HTM_COLOR_RED) sColor = _T("#ff0000");
	else if (color == HTM_COLOR_GREEN) sColor = _T("#00ff00");
	else if (color == HTM_COLOR_BLUE) sColor = _T("#0000ff");
	else if (color == HTM_COLOR_YELLOW) sColor = _T("#ffff00");
	else if (color == HTM_COLOR_CYAN) sColor = _T("#00ffff");
	else if (color == HTM_COLOR_MAGENTA) sColor = _T("#ff00ff");
	else if (color == HTM_COLOR_GRAY) sColor = _T("#c0c0c0");
	else if (color == HTM_COLOR_WHITE) sColor = _T("#ffffff");

	switch (align)
	{
		case HTM_ALIGN_LEFT :
			sTmp.Format(_T("<td style=\"width:%d\" align=\"left\"><font face=\"%s\" size=\"%d\" color=\"%s\">%s%s%s %s %s%s%s</font></td>"),
			width,font_name,font_size,sColor,sIsBold_start,sIsItalic_start,sIsULine_start,txt,sIsBold_end,sIsItalic_end,sIsULine_end);
			m_sarrHTMText.Add(sTmp);
		break;
		case HTM_ALIGN_CENTER :
			sTmp.Format(_T("<td style=\"width:%d\" align=\"center\"><font face=\"%s\" size=\"%d\" color=\"%s\">%s%s%s %s %s%s%s</font></td>"),
			width,font_name,font_size,sColor,sIsBold_start,sIsItalic_start,sIsULine_start,txt,sIsBold_end,sIsItalic_end,sIsULine_end);
			m_sarrHTMText.Add(sTmp);
		break;
		case HTM_ALIGN_RIGHT :
			sTmp.Format(_T("<td style=\"width:%d\" align=\"right\"><font face=\"%s\" size=\"%d\" color=\"%s\">%s%s%s %s %s%s%s</font></td>"),
			width,font_name,font_size,sColor,sIsBold_start,sIsItalic_start,sIsULine_start,txt,sIsBold_end,sIsItalic_end,sIsULine_end);
			m_sarrHTMText.Add(sTmp);
		break;
	};

}

void CMyHtmlView::setHTM_table_column(int num,short width,short font_size,LPCTSTR font_name,HTM_COLORS color,HTM_ALIGN align,bool bold,bool italic,bool uline)
{
	CString sNum;

	sNum.Format(_T("%d"),num);
	setHTM_table_column(sNum,width,font_size,font_name,color,align,bold,italic,uline);
}


void CMyHtmlView::setHTM_table_column(double num,short width,short dec,short font_size,LPCTSTR font_name,HTM_COLORS color,HTM_ALIGN align,bool bold,bool italic,bool uline)
{
	CString sNum;

	sNum.Format(_T("%.*f"),dec,num);
	setHTM_table_column(sNum,width,font_size,font_name,color,align,bold,italic,uline);
}


void CMyHtmlView::setHTM_end_table_column(void)
{
	CString sTmp;
	sTmp.Format(_T("</tr>"));
	m_sarrHTMText.Add(sTmp);
}

void CMyHtmlView::endHTM_table(void)
{
	CString sTmp;
	sTmp.Format(_T("</table>"));
	m_sarrHTMText.Add(sTmp);
}

void CMyHtmlView::showHTM(void)
{

	CString sFN;

	m_sHTM.Empty();
	if (m_sarrHTMText.GetCount() > 0)
	{
		for (short i = 0;i < m_sarrHTMText.GetCount();i++)
		{
			m_sHTM += m_sarrHTMText.GetAt(i);
		}
	
		sFN.Format(_T("%s%s"),getTempDir(),m_sHTML_FileName);

		CStdioFileEx	fileEx;

		// Set the code page. Will not make any difference for Unicode-Unicode or ANSI-ANSI using the
		// same code page
//		fileEx.SetCodePage(nCodePage);

		// Open file
		if (fileEx.Open(sFN, CFile::modeWrite | CFile::modeCreate | CFile::typeText))
		{
			// Is file unicode?
	
			fileEx.WriteString(m_sHTM);

			fileEx.Close();
		}

		if (fileExists(sFN))	Navigate(sFN,NULL,NULL);

	}


}


////////////////////////////////////////////////////////////////////////////////////
#define ENTRY_WINDOWPLACEMENT _T("WindowPlacement")

const LPCTSTR DB_SECTION							= _T("DATABASES");
const LPCTSTR DB_NAMES								= _T("DBNAME");
const LPCTSTR DB_LOCATIONS						= _T("DBLOCATION");
const LPCTSTR DB_DSN_NAMES 					= _T("DB_DSN_NAME");
const LPCTSTR DB_CLIENTS		 					= _T("DB_CLIENTS");

const LPCTSTR ADMIN_SECTION					= _T("ADMINISTRATOR");
const LPCTSTR ADMIN_KEY_LOCATION			= _T("Location");
const LPCTSTR ADMIN_KEY_DBSERVER			= _T("DBServer");
const LPCTSTR ADMIN_KEY_USERNAME			= _T("UserName");
const LPCTSTR ADMIN_KEY_PSW					= _T("Password");
// Holds data if it's a connection to ODBC setup (e.g. used in FastReports); 060703 p�d
const LPCTSTR ADMIN_DSN_NAME					= _T("DSN_Name");	

const LPCTSTR USER_SECTION						= _T("USER");
const LPCTSTR USER_KEY_NAME					= _T("UserName");
const LPCTSTR USER_KEY_DB						= _T("Database");


// Modify this rutine, to check if file exists; 051213 p�d
BOOL fileExists(LPCTSTR fn)
{
	BOOL bFound = FALSE;
	CFileFind find;
	bFound = find.FindFile(fn);
	find.Close();
	return bFound;
}

CString getProgDir(void)
{
	CString sPath;
	if(AfxGetApp() != NULL)
	{
		::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
		sPath.ReleaseBuffer();
	}
	else
	{
		::GetModuleFileName(g_hInstanceDLL, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
		sPath.ReleaseBuffer();
		int nIndex = sPath.ReverseFind(_T('\\'));
		sPath.Truncate(nIndex);
	}

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();


	return sPath;
}

CString getTempDir(void)
{
	TCHAR lpTempPathBuffer[MAX_BUFFER+1];
	DWORD dwRetVal = GetTempPath(MAX_BUFFER, lpTempPathBuffer);
	CString csBuf;

	if(dwRetVal > MAX_BUFFER || (dwRetVal == 0))
		csBuf.Empty();
	else
		csBuf = lpTempPathBuffer;

	return csBuf;
}

CString getTempFilePath(void)
{
	CString csPath;
	if(GetTempPath(_MAX_PATH,csPath.GetBuffer(_MAX_PATH+1)) != 0)
	{
		csPath.ReleaseBuffer();
		CString csTempFile;
		if (GetTempFileName(csPath,_T(""),0,csTempFile.GetBuffer(_MAX_PATH+1)) != 0)
		{
			csTempFile.ReleaseBuffer();
			return csTempFile;
		}
	}
	return CString();
}


CString extractFileName(LPCTSTR fn)
{
	BOOL bFileFound = FALSE;
	int nIndex;
	CString sFileNameAndPath;
	CString sFileName;
	CFileFind find;
	// Try to find the file on disk
	if (find.FindFile(fn))
	{
		find.FindNextFile();
		sFileName = find.GetFileName();
		bFileFound = TRUE;
	}
	find.Close();
	// If file wasn't found on disk, extract the file name ...
	if (!bFileFound)
	{
		sFileNameAndPath = fn;

		nIndex = sFileNameAndPath.ReverseFind('\\');

		if (nIndex > -1)
		{
			sFileName = sFileNameAndPath.Right(sFileNameAndPath.GetLength() - nIndex - 1);
		}

	}

	return sFileName;
}

CString extractFilePath(LPCTSTR fn)
{
	CString sFileName;
	CString sFilePathAndName;
	CFileFind find;
	if (find.FindFile(fn))
	{
		find.FindNextFile();
		sFileName = find.GetFileName();
		sFilePathAndName = find.GetFilePath();
	}

	find.Close();
	return sFilePathAndName.Left(_tcslen(sFilePathAndName) - _tcslen(sFileName));
}

CString extractFileNameNoExt(LPCTSTR fn)
{
	CString sFileNameNoExt;
	CFileFind find;
	if (find.FindFile(fn))
	{
		find.FindNextFile();
		sFileNameNoExt = find.GetFileTitle();
	}

	find.Close();
	return sFileNameNoExt;
}

CString getYearMonth(void)
{
	CString sYearMonth;
	CTime t = CTime::GetCurrentTime();
	sYearMonth.Format(_T("%04d%02d"),
		t.GetYear(),
		t.GetMonth());

	return sYearMonth;
}

CString getDate(void)
{
	CString sDate;
	CTime t = CTime::GetCurrentTime();
	sDate.Format(_T("%04d%02d%02d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay());

	return sDate;
}

CString getDateEx(void)
{
	CString sDate;
	CTime t = CTime::GetCurrentTime();
	sDate.Format(_T("%04d-%02d-%02d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay());

	return sDate;
}

CString getDateTime(void)
{
	CString sDateTime;
	CTime t = CTime::GetCurrentTime();
	sDateTime.Format(_T("%04d-%02d-%02d %02d:%02d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay(),
		t.GetHour(),
		t.GetMinute());

	return sDateTime;
}
CString getDateTimeEx(void)
{
	CString sDateTime;
	CTime t = CTime::GetCurrentTime();
	sDateTime.Format(_T("%04d%02d%02d%02d%02d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay(),
		t.GetHour(),
		t.GetMinute());

	return sDateTime;
}

CString getDateTimeEx2(void)
{

	CString sDateTime;
	SYSTEMTIME tSys;
	GetLocalTime(&tSys);
	sDateTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d:%d"),
		tSys.wYear,
		tSys.wMonth,
		tSys.wDay,
		tSys.wHour,
		tSys.wMinute,
		tSys.wSecond,
		tSys.wMilliseconds);

	return sDateTime;
}

/*	Not used 060801 p�d
CString getENUDate(void)
{
	char s[100];
  int rc;
  time_t temp;
  struct tm *timeptr;

	temp = time(NULL);
  timeptr = localtime(&temp);

  rc = strftime(s,sizeof(s),"%a, %d %b %Y", timeptr);

	return s;
}
*/
BOOL getTimeSeparated(LPCTSTR dt_str,int *h,int *m,int *s)
{
	TCHAR szDay[10];
	TCHAR szDayNum[10];
	TCHAR szMon[10];
	TCHAR szYear[10];
	TCHAR szTime[10];
	TCHAR szDummy[10];

	_stscanf(dt_str,_T("%s %s %s %s %s %s"),
					szDay,
					szDayNum,
					szMon,
					szYear,
					szTime,
					szDummy);

	if (_tcslen(szTime) == 8)
	{
		// Get Hour portin of Time string; 060801 p�d
		memset(szDummy,0,10);
		szDummy[0] = szTime[0];
		szDummy[1] = szTime[1];
		*h = _tstoi(szDummy);
		// Get Minute portin of Time string; 060801 p�d
		memset(szDummy,0,10);
		szDummy[0] = szTime[3];
		szDummy[1] = szTime[4];
		*m = _tstoi(szDummy);
		// Get Seconds portin of Time string; 060801 p�d
		memset(szDummy,0,10);
		szDummy[0] = szTime[6];
		szDummy[1] = szTime[7];
		*s = _tstoi(szDummy);

		return TRUE;
	}
	return FALSE;
}

// Compares two times in the strings (Format e.g. "Tue, 01 Aug 2006 00:00:00 +0100")
BOOL compTimes(LPCTSTR dt_str0,LPCTSTR dt_str1)
{
	int h0,h1,m0,m1,s0,s1;
	// Get time from strings
	getTimeSeparated(dt_str0,&h0,&m0,&s0);
	getTimeSeparated(dt_str1,&h1,&m1,&s1);
	// Compare times, to see if time in h1,m1,s1 differs
	// from h0,m0,s0; 060802 p�d
	if (h1 > h0 ||	// Hours
			m1 > m0 ||	// Minutes
			s1 > s0)		// Seconds
	{
		return FALSE;	// Time differs
	}
	return TRUE;
}


CString getDBDateTime(void)
{
	CString sDateTime;
	CTime t = CTime::GetCurrentTime();
	sDateTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay(),
		t.GetHour(),
		t.GetMinute(),
		t.GetSecond());

	return sDateTime;
}

void regSetInt(LPCTSTR root,LPCTSTR key, LPCTSTR item,DWORD value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);

  if (RegCreateKeyEx(HKEY_CURRENT_USER,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_WRITE|KEY_SET_VALUE,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		RegSetValueEx(hk, 
								item, 
								NULL, 
								REG_DWORD, 
								(LPBYTE)&value, 
								sizeof(DWORD));
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}

DWORD regGetInt(LPCTSTR root,LPCTSTR key, LPCTSTR item,int def_value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];
	DWORD dwKeySize = 255;
	DWORD nValue = def_value;
	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it

	_stprintf(szRoot,_T("%s\\%s"),root,key);

	if (RegCreateKeyEx(HKEY_CURRENT_USER,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_READ,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		RegQueryValueEx(hk, item, 0,NULL, 
				(LPBYTE)&nValue, 
				&dwKeySize); 	
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return nValue;
}

// Write to Registry Local machine; 081202 p�d
void regSetInt_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,DWORD value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);

  if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_WRITE|KEY_SET_VALUE,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		RegSetValueEx(hk, 
								item, 
								NULL, 
								REG_DWORD, 
								(LPBYTE)&value, 
								sizeof(DWORD));
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}


// Get from Registry Local machine; 081202 p�d
DWORD regGetInt_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,int def_value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[127];
	DWORD dwKeySize = 255;
	DWORD nValue = def_value;
	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_READ,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		RegQueryValueEx(hk, item, 0,NULL, 
				(LPBYTE)&nValue, 
				&dwKeySize); 	
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return nValue;
}

//	const LPCTSTR REG_ROOT								= _T("SOFTWARE\\HaglofManagmentSystem\\");
//	const LPCTSTR REG_KEY_USERNAME				= _("USER_DATA");
//	const LPCTSTR REG_STR_USERPRIV				= _("UserPriviliges");
//	const LPCTSTR REG_STR_SELDB					= _("SelectedDataBase");

void regSetStr(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[255];
	TCHAR szValue[1024];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);

  if (RegCreateKeyEx(HKEY_CURRENT_USER,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_WRITE|KEY_SET_VALUE,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{
	_stprintf(szValue,_T("%s"),value);

	RegSetValueEx(hk,
		item,
		0,
		REG_SZ,
		(LPBYTE)&szValue,
		(DWORD) (lstrlen(szValue)+1)*sizeof(TCHAR));
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}

void regSetStr_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR value)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);

  if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_WRITE|KEY_SET_VALUE,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{
	_stprintf(szValue,_T("%s"),value);

	RegSetValueEx(hk,
		item,
		0,
		REG_SZ,
		(LPBYTE)&szValue,
		(DWORD) (lstrlen(szValue)+1)*sizeof(TCHAR));
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);
}

CString regGetStr(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR def_str)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 255;
	_tcscpy(szValue,(def_str));
	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);
  if (RegCreateKeyEx(HKEY_CURRENT_USER,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_READ,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		// Retrieve the value of the Left key
		RegQueryValueEx(hk,
									item,
									0,
									NULL,//&dwType,
									(PBYTE)&szValue,
									&dwKeySize);
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return szValue;
}

CString regGetStr_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR def_str)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 255;
	_tcscpy(szValue,(def_str));
	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),root,key);
  if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_READ,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		// Retrieve the value of the Left key
		RegQueryValueEx(hk,
									item,
									0,
									NULL,//&dwType,
									(PBYTE)&szValue,
									&dwKeySize);
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return szValue;
}


CString regGetSQLServerInstance(void)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	TCHAR szValue[512];
	DWORD dwKeySize = 255;
	_tcscpy(szValue,_T(""));
	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),_T("Software"),_T("Microsoft\\Microsoft SQL Server"));

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
								szRoot,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_READ,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		// Retrieve the value of the Left key
		RegQueryValueEx(hk,
									_T("InstalledInstances"),
									0,
									NULL,//&dwType,
									(PBYTE)&szValue,
									&dwKeySize);
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return szValue;
}

BOOL isWow64()
{
	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
	LPFN_ISWOW64PROCESS fnIsWow64Process;
    BOOL bIsWow64 = FALSE;

    //IsWow64Process is not available on all supported versions of Windows.
    //Use GetModuleHandle to get a handle to the DLL that contains the function
    //and GetProcAddress to get a pointer to the function if available.
    fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle(TEXT("kernel32")),"IsWow64Process");

    if(NULL != fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(),&bIsWow64))
        {
            //handle error
        }
    }
    return bIsWow64;
}

void regGetSQLServerInstances(CStringArray &arr)
{
	CRegKey Key;
	DWORD lSize = MAX_PATH;
	TCHAR strBiosVersion[260];
	arr.RemoveAll();
	if(Key.Open(HKEY_LOCAL_MACHINE, _T("Software\\Microsoft\\Microsoft SQL Server"), KEY_READ | KEY_WOW64_32KEY) == ERROR_SUCCESS)
	{
		if (Key.QueryMultiStringValue(_T("InstalledInstances"), strBiosVersion,&lSize ) == ERROR_SUCCESS)
		{
			LPCTSTR pStr = strBiosVersion;
			while (*pStr != '\0') 
			{
				arr.Add(pStr);
				pStr = pStr + lstrlen(pStr); // pStr now points to null at end of string
				pStr++; // pStr now points to the next string, or the second null to terminate
			}
		}
		Key.Close();
	}

	lSize = MAX_PATH;

	// If we are running under 64-bit Windows (Wow64) we also need to enumerate 64-bit instances; 111122 Peter, redmine #2548
	if( isWow64() )
	{
		if(Key.Open(HKEY_LOCAL_MACHINE, _T("Software\\Microsoft\\Microsoft SQL Server"), KEY_READ | KEY_WOW64_64KEY) == ERROR_SUCCESS)
		{
			if (Key.QueryMultiStringValue(_T("InstalledInstances"), strBiosVersion,&lSize ) == ERROR_SUCCESS)
			{
				LPCTSTR pStr = strBiosVersion;
				while (*pStr != '\0') 
				{
					arr.Add(pStr);
					pStr = pStr + lstrlen(pStr); // pStr now points to null at end of string
					pStr++; // pStr now points to the next string, or the second null to terminate
				}
			}
			Key.Close();
		}
	}
}

BOOL regGetBin(LPCTSTR root,LPCTSTR key, LPCTSTR item,
	BYTE** ppData, UINT* pBytes)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	DWORD dwKeySize = 510;

	*ppData = NULL;
	*pBytes = 0;

	_stprintf(szRoot,_T("%s\\%s"), root, key);
	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		DWORD dwType, dwCount;
		LONG lResult = RegQueryValueEx(hk, item, NULL, &dwType,
			NULL, &dwCount);
		*pBytes = dwCount;

		if (lResult == ERROR_SUCCESS)
		{
			ASSERT(dwType == REG_BINARY);
			*ppData = new BYTE[*pBytes];
			lResult = RegQueryValueEx(hk, item, NULL, &dwType,
				*ppData, &dwCount);
		}
		RegCloseKey(hk);

		if (lResult == ERROR_SUCCESS)
		{
			ASSERT(dwType == REG_BINARY);
			return TRUE;
		}
		else
		{
			delete [] *ppData;
			*ppData = NULL;
		}
	}
	return FALSE;
}


BOOL regSetBin(LPCTSTR root,LPCTSTR key, LPCTSTR item,
	LPBYTE pData, UINT nBytes)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szRoot[512];
	DWORD dwKeySize = 510;

	_stprintf(szRoot,_T("%s\\%s"), root, key);

	if (RegCreateKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_WRITE|KEY_SET_VALUE,
		NULL,
		&hk,
		&dwDisp) == ERROR_SUCCESS)
	{
		LONG lResult;

		lResult = RegSetValueEx(hk, item, NULL, REG_BINARY, pData, nBytes);

		RegCloseKey(hk);
		return lResult == ERROR_SUCCESS;
	}
	else
		return FALSE;
}




CString createUserID(void)
{
	CString sUserID;
	CTime t = CTime::GetCurrentTime();
	sUserID.Format(_T("%d%d%d%d%d%d"),
		t.GetYear(),
		t.GetMonth(),
		t.GetDay(),
		t.GetHour(),
		t.GetMinute(),
		t.GetSecond());

	return sUserID;
}

void splitInfo(const TCHAR *str,int *items,int *len,TCHAR sep)
{
  TCHAR *p = NULL,strBuff[MAX_BUFFER],strSep[5];
  int nNumOfItems = 0;
	_tcscpy(strBuff,str);
	strSep[0] = sep;
	strSep[1] = '\0';
	// Do a check that the last chanractes equals a "sep" charcter
	if (strBuff[_tcslen(strBuff)-1] != sep)
	{
		_tcscat(strBuff,strSep);
	}
	TCHAR *buff = strBuff;
  for (p = buff; p < buff + _tcslen(buff);p++)
      if (*p == sep) nNumOfItems++;
  *items = nNumOfItems;
  *len = (int)_tcslen(buff);
}

TCHAR* split(const TCHAR *str,int colNum,TCHAR sep)
{
	TCHAR *p = NULL,strBuff[MAX_BUFFER],strSep[5],strTemp[MAX_BUFFER];
	int poscnt = 1,cnt = 0,start = 0,end = 0,len = 0;
	_tcscpy_s(strTemp,MAX_BUFFER,str);
	strSep[0] = sep;
	strSep[1] = '\0';
	// Do a check that the last character equals a "sep" charcter
	if (strTemp[_tcslen(strTemp)-1] != sep)
	{
		_tcscat_s(strTemp,MAX_BUFFER,strSep);
	}
	TCHAR *buff = strTemp;
	len = (int)_tcslen(buff);
   // Check that the input string < MAX_BUFFER
   strBuff[0] = '\0';
   if (len < MAX_BUFFER - 1)
   {
      for (p = (TCHAR *)buff; p < buff + _tcslen(buff);p++)
      {
        if (*p == sep || *p == EOL)
        {
           end = poscnt;
           if (cnt == colNum) break;
           start = end;
           cnt++;
        }
        poscnt++;
      }
      cnt = 0;
      for (p = (TCHAR *)buff + start; p < buff + end;p++) 	
			{
        // -------------------------------------------------
        // Truncate to len, if the input string is longer
        // then the max len specified and break.
        // -------------------------------------------------
        if (cnt >= len && len != -1) break;

        if (*p != sep && *p != EOL)
        {
           strBuff[cnt++] = *p;
        }
      }
      strBuff[cnt] = '\0';
   }
   p = strBuff;
   return p;
}

// Functions used to keep track of window size and placement, set by user; 060125 p�d
// Key's and items in registry
const LPCTSTR REG_WINDOW_POS					= _T("WINDOW_POS");
const LPCTSTR REG_WINDOW_CMDSHOW			= _T("CMDSHOW");
const LPCTSTR REG_WINDOW_LEFT				= _T("LEFT");
const LPCTSTR REG_WINDOW_TOP					= _T("TOP");
const LPCTSTR REG_WINDOW_WIDTH				= _T("WIDTH");
const LPCTSTR REG_WINDOW_HEIGHT			= _T("HEIGHT");

void getWindowPlacement(LPCTSTR root,					
												WINDOWPLACEMENT *wp,
												RECT def_placement
												)
{
	int nLeft,nTop,nWidth,nHeight,nCmdShow;

	wp->length = sizeof(WINDOWPLACEMENT);
	nCmdShow	= regGetInt((root),(REG_WINDOW_POS),(REG_WINDOW_CMDSHOW));
	nLeft			= regGetInt((root),(REG_WINDOW_POS),(REG_WINDOW_LEFT));
	nTop			= regGetInt((root),(REG_WINDOW_POS),(REG_WINDOW_TOP));
	nWidth		= regGetInt((root),(REG_WINDOW_POS),(REG_WINDOW_WIDTH));
	nHeight		= regGetInt((root),(REG_WINDOW_POS),(REG_WINDOW_HEIGHT));

	// Make sure there's values in registry; 060124 p�d
	if (nLeft == 0) nLeft			= def_placement.left;
	if (nTop == 0) nTop				= def_placement.top;
	if (nWidth == 0) nWidth		= def_placement.right;
	if (nHeight == 0) nHeight = def_placement.bottom;


	if (nCmdShow < 3)
	{
		wp->showCmd									= nCmdShow;
//		wp->showCmd	= SW_SHOWNORMAL;

		wp->rcNormalPosition.left		= nLeft;
		wp->rcNormalPosition.top		= nTop;
		wp->rcNormalPosition.right	= nWidth;
		wp->rcNormalPosition.bottom	= nHeight;
	}
}

void setWindowPlacement(LPCTSTR root,				// Root in registry
												CWnd *wnd						// Window to save settings
												)
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(WINDOWPLACEMENT);
	wnd->GetWindowPlacement(&wp);

	if (wp.showCmd < 3)
	{
		regSetInt((root),(REG_WINDOW_POS),(REG_WINDOW_CMDSHOW),(DWORD)wp.showCmd);
		regSetInt((root),(REG_WINDOW_POS),(REG_WINDOW_LEFT),(DWORD)wp.rcNormalPosition.left);
		regSetInt((root),(REG_WINDOW_POS),(REG_WINDOW_TOP),(DWORD)wp.rcNormalPosition.top);
		regSetInt((root),(REG_WINDOW_POS),(REG_WINDOW_WIDTH),(DWORD)wp.rcNormalPosition.right);
		regSetInt((root),(REG_WINDOW_POS),(REG_WINDOW_HEIGHT),(DWORD)wp.rcNormalPosition.bottom);
	}
}

void LoadPlacement(CWnd *pwnd, LPCTSTR pszSection)
{
	UINT nBytes = 0;
	BYTE* pBytes = 0;

	regGetBin(pszSection, _T(""), (REG_WINDOW_POS), &pBytes, &nBytes);
//	AfxGetApp()->GetProfileBinary(pszSection, ENTRY_WINDOWPLACEMENT, &pBytes, &nBytes);
	if (nBytes == sizeof(WINDOWPLACEMENT))
	{
		pwnd->SetWindowPlacement((WINDOWPLACEMENT*) pBytes);
	}
	if (pBytes && nBytes) delete[] pBytes;

}

void SavePlacement(CWnd *pwnd,LPCTSTR pszSection)
{
	WINDOWPLACEMENT wp;
	pwnd->GetWindowPlacement(&wp);

	// check if the mdi child window is maximized.
	// GetWindowPlacement() does not always tell us this.
	/*BOOL bMaximized;
	((CMDIFrameWnd*)pwnd->GetParentFrame())->MDIGetActive(&bMaximized);
	if(bMaximized == TRUE)
	{
		wp.flags |= WPF_RESTORETOMAXIMIZED;
		wp.showCmd = SW_MAXIMIZE
	}*/

//	AfxGetApp()->WriteProfileBinary(pszSection, ENTRY_WINDOWPLACEMENT, (BYTE*) &wp, sizeof(wp));
	regSetBin(pszSection, _T(""), (REG_WINDOW_POS), (BYTE*)&wp, sizeof(wp));
}

//////////////////////////////////////////////////////////////////////////////
// Network functions; 060214 p�d

BOOL isAdmin(void)
{
	CString str;
	CNetUserInfo info;
	BOOL bMore;
  TCHAR szBuff[255];
  DWORD cchBuff = 255;
  GetUserName(szBuff,&cchBuff);
	CNetDomain pDomain(_T(""));
	CNetUsers pUsers(pDomain);
	CString sPDC;
pDomain.GetPDC(sPDC);
//	AfxMessageBox(sPDC);

	pUsers.FindFirstUser(str, bMore);
	while(bMore)
	{
		pUsers.GetUserInfo(str,&info);
		// Check if User has Administrator priviliges; 060214 p�d
		if (_tcscmp(szBuff,info.name) == 0 && info.priv == 2)
			return TRUE;

		pUsers.FindNextUser(str, bMore);
	}
	return FALSE;
}

BOOL getNetworkUserList(vecNETWORK_USERS &vec)
{
	CString str;
	CNetUserInfo info;
	BOOL bMore;
  /*TCHAR szBuff[255];
  DWORD cchBuff = 255;
  GetUserName(szBuff,&cchBuff);*/
	CNetDomain pDomain(_T(""));
	CNetUsers pUsers(pDomain);
	CNetLocalGroups pLocalGroups(pDomain);
	CNetGroups pGroups(pDomain);

	vec.clear();

	pUsers.FindFirstUser(str, bMore);
	while(bMore)
	{
		pUsers.GetUserInfo(str,&info);
		vec.push_back(_network_users(info.name,
																 info.full_name,
																 info.comment,
																 1,
																 info.priv));
		pUsers.FindNextUser(str, bMore);
	}
	return TRUE;
}

BOOL getNetworkServersList(CStringArray &arr)
{
	CString str,S;
	CNetUserInfo info;
	BOOL bMore;
  /*char szBuff[255];
  DWORD cchBuff = 255;
  GetUserName(szBuff,&cchBuff);*/
	CNetDomain pDomain(_T(""));
	CNetUsers pUsers(pDomain);
	CNetLocalGroups pLocalGroups(pDomain);
	CNetGroups pGroups(pDomain);

	

	pUsers.FindFirstUser(str, bMore);
	while(bMore)
	{
		pUsers.GetUserInfo(str,&info);
		S.Format(_T("getNetworkServersList\nstr %s\ninfo.full_name %s"),
			str,info.full_name);
//		AfxMessageBox(S);
/*
		vec.push_back(_network_users(info.name,
																 info.full_name,
																 info.comment,
																 1,
																 info.priv));
*/
		pUsers.FindNextUser(str, bMore);
	}
	return TRUE;
}

CString getUserName(void)
{
	TCHAR szUserName[100];
  DWORD nUserName = sizeof(szUserName);
  if (GetUserName(szUserName, &nUserName)) 
	{
		return (szUserName);
	}
	return _T("");
}

CString getNameOfComputer(void)
{
	TCHAR chrComputerName[MAX_COMPUTERNAME_LENGTH + 1];  
	CString strRetVal;  
	DWORD dwBufferSize = MAX_COMPUTERNAME_LENGTH + 1;  
   
	if(GetComputerName(chrComputerName,&dwBufferSize)) 
	{  
		// We got the name, set the return value.  
		strRetVal = chrComputerName;  
	} 
	else 
	{  
		// Failed to get the name, call GetLastError here to get  
		// the error code.  
		strRetVal = "";  
	}  
  return(strRetVal);  
}

BOOL isIPAddress(CString ip_address)
{
	//-----------------------------------------
	// Declare and initialize variables
	WSADATA wsaData;
	int iResult;
	DWORD dwError;
	struct hostent *remoteHost;
	CString host_name;
	struct in_addr addr;
	char szHostName[128];

	host_name = (ip_address);
	// Validate the parameters

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) 
	{
			// "WSAStartup failed: %d\n", iResult);
			return FALSE;
	}


	// If the user input is an alpha name for the host, use gethostbyname()
	// If not, get host by addr (assume IPv4)
	sprintf(szHostName,"%S",host_name);
	if (isalpha(host_name[0]))  // host address is a name 
	{       
		remoteHost = gethostbyname(szHostName);
	} 
	else 
	{
		addr.s_addr = inet_addr(szHostName);

		if (addr.s_addr == INADDR_NONE) 
		{
				return FALSE;
		} 
		else
			remoteHost = gethostbyaddr((char *)&addr, 4, AF_INET);
	}

	if (remoteHost == NULL) 
	{
			dwError = WSAGetLastError();
			if (dwError != 0) {
				if (dwError == WSAHOST_NOT_FOUND) 
				{
					// Host not found
					return FALSE;
				} 
				else if (dwError == WSANO_DATA) 
				{
					// No data record found
					return FALSE;
				} 
				else 
				{
					// Function failed with error
					return FALSE;
				}
			}
	} 
	else 
	{
		// Address length: %d\n", remoteHost->h_length);
		// First IP Address: %s\n", inet_ntoa(addr));
		addr.s_addr = *(u_long *) remoteHost->h_addr_list[0];
	}

	return (ip_address == inet_ntoa(addr));
}

BOOL isThisANetworkServer(CString name)
{
	struct hostent *remoteHost;
	char szName[128];

	sprintf(szName,"%S",name);
	remoteHost = gethostbyname(szName);

	if (remoteHost != NULL)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

BOOL getLocalIP(CStringArray& arr)
{
  WSADATA  wsaData;
  char     szHostname[100];
  HOSTENT *pHostEnt;
  int      nAdapter = 0;
  struct   sockaddr_in sAddr;
	CString sIP;

	arr.RemoveAll();

  if (WSAStartup(0x0101, &wsaData))
  {
     printf("WSAStartup failed %s\n", WSAGetLastError());
     return FALSE;
  }

  gethostname( szHostname, sizeof( szHostname ));
  pHostEnt = gethostbyname( szHostname );

  while ( pHostEnt->h_addr_list[nAdapter] )
  {
   // pHostEnt->h_addr_list[nAdapter] is the current address in host
   // order.

   // Copy the address information from the pHostEnt to a sockaddr_in
   // structure.
     memcpy ( &sAddr.sin_addr.s_addr, pHostEnt->h_addr_list[nAdapter],
              pHostEnt->h_length);
		 sIP.Format(_T("%S"),inet_ntoa(sAddr.sin_addr));
		 arr.Add(sIP);
		 nAdapter++;
  }
  // Output the machines IP Address.
	WSACleanup();
  return TRUE;

}


/////////////////////////////////////////////////////////////////////////////////////////////

#define MY_ENCRYPT CALG_RC4
//#define MY_ENCRYPT CALG_DES

BOOL SetupCryptoClient()
{
	// Ensure that the default cryptographic client is set up.	
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;		
	// Attempt to acquire a handle to the default key container.
	if (!CryptAcquireContext(&hProv, NULL, MS_DEF_PROV, PROV_RSA_FULL, 0))	
	{
		// Some sort of error occured, create default key container.
		if (!CryptAcquireContext(&hProv, NULL, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_NEWKEYSET))
		{	
			// Error creating key container!			
			return FALSE;		
		}	
	}
	// Attempt to get handle to signature key.
	if (!CryptGetUserKey(hProv, AT_SIGNATURE, &hKey))	
	{
		if (GetLastError() == NTE_NO_KEY)		
		{			
			// Create signature key pair.
			if (!CryptGenKey(hProv, AT_SIGNATURE, 0, &hKey))			
			{
				// Error during CryptGenKey!				
				CryptReleaseContext(hProv, 0);
				return FALSE;			
			}			
			else			
			{				
				CryptDestroyKey(hKey);			
			}		
		}		
		else 		
		{
			// Error during CryptGetUserKey!			
			CryptReleaseContext(hProv, 0);
			return FALSE;		
		}	
	}	
	
	// Attempt to get handle to exchange key.
	if (!CryptGetUserKey(hProv,AT_KEYEXCHANGE,&hKey))	
	{
		if (GetLastError()==NTE_NO_KEY)		
		{			
			// Create key exchange key pair.
			if (!CryptGenKey(hProv,AT_KEYEXCHANGE,0,&hKey))			
			{
				// Error during CryptGenKey!				
				CryptReleaseContext(hProv, 0);
				return FALSE;			
			}			
			else			
			{				
				CryptDestroyKey(hKey);			
			}		
		}		
		else		
		{
			// Error during CryptGetUserKey!			
			CryptReleaseContext(hProv, 0);
			return FALSE;		
		}	
	}	

	CryptReleaseContext(hProv, 0);	
	return TRUE;
}


BOOL EncryptString(TCHAR* szPassword,TCHAR* szEncryptPwd,TCHAR *szKey)
{	
	BOOL bResult = TRUE;	
	HKEY hRegKey = NULL;	
	HCRYPTPROV hProv = NULL;	
	HCRYPTKEY hKey = NULL;
	HCRYPTKEY hXchgKey = NULL;	
	HCRYPTHASH hHash = NULL;	
	DWORD dwLength;
	// Get handle to user default provider.
	if (CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, 0))	
	{
		// Create hash object.		
		if (CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))		
		{
			// Hash password string.			
			dwLength = (DWORD)sizeof(TCHAR)*_tcslen(szKey);
			if (CryptHashData(hHash, (BYTE *)szKey, dwLength, 0))			
			{
				// Create block cipher session key based on hash of the password.
				if (CryptDeriveKey(hProv, MY_ENCRYPT, hHash, CRYPT_EXPORTABLE, &hKey))				
				{
					// Determine number of bytes to encrypt at a time.
					dwLength = (DWORD)sizeof(TCHAR)*_tcslen(szPassword);					
					// Allocate memory.
					BYTE *pbBuffer = (BYTE *)malloc(dwLength);					
					if (pbBuffer != NULL)					
					{
						memcpy(pbBuffer, szPassword, dwLength);						
						// Encrypt data
						if (CryptEncrypt(hKey, 0, TRUE, 0, pbBuffer, &dwLength, dwLength)) 						
						{
							// return encrypted string
							memcpy(szEncryptPwd, pbBuffer, dwLength);

						}	
						else						
						{							
							bResult = FALSE;						
						}						
						// Free memory
						free(pbBuffer);					
					}
					else					
					{						
						bResult = FALSE;					
					}
					CryptDestroyKey(hKey);  // Release provider handle.				
				}				
				else				
				{
					// Error during CryptDeriveKey!					
					bResult = FALSE;				
				}			
			}			
			else			
			{
				// Error during CryptHashData!				
				bResult = FALSE;			
			}
			CryptDestroyHash(hHash); 
			// Destroy session key.		
		}		
		else		
		{
			// Error during CryptCreateHash!			
			bResult = FALSE;		
		}
		CryptReleaseContext(hProv, 0);	
	}	
	return bResult;
}


BOOL DecryptString(TCHAR* szEncryptPwd,TCHAR* szPassword,TCHAR *szKey) 
{	
	BOOL bResult = TRUE;	
	HCRYPTPROV hProv = NULL;		
	HCRYPTKEY hKey = NULL;		
	HCRYPTKEY hXchgKey = NULL;
	HCRYPTHASH hHash = NULL;
	TCHAR szPasswordTemp[32] = _T("");
	DWORD dwLength;
	// Get handle to user default provider.
	if (CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, 0))		
	{
		// Create hash object.			
		if (CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
		{				
			// Hash password string.
			dwLength = (DWORD)sizeof(TCHAR)*_tcslen(szKey);
			if (CryptHashData(hHash, (BYTE *)szKey, dwLength, 0))				
			{
				// Create block cipher session key based on hash of the password.
				if (CryptDeriveKey(
					hProv, MY_ENCRYPT, hHash, CRYPT_EXPORTABLE, &hKey))					
				{
					// we know the encrypted password and the length
					dwLength = (DWORD)sizeof(TCHAR)*_tcslen(szEncryptPwd);						
					// copy encrypted password to temporary TCHAR
					_tcscpy(szPasswordTemp,szEncryptPwd);
					if (!CryptDecrypt(
							hKey, 0, TRUE, 0, (BYTE *)szPasswordTemp, &dwLength))
						bResult = FALSE;						
					CryptDestroyKey(hKey);  // Release provider handle.					
					// copy decrypted password to outparameter
					_tcscpy(szPassword,szPasswordTemp);
				}					
				else					
				{
					// Error during CryptDeriveKey!						
					bResult = FALSE;					
				}				
			}				
			else
			{					
				// Error during CryptHashData!					
				bResult = FALSE;				
			}
			CryptDestroyHash(hHash); // Destroy session key.			
		}			
		else			
		{
			// Error during CryptCreateHash!				
			bResult = FALSE;			
		}
		CryptReleaseContext(hProv, 0);		
	}		
	return bResult;
}

BOOL WriteAdminIniData(LPCTSTR db_serv,LPCTSTR location,LPCTSTR user,LPCTSTR psw,LPCTSTR dsn_name,LPCTSTR client,int authentication)
{
	TCHAR szDBServer[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szAutentication[32];

	_tcscpy(szDBServer,db_serv);
	_tcscpy(szDBUser,user);
	_tcscpy(szDBPsw,psw);
	_tcscpy(szDSNName,dsn_name);

	regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBLOCATION,location);
	regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBSERVER,db_serv);
	regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_USERNAME,user);
	regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_PSW,psw);
	_stprintf(szAutentication,_T("%d"),authentication);
	regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_DB_AUTHENTICATION_ITEM,szAutentication);

	return TRUE;
}

BOOL GetAdminIniData(TCHAR* db_serv,TCHAR* location,TCHAR* user,TCHAR* psw,TCHAR* dsn_name,TCHAR* client,int *idx)
{
	CString sDBServer;
	CString sDBLocation;
	CString sDBUser;
	CString sDBPsw;
	CString sDSNName;
	CString sDBClient;
	CString sAuthenticaction;

	sDBLocation = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBLOCATION);
	sDBServer = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBSERVER);
	sDBUser = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_USERNAME);
	sDBPsw = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_PSW);
	sAuthenticaction = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_DB_AUTHENTICATION_ITEM);

	_tcscpy(db_serv,sDBServer.GetBuffer());
	_tcscpy(location,sDBLocation.GetBuffer());
	_tcscpy(user,sDBUser.GetBuffer());
	_tcscpy(psw,sDBPsw.GetBuffer());
	_tcscpy(dsn_name,sDSNName.GetBuffer());
	_tcscpy(client,sDBClient.GetBuffer());
	*idx = _tstoi(sAuthenticaction);

	return TRUE;
}

BOOL IsAdminIniDataSet(void)
{
	CString sDBLocation;
	sDBLocation = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBLOCATION);

	return sDBLocation != _T("");
}

//	const LPCTSTR REG_STR_SELDB					= _T("DBSelected");

BOOL WriteUserDBToRegistry(LPCTSTR db_name)
{
	regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_SELDB,db_name);
	return TRUE;
}

BOOL GetUserDBInRegistry(LPTSTR db_name)
{
	// Save to registry; 060303 p�d
	_tcscpy(db_name,regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_SELDB));

	return TRUE;
}

BOOL GetAdminIniDataSelectedDB(LPTSTR db_selected)
{
	CString sDBSelected;

	sDBSelected = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_SELDB);

	_tcscpy(db_selected,sDBSelected.GetBuffer());

	return TRUE;
}

BOOL GetAuthentication(int *authentication)
{
	CString sAuthenticaction;
	sAuthenticaction = regGetStr(REG_ROOT,REG_KEY_DATABASE,REG_DB_AUTHENTICATION_ITEM);
	*authentication = _tstoi(sAuthenticaction);

	return (*authentication >= 0);
}

// List holding Database servers supported by HMS; 060217 p�d
void GetSupportedServers(vecADMIN_INI_DATABASES &vec1,vecDBSERVERS &vec2)
{
	_admin_ini_databases data;
	if (vec1.size() > 0)
	{
		vec2.clear();
		for (UINT i = 0;i < vec1.size();i++)
		{
			data = vec1[i];
			if (_tcscmp(data.sDBClient,MySQL) == 0)
			{
				vec2.push_back(_dbservers(ID_MYSQL,(data.sDBServerName),SA_MySQL_Client,data.sLocation,data.sDSNName));
			}
			else if (_tcscmp(data.sDBClient,SQLServer) == 0)
			{
				vec2.push_back(_dbservers(ID_SQLSERVER,(data.sDBServerName),SA_SQLServer_Client,data.sLocation,data.sDSNName));
			}
			else if (_tcscmp(data.sDBClient,OracleServer) == 0)	// Added 2009-10-06 P�D
			{
				vec2.push_back(_dbservers(ID_SQLSERVER,(data.sDBServerName),SA_SQLServer_Client,data.sLocation,data.sDSNName));
			}
		}	// for (UINT i = 0;i < dbData.size();i++)
	}	// if (dbData.size() > 0)

}

void setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}	// if (pWnd)
}


// Check what DBServer client is used, and setup dbname
// according to spec. in SQLApi++; 060310 p�d
CString setDBServerName(SAClient_t &client,LPCTSTR db_name,LPCTSTR db_selected)
{
	CString sDBName;
	switch(client)
	{
		case SA_MySQL_Client:
		case SA_SQLServer_Client:
		{
			if (db_selected == _T(""))
				sDBName.Format(_T("%s@"),db_name);
			else
				sDBName.Format(_T("%s@%s"),db_name,db_selected);
			return sDBName;
		}
	}
	return _T("");
}

BOOL getDBInfo(LPCTSTR schema,LPTSTR db_path,LPTSTR db_user,LPTSTR db_psw,SAClient_t *client,int *autentication)
{
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBServerPath[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBClient[32];
	SAClient_t saClient;
	int nIsWindowsAuthentication;
	BOOL bIsOK = FALSE;
	// Get data from admin.ini file
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	// Set client foe SQLApi
	saClient = SA_SQLServer_Client;

		
	_stprintf(szDBServerPath,_T("%s%s"),setDBServerName(saClient,szDBLocation),schema);

	// Setup return values; 060217 p�d
	*client  = saClient;
	_tcscpy(db_path,szDBServerPath);
	_tcscpy(db_user,szDBUser);
	_tcscpy(db_psw,szDBPsw);
	*autentication = nIsWindowsAuthentication;


	if (nIsWindowsAuthentication == 0)
	{
		bIsOK = (_tcscmp(db_path,_T("")) != 0);
	}
	else if (nIsWindowsAuthentication == 1)
	{
		bIsOK = (_tcscmp(db_path,_T("")) != 0 &&
						 _tcscmp(db_user,_T("")) != 0 &&
						 _tcscmp(db_psw,_T("")) != 0);
	}

	return bIsOK;
}

BOOL getDBLocation(LPTSTR db_location)
{
	CString sAdminPath;
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBClient[32];
	int nIsWindowsAuthentication;
	// Get data from admin.ini file
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	
	// Setup return values; 060317 p�d
	_tcscpy(db_location,szDBLocation);

	return TRUE;
}

BOOL getDBInfo(LPCTSTR schema,LPTSTR db_path,LPTSTR db_user,LPTSTR db_psw,LPTSTR dsn_name,SAClient_t *client)
{
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBServerPath[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBClient[32];
	SAClient_t saClient;
	int nIsWindowsAuthentication;
	// Get data from admin.ini file
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	// Set client foe SQLApi
	saClient = SA_SQLServer_Client;


	_stprintf(szDBServerPath,_T("%s%s"),setDBServerName(saClient,szDBLocation),schema);

	// Setup return values; 060217 p�d
	*client  = saClient;
	_tcscpy(db_path,szDBServerPath);
	_tcscpy(db_user,szDBUser);
	_tcscpy(dsn_name,szDSNName);
	_tcscpy(db_psw,szDBPsw);

	return TRUE;
}

BOOL getDBUserInfo(LPTSTR db_path,LPTSTR user_name,LPTSTR psw,LPTSTR dsn_name,LPTSTR location,LPTSTR db_name,SAClient_t *client)
{
	CString S;
	TCHAR szDBServerPath[128];
	TCHAR szDBLocation[128];
	TCHAR szDB[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	SAClient_t m_client;
	if(getDBInfo(ADMIN_DB_NAME,szDBServerPath,szDBUser,szDBPsw,szDSNName,&m_client) && 
		 getDBLocation(szDBLocation))
	{
		GetUserDBInRegistry(szDB);
		
		S.Format(_T("%s%s"),setDBServerName(m_client,szDBLocation),szDB);

		*client = m_client;
		_tcscpy(db_path,S);
		_tcscpy(user_name,szDBUser);
		_tcscpy(psw,szDBPsw);
		_tcscpy(dsn_name,szDSNName);
		_tcscpy(location,szDBLocation);
		_tcscpy(db_name,szDB);

		return TRUE;
	}
	return FALSE;
}

BOOL getDBUserInfo(LPTSTR db_path,
									 LPTSTR db_name,
									 LPTSTR user_name,
									 LPTSTR psw,
									 SAClient_t *client,
									 int *authentication)
{
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBServerPath[512];
	TCHAR szDBSelected[32];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBClient[32];
	SAClient_t saClient;
	int nIsWindowsAuthentication;
	BOOL bIsOK = FALSE;
	// Get data from admin.ini file
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nIsWindowsAuthentication);
	// Set client foe SQLApi
	GetUserDBInRegistry(szDBSelected);
	saClient = SA_SQLServer_Client;
		
	_stprintf(szDBServerPath,_T("%s%s"),setDBServerName(saClient,szDBLocation),szDBSelected);

	// Setup return values; 060217 p�d
	*client  = saClient;
	_tcscpy(db_path,szDBServerPath);
	_tcscpy(db_name,szDBSelected);
	_tcscpy(user_name,szDBUser);
	_tcscpy(psw,szDBPsw);
	*authentication = nIsWindowsAuthentication;

	if (nIsWindowsAuthentication == 0)
	{
		bIsOK = (_tcscmp(db_path,_T("")) != 0);
	}
	else if (nIsWindowsAuthentication == 1)
	{
		bIsOK = (_tcscmp(db_path,_T("")) != 0 &&
						 _tcscmp(user_name,_T("")) != 0 &&
						 _tcscmp(psw,_T("")) != 0);
	}

	return bIsOK;
}

/*
const LPCTSTR REG_WEB_NEWS_ROOT			= "WEB_NEWS";
const LPCTSTR REG_WEB_NEWS_URL				= "URL";			// Address to web page. E.g. http://hms-haglof.se; 060801
const LPCTSTR REG_WEB_NEWS_FILEPATH	= "FILEPATH";	// Location of file to read on the web server; 060801 p�d
*/
BOOL getHMSHomepageURL(LPTSTR url)
{
	// Read from registry; 060303 p�d
	_tcscpy(url,regGetStr_LM((REG_ROOT),(REG_WEB_NEWS_ROOT),(REG_WEB_NEWS_URL)));

	return (url != _T(""));
}

BOOL getRSSLocation(LPTSTR url,LPTSTR file_path)
{
	// Read from registry; 060303 p�d
	_tcscpy(url,regGetStr_LM((REG_ROOT),(REG_WEB_NEWS_ROOT),(REG_WEB_NEWS_URL)));
	_tcscpy(file_path,regGetStr_LM((REG_ROOT),(REG_WEB_NEWS_ROOT),(REG_WEB_NEWS_FILEPATH)));

	return (url != _T("") && file_path != _T(""));
}


BOOL setRSSFileLastPubDateInReg(LPCTSTR dt_str)
{
	regSetStr((REG_ROOT),(REG_WEB_NEWS_ROOT),(REG_WEB_NEWS_PUBDATE),(dt_str));

	return TRUE;
}

BOOL getRSSFileLastPubDateInReg(LPTSTR dt_str)
{
	// Read from registry; 060801 p�d
	_tcscpy(dt_str,regGetStr((REG_ROOT),(REG_WEB_NEWS_ROOT),(REG_WEB_NEWS_PUBDATE)));
	return (dt_str != _T(""));
}

BOOL getPermanentCategoriesInReg(LPTSTR dt_str)
{
	CString sBuffer;
	// Read from registry; 060801 p�d
	sBuffer = regGetStr_LM((REG_ROOT),(REG_WEB_NEWS_ROOT),(REG_WEB_NEWS_PER_CAT));
	if (sBuffer.Right(1) != _T(";"))
	{
		sBuffer += _T(";");
	}
	_tcscpy(dt_str,sBuffer);
	return (dt_str != _T(""));
}

void setConnectToWEB(DWORD set)
{
	// Write to registry; 081201 p�d
	regSetInt_LM(REG_ROOT,REG_WEB_CONNECT_KEY,REG_WEB_CONNECT,set );
}

DWORD getConnectToWEB(void)
{
	// Read from registry; 081201 p�d
	return regGetInt_LM(REG_ROOT,REG_WEB_CONNECT_KEY,REG_WEB_CONNECT,1 /* Default value = OK */ );
}

CString getNewsCategoriesSetupPathAndFile(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 
	{
		sPath.Format(_T("%s\\%s\\%s"),szPath,SUBDIR_HMS_APPDATA,NEW_CATEGORIES_FILE_NAME);
	}
	return sPath;
}

CString getVersionInfo(HMODULE hLib, LPCTSTR csEntry)
{
	CFileVersionInfo verinfo;
	if (verinfo.Create(hLib))
	{
		if (_tcscmp(csEntry,VER_NUMBER) == 0)
		{
			return verinfo.GetFileVersion();
		}

		if (_tcscmp(csEntry,VER_COMPANY) == 0)
		{
			return verinfo.GetCompanyName();
		}

		if (_tcscmp(csEntry,VER_COPYRIGHT) == 0)
		{
			return verinfo.GetLegalCopyright();
		}
	}

	return _T("");
}

CString getVersionInfo(CString csFilename, LPCTSTR csEntry)
{
	CFileVersionInfo verinfo;
	if (verinfo.Create(csFilename))
	{
		if (_tcscmp(csEntry,VER_NUMBER) == 0)
		{
			return verinfo.GetFileVersion();
		}

		if (_tcscmp(csEntry,VER_COMPANY) == 0)
		{
			return verinfo.GetCompanyName();
		}

		if (_tcscmp(csEntry,VER_COPYRIGHT) == 0)
		{
			return verinfo.GetLegalCopyright();
		}
	}

	return _T("");
}

void saveNewsCatergoriesSetupInfo(vecNewsCategoriesSetup &vec)
{
	FILE *fp;
	CString sFileName;
	CString sCategory;
	int nLength = 0;
	CNewsCategoriesSetupItem rec;
	char szFileName[MAX_PATH];
	sFileName = getNewsCategoriesSetupPathAndFile();
	sprintf(szFileName,"%S",sFileName);
	if ((fp = fopen(szFileName,"wb")) != NULL)
	{
		// Read information from Radiobuttons etc; 060809 p�d
		for (UINT i = 0;i < vec.size();i++)
		{
			rec = vec[i];
			fwrite(&rec,sizeof(CNewsCategoriesSetupItem),1,fp);
		}
		fclose(fp);
	}

}

void getNewsCatergoriesSetupInfo(vecNewsCategoriesSetup &vec)
{
	
	FILE *fp;
	CString sFileName;
	CString sCategory;
	CNewsCategoriesSetupItem rec;
	int lSize;
	vec.clear();
	char szFileName[MAX_PATH];

	sFileName = getNewsCategoriesSetupPathAndFile();
	if (fileExists(sFileName))
	{
		sprintf(szFileName,"%S",sFileName);
		if ((fp = fopen(szFileName,"rb")) != NULL)
		{	
			fseek(fp,0L,SEEK_END);
			lSize = ftell(fp) / sizeof(CNewsCategoriesSetupItem);

			fseek(fp,0L,SEEK_SET);
			for (int l = 0;l < lSize;l++)
			{
				fread(&rec,sizeof(CNewsCategoriesSetupItem),1,fp);
				vec.push_back(rec);
			}	// while(!feof(fp))
			fclose(fp);
		}	// if ((fp = fopen(sFileName,"rb")) != NULL)
	}	// if (fileExists(sFileName))

}

CString getSuiteModuleConfigPathAndFile(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	szPath[0] = '\0';
	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 
	{
		sPath.Format(_T("%s\\%s\\%s"),szPath,SUBDIR_HMS_APPDATA,SUITE_MODULE_FILE_NAME);
	}
	return sPath;
}

// Path to ALL USERS in Document and settings; 100322 p�d
CString getPathToCommonAppData(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	szPath[0] = '\0';
	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_COMMON_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 
	{
		sPath.Format(_T("%s"),szPath);
	}
	return sPath;
}

void trim(TCHAR *str)
{
	TCHAR szBuffer[512];
	UINT nCnt = 0;
	for (TCHAR *p = str;p < _tcslen(str) + str;p++)
	{
		if (*p > TCHAR(32))
		{
			szBuffer[nCnt] = *p;
			nCnt++;
		}
	}
	szBuffer[nCnt] = '\0';
	str = szBuffer;
}


BOOL messageDlg(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg,CWnd *parent)
{
	if( DisplayMsg() )
	{
		BOOL bReturn = FALSE;
		CMessageDlg *dlg = new CMessageDlg(cap,ok_btn,cancel_btn,msg,parent);

		bReturn = (dlg->DoModal() == IDOK);

		delete dlg;

		return bReturn;
	}
	else
	{
		return FALSE;
	}
}

CList<_languages>listLang;
BOOL getLangFiles(CList<_languages> &list)
{
	listLang.RemoveAll();
	// Get languages in system; 051011 p�d
	::EnumSystemLocales(EnumLocalesProc, LCID_SUPPORTED); //LCID_INSTALLED);

	POSITION pos = listLang.GetHeadPosition();
	for (int i=0;i < listLang.GetCount();i++)
	{
		list.AddHead(_languages(listLang.GetNext(pos)));
	}

	return (list.GetCount() > 0);
}

//////////////////////////////////////////////////////////////////////////
// Callback function for EnumSystemLocals; 051011 p�d
BOOL CALLBACK EnumLocalesProc(LPTSTR lpLocaleString)
{
	_languages rec;
	unsigned long uiCurLocale;
	TCHAR szValue[STR_LEN];
	char szLocaleStr[255];

	if (!lpLocaleString)
		    return FALSE;

	// This enumeration returns the LCID as a character string while
	//    we will be using numbers in other NLS calls.
	sprintf(szLocaleStr,"%S",lpLocaleString);
	uiCurLocale = strtoul(szLocaleStr, NULL, 16);

	// Get the language name associated with this locale ID.
	GetLocaleInfo(uiCurLocale, LOCALE_SCOUNTRY, szValue, STR_LEN);
	_tcscpy(rec.szLng,szValue);
	GetLocaleInfo(uiCurLocale, LOCALE_SABBREVLANGNAME, szValue, STR_LEN);
	_tcscpy(rec.szLngAbrev,szValue);

	listLang.AddHead(_languages(rec));

	return TRUE;
}


// These function was created by T. Matheny; 070119 p�d

BOOL IsSubstringInString(LPCTSTR lpszSubString, LPCTSTR lpszString)
{
    int nI;
    int nLenSubStr = _tcslen(lpszSubString);
    int nLenStr = _tcslen(lpszString);
    int nEnd = nLenStr - nLenSubStr;

    for(nI = 0; nI <= nEnd; nI++)
    {
        if(_tcsnccmp(lpszSubString, &lpszString[nI], nLenSubStr) == 0)
        {
            return TRUE;
        }
    }
    
    return FALSE;
}

HWND GetHWndFromTitle(LPCTSTR lpszWndSubTitleStr, HWND hMainWnd, BOOL bIsLeadingTitle, DWORD dwProcessId, DWORD dwThreadId)
{
    TCHAR szBuff[MAX_PATH + 1] = {'\0'};
    HWND hTitleWnd = NULL;
    HWND hWnd = NULL;
    int nLen = _tcslen(lpszWndSubTitleStr);
    
    if(hMainWnd == NULL)
    {
        // One way to do it
        //g_hWndFirstTopLevel = NULL;
        //BOOL bResult = ::EnumWindows(EnumWindowsProc, FALSE);

        //hMainWnd = g_hWndFirstTopLevel;

        hMainWnd = ::GetDesktopWindow();
#ifdef _WIN32_WCE
        hWnd = ::GetWindow(hMainWnd, GW_HWNDFIRST);
#else
        hWnd = ::GetWindow(hMainWnd, GW_CHILD);

#endif  

    }
    else
    {    
        hWnd = ::GetWindow(hMainWnd, GW_HWNDFIRST);
    }

    DWORD l_dwThreadId;
    DWORD l_dwProcessId;

    while (hWnd != 0)
    {
        if ((hWnd != hMainWnd) && ::IsWindowVisible(hWnd)&&
            (::GetWindow(hWnd, GW_OWNER) == 0)&&
            (::GetWindowText(hWnd, szBuff, MAX_PATH) != 0)&&
           (_tcsicmp (szBuff, _T("Desktop")) != 0))
        {
            if(bIsLeadingTitle != TRUE)
            {
                if(IsSubstringInString(lpszWndSubTitleStr, szBuff) == TRUE)
                {
                      l_dwThreadId = GetWindowThreadProcessId(hWnd, &l_dwProcessId);
                      if(((l_dwThreadId == dwThreadId)&&(l_dwProcessId == dwProcessId))||
                          ((dwThreadId == 0)&&(dwProcessId == 0)))
                      {
                          hTitleWnd = hWnd;

                          
                          break;
                      }
                }
            }
            else
            {
                if(_tcsnccmp(lpszWndSubTitleStr, szBuff, nLen) == 0)
                {
                      l_dwThreadId = GetWindowThreadProcessId(hWnd, &l_dwProcessId);
                      if(((l_dwThreadId == dwThreadId)&&(l_dwProcessId == dwProcessId))||
                          ((dwThreadId == 0)&&(dwProcessId == 0)))
                      {
                          hTitleWnd = hWnd;

                          
                          break;
                      }
                }
            }
               
        }

        hWnd = ::GetWindow(hWnd, GW_HWNDNEXT);
    }

    return hTitleWnd;
}

LPTSTR GetDirectory(LPCTSTR lpszCmdLine, LPTSTR lpszDirectory)
{
    int nLenCmd = _tcslen(lpszCmdLine);
    LPTSTR lpszReturn = NULL;
    int nI;

    for(nI = nLenCmd; nI >= 0; nI--)
    {
        if(lpszCmdLine[nI] == '\\')
        {
            _tcsncpy(lpszDirectory, lpszCmdLine, nI);
            lpszDirectory[nI] = '\0';
            lpszReturn = lpszDirectory;
            break;

        }
    }

    return lpszReturn;
}


HWND CreateProcessWithConditions(LPTSTR lpszCmdLine, LPCTSTR lpszWinTitle, HWND hMainWindow, LPCTSTR lpszMapFile,
                                 DWORD dwMaxWaitInMS, BOOL bIsLeadingTitle, LPTSTR lpszCurrentDirectory) 
{
    HANDLE m_hMap = NULL;
    int nCount;
    int nLoops;


    LPVOID lpEnvironment = NULL;
    LPTSTR lpCurrentDirectory = NULL;
    TCHAR szCurrentDirectory[4*_MAX_PATH];
    STARTUPINFO si={0};
    PROCESS_INFORMATION pi;

    memset(&si,0,sizeof(STARTUPINFO));

    si.cb=sizeof(STARTUPINFO);
    si.lpReserved=NULL;
    si.lpDesktop=NULL;
    si.lpTitle=NULL;
    si.dwX=0;
    si.dwY=0;
    si.dwXSize=400;
    si.dwYSize=300;
    si.dwXCountChars=15;
    si.dwYCountChars=10;
    si.dwFillAttribute=0;
    si.dwFlags=STARTF_USESHOWWINDOW/*|STARTF_USESIZE*/;
    si.wShowWindow=SW_SHOWNORMAL;
    si.cbReserved2=0;
    si.lpReserved2=NULL;
    si.hStdInput=NULL;
    si.hStdOutput=NULL;
    si.hStdError=NULL;


    nLoops = int(double(dwMaxWaitInMS)/250);
    if(nLoops < 8)
    {
        nLoops = 8;
    }

//    lpCurrentDirectory = GetDirectory(lpszCmdLine, szCurrentDirectory);
    lpCurrentDirectory = GetDirectory(lpszCurrentDirectory, szCurrentDirectory);
    
    if(CreateProcess(NULL, lpszCmdLine,NULL,NULL,FALSE,
                     /*CREATE_NEW_CONSOLE|*/NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,
                     lpEnvironment,lpCurrentDirectory,&si,&pi))
    {

       Sleep(2500);
        
       if(WaitForInputIdle(pi.hProcess,  dwMaxWaitInMS) != 0)
       {
           TerminateProcess(pi.hProcess, 0);
           return NULL;
       }
    }
    else
    {
        return NULL;
    }

// Wait for map file to be created if lpszMapFile != NULL
    if(lpszMapFile != NULL)
    {
        nCount = 0;

        while((m_hMap == NULL)&&(nCount <= nLoops))
        {
            m_hMap = ::CreateFileMapping((HANDLE)0xFFFFFFFF, NULL,PAGE_READWRITE,
                                         0, 512, lpszMapFile);
            Sleep(250);
            nCount++;
        }

        if(m_hMap != NULL)
        {
            if(::GetLastError() != ERROR_ALREADY_EXISTS) 
            {
                ::CloseHandle(m_hMap); 
                TerminateProcess(pi.hProcess, 0);
                return NULL;
            }
            ::CloseHandle(m_hMap); 
            
        }
        else
        {
            TerminateProcess(pi.hProcess, 0);
            return NULL;
        }

    }

    if((lpszWinTitle != NULL)&&(hMainWindow != NULL)) 
    { 
        HWND hWnd = NULL;
        nCount = 0;
        while((hWnd == NULL)&&(nCount <= nLoops))// Wait 25 seconds before failing
        {
            hWnd = GetHWndFromTitle(lpszWinTitle, hMainWindow, bIsLeadingTitle, pi.dwProcessId, pi.dwThreadId);
            Sleep(250);
            nCount++;
        }

        if(hWnd == NULL)
        {
            TerminateProcess(pi.hProcess, 0);
            return NULL;
        }
        else
        {
            return hWnd;
        }
    } 
    else
    {
        TerminateProcess(pi.hProcess, 0);
        return NULL;
    }

    return (HWND)TRUE;
}

// Queries for Operating system, memory etc; 070320 p�d

CString QueryOS()
{
	CString sResponse;
	OSVERSIONINFO OSversion;
	
	OSversion.dwOSVersionInfoSize=sizeof(OSVERSIONINFO);

	::GetVersionEx(&OSversion);

	switch(OSversion.dwPlatformId)
	{
	    case VER_PLATFORM_WIN32s: 
			{
				sResponse.Format(_T("Windows %d.%d"),OSversion.dwMajorVersion,OSversion.dwMinorVersion);
        break;
			}
			case VER_PLATFORM_WIN32_WINDOWS:
			{
			  if(OSversion.dwMinorVersion == 0)
					sResponse=_T("Windows	95");  
			  else if(OSversion.dwMinorVersion == 10)  
					sResponse=_T("Windows	98");
			  else if (OSversion.dwMinorVersion == 90)  
					sResponse=_T("Windows	Me");
        break;        
			}
			case VER_PLATFORM_WIN32_NT:
			{
				if(OSversion.dwMajorVersion == 5 && OSversion.dwMinorVersion == 0)
				 sResponse.Format(_T("Windows 2000 With %s"),OSversion.szCSDVersion);
				else if(OSversion.dwMajorVersion == 5 && OSversion.dwMinorVersion == 1)
				 sResponse.Format(_T("Windows XP %s"),OSversion.szCSDVersion);
				else if(OSversion.dwMajorVersion <= 4) 	  
					sResponse.Format(_T("Windows NT %d.%d with %s"),OSversion.dwMajorVersion,
												 OSversion.dwMinorVersion,OSversion.szCSDVersion);
				else	// for unknown windows/newest windows version	  
					sResponse.Format(_T("Windows %d.%d "),OSversion.dwMajorVersion,OSversion.dwMinorVersion);
		}
	}	
	return sResponse;
}

CString QueryTotalRAM()
{
	CString sResponse;
	MEMORYSTATUS memoryStatus;
	ZeroMemory(&memoryStatus,sizeof(MEMORYSTATUS));
	memoryStatus.dwLength = sizeof (MEMORYSTATUS);
	
	::GlobalMemoryStatus (&memoryStatus);
	
	sResponse.Format(_T("%ld MB"),(DWORD) ceil((double)memoryStatus.dwTotalPhys/1024/1024));

	return sResponse;
}

CString QueryFreeRAM()
{
	CString sResponse;
	MEMORYSTATUS memoryStatus;
	ZeroMemory(&memoryStatus,sizeof(MEMORYSTATUS));
	memoryStatus.dwLength = sizeof (MEMORYSTATUS);
	
	::GlobalMemoryStatus (&memoryStatus);
	
	sResponse.Format(_T("%ld KB"),(DWORD) (memoryStatus.dwAvailPhys/1024));

	return sResponse;
}

CString QueryPercentUsedRAM()
{
	CString sResponse;
	MEMORYSTATUS memoryStatus;
	ZeroMemory(&memoryStatus,sizeof(MEMORYSTATUS));
	memoryStatus.dwLength = sizeof (MEMORYSTATUS);
	
	::GlobalMemoryStatus (&memoryStatus);
	
	sResponse.Format(_T("%ld %%"),memoryStatus.dwMemoryLoad);

	return sResponse;
}

void setDBQualified(int value)
{
	regSetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_QUALIFIED_ITEM,value);
}

int getDBQualified(void)
{
	return regGetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_QUALIFIED_ITEM,1);
}

void setIsDBConSet(int value)
{
	regSetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_CONNECTED_ITEM,value);
}

int getIsDBConSet(void)
{
	return regGetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_CONNECTED_ITEM,-1);
}

int getAddDataToTables(void)
{
	return regGetInt(REG_ROOT,REG_FORREST_KEY,REG_SETUP_TABLES_ITEM,0);
}

void setAddDataToTables(int value)
{
	regSetInt(REG_ROOT,REG_FORREST_KEY,REG_SETUP_TABLES_ITEM,value);
}


//////////////////////////////////////////////////////////
// Misc. functions for calculations

// Calculate volume from DGV and Avg. height based on
// a "Formtal".
double getM3SkVolFromGyAndAvgHgt(double gy,double avg_hgt)
{
	double fFormtal = 0;

	if(avg_hgt <= 13.0)
	{
		fFormtal = 0.5;
	}
	else if(avg_hgt >= 20.0)
	{
		fFormtal = 0.45;
	}
	else
	{
		fFormtal = (0.50 - (((0.05/7)*(avg_hgt-13.0)) + 0.005));
	}

	return (avg_hgt * fFormtal * gy);
}


BOOL getHeightFunctions(vecUCFunctions &func_list1,vecUCFunctionList& func_list2)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
	typedef CRuntimeClass *(*Func2)(vecUCFunctionList&);
  Func1 proc1;
  Func2 proc2;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,CALCULATION_MODULE);

	func_list1.clear();
	func_list2.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_HEIGHT_FUNCTIONS));
		proc2 = (Func2)GetProcAddress((HMODULE)hModule, (GET_HEIGHT_FUNCTION_LIST));
		if (proc1 != NULL && proc2 != NULL)
		{
			proc1(func_list1);
			proc2(func_list2);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}
BOOL getVolumeFunctions(vecUCFunctions &func_list1,vecUCFunctionList& func_list2)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
	typedef CRuntimeClass *(*Func2)(vecUCFunctionList&);
  Func1 proc1;
  Func2 proc2;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,CALCULATION_MODULE);

	func_list1.clear();
	func_list2.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_VOLUME_FUNCTIONS));
		proc2 = (Func2)GetProcAddress((HMODULE)hModule, (GET_VOLUME_FUNCTION_LIST));
		if (proc1 != NULL && proc2 != NULL)
		{
			proc1(func_list1);
			proc2(func_list2);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}


BOOL getBarkFunctions(vecUCFunctions &func_list1,vecUCFunctionList& func_list2)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
	typedef CRuntimeClass *(*Func2)(vecUCFunctionList&);
  Func1 proc1;
  Func2 proc2;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,CALCULATION_MODULE);

	func_list1.clear();
	func_list2.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_BARK_FUNCTIONS));
		proc2 = (Func2)GetProcAddress((HMODULE)hModule, (GET_BARK_FUNCTION_LIST));
		if (proc1 != NULL && proc2 != NULL)
		{
			proc1(func_list1);
			proc2(func_list2);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

BOOL getVolumeFunctions_ub(vecUCFunctions &func_list1,vecUCFunctionList& func_list2)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
	typedef CRuntimeClass *(*Func2)(vecUCFunctionList&);
  Func1 proc1;
  Func2 proc2;

	CString sModule;

	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,CALCULATION_MODULE);

	func_list1.clear();
	func_list2.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_VOLUME_UB_FUNCTIONS));
		proc2 = (Func2)GetProcAddress((HMODULE)hModule, (GET_VOLUME_UB_FUNCTION_LIST));
		if (proc1 != NULL && proc2 != NULL)
		{
			proc1(func_list1);
			proc2(func_list2);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

BOOL getExchangeFunctions(vecUCFunctions &func_list1)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
  Func1 proc1;

	CString sModule;

	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,EXCHANGE_MODULE);

	func_list1.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_EXCH_FUNCTIONS));
		if (proc1 != NULL)
		{
			proc1(func_list1);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

BOOL getForrestNormFunctions(vecUCFunctions &func_list1)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
  Func1 proc1;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LANDVALUE_MODULE);

	func_list1.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_LANDVALUE_FUNCTIONS));
		if (proc1 != NULL)
		{
			proc1(func_list1);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

BOOL getForrestNormTypeOfInfring(vecUCFunctions &func_list1)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
  Func1 proc1;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LANDVALUE_MODULE);

	func_list1.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, (GET_LANDVALUE_TYPEOF_INFRING));
		if (proc1 != NULL)
		{
			proc1(func_list1);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

BOOL getGROTFunctions(vecUCFunctions &func_list)
{
	BOOL bReturn = FALSE;
	typedef CRuntimeClass *(*Func1)(vecUCFunctions&);
  Func1 proc1;

	CString sModule;

	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,CALCULATION_MODULE);

	func_list.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, GET_GROT_FUNCTIONS);
		if (proc1 != NULL)
		{
			proc1(func_list);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

// Create a H100 (e.g. T20,G32 from ex. 120 => T20); 070508 p�d
CString setupH100Value(LPCTSTR str)
{
	TCHAR szBuffer[32];
	_tcscpy(szBuffer,str);
	// Check first character in szBuffer, to see which
	// Specie's selected. I.e. 1 = Pine, 2 = Spruce all rest
	// will be Birch, if not IMP (Impediment) is set; 070508 p�d
	if (_tcscmp(szBuffer,_T("IMP")) == 0 || _tcscmp(szBuffer,_T("imp")) == 0)
		return _T("IMP");

	if (szBuffer[0] == '1' || szBuffer[0] == 'T' || szBuffer[0] == 't')
		szBuffer[0] = 'T';
	else	if (szBuffer[0] == '2' || szBuffer[0] == 'G' || szBuffer[0] == 'g')
		szBuffer[0] = 'G';
	else
		szBuffer[0] = 'B';
	return szBuffer;
}

CString getLangSet()
{
	HKEY hk;
	TCHAR strValue[127];
	TCHAR szRoot[127];
	DWORD dwLength = 126;	// Size of the value

	strValue[0] = '\0';

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf(szRoot,_T("%s\\%s"),REG_ROOT,REG_LANG_KEY);
	if (RegOpenKeyEx(HKEY_CURRENT_USER,
				  szRoot,
				  0,
				  KEY_QUERY_VALUE,
				  &hk) == ERROR_SUCCESS)
	{
		// Retrieve the value of the Left key
		RegQueryValueEx(hk,REG_LANG_ITEM,	NULL,NULL,(LPBYTE)strValue,&dwLength);
	}

	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return strValue;
}

CString getModulesDir(void)
{
	CString sPath;
	CString sModulesPath;
	if(AfxGetApp() != NULL)
	{
		VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
	}
	else
	{
		VERIFY(::GetModuleFileName(g_hInstanceDLL, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
		int nIndex = sPath.ReverseFind(_T('\\'));
		sPath.Truncate(nIndex);
	}

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"), sPath, SUBDIR_MODULES);

	return sModulesPath;
}

CString getModuleFN(HINSTANCE hinst)
{
	CString sPath;
	CString sModulesPath;
	VERIFY(::GetModuleFileName(hinst, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));

	sPath.ReleaseBuffer();

	return sPath;
}

CString getLanguageDir(void)
{
	CString sPath;
	CString sModulesPath;
	TCHAR szPath[_MAX_PATH]; 

	CWinApp* AFXAPI afxGetApp=AfxGetApp();
	if(afxGetApp != NULL)
	{
		::GetModuleFileName(afxGetApp->m_hInstance, szPath, _MAX_PATH);	// AfxGetApp() fungerar inte n�r man mixar olika versioner av MFC! 
		sPath = szPath;
	}
	else
	{
		::GetModuleFileName(g_hInstanceDLL, szPath, _MAX_PATH);
		sPath = szPath;
		int nIndex = sPath.ReverseFind(_T('\\'));
		sPath.Truncate(nIndex);
	}

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"),sPath,SUBDIR_LANGUAGE);

	return sModulesPath;
} 

CString getMyDocumentsDir(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
															 CSIDL_PERSONAL, 
															 NULL, 
															 0, 
															 szPath))) 
	{
		sPath.Format(_T("%s"),szPath);
		return sPath;
	}

	return _T("");
}

// This function'll get the "dafault" object inventory directory.
// The directory's set to:
//	"C:\Documents and Settings\(anv�ndare)\Mina dokument\HMS\LandValue"; 080205 p�d
CString getDefaultObjectInventoryDir(void)
{
	CString sMyDocumentsDir;
	CString sObjectsInventoryDir;
	sMyDocumentsDir = getMyDocumentsDir();
	if (!sMyDocumentsDir.IsEmpty())
	{
		sObjectsInventoryDir.Format(_T("%s\\%s"),
																sMyDocumentsDir,
																INVENTORY_DATA_MAIN_DIRECTORY);
		return sObjectsInventoryDir;
	}
	return _T("");
}


//HMS-76 20220127 H�mte s�kv�g f�r xml samt json export
CString getRegisterExportXmlJsonDir(void)
{
	CString sDir;
	sDir = regGetStr(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_XMLJSON);

	return sDir;

}
//HMS-76 20220127 Spara s�kv�g f�r xml samt json export
void setRegisterExportXmlJsonDir(LPCTSTR path)
{
regSetStr(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_XMLJSON,path);
}

// This function'll get the Inventory directory set in register
CString getRegisterObjectInventoryDir(void)
{
	CString sObjectInventory;
	sObjectInventory = regGetStr(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_ITEM);

	return sObjectInventory;
}

// This function'll set the Inventory directory set in register
void setRegisterObjectInventoryDir(LPCTSTR inv_dir)
{
	regSetStr(REG_ROOT,REG_LANDVALUE_KEY,REG_LANDVALUE_ITEM,inv_dir);
}

BOOL getRegisterDBBackupAllowed(void)
{
	int nValue = -1;
	nValue = regGetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_DBBACKUP_ALLOWED,1);

	return (nValue == 1);	// 1 = Allowed
}

void setRegisterDBBackupAllowed(int v)
{
	regSetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_DBBACKUP_ALLOWED,v);
}

BOOL getRegisterDBRestoreAllowed(void)
{
	int nValue = -1;
	nValue = regGetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_DBRESTORE_ALLOWED,1);

	return (nValue == 1);	// 1 = Allowed
}

void setRegisterDBRestoreAllowed(int v)
{
	regSetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_DBRESTORE_ALLOWED,v);
}

BOOL getRegisterDBCopyAllowed(void)
{
	int nValue = -1;
	nValue = regGetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_DBCOPY_ALLOWED,1);

	return (nValue == 1);	// 1 = Allowed
}

void setRegisterDBCopyAllowed(int v)
{
	regSetInt(REG_ROOT,REG_KEY_DATABASE,REG_DB_DBCOPY_ALLOWED,v);
}

CString getSuitesDir(void)
{
	CString sPath;
	CString sModulesPath;
	if(AfxGetApp() != NULL)
	{
		VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
	}
	else
	{
		VERIFY(::GetModuleFileName(g_hInstanceDLL, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
		int nIndex = sPath.ReverseFind(_T('\\'));
		sPath.Truncate(nIndex);
	}

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"), sPath, SUBDIR_SUITES);

	return sModulesPath;
}

CString getReportsDir(void)
{
	CString sPath;
	CString sModulesPath;
	if(AfxGetApp() != NULL)
	{
		VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
	}
	else
	{
		VERIFY(::GetModuleFileName(g_hInstanceDLL, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
		int nIndex = sPath.ReverseFind(_T('\\'));
		sPath.Truncate(nIndex);
	}

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"), sPath, SUBDIR_REPORTS);

	return sModulesPath;
}

CString getSetupsDir(void)
{
	CString sPath;
	CString sModulesPath;
	if(AfxGetApp() != NULL)
	{
		VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
	}
	else
	{
		VERIFY(::GetModuleFileName(g_hInstanceDLL, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
		sPath.ReleaseBuffer();
		int nIndex = sPath.ReverseFind(_T('\\'));
		sPath.Truncate(nIndex);
	}

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"), sPath, SUBDIR_SETUPS);

	return sModulesPath;
}

// Read language set in system; 071130 p�d
CString getSystemLanguage(void)
{
	TCHAR szValue[STR_LEN];
	// Get the language name associated with this locale ID.
	::GetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SABBREVLANGNAME,szValue,STR_LEN);

	return szValue;
}

// This function'll get the Inventory directory set in register
CString getRegisterRelease(void)
{
	CString sRelease;
	// Read releasenumber, from registry; 090520 p�d
	// _LM = LocalMachine
	sRelease = regGetStr_LM(REG_ROOT,REG_WEB_CONNECT_KEY,REG_RELEASE);

	return sRelease;
}

CString getSystemLanguageName(void)
{
	TCHAR szValue[STR_LEN];
	// Get the language name associated with this locale ID.
	::GetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SENGLANGUAGE,szValue,STR_LEN);

	return szValue;
}

/************************************************************************
* First checks if the new directory path exists if not then try to
* rename the folder.
*
* @param old_name The path to the folder that's gonna change the name.
* @param new_name The new path name.
* @return Returns 0 if the rename is successful. Returns the error code 
* EEXIST if the folder already exists and not zero if rename fails that
* probably means that the file/folder are locked. 
*
* Issue: #1885
************************************************************************/
int renameDirectory(LPCTSTR old_name,LPCTSTR new_name)
{
	 if( (_taccess( new_name, 0 )) == -1 )
	 {
		 return _trename(old_name,new_name);
	 }
	 else
	 {
		 //the new path already exists
		 return EEXIST;
	 }
}

// Added: 2008-02-04 p�d
// Just check if directory already exists; 080204 p�d
BOOL isDirectory(LPCTSTR dir_path)
{
	CDiskObject dobj;
	return dobj.DirectoryExists(dir_path);
}

// Create directory if not already exists; 080204 p�d
BOOL createDirectory(LPCTSTR dir_path)
{
	CDiskObject dobj;
	return dobj.CreateDirectory(dir_path);
}

BOOL copyFile(LPCTSTR from_file,LPCTSTR to_dir)
{
	CDiskObject dobj;
	return dobj.CopyFileW(from_file,to_dir);
}

BOOL getListOfSubDirs(LPCTSTR main_dir,CStringArray &sub_dir_list)
{
	CDiskObject dobj;
	// Check to see that the main dirctory actaully exists; 080204 p�d
	if (isDirectory(main_dir))
	{
		//AfxMessageBox(main_dir);
		dobj.EnumDirectories(main_dir,sub_dir_list);
	}
	return (sub_dir_list.GetCount() > 0);
}

BOOL getListOfFilesInDirectory(LPCTSTR filter,LPCTSTR main_dir,CStringArray &file_list,int mode)
{
	CDiskObject dobj;
	// Check to see that the main dirctory actaully exists; 080204 p�d
	if (isDirectory(main_dir))
	{
		return dobj.EnumFilesInDirectoryWithFilter(filter,main_dir,file_list,mode);
	}

	return FALSE;
}

BOOL getFileInformation(LPCTSTR file_name,BY_HANDLE_FILE_INFORMATION &file_info)
{
	CDiskObject dobj;
	if (fileExists(file_name))
	{
		return dobj.FileInformation(file_name,file_info);
	}
	return FALSE;
}

BOOL convertFileDateTime(FILETIME ft,CString &s)
{
	CString sDateTime;
	FILETIME conv_ft;
	SYSTEMTIME st;
	if (FileTimeToLocalFileTime(&ft,&conv_ft) == 0)
	{
		s = _T("");
		return FALSE;
	}	// if (FileTimeToLocalFileTime(&ft,&conv_ft) == 0)

	if (FileTimeToSystemTime(&conv_ft,&st) == 0)
	{
		s = _T("");
		return FALSE;
	}	// if (FileTimeToSystemTime(&conv_ft,&st) == 0)

	sDateTime.Format(_T("%04d-%02d-%02d %02d:%02d"),
			st.wYear,
			st.wMonth,
			st.wDay,
			st.wHour,
			st.wMinute);
	s = (sDateTime);
	return TRUE;
}

BOOL removeFile(CString file_name)
{
	CDiskObject dobj;
	if (fileExists(file_name))
	{
		return dobj.RemoveFile(file_name);
	}

	return FALSE;
}

CString getLanguageFN(LPCTSTR lang_dir,LPCTSTR prog_name,LPCTSTR lang_abrev,LPCTSTR lang_ext,LPCTSTR def_lngabrev)
{
	CString sLangFN;

	sLangFN.Format(_T("%s%s%s%s"),lang_dir,prog_name,lang_abrev,lang_ext);
	if (!fileExists(sLangFN))
	{
		sLangFN.Format(_T("%s%s%s%s"),lang_dir,prog_name,def_lngabrev,lang_ext);
	}
	return (sLangFN);
}

long getFileSize(LPCTSTR szFileName ) 
{ 
	std::ifstream f;
  f.open(szFileName, std::ios_base::binary | std::ios_base::in);
  if (!f.good() || f.eof() || !f.is_open()) { return 0; }
  f.seekg(0, std::ios_base::beg);
  std::ifstream::pos_type begin_pos = f.tellg();
  f.seekg(0, std::ios_base::end);
  return static_cast<long>(f.tellg() - begin_pos);
}

CString formatNumber(long number)
{
	TCHAR szBuffer[32];
	CString sNumber;
	CString sNumberReturn;
	short nCnt = 0;
	long lNumber = number;

	if (number < 0)
		lNumber *= -1;

	_stprintf(szBuffer,_T("%ld"),lNumber);

	if (lNumber < 1000 || lNumber > 999999999)
	{
		if (number < 0)
			sNumberReturn.Format(_T("-%s"),szBuffer);
		else
			sNumberReturn.Format(_T("%s"),szBuffer);
		return sNumberReturn;	// No action
	}

	sNumber.Empty();
	for (TCHAR *p=szBuffer;p < szBuffer + _tcslen(szBuffer);p++)
	{
		if (lNumber >= 1000 && lNumber < 10000)
			if (nCnt == 1) sNumber += _T(" ");
		if (lNumber >= 10000 && lNumber < 100000)
			if (nCnt == 2) sNumber += _T(" ");
		if (lNumber >= 100000 && lNumber < 1000000)
			if (nCnt == 3) sNumber += _T(" ");
		if (lNumber >= 1000000 && lNumber < 10000000)
			if (nCnt == 1 || nCnt == 4) sNumber += _T(" ");
		if (lNumber >= 10000000 && lNumber < 100000000)
			if (nCnt == 2 || nCnt == 5) sNumber += _T(" ");
		if (lNumber >= 100000000 && lNumber < 1000000000)
			if (nCnt == 3 || nCnt == 6) sNumber += _T(" ");
		
		sNumber += *p;

		nCnt++;
	}	// for (TCHAR *p=szBuffer;p < szBuffer + _tcslen(szBuffer);p++)

	if (number < 0)
		sNumberReturn.Format(_T("-%s"),sNumber);
	else
		sNumberReturn.Format(_T("%s"),sNumber);

	return sNumberReturn;
}

CString formatNumber(double number,short dec)
{
	TCHAR szBuffer[32];
	CString sNumber;
	CString sNumberReturn;
	short nCnt = 0;
	double fNumber = number;

	if (number < 0)
		fNumber *= -1;

	_stprintf(szBuffer,_T("%.*f"),dec,fNumber);

	if (fNumber < 1000 || fNumber > 999999999)
	{
		if (number < 0)
			sNumberReturn.Format(_T("-%s"),szBuffer);
		else
			sNumberReturn.Format(_T("%s"),szBuffer);
		return sNumberReturn;	// No action
	}

	sNumber.Empty();
	for (TCHAR *p=szBuffer;p < szBuffer + _tcslen(szBuffer);p++)
	{
		if (*p != '.' && *p != ',')
		{
			if (fNumber >= 1000.0 && fNumber < 10000.0)
				if (nCnt == 1) sNumber += _T(" ");
			if (fNumber >= 10000.0 && fNumber < 100000.0)
				if (nCnt == 2) sNumber += _T(" ");
			if (fNumber >= 100000.0 && fNumber < 1000000.0)
				if (nCnt == 3) sNumber += _T(" ");
			if (fNumber >= 1000000.0 && fNumber < 10000000.0)
				if (nCnt == 1 || nCnt == 4) sNumber += _T(" ");
			if (fNumber >= 10000000.0 && fNumber < 100000000.0)
				if (nCnt == 2 || nCnt == 5) sNumber += _T(" ");
			if (fNumber >= 100000000.0 && fNumber < 1000000000.0)
				if (nCnt == 3 || nCnt == 6) sNumber += _T(" ");
		}

		sNumber += *p;

		nCnt++;
	}	// for (TCHAR *p=szBuffer;p < szBuffer + _tcslen(szBuffer);p++)

	if (number < 0)
		sNumberReturn.Format(_T("-%s"),sNumber);
	else
		sNumberReturn.Format(_T("%s"),sNumber);

	return sNumberReturn;
}


void readUNICODESelectedFile(IN const UINT nCodePage,LPCTSTR fn,CStringArray &arr)
{
	int				nCharCount = 0;
	CStdioFileEx	fileEx;
	CString			sText, sLine;

	// Set the code page. Will not make any difference for Unicode-Unicode or ANSI-ANSI using the
	// same code page
	fileEx.SetCodePage(nCodePage);

	// Open file
	if (fileEx.Open(fn, CFile::modeRead | CFile::typeText))
	{
		// Is file unicode?

		nCharCount = fileEx.GetCharCount();

		// Read first nCharCount lines
		for (long nLineCount = 0; nLineCount < nCharCount && fileEx.ReadString(sLine); nLineCount++)
		{
			if (sLine.Right(1) != ';')
				sLine += ';';
			arr.Add(sLine);
		}

		fileEx.Close();
	}
}


void setFromNavBarOrOtherInReg(short v)
{
	// Write to registry; 081201 p�d
	regSetInt(REG_ROOT,REG_IS_NAVBAR_KEY,REG_IS_NAVBAR,v );
}

short getFromNavBarOrOtherInReg(void)
{
	// Read from registry; 081201 p�d
	return regGetInt(REG_ROOT,REG_IS_NAVBAR_KEY,REG_IS_NAVBAR,1 /* Default value = from NavBar */ );
}


CString makeStringFromNumeric(CString value)
{
	BOOL bIsNumeric = TRUE;
	double fValue = 0.0;
	CString sValue(value);
	TCHAR szBuffer[255];
	_tcscpy(szBuffer,value);
	for (TCHAR *p = szBuffer;p < _tcslen(szBuffer)+szBuffer;p++)
	{
		if (isalpha(*p) || *p == ' ' ||*p == '-' || *p == ';' || *p == '_' || *p == '>' || *p == '<' || *p == '!')
		{
			bIsNumeric = FALSE;
			break;
		}
	}
	if (bIsNumeric)
	{
		fValue = _tstof(value);
		sValue.Format(_T("%.0f"),fValue);
	}

	return sValue;
}


void scanFileName(CString& fn)
{
	fn.Replace(_T("\\"),_T("_"));
	fn.Replace(_T("/"),_T("_"));
	fn.Replace(_T(":"),_T("_"));
	fn.Replace(_T("*"),_T("_"));
	fn.Replace(_T("?"),_T("_"));
	fn.Replace(_T("\""),_T("_"));
	fn.Replace(_T("<"),_T("_"));
	fn.Replace(_T(">"),_T("_"));
	fn.Replace(_T("|"),_T("_"));
}

void doEvents()
{
	// Process window messages
	MSG msg;
	while ( PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) )
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

// Function added 2010-02-18 P�D
// Remove Carrige Returns and Line Feeds from a string
// and replace with data in string 'replace'; 100218 p�d
CString cleanCRLF(LPCTSTR str,LPCTSTR replace)
{
	CString sStr(str),S;
	BOOL bIsCRInStr = (sStr.Find(L"\n") > -1);
	BOOL bIsLFInStr = (sStr.Find(L"\r") > -1);

	if (bIsCRInStr && !bIsLFInStr)
		sStr.Replace(L"\n",replace);
	else if (bIsLFInStr && !bIsCRInStr)
		sStr.Replace(L"\r",replace);
	else if (bIsLFInStr && bIsCRInStr)
	{
		sStr.Replace(L"\n",replace);
		sStr.Replace(L"\r",L"");
	}
	return sStr;
}

//---------------------------------------------------------------------------------------
// Added 2010-03-17 P�D
// Ber�kna Noordkoordinat RT90 fr�n Latitud,Longitud
#define Deg2Rad 3.1415926536/180.0
int SweRef99LatLongtoRT90xy(CLatLongToRT90_data *tgps)
{
  double Sx,Sy,Sz,fi,la,Np,e2,f,a,Rx,Ry,Rz;
  double h,L,F,p,T;
  double dX,dY,dZ,wx,wy,wz,d;
  double ah,n,Lon0,k0,dLon,FN,FE;
  double e4,sin2L,n2,n4;
  double A,B,C,D,Lath,B1,B2,B3,B4,ep,np;
  double x,y;
  int Latd,Latm,Lond,Lonm;
  float Lats,Lons;

  dX=-414.0978567149;
  dY=-41.3381489658;
  dZ=-603.0627177516;

  wx=(-0.8550434314/3600.0)*(Deg2Rad);
  wy=(2.1413465185/3600.0)*(Deg2Rad);
  wz=(-7.0227209516/3600.0)*(Deg2Rad);
  d=0.0;

  a=6378137.0;
  f=1.0/298.257222101;

 h=tgps->getHgt();

 if(tgps->getLat() <= 0.0 && tgps->getLong() == 0.0 && tgps->getHgt() == 0.0)
  {
    tgps->setX(0.0);
    tgps->setY(0.0);
    return 0;
  }

  //dont use if not in Europe
	if(tgps->getLat() < 0.0 || tgps->getLong() < 0.0)
	{
    tgps->setX(0.0);
    tgps->setY(0.0);
    return 0;
  }

	Latd=tgps->getLat()/100;
	Latm=(int)tgps->getLat()-Latd*100;
	Lats=(tgps->getLat()-(float)Latd*100.0-(float)Latm*1.0)*60.0;
  fi=(Latd*1.0+Latm/60.0+Lats/3600.0)*Deg2Rad;    //radians

	Lond=tgps->getLong()/100;
	Lonm=(int)tgps->getLong()-Lond*100;
	Lons=(tgps->getLong()-(float)Lond*100.0-(float)Lonm*1.0)*60.0;
  la=(Lond*1.0+Lonm/60.0+Lons/3600.0)*Deg2Rad;

  e2=f*(2.0-f);
  Np=a/sqrt(1.0-e2*sin(fi)*sin(fi));

  //lat,long sweref99 to X,Y,Z sweref99
  Sx=(Np+h)*cos(fi)*cos(la);
  Sy=(Np+h)*cos(fi)*sin(la);
  Sz=(Np*(1.0-e2)+h)*sin(fi);


  //X,Y,Z sweref99 to X,Y,Z rt90
  Rx=dX+(1.0+d)*((cos(wz)*cos(wy))*Sx+(sin(wz)*cos(wx)+cos(wz)*sin(wy)*sin(wx))*Sy+(sin(wz)*sin(wx)-cos(wz)*sin(wy)*cos(wx))*Sz);
  Ry=dY+(1.0+d)*((-sin(wz)*cos(wy))*Sx+(cos(wz)*cos(wx)-sin(wz)*sin(wy)*sin(wx))*Sy+(cos(wz)*sin(wx)+sin(wz)*sin(wy)*cos(wx))*Sz);
  Rz=dZ+(1.0+d)*(sin(wy)*Sx-sin(wx)*Sy+(cos(wy)*cos(wx))*Sz);

  a=6377397.155;
  f=1.0/299.1528128;

  e2=f*(2.0-f);
  Np=a/sqrt(1.0-e2*sin(fi)*sin(fi));

  //X,Y,Z rt90 to lat,long rt90
  p=sqrt(Rx*Rx+Ry*Ry);
  T=atan(Rz/(p*sqrt(1.0-e2)));

  L=atan(Ry/Rx);    //longitude
  F=atan((Rz+((a*e2)/(sqrt(1.0-e2)))*sin(T)*sin(T)*sin(T))/(p-a*e2*cos(T)*cos(T)*cos(T)));  //latitud
  //H=(p/cos(F))-Np;    //height not used

  //lat,long rt90 to x,y
  Lon0=(15.0+48.0/60.0+29.8/3600.0)*Deg2Rad;
  k0=1.0;
  FN=0.0;
  FE=1500000.0;

  e4=e2*e2;
  n=f/(2.0-f);
  n2=n*n;
  n4=n2*n2;
  ah=(a/(1.0+n))*(1.0+n2/4.0+n4/64.0);

  A=e2;
  B=(1.0/6.0)*(5.0*e4-e2*e4);
  C=(1.0/120.0)*(104.0*e2*e4-45.0*e4*e4);
  D=(1.0/1260.0)*(1237.0*e4*e4);

  sin2L=sin(F)*sin(F);
  Lath=F-sin(F)*cos(F)*(A+B*sin2L+C*sin2L*sin2L+D*sin2L*sin2L*sin2L);

  dLon=L-Lon0;

  ep=atan(tan(Lath)/cos(dLon));
  np=(1.0/2.0)*log((1.0+cos(Lath)*sin(dLon))/(1.0-cos(Lath)*sin(dLon)));  //atanh(cos(Lath)*sin(dLon));


  B1=(1.0/2.0)*n-(2.0/3.0)*n2+5.0/16.0*(n2*n)+(41.0/180.0)*n4;
  B2=(13.0/48.0)*n2-(3.0/5.0)*n2*n+(557.0/1440.0)*n4;
  B3=(61.0/240.0)*n2*n-(103.0/140.0)*n4;
  B4=(49561.0/161280.0)*n4;

  x=k0*ah*(ep+B1*sin(2.0*ep)*cosh(2.0*np)+B2*sin(4.0*ep)*cosh(4.0*np)+B3*sin(6.0*ep)*cosh(6.0*np)+B4*sin(8.0*ep)*cosh(8.0*np))+FN;
  y=k0*ah*(np+B1*cos(2.0*ep)*sinh(2.0*np)+B2*cos(4.0*ep)*sinh(4.0*np)+B3*cos(6.0*ep)*sinh(6.0*np)+B4*cos(8.0*ep)*sinh(8.0*np))+FE;

  tgps->setX(x);
  tgps->setY(y);

  return 1;
}

// Creates a directory and set filesystem permissions to world read/writeable
int CreatePublicDirectory(CString csPath)
{
  if(!CreateDirectory(csPath,NULL))
    return FALSE;

  HANDLE hDir = CreateFile(csPath,READ_CONTROL|WRITE_DAC,0,NULL,OPEN_EXISTING,FILE_FLAG_BACKUP_SEMANTICS,NULL);
  if(hDir == INVALID_HANDLE_VALUE)
    return FALSE; 

  ACL* pOldDACL;
  SECURITY_DESCRIPTOR* pSD = NULL;
  GetSecurityInfo(hDir, SE_FILE_OBJECT , DACL_SECURITY_INFORMATION,NULL, NULL, &pOldDACL, NULL, (void**)&pSD);

  PSID pSid = NULL;
  SID_IDENTIFIER_AUTHORITY authNt = SECURITY_NT_AUTHORITY;
  AllocateAndInitializeSid(&authNt,2,SECURITY_BUILTIN_DOMAIN_RID,DOMAIN_ALIAS_RID_USERS,0,0,0,0,0,0,&pSid);

  EXPLICIT_ACCESS ea={0};
  ea.grfAccessMode = GRANT_ACCESS;
  ea.grfAccessPermissions = GENERIC_ALL;
  ea.grfInheritance = CONTAINER_INHERIT_ACE|OBJECT_INHERIT_ACE;
  ea.Trustee.TrusteeType = TRUSTEE_IS_GROUP;
  ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
  ea.Trustee.ptstrName = (LPTSTR)pSid;

  ACL* pNewDACL = 0;
  DWORD err = SetEntriesInAcl(1,&ea,pOldDACL,&pNewDACL);

  if(pNewDACL)
    SetSecurityInfo(hDir,SE_FILE_OBJECT,DACL_SECURITY_INFORMATION,NULL, NULL, pNewDACL, NULL);

  FreeSid(pSid);
  LocalFree(pNewDACL);
  LocalFree(pSD);
  LocalFree(pOldDACL);
  CloseHandle(hDir);

  return TRUE;

}


CString getLocaleDecimalPoint(void)
{
	TCHAR szValue[STR_LEN];

	if(AfxGetApp() != NULL)
	{
		GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL,szValue, STR_LEN);
	}
	else
	{
	GetLocaleInfo(LOCALE_SYSTEM_DEFAULT, LOCALE_SDECIMAL,szValue, STR_LEN);	// use this for HMS running as a webservice
	}

	return szValue;
}

double localeStrToDbl(LPCTSTR value)
{
	CString sDecPnt(getLocaleDecimalPoint());
	CString sValue(value);
	if (sValue.Find(L",") && sDecPnt == L".")	sValue.Replace(L",",sDecPnt);
	else if (sValue.Find(L".") && sDecPnt == L",")	sValue.Replace(L".",sDecPnt);

	return _tstof(sValue);
}

void TextToHtml(CString *pcsText)
{
	pcsText->Replace(_T("&"), _T("&amp;"));
	pcsText->Replace(_T("/"), _T("&#47;"));
	pcsText->Replace(_T("<"), _T("&lt;"));
	pcsText->Replace(_T(">"), _T("&gt;"));
	pcsText->Replace(_T("\""), _T("&quot;"));
}
