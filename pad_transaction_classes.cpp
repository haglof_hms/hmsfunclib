#include "stdafx.h"
#include "pad_transaction_classes.h"
#include "pad_hms_miscfunc.h"


// Transaction class

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class: CTransaction_prl_data. 

CTransaction_prl_data::CTransaction_prl_data(void)
{
	m_nSpcID				= -1;
	m_nIdentifer		= -1;
	m_sQualName			= _T("");
	m_vecInt.clear();
}


CTransaction_prl_data::CTransaction_prl_data(int spc_id,int identifer,LPCTSTR qual_name,vecInt &vec)
{
	m_nSpcID				= spc_id;
	m_nIdentifer		= identifer;
	m_sQualName			= (qual_name);	
	if (vec.size() > 0)
	{
		m_vecInt.clear();
		for (UINT i = 0;i < vec.size();i++)
		{
			m_vecInt.push_back(vec[i]);
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
}
	// Copy constructor
/*
CTransaction_prl_data::CTransaction_prl_data(const CTransaction_prl_data &c)
{
	*this = c;
}
*/
int CTransaction_prl_data::getSpcID(void)
{
	return m_nSpcID;
}

int CTransaction_prl_data::getIdentifer(void)
{
	return m_nIdentifer;
}

LPCTSTR CTransaction_prl_data::getQualName(void)
{
	return m_sQualName;
}
vecInt &CTransaction_prl_data::getInts(void)
{
	return m_vecInt;
}

void CTransaction_prl_data::setInts(int value)
{
	m_vecInt.push_back(value);
}

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class. Transfers data from CDataEnterDlg
// via CPricelistsView to CPricelistsFormView. Setting values to Pricelist and Quality-
// description ReportControls (Grids); 060324 p�d

CTransaction_diameterclass::CTransaction_diameterclass(void)
{
	m_nSpcID			= -1;
	m_nStartDiam	= 0;		// Start from this diamter (mm)
	m_nDiamClass	= 0;		// Diamter class increment in (mm)
	m_nNumOfDCLS	= 0;		// Number of diamters in this diamterclass
}

CTransaction_diameterclass::CTransaction_diameterclass(int spc_id,int start_diam,int diam_class,int numof_dcls)
{
	m_nSpcID			= spc_id;
	m_nStartDiam	= start_diam;		// Start from this diamter (mm)
	m_nDiamClass	= diam_class;		// Diamter class increment in (mm)
	m_nNumOfDCLS	= numof_dcls;		// Number of diamters in this diamterclass
}

CTransaction_diameterclass::CTransaction_diameterclass(const	CTransaction_diameterclass &c)
{
	*this = c;
}

int CTransaction_diameterclass::getSpcID(void)
{
	return m_nSpcID;
}

int CTransaction_diameterclass::getStartDiam(void)
{
	return m_nStartDiam;
}

int CTransaction_diameterclass::getDiamClass(void)
{
	return m_nDiamClass;
}

int CTransaction_diameterclass::getNumOfDCLS(void)
{
	return m_nNumOfDCLS;
}

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_species
CTransaction_species::CTransaction_species(void)
{
	m_nID					= -1;
	m_nSpcID			= -1;
	m_sSpcName		= _T("");
	m_sNotes			= _T("");
	m_sCreated		= _T("");
}

CTransaction_species::CTransaction_species(int id,int spc_id,int nP30SpcId,LPCTSTR spc_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID			= id;
	m_nSpcID		= spc_id;
	m_nP30SpcID		= nP30SpcId;
	m_sSpcName		= (spc_name);
	m_sNotes		= (notes);
	m_sCreated		= (created);
}

CTransaction_species::CTransaction_species(const CTransaction_species &c)
{
	*this = c;
}

int CTransaction_species::getID(void)
{
	return m_nID;
}

int CTransaction_species::getSpcID(void)
{
	return m_nSpcID;
}

int CTransaction_species::getP30SpcID(void)
{
	return m_nP30SpcID;
}


CString CTransaction_species::getSpcName(void)
{
	return m_sSpcName;
}

CString CTransaction_species::getNotes(void)
{
	return m_sNotes;
}

CString CTransaction_species::getCreated(void)
{
	return m_sCreated;
}

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_assort
CTransaction_assort::CTransaction_assort(void)
{
	m_nSpcID						= -1;		// No specie yet
	m_sAssortName				= _T("");
	m_fMinDiam					= 0.0;
	m_fPriceM3fub				= 0.0;
	m_fPriceM3to				= 0.0;
	m_nPulpType					= -1;
}

CTransaction_assort::CTransaction_assort(int spc_id,
	                                      LPCTSTR assort_name,
																				double min_diam,
																				double price_m3fub,
																				double price_m3to,
																				int pulp_type)

{
	m_nSpcID						= spc_id;
	m_sAssortName				= (assort_name);
	m_fMinDiam					= min_diam;
	m_fPriceM3fub				= price_m3fub;
	m_fPriceM3to				= price_m3to;
	m_nPulpType					= pulp_type;
}

CTransaction_assort::CTransaction_assort(const CTransaction_assort &c)
{
	*this = c;
}

int CTransaction_assort::getSpcID(void)									{ return m_nSpcID; }
CString CTransaction_assort::getAssortName(void)				{ return m_sAssortName; }
double CTransaction_assort::getMinDiam(void)						{ return m_fMinDiam;	}
double CTransaction_assort::getPriceM3fub(void)					{ return m_fPriceM3fub; }
double CTransaction_assort::getPriceM3to(void)					{ return m_fPriceM3to;	}
int CTransaction_assort::getPulpType(void)							{ return m_nPulpType;	}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_pricelist
CTransaction_pricelist::CTransaction_pricelist(void)
{
	m_nID							= -1;
	m_sName						= _T("");
	m_nTypeOf					= -1;
	m_sPricelistFile	= _T("");
	m_sCreatedBy			= _T("");
	m_sCreated				= _T("");
}

CTransaction_pricelist::CTransaction_pricelist(int id,LPCTSTR name,int type_of,LPCTSTR prl_name,LPCTSTR created_by,LPCTSTR created)
{
	m_nID							= id;
	m_sName						= (name);
	m_nTypeOf					= type_of;
	m_sPricelistFile	= (prl_name);
	m_sCreatedBy			= (created_by);
	m_sCreated				= (created);
}

CTransaction_pricelist::CTransaction_pricelist(const CTransaction_pricelist &c)
{
	*this = c;
}

int CTransaction_pricelist::getID(void)									{	return m_nID;}
CString CTransaction_pricelist::getName(void)						{	return m_sName;}
int CTransaction_pricelist::getTypeOf(void)							{	return m_nTypeOf;}
CString CTransaction_pricelist::getPricelistFile(void)	{	return m_sPricelistFile;}
CString CTransaction_pricelist::getCreatedBy(void)			{	return m_sCreatedBy;}
CString CTransaction_pricelist::getCreated(void)				{	return m_sCreated;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_template
CTransaction_template::CTransaction_template(void)
{
	m_nID							= -1;
	m_sTemplateName		= _T("");
	m_nTypeOf					= -1;
	m_sTemplateFile		= _T("");
	m_sTemplateNotes	= _T("");
	m_sCreatedBy			= _T("");
	m_sCreated				= _T("");
}

CTransaction_template::CTransaction_template(int id,LPCTSTR tmpl_name,int type_of,LPCTSTR tmpl_file,LPCTSTR notes,LPCTSTR created_by,LPCTSTR created)
{
	m_nID							= id;
	m_sTemplateName		= (tmpl_name);
	m_nTypeOf					= type_of;
	m_sTemplateFile		= (tmpl_file);
	m_sTemplateNotes	= (notes);
	m_sCreatedBy			= (created_by);
	m_sCreated				= (created);
}

CTransaction_template::CTransaction_template(const CTransaction_template &c)
{
	*this = c;
}

int CTransaction_template::getID(void)								{	return m_nID;}
CString CTransaction_template::getTemplateName(void)	{	return m_sTemplateName;}
int CTransaction_template::getTypeOf(void)						{	return m_nTypeOf;}
CString CTransaction_template::getTemplateFile(void)	{	return m_sTemplateFile;}
CString CTransaction_template::getTemplateNotes(void)	{	return m_sTemplateNotes;}
CString CTransaction_template::getCreatedBy(void)			{	return m_sCreatedBy;}
CString CTransaction_template::getCreated(void)				{	return m_sCreated;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_postnumber
CTransaction_postnumber::CTransaction_postnumber(void)
{
	m_nID							= -1;
	m_sName						= _T("");
	m_sNumber					= _T("");
	m_sCreated				= _T("");
}

CTransaction_postnumber::CTransaction_postnumber(int id,LPCTSTR name,LPCTSTR number,LPCTSTR created)
{
	m_nID							= id;
	m_sName						= (name);
	m_sNumber					= (number);
	m_sCreated				= (created);
}

CTransaction_postnumber::CTransaction_postnumber(const CTransaction_postnumber &c)
{
	*this = c;
}

int CTransaction_postnumber::getID(void)									{	return m_nID;}

CString CTransaction_postnumber::getName(void)						{	return m_sName;}

CString CTransaction_postnumber::getNumber(void)					{	return m_sNumber;}

CString CTransaction_postnumber::getCreated(void)					{	return m_sCreated;}



//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_category
CTransaction_category::CTransaction_category(void)
{
	m_nID							= -1;
	m_sCategory				= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_category::CTransaction_category(int id,LPCTSTR category,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sCategory				= (category);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_category::CTransaction_category(const CTransaction_category &c)
{
	*this = c;
}

int CTransaction_category::getID(void)									{	return m_nID;}

CString CTransaction_category::getCategory(void)				{	return m_sCategory;}

CString CTransaction_category::getNotes(void)						{	return m_sNotes;}

CString CTransaction_category::getCreated(void)					{	return m_sCreated;}



CTransaction_sample_tree_category::CTransaction_sample_tree_category(void)
{
	m_nID = -1;
	m_sCategory = _T("");
	m_sNotes = _T("");
	m_sCreated = _T("");
}

CTransaction_sample_tree_category::CTransaction_sample_tree_category(int id, LPCTSTR category, LPCTSTR notes, LPCTSTR created)
{
	m_nID = id;
	m_sCategory = category;
	m_sNotes = notes;
	m_sCreated = created;
}

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_contacts
CTransaction_contacts::CTransaction_contacts(void)
{
	m_nID							= -1;
	m_sPNR_ORGNR			= _T("");
	m_sName						= _T("");
	m_sCompany				= _T("");
	m_sAddress				= _T("");
	m_sCoAddress			= _T("");
	m_sPostNum				= _T("");
	m_sPostAddress		= _T("");
	m_sCounty					= _T("");
	m_sCountry				= _T("");
	m_sPhoneWork			= _T("");
	m_sPhoneHome			= _T("");
	m_sFaxNumber			= _T("");
	m_sMobile					= _T("");
	m_sEMail					= _T("");
	m_sWebSite				= _T("");
	m_sNotes					= _T("");
	m_sCreatedBy			= _T("");
	m_sCreated				= _T("");
	m_sOwnerShare			= _T("");
	m_sBankgiro				= _T("");
	m_sPlusgiro				= _T("");
	m_sBankkonto			= _T("");
	m_sLevnummer			= _T("");

}

CTransaction_contacts::CTransaction_contacts(int id,
																						 LPCTSTR pnr_orgnr,
																						 LPCTSTR name,
																						 LPCTSTR company,
																						 LPCTSTR address,
																						 LPCTSTR co_address,
																						 LPCTSTR post_num,
																						 LPCTSTR post_address,
																						 LPCTSTR county,
																						 LPCTSTR country,
																						 LPCTSTR phone_work,
																						 LPCTSTR phone_home,
																						 LPCTSTR fax_number,
																						 LPCTSTR mobile,
																						 LPCTSTR e_mail,
																						 LPCTSTR web_site,
																						 LPCTSTR notes,
																						 LPCTSTR created_by,
																						 LPCTSTR created,
																					#ifdef VER120
																						 LPCTSTR vat_number,
																						 int connect_id,
																						 int type_of,
																					#endif
																						 LPCTSTR bankgiro,
																						 LPCTSTR plusgiro,
																						 LPCTSTR bankkonto,
																						 LPCTSTR levnummer,
																						 LPCTSTR owner_share)
{
	m_nID							= id;
	m_sPNR_ORGNR			= (pnr_orgnr);
	m_sName						= (name);
	m_sCompany				= (company);
	m_sAddress				= (address);
	m_sCoAddress				= (co_address);
	m_sPostNum				= (post_num);
	m_sPostAddress		= (post_address);
	m_sCounty					= (county);
	m_sCountry				= (country);
	m_sPhoneWork			= (phone_work);
	m_sPhoneHome			= (phone_home);
	m_sFaxNumber			= (fax_number);
	m_sMobile					= (mobile);
	m_sEMail					= (e_mail);
	m_sWebSite				= (web_site);
	m_sNotes					= (notes);
	m_sCreatedBy			= (created_by);
	m_sCreated				= (created);
#ifdef VER120
	m_sVATNumber			= vat_number;
	m_nConnectID			= connect_id;	
	m_nTypeOf					= type_of;
#endif
	m_sBankgiro				= bankgiro;
	m_sPlusgiro				= plusgiro;
	m_sBankkonto			= bankkonto;
	m_sLevnummer			= levnummer;

	m_sOwnerShare			= (owner_share);
}

CTransaction_contacts::CTransaction_contacts(const CTransaction_contacts &c)
{
	*this = c;
}

int CTransaction_contacts::getID(void)									{	return m_nID;}
CString CTransaction_contacts::getPNR_ORGNR(void)				{	return m_sPNR_ORGNR;}
CString CTransaction_contacts::getName(void)						{	return m_sName;}
CString CTransaction_contacts::getCompany(void)					{	return m_sCompany;}
CString CTransaction_contacts::getAddress(void)					{	return m_sAddress;}
CString CTransaction_contacts::getCoAddress(void)					{	return m_sCoAddress;}
CString CTransaction_contacts::getPostNum(void)					{	return m_sPostNum;}
CString CTransaction_contacts::getPostAddress(void)			{	return m_sPostAddress;}
CString CTransaction_contacts::getCounty(void)					{	return m_sCounty;}
CString CTransaction_contacts::getCountry(void)					{	return m_sCountry;}
CString CTransaction_contacts::getPhoneWork(void)				{	return m_sPhoneWork;}
CString CTransaction_contacts::getPhoneHome(void)				{	return m_sPhoneHome;}
CString CTransaction_contacts::getFaxNumber(void)				{	return m_sFaxNumber;}
CString CTransaction_contacts::getMobile(void)					{	return m_sMobile;}
CString CTransaction_contacts::getEMail(void)						{	return m_sEMail;}
CString CTransaction_contacts::getWebSite(void)					{	return m_sWebSite;}
CString CTransaction_contacts::getNotes(void)						{	return m_sNotes;}
CString CTransaction_contacts::getCreatedBy(void)				{	return m_sCreatedBy;}
CString CTransaction_contacts::getCreated(void)					{	return m_sCreated;}
#ifdef VER120
CString CTransaction_contacts::getVATNumber(void)				{	return m_sVATNumber;}
int CTransaction_contacts::getConnectID(void)						{	return m_nConnectID;}
int CTransaction_contacts::getTypeOf(void)							{	return m_nTypeOf;}
#endif
CString CTransaction_contacts::getBankgiro(void)				{ return m_sBankgiro; }
CString CTransaction_contacts::getPlusgiro(void)				{ return m_sPlusgiro; }
CString CTransaction_contacts::getBankkonto(void)				{ return m_sBankkonto; }
CString CTransaction_contacts::getLevnummer(void)				{ return m_sLevnummer; }

CString CTransaction_contacts::getOwnerShare(void)			{	return m_sOwnerShare;}



//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_categories_for_contacts
CTransaction_categories_for_contacts::CTransaction_categories_for_contacts(void)
{
	m_nContactID			= -1;
	m_nCategoryID			= -1;
	m_sCreated				= _T("");
}

CTransaction_categories_for_contacts::CTransaction_categories_for_contacts(int contact, int category,LPCTSTR created)
{
	m_nContactID			= contact;
	m_nCategoryID			= category;
	m_sCreated				= (created);
}

CTransaction_categories_for_contacts::CTransaction_categories_for_contacts(const CTransaction_categories_for_contacts &c)
{
	*this = c;
}

int CTransaction_categories_for_contacts::getContactID(void)				{	return m_nContactID;}
void CTransaction_categories_for_contacts::setContactID(int id)			{	m_nContactID = id;}

int CTransaction_categories_for_contacts::getCategoryID(void)				{	return m_nCategoryID;}

CString CTransaction_categories_for_contacts::getCreated(void)			{	return m_sCreated;}

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_type
CTransaction_trakt_type::CTransaction_trakt_type(void)
{
	m_nID							= -1;
	m_sTraktType			= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_trakt_type::CTransaction_trakt_type(int id,LPCTSTR type_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sTraktType			= (type_name);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_trakt_type::CTransaction_trakt_type(const CTransaction_trakt_type &c)
{
	*this = c;
}

int CTransaction_trakt_type::getID(void)									{	return m_nID;}

CString CTransaction_trakt_type::getTraktType(void)				{	return m_sTraktType;}

CString CTransaction_trakt_type::getNotes(void)						{	return m_sNotes;}

CString CTransaction_trakt_type::getCreated(void)					{	return m_sCreated;}



//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_origin
CTransaction_origin::CTransaction_origin(void)
{
	m_nID							= -1;
	m_sOrigin					= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_origin::CTransaction_origin(int id,LPCTSTR origin_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sOrigin					= (origin_name);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_origin::CTransaction_origin(const CTransaction_origin &c)
{
	*this = c;
}

int CTransaction_origin::getID(void)									{	return m_nID;}

CString CTransaction_origin::getOrigin(void)					{	return m_sOrigin;}

CString CTransaction_origin::getNotes(void)						{	return m_sNotes;}

CString CTransaction_origin::getCreated(void)					{	return m_sCreated;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_input_method
CTransaction_input_method::CTransaction_input_method(void)
{
	m_nID							= -1;
	m_sInputMethod		= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_input_method::CTransaction_input_method(int id,LPCTSTR input_method_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sInputMethod		= (input_method_name);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_input_method::CTransaction_input_method(const CTransaction_input_method &c)
{
	*this = c;
}

int CTransaction_input_method::getID(void)									{	return m_nID;}

CString CTransaction_input_method::getInputMethod(void)			{	return m_sInputMethod;}

CString CTransaction_input_method::getNotes(void)						{	return m_sNotes;}

CString CTransaction_input_method::getCreated(void)					{	return m_sCreated;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_hgt_class
CTransaction_hgt_class::CTransaction_hgt_class(void)
{
	m_nID							= -1;
	m_sHgtClass				= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_hgt_class::CTransaction_hgt_class(int id,LPCTSTR hgt_class_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sHgtClass				= (hgt_class_name);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_hgt_class::CTransaction_hgt_class(const CTransaction_hgt_class &c)
{
	*this = c;
}

int CTransaction_hgt_class::getID(void)									{	return m_nID;}

CString CTransaction_hgt_class::getHgtClass(void)				{	return m_sHgtClass;}

CString CTransaction_hgt_class::getNotes(void)					{	return m_sNotes;}

CString CTransaction_hgt_class::getCreated(void)				{	return m_sCreated;}



//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_cut_class
CTransaction_cut_class::CTransaction_cut_class(void)
{
	m_nID							= -1;
	m_sCutClass				= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_cut_class::CTransaction_cut_class(int id,LPCTSTR cut_class_name,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sCutClass				= (cut_class_name);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_cut_class::CTransaction_cut_class(const CTransaction_cut_class &c)
{
	*this = c;
}

int CTransaction_cut_class::getID(void)									{	return m_nID;}

CString CTransaction_cut_class::getCutClass(void)				{	return m_sCutClass;}

CString CTransaction_cut_class::getNotes(void)					{	return m_sNotes;}

CString CTransaction_cut_class::getCreated(void)				{	return m_sCreated;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_county_municipal_parish
CTransaction_county_municipal_parish::CTransaction_county_municipal_parish(void)
{
	m_nID							= -1;
	m_nCountyID				= -1;
	m_nMunicipalID		= -1;
	m_nParishID				= -1;
	m_sCountyName			= _T("");;
	m_sMunicipalName	= _T("");;
	m_sParishName			= _T("");;
	m_sCreated				= _T("");
}

CTransaction_county_municipal_parish::CTransaction_county_municipal_parish(int id,
																																					 int county_id,
																																					 int municipal_id,
																																					 int parish_id,
																																					 LPCTSTR county_name,
																																					 LPCTSTR municipal_name,
																																					 LPCTSTR parish_name,
																																					 LPCTSTR created)
{
	m_nID							= id;
	m_nCountyID				= county_id;
	m_nMunicipalID		= municipal_id;
	m_nParishID				= parish_id;
	m_sCountyName			= (county_name);;
	m_sMunicipalName	= (municipal_name);;
	m_sParishName			= (parish_name);;
	m_sCreated				= (created);
}

CTransaction_county_municipal_parish::CTransaction_county_municipal_parish(const CTransaction_county_municipal_parish &c)
{
	*this = c;
}

int CTransaction_county_municipal_parish::getID(void)									{	return m_nID;}
int CTransaction_county_municipal_parish::getCountyID(void)						{	return m_nCountyID;}
int CTransaction_county_municipal_parish::getMunicipalID(void)				{	return m_nMunicipalID;}
int CTransaction_county_municipal_parish::getParishID(void)						{	return m_nParishID;}

CString CTransaction_county_municipal_parish::getCountyName(void)			{	return m_sCountyName;}
CString CTransaction_county_municipal_parish::getMunicipalName(void)	{	return m_sMunicipalName;}
CString CTransaction_county_municipal_parish::getParishName(void)			{	return m_sParishName;}

CString CTransaction_county_municipal_parish::getCreated(void)				{	return m_sCreated;}


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt

// Default constructor
CTransaction_trakt::CTransaction_trakt(void)
{
	m_nTraktID	= -1;
	m_sTraktNum	= _T("");
	m_sTraktPNum = _T("");	// Delnummer
	m_sTraktName = _T("");
	m_sTraktType = _T("");
	m_sTraktDate = _T("");
	m_nTraktPropID = -1;	// ID for Property
	m_fTraktAreal = 0.0;
	m_nTraktLength = 0;
	m_nTraktWidth = 0;
	m_fTraktArealConsiderd = 0.0;
	m_fTraktArealHandled = 0.0;
	m_nTraktAge = 0;
	m_sTraktSIH100 = _T("");
	m_fTraktWithdraw = 0.0;
	m_nTraktHgtOverSea = -1;
	m_nTraktGround = -1;
	m_nTraktSurface = -1;
	m_nTraktRake = -1;
	m_sTraktMapdata = _T("");
	m_nTraktLatitude = -1;
	m_nTraktLongitude = -1;
	m_sTraktXCoord = _T("");
	m_sTraktYCoord = _T("");
	m_sTraktOrigin = _T("");
	m_sTraktInpMethod = _T("");
	m_sTraktHgtClass = _T("");
	m_sTraktCutClass = _T("");
	m_nTraktSimData = 0;
	m_nTraktNearCoast = 0;
	m_nTraktSoutheast = 0;
	m_nTraktRegion5 = 0;
	m_nTraktPartOfPlot = 0;
	m_sTraktSIH100_Pine = _T("");
	m_sPropNum = _T("");
	m_sPropName = _T("");
	m_sTraktNotes = _T("");
	m_sTraktCreatedBy = _T("");
	m_sCreated = _T("");
	m_nWSide	= -1;	// No side yet
	m_nOnlyForStand = 0;	// Default value
	// DATA-MEMBER NOT INCLUDED IN CONSTRUCTOR; 090625 p�d
	m_fCruiseWidth = 0.0;
	m_sTraktPoint=_T("");
	m_sTraktCoordinates=_T("");
}

CTransaction_trakt::CTransaction_trakt(int id,LPCTSTR num,LPCTSTR pnum,LPCTSTR name,LPCTSTR type,LPCTSTR date,
									 int prop_id,double areal,double areal_considerd,double areal_handled,
									 int age,LPCTSTR h100,double withdraw,int hgt_over_sea,
									 int ground,int surface,int rake,LPCTSTR mapdata,int latitude,int longitude,
									 LPCTSTR x_coord,LPCTSTR y_coord,LPCTSTR origin,LPCTSTR inp_method,LPCTSTR hgt_class,LPCTSTR cut_class,
									 BOOL sim_data,BOOL near_coast,BOOL southeast,BOOL region5,BOOL part_of_plot,LPCTSTR si_h100_pine,
									 LPCTSTR prop_num,LPCTSTR prop_name,
									 LPCTSTR notes,LPCTSTR created_by,LPCTSTR created,int wside,
									 short only_for_stand,int nTraktLength,int nTraktWidth,
									 BOOL bTillfUtnyttj,LPCTSTR cPoint,LPCTSTR cTraktCoordinates)
{
	m_nTraktID	= id;
	m_sTraktNum	= (num);
	m_sTraktPNum = (pnum);	// Delnummer
	m_sTraktName = (name);
	m_sTraktType = (type);
	m_sTraktDate = (date);
	m_nTraktPropID = prop_id;	// ID for Property
	m_fTraktAreal = areal;

	m_nTraktLength = nTraktLength;
	m_nTraktWidth = nTraktWidth;

	m_fTraktArealConsiderd = areal_considerd;
	m_fTraktArealHandled = areal_handled;
	m_nTraktAge = age;
	m_sTraktSIH100 = (h100);
	m_fTraktWithdraw = withdraw;
	m_nTraktHgtOverSea = hgt_over_sea;
	m_nTraktGround = ground;
	m_nTraktSurface = surface;
	m_nTraktRake = rake;
	m_sTraktMapdata = (mapdata);
	m_nTraktLatitude = latitude;
	m_nTraktLongitude = longitude;
	m_sTraktXCoord = (x_coord);
	m_sTraktYCoord = (y_coord);
	m_sTraktOrigin = (origin);
	m_sTraktInpMethod = (inp_method);
	m_sTraktHgtClass = (hgt_class);
	m_sTraktCutClass = (cut_class);
	m_nTraktSimData = sim_data;
	m_nTraktNearCoast = near_coast;
	m_nTraktSoutheast = southeast;
	m_nTraktRegion5 = region5;
	m_nTraktPartOfPlot = part_of_plot;
	m_sTraktSIH100_Pine = (si_h100_pine);
	m_sPropNum = (prop_num);
	m_sPropName = (prop_name);
	m_sTraktNotes = (notes);
	m_sTraktCreatedBy = (created_by);
	m_sCreated = (created);
	m_nWSide = wside;
	m_nOnlyForStand = only_for_stand;
	m_bTillfUtnyttj = bTillfUtnyttj;
	m_sTraktPoint=cPoint;
	m_sTraktCoordinates=cTraktCoordinates;
}
// Copy constructor
CTransaction_trakt::CTransaction_trakt(const CTransaction_trakt &v)
{
	*this = v;
}

int CTransaction_trakt::getTraktID(void)								{ return m_nTraktID; }
CString CTransaction_trakt::getTraktNum(void)						{ return m_sTraktNum; }
CString CTransaction_trakt::getTraktPNum(void)					{ return m_sTraktPNum; }
CString CTransaction_trakt::getTraktName(void)					{ return m_sTraktName; }
CString CTransaction_trakt::getTraktType(void)					{ return m_sTraktType; }
CString CTransaction_trakt::getTraktDate(void)					{ return m_sTraktDate; }
int CTransaction_trakt::getTraktPropID(void)						{ return m_nTraktPropID; }
double CTransaction_trakt::getTraktAreal(void)					{ return m_fTraktAreal; }

int CTransaction_trakt::getTraktLength(void) 				{ return m_nTraktLength; }
int CTransaction_trakt::getTraktWidth(void)				{ return m_nTraktWidth; }

double CTransaction_trakt::getTraktArealConsiderd(void)	{ return m_fTraktArealConsiderd; }
double CTransaction_trakt::getTraktArealHandled(void)		{ return m_fTraktArealHandled; }
int CTransaction_trakt::getTraktAge(void)								{ return m_nTraktAge; }
CString CTransaction_trakt::getTraktSIH100(void)				{ return m_sTraktSIH100; }
double CTransaction_trakt::getTraktWithdraw(void)				{ return m_fTraktWithdraw; }
int CTransaction_trakt::getTraktHgtOverSea(void)				{ return m_nTraktHgtOverSea; }
int CTransaction_trakt::getTraktGround(void)						{ return m_nTraktGround; }
int CTransaction_trakt::getTraktSurface(void)						{ return m_nTraktSurface; } 
int CTransaction_trakt::getTraktRake(void)							{ return m_nTraktRake; }
CString CTransaction_trakt::getTraktMapdata(void)				{ return m_sTraktMapdata; }
int CTransaction_trakt::getTraktLatitude(void)					{ return m_nTraktLatitude; }
int CTransaction_trakt::getTraktLongitude(void)					{ return m_nTraktLongitude; }
CString CTransaction_trakt::getTraktXCoord(void)				{ return m_sTraktXCoord; }
CString CTransaction_trakt::getTraktYCoord(void)				{ return m_sTraktYCoord; }
CString CTransaction_trakt::getTraktOrigin(void)				{ return m_sTraktOrigin; }
CString CTransaction_trakt::getTraktInpMethod(void)			{ return m_sTraktInpMethod; }
CString CTransaction_trakt::getTraktHgtClass(void)			{ return m_sTraktHgtClass; }
CString CTransaction_trakt::getTraktCutClass(void)			{ return m_sTraktCutClass; }
BOOL CTransaction_trakt::getTraktSimData(void)					{ return m_nTraktSimData; }
BOOL CTransaction_trakt::getTraktNearCoast(void)				{ return m_nTraktNearCoast; }
void CTransaction_trakt::setTraktNearCoast(int v)				{ m_nTraktNearCoast = v; }
BOOL CTransaction_trakt::getTraktSoutheast(void)				{ return m_nTraktSoutheast; }
void CTransaction_trakt::setTraktSoutheast(int v)				{ m_nTraktSoutheast = v; }
BOOL CTransaction_trakt::getTraktRegion5(void)					{ return m_nTraktRegion5; }
void CTransaction_trakt::setTraktRegion5(int v)					{ m_nTraktRegion5 = v; }
BOOL CTransaction_trakt::getTraktPartOfPlot(void)				{ return m_nTraktPartOfPlot; }
void CTransaction_trakt::setTraktPartOfPlot(int v)			{ m_nTraktPartOfPlot = v; }
CString CTransaction_trakt::getTraktSIH100_Pine(void)		{ return m_sTraktSIH100_Pine; }
void CTransaction_trakt::setTraktSIH100_Pine(LPCTSTR v)	{ m_sTraktSIH100_Pine = v; }
CString CTransaction_trakt::getTraktPropNum(void)				{ return m_sPropNum; }
CString CTransaction_trakt::getTraktPropName(void)			{ return m_sPropName; }
CString CTransaction_trakt::getTraktNotes(void)					{ return m_sTraktNotes; }
CString CTransaction_trakt::getTraktCreatedBy(void)			{ return m_sTraktCreatedBy; }

CString CTransaction_trakt::getTraktPoint(void)	{ return m_sTraktPoint; }
CString CTransaction_trakt::getTraktCoordinates(void)	{ return m_sTraktCoordinates; }


CString CTransaction_trakt::getCreated(void)						{ return m_sCreated; }
int CTransaction_trakt::getWSide(void)									{ return m_nWSide; }
void CTransaction_trakt::setWSide(int v)								{ m_nWSide = v; }
short CTransaction_trakt::getOnlyForStand(void)					{ return m_nOnlyForStand; }
short CTransaction_trakt::getTillfUtnyttj(void)
{
	if(m_bTillfUtnyttj)
	return  1;
	else
	return 0;
}
void CTransaction_trakt::setOnlyForStand(short v)				{ m_nOnlyForStand = v; }

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_data

// Default constructor
CTransaction_trakt_data::CTransaction_trakt_data(void)
{
		m_nTDataID	= -1;
		m_nTDataTraktID = -1;
		m_nTDataType = -1;
		m_nSpeiceID = -1;
		m_sSpecieName = _T("");
		m_fPercent = 0.0;
		m_nNumOf = -1;
		m_fDA	= 0.0;
		m_fDG = 0.0;
		m_fDGV = 0.0; 
		m_fHGV = 0.0;
		m_fGY = 0.0;
		m_fAvgHgt = 0.0;
		m_fH25 = 0.0;
		m_fGreenCrown = 0.0;
		m_fM3SK = 0.0;
		m_fM3FUB = 0.0;
		m_fM3UB = 0.0;
		m_fAvgM3SK = 0.0;
		m_fAvgM3FUB = 0.0;
		m_fAvgM3UB = 0.0;
		m_fGrot = 0.0;
		m_sCreated = _T("");
}

CTransaction_trakt_data::CTransaction_trakt_data(int tdata_id,int trakt_id,int data_type,
																								 int spc_id,LPCTSTR spc_name,
																								 double percent,long numof,
																								 double da,double dg,double dgv,double hgv,
																								 double gy,double avg_hgt,double h25,double green_crown,
																								 double m3sk,double m3fub,double m3ub,
																								 double avg_m3sk,double avg_m3fub,double avg_m3ub,double grot,
																								 LPCTSTR created)
{
		m_nTDataID	= tdata_id;
		m_nTDataTraktID = trakt_id;
		m_nTDataType = data_type;
		m_nSpeiceID = spc_id;
		m_sSpecieName = (spc_name);
		m_fPercent = percent;
		m_nNumOf = numof;
		m_fDA	= da;
		m_fDG = dg;
		m_fDGV = dgv; 
		m_fHGV = hgv;
		m_fGY = gy;
		m_fAvgHgt = avg_hgt;
		m_fH25 = h25;
		m_fGreenCrown = green_crown;
		m_fM3SK = m3sk;
		m_fM3FUB = m3fub;
		m_fM3UB = m3ub;
		m_fAvgM3SK = avg_m3sk;
		m_fAvgM3FUB = avg_m3fub;
		m_fAvgM3UB = avg_m3ub;
		m_fGrot = grot;
		m_sCreated = (created);
}

// Copy constructor
CTransaction_trakt_data::CTransaction_trakt_data(const CTransaction_trakt_data &v)
{
	*this = v;
}

int CTransaction_trakt_data::getTDataID(void)				{ return m_nTDataID; }
void CTransaction_trakt_data::setTDataID(int v)			{ m_nTDataID = v; }
int CTransaction_trakt_data::getTDataTraktID(void)	{ return m_nTDataTraktID; }
int CTransaction_trakt_data::getTDataType(void)			{ return m_nTDataType; }
void CTransaction_trakt_data::setTDataType(int v)		{ m_nTDataType = v; }
int CTransaction_trakt_data::getSpecieID(void)			{ return m_nSpeiceID; }
CString CTransaction_trakt_data::getSpecieName(void){ return m_sSpecieName; }
double CTransaction_trakt_data::getPercent(void)		{ return m_fPercent; }
void CTransaction_trakt_data::setPercent(double v)	{ m_fPercent = v; }
long CTransaction_trakt_data::getNumOf(void)				{ return m_nNumOf; }
void CTransaction_trakt_data::setNumOf(long v)			{ m_nNumOf = v; }
double CTransaction_trakt_data::getDA(void)					{ return m_fDA; }
void CTransaction_trakt_data::setDA(double v)				{ m_fDA = v; }
double CTransaction_trakt_data::getDG(void)					{ return m_fDG; }
void CTransaction_trakt_data::setDG(double v)				{ m_fDG = v; }
double CTransaction_trakt_data::getDGV(void)				{ return m_fDGV; }
void CTransaction_trakt_data::setDGV(double v)			{ m_fDGV = v; }
double CTransaction_trakt_data::getHGV(void)				{ return m_fHGV; }
void CTransaction_trakt_data::setHGV(double v)			{ m_fHGV = v; }
double CTransaction_trakt_data::getGY(void)					{ return m_fGY; }
void CTransaction_trakt_data::setGY(double v)				{ m_fGY = v; }
double CTransaction_trakt_data::getAvgHgt(void)			{ return m_fAvgHgt; }
void CTransaction_trakt_data::setAvgHgt(double v)		{ m_fAvgHgt = v; }
double CTransaction_trakt_data::getH25(void)				{ return m_fH25; }
void CTransaction_trakt_data::setH25(double v)			{ m_fH25 = v; }
double CTransaction_trakt_data::getGreenCrown(void)	{ return m_fGreenCrown; }
void CTransaction_trakt_data::setGreenCrown(double v)	{ m_fGreenCrown = v; }
double CTransaction_trakt_data::getM3SK(void)				{ return m_fM3SK; }
void CTransaction_trakt_data::setM3SK(double v)			{ m_fM3SK = v; }
double CTransaction_trakt_data::getM3FUB(void)			{ return m_fM3FUB; }
void CTransaction_trakt_data::setM3FUB(double v)		{ m_fM3FUB = v; }
double CTransaction_trakt_data::getM3UB(void)				{ return m_fM3UB; }
void CTransaction_trakt_data::setM3UB(double v)			{ m_fM3UB = v; }
double CTransaction_trakt_data::getAvgM3SK(void)		{ return m_fAvgM3SK; }
void CTransaction_trakt_data::setAvgM3SK(double v)	{ m_fAvgM3SK = v; }
double CTransaction_trakt_data::getAvgM3FUB(void)		{ return m_fAvgM3FUB; }
void CTransaction_trakt_data::setAvgM3FUB(double v)	{ m_fAvgM3FUB = v; }
double CTransaction_trakt_data::getAvgM3UB(void)		{ return m_fAvgM3UB; }
void CTransaction_trakt_data::setAvgM3UB(double v)	{ m_fAvgM3UB = v; }
double CTransaction_trakt_data::getGrot(void)				{ return m_fGrot; }
void CTransaction_trakt_data::setGrot(double v)			{ m_fGrot = v; }
CString CTransaction_trakt_data::getCreated(void)		{ return m_sCreated; }



////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_trans

// Default constructor
CTransaction_trakt_trans::CTransaction_trakt_trans(void)
{
	m_nTTransDataID	= -1;
	m_nTTransTraktID	= -1;
	m_nTTransFromID	= -1;
	m_nTTransToID	= -1;
	m_nSpeiceID	= -1;
	m_nTTransDataType	= 0;
	m_sFromName	= "";
	m_sToName	= "";
	m_sSpecieName = "";
	m_fM3FUB = 0.0;
	m_fPercent = 0.0;
	m_sCreated = "";
	m_fM3Trans = 0.0;
}

CTransaction_trakt_trans::CTransaction_trakt_trans(int trans_data_id,
																									 int trans_trakt_id,
																									 int trans_from_id,
																									 int trans_to_id,
																									 int trans_spc_id,
																									 int trans_data_type,
																									 LPCTSTR from_name,
																									 LPCTSTR to_name,
																									 LPCTSTR spc_name,
																									 double m3sk,
																									 double percent,
																									 double m3trans,
																									 LPCTSTR created)
{
	m_nTTransDataID	= trans_data_id;
	m_nTTransTraktID	= trans_trakt_id;
	m_nTTransFromID	= trans_from_id;
	m_nTTransToID	= trans_to_id;
	m_nSpeiceID	= trans_spc_id;
	m_nTTransDataType	= trans_data_type;
	m_sFromName	= from_name;
	m_sToName	= to_name;
	m_sSpecieName = spc_name;
	m_fM3FUB = m3sk;
	m_fPercent = percent;
	m_fM3Trans = m3trans;
	m_sCreated = created;
}

// Copy constructor
CTransaction_trakt_trans::CTransaction_trakt_trans(const CTransaction_trakt_trans &v)
{
	*this = v;
}

int CTransaction_trakt_trans::getTTransDataID(void)		{ return m_nTTransDataID; }
int CTransaction_trakt_trans::getTTransTraktID(void)	{ return m_nTTransTraktID; }
int CTransaction_trakt_trans::getTTransFromID(void)		{ return m_nTTransFromID; }
int CTransaction_trakt_trans::getTTransToID(void)			{ return m_nTTransToID; }
int CTransaction_trakt_trans::getSpecieID(void)				{ return m_nSpeiceID; }
int CTransaction_trakt_trans::getTTransDataType(void)	{ return m_nTTransDataType; }
CString CTransaction_trakt_trans::getFromName(void)		{ return m_sFromName; }
CString CTransaction_trakt_trans::getToName(void)			{ return m_sToName; }
CString CTransaction_trakt_trans::getSpecieName(void)	{ return m_sSpecieName; }
double CTransaction_trakt_trans::getM3FUB(void)				{ return m_fM3FUB; }
void CTransaction_trakt_trans::setM3FUB(double v)			{ m_fM3FUB = v; }
double CTransaction_trakt_trans::getPercent(void)			{ return m_fPercent; }
void CTransaction_trakt_trans::setPercent(double v)		{ m_fPercent = v; }
double CTransaction_trakt_trans::getM3Trans(void)			{ return m_fM3Trans; }
void CTransaction_trakt_trans::setM3Trans(double v)		{ m_fM3Trans = v; }
CString CTransaction_trakt_trans::getCreated(void)		{ return m_sCreated; }


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_ass

// Default constructor
CTransaction_trakt_ass::CTransaction_trakt_ass(void)
{
	m_nTAssID = -1;
	m_nTAssTraktID = -1;
	m_nTAssTraktDataID = -1;
	m_nTAssTraktDataType = 0;
	m_sTAssName = _T("");
	m_fPriceM3FUB	= 0.0;
	m_fPriceM3TO = 0.0;
	m_fM3FUB	= 0.0;
	m_fM3TO	= 0.0;
	m_fValueM3FUB	= 0.0;
	m_fValueM3TO = 0.0;
	m_sCreated = _T("");
	m_fKrPerM3 = 0.0;
}

CTransaction_trakt_ass::CTransaction_trakt_ass(int tass_assort_id,int tass_trakt_id,int tass_data_id,int tass_data_type,
																							 LPCTSTR tass_name,
																							 double tass_price_m3fub,double tass_price_m3to,
																							 double tass_m3fub,double tass_m3to,
																							 double tass_value_m3fub,double tass_value_m3to,
																							 LPCTSTR created,double kr_m3)
{
	m_nTAssID = tass_assort_id;
	m_nTAssTraktID = tass_trakt_id;
	m_nTAssTraktDataID = tass_data_id;
	m_nTAssTraktDataType = tass_data_type;
	m_sTAssName = (tass_name);
	m_fPriceM3FUB	= tass_price_m3fub;
	m_fPriceM3TO = tass_price_m3to;
	m_fM3FUB	= tass_m3fub;
	m_fM3TO	= tass_m3to;
	m_fValueM3FUB	= tass_value_m3fub;
	m_fValueM3TO = tass_value_m3to;
	m_sCreated = (created);
	m_fKrPerM3 = kr_m3;

}

// Copy constructor
CTransaction_trakt_ass::CTransaction_trakt_ass(const CTransaction_trakt_ass &v)
{
	*this = v;
}

int CTransaction_trakt_ass::getTAssID(void)	{ return m_nTAssID; }
void CTransaction_trakt_ass::setTAssID(int v)	{m_nTAssID = v; }

int CTransaction_trakt_ass::getTAssTraktID(void)	{ return m_nTAssTraktID; }
void CTransaction_trakt_ass::setTAssTraktID(int v)	{ m_nTAssTraktID = v; }

int CTransaction_trakt_ass::getTAssTraktDataID(void)	{ return m_nTAssTraktDataID; }
void CTransaction_trakt_ass::setTAssTraktDataID(int v)	{ m_nTAssTraktDataID = v; }

int CTransaction_trakt_ass::getTAssTraktDataType(void)	{ return m_nTAssTraktDataType; }
void CTransaction_trakt_ass::setTAssTraktDataType(int v)	{ m_nTAssTraktDataType = v; }

CString CTransaction_trakt_ass::getTAssName(void)		{ return m_sTAssName; }

double CTransaction_trakt_ass::getPriceM3FUB(void)	{ return m_fPriceM3FUB; } 
void CTransaction_trakt_ass::setPriceM3FUB(double v)	{ m_fPriceM3FUB = v; } 

double CTransaction_trakt_ass::getPriceM3TO(void)		{ return m_fPriceM3TO; }
void CTransaction_trakt_ass::setPriceM3TO(double v)	{ m_fPriceM3TO = v; }

double CTransaction_trakt_ass::getM3FUB(void)				{ return m_fM3FUB; } 
void CTransaction_trakt_ass::setM3FUB(double v)			{ m_fM3FUB = v; } 

double CTransaction_trakt_ass::getM3TO(void)				{ return m_fM3TO; }
void CTransaction_trakt_ass::setM3TO(double v)			{ m_fM3TO = v; }

double CTransaction_trakt_ass::getValueM3FUB(void)		{ return m_fValueM3FUB; } 
void CTransaction_trakt_ass::setValueM3FUB(double v)	{ m_fValueM3FUB = v; } 

double CTransaction_trakt_ass::getValueM3TO(void)			{ return m_fValueM3TO; }
void CTransaction_trakt_ass::setValueM3TO(double v)		{ m_fValueM3TO = v; }

CString CTransaction_trakt_ass::getCreated(void)		{ return m_sCreated; }

double CTransaction_trakt_ass::getKrPerM3(void)				{ return m_fKrPerM3; }
void CTransaction_trakt_ass::setKrPerM3(double v)			{ m_fKrPerM3 = v; }

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_misc_data
CTransaction_trakt_misc_data::CTransaction_trakt_misc_data(void)
{
	m_nTSetTraktID		= -1;
	m_sName						= _T("");
	m_nTypeOf					= -1;
	m_sXMLPricelist		= _T("");
	m_sCostsName			= _T("");
	m_nCostsTypeOf		= -1;
	m_sXMLCosts				= _T("");
	m_fDiamClass			= 0.0;
	m_sCreated				= _T("");
}

CTransaction_trakt_misc_data::CTransaction_trakt_misc_data(int tset_trakt_id,
																													 LPCTSTR name,
																													 int type_of,
																													 LPCTSTR xml_prl,
																													 LPCTSTR costs_name,
																													 int costs_type_of,																												 
																													 LPCTSTR xml_costs,
																													 double diam_class,
																													 LPCTSTR created)
{
	m_nTSetTraktID	= tset_trakt_id;
	m_sName						= (name);
	m_nTypeOf					= type_of;
	m_sXMLPricelist		= (xml_prl);
	m_sCostsName			= (costs_name);
	m_nCostsTypeOf		= costs_type_of;
	m_sXMLCosts				= (xml_costs);
	m_fDiamClass			= diam_class;
	m_sCreated				= created;
}

CTransaction_trakt_misc_data::CTransaction_trakt_misc_data(const CTransaction_trakt_misc_data &c)
{
	*this = c;
}

int CTransaction_trakt_misc_data::getTSetTraktID(void)			{	return m_nTSetTraktID;}
CString CTransaction_trakt_misc_data::getName(void)					{	return m_sName;}
int CTransaction_trakt_misc_data::getTypeOf(void)						{	return m_nTypeOf;}
CString CTransaction_trakt_misc_data::getXMLPricelist(void)	{	return m_sXMLPricelist;}
CString CTransaction_trakt_misc_data::getCostsName(void)		{	return m_sCostsName;}
int CTransaction_trakt_misc_data::getCostsTypeOf(void)			{	return m_nCostsTypeOf;}
CString CTransaction_trakt_misc_data::getXMLCosts(void)			{	return m_sXMLCosts;}
double CTransaction_trakt_misc_data::getDiamClass(void)			{	return m_fDiamClass;}
CString CTransaction_trakt_misc_data::getCreated(void)			{	return m_sCreated;}

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_set_spc
CTransaction_trakt_set_spc::CTransaction_trakt_set_spc(void)
{
	m_nTSetspcID					= -1;
	m_nTSetspcDataID			= -1;
	m_nTSetspcTraktID			= -1;
	m_nTSetspcDataType		= 0;
	m_nSpcID							= -1;
	m_nHgtFuncID					= -99;	// Dummy value
	m_nHgtFuncSpcID				= -99;	// Dummy value			
	m_nHgtFuncIndex				= -99;	// Dummy value
	m_nVolFuncID					= -99;	// Dummy value
	m_nVolFuncSpcID				= -99;	// Dummy value
	m_nVolFuncIndex				= -99;	// Dummy value
	m_nBarkFuncID					= -99;	// Dummy value
	m_nBarkFuncSpcID			= -99;	// Dummy value
	m_nBarkFuncIndex			= -99;	// Dummy value
	m_nVolUBFuncID				= -99;	// Dummy value
	m_nVolUBFuncSpcID			= -99;	// Dummy value
	m_nVolUBFuncIndex			= -99;	// Dummy value
	m_fM3SkToM3Ub					= 0.0;
	m_fM3SkToM3Fub				= 0.0;
	m_fM3FubToM3To				= 0.0;
	m_fDiamClass					= 0.0;
	m_nSelQDescIndex			= -99;
	m_sExtraInfo					= _T("");
	m_nTranspDist1				= 0;
	m_nTranspDist2				= 0;
	m_sCreated						= _T("");
#ifdef VER120
	m_sQDescName					= _T("");
	m_nGrotFuncID					= -99;					// ID for GROT function selected
	m_fGrotPercent				= 0.0;					// Uttagsprocent GROT
	m_fGrotPrice					= 0.0;
	m_fGrotCost						= 0.0;
	m_fDefaultH25					= 0.0;
#endif

}

CTransaction_trakt_set_spc::CTransaction_trakt_set_spc(int setspc_id,
																											 int setspc_data_id,
																											 int setspc_data_trakt_id,
																											 int setspc_data_type,
																											 int spc_id,
																											 int hgt_func_id,
																											 int hgt_func_spc_id,
																											 int hgt_func_idx,
																											 int vol_func_id,
																											 int vol_func_spc_id,
																											 int vol_func_idx,
																											 int bark_func_id,
																											 int bark_func_spc_id,
																											 int bark_func_idx,
																											 int vol_ub_func_id,
																											 int vol_ub_func_spc_id,
																											 int vol_ub_func_idx,
																											 double m3sk_m3ub,
																											 double m3sk_m3fub,
																											 double m3fub_m3to,
																											 double diam_class,
																											 int sel_qdesc_index,
																											 LPCTSTR extra_info,
																											 int transp_dist1,
																											 int transp_dist2,
#ifdef VER120
																											 LPCTSTR create_date,
																											 LPCTSTR qdesc_name,
													 														 int grot_id,
																											 double grot_percent,
																											 double grot_price,
																											 double grot_cost,
																											 double default_h25,
																											 double default_greencrown)
#else
																											 LPCTSTR create_date)
#endif
{
	m_nTSetspcID						= setspc_id;
	m_nTSetspcDataID				= setspc_data_id;
	m_nTSetspcTraktID				= setspc_data_trakt_id;
	m_nTSetspcDataType			= setspc_data_type;
	m_nSpcID								= spc_id;
	m_nHgtFuncID						= hgt_func_id;						
	m_nHgtFuncSpcID					= hgt_func_spc_id;				
	m_nHgtFuncIndex					= hgt_func_idx;				
	m_nVolFuncID						= vol_func_id;						
	m_nVolFuncSpcID					= vol_func_spc_id;				
	m_nVolFuncIndex					= vol_func_idx;				
	m_nBarkFuncID						= bark_func_id;					
	m_nBarkFuncSpcID				= bark_func_spc_id;				
	m_nBarkFuncIndex				= bark_func_idx;				
	m_nVolUBFuncID					= vol_ub_func_id;						
	m_nVolUBFuncSpcID				= vol_ub_func_spc_id;				
	m_nVolUBFuncIndex				= vol_ub_func_idx;				
	m_fM3SkToM3Ub						= m3sk_m3ub;				
	m_fM3SkToM3Fub					= m3sk_m3fub;				
	m_fM3FubToM3To					= m3fub_m3to;				
	m_fDiamClass						= diam_class;
	m_nSelQDescIndex				= sel_qdesc_index;
	m_sExtraInfo						= extra_info;
	m_nTranspDist1					= transp_dist1;
	m_nTranspDist2					= transp_dist2;
	m_sCreated							= create_date;
#ifdef VER120
	m_sQDescName						= qdesc_name;
	m_nGrotFuncID						= grot_id;					// ID for GROT function selected
	m_fGrotPercent					= grot_percent;			// Uttagsprocent
	m_fGrotPrice						= grot_price;				// Pris (kr/ton)
	m_fGrotCost							= grot_cost;				// Kostnad (kr/ton)
	m_fDefaultH25						= default_h25;
	m_fDefaultGreenCrown	= default_greencrown;	// #4555: Standard gr�nkroneandel
#endif

}

CTransaction_trakt_set_spc::CTransaction_trakt_set_spc(const CTransaction_trakt_set_spc &c)
{
	*this = c;
}

int CTransaction_trakt_set_spc::getTSetspcID(void)							{ return m_nTSetspcID; }
int CTransaction_trakt_set_spc::getTSetspcDataID(void)					{ return m_nTSetspcDataID; }
int CTransaction_trakt_set_spc::getTSetspcTraktID(void)					{ return m_nTSetspcTraktID; }
int CTransaction_trakt_set_spc::getTSetspcDataType(void)				{ return m_nTSetspcDataType; }

int CTransaction_trakt_set_spc::getSpcID(void)									{	return m_nSpcID;}
int CTransaction_trakt_set_spc::getHgtFuncID(void)							{ return m_nHgtFuncID; }
int CTransaction_trakt_set_spc::getHgtFuncSpcID(void)						{ return m_nHgtFuncSpcID; }
int CTransaction_trakt_set_spc::getHgtFuncIndex(void)						{ return m_nHgtFuncIndex; }

int CTransaction_trakt_set_spc::getVolFuncID(void)							{ return m_nVolFuncID; }
int CTransaction_trakt_set_spc::getVolFuncSpcID(void)						{ return m_nVolFuncSpcID; }
int CTransaction_trakt_set_spc::getVolFuncIndex(void)						{ return m_nVolFuncIndex; }

int CTransaction_trakt_set_spc::getBarkFuncID(void)							{ return m_nBarkFuncID; }
int CTransaction_trakt_set_spc::getBarkFuncSpcID(void)					{ return m_nBarkFuncSpcID; }
int CTransaction_trakt_set_spc::getBarkFuncIndex(void)					{ return m_nBarkFuncIndex; }

int CTransaction_trakt_set_spc::getVolUBFuncID(void)						{ return m_nVolUBFuncID; }
int CTransaction_trakt_set_spc::getVolUBFuncSpcID(void)					{ return m_nVolUBFuncSpcID; }
int CTransaction_trakt_set_spc::getVolUBFuncIndex(void)					{ return m_nVolUBFuncIndex; }

double CTransaction_trakt_set_spc::getM3SkToM3Ub(void)					{ return m_fM3SkToM3Ub; }
double CTransaction_trakt_set_spc::getM3SkToM3Fub(void)					{ return m_fM3SkToM3Fub; }
double CTransaction_trakt_set_spc::getM3FubToM3To(void)					{ return m_fM3FubToM3To; }

double CTransaction_trakt_set_spc::getDiamClass(void)						{ return m_fDiamClass; }

int CTransaction_trakt_set_spc::getSelQDescIndex(void)					{ return m_nSelQDescIndex; }

CString CTransaction_trakt_set_spc::getExtraInfo(void)					{ return m_sExtraInfo; }

int CTransaction_trakt_set_spc::getTranspDist1(void)						{ return m_nTranspDist1; }
int CTransaction_trakt_set_spc::getTranspDist2(void)						{ return m_nTranspDist2; }

CString CTransaction_trakt_set_spc::getCreated(void)						{ return m_sCreated; }
#ifdef VER120
CString CTransaction_trakt_set_spc::getQDescName(void)					{	return m_sQDescName;}
int CTransaction_trakt_set_spc::getGrotFuncID(void)							{ return m_nGrotFuncID; }
double CTransaction_trakt_set_spc::getGrotPercent(void)					{ return m_fGrotPercent; }
double CTransaction_trakt_set_spc::getGrotPrice(void)						{ return m_fGrotPrice; }
double CTransaction_trakt_set_spc::getGrotCost(void)						{ return m_fGrotCost; }
double CTransaction_trakt_set_spc::getDefaultH25(void)						{ return m_fDefaultH25; }
double CTransaction_trakt_set_spc::getDefaultGreenCrown(void)				{ return m_fDefaultGreenCrown; }
#endif


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_plot
CTransaction_plot::CTransaction_plot(void)
{
	m_nPlotID	= -1;
	m_nTraktID = -1;
	m_sPlotName = _T("");
	m_nPlotType = -1;
	m_fRadius = 0.0;
	m_fLength1 = 0.0;
	m_fWidth1 = 0.0;
	m_fArea = 0.0;
	m_sCoord = _T("");
	m_nNumOfTrees = 0;
	m_sCreated	= _T("");
}

CTransaction_plot::CTransaction_plot(int plot_id,
										int trakt_id,
										LPCTSTR plot_name,
										int plot_type,
										double radius,
										double length1,
										double width1,
										double area,
										LPCTSTR coord,
										int numof_trees,
										LPCTSTR created)
{
	m_nPlotID	= plot_id;
	m_nTraktID = trakt_id;
	m_sPlotName = (plot_name);
	m_nPlotType = plot_type;
	m_fRadius = radius;
	m_fLength1 = length1;
	m_fWidth1 = width1;
	m_fArea = area;
	m_sCoord = (coord);
	m_nNumOfTrees = numof_trees;
	m_sCreated = created;
}

CTransaction_plot::CTransaction_plot(const CTransaction_plot &c)
{
	*this = c;
}

int CTransaction_plot::getPlotID(void)				{ return m_nPlotID; }
int CTransaction_plot::getTraktID(void)				{ return m_nTraktID; }

CString CTransaction_plot::getPlotName(void)	{ return m_sPlotName; }
void CTransaction_plot::setPlotName(LPCTSTR v)	{ m_sPlotName = v; }

int CTransaction_plot::getPlotType(void)			{ return m_nPlotType; }
void CTransaction_plot::setPlotType(int v)		{ m_nPlotType = v; }

double CTransaction_plot::getRadius(void)			{ return m_fRadius; }
void CTransaction_plot::setRadius(double v)		{ m_fRadius = v; }

double CTransaction_plot::getLength1(void)		{ return m_fLength1; }
void CTransaction_plot::setLength1(double v)	{ m_fLength1 = v; }

double CTransaction_plot::getWidth1(void)			{ return m_fWidth1; }
void CTransaction_plot::setWidth1(double v)		{ m_fWidth1 = v; }

double CTransaction_plot::getArea(void)				{ return m_fArea; }
void CTransaction_plot::setArea(double v)			{ m_fArea = v; }

CString CTransaction_plot::getCoord(void)			{ return m_sCoord; }
void CTransaction_plot::setCoord(LPCTSTR v)		{ m_sCoord = v; }

int CTransaction_plot::getNumOfTrees(void)		{ return m_nNumOfTrees; }
void CTransaction_plot::setNumOfTrees(int v)	{ m_nNumOfTrees = v; }

CString CTransaction_plot::getCreated(void)		{ return m_sCreated; }

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_sample_tree

CTransaction_sample_tree::CTransaction_sample_tree(void)
{
	m_nTreeID	= -1;
	m_nTraktID	= -1;
	m_nPlotID	= -1;
	m_nSpcID	= -1;
	m_sSpcName = _T("");
	m_fDbh	= 0.0;
	m_fHgt	= 0.0;
	m_fGCrownPerc	= 0.0;
	m_fBarkThick	= 0.0;
	m_fGROT	= 0.0;
	m_nAge	= -1;
	m_nGrowth	= -1;
	m_fDCLS_from = 0.0;
	m_fDCLS_to = 0.0;
	m_fM3Sk	= 0.0;
	m_fM3Fub	= 0.0;
	m_fM3Ub	= 0.0;
	m_nTreeType = -1;
	m_sBonitet = _T("");
	m_sCreated = _T("");
	m_sCoord = _T("");
	m_nDistCable = 0;
	m_nCategory = 0;
	m_nTopCut = 0;
}

CTransaction_sample_tree::	CTransaction_sample_tree(int tree_id,int trakt_id,
																			int plot_id,int spc_id,LPCTSTR spc_name,
																			double dbh,double hgt,double gcrown_perc,double bark_thick,
																			double grot,int age,int growth,double dcls_from,double dcls_to,
																			double m3sk,double m3fub,double m3ub,int tree_type,LPCTSTR bonitet,LPCTSTR created,
																			LPCTSTR coord, int distCable, int category, int topcut)
{
	m_nTreeID	= tree_id;
	m_nTraktID	= trakt_id;
	m_nPlotID	= plot_id;
	m_nSpcID	= spc_id;
	m_sSpcName = (spc_name);
	m_fDbh	= dbh;
	m_fHgt	= hgt;
	m_fGCrownPerc	= gcrown_perc;
	m_fBarkThick	= bark_thick;
	m_fGROT	= grot;
	m_nAge	= age;
	m_nGrowth	= growth;
	m_fDCLS_from = dcls_from;
	m_fDCLS_to = dcls_to;
	m_fM3Sk	= m3sk;
	m_fM3Fub	= m3fub;
	m_fM3Ub	= m3ub;
	m_nTreeType = tree_type;
	m_sBonitet = (bonitet);
	m_sCreated = created;
	m_sCoord = coord;
	m_nDistCable = distCable;
	m_nCategory = category;
	m_nTopCut = topcut;
}

// Copy constructor; 060320 p�d
CTransaction_sample_tree::CTransaction_sample_tree(const CTransaction_sample_tree &c)
{
	*this = c;
}

int CTransaction_sample_tree::getTreeID(void)					{ return m_nTreeID; }
int CTransaction_sample_tree::getTraktID(void)					{ return m_nTraktID; }

int CTransaction_sample_tree::getPlotID(void)					{ return m_nPlotID; }
int CTransaction_sample_tree::getSpcID(void)						{ return m_nSpcID; }
CString CTransaction_sample_tree::getSpcName(void)			{ return m_sSpcName; }
double CTransaction_sample_tree::getDBH(void)					{ return m_fDbh; }
void CTransaction_sample_tree::setDBH(double v)				{ m_fDbh = v; }
double CTransaction_sample_tree::getHgt(void)					{ return m_fHgt; }
void CTransaction_sample_tree::setHgt(double v)				{ m_fHgt = v; }
double CTransaction_sample_tree::getGCrownPerc(void)		{ return m_fGCrownPerc; }
double CTransaction_sample_tree::getBarkThick(void)		{ return m_fBarkThick; }
void CTransaction_sample_tree::setBarkThick(double v)	{ m_fBarkThick = v; }
double CTransaction_sample_tree::getGROT(void)					{ return m_fGROT; }
void CTransaction_sample_tree::setGROT(double v)				{ m_fGROT = v; }
int CTransaction_sample_tree::getGrowth(void)						{ return m_nGrowth; }
int CTransaction_sample_tree::getAge(void)							{ return m_nAge; }
double CTransaction_sample_tree::getDCLS_from(void)			{ return m_fDCLS_from; }
void CTransaction_sample_tree::setDCLS_from(double v)		{ m_fDCLS_from = v; }
double CTransaction_sample_tree::getDCLS_to(void)				{ return m_fDCLS_to; }
void CTransaction_sample_tree::setDCLS_to(double v)			{ m_fDCLS_to = v; }
double CTransaction_sample_tree::getM3Sk(void)					{ return m_fM3Sk; }
void CTransaction_sample_tree::setM3Sk(double v)				{ m_fM3Sk = v; }
double CTransaction_sample_tree::getM3Fub(void)					{ return m_fM3Fub; }
void CTransaction_sample_tree::setM3Fub(double v)				{ m_fM3Fub = v; }
double CTransaction_sample_tree::getM3Ub(void)					{ return m_fM3Ub; }
void CTransaction_sample_tree::setM3Ub(double v)				{ m_fM3Ub = v; }
int CTransaction_sample_tree::getTreeType(void)					{ return m_nTreeType; }
CString CTransaction_sample_tree::getBonitet(void)			{ return m_sBonitet; }
CString CTransaction_sample_tree::getCoord(void)			{ return m_sCoord; }
CString CTransaction_sample_tree::getCreated(void)			{ return m_sCreated; }
int CTransaction_sample_tree::getDistCable(void)			{ return m_nDistCable; }
int CTransaction_sample_tree::getCategory(void)			{ return m_nCategory; }
int CTransaction_sample_tree::getTopCut(void)			{ return m_nTopCut; }
void CTransaction_sample_tree::setTopCut(int topcut)			{ m_nTopCut = topcut; }
void CTransaction_sample_tree::setCoord(CString coord)			{ m_sCoord = coord; }


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_dcls_tree

CTransaction_dcls_tree::CTransaction_dcls_tree(void)
{
	m_nTreeID	= -1;
	m_nTraktID	= -1;
	m_nPlotID	= -1;
	m_nSpcID	= -1;
	m_sSpcName = _T("");
	m_fDCLS_from = 0.0;
	m_fDCLS_to = 0.0;
	m_fHgt	= 0.0;
	m_nNumOf	= 0;
	m_fGCrownPerc	= 0.0;
	m_fBarkThick	= 0.0;
	m_fM3Sk	= 0.0;
	m_fM3Fub	= 0.0;
	m_fM3Ub	= 0.0;
	m_fGROT	= 0.0;
	m_nAge	= -1;
	m_nGrowth	= -1;
	m_nNumOfRandTrees = 0;
	m_sCreated = _T("");
}

CTransaction_dcls_tree::	CTransaction_dcls_tree(int tree_id,int trakt_id,
																			int plot_id,int spc_id,LPCTSTR spc_name,
																			double dcls_from,double dcls_to,double hgt,
																			long num_of,double gcrown_perc,double bark_thick,
																			double m3sk,double m3fub,double m3ub,double grot,
																			int age,int growth,int rand_trees,LPCTSTR created)
{
	m_nTreeID	= tree_id;
	m_nTraktID	= trakt_id;
	m_nPlotID	= plot_id;
	m_nSpcID	= spc_id;
	m_sSpcName = (spc_name);
	m_fDCLS_from = dcls_from;
	m_fDCLS_to = dcls_to;
	m_fHgt	= hgt;
	m_nNumOf = num_of;
	m_fGCrownPerc	= gcrown_perc;
	m_fBarkThick	= bark_thick;
	m_fM3Sk	= m3sk;
	m_fM3Fub	= m3fub;
	m_fM3Ub	= m3ub;
	m_fGROT	= grot;
	m_nAge	= age;
	m_nGrowth	= growth;
	m_nNumOfRandTrees = rand_trees;
	m_sCreated = created;
}

// Copy constructor; 060320 p�d
CTransaction_dcls_tree::CTransaction_dcls_tree(const CTransaction_dcls_tree &c)
{
	*this = c;
}

int CTransaction_dcls_tree::getTreeID(void)					{ return m_nTreeID; }
int CTransaction_dcls_tree::getTraktID(void)					{ return m_nTraktID; }

int CTransaction_dcls_tree::getPlotID(void)					{ return m_nPlotID; }
int CTransaction_dcls_tree::getSpcID(void)						{ return m_nSpcID; }
CString CTransaction_dcls_tree::getSpcName(void)			{ return m_sSpcName; }

double CTransaction_dcls_tree::getDCLS_from(void)			{ return m_fDCLS_from; }
void CTransaction_dcls_tree::setDCLS_from(double v)		{ m_fDCLS_from = v; }

double CTransaction_dcls_tree::getDCLS_to(void)				{ return m_fDCLS_to; }
void CTransaction_dcls_tree::setDCLS_to(double v)			{ m_fDCLS_to = v; }

double CTransaction_dcls_tree::getHgt(void)					{ return m_fHgt; }
void CTransaction_dcls_tree::setHgt(double v)				{ m_fHgt = v; }

long CTransaction_dcls_tree::getNumOf(void)						{ return m_nNumOf; }
void CTransaction_dcls_tree::setNumOf(int v)					{ m_nNumOf = v; }

double CTransaction_dcls_tree::getGCrownPerc(void)		{ return m_fGCrownPerc; }
void CTransaction_dcls_tree::setGCrownPerc(double v)	{ m_fGCrownPerc = v; }

double CTransaction_dcls_tree::getBarkThick(void)		{ return m_fBarkThick; }
void CTransaction_dcls_tree::setBarkThick(double v)	{ m_fBarkThick = v; }

double CTransaction_dcls_tree::getM3sk(void)					{ return m_fM3Sk; }
void CTransaction_dcls_tree::setM3sk(double v)				{ m_fM3Sk = v; }

double CTransaction_dcls_tree::getM3fub(void)				{ return m_fM3Fub; }
void CTransaction_dcls_tree::setM3fub(double v)			{ m_fM3Fub = v; }

double CTransaction_dcls_tree::getM3ub(void)				{ return m_fM3Ub; }
void CTransaction_dcls_tree::setM3ub(double v)			{ m_fM3Ub = v; }

double CTransaction_dcls_tree::getGROT(void)					{ return m_fGROT; }
void CTransaction_dcls_tree::setGROT(double v)				{ m_fGROT = v; }

int CTransaction_dcls_tree::getAge(void)						{ return m_nAge; }
int CTransaction_dcls_tree::getGrowth(void)					{ return m_nGrowth; }

int CTransaction_dcls_tree::getNumOfRandTrees(void)	{ return m_nNumOfRandTrees; }
void CTransaction_dcls_tree::setNumOfRandTrees(int val)	{ m_nNumOfRandTrees = val; }

CString CTransaction_dcls_tree::getCreated(void)		{ return m_sCreated; }

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_tree_assort

CTransaction_tree_assort::CTransaction_tree_assort(void)
{
	m_nTraktID	= -1;
	m_nTreeID	= -1;
	m_fDCLS = 0.0;
	m_nSpcID = -1;
	m_sSpcName = _T("");
	m_sAssortName = _T("");
	m_fExchVolume = 0.0;
	m_fExchPrice = 0.0;
	m_nPriceIn	= -1;
}

CTransaction_tree_assort::CTransaction_tree_assort(int trakt_id,
																									 int tree_id,
																									 double dcls,
																									 int spc_id,
																									 LPCTSTR spc_name,
																									 LPCTSTR assort_name,
																									 double exch_volume,
																									 double exch_price,
																									 int price_in)
{
	m_nTraktID	= trakt_id;
	m_nTreeID	= tree_id;
	m_fDCLS = dcls;
	m_nSpcID = spc_id;
	m_sSpcName = (spc_name);
	m_sAssortName = (assort_name);
	m_fExchVolume = exch_volume;
	m_fExchPrice = exch_price;
	m_nPriceIn	= price_in;
}

// Copy constructor; 060320 p�d
CTransaction_tree_assort::CTransaction_tree_assort(const CTransaction_tree_assort &c)
{
	*this = c;
}

int CTransaction_tree_assort::getTraktID(void)					{ return m_nTraktID; }
int CTransaction_tree_assort::getTreeID(void)						{ return m_nTreeID; }
double CTransaction_tree_assort::getDCLS(void)					{ return m_fDCLS; }
int CTransaction_tree_assort::getSpcID(void)						{ return m_nSpcID; }
CString CTransaction_tree_assort::getSpcName(void)			{ return m_sSpcName; }
CString CTransaction_tree_assort::getAssortName(void)		{ return m_sAssortName; }
double CTransaction_tree_assort::getExchVolume(void)		{ return m_fExchVolume; }
void CTransaction_tree_assort::setExchVolume(double v)	{ m_fExchVolume = v; }
double CTransaction_tree_assort::getExchPrice(void)			{ return m_fExchPrice; }
int CTransaction_tree_assort::getPriceIn(void)					{ return m_nPriceIn; }

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_property
CTransaction_property::CTransaction_property(void)
{
	m_nID										= -1;
	m_sCountyCode						= _T("");
	m_sMunicipalCode				= _T("");
	m_sParishCode						= _T("");
	m_sCountyName						= _T("");
	m_sMunicipalName				= _T("");
	m_sParishName						= _T("");
	m_sPropertyNum					= _T("");
	m_sPropertyName					= _T("");
	m_sBlock								= _T("");
	m_sUnit									= _T("");
	double m_fAreal					= 0.0;
	double m_fArealMeasured	= 0.0;
	m_sCreatedBy						= _T("");
	m_sCreated							= _T("");
	m_sObjID								= _T("");
	m_sPropFullName					= _T("");
	m_sMottagarref					= _T("");
	m_sCoord = _T("");
}

CTransaction_property::CTransaction_property(int id,
																						 LPCTSTR county_code,
																						 LPCTSTR municipal_code,
																						 LPCTSTR parish_code,
																						 LPCTSTR county_name,
																						 LPCTSTR municipal_name,
																						 LPCTSTR parish_name,
																						 LPCTSTR prop_num,
																						 LPCTSTR prop_name,
																						 LPCTSTR block,
																						 LPCTSTR unit,
																						 double areal,
																						 double areal_measured,
																						 LPCTSTR created_by,
																						 LPCTSTR created,
																						 LPCTSTR obj_id,
																						 LPCTSTR prop_full_name,
																						 LPCTSTR mot_ref,
																						 LPCTSTR sCoord)
{
	m_nID										= id;
	m_sCountyCode						= (county_code);
	m_sMunicipalCode				= (municipal_code);
	m_sParishCode						= (parish_code);
	m_sCountyName						= (county_name);
	m_sMunicipalName				= (municipal_name);
	m_sParishName						= (parish_name);
	m_sPropertyNum					= (prop_num);
	m_sPropertyName					= (prop_name);
	m_sBlock								= (block);
	m_sUnit									= (unit);
	m_fAreal								= areal;
	m_fArealMeasured				= areal_measured;
	m_sCreatedBy						= (created_by);
	m_sCreated							= (created);
	m_sObjID								= (obj_id);
	m_sPropFullName					= prop_full_name;
	m_sMottagarref					= mot_ref;
	m_sCoord = sCoord;
}

CTransaction_property::CTransaction_property(const CTransaction_property &c)
{
	*this = c;
}

int CTransaction_property::getID(void)									{	return m_nID;}
CString CTransaction_property::getCountyCode(void)			{	return m_sCountyCode;}
CString CTransaction_property::getMunicipalCode(void)		{	return m_sMunicipalCode;}
CString CTransaction_property::getParishCode(void)			{	return m_sParishCode;}
CString CTransaction_property::getCountyName(void)			{	return m_sCountyName;}
CString CTransaction_property::getMunicipalName(void)		{	return m_sMunicipalName;}
CString CTransaction_property::getParishName(void)			{	return m_sParishName;}
CString CTransaction_property::getPropertyNum(void)			{	return m_sPropertyNum;}
CString CTransaction_property::getPropertyName(void)		{	return m_sPropertyName;}
CString CTransaction_property::getBlock(void)						{	return m_sBlock;}
CString CTransaction_property::getUnit(void)						{	return m_sUnit;}
double CTransaction_property::getAreal(void)						{	return m_fAreal;}
double CTransaction_property::getArealMeasured(void)		{	return m_fArealMeasured;}
CString CTransaction_property::getCreatedBy(void)				{	return m_sCreatedBy;}
CString CTransaction_property::getCreated(void)					{	return m_sCreated;}
CString CTransaction_property::getObjectID(void)				{	return m_sObjID;}
CString CTransaction_property::getPropFullName(void)		{	return m_sPropFullName;}
CString CTransaction_property::getMottagarref(void)		  {	return m_sMottagarref;}
CString CTransaction_property::getCoord(void)		  {	return m_sCoord;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_prop_owners
CTransaction_prop_owners::CTransaction_prop_owners(void)
{
	m_nPropID								= -1;
	m_nContactID						= -1;
	m_sShareOff 						= _T("");
	m_nIsContact						= 0;	// Not contact as default; 080609 p�d
	m_sCreated							= _T("");
}

CTransaction_prop_owners::CTransaction_prop_owners(int prop_id,
																									 int contact_id,
																									 LPCTSTR share_off,
																									 int is_contact,
																									 LPCTSTR created)
{
	m_nPropID								= prop_id;
	m_nContactID						= contact_id;
	m_sShareOff 						= (share_off);
	m_nIsContact						= is_contact;
	m_sCreated							= (created);
}

CTransaction_prop_owners::CTransaction_prop_owners(const CTransaction_prop_owners &c)
{
	*this = c;
}

int CTransaction_prop_owners::getPropID(void)							{	return m_nPropID;}
int CTransaction_prop_owners::getContactID(void)					{	return m_nContactID;}
CString CTransaction_prop_owners::getShareOff(void)				{	return m_sShareOff;}
int CTransaction_prop_owners::getIsContact(void)					{	return m_nIsContact;}
CString CTransaction_prop_owners::getCreated(void)				{	return m_sCreated;}


//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_costtempl
CTransaction_costtempl::CTransaction_costtempl(void)
{
	m_nID							= -1;
	m_sTemplateName		= _T("");
	m_nTypeOf					= -1;
	m_sTemplateFile		= _T("");
	m_sTemplateNotes	= _T("");
	m_sCreatedBy			= _T("");
	m_sCreated				= _T("");
}

CTransaction_costtempl::CTransaction_costtempl(int id,LPCTSTR tmpl_name,int type_of,LPCTSTR tmpl_file,LPCTSTR notes,LPCTSTR created_by,LPCTSTR created)
{
	m_nID							= id;
	m_sTemplateName		= (tmpl_name);
	m_nTypeOf					= type_of;
	m_sTemplateFile		= (tmpl_file);
	m_sTemplateNotes	= (notes);
	m_sCreatedBy			= (created_by);
	m_sCreated				= (created);
}

CTransaction_costtempl::CTransaction_costtempl(const CTransaction_costtempl &c)
{
	*this = c;
}

int CTransaction_costtempl::getID(void)								{	return m_nID;}
CString CTransaction_costtempl::getTemplateName(void)	{	return m_sTemplateName;}
int CTransaction_costtempl::getTypeOf(void)						{	return m_nTypeOf;}
CString CTransaction_costtempl::getTemplateFile(void)	{	return m_sTemplateFile;}
CString CTransaction_costtempl::getTemplateNotes(void)	{	return m_sTemplateNotes;}
CString CTransaction_costtempl::getCreatedBy(void)			{	return m_sCreatedBy;}
CString CTransaction_costtempl::getCreated(void)				{	return m_sCreated;}
