#include "StdAfx.h"

#include "xmlliteBase.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// CxmlliteBase; 2009-12-10 P�D

CxmlliteBase::CxmlliteBase(void)
{
	m_pStream = NULL;
	m_pReader = NULL;
}

CxmlliteBase::~CxmlliteBase()
{
	m_sXML.clear();
	m_sXML_original.ReleaseBuffer();

	if (m_pStream) m_pStream = NULL; 
	if (m_pReader) m_pReader = NULL;
}

// Methods for Loading and Saving xml file(s); 060407 p�d

// Public ..........

// Load from file; 091215 p�d
BOOL CxmlliteBase::loadFile(LPCTSTR file)
{
	if( FAILED(SHCreateStreamOnFile(file, STGM_READ, &m_pStream)) )
	{
		return FALSE;
	}
	// Set up xml reader
	if( FAILED(::CreateXmlReader(__uuidof(IXmlReader), reinterpret_cast<void**>(&m_pReader), NULL)) )	
	{	
		return FALSE;	
	}

	if( FAILED(m_pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)) )	
	{	
		return FALSE;	
	}

	if( FAILED(m_pReader->SetInput(m_pStream)) )	
	{	
		return FALSE;	
	}


	return TRUE;
}

// Load from stream; 091215 p�f
BOOL CxmlliteBase::loadStream(LPCTSTR buffer)
{
	// Create a stream
	if (FAILED(CreateStreamOnHGlobal(NULL, TRUE, &m_pStream)))	
	{	
		return FALSE;	
	}

	// Set up xml reader
	if( FAILED(::CreateXmlReader(__uuidof(IXmlReader), reinterpret_cast<void**>(&m_pReader), NULL)) )	
	{	
		return FALSE;	
	}

	if( FAILED(m_pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)) )	
	{	
		return FALSE;	
	}

	if( FAILED(m_pReader->SetInput(m_pStream)) )	
	{	
		return FALSE;	
	}

	// All ok so fare ...

	// Prepare buffer to be used in stream. I.e. convert to ascii and check
	// for UTF-8; 091215 p�d
	// Need to make the string ascii, not unicode; 091215 p�d
	m_sXML_original = buffer;
	if(AfxGetApp() == NULL)	// we are running from a webserver or something?
		m_sXML_original.AppendChar('\n');
	convert_to_ascii(m_sXML_original,m_sXML);
	// Check for encoding in xml-data. Need to change, seams like 
	// the pReader->Read(&nodeType) doesn't work in UTF-8; 091215 p�d
	find_and_replace(m_sXML,"UTF-8","ISO-8859-1");
	find_and_replace(m_sXML,"utf-8","ISO-8859-1");

	streamCopy();
	streamRewind();

	return TRUE;
}

// Private ...........

void CxmlliteBase::convert_to_ascii(LPCTSTR in_str,std::string& out_str)
{
  int count = wcslen(in_str);
  char* c = new char[count + 1];
	wchar_t* pwchr = const_cast<wchar_t*> (&in_str[0]);
  for(int j = 0; j < count; ++j){
     c[j] = static_cast<char> (*pwchr);
     pwchr++;
  } 
  c[count] = '\0';
	out_str = c;
}

std::string& CxmlliteBase::find_and_replace( std::string& tInput, std::string tFind, std::string tReplace ) 
{ 
	size_t uPos = 0; 
	size_t uFindLen = tFind.length(); 
	size_t uReplaceLen = tReplace.length();
	if( uFindLen == 0 )
	{
		return tInput;
	}

	for( ;(uPos = tInput.find( tFind, uPos )) != std::string::npos; )
	{
    tInput.replace( uPos, uFindLen, tReplace );
    uPos += uReplaceLen;
	}
}

// Protected ...............

HRESULT CxmlliteBase::streamRewind(void)
{
  LARGE_INTEGER liBeggining = { 0 };
  HRESULT hrRet = S_OK;

  hrRet = m_pStream->Seek(liBeggining, STREAM_SEEK_SET, NULL);

  return hrRet;
}

HRESULT CxmlliteBase::readerRewind(void)
{
	UINT nLinePos;
	HRESULT hrRetSeek = S_OK;
	HRESULT hrRetSetInput = S_OK;
	// We'll check to see if linepos of
	// the m_pReader GT 0, if so we'll rewind
	// the m_pStream pos and set the m_pReader
	// to the rwinded m_pStream; 091216 p�d
	m_pReader->GetLinePosition(&nLinePos);
	if (nLinePos >= 0)	// HACK! returnerar alltid 0!
	{
		// Rewind stream; 091216 p�d
		LARGE_INTEGER liBeggining = { 0 };
	  hrRetSeek = m_pStream->Seek(liBeggining, STREAM_SEEK_SET, NULL);
		// Set rewindes stream to XML-rerader; 091216 p�d
		hrRetSetInput = m_pReader->SetInput(m_pStream);
	}

	if (hrRetSeek == S_OK && hrRetSetInput == S_OK)
		return S_OK;
	else
		return S_FALSE;
}

HRESULT CxmlliteBase::streamCopy(void)
{
  ULONG ulBytesWritten = 0;
  ULONG ulSize = 0;
  ULARGE_INTEGER uliSize = { 0 };
  HRESULT hrRetTemp = S_OK;
  HRESULT hrRet = S_OK;

  hrRetTemp = streamRewind();
  if (hrRetTemp != S_OK)
  {
    hrRet = hrRetTemp;
		goto StreamStringCopy_0;
  }

  hrRetTemp = m_pStream->SetSize(uliSize);
  if (hrRetTemp != S_OK)
  {
    hrRet = hrRetTemp;
		goto StreamStringCopy_0;
  }
	
	ulSize = m_sXML.length();

	hrRet = m_pStream->Write((const void*)m_sXML.c_str(), ulSize, &ulBytesWritten);
  if (hrRetTemp != S_OK)
  {
    hrRet = hrRetTemp;
		goto StreamStringCopy_0;
  }

  if (ulSize == ulBytesWritten)
  {
    hrRet = S_OK;
  }
  else
  {
    hrRet = S_FALSE;
  }

StreamStringCopy_0:


  return hrRet;
}

// Method; goto node element; 091222 p�d
// Will set the reader to point to the specific element; 091222 p�d
BOOL CxmlliteBase::gotoNodeElement(LPCTSTR node_element)
{
	BOOL bFound = FALSE;
	const WCHAR* pwszLocalName;
	XmlNodeType nodeType = XmlNodeType_None;

	m_sNodeElement = node_element;

	if (!isReader()) return FALSE;

	readerRewind();	// Set reader to point to first item in stream; 091222 p�d

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(node_element,pwszLocalName) == 0)
			{
				bFound = TRUE;			
				break;
			}
		}	// if( nodeType == XmlNodeType_Element )
	}	// while (getReader()->Read(&nodeType) == S_OK)
	return bFound;
}

BOOL CxmlliteBase::gotoNextNodeElement(void)
{
	BOOL bFound = FALSE;
	const WCHAR* pwszLocalName;
	XmlNodeType nodeType = XmlNodeType_None;

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (m_sNodeElement.Compare(pwszLocalName) == 0)
			{
				bFound = TRUE;			
				break;
			}
		}	// if( nodeType == XmlNodeType_Element )
	}	// while (getReader()->Read(&nodeType) == S_OK)

	return bFound;
}

// Read value from specific node element; 091222 p�d
BOOL CxmlliteBase::getNodeValue(LPCTSTR node_text,LPTSTR data)
{
	BOOL bFound = FALSE;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,node_text) == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				_tcscpy(data,pwszValue);
				bFound = TRUE;
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	if (!bFound) 
		_tcscpy(data,L"");

	return bFound;
}

BOOL CxmlliteBase::getNodeValue(LPCTSTR node_text,int *data)
{
	BOOL bFound = FALSE;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,node_text) == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				*data = _tstoi(pwszValue);
				bFound = TRUE;
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return bFound;
}

BOOL CxmlliteBase::getNodeValue(LPCTSTR node_text,double *data)
{
	BOOL bFound = FALSE;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,node_text) == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				*data = _tstof(pwszValue);
				bFound = TRUE;
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return bFound;
}

// Methods; read attrribte by name; 091222 p�d
BOOL CxmlliteBase::getAttr(LPCTSTR attr_name,LPTSTR data)
{
	const WCHAR* pwszValue;
	_tcscpy(data,L"");
	// Iterate through attributes and serach for item; 091215 p�d
	if (FAILED(getReader()->MoveToAttributeByName(attr_name,NULL))) return FALSE;
	if (FAILED(getReader()->GetValue(&pwszValue, NULL))) return FALSE;
	_tcscpy(data,pwszValue);

	return TRUE;
}

BOOL CxmlliteBase::getAttr(LPCTSTR attr_name,int *data)
{
	const WCHAR* pwszValue;
	// Iterate through attributes and serach for item; 091215 p�d
	if (FAILED(getReader()->MoveToAttributeByName(attr_name,NULL))) return FALSE;
	if (FAILED(getReader()->GetValue(&pwszValue, NULL))) return FALSE;
	*data = _tstoi(pwszValue);
	return TRUE;
}

BOOL CxmlliteBase::getAttr(LPCTSTR attr_name,double *data)
{
	const WCHAR* pwszValue;
	// Iterate through attributes and serach for item; 091215 p�d
	if (FAILED(getReader()->MoveToAttributeByName(attr_name,NULL))) return FALSE;
	if (FAILED(getReader()->GetValue(&pwszValue, NULL))) return FALSE;
	*data = _tstof(pwszValue);
	return TRUE;

	return TRUE;
}


// Methods for reading Attributes by position; 091222 p�d
// attr_pos starts from 0; 091222 p�d
BOOL CxmlliteBase::getAttr(int attr_pos,LPTSTR data)
{
	const WCHAR* pwszValue;
	BOOL bFound = FALSE;
	int nAttrCount = 0;	// Start pos for attributes; 091222 p�d
	UINT nNumOfAttributes = 0;
	// Make sure the attribute position is within limits; 091222 p�d
	getReader()->GetAttributeCount(&nNumOfAttributes);
	if (attr_pos < 0 || attr_pos > nNumOfAttributes-1) 
	{
		_tcscpy(data,L"");
		return FALSE;
	}

	// Iterate through attributes and serach for item; 091215 p�d
	for (HRESULT result = getReader()->MoveToFirstAttribute();
			 S_OK == result;
			 result = getReader()->MoveToNextAttribute())
	{
		if (nAttrCount == attr_pos)
		{
			getReader()->GetValue(&pwszValue, NULL);
			_tcscpy(data,pwszValue);
			bFound = TRUE;
			break;
		}
		nAttrCount++;
	}	// for (HRESULT result = pReader->MoveToFirstAttribute();S_OK == result;result = pReader->MoveToNextAttribute())

	if (!bFound)
		_tcscpy(data,L"");


	return bFound;
}

BOOL CxmlliteBase::getAttr(int attr_pos,int *data)
{
	const WCHAR* pwszValue;
	BOOL bFound = FALSE;
	int nAttrCount = 1;	// Start pos for attributes; 091222 p�d
	UINT nNumOfAttributes = 0;
	// Make sure the attribute position is within limits; 091222 p�d
	getReader()->GetAttributeCount(&nNumOfAttributes);
	if (attr_pos <= 0 || attr_pos > nNumOfAttributes) return FALSE;

	// Iterate through attributes and serach for item; 091215 p�d
	for (HRESULT result = getReader()->MoveToFirstAttribute();
			 S_OK == result;
			 result = getReader()->MoveToNextAttribute())
	{
		if (nAttrCount == attr_pos)
		{
			getReader()->GetValue(&pwszValue, NULL);
			*data = _tstoi(pwszValue);
			bFound = TRUE;
			break;
		}
		nAttrCount++;
	}	// for (HRESULT result = pReader->MoveToFirstAttribute();S_OK == result;result = pReader->MoveToNextAttribute())

	return bFound;
}

BOOL CxmlliteBase::getAttr(int attr_pos,double *data)
{
	const WCHAR* pwszValue;
	BOOL bFound = FALSE;
	int nAttrCount = 1;	// Start pos for attributes; 091222 p�d
	UINT nNumOfAttributes = 0;
	// Make sure the attribute position is within limits; 091222 p�d
	getReader()->GetAttributeCount(&nNumOfAttributes);
	if (attr_pos <= 0 || attr_pos > nNumOfAttributes) return FALSE;

	// Iterate through attributes and serach for item; 091215 p�d
	for (HRESULT result = getReader()->MoveToFirstAttribute();
			 S_OK == result;
			 result = getReader()->MoveToNextAttribute())
	{
		if (nAttrCount == attr_pos)
		{
			getReader()->GetValue(&pwszValue, NULL);
			*data = _tstof(pwszValue);
			bFound = TRUE;
			break;
		}
		nAttrCount++;
	}	// for (HRESULT result = pReader->MoveToFirstAttribute();S_OK == result;result = pReader->MoveToNextAttribute())

	return bFound;
}

//----------------------------------------------------------------------------------------
// Method for reading all attributes within an element; 091222 p�d
BOOL CxmlliteBase::gotoFirstAttribute(void)
{
	return (getReader()->MoveToFirstAttribute() == S_OK);
}

BOOL CxmlliteBase::gotoNextAttribute(void)
{
	return (getReader()->MoveToNextAttribute() == S_OK);
}

BOOL CxmlliteBase::getAttrValue(LPTSTR data)
{
	const WCHAR* pwszValue;
	_tcscpy(data,L"");
	if (FAILED(getReader()->GetValue(&pwszValue, NULL))) return FALSE;
	_tcscpy(data,pwszValue);
	return TRUE;
}

BOOL CxmlliteBase::getAttrValue(int *data)
{
	const WCHAR* pwszValue;
	if (FAILED(getReader()->GetValue(&pwszValue, NULL))) return FALSE;
	*data = _tstoi(pwszValue);
	return TRUE;
}

BOOL CxmlliteBase::getAttrValue(double *data)
{
	const WCHAR* pwszValue;
	if (FAILED(getReader()->GetValue(&pwszValue, NULL))) return FALSE;
	*data = _tstof(pwszValue);
	return TRUE;
}
