#ifndef _PAD_CALCULATION_CLASSES_H_
#define _PAD_CALCULATION_CLASSES_H_

#include "pad_transaction_classes.h"
#include <vector>
#include <map>

typedef std::map<short,bool> mapShortAndBool;


class CFuncDesc
{
	int nFuncID;
	TCHAR szFullName[50];
	TCHAR szAbbrevName[25];
	TCHAR szBasis[25];
	int nOnlySW;
public:
	CFuncDesc()
	{
		nFuncID = -1;
		_tcscpy_s(szFullName,50,_T(""));
		_tcscpy_s(szAbbrevName,25,_T(""));
		_tcscpy_s(szBasis,25,_T(""));
		nOnlySW = -1;
	}
	CFuncDesc(int id,LPCTSTR full_name,LPCTSTR abbrev,LPCTSTR basis,int only_sw)
	{
		nFuncID = id;
		_tcscpy_s(szFullName,50,full_name);
		_tcscpy_s(szAbbrevName,25,abbrev);
		_tcscpy_s(szBasis,25,basis);
		nOnlySW = only_sw;
	}

	// GET
	int getFuncID()	{ return nFuncID; }
	LPCTSTR getFullName() { return szFullName; }
	LPCTSTR getAbbrevName() { return szAbbrevName; }
	LPCTSTR getBasis() { return szBasis; }
	int getOnlySW() const { return nOnlySW; }
	// SET
	void setFullName(LPCTSTR v) { _tcscpy_s(szFullName,50,v); }
	void setAbbrevName(LPCTSTR v) { _tcscpy_s(szAbbrevName,25,v); }
	void setBasis(LPCTSTR v) { _tcscpy_s(szBasis,25,v); }
	void setOnlySW(int v) { nOnlySW = v; }
};

typedef std::vector<CFuncDesc> vecFuncDesc;

////////////////////////////////////////////////////////////////////////////
// UCFunctionList; show ALL functions for e.g. volumes per 
// functiontype (Brandel,N�slunds etc); 070416 p�d
class UCFunctionList
{
	int m_nIdentifer;
	int m_nSpcID;
	int m_nIndex;
	TCHAR m_szSpcName[64];
	TCHAR m_szFuncType[64];	// E.g. "Brandels"
	TCHAR m_szFuncArea[64];	// E.g. "Norra"
	TCHAR m_szFuncDesc[64];	// E.g. BRG + KRG (Breddgrad + Krongr�ns)
public:
	UCFunctionList(void);
	UCFunctionList(int id,int spc_id,int idx,LPCTSTR spc_name,LPCTSTR func_type,LPCTSTR func_area,LPCTSTR desc);

	int getID(void);
	int getSpcID(void);
	int getIndex(void);
	LPCTSTR getSpcName(void);
	LPCTSTR getFuncType(void);
	LPCTSTR getFuncArea(void);
	LPCTSTR getFuncDesc(void);
};

typedef std::vector<UCFunctionList> vecUCFunctionList;


////////////////////////////////////////////////////////////////////////////
// UCFunctions; holds functiontypes e.g. for volume (Brandel,N�slunds etc); 070416 p�d
// This class also holds the vector to UCFunctionList (vecUCFunctionList); 070416 p�d
class UCFunctions
{
//private:
	int m_nIdentifer;
	TCHAR m_szName[64];
public:
	UCFunctions(void);
	UCFunctions(int id,LPCTSTR name);
	UCFunctions(const UCFunctions &);

	int getIdentifer(void);
	LPCTSTR getName(void);
};

typedef std::vector<UCFunctions> vecUCFunctions;

//////////////////////////////////////////////////////////////////////////
// Calculate Height,Bark and Volume; 070525 p�d

BOOL doUCCalculate(CTransaction_trakt,
				 					 CTransaction_trakt_misc_data,
									 vecTransactionTraktData &,
									 vecTransactionSampleTree &,
									 vecTransactionDCLSTree &,
									 vecTransactionTraktSetSpc &,
									 bool doCalc = true,bool doGrotCalc = true);
//////////////////////////////////////////////////////////////////////////
// Calculate "Utbyte R.Ollas"; 070525 p�d

BOOL doUMExchange(CTransaction_trakt_misc_data rec1,
								  vecTransactionDCLSTree &dcls_tree_list,
									vecTransactionTreeAssort &tree_assort,
									vecTransactionTraktSetSpc &set_spc);

BOOL doUMExchangeSimple(CTransaction_trakt_misc_data rec1,
												vecTransactionDCLSTree &dcls_tree_list,
												vecTransactionTraktSetSpc &set_spc,
												vecTransactionTreeAssort &tree_assort,
												vecTransaction_exchange_simple &exch_data);

//////////////////////////////////////////////////////////////////////////
// Calculate "Intr�ng Skogsnorm"; 080508 p�d

BOOL doUMForrestNorm(int calc_origin,
										 mapString& map_err_text,
										 CTransaction_elv_object rec0,
										 CTransaction_trakt rec1,
										 vecObjectTemplate_p30_table& p30,
										 vecObjectTemplate_p30_nn_table& p30_nn,
										 vecTransactionTraktData& trakt_data,
										 vecTransaction_elv_m3sk_per_specie& m3sk_in_out,
										 vecTransactionSampleTree& rand_trees,
										 CTransaction_eval_evaluation& eval_data,
										 vecTransactionTrakt& cruise_stands,
										 //#HMS-94 20220919 J� M�ste skicka med info om tr�dslagens inst�llda p30 tr�dslag f�r att kunna ber�kna kanttr�dsers�ttning
										 vecTransactionSpecies& vecSpecies,
										 // Return data ...
										 CStringArray& error_log,
										 CTransaction_elv_return_storm_dry& storm_dry,
										 CTransaction_elv_return_land_and_precut& land_and_precut,
										 mapDouble& rand_trees_volume);

#endif // _UCLISTCLASSES_H_
