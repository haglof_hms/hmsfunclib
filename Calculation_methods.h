#ifndef _CALCULATION_METHODS_H_
#define _CALCULATION_METHODS_H_

#include "pad_transaction_classes.h"
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "pad_calculation_classes.h"

#include <vector>
#include <map>

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for treetype same as in UCCalculate; 080505 p�d
#define SAMPLE_TREE				0			// A sampletree. I.e. it has a heigth etc
#define TREE_OUTSIDE			1			// "Intr�ngsv�rdering; kanttr�d Provtr�d"
#define TREE_OUTSIDE_NO_SAMP	2			// "Intr�ngsv�rdering; kanttr�d Ej Provtr�d"
#define TREE_SAMPLE_REMAINIG	3			// "Provtr�d (Kvarl�mmnat)"
#define TREE_SAMPLE_EXTRA		4			// "Provtr�d (Extra inl�sta)"
#define TREE_ANYTYPE			0xffffffff
#define TREE_OUTSIDE_LEFT 5	// Kanttr�d l�mnas (Provtr�d)
#define TREE_OUTSIDE_LEFT_NO_SAMP 6	// Kanttr�d l�mnas  (Ber�knad h�jd)
#define TREE_POS_NO_SAMP 7	// Pos.tr�d (uttag)
#define TREE_INSIDE_LEFT 8	// Tr�d i gatan l�mnas (Provtr�d)
#define TREE_INSIDE_LEFT_NO_SAMP 9	// Tr�d i gatan l�mnas  (Ber�knad h�jd)


/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines type of "St�mplingsl�ngd"; 080505 p�d
const int STMP_LEN_WITHDRAW				=	1;		// "St�mplingsl�ngd - Uttag"
const int STMP_LEN_TO_BE_LEFT			=	2;		// "St�mplingsl�ngd - Kvarl�mmnat"
const int STMP_LEN_WITHDRAW_ROAD	= 3;		// "St�mplingsl�ngd - Uttag i Stickv�g"

// Identifies that this is a pricelist used for calulation (utbyte)
// using Rune Ollas calculations; 080505 p�d
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_1			1		// Pricelist for R. Ollars
#define ID_RUNE_OLLAS_TYPEOF_PRICELIST_2			2		// "Simple" avg. prices per assortment

/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated type for determin' kind of assortment; 070531 p�d
#define ASSORTMENT_TIMBER_TO				0
#define ASSORTMENT_TIMBER_FUB_1				1
#define ASSORTMENT_TIMBER_FUB_3				3
#define ASSORTMENT_NOT_PULP_NOT_TMBER		1
#define ASSORTMENT_PULPWOOD					2

//////////////////////////////////////////////////////////////////////////////////////////
// Type of cost template; 071008 p�d
#define COST_TYPE_1				1	// "Kostnader f�r Rotpost"
#define COST_TYPE_2				2	// "Kostnader f�r Rotpost i LandValue"

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for "Rotpost" origin
#define ROTPOST_ORIGIN1			1	// "Vanlig" rotpost
#define ROTPOST_ORIGIN2_1		2	// "Intr�ngsv�rdering" rotpost "Kostnadsmall med huggningskostnad kr/m3"
#define ROTPOST_ORIGIN2_2		3	// "Intr�ngsv�rdering" rotpost "Kostnadsmall med huggningskostnad enl. tabell DGV"

/////////////////////////////////////////////////////////////////////////////////////////////////
// Defines for "Rotpost" origin
#define OTHER_COST_0			0	// "Kostnader f�r 'Vanlig' rotpost i kostndsmall"
#define OTHER_COST_1			1	// "�vriga intr�ngskostnader"
#define OTHER_COST_2			2	// "�vriga fasta kostnader"

//////////////////////////////////////////////////////////////////////////////////////////
// Table names; 080505 p�d
const LPCTSTR TBL_TRAKT							= _T("esti_trakt_table");
const LPCTSTR TBL_TRAKT_DATA					= _T("esti_trakt_data_table");
const LPCTSTR TBL_TRAKT_SPC_ASS					= _T("esti_trakt_spc_assort_table");
const LPCTSTR TBL_TRAKT_TRANS					= _T("esti_trakt_trans_assort_table");
const LPCTSTR TBL_TRAKT_SET_SPC					= _T("esti_trakt_set_spc_table");
const LPCTSTR TBL_TRAKT_MISC_DATA				= _T("esti_trakt_misc_data_table");

const LPCTSTR TBL_TRAKT_TYPE					= _T("esti_trakt_type_table");
const LPCTSTR TBL_ORIGIN						= _T("esti_origin_table");
const LPCTSTR TBL_INPUT_METHOD					= _T("esti_inpdata_table");
const LPCTSTR TBL_HGT_CLASS						= _T("esti_hgtcls_table");
const LPCTSTR TBL_CUT_CLASS						= _T("esti_cutcls_table");

const LPCTSTR TBL_TRAKT_PLOT					= _T("esti_trakt_plot_table");
const LPCTSTR TBL_TRAKT_SAMPLE_TREES			= _T("esti_trakt_sample_trees_table");
const LPCTSTR TBL_TRAKT_DCLS_TREES				= _T("esti_trakt_dcls_trees_table");
const LPCTSTR TBL_TRAKT_DCLS1_TREES				= _T("esti_trakt_dcls1_trees_table");
const LPCTSTR TBL_TRAKT_DCLS2_TREES				= _T("esti_trakt_dcls2_trees_table");
const LPCTSTR TBL_SPECIES_TABLE	= _T("fst_species_table");

const LPCTSTR TBL_TRAKT_TREES_ASSORT			= _T("esti_trakt_trees_assort_table");
const LPCTSTR TBL_TRAKT_ROTPOST					= _T("esti_trakt_rotpost_table");
const LPCTSTR TBL_TRAKT_ROTPOST_SPC				= _T("esti_trakt_rotpost_spc_table");
const LPCTSTR TBL_TRAKT_ROTPOST_OTC				= _T("esti_trakt_rotpost_other_table");

const LPCTSTR TBL_SAMPLE_TREES_CATEGORY			= _T("esti_sample_trees_category_table");
// Tables in UMLandValue
const LPCTSTR TBL_ELV_CRUISE					= _T("elv_cruise_table");

const LPCTSTR TBL_ELV_EVAL					= _T("elv_evaluation_table");

//////////////////////////////////////////////////////////////////////////////////////////
// This structure is added to hold information collected in function: 
// "getExchPriceCalulatedInExchangeModule(..)"; 071024 p�d
typedef struct _exch_calc_info_struct
{
	int nTraktID;
	int nTraktDataID;
	int nAssortID;
	double fExchPrice;
	int nPriceIn;
	_exch_calc_info_struct(void)
	{
		nTraktID	= -1;
		nTraktDataID = -1;
		nAssortID = -1;
		fExchPrice = 0.0;
		nPriceIn = -1;
	}
	_exch_calc_info_struct(int trakt_id,int trakt_data_id,int assort_id,double exch_price,int price_in)
	{
		nTraktID	= trakt_id;
		nTraktDataID = trakt_data_id;
		nAssortID = assort_id;
		fExchPrice = exch_price;
		nPriceIn = price_in;
	}

} EXCH_CALC_INFO_STRUCT;

typedef std::vector<EXCH_CALC_INFO_STRUCT> vecExchCalcInfo;

//////////////////////////////////////////////////////////////////////////////////////////
// This structure's used to hold information on specie(s) not havin' any functions
// added. I.e. no volume-,bark- or heightfunction; 070626 p�d
// OBS! This structure is also in UMEstimate; 080117 p�d
typedef struct _func_spc_struct
{
	int nSpcID;
	CString sSpcName;
	_func_spc_struct(void)
	{
		nSpcID = -1;
		sSpcName = _T("");
	}
	_func_spc_struct(int spc_id,LPCTSTR spc_name)
	{
		nSpcID = spc_id;
		sSpcName = (spc_name);
	}
} FUNC_SPC_STRUCT;

typedef std::vector<FUNC_SPC_STRUCT> vecFuncSpc;

class CDBHandleCalc : public CDBBaseClass_SQLApi //CDBHandleCalcBase
{
//private:

	BOOL traktDataExists(CTransaction_trakt_data &);
	BOOL traktAssExists(CTransaction_trakt_ass &);
	BOOL traktAssExists(int trakt_id);
	BOOL traktAssExists(int trakt_id,int trakt_data_id);
	BOOL traktTransExists(CTransaction_trakt_trans &);
	BOOL traktTransExists(int trakt_data_id,int trakt_id);
	BOOL traktSampleTreeExists(CTransaction_sample_tree &);
	BOOL traktSampleTreeExists(int trakt_id);
	BOOL traktDCLSTreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLSTreeExists(int trakt_id);
	BOOL traktDCLS1TreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLS1TreeExists(int trakt_id);
	BOOL traktDCLS2TreeExists(CTransaction_dcls_tree &);
	BOOL traktDCLS2TreeExists(int trakt_id);
	BOOL traktAssTreeExists(CTransaction_tree_assort &);
	BOOL traktAssTreeExists(int trakt_id);
	BOOL traktRotpostExists(CTransaction_trakt_rotpost &);
	BOOL traktRotpostSpcExists(CTransaction_trakt_rotpost_spc &);
	BOOL traktRotpostSpcExists(int trakt_id);
	BOOL traktRotpostOtcExists(CTransaction_trakt_rotpost_other &);
	BOOL traktRotpostOtcExists(int trakt_id);

public:
	CDBHandleCalc(void);
	CDBHandleCalc(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CDBHandleCalc(DB_CONNECTION_DATA &db_connection);

	// Handle Trakt and Trakt data; 070301 p�d
	BOOL setTrakt_Areal_Factor(int trakt_id,double areal_factor);

	BOOL getTraktData(vecTransactionTraktData &,int trakt_id = -1,int data_type = -1);
	BOOL getTraktData(CTransaction_trakt_data &,int trakt_id = -1,int spc_id = -1,int data_type = -1);
	BOOL addTraktData(CTransaction_trakt_data &);
	BOOL updTraktData(CTransaction_trakt_data &);
	BOOL updTraktData(vecTransactionTraktData &vec);

	BOOL getTraktAss(vecTransactionTraktAss &,int trakt_id);
	BOOL getTraktAss(CTransaction_trakt_ass &rec,CTransaction_trakt_ass rec1);
	BOOL getTraktAss(CTransaction_trakt_ass &rec,int trakt_id,int trakt_data_id,int data_type,int assort_id);
	BOOL addTraktAss(CTransaction_trakt_ass &);
	BOOL updTraktAss(CTransaction_trakt_ass &);
	BOOL updTraktAss(vecTransactionTraktAss &vec);
	BOOL updTraktAss_prices(int trakt_id,int trakt_data_id,int data_type,int assort_id,double exch_price,int price_in);
	BOOL updTraktAss_prices(vecExchCalcInfo &);
	BOOL resetTraktAss(int trakt_id);
	BOOL recalcTraktAss_on_k3_per_m3(int trakt_id,int price_set_in);

	// Transfer information; 080505 p�d
	BOOL getTraktTrans(vecTransactionTraktTrans &,int trakt_id = -1);
	BOOL getTraktTrans(CTransaction_trakt_trans &rec1,CTransaction_trakt_trans rec);
	BOOL addTraktTrans(CTransaction_trakt_trans &);
	BOOL updTraktTrans(CTransaction_trakt_trans &);
	BOOL removeTraktTrans(int trakt_data_id,int trakt_id);

	BOOL getTraktSetSpc(vecTransactionTraktSetSpc &,int trakt_id = -1);

	// Plot (Group); 080505 p�d
	BOOL getPlots(int trakt_id,vecTransactionPlot &);
	
	// Get misc data, e.g. Priclist etc; 080505
	BOOL getTraktMiscData(int trakt_id,CTransaction_trakt_misc_data &);

	// Sampletrees; 070510 p�d
	BOOL getSampleTrees(int trakt_id,int tree_type,vecTransactionSampleTree &);
	BOOL getSampleTree(CTransaction_sample_tree &rec1,CTransaction_sample_tree rec);
	BOOL addSampleTrees(CTransaction_sample_tree &);
	BOOL addSampleTrees(vecTransactionSampleTree &);
	BOOL updSampleTrees(CTransaction_sample_tree &);
	BOOL updSampleTrees(vecTransactionSampleTree &);
	BOOL delSampleTreesInTrakt(int trakt_id,int tree_type);

	// Function to grab data for all dcls, species, etc. at once; Optimizations by Peter
	BOOL getDCLSAllTrees(int trakt_id, vecTransactionDCLSTree &vec0, vecTransactionDCLSTree &vec1, vecTransactionDCLSTree &vec2);
	BOOL getDCLSNumOfTrees(int trakt_id, double areal_factor, std::map<int,int> &numTrees);

	// DCLStrees; 070510 p�d
	BOOL getDCLSTrees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLSTree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec);
	BOOL addDCLSTrees(CTransaction_dcls_tree &);
	BOOL updDCLSTrees(CTransaction_dcls_tree &);
	BOOL updDCLSTrees(vecTransactionDCLSTree &);
	BOOL updDCLSTrees_m3fub(CTransaction_dcls_tree &);
	BOOL updDCLSTrees_m3fub(vecTransactionDCLSTree &vec,std::map<int,double>&);
	BOOL delDCLSTreesInTrakt(int trakt_id);
	BOOL delDCLSTreesInTrakt(int dcls_id, int trakt_id);
	BOOL getDCLSNumOfTrees(int trakt_id,int spc_id,double areal_factor,long* num_of_trees);
	
	// DCLS1trees; 070510 p�d
	BOOL getDCLS1Trees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLS1Tree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec);
	BOOL addDCLS1Trees(CTransaction_dcls_tree &);
	BOOL updDCLS1Trees(CTransaction_dcls_tree &);
	BOOL updDCLS1Trees(vecTransactionDCLSTree &);
	BOOL delDCLS1TreesInTrakt(int trakt_id);

	// DCLS2trees; 070510 p�d
	BOOL getDCLS2Trees(int trakt_id,vecTransactionDCLSTree &);
	BOOL getDCLS2Tree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec);
	BOOL addDCLS2Trees(CTransaction_dcls_tree &);
	BOOL updDCLS2Trees(CTransaction_dcls_tree &);
	BOOL updDCLS2Trees(vecTransactionDCLSTree &);
	BOOL delDCLS2TreesInTrakt(int trakt_id);
	
	// Assortments for Tree; 070510 p�d
	BOOL addAssTree(CTransaction_tree_assort &);
	BOOL addAssTree(vecTransactionTreeAssort &);
	BOOL delAssTree(int trakt_id);

	// "Rotpost"; 071011 p�d
	BOOL getRotpost(CTransaction_trakt_rotpost &vec,int trakt_id);
	BOOL addRotpost(CTransaction_trakt_rotpost &);
	BOOL updRotpost(CTransaction_trakt_rotpost &);

	// "Rotpost Transport och Huggningskostnader per tr�dslag"; 071011 p�d
	BOOL addRotpostSpc(vecTransaction_trakt_rotpost_spc &);
	BOOL addRotpostSpc(CTransaction_trakt_rotpost_spc &);
	BOOL delRotpostSpc(int trakt_id);

	// "Rotpost �vriga kostnader"; 080312 p�d
	BOOL addRotpostOtc(vecTransaction_trakt_rotpost_other &);
	BOOL addRotpostOtc(CTransaction_trakt_rotpost_other &);
	BOOL delRotpostOtc(int trakt_id);

	// External database item.
	// Special function combining 2 or more tables; 071024 p�d
	BOOL getExchPriceCalulatedInExchangeModule(int trakt_id,vecExchCalcInfo &vec);

	BOOL getSampletreesOutsideH25MinMax(int nTraktId, CStringArray* logText);	// #4670 Kolla om det finns provtr�d utanf�r H25s omr�de
};


void getDiamClass(double dbh,double dcls,double *dcls_from,double *dcls_to);

void calcTransfersByPercent(int spc_id,
							LPCTSTR fromAssort,
							LPCTSTR toAssort,
							double percent,
							CTransaction_trakt_data recTraktData,
							vecTransactionTraktAss &vecTraktAss,
							short only_for_trans,
							bool first_time,
							double *vol_trans);

void calcRotpostForTrakt(BOOL bConnected,
							DB_CONNECTION_DATA m_dbConnectionData,
							int trakt_id,
							vecTransactionTraktAss *pTraktAss = NULL,
							vecTransactionTraktSetSpc *pTraktSetSpc = NULL,
							vecTransactionTraktData *pTraktData = NULL,
							CTransaction_trakt_misc_data *pTraktMiscData = NULL);

int doCruisingCalculations(int spc_id,
							BOOL remove_trakt_trans,
							CStringArray &log,
							BOOL bConnected,
							DB_CONNECTION_DATA m_dbConnectionData,
							CTransaction_trakt &trakt,
							bool do_transfers = true,
							int object_id = -1);				// Originates from active object in UMLandValue

//#4603 20151021 J� Kontroll av s�derbergs bark o h�jdfunktioner per tr�dslag.
int doSoderbergHgtandVolCheckonTrakt(DB_CONNECTION_DATA m_dbConnectionData,CTransaction_trakt recTrakt,CStringArray &log,CStringArray &log2);


int doTopCutCalculations(CStringArray &log,
						 BOOL bConnected,
						 DB_CONNECTION_DATA m_dbConnectionData,
						 CTransaction_trakt &trakt);			//Ber�knar toppningsv�rde f�r provtr�d #4204 20141203 J�

// #4670 Kontrollera om vi anv�nder H25 och har h�jder st�rre �n max
int CheckH25MinMax(DB_CONNECTION_DATA m_dbConnectionData, CTransaction_trakt recTrakt, CStringArray &logText);

#endif