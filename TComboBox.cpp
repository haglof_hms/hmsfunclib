// TComboBox.cpp : implementation file
//

#include "stdafx.h"
#include "TComboBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define FORCED_PREFIX	_T("�")
#define FORCED_POSTFIX	_T("�")

// Here's the edit control's ID (which is typically 1 less than the
// first control in the resource.h file).
#define EDIT_CTRL_ID 999

bool bForceWindowsDefaultFunction;

/////////////////////////////////////////////////////////////////////////////
// CTComboBox

CTComboBox::CTComboBox()
{
	m_iBottom = -1;
	lptsArray = NULL;
	m_eType = CTComboBox::Unknown;

	// Initialize selected variables
	m_csSelectedText = _T("");
	m_csSelectedCode = _T("");
	m_iSelectedCode = -1;
	m_lSelectedCode = 0;
	m_dwSelectedCode = 0;
	m_bAllowDuplicates = true;
	bForceWindowsDefaultFunction = false;
	m_pReadOnlyEdit = NULL;

	// Does the control already exist (i.e. only allocate it once)?
	if (! m_pReadOnlyEdit)
		m_pReadOnlyEdit = new CEdit();
}	// CTComboBox::CTComboBox()

CTComboBox::~CTComboBox()
{
	// Validate pointer
	if (m_pReadOnlyEdit)
	{
		delete m_pReadOnlyEdit;
		m_pReadOnlyEdit = NULL;
	}	// End of pointer validation
	CComboBox::~CComboBox();

	m_eType = CTComboBox::Unknown;
}	// CTComboBox::~CTComboBox()


BEGIN_MESSAGE_MAP(CTComboBox, CComboBox)
	//{{AFX_MSG_MAP(CTComboBox)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTComboBox message handlers

LRESULT CTComboBox::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// Determine the specific messages we're trapping
	switch(message)
	{
	// Capture the GETTEXT message
	case WM_GETTEXT:
		{
			CString
				csChildTxt = _T("");
			CWnd
				*pChildWnd = GetWindow(GW_CHILD);

			// Check if length of string allowed is greater than zero (0)
			// NOTE:	We check for 1 because the Windows message includes
			//			the size allocated for the string plus 1 for the
			//			terminating NULL character.
			if (wParam > 1)
			{
				// Check if pointer is valid
				if (pChildWnd)
				{
					pChildWnd->GetWindowText(csChildTxt);

					_tcsncpy((LPTSTR)lParam, (LPCTSTR)csChildTxt, wParam - 1);
				}	// End of pointer validation
				else
					// Copy out the selected text, stripping of "forced" entry
					// pre/post string attachments
					_tcsncpy((LPTSTR)lParam, (LPCTSTR)GetCurrentText(), wParam - 1);
			}	// End of test if the allowable string size will be greater than 0

			// See if we were unable to retrieve a string from ourselves...
			// if so, get it from the default Windows processing loop.
			if (bForceWindowsDefaultFunction || (_tcslen((LPTSTR)lParam) == 0))
			{
				bForceWindowsDefaultFunction = false;
				return CComboBox::DefWindowProc(message, wParam, lParam);
			}	// End of check if desired to force windows default function or need to call it!
			else
				return true;
		}	// See if the get text message is being sent
		break;

	// Same message for enable/disable
	case WM_ENABLE:
		{
			LRESULT
				lRtrn = CComboBox::DefWindowProc(message, wParam, lParam);

			// Check if disabling the combobox
			if (wParam == 0)
			{
				// Verify pointer exists and need to create the edit control
				if ((m_pReadOnlyEdit) ||
					(m_pReadOnlyEdit && (m_pReadOnlyEdit->m_hWnd == NULL)))
				{
					CRect
						oRect;
					GetClientRect(&oRect);
					oRect.right -= 15;		// When want the disabled drop-down portion visible

					// Adjust rectangle for border
					int
						iBorderWidth = 3;

					// Adjust when drop-list is type of combobox
					if ((GetStyle() & CBS_DROPDOWNLIST) == CBS_DROPDOWNLIST)
						iBorderWidth++;

					oRect.left += iBorderWidth;
					oRect.top += iBorderWidth;
					oRect.right -= iBorderWidth;
					oRect.bottom -= iBorderWidth;

					// See if we need to create the edit control
					if (m_pReadOnlyEdit->m_hWnd == NULL)
					{
						// Create the read-only edit control
						if (m_pReadOnlyEdit->Create(WS_CHILD | ES_LEFT |
													ES_AUTOHSCROLL | ES_READONLY,
													oRect, this, EDIT_CTRL_ID))
						{
							DWORD
								dwExStyle = WS_EX_CLIENTEDGE | WS_EX_NOPARENTNOTIFY;

							// Comment out code that would display a border on the edit control
							m_pReadOnlyEdit->ModifyStyleEx(0, dwExStyle/*, SWP_DRAWFRAME | SWP_NOSIZE*/);
							CFont
								*pCurFont = GetFont();
							if (pCurFont)
								m_pReadOnlyEdit->SetFont(pCurFont);
						}	// End of test if edit control was created properly or not
						else
						{
							delete m_pReadOnlyEdit;
							m_pReadOnlyEdit = NULL;
						}	// Error occurred, don't worry about edit control
					}	// End of test if we need to create the edit control
				}	// End of pointer validation

				// Check if edit control has been created and has a valid windows handle
				if (m_pReadOnlyEdit &&
					(m_pReadOnlyEdit->m_hWnd != NULL))
				{
					// Try to show the edit control
					m_pReadOnlyEdit->ShowWindow(SW_SHOW);
					m_pReadOnlyEdit->SetReadOnly(TRUE);
					m_pReadOnlyEdit->ModifyStyle(WS_TABSTOP, 0);
					CString
						csComboText;
					GetWindowText(csComboText);
					m_pReadOnlyEdit->SetWindowText(csComboText);
				}	// End of pointer validation
			}	// End of test if disabling the combo
			else
			{
				// Check if enabling window, and edit control exists
				if ((wParam == 1) && m_pReadOnlyEdit)
					m_pReadOnlyEdit->ShowWindow(SW_HIDE);		// Hide the edit control
			}	// End of test if NOT disabling the combo

			return lRtrn;
		}	// End of test if enable/disable of combo called for
		break;

	// This message is sent when changing "modes" of the combobox
	// (i.e. the WM_ENABLE message doesn't appear to be called to disable
	//  the combobox as one would think!)
	case WM_CANCELMODE:
		DWORD
			dwStyle = GetStyle();

		// See if we're disabling the control
		if ((dwStyle & WS_DISABLED) == WS_DISABLED)
		{
			// Check if edit control has been created and has a valid windows handle
			if (m_pReadOnlyEdit &&
				(m_pReadOnlyEdit->m_hWnd != NULL))
			{
				// Try to show the edit control
				m_pReadOnlyEdit->ShowWindow(SW_SHOW);
				m_pReadOnlyEdit->SetReadOnly(TRUE);
				m_pReadOnlyEdit->ModifyStyle(WS_TABSTOP, 0);
				CString
					csComboText;
				GetWindowText(csComboText);
				m_pReadOnlyEdit->SetWindowText(csComboText);
			}	// End of pointer validation
		}	// Check if disabled flag is on
		break;
	}	// End of message trapping

	return CComboBox::DefWindowProc(message, wParam, lParam);
}	// CTComboBox::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)

void CTComboBox::OnDestroy()
{
	CComboBox::OnDestroy();

	// Make sure to keep the selection in case the user
	// needs this information AFTER the dialog closes!
	GetSelectedText();
	GetSelectedCode(m_csSelectedCode);
	GetSelectedCode(&m_iSelectedCode);
	GetSelectedCode(&m_lSelectedCode);
	GetSelectedCode(&m_dwSelectedCode);

	// Check if pointer to array was allocated
	if (lptsArray != NULL)
	{
		LPTSTR
			LPTSTR;
		// Release memory as needed
		while (m_iBottom >= 0)
		{
			LPTSTR = lptsArray[m_iBottom];

			// Make sure pointer is valid
			if (LPTSTR != NULL)
			{
				delete [] LPTSTR;
				lptsArray[m_iBottom--] = NULL;
			}	// End of pointer validation
		}	// End of check if something has been added
		delete [] lptsArray;
		lptsArray = NULL;
	}	// End of check if pointer to array was allocated

	m_eType = CTComboBox::Unknown;
}	// CTComboBox::OnDestroy()

bool CTComboBox::MakeDropDown()
{
	bool
		bRtrn = false;

	bRtrn = (ModifyStyleEx(CBS_DROPDOWNLIST, CBS_DROPDOWN,
							SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER) == TRUE ? true : false);

	return bRtrn;
}	// CTComboBox::MakeDropDown()

bool CTComboBox::MakeDropDownList()
{
	bool
		bRtrn = false;

	bRtrn = (ModifyStyleEx(CBS_DROPDOWN, CBS_DROPDOWNLIST,
							SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER) == TRUE ? true : false);

	return bRtrn;
}	// CTComboBox::MakeDropDownList()

bool CTComboBox::AddEntry(CString &rcsText, CString &rcsCode)
{
	bool
		bRtrn = false;

	// Allocate the initial pointer if necessary
	if (lptsArray == NULL)
		lptsArray = new LPTSTR[SHRT_MAX];

	// Make sure that something was allocated
	if (lptsArray != NULL)
	{
		bool
			bOK = true;

		// Determine if duplicates are not allowed
		if (!m_bAllowDuplicates)
		{
			if (FindString(-1, rcsText) != CB_ERR)
			{
				bOK = false;
				bRtrn = true;
			}	// End of check if item doesn't exist in the combobox
		}	// End of determining if duplicates are allowed or not

		// Determine if OK to add the entry
		if (bOK)
		{
			LPTSTR
				str;

			// Allocate some memory
			int
				iSzTchar = sizeof _TCHAR;

			str = new _TCHAR[(rcsCode.GetLength() + 1) * iSzTchar];

			// Validate pointer
			if (str != NULL)
			{
				// Set the type if necessary
				if (m_eType == CTComboBox::Unknown)
					m_eType = CTComboBox::String;
				_tcscpy(str, (LPTSTR)(LPCTSTR)rcsCode);

				// Add the code string to our list
				lptsArray[++m_iBottom] = str;

				bRtrn = AddEntry(rcsText, (DWORD)str);
			}	// End of pointer validation
		}	// End of check if OK to add the entry
	}	// End of pointer to array validation

	return bRtrn;
}	// CTComboBox::AddEntry(CString &rcsText, CString &rcsCode)

bool CTComboBox::AddEntry(CString &rcsText, LPCTSTR lpcsCode)
{
	// Set the type if necessary
	if (m_eType == CTComboBox::Unknown)
		m_eType = CTComboBox::LPTSTRing;

	// Call the CString based function
	return AddEntry(rcsText, CString(lpcsCode));
}	// CTComboBox::AddEntry(CString &rcsText, LPCTSTR lpcsCode)

bool CTComboBox::AddEntry(CString &rcsText, int iCode)
{
	// Set the type if necessary
	if (m_eType == CTComboBox::Unknown)
		m_eType = CTComboBox::Int;

	// Call the standard insert
	return AddEntry(rcsText, (DWORD)iCode);
}	// CTComboBox::AddEntry(CString &rcsText, int iCode)

bool CTComboBox::AddEntry(CString &rcsText, long lCode)
{
	// Set the type if necessary
	if (m_eType == CTComboBox::Unknown)
		m_eType = CTComboBox::Long;

	// Call the standard insert
	return AddEntry(rcsText, (DWORD)lCode);
}	// CTComboBox::AddEntry(CString &rcsText, long lCode)

bool CTComboBox::AddEntry(CString &rcsText, DWORD dwCode)
{
	bool
		bRtrn = false;
	int
		iInsertPos;

	// Set the type if necessary
	if (m_eType == CTComboBox::Unknown)
		m_eType = CTComboBox::Double;

	bool
		bOK = true;

	// Determine if duplicates are not allowed
	if (!m_bAllowDuplicates)
	{
		if (FindString(-1, rcsText) != CB_ERR)
		{
			bOK = false;
			bRtrn = true;
		}	// End of check if item doesn't exist in the combobox
	}	// End of determining if duplicates are allowed or not

	// Determine if OK to add the entry
	if (bOK)
	{
		// Attempt to insert the string into the combobox
		iInsertPos = CComboBox::AddString((LPCTSTR)rcsText);

		// Check if position is valid
		if ((iInsertPos != CB_ERR) && (iInsertPos != CB_ERRSPACE))
		{
			// Make sure that the Data was set properly
			if (CComboBox::SetItemData(iInsertPos, dwCode) != CB_ERR)
				bRtrn = true;
		}	// End of check if position is valid
	}	// End of determining if OK to add the entry

	return bRtrn;
}	// CTComboBox::AddEntry(CString &rcsText, DWORD dwCode)

CString CTComboBox::GetCurrentText()
{
	return GetSelectedText();
}	// CTComboBox::GetCurrentText()

CString &CTComboBox::GetSelectedText()
{
	CString
		csTmp = _T("");

	// See if the handle to the window is OK
	if (m_hWnd != NULL)
	{
		// Get the current selection
		int iCurSel = this->GetCurSel();

		// Useful to trace Windows messages
		//TRACE1("Current selection #%d in CTComboBox::GetSelectedText()\n", iCurSel);

		// Make sure the current selection was set
		if (iCurSel != CB_ERR)
		{
			m_csSelectedText = GetText(iCurSel);

			// Test if we have to get rid of "forced" information
			if (m_csSelectedText.Left(1) == FORCED_PREFIX)
				m_csSelectedText = m_csSelectedText.Mid(1, m_csSelectedText.GetLength() - 2);
		}	// End of test to make sure that something was selected
		else
		{
			// NOTE: This must exist in this case, since we have to
			// force the default Windows processing of a combobox!
			// DO NOT REMOVE!
			bForceWindowsDefaultFunction = true;
		}	// End of test to make sure that "" is returned when nothing is selected
	}	// End of check to see if handle to window is valid

	return m_csSelectedText;
}	// CTComboBox::GetSelectedText()

CString CTComboBox::GetText(int iCBSelection)
{
	ASSERT(iCBSelection != CB_ERR);

	CString
		csTmp = _T("");

	// Make sure that a selection was given
	if (iCBSelection != CB_ERR)
		GetLBText(iCBSelection, csTmp);

	return csTmp;
}	// CTComboBox::GetText(int iCBSelection)

bool CTComboBox::GetSelectedCode(CString &rcsCode)
{
	// See if the handle to the window is OK
	if (m_hWnd != NULL)
	{
		// Get the current selection
		int iCurSel = this->GetCurSel();

		// Make sure the current selection was set
		if (iCurSel != CB_ERR)
			return GetCode(iCurSel, rcsCode);
		else
		{
			rcsCode = _T("");
			return false;
		}	// End of check if there was no selection
	}	// End of check if screen is active
	else
		rcsCode = m_csSelectedCode;

	return true;
}	// CTComboBox::GetSelectedCode(CString &rcsCode)

bool CTComboBox::GetSelectedCode(int *piCode)
{
	return GetCurrentCode(piCode);
}	// CTComboBox::GetSelectedCode(int *piCode)

bool CTComboBox::GetSelectedCode(long *plCode)
{
	return GetCurrentCode(plCode);
}	// CTComboBox::GetSelectedCode(long *plCode)

bool CTComboBox::GetSelectedCode(DWORD *pdwCode)
{
	return GetCurrentCode(pdwCode);
}	// CTComboBox::GetSelectedCode(DWORD *pdwCode)

bool CTComboBox::GetCurrentCode(CString &rcsCode)
{
	return GetSelectedCode(rcsCode);
}	// CTComboBox::GetCurrentCode(CString &rcsCode)

bool CTComboBox::GetCurrentCode(int *piCode)
{
	ASSERT(piCode != NULL);

	bool
		bRtrn = false;
	int
		iVal = -1;

	// See if the handle to the window is OK
	if (m_hWnd != NULL)
	{
		// Get the current selection
		int iCurSel = this->GetCurSel();

		// Make sure the current selection was set
		if (iCurSel != CB_ERR)
			bRtrn = GetCode(iCurSel, &iVal);
	}	// End of check if screen is active
	else
		iVal = m_iSelectedCode;

	// Convert value as needed
	switch (m_eType)
	{
	case String:
	case LPTSTRing:
		if ((m_csSelectedCode.GetLength() == 0) || (m_csSelectedCode == _T("")))
			*piCode = iVal;
		else
			*piCode = _tstoi(m_csSelectedCode);
		break;

	case Int:
	case Long:
	case Double:
		*piCode = iVal;
		break;
	}	// End of conversion

	return bRtrn;
}	// CTComboBox::GetCurrentCode(int *piCode)

bool CTComboBox::GetCurrentCode(long *plCode)
{
	ASSERT(plCode != NULL);

	bool
		bRtrn = false;
	long
		lVal = 0;

	// See if the handle to the window is OK
	if (m_hWnd != NULL)
	{
		// Get the current selection
		int iCurSel = this->GetCurSel();

		// Make sure the current selection was set
		if (iCurSel != CB_ERR)
			bRtrn = GetCode(iCurSel, &lVal);
	}	// End of check if screen is active
	else
		lVal = m_lSelectedCode;

	// Convert value as needed
	switch (m_eType)
	{
	case String:
	case LPTSTRing:
		if ((m_csSelectedCode.GetLength() == 0) || (m_csSelectedCode == _T("")))
			*plCode = lVal;
		else
			*plCode = _ttol(m_csSelectedCode);
		break;

	case Int:
	case Long:
	case Double:
		*plCode = lVal;
		break;
	}	// End of conversion

	return bRtrn;
}	// CTComboBox::GetCurrentCode(long *plCode)

bool CTComboBox::GetCurrentCode(DWORD *pdwCode)
{
	ASSERT(pdwCode != NULL);

	bool
		bRtrn = false;
	DWORD
		dwVal = 0;

	// See if the handle to the window is OK
	if (m_hWnd != NULL)
	{
		// Get the current selection
		int iCurSel = this->GetCurSel();

		// Make sure the current selection was set
		if (iCurSel != CB_ERR)
			bRtrn = GetCode(iCurSel, &dwVal);
	}	// End of check if screen is active
	else
		dwVal = m_dwSelectedCode;

	// Convert value as needed
	switch (m_eType)
	{
	case String:
	case LPTSTRing:
		if ((m_csSelectedCode.GetLength() == 0) || (m_csSelectedCode == _T("")))
			*pdwCode = dwVal;
		else
			*pdwCode = _ttol(m_csSelectedCode);
		break;

	case Int:
	case Long:
	case Double:
		*pdwCode = dwVal;
		break;
	}	// End of conversion

	return bRtrn;
}	// CTComboBox::GetCurrentCode(DWORD *pdwCode)

bool CTComboBox::GetCode(int iCBSelection, CString &rcsCode)
{
	ASSERT(iCBSelection != CB_ERR);

	bool
		bRtrn = false;
	DWORD
		dwData = 0;
	/*LPTSTR*/ LPTSTR
		LPTSTR = NULL;

	// Get the item data
	dwData = GetItemData(iCBSelection);

	// Check item data
	if (dwData != NULL)
		bRtrn = true;

	// Make sure that all is OK so far
	if (bRtrn)
	{
		// Determine which type of code it is originally
		switch (m_eType)
		{
		case Unknown:
			rcsCode = _T("");
			break;
		case String:
		case LPTSTRing:
			LPTSTR = (_TCHAR *)dwData;
			rcsCode = LPTSTR;
			bRtrn = true;
			break;
		case Int:
			rcsCode.Format(_T("%d"), dwData);
			bRtrn = true;
			break;
		case Long:
		case Double:
			rcsCode.Format(_T("%ld"), dwData);
			bRtrn = true;
		}	// Switch depending on the type of code
	}	// End of check if all is OK so far

	return bRtrn;
}	// CTComboBox::GetCode(int iCBSelection, CString &rcsCode)

bool CTComboBox::GetCode(int iCBSelection, int *piCode)
{
	ASSERT(iCBSelection != CB_ERR);

	// Call the standard Get
	return GetCode(iCBSelection, (DWORD *)piCode);
}	// CTComboBox::GetCode(int iCBSelection, int *piCode)

bool CTComboBox::GetCode(int iCBSelection, long *plCode)
{
	ASSERT(iCBSelection != CB_ERR);

	// Call the standard Get
	return GetCode(iCBSelection, (DWORD *)plCode);
}	// CTComboBox::GetCode(int iCBSelection, long *plCode)

bool CTComboBox::GetCode(int iCBSelection, DWORD *pdwCode)
{
	ASSERT(iCBSelection != CB_ERR);

	bool
		bRtrn = false;
	DWORD
		dwVal = 0;

	// Make sure the current selection was set
	if (iCBSelection != CB_ERR)
	{
		// Try to get the data value
		dwVal = this->GetItemData(iCBSelection);

		// Convert value as needed
		switch (m_eType)
		{
		case String:
		case LPTSTRing:
			*pdwCode = _ttol((LPTSTR)dwVal);
			break;

		case Int:
		case Long:
		case Double:
			*pdwCode = dwVal;
			break;
		}	// End of conversion

		bRtrn = true;
	}	// End of current selection validation

	return bRtrn;
}	// CTComboBox::GetCode(int iCBSelection, DWORD *pdwCode)

int CTComboBox::FindCode(int iStartAfter, LPCTSTR lpsCodeToFind)
{
	int
		iRtrn = CB_ERR,
		iItemCount = this->GetCount();

	ASSERT(iStartAfter <= iItemCount);

	// See if need to loop
	if (iItemCount > 0)
	{
		DWORD
			dwTmp;
		LPTSTR
			str;
		int
			n;
		// Loop through all selections
		for (n = (iStartAfter == -1 ? 0 : iStartAfter); n < iItemCount; n++)
		{
			dwTmp = this->GetItemData(n);
			str = (LPTSTR)dwTmp;

			// Check for match
			if (_tcsicmp(str, lpsCodeToFind) == 0)
			{
				iRtrn = n;
				break;
			}	// End of check for match
		}	// End of loop through all selections

		// Set error condition up if necessary
		if (n >= iItemCount)
			iRtrn = CB_ERR;
	}	// End of check if need to perform loop

	return iRtrn;
}	// CTComboBox::FindCode(int iStartAfter, LPCTSTR lpsCodeToFind)

int CTComboBox::FindCode(int iStartAfter, int iCode)
{
	return (FindCode(iStartAfter, (DWORD)iCode));
}	// CTComboBox::FindCode(int iStartAfter, int iCode)

int CTComboBox::FindCode(int iStartAfter, long lCode)
{
	return (FindCode(iStartAfter, (DWORD)lCode));
}	// CTComboBox::FindCode(int iStartAfter, long lCode)

int CTComboBox::FindCode(int iStartAfter, DWORD dwCode)
{
	int
		iRtrn = CB_ERR,
		iItemCount = this->GetCount();
	DWORD
		dwTmp;

	ASSERT(iStartAfter <= iItemCount);

	int
		iSelNum;
	// Look for the proper value to select
	for (iSelNum = (iStartAfter == -1 ? 0 : iStartAfter); iSelNum < iItemCount; iSelNum++)
	{
		GetCode(iSelNum, &dwTmp);
		if (dwTmp == dwCode)
			break;
	}

	// Set error condition up if necessary
	if (iSelNum >= iItemCount)
		iRtrn = CB_ERR;
	else
		iRtrn = iSelNum;

	return iRtrn;
}	// CTComboBox::FindCode(int iStartAfter, DWORD dwCode)

int CTComboBox::SelectCode(int iStartAfter, LPCTSTR lpsCodeToSearchFor, bool bForceFill /*= true*/)
{
	int
		iRtrn = FindCode(iStartAfter, lpsCodeToSearchFor);

	// Now select the item if we can
	if (iRtrn != CB_ERR)
		this->SetCurSel(iRtrn);
	else
	{
		// If forcefill is true then force it into the combobox
		if (bForceFill)
		{
			CString
				csTmp;
			csTmp.Format(_T("%s%s%s"), (LPCTSTR)FORCED_PREFIX,
										lpsCodeToSearchFor,
										(LPCTSTR)FORCED_POSTFIX);
			switch (m_eType)
			{
			case String:
			case LPTSTRing:
				AddEntry(csTmp, lpsCodeToSearchFor);
				break;

			case Int:
			case Long:
			case Double:
				AddEntry(csTmp, -1);
			}
			iRtrn = SelectString(-1, csTmp);
		}	// End of test if user wants to force the string into place
	}	// End of test if couldn't select the item

	return iRtrn;
}	// CTComboBox::SelectCode()

int CTComboBox::SelectCode(int iStartAfter, int iCode, bool bForceFill /*= true*/)
{
	return (SelectCode(iStartAfter, (DWORD) iCode, bForceFill));
}	// CTComboBox::SelectCode(int iStartAfter, int iCode)

int CTComboBox::SelectCode(int iStartAfter, long lCode, bool bForceFill /*= true*/)
{
	return (SelectCode(iStartAfter, (DWORD) lCode, bForceFill));
}	// CTComboBox::SelectCode(int iStartAfter, long lCode)

int CTComboBox::SelectCode(int iStartAfter, DWORD dwCode, bool bForceFill /*= true*/)
{
	int
		iRtrn = FindCode(iStartAfter, dwCode);

	// Now select the item if we can
	if (iRtrn != CB_ERR)
		this->SetCurSel(iRtrn);
	else
	{
		// If forcefill is true then force it into the combobox
		if (bForceFill)
		{
			CString
				csTmp;
			csTmp.Format(_T("%s%ld%s"), (LPCTSTR)FORCED_PREFIX,
										dwCode,
										(LPCTSTR)FORCED_POSTFIX);
			switch (m_eType)
			{
			case String:
			case LPTSTRing:
				AddEntry(csTmp, _T(""));
				break;

			case Int:
			case Long:
			case Double:
			case Unknown:
				AddEntry(csTmp, dwCode);
			}
			iRtrn = SelectString(-1, csTmp);
		}	// End of test if user wants to force the string into place
	}	// End of test if couldn't select the item

	return iRtrn;
}	// CTComboBox::SelectCode(int iStartAfter, DWORD dwCode)

int CTComboBox::SelectText(CString &rcsTextToSelect, bool bForceFill /*= true*/)
{
	int
		iRtrn = CB_ERR;

	// Make sure string isn't empty
	if (!((rcsTextToSelect.GetLength() == 0) || (rcsTextToSelect == _T(""))))
	{
		// Try to select string
		iRtrn = SelectString(-1, rcsTextToSelect);

		// Was item NOT selected & did user request force fill?
		if ((iRtrn == CB_ERR) && bForceFill)
		{
			CString
				csTmp;
			csTmp.Format(_T("%s%s%s"), (LPCTSTR)FORCED_PREFIX,
										(LPCTSTR)rcsTextToSelect,
										(LPCTSTR)FORCED_POSTFIX);
			switch (m_eType)
			{
			case String:
			case LPTSTRing:
				AddEntry(csTmp, _T(""));
				break;

			case Int:
			case Long:
			case Double:
			case Unknown:
				AddEntry(csTmp, -1);
				break;
			}
			iRtrn = SelectString(-1, csTmp);
		}	// Add the code in if necessary
	}	// End of check if string isn't empty

	return iRtrn;
}	// CTComboBox::SelectText()
