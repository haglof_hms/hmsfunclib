#ifndef _TEMPLATEPARSER_H_
#define _TEMPLATEPARSER_H_

#include "pad_transaction_classes.h"
#include "pad_calculation_classes.h"

#include "xmlliteBase.h"

class TemplateParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	void setAttr(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name,LPCTSTR value);

	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	TemplateParser(void);

	virtual ~TemplateParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL getTemplateName(LPTSTR);
	BOOL setTemplateName(LPCTSTR);
	BOOL getTemplateDoneBy(LPTSTR);
	BOOL getTemplateDate(LPTSTR);
	BOOL getTemplateNotes(LPTSTR);
	
	BOOL getTemplateExchange(int *id,LPTSTR func_name);
	BOOL getTemplatePricelist(int *id,LPTSTR func_name);
	BOOL setTemplatePricelist(int id,LPCTSTR func_name);
	BOOL getTemplatePricelistTypeOf(int *type_of);
	
	BOOL getTemplateDCLS(double*);
	BOOL getTemplateHgtOverSea(int*);
	BOOL getTemplateLatitude(int*);
	BOOL getTemplateLongitude(int*);
	BOOL getTemplateGrowthArea(LPTSTR);
	BOOL getTemplateSI_H100(LPTSTR);
	BOOL getTemplateCostsTmpl(int*,LPTSTR);
	BOOL setTemplateCostsTmpl(int,LPCTSTR);
	// "S�derbergs" specific data; 071206 p�d
	BOOL getIsAtCoast(int*);
	BOOL getIsSouthEast(int*);
	BOOL getIsRegion5(int*);
	BOOL getPartOfPlot(int*);
	BOOL getSIH100_Pine(LPTSTR);

	BOOL getSpeciesInTemplateFile(vecTransactionSpecies &);

	// Methods related to specie in template; 070828 p�d
	BOOL getFunctionsForSpecie(int spc_id,vecUCFunctionList &,double *m3fub_m3to,double *m3sk_m3ub_const);

	BOOL getMiscDataForSpecie(int spc_id,int *,LPTSTR,double*,double*,double*,int*,int*,double*);
	BOOL getGrotDataForSpecie(int spc_id,int *,double*,double*,double*);
	BOOL getTransfersForSpecie(int spc_id,vecTransactionTemplateTransfers &);

	BOOL getObjTmplP30(vecObjectTemplate_p30_table &,CString& name,CString& done_by,CString& notes);

	BOOL getObjTmplP30_nn(vecObjectTemplate_p30_nn_table &,CString &area_name,int *area_index);
	BOOL getObjTmplP30_nn_notes(CString &notes);
	BOOL getObjTmplP30_nn_name(CString &name);
	BOOL getObjTmplP30_nn_done_by(CString &done_by);
	BOOL getObjTmplP30_nn_related_species(mapShortAndBool &pine_map,mapShortAndBool &spruce_map);
	BOOL getObjTmplHCost(int *type_set,double *break_value,double *factor,vecObjectTemplate_hcost_table &,CString& name,CString& done_by);


	BOOL getXML(CString &xml);
};


class JustSaveParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	JustSaveParser(void);

	virtual ~JustSaveParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);
};


// Parser based on xmlLite; 100112 p�d

class xmlliteTemplateParser : public CxmlliteBase
{
	protected:
public:
	xmlliteTemplateParser(void);
	virtual ~xmlliteTemplateParser();
	
	BOOL getTemplateName(LPTSTR);
	BOOL getTemplateDoneBy(LPTSTR);
	BOOL getTemplateDate(LPTSTR);
	
	BOOL getTemplateExchange(int *id,LPTSTR func_name);
	BOOL getTemplatePricelist(int *id,LPTSTR func_name);
	BOOL getTemplatePricelistTypeOf(int *type_of);
	
	BOOL getTemplateDCLS(double*);
	BOOL getTemplateHgtOverSea(int*);
	BOOL getTemplateLatitude(int*);
	BOOL getTemplateGrowthArea(LPTSTR);
	BOOL getTemplateSI_H100(LPTSTR);
	BOOL getTemplateCostsTmpl(int*,LPTSTR);
	// "S�derbergs" specific data; 071206 p�d
	BOOL getIsAtCoast(int*);
	BOOL getIsSouthEast(int*);
	BOOL getIsRegion5(int*);
	BOOL getPartOfPlot(int*);
	BOOL getSIH100_Pine(LPTSTR);

	BOOL getSpeciesInTemplateFile(vecTransactionSpecies &);

	// Methods related to specie in template; 070828 p�d
	BOOL getFunctionsForSpecie(int spc_id,vecUCFunctionList &,double *m3fub_m3to);

	BOOL getMiscDataForSpecie(int spc_id,int *,LPTSTR,double*,double*,double*,int*,int*,double*);
	BOOL getTransfersForSpecie(int spc_id,vecTransactionTemplateTransfers &);

	BOOL getObjTmplP30(vecObjectTemplate_p30_table &,CString& name,CString& done_by,CString& notes);

	BOOL getObjTmplP30_nn(vecObjectTemplate_p30_nn_table &,CString &area_name,int *area_index);
	BOOL getObjTmplP30_nn_notes(CString &notes);
	BOOL getObjTmplP30_nn_name(CString &name);
	BOOL getObjTmplP30_nn_done_by(CString &done_by);
	BOOL getObjTmplP30_nn_related_species(mapShortAndBool &pine_map,mapShortAndBool &spruce_map);
	BOOL getObjTmplHCost(int *type_set,double *break_value,double *factor,vecObjectTemplate_hcost_table &,CString& name,CString& done_by);

};


#endif