#include "StdAfx.h"
#include "PricelistParser.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


PricelistParser::PricelistParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

PricelistParser::~PricelistParser()
{
	pDomDoc->loadXML(_bstr_t(""));
	pDomDoc = NULL;
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL PricelistParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL PricelistParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL PricelistParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString PricelistParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double PricelistParser::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

int PricelistParser::getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	int nValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		nValue = _tstoi(szData);
	}	// if (pAttr)

	return nValue;
}

BOOL PricelistParser::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

// Methods for reading Header information
BOOL PricelistParser::getHeaderName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_HEADER_NAME);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL PricelistParser::setHeaderName(LPCTSTR data)
{
	CComBSTR bstrData(data);
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_HEADER_NAME);
	if (pNode)
	{	
		pNode->put_text(bstrData);
		return TRUE;
	}

	return FALSE;
}

BOOL PricelistParser::getHeaderDoneBy(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_HEADER_DONE_BY);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));

		return TRUE;
	}

	return FALSE;
}

BOOL PricelistParser::getHeaderNotes(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_HEADER_NOTES);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL PricelistParser::getHeaderDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_HEADER_DATE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL PricelistParser::getHeaderPriceIn(int *data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_HEADER_PRICE_IN);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*data = _tstoi(_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL PricelistParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}


BOOL PricelistParser::getSpeciesInPricelistFile(vecTransactionSpecies &vec)
{
	CComBSTR bstrData;
	long lNumOf;
	int nSpcID = -1;
	int nP30SpcID=0;
	TCHAR szSpcName[127];
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	vec.clear();

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));
	if (pNodeList)
	{	
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pChild_spcid = attributes1->getNamedItem(_bstr_t("id"));
				if (pChild_spcid)
				{
					pChild_spcid->get_text( &bstrData );
					nSpcID = _tstoi(_bstr_t(bstrData));

				}	// if (pChild_spcid)
				MSXML2::IXMLDOMNodePtr pChild_spcname = attributes1->getNamedItem(_bstr_t("name"));
				if (pChild_spcname)
				{
					pChild_spcname->get_text( &bstrData );
					_tcscpy(szSpcName,_bstr_t(bstrData));

				}	// if (pChild_spcname)


				vec.push_back(CTransaction_species(l+1,nSpcID,0,szSpcName));
				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList = NULL;
		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

BOOL PricelistParser::getAssortmentPerSpecie(vecTransactionAssort &vec)
{
	long lNumOfAssort;
	int nSpcID = -1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pParentNode = NULL;
	vec.clear();
				
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_SPECIES_ASSORTMENTS));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfAssort );
		for (long l1 = 0;l1 < lNumOfAssort;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode)
			{
				pSubNode->get_parentNode(&pParentNode);
				if (pParentNode)
				{
					pParentNode->get_attributes( &attributes1 );
					MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));
					if (pAttrChild1)
					{
						// Try to find specie and add assortments for this specie; 060410 p�d
						nSpcID = _tstoi(pAttrChild1->text);
						pAttrChild1 = NULL;
					}
				}

				pSubNodeList->item[l1]->get_attributes( &attributes2 );

				vec.push_back(CTransaction_assort(nSpcID,getAttrText(attributes2,_T("assortname")),
																								 getAttrDouble(attributes2,_T("min_diam")),
																								 getAttrDouble(attributes2,_T("price_m3fub")),
																								 getAttrDouble(attributes2,_T("price_m3to")),
																								 getAttrInt(attributes2,_T("massa")) ));

				pSubNode = NULL;		
			}	// if (pSubNode)
		}	// for (long l1 = 0;l1 < lNumOfAssort;l1++)

		pSubNodeList = NULL;

		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL PricelistParser::getDCLSForSpecie(int spc_id,vecTransactionDiameterclass &vec)
{
	CString S;
	long lNumOf,lNumOfDCLS;
	int nSpcID = -1;
	int nStartDiam1,nStartDiam2,nDCLS1,nDCLS2,nNumOfDCLS1;
	TCHAR szID1[50];
	TCHAR szID2[50];
	BSTR szAttrID1;
	BSTR szAttrID2;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	
	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));

	nStartDiam1 = nStartDiam2 = nDCLS1 = nDCLS2 = nNumOfDCLS1 = 0;

	vec.clear();
	if (pNodeList)
	{	
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				nSpcID = _tstoi(pAttrChild1->text);
				if (spc_id == nSpcID || spc_id == -1)
				{
					MSXML2::IXMLDOMNodePtr pDCLSChild = pChild->selectSingleNode(_bstr_t("DiamClasses"));
					pDCLSChild->get_attributes( &attributes2 );

					attributes2->get_length( &lNumOfDCLS );

					for (long l1 = 0;l1 < lNumOfDCLS;l1++)
					{
						_stprintf(szID1,_T("d%d"),l1+1);
						szAttrID1 = _bstr_t(szID1);
						pAttrChild2 = attributes2->getNamedItem(szAttrID1);

						if (pAttrChild2)
						{
							nStartDiam1 = _tstoi(pAttrChild2->text);
						}
						_stprintf(szID2,_T("d%d"),l1+2);
						szAttrID2 = _bstr_t(szID2);
						pAttrChild3 = attributes2->getNamedItem(szAttrID2);	

						if (pAttrChild3)
						{
							nStartDiam2 = _tstoi(pAttrChild3->text);
						}
						if ((nStartDiam2 - nStartDiam1) > 0)
						{
							nDCLS1 = (nStartDiam2 - nStartDiam1);
						}
						nNumOfDCLS1++;

						vec.push_back(CTransaction_diameterclass(spc_id,nStartDiam1,nDCLS1,nNumOfDCLS1));

						if (nDCLS1 != nDCLS2 && nDCLS2 > 0)
						{
							nNumOfDCLS1 = 0;
						}

						nDCLS2 = nDCLS1;
					}

				}	// if (spc_id == nSpcID)
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}


BOOL PricelistParser::getPriceForSpecie(int spc_id,vecTransactionPrlData &vec)
{
	long lNumOf,lNumOfDCLS;
	int nSpcID = -1;
	int nPrice = 0;
	TCHAR szID1[50];
	TCHAR szQualName[50];
	BSTR szAttrID1;
	vecInt m_vecInt;

	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pAttrChild1 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));

	vec.clear();
	if (pNodeList)
	{	
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				nSpcID = _tstoi(pAttrChild1->text);
				if (spc_id == nSpcID || spc_id == -1)
				{
					for (pAttrChild2 = pChild->firstChild;
							 NULL != pAttrChild2;
							 pAttrChild2 = pAttrChild2->nextSibling)
					{
						if (_tcscmp(pAttrChild2->nodeName,_T("PricePerQuality")) == 0)					
						{
							pAttrChild2->get_attributes( &attributes2 );

							attributes2->get_length( &lNumOfDCLS );
					
							m_vecInt.clear();

							for (long l1 = 0;l1 < lNumOfDCLS;l1++)
							{
								if (l1 == 0)
								{
									szAttrID1 = _bstr_t("qualname");
									pAttrChild3 = attributes2->getNamedItem(szAttrID1);
									_tcscpy(szQualName,pAttrChild3->text);
								}
								else if (l1 > 0)
								{
									_stprintf(szID1,_T("d%d"),l1);
									szAttrID1 = _bstr_t(szID1);
									pAttrChild3 = attributes2->getNamedItem(szAttrID1);
									nPrice = _tstoi(pAttrChild3->text);
									m_vecInt.push_back(nPrice);
								}	// if (l1 > 0)
							}	// for (long l1 = 0;l1 < lNumOfDCLS;l1++)
							
							// Add each PricePerQuality row to vector; 060411 p�d
							vec.push_back(CTransaction_prl_data(nSpcID,nSpcID,szQualName,m_vecInt));

						} // if (_tcscmp(pAttrChild1->nodeName,_T("PricePerQuality")) == 0)
					}	// for (pAttrChild1 = pChild->firstChild;
				}	// if (spc_id == nSpcID)
			
				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList =NULL;
		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

BOOL PricelistParser::getQualDescNames(CStringArray &vec)
{
	long lNumOf;
	int nSpcID = -1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));

	vec.RemoveAll();	// Clear
	if (pNodeList)
	{	
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				for (pAttrChild2 = pChild->firstChild;
						 NULL != pAttrChild2;
						 pAttrChild2 = pAttrChild2->nextSibling)
				{
					if (_tcscmp(pAttrChild2->nodeName,_T("QualityDesc")) == 0)					
					{
						pAttrChild2->get_attributes( &attributes2 );

						pAttrChild3 = attributes2->getNamedItem(_bstr_t("name"));

						if (pAttrChild3)
						{
							vec.Add(pAttrChild3->text);
						}	// if (pAttrChild3)
					} // if (_tcscmp(pAttrChild1->nodeName,_T("QualityDesc")) == 0)
				}	// for (pAttrChild1 = pChild->firstChild;
		
				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList = NULL;

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}


BOOL PricelistParser::getQualDescNameForSpecie(int spc_id,CStringArray &vec)
{
	long lNumOf;
	int nSpcID = -1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));

	vec.RemoveAll();	// Clear
	if (pNodeList)
	{	
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				nSpcID = _tstoi(pAttrChild1->text);
				if (spc_id == nSpcID || spc_id == -1)
				{
					for (pAttrChild2 = pChild->firstChild;
							 NULL != pAttrChild2;
							 pAttrChild2 = pAttrChild2->nextSibling)
					{
						if (_tcscmp(pAttrChild2->nodeName,_T("QualityDesc")) == 0)					
						{
							pAttrChild2->get_attributes( &attributes2 );

							pAttrChild3 = attributes2->getNamedItem(_bstr_t("name"));

							if (pAttrChild3)
							{
								vec.Add(pAttrChild3->text);
							}	// if (pAttrChild3)
						} // if (_tcscmp(pAttrChild1->nodeName,_T("QualityDesc")) == 0)
					}	// for (pAttrChild1 = pChild->firstChild;
				}	// if (spc_id == nSpcID)

				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList = NULL;

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

BOOL PricelistParser::getQualDescNameForSpecie(int spc_id,vecTransactionQualDesc &vec)
{
	long lNumOf;
	int nSpcID = -1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));

	vec.clear();	// Clear
	if (pNodeList)
	{	
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				nSpcID = _tstoi(pAttrChild1->text);
				if (spc_id == nSpcID || spc_id == -1)
				{
					for (pAttrChild2 = pChild->firstChild;
							 NULL != pAttrChild2;
							 pAttrChild2 = pAttrChild2->nextSibling)
					{
						if (_tcscmp(pAttrChild2->nodeName,_T("QualityDesc")) == 0)					
						{
							pAttrChild2->get_attributes( &attributes2 );

							pAttrChild3 = attributes2->getNamedItem(_bstr_t("name"));

							if (pAttrChild3)
							{
								vec.push_back(CTransaction_qual_desc(nSpcID,pAttrChild3->text));
							}	// if (pAttrChild3)
						} // if (_tcscmp(pAttrChild1->nodeName,_T("QualityDesc")) == 0)
					}	// for (pAttrChild1 = pChild->firstChild;
				}	// if (spc_id == nSpcID)

				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList = NULL;

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}


BOOL PricelistParser::getQualDescPercentForSpecie(int spc_id,vecTransactionPrlData &vec)
{
	long lNumOf1,lNumOf2,lNumOf3;
	int nSpcID = -1;
	int nPercent = 0;
	int nIdentifer = 0;
	TCHAR szID1[50];
	TCHAR szQualName[50];
	BSTR szAttrID1;
	vecInt m_vecInt;

	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pChild = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild4 = NULL;
	MSXML2::IXMLDOMNodeListPtr pNodeList1 = NULL;
	MSXML2::IXMLDOMNodeListPtr pNodeList2 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = NULL;

	pRoot = pDomDoc->documentElement;
	pNodeList1 = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));
	vec.clear();
	if (pNodeList1)
	{	
		pNodeList1->get_length( &lNumOf1 );
		for (long l = 0;l < lNumOf1;l++)
		{
			pChild = pNodeList1->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				nSpcID = _tstoi(pAttrChild1->text);
				if (spc_id == nSpcID || spc_id == -1)
				{
					for (pAttrChild2 = pChild->firstChild;
							 NULL != pAttrChild2;
							 pAttrChild2 = pAttrChild2->nextSibling)
					{
						// Found a QualityDescription node; 060411 p�d
						if (_tcscmp(pAttrChild2->nodeName,_T("QualityDesc")) == 0)					
						{
							pAttrChild2->get_childNodes( &pNodeList2 );
							if (pNodeList2)
							{
								pNodeList2->get_length( &lNumOf2 );
								if (lNumOf2 > 0)
								{
									for (long l1 = 0;l1 < lNumOf2;l1++)
									{
										pAttrChild3 = pNodeList2->item[l1];
										if (pAttrChild3)
										{
											pAttrChild3->get_attributes( &attributes2 );
											
											attributes2->get_length( &lNumOf3 );

											m_vecInt.clear();

											for (long l2 = 0;l2 < lNumOf3;l2++)
											{
												if (l2 == 0)
												{
													szAttrID1 = _bstr_t("qualname");
													pAttrChild4 = attributes2->getNamedItem(szAttrID1);
													_tcscpy(szQualName,pAttrChild4->text);
												}
												else if (l2 > 0)
												{
													_stprintf(szID1,_T("d%d"),l2);
													szAttrID1 = _bstr_t(szID1);
													pAttrChild4 = attributes2->getNamedItem(szAttrID1);
													nPercent = _tstoi(pAttrChild4->text);
													m_vecInt.push_back(nPercent);
												}	// if (l1 > 0)
											}	// for (long l1 = 0;l1 < lNumOfDCLS;l1++)
											pAttrChild3 =NULL;
										}	// if (pAttrChild3)

										// Add each QualityDescription row to vector; 060411 p�d
										vec.push_back(CTransaction_prl_data(nSpcID,nIdentifer,szQualName,m_vecInt));
									} // for (long l1 = 0;l1 < lNumOf2;l1++)					
								} // if (lNumOf2 > 0)
							} // if (pNodeList)
							// Count number of Qualitydescriptions; 060411 p�d
							nIdentifer++;
						} // if (_tcscmp(pAttrChild1->nodeName,_T("QualityDesc")) == 0)
					}	// for (pAttrChild1 = pChild->firstChild;
				}	// if (spc_id == nSpcID)
			
				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList1 = NULL;

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

BOOL PricelistParser::getQualDescPercentForQualityAndSpecie(int spc_id,LPCTSTR qual_name,vecTransactionPrlData &vec)
{
	long lNumOf1,lNumOf2,lNumOf3;
	int nSpcID = -1;
	int nPercent = 0;
	int nIdentifer = 0;
	TCHAR szID1[50];
	TCHAR szQualName[50];
	TCHAR szQuality[50];
	BSTR szAttrID1;
	vecInt m_vecInt;
	CString S;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1_1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pChild = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild2 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild3_1 = NULL;
	MSXML2::IXMLDOMNodePtr pAttrChild4 = NULL;
	MSXML2::IXMLDOMNodeListPtr pNodeList1 = NULL;
	MSXML2::IXMLDOMNodeListPtr pNodeList2 = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = NULL;

	pRoot = pDomDoc->documentElement;
	pNodeList1 = pRoot->getElementsByTagName(_bstr_t(NODE_SPECIES));
	vec.clear();
	if (pNodeList1)
	{	
		pNodeList1->get_length( &lNumOf1 );
		for (long l = 0;l < lNumOf1;l++)
		{
			pChild = pNodeList1->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pAttrChild1 = attributes1->getNamedItem(_bstr_t("id"));

				nSpcID = _tstoi(pAttrChild1->text);
				if (spc_id == nSpcID)
				{
					for (pAttrChild2 = pChild->firstChild;
							 NULL != pAttrChild2;
							 pAttrChild2 = pAttrChild2->nextSibling)
					{
						// Found a QualityDescription node; 060411 p�d
						if (_tcscmp(pAttrChild2->nodeName,_T("QualityDesc")) == 0)					
						{
							pAttrChild2->get_attributes( &attributes1_1 );
							pAttrChild3_1 = attributes1_1->getNamedItem(_bstr_t("name"));
							if (pAttrChild3_1)
							{
								_tcscpy(szQuality,pAttrChild3_1->text);
								pAttrChild3_1 = NULL;
							}	// if (pAttrChild3)						

							if (_tcscmp(szQuality,qual_name) == 0)
							{

								pAttrChild2->get_childNodes( &pNodeList2 );
								if (pNodeList2)
								{

									pNodeList2->get_length( &lNumOf2 );
									if (lNumOf2 > 0)
									{
										for (long l1 = 0;l1 < lNumOf2;l1++)
										{
											pAttrChild3 = pNodeList2->item[l1];
											if (pAttrChild3)
											{
												pAttrChild3->get_attributes( &attributes2 );
												
										
												attributes2->get_length( &lNumOf3 );

												m_vecInt.clear();

												for (long l2 = 0;l2 < lNumOf3;l2++)
												{
													if (l2 == 0)
													{
														szAttrID1 = _bstr_t("qualname");
														pAttrChild4 = attributes2->getNamedItem(szAttrID1);
														_tcscpy(szQualName,pAttrChild4->text);
													}
													else if (l2 > 0)
													{
														_stprintf(szID1,_T("d%d"),l2);
														szAttrID1 = _bstr_t(szID1);
														pAttrChild4 = attributes2->getNamedItem(szAttrID1);
														nPercent = _tstoi(pAttrChild4->text);
														m_vecInt.push_back(nPercent);
													}	// if (l1 > 0)
												}	// for (long l1 = 0;l1 < lNumOfDCLS;l1++)
												pAttrChild3 = NULL;
											}	// if (pAttrChild3)

											// Add each QualityDescription row to vector; 060411 p�d
											vec.push_back(CTransaction_prl_data(nSpcID,nIdentifer,szQualName,m_vecInt));
											
										} // for (long l1 = 0;l1 < lNumOf2;l1++)					
									} // if (lNumOf2 > 0)
									pNodeList2 = NULL;
								} // if (pNodeList2)
							}
							// Count number of Qualitydescriptions; 060411 p�d
							nIdentifer++;
						} // if (_tcscmp(pAttrChild1->nodeName,_T("QualityDesc")) == 0)
					}	// for (pAttrChild1 = pChild->firstChild;
				}	// if (spc_id == nSpcID)

				pChild = NULL;
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		pNodeList1 = NULL;

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// xmllitePricelistParser; 2009-12-10 P�D

xmllitePricelistParser::xmllitePricelistParser(void)
	: CxmlliteBase()
{
}

xmllitePricelistParser::~xmllitePricelistParser()
{
}

// Methods for Loading and Saving xml file(s); 060407 p�d

// Public

// Methods for reading Header information
BOOL xmllitePricelistParser::getHeaderName(LPTSTR data)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,L"Name") == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				_tcscpy(data,pwszValue);
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return TRUE;
}

BOOL xmllitePricelistParser::getHeaderDoneBy(LPTSTR data)
{
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,L"Done_by") == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				_tcscpy(data,pwszValue);
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return TRUE;
}

BOOL xmllitePricelistParser::getHeaderNotes(LPTSTR data)
{
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,L"Notes") == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				_tcscpy(data,pwszValue);
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return TRUE;
}

BOOL xmllitePricelistParser::getHeaderDate(LPTSTR data)
{
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,L"Date") == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				_tcscpy(data,pwszValue);
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return TRUE;
}

BOOL xmllitePricelistParser::getHeaderPriceIn(int *data)
{
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;

	if (!isReader()) return FALSE;

	readerRewind();

	while (getReader()->Read(&nodeType) == S_OK)
	{
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
		}
		if (nodeType == XmlNodeType_Text)
		{
			if (_tcscmp(pwszLocalName,L"Timberprice_in") == 0) 
			{
				getReader()->GetValue(&pwszValue, NULL);
				*data = _tstoi(pwszValue);
				break;
			}	// if (nodeType == XmlNodeType_Text)
		}	// if (_tcscmp(pwszLocalName,L"Name") == 0) 
	}	// while (getReader()->Read(&nodeType))
	
	return TRUE;
}

/*
BOOL xmllitePricelistParser::getXML(CString &xml)
{
	xml = m_sXML.c_str();
	return FALSE;
}
*/

BOOL xmllitePricelistParser::getSpeciesInPricelistFile(vecTransactionSpecies &vec)
{
	CString sSpcName;
	int nSpcID;
	int nAttrCount = 0;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;
	int nCounter = 1;

	readerRewind();

	vec.clear();

	// Read off xml file
	while(getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				nAttrCount = 0;
				nSpcID=0;
				sSpcName=_T("");
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						nAttrCount++;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					if (_tcscmp(pwszLocalAttrName,L"name") == 0)
					{
						sSpcName = pwszValue;
						nAttrCount++;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = pReader->MoveToFirstAttribute();S_OK == result;result = pReader->MoveToNextAttribute())
				if (nAttrCount == 2)
				{
					vec.push_back(CTransaction_species(nCounter,nSpcID,0,sSpcName));
					nCounter++;
				}
			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 
		}	// if( nodeType == XmlNodeType_Element )
	}	// while( pReader->Read(&nodeType) == S_OK )

	return TRUE;
}

BOOL xmllitePricelistParser::getAssortmentPerSpecie(vecTransactionAssort &vec)
{
	CString sSpcName,S;
	int nSpcID;
	CString sAssortName;
	double fMinDiam = 0.0;
	double fPriceM3Fub = 0.0;
	double fPriceM3To = 0.0;
	short nPulpWood = 0;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	BOOL bFound = FALSE;
	XmlNodeType nodeType = XmlNodeType_None;

	readerRewind();

	vec.clear();
	// Read off xml file
	while(getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;

				int nAttrCount = 0;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 

			if (_tcscmp(pwszLocalName,L"Assortments") == 0 && bFound) 
			{
				int nAttrCount = 0;
				// Iterate through values; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetValue(&pwszValue, NULL);
					switch(nAttrCount)
					{
						case 0 : sAssortName = pwszValue; break;
						case 1 : fMinDiam = _tstof(pwszValue); break;
						case 2 : fPriceM3Fub = _tstof(pwszValue); break;
						case 3 : fPriceM3To = _tstof(pwszValue); break;
						case 4 : 
						{
							nPulpWood = _tstoi(pwszValue); 
							vec.push_back(CTransaction_assort(nSpcID,sAssortName,fMinDiam,fPriceM3Fub,fPriceM3To,nPulpWood));

//							S.Format(L"%d  %s  %f  %f  %f  %d",nSpcID,sAssortName,fMinDiam,fPriceM3Fub,fPriceM3To,nPulpWood);
//							AfxMessageBox(S);

							break;
						}
					};
					nAttrCount++;
				}
			}	// if (_tcscmp(pwszLocalName,L"Assortments") == 0 && bFound) 
		}	// if( nodeType == XmlNodeType_Element )
	}	// while(getReader()->Read(&nodeType) == S_OK )

	return TRUE;
}

BOOL xmllitePricelistParser::getDCLSForSpecie(int spc_id,vecTransactionDiameterclass &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;
	int nSpcID = 0;
	CString S;
	UINT nLinePos;

	readerRewind();

	vec.clear();
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;

				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						break;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 
			if (bFound && (nSpcID == spc_id || spc_id == -1))
			{
						CString S;
				int nStartDiam1,nStartDiam2,nLastDCLS,nDCLS;
				vecInt dcls_classes;
				if (_tcscmp(pwszLocalName,L"DiamClasses") == 0) 
				{
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetValue(&pwszValue, NULL);
						dcls_classes.push_back(_tstoi(pwszValue));
					}
					// Iterate through diamterclasses an calculate diamterclasses used
					// between each dcls; 091216 p�d
					if (dcls_classes.size() > 0)
					{
						for (UINT i = 0;i < dcls_classes.size();i++)
						{
							nStartDiam1 = dcls_classes[i];
							if (i <  dcls_classes.size()-1)
								nStartDiam2 = dcls_classes[i+1];
							// Calculate diamterclass until we reach the
							// second last, the diamterclass for the last entry
							// 'll be set to the diamterclass for the second last entry; 091216 p�d
							if (i < dcls_classes.size()-1)
							{
								nLastDCLS = (nStartDiam2-nStartDiam1);
							}
							vec.push_back(CTransaction_diameterclass(spc_id,nStartDiam1,nLastDCLS,i+1));
				
						}	// for (UINT i = 0;i < dcls_classes.size();i++)
					}	// if (dcls_classes.size() > 0)
					dcls_classes.clear();
				}
			}
		}	// if( nodeType == XmlNodeType_Element )

	}
	return TRUE;
}


BOOL xmllitePricelistParser::getPriceForSpecie(int spc_id,vecTransactionPrlData &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];

	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;
	int nSpcID = 0;
	int nCounter = 0;
	vecInt vInts;

	readerRewind();

	vec.clear();
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;

				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						break;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 
			if (bFound && (nSpcID == spc_id || spc_id == -1))
			{
				if (_tcscmp(pwszLocalName,L"PricePerQuality") == 0) 
				{
					vInts.clear();
					nCounter = 0;
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetValue(&pwszValue, NULL);
						if (nCounter == 0)	// Name of quality; 091216 p�d
						{
							_tcscpy(szQualName,pwszValue);
						}	
						else	// Price per quality and diamterclass; 091216 p�d
						{
							vInts.push_back(_tstoi(pwszValue));
						}
						nCounter++;
					}
					// Add each PricePerQuality row to vector; 091216 p�d
					vec.push_back(CTransaction_prl_data(nSpcID,nSpcID,szQualName,vInts));
				}
			}
		}	// if( nodeType == XmlNodeType_Element )

	}
	return TRUE;
}

BOOL xmllitePricelistParser::getQualDescNames(CStringArray &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];

	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;

	readerRewind();

	vec.RemoveAll();
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;
			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 
		}	// if( nodeType == XmlNodeType_Element )
		if (bFound)
		{
			if (_tcscmp(pwszLocalName,L"QualityDesc") == 0) 
			{
				// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetValue(&pwszValue, NULL);
					vec.Add(pwszValue);
				}
			}
		}
	}
	return TRUE;
}


BOOL xmllitePricelistParser::getQualDescNameForSpecie(int spc_id,CStringArray &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];
	int nSpcID = 0;

	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;

	readerRewind();

	vec.RemoveAll();
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						break;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 

			if (bFound && (nSpcID == spc_id || spc_id == -1))
			{
				if (_tcscmp(pwszLocalName,L"QualityDesc") == 0) 
				{
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetValue(&pwszValue, NULL);
						vec.Add(pwszValue);
					}
				}
			}
		}	// if( nodeType == XmlNodeType_Element )
	}
	return TRUE;
}

BOOL xmllitePricelistParser::getQualDescNameForSpecie(int spc_id,vecTransactionQualDesc &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];
	int nSpcID = 0;

	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;

	readerRewind();

	vec.clear();
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						break;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 

			if (bFound && (nSpcID == spc_id || spc_id == -1))
			{
				if (_tcscmp(pwszLocalName,L"QualityDesc") == 0) 
				{
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetValue(&pwszValue, NULL);
						vec.push_back(CTransaction_qual_desc(nSpcID,pwszValue));
					}
				}
			}
		}	// if( nodeType == XmlNodeType_Element )
	}
	return TRUE;
}


BOOL xmllitePricelistParser::getQualDescPercentForSpecie(int spc_id,vecTransactionPrlData &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];
	TCHAR szFirstQualName[127];
	int nSpcID = 0;
	int nNumOfQualtiDesc = 0;
	int nCounter = 0;
	vecInt vInts;
	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;
	BOOL bDoAdd = FALSE;

	CString S;

	readerRewind();

	vec.clear();
	bDoAdd = FALSE;
	nNumOfQualtiDesc = 0;
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						break;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 

			if (bFound && (nSpcID == spc_id || spc_id == -1))
			{
				//---------------------------------------------------------------------
				// Find element "QualityDesc" for specie in XML; 091216 p�d
				getReader()->GetLocalName(&pwszLocalName, NULL);		
				//---------------------------------------------------------------------
				// Read percen per quality for "QualityDesc" for specie in XML; 091216 p�d
				if (_tcscmp(pwszLocalName,L"Percent") == 0) 
				{
					vInts.clear();
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetLocalName(&pwszLocalAttrName, NULL);
						getReader()->GetValue(&pwszValue, NULL);
						if (_tcscmp(pwszLocalAttrName,L"qualname") == 0)
						{
							_tcscpy(szQualName,pwszValue);
							if (nCounter == 0) 
								_tcscpy(szFirstQualName,szQualName);
						}
						else
						{
							vInts.push_back(_tstoi(pwszValue));
						}
					}
					// Add each QualityDescription row to vector; 091216 p�d
					if (nCounter > 0 && _tcscmp(szFirstQualName,szQualName) == 0)
						nNumOfQualtiDesc++;

					vec.push_back(CTransaction_prl_data(nSpcID,nNumOfQualtiDesc,szQualName,vInts));
	
					nCounter++;
				}	// if (_tcscmp(pwszLocalName,L"Percent") == 0 && bDoAdd) 
			}	// if (bFound && (nSpcID == spc_id || spc_id == -1))
		}	// if( nodeType == XmlNodeType_Element )

	}
	return TRUE;
}

BOOL xmllitePricelistParser::getQualDescPercentForQualityAndSpecie(int spc_id,LPCTSTR qual_name,vecTransactionPrlData &vec)
{
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];
	int nSpcID = 0;
	int nNumOfQualtiDesc = 0;
	vecInt vInts;
	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;
	BOOL bDoAdd = FALSE;

	CString S;

	readerRewind();

	vec.clear();
	bDoAdd = FALSE;
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						break;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 

			if (bFound && (nSpcID == spc_id || spc_id == -1))
			{
				//---------------------------------------------------------------------
				// Find element "QualityDesc" for specie in XML; 091216 p�d
				getReader()->GetLocalName(&pwszLocalName, NULL);		
				if (_tcscmp(pwszLocalName,L"QualityDesc") == 0) 
				{
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetValue(&pwszValue, NULL);
						if (_tcscmp(pwszValue,qual_name) == 0)
						{
							bDoAdd = TRUE;
							break;
						}	// if (_tcscmp(pwszValue,qual_name) == 0)
						else
							bDoAdd = FALSE;
					}
				}	// if (_tcscmp(pwszLocalName,L"QualityDesc") == 0) 

				//---------------------------------------------------------------------
				// Read percen per quality for "QualityDesc" for specie in XML; 091216 p�d
				if (_tcscmp(pwszLocalName,L"Percent") == 0 && bDoAdd) 
				{
					nNumOfQualtiDesc = 0;
					vInts.clear();
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetLocalName(&pwszLocalAttrName, NULL);
						getReader()->GetValue(&pwszValue, NULL);
						if (_tcscmp(pwszLocalAttrName,L"qualname") == 0)
						{
							_tcscpy(szQualName,pwszValue);
						}
						else
						{
							vInts.push_back(_tstoi(pwszValue));
						}
					}
					// Add each QualityDescription row to vector; 091216 p�d
					vec.push_back(CTransaction_prl_data(nSpcID,nNumOfQualtiDesc,szQualName,vInts));
					nNumOfQualtiDesc++;
				}	// if (_tcscmp(pwszLocalName,L"Percent") == 0 && bDoAdd) 
			}	// if (bFound && (nSpcID == spc_id || spc_id == -1))
		}	// if( nodeType == XmlNodeType_Element )

	}
	return TRUE;
}
