/*	2006-03-17
*
*		Transaction classes; classes used for communication between Main program/Sutes/User modules
*
*/

#ifndef _TRANSACTION_CLASSES_H_
#define _TRANSACTION_CLASSES_H_

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class: CTransaction_prl_data. 

typedef std::vector<int> vecInt;

class CTransaction_prl_data
{
	int m_nIdentifer;
	CString m_sQualName;
	vecInt m_vecInt;		// Holds price/percentage values (dynamic)
public:
	CTransaction_prl_data(void);
	CTransaction_prl_data(int identifer,LPCSTR qual_name,vecInt &vec);
	// Copy constructor
	CTransaction_prl_data(const CTransaction_prl_data &c);

	int getIdentifer(void);
	LPCSTR getQualName(void);
	vecInt &getInts(void);
	void setInts(int value);

};

typedef std::vector<CTransaction_prl_data> vecTransactionPrlData;

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class. Transfers data from CDataEnterDlg
// via CPricelistsView to CPricelistsFormView. Setting values to Pricelist and Quality-
// description ReportControls (Grids); 060324 p�d

class CTransaction_diameterclass
{
	int m_nStartDiam;		// Start from this diamter (mm)
	int m_nDiamClass;		// Diamter class increment in (mm)
	int m_nNumOfDCLS;		// Number of diamters in this diamterclass
public:
	CTransaction_diameterclass(void);
	CTransaction_diameterclass(int,int,int);
	CTransaction_diameterclass(const	CTransaction_diameterclass &);

	int getStartDiam(void);
	int getDiamClass(void);
	int getNumOfDCLS(void);
};

typedef std::vector<CTransaction_diameterclass> vecTransactionDiameterclass;

// Species transaction classes; 060317 p�d
class CTransaction_species
{
	int m_nID;
	int m_nSpcID;
	CString m_sSpcName;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_species(void);

	CTransaction_species(int id,int spc_id = 0,LPCSTR spc_name = _T(""),LPCSTR notes = _T(""),LPCSTR created = _T(""));
	
	// Copy constructor
	CTransaction_species(const CTransaction_species &);


	int getID(void);
	int getSpcID(void);
	CString getSpcName(void);
	CString getNotes(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_species> vecTransactionSpecies;

typedef std::vector<int> vecIntegers;


// Assortment transaction classes; 060317 p�d
class CTransaction_assort
{
	int m_nSpcID;
	CString m_sAssortName;
	double m_fMinDiam;
	double m_fPriceM3fub;
	double m_fPriceM3to;
	BOOL m_bIsPulp;
	double m_fTranspReduction;
	double m_fStretch;
	double m_fMaxReduction;
public:
	// Default constructor
	CTransaction_assort(void);

	CTransaction_assort(int spc_id,
											LPCSTR assort_name,
											double min_diam,
											double price_m3fub,
											double price_m3to,
											BOOL is_pulp,
											double transp_reduction,
											double stretch,
											double max_reduction);

	
	// Copy constructor
	CTransaction_assort(const CTransaction_assort &);

	int getSpcID(void);
	CString getAssortName(void);
	double getMinDiam(void);
	double getPriceM3fub(void);
	double getPriceM3to(void);
	BOOL getIsPulp(void);
	double getTranspReduction(void);
	double getStretch(void);
	double getMaxReduction(void);

};

typedef std::vector<CTransaction_assort> vecTransactionAssort;

// Pricelist transaction classes; 060406 p�d
class CTransaction_pricelist
{
	int m_nID;
	CString m_sName;
	CString m_sPricelistFile;
	CString m_sCreatedBy;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_pricelist(void);

	CTransaction_pricelist(int id,LPCSTR name = _T(""),LPCSTR prl_name = _T(""),
																LPCSTR created_by = _T(""),LPCSTR created = _T(""));
	
	// Copy constructor
	CTransaction_pricelist(const CTransaction_pricelist &);


	int getID(void);
	CString getName(void);
	CString getPricelistFile(void);
	CString getCreatedBy(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_pricelist> vecTransactionPricelist;


// Postnumber transaction classes; 061129 p�d
class CTransaction_postnumber
{
	int m_nID;
	CString m_sName;
	CString m_sNumber;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_postnumber(void);

	CTransaction_postnumber(int id,LPCSTR name,
																 LPCSTR number,
																 LPCSTR created);
	
	// Copy constructor
	CTransaction_postnumber(const CTransaction_postnumber &);


	int getID(void);
	CString getName(void);
	CString getNumber(void);
	CString getCreated(void);
};

// Category transaction classes; 061129 p�d
class CTransaction_category
{
	int m_nID;
	CString m_sCategory;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_category(void);

	CTransaction_category(int id,LPCSTR category,
															 LPCSTR notes,
															 LPCSTR created);
	
	// Copy constructor
	CTransaction_category(const CTransaction_category &);


	int getID(void);
	CString getCategory(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_category> vecTransactionCategory;


// Contacts transaction classes; 061129 p�d
class CTransaction_contacts
{
	int m_nID;
	CString m_sPNR_ORGNR;
	CString m_sName;
	CString m_sCompany;
	CString m_sAddress;
	CString m_sPostNum;
	CString m_sPostAddress;
	CString m_sCounty;
	CString m_sCountry;
	CString m_sPhoneWork;
	CString m_sPhoneHome;
	CString m_sFaxNumber;
	CString m_sMobile;
	CString m_sEMail;
	CString m_sWebSite;
	CString m_sNotes;
	CString m_sCreatedBy;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_contacts(void);

	CTransaction_contacts(int id,
												LPCSTR pnr_orgnr,
												LPCSTR name,
												LPCSTR company,
												LPCSTR address,
												LPCSTR post_num,
												LPCSTR post_address,
												LPCSTR county,
												LPCSTR country,
												LPCSTR phone_work,
												LPCSTR phone_home,
												LPCSTR fax_number,
												LPCSTR mobile,
												LPCSTR e_mail,
												LPCSTR web_site,
												LPCSTR notes,
												LPCSTR created_by,
												LPCSTR created);
	
	// Copy constructor
	CTransaction_contacts(const CTransaction_contacts &);

	int getID(void);
	CString getPNR_ORGNR(void);
	CString getName(void);
	CString getCompany(void);
	CString getAddress(void);
	CString getPostNum(void);
	CString getPostAddress(void);
	CString getCounty(void);
	CString getCountry(void);
	CString getPhoneWork(void);
	CString getPhoneHome(void);
	CString getFaxNumber(void);
	CString getMobile(void);
	CString getEMail(void);
	CString getWebSite(void);
	CString getNotes(void);
	CString getCreatedBy(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_contacts> vecTransactionContacts;


// Category for contacts transaction classes; 061221 p�d
class CTransaction_categories_for_contacts
{
	int m_nContactID;
	int m_nCategoryID;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_categories_for_contacts(void);

	CTransaction_categories_for_contacts(int contact,
																			 int category,
																			 LPCSTR created);
	
	// Copy constructor
	CTransaction_categories_for_contacts(const CTransaction_categories_for_contacts &);


	int getContactID(void);
	void setContactID(int);
	int getCategoryID(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_categories_for_contacts> vecTransactionContactsForCategory;


// TraktType transaction classes; 070111 p�d
class CTransaction_trakt_type
{
	int m_nID;
	CString m_sTraktType;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_trakt_type(void);

	CTransaction_trakt_type(int id,LPCSTR type_name,
															 LPCSTR notes,
															 LPCSTR created);
	
	// Copy constructor
	CTransaction_trakt_type(const CTransaction_trakt_type &);


	int getID(void);
	CString getTraktType(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_trakt_type> vecTransactionTraktType;


// TraktType transaction classes; 070111 p�d
class CTransaction_county_municipal_parish
{
	int m_nID;
	int m_nCountyID;
	int m_nMunicipalID;
	int m_nParishID;
	CString m_sCountyName;
	CString m_sMunicipalName;
	CString m_sParishName;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_county_municipal_parish(void);

	CTransaction_county_municipal_parish(int id,
																			 int county_id,
																			 int municipal_id,
																			 int parish_id,
																			 LPCSTR county_name,
																			 LPCSTR municipal_name,
																			 LPCSTR parish_name,
																			 LPCSTR created);
	
	// Copy constructor
	CTransaction_county_municipal_parish(const CTransaction_county_municipal_parish &);


	int getID(void);
	int getCountyID(void);
	int getMunicipalID(void);
	int getParishID(void);
	CString getCountyName(void);
	CString getMunicipalName(void);
	CString getParishName(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_county_municipal_parish> vecCMP;


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trees

class CTransaction_trees
{
private:
	int m_nSpcID;
	double m_fDbh;
	double m_fHgt;
	double m_fGCrown;
	double m_fBarkThick;
	double m_fM3Sk_vol;
	double m_fM3Fub_vol;
	CHAR m_nIsASampleTree;
	int m_nAge;
	int m_nGrowth;
	int m_nVFuncIdx;	// Points to the index for the selected voilume function
	int m_nHFuncIdx;	// Points to the index for the selected height function
	int m_nQualID;		// Points to quality desc. table; 060320 p�d
public:
	// Default constructor
	CTransaction_trees(void);
	
	CTransaction_trees(int,double,double,double,double,double,double,CHAR,int,int,int,int,int);
	
	// Copy constructor; 060320 p�d
	CTransaction_trees(const CTransaction_trees &c);

	int getSpcID(void);
	double getDBH(void);
	double getHgt(void);
	double getGCrown(void);
	double getBarkThick(void);
	double getM3sk_vol(void);
	double getM3fub_vol(void);
	CHAR getIsASampleTree(void);
	int getAge(void);
	int getGrowth(void);
	int getVFuncIdx(void);
	int getHFuncIdx(void);
	int getQualID(void);
};

typedef std::vector<CTransaction_trees> vecTransactionTrees;

// Property transaction classes; 061129 p�d
class CTransaction_property
{
	int m_nID;
	CString m_sCountyCode;
	CString m_sMunicipalCode;
	CString m_sParishCode;
	CString m_sCountyName;
	CString m_sMunicipalName;
	CString m_sParishName;
	CString m_sPropertyNum;
	CString m_sPropertyName;
	CString m_sBlock;
	CString m_sUnit;
	double m_fAreal;
	double m_fArealMeasured;
	CString m_sCreatedBy;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_property(void);

	CTransaction_property(int id,
												LPCSTR county_code,
												LPCSTR municipal_code,
												LPCSTR parish_code,
												LPCSTR county_name,
												LPCSTR municipal_name,
												LPCSTR parish_name,
												LPCSTR prop_num,
												LPCSTR prop_name,
												LPCSTR block,
												LPCSTR unit,
												double areal,
												double areal_measured,
												LPCSTR created_by,
												LPCSTR created);
	
	// Copy constructor
	CTransaction_property(const CTransaction_property &);

	int getID(void);
	CString getCountyCode(void);
	CString getMunicipalCode(void);
	CString getParishCode(void);
	CString getCountyName(void);
	CString getMunicipalName(void);
	CString getParishName(void);
	CString getPropertyNum(void);
	CString getPropertyName(void);
	CString getBlock(void);
	CString getUnit(void);
	double getAreal(void);
	double getArealMeasured(void);
	CString getCreatedBy(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_property> vecTransactionProperty;

// Property transaction classes; 070118 p�d
class CTransaction_prop_owners
{
	int m_nPropID;
	int m_nContactID;
	CString m_sShareOff;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_prop_owners(void);

	CTransaction_prop_owners(int prop_id,
		                       int contact_id,
      										 LPCSTR share_off,
												   LPCSTR created);
	
	// Copy constructor
	CTransaction_prop_owners(const CTransaction_prop_owners &);

	int getPropID(void);
	int getContactID(void);
	CString getShareOff(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_prop_owners> vecTransactionPropOwners;



#endif