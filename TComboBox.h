#if !defined(AFX_TCOMBOBOX_H__C2E07D5F_3B83_11D3_BE43_00105AE37FA0__INCLUDED_)
#define AFX_TCOMBOBOX_H__C2E07D5F_3B83_11D3_BE43_00105AE37FA0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TComboBox.h : header file
//

#include <LIMITS.H>

/////////////////////////////////////////////////////////////////////////////
// CTComboBox window

class CTComboBox : public CComboBox
{
// Construction
public:
	CTComboBox();

// Attributes
public:
	// Provide helper functions so that the programmers don't have to
	// remember (or look up) the Windows flags for comboboxes to perform
	// the following tasks.
	bool MakeDropDown();
	bool MakeDropDownList();

// Operations
public:

	// Function to allow/disallow duplicates, ONLY when
	// using specifically any of the AddEntry() functions!
	// Parameters:
	//		bAllowDuplicates	-	true to allow duplicates to be added
	//								false to NOT allow duplicates to be added
	// Return values:
	//	Previous value of m_bAllowDuplicates flag
	bool SetDuplicates(bool bAllowDuplicates = false);

	// Function used to add an entry into the combobox
	bool AddEntry(CString &rcsText, CString &rcsCode);
	bool AddEntry(CString &rcsText, LPCTSTR lpcsCode);
	bool AddEntry(CString &rcsText, int iCode);
	bool AddEntry(CString &rcsText, long lCode);
	bool AddEntry(CString &rcsText, DWORD dwCode);

	// Function used to find an entry based on the code
	// (similar to the CComboBox::FindString() function).
	int FindCode(int iStartAfter, LPCTSTR lpsCodeToFind);
	int FindCode(int iStartAfter, int iCode);
	int FindCode(int iStartAfter, long lCode);
	int FindCode(int iStartAfter, DWORD dwCode);

	// Function used to pre-select an entry based on the code
	// (similar to the CComboBox::SelectString() function).
	int SelectCode(int iStartAfter, LPCTSTR lpsCodeToSearchFor, bool bForceFill = true);
	int SelectCode(int iStartAfter, int iCode, bool bForceFill = true);
	int SelectCode(int iStartAfter, long lCode, bool bForceFill = true);
	int SelectCode(int iStartAfter, DWORD dwCode, bool bForceFill = true);

	// Function used to select the text, optionally filling the combobox
	// whether text is in the list or not
	int SelectText(CString &rcsTextToSelect, bool bForceFill = true);

	// Function used to retrieve the currently selected text
	// This function also performs string trimming on the returned text
	CString GetCurrentText();

	// Function used to get currently select text
	CString &GetSelectedText();

	// Functions used to retrieve the selected code
	bool GetSelectedCode(CString &rcsCode);
	bool GetSelectedCode(int *piCode);
	bool GetSelectedCode(long *plCode);
	bool GetSelectedCode(DWORD *pdwCode);

	// Function used to retrieve the currently selected code
	bool GetCurrentCode(CString &rcsCode);
	bool GetCurrentCode(int *piCode);
	bool GetCurrentCode(long *plCode);
	bool GetCurrentCode(DWORD *pdwCode);

	// ******************************************************************
	// NOTE: The screen that this control is on must have a valid HWND
	//       (i.e. it must be active), in order to call these functions!

	// Function used to get the text of a specific selection
	CString GetText(int iCBSelection);

	// Function used to retrieve the selected code
	bool GetCode(int iCBSelection, CString &rcsCode);
	bool GetCode(int iCBSelection, int *piCode);
	bool GetCode(int iCBSelection, long *plCode);
	bool GetCode(int iCBSelection, DWORD *pdwCode);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTComboBox)
	protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTComboBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CTComboBox)
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	// NOTE:	If this array is too large, an error will occur in CHKSTK.ASM.
	//			This array is allocated only when calling string based AddEntry(),
	//			(i.e. AddEntry(_T("Testing..."), _T("1")) <- the last entry is string based).
	LPTSTR			*lptsArray;
	int				m_iBottom;
	typedef enum {Unknown, String, LPTSTRing, Int, Long, Double} ComboBoxType;
	ComboBoxType	m_eType;
	CString			m_csSelectedText;
	CString			m_csSelectedCode;
	int				m_iSelectedCode;
	long			m_lSelectedCode;
	DWORD			m_dwSelectedCode;
	bool			m_bAllowDuplicates;
	CEdit			*m_pReadOnlyEdit;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TCOMBOBOX_H__C2E07D5F_3B83_11D3_BE43_00105AE37FA0__INCLUDED_)
