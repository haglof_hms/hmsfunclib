#ifndef _RESLANGFILEREADER_H_
#define _RESLANGFILEREADER_H_

#include <map>

typedef std::map<DWORD, CString>  StrMap;
typedef std::pair<DWORD, CString> StrPair;

class RLFReader
{
public:
	RLFReader(void);
	virtual ~RLFReader();

	void clean(void);

	BOOL Load(LPCTSTR);
	BOOL LoadEx(LPCTSTR,StrMap&);

	CString str(DWORD resid);

private:
	StrMap m_strings;
};

#endif
