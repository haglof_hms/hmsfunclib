// netserv.h : main header file for the NETSERV application
//

#if !defined(AFX_NETSERV_H__32A2C2CE_C860_4860_9CF4_86E510A04F3B__INCLUDED_)
#define AFX_NETSERV_H__32A2C2CE_C860_4860_9CF4_86E510A04F3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNetservApp:
// See netserv.cpp for the implementation of this class
//

class CNetservApp : public CWinApp
{
public:
	CNetservApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetservApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CNetservApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETSERV_H__32A2C2CE_C860_4860_9CF4_86E510A04F3B__INCLUDED_)
