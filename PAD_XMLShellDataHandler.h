#ifndef _XMLSHELLDATAHANDLER_H_
#define _XMLSHELLDATAHANDLER_H_

////////////////////////////////////////////////////////////////////////////
// Tags in file ShellData files section UserModules; 051130 p�d
//
const LPCTSTR USER_MODULE_TAG		= _T("UserModules/*");
const LPCTSTR USER_MODULE_ITEM		= _T("um");

class CSTD_Reports
{
	CString m_sFileName;
	int m_nStrID;
	CString m_sCaption;
public:
	CSTD_Reports() 
	{
		m_sFileName	= _T("");
		m_nStrID	= -1;
		m_sCaption	= _T("");
	}

	CSTD_Reports(LPTSTR fn,int str_id,LPCTSTR caption) 
	{
		m_sFileName	= (fn);
		m_nStrID	= str_id;
		m_sCaption	= (caption);
	}

	CSTD_Reports(const CSTD_Reports &c) 
	{
		*this = c;
	}


	CString getFileName(void)		{ return m_sFileName; }
	int getStrID(void)			{ return m_nStrID; }
	CString getCaption(void)		{ return m_sCaption; }
};

typedef std::vector<CSTD_Reports> vecSTDReports;

// class handler for XML; 2005-03-24 p�d
class XMLShellData
{
private:  // Data members
  TCHAR buff[255];
	MSXML2::IXMLDOMDocument2Ptr pXMLDoc;
  CString m_sModulePath;

private:  // Methods
	

public:
  // Defualt constructor
  XMLShellData();
  // Constructor
  XMLShellData(LPCTSTR module_path,HINSTANCE hinst);
  // Destructor
  ~XMLShellData();

  BOOL load(LPCTSTR xml_fn);
	BOOL getUserModules(CStringArray &);

	BOOL getReports(LPCTSTR module,int index,vecSTDReports &,LPCTSTR lang);	// Holds reports for Suite/Usermodule

	CString str(LPCTSTR path);

};

#endif