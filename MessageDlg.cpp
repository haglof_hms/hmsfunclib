// MessageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MessageDlg.h"


// CMessageDlg dialog

IMPLEMENT_DYNAMIC(CMessageDlg, CDialog)

BEGIN_MESSAGE_MAP(CMessageDlg, CDialog)
END_MESSAGE_MAP()

CMessageDlg::CMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageDlg::IDD, pParent)
{
}

CMessageDlg::CMessageDlg(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg,CWnd *pParent)
	: CDialog(CMessageDlg::IDD, pParent)
{
	m_sCaption = cap;
	m_sOKBtn = ok_btn;
	m_sCancelBtn = cancel_btn;
	m_sMsgText = msg;
}

CMessageDlg::~CMessageDlg()
{
}

void CMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_HTML_TEXT1, m_wndHTML);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_sCaption);

	m_wndHTML.SetWindowText(m_sMsgText);
	m_wndHTML.ModifyStyleEx(0, WS_EX_TRANSPARENT);

	m_wndOKBtn.SetWindowText(m_sOKBtn);
	m_wndCancelBtn.SetWindowText(m_sCancelBtn);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


// CMessageDlg message handlers