#include "StdAfx.h"
#include "ResLangFileReader.h"
#include "xmllite.h"
#include <shlwapi.h>

RLFReader::RLFReader(void)
{
}

RLFReader::~RLFReader()
{
	clean();
}

void RLFReader::clean(void)
{
	m_strings.clear();
}

BOOL RLFReader::Load(LPCTSTR xml_fn)
{

	CComPtr<IStream> pFileStream;
	CComPtr<IXmlReader> pReader;
	DWORD id = 0;
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType;

	// Set up xml reader
	if( FAILED(SHCreateStreamOnFile(xml_fn, STGM_READ, &pFileStream)) )
	{
		return FALSE;
	}
	if( FAILED(CreateXmlReader(__uuidof(IXmlReader), (void**)&pReader, NULL)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetInput(pFileStream)) )
	{
		return FALSE;
	}

	m_strings.clear();

	// Read off xml file
	while( pReader->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			// Loop through attributes
			hr = pReader->MoveToFirstAttribute();
			while( hr == S_OK )
			{
				// Get name & value
				if( FAILED(pReader->GetLocalName(&pwszLocalName, NULL)) )
				{
					break;
				}
				if( FAILED(pReader->GetValue(&pwszValue, NULL)) )
				{
					break;
				}

				// Extract id
				if( wcscmp(pwszLocalName, L"id") == 0 )
				{
					id = _wtoi(pwszValue);
				}
				// Extract value
				else if( wcscmp(pwszLocalName, L"value") == 0 )
				{
					// Store value (make sure id is set)
					if( id != 0 )
					{
						m_strings[id] = pwszValue;
					}
				}

				hr = pReader->MoveToNextAttribute();
			}
		}
	}

	return TRUE;
}

BOOL RLFReader::LoadEx(LPCTSTR xml_fn,StrMap& vec)
{

	CComPtr<IStream> pFileStream;
	CComPtr<IXmlReader> pReader;
	DWORD id = 0;
	HRESULT hr;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType;

	m_strings.clear();
	// Set up xml reader
	if( FAILED(SHCreateStreamOnFile(xml_fn, STGM_READ, &pFileStream)) )
	{
		return FALSE;
	}
	if( FAILED(CreateXmlReader(__uuidof(IXmlReader), (void**)&pReader, NULL)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit)) )
	{
		return FALSE;
	}
	if( FAILED(pReader->SetInput(pFileStream)) )
	{
		return FALSE;
	}

	// Read off xml file
	while( pReader->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			// Loop through attributes
			hr = pReader->MoveToFirstAttribute();
			while( hr == S_OK )
			{
				// Get name & value
				if( FAILED(pReader->GetLocalName(&pwszLocalName, NULL)) )
				{
					break;
				}
				if( FAILED(pReader->GetValue(&pwszValue, NULL)) )
				{
					break;
				}

				// Extract id
				if( wcscmp(pwszLocalName, L"id") == 0 )
				{
					id = _wtoi(pwszValue);
				}
				// Extract value
				else if( wcscmp(pwszLocalName, L"value") == 0 )
				{
					// Store value (make sure id is set)
					if( id != 0 )
					{
						vec[id] = pwszValue;
					}
				}

				hr = pReader->MoveToNextAttribute();
			}
		}
	}

	return TRUE;
}

CString RLFReader::str(DWORD resid)
{
	return m_strings[resid];
}
