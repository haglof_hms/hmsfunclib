#include "StdAfx.h"

#include "calculation_methods.h"
#include "pricelistparser.h"	
#include "coststmplparser.h"	


extern int m_globalSpcID;


//////////////////////////////////////////////////////////////////////////////////
// CDBHandleCalc; Handle ALL transactions for Forrest suite specifics; 061116 p�d



CDBHandleCalc::CDBHandleCalc(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CDBHandleCalc::CDBHandleCalc(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CDBHandleCalc::CDBHandleCalc(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PRIVATE
BOOL CDBHandleCalc::traktDataExists(CTransaction_trakt_data &rec)
{

	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdata_id=%d and tdata_trakt_id=%d and tdata_data_type=%d"),
		TBL_TRAKT_DATA,
		rec.getTDataID(),
		rec.getTDataTraktID(),
		rec.getTDataType());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktAssExists(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_assort_id=%d and tass_trakt_id=%d and tass_trakt_data_id=%d and tass_data_type=%d"),
		TBL_TRAKT_SPC_ASS,
		rec.getTAssID(),
		rec.getTAssTraktID(),
		rec.getTAssTraktDataID(),
		rec.getTAssTraktDataType());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktAssExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_trakt_id=%d"),
		TBL_TRAKT_SPC_ASS,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktAssExists(int trakt_id,int trakt_data_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tass_trakt_id=%d and tass_trakt_data_id=%d"),
		TBL_TRAKT_SPC_ASS,
		trakt_id,
		trakt_data_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktTransExists(CTransaction_trakt_trans &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttrans_trakt_data_id=%d and ttrans_trakt_id=%d and ttrans_from_ass_id=%d and ttrans_to_ass_id=%d and ttrans_to_spc_id=%d and ttrans_data_type=%d"),
		TBL_TRAKT_TRANS,
		rec.getTTransDataID(),
		rec.getTTransTraktID(),
		rec.getTTransFromID(),
		rec.getTTransToID(),
		rec.getSpecieID(),
		rec.getTTransDataType());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktTransExists(int trakt_data_id,int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttrans_trakt_data_id=%d and ttrans_trakt_id=%d"),
		TBL_TRAKT_TRANS,
		trakt_data_id,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktSampleTreeExists(CTransaction_sample_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttree_id=%d and ttree_trakt_id=%d"),
		TBL_TRAKT_SAMPLE_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktSampleTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where ttree_trakt_id=%d"),
		TBL_TRAKT_SAMPLE_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktDCLSTreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_id=%d and tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktDCLSTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls_trakt_id=%d"),
		TBL_TRAKT_DCLS_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktDCLS1TreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("(select 1 from %s where tdcls1_id=%d and tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktDCLS1TreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls1_trakt_id=%d"),
		TBL_TRAKT_DCLS1_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktDCLS2TreeExists(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls2_id=%d and tdcls2_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		rec.getTreeID(),
		rec.getTraktID());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktDCLS2TreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tdcls2_trakt_id=%d"),
		TBL_TRAKT_DCLS2_TREES,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktAssTreeExists(CTransaction_tree_assort &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tassort_tree_id=%d and tassort_tree_trakt_id=%d and tassort_tree_dcls=%.0f"),
		TBL_TRAKT_TREES_ASSORT,
		rec.getTreeID(),
		rec.getTraktID(),
		rec.getDCLS());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktAssTreeExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where tassort_tree_trakt_id=%d"),
		TBL_TRAKT_TREES_ASSORT,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktRotpostExists(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where rot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST,
		rec.getRotTraktID());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktRotpostSpcExists(CTransaction_trakt_rotpost_spc &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where spcrot_id=%d and spcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_SPC,
		rec.getRotSpcID_pk(),
		rec.getRotSpcTraktID_pk());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktRotpostSpcExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where spcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_SPC,
		trakt_id);
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktRotpostOtcExists(CTransaction_trakt_rotpost_other &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where otcrot_id=%d and otcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_OTC,
		rec.getOtcRotID_pk(),
		rec.getOtcRotTraktID_pk());
	return exists(sSQL);
}

BOOL CDBHandleCalc::traktRotpostOtcExists(int trakt_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where otcrot_trakt_id=%d"),
		TBL_TRAKT_ROTPOST_OTC,
		trakt_id);
	return exists(sSQL);
}

// Update esti_trakt_table,trakt_areal_factor; 091102 p�d
BOOL CDBHandleCalc::setTrakt_Areal_Factor(int trakt_id,double areal_factor)
{
	CString sSQL;

	try
	{
		sSQL.Format(_T("update %s set trakt_areal_factor=:1,created=GETDATE()	where trakt_id=:2"),TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsDouble()	= areal_factor;
		m_saCommand.Param(2).setAsLong()	= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////
// Trakt data
BOOL CDBHandleCalc::getTraktData(vecTransactionTraktData &vec,int trakt_id,int data_type)
{
	CString sSQL;
	try
	{
		vec.clear();
		// If trakt_id	> -1, use trakt_id to select
		// a specific trakt data; 070309 p�d
		if (trakt_id == -1)
			sSQL.Format(_T("select * from %s order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA);
		else if (trakt_id > -1 && data_type == -1)
			sSQL.Format(_T("select * from %s where tdata_trakt_id=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,trakt_id);
		else if (trakt_id > -1 && data_type > -1)
			sSQL.Format(_T("select * from %s where tdata_trakt_id=%d and tdata_data_type=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,trakt_id,data_type);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_trakt_data(m_saCommand.Field(_T("tdata_id")).asLong(),
													m_saCommand.Field(_T("tdata_trakt_id")).asLong(),
													m_saCommand.Field(_T("tdata_data_type")).asShort(),
													m_saCommand.Field(_T("tdata_spc_id")).asLong(),
													m_saCommand.Field(_T("tdata_spc_name")).asString(),
													m_saCommand.Field(_T("tdata_percent")).asDouble(),
													m_saCommand.Field(_T("tdata_numof")).asLong(),
													m_saCommand.Field(_T("tdata_da")).asDouble(),
													m_saCommand.Field(_T("tdata_dg")).asDouble(),
													m_saCommand.Field(_T("tdata_dgv")).asDouble(),
													m_saCommand.Field(_T("tdata_hgv")).asDouble(),
													m_saCommand.Field(_T("tdata_gy")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_hgt")).asDouble(),
													m_saCommand.Field(_T("tdata_h25")).asDouble(),
													m_saCommand.Field(_T("tdata_gcrown")).asDouble(),
													m_saCommand.Field(_T("tdata_m3sk_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_m3fub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_m3ub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_m3sk_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_m3fub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_m3ub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_grot")).asDouble(),
													m_saCommand.Field(_T("created")).asString()));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getTraktData(CTransaction_trakt_data &rec,int trakt_id,int spc_id,int data_type)
{
	CString sSQL;
	try
	{
		// If trakt_id	> -1, use trakt_id to select
		// a specific trakt data; 070309 p�d
		if (trakt_id == -1 || spc_id == -1 || data_type == -1)
			return FALSE;

		sSQL.Format(_T("select * from %s where tdata_trakt_id=%d and tdata_spc_id=%d and tdata_data_type=%d order by tdata_id,tdata_trakt_id"),
								TBL_TRAKT_DATA,
								trakt_id,
								spc_id,
								data_type);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = (CTransaction_trakt_data(m_saCommand.Field(_T("tdata_id")).asLong(),
													m_saCommand.Field(_T("tdata_trakt_id")).asLong(),
													m_saCommand.Field(_T("tdata_data_type")).asShort(),
													m_saCommand.Field(_T("tdata_spc_id")).asLong(),
													m_saCommand.Field(_T("tdata_spc_name")).asString(),
													m_saCommand.Field(_T("tdata_percent")).asDouble(),
													m_saCommand.Field(_T("tdata_numof")).asLong(),
													m_saCommand.Field(_T("tdata_da")).asDouble(),
													m_saCommand.Field(_T("tdata_dg")).asDouble(),
													m_saCommand.Field(_T("tdata_dgv")).asDouble(),
													m_saCommand.Field(_T("tdata_hgv")).asDouble(),
													m_saCommand.Field(_T("tdata_gy")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_hgt")).asDouble(),
													m_saCommand.Field(_T("tdata_h25")).asDouble(),
													m_saCommand.Field(_T("tdata_gcrown")).asDouble(),
													m_saCommand.Field(_T("tdata_m3sk_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_m3fub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_m3ub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_m3sk_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_m3fub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_avg_m3ub_vol")).asDouble(),
													m_saCommand.Field(_T("tdata_grot")).asDouble(),
													m_saCommand.Field(_T("created")).asString()));

		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::addTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	try
	{
		if (!traktDataExists(rec))
		{
			sSQL.Format(_T("insert into %s (tdata_id,tdata_trakt_id,tdata_data_type,tdata_spc_id,tdata_spc_name,tdata_percent,tdata_numoftdata_da,tdata_dg,tdata_dgv,tdata_hgv,tdata_gy,tdata_avg_hgt,")
									_T("tdata_h25,tdata_m3sk_vol,tdata_m3fub_vol,tdata_m3ub_vol,tdata_avg_m3sk_vol,tdata_avg_m3fub_vol,tdata_avg_m3ub_vol,tdata_gcrown)  ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21)"),TBL_TRAKT_DATA);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
			m_saCommand.Param(1).setAsLong()		= rec.getTDataID();
			m_saCommand.Param(2).setAsLong()		= rec.getTDataTraktID();
			m_saCommand.Param(3).setAsShort()		= rec.getTDataType();
			m_saCommand.Param(4).setAsLong()		= rec.getSpecieID();
			m_saCommand.Param(5).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(6).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(7).setAsLong()		= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()	= rec.getDA();
			m_saCommand.Param(9).setAsDouble()	= rec.getDG();
			m_saCommand.Param(10).setAsDouble()	= rec.getDGV();
			m_saCommand.Param(11).setAsDouble()	= rec.getHGV();
			m_saCommand.Param(12).setAsDouble()	= rec.getGY();
			m_saCommand.Param(13).setAsDouble()	= rec.getAvgHgt();
			m_saCommand.Param(14).setAsDouble()	= rec.getH25();
			m_saCommand.Param(15).setAsDouble()	= rec.getM3SK();
			m_saCommand.Param(16).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(17).setAsDouble()	= rec.getM3UB();
			m_saCommand.Param(18).setAsDouble()	= rec.getAvgM3SK();
			m_saCommand.Param(19).setAsDouble()	= rec.getAvgM3FUB();
			m_saCommand.Param(20).setAsDouble()	= rec.getAvgM3UB();
			m_saCommand.Param(21).setAsDouble()	= rec.getGreenCrown();

			m_saCommand.Execute();
		}	// if (!traktDataExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updTraktData(CTransaction_trakt_data &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	CTransaction_trakt_data rec1;
	try
	{
		sSQL.Format(_T("update %s set tdata_spc_name=:1,tdata_percent=:2,tdata_numof=:3,tdata_da=:4,tdata_dg=:5,tdata_dgv=:6,tdata_hgv=:7,tdata_gy=:8,tdata_avg_hgt=:9,")
			_T("tdata_h25=:10,tdata_m3sk_vol=:11,tdata_m3fub_vol=:12,tdata_m3ub_vol=:13,tdata_avg_m3sk_vol=:14,tdata_avg_m3fub_vol=:15,tdata_avg_m3ub_vol=:16,tdata_gcrown=:17,tdata_grot=:18,created=GETDATE() ")
								_T("where tdata_id=:19 and tdata_trakt_id=:20 and tdata_data_type=:21 and tdata_spc_id=:22"), TBL_TRAKT_DATA);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

		m_saCommand.Param(1).setAsString()	= rec.getSpecieName();
		m_saCommand.Param(2).setAsDouble()	= rec.getPercent();
		m_saCommand.Param(3).setAsLong()		= rec.getNumOf();
		m_saCommand.Param(4).setAsDouble()	= rec.getDA();
		m_saCommand.Param(5).setAsDouble()	= rec.getDG();
		m_saCommand.Param(6).setAsDouble()	= rec.getDGV();
		m_saCommand.Param(7).setAsDouble()	= rec.getHGV();
		m_saCommand.Param(8).setAsDouble()	= rec.getGY();
		m_saCommand.Param(9).setAsDouble()	= rec.getAvgHgt();
		m_saCommand.Param(10).setAsDouble()	= rec.getH25();
		m_saCommand.Param(11).setAsDouble()	= rec.getM3SK();
		m_saCommand.Param(12).setAsDouble()	= rec.getM3FUB();
		m_saCommand.Param(13).setAsDouble()	= rec.getM3UB();
		m_saCommand.Param(14).setAsDouble()	= rec.getAvgM3SK();
		m_saCommand.Param(15).setAsDouble()	= rec.getAvgM3FUB();
		m_saCommand.Param(16).setAsDouble()	= rec.getAvgM3UB();
		m_saCommand.Param(17).setAsDouble()	= rec.getGreenCrown();
		m_saCommand.Param(18).setAsDouble()	= rec.getGrot();

		m_saCommand.Param(19).setAsLong()		= rec.getTDataID();
		m_saCommand.Param(20).setAsLong()		= rec.getTDataTraktID();
		m_saCommand.Param(21).setAsShort()	= rec.getTDataType();
		m_saCommand.Param(22).setAsLong()		= rec.getSpecieID();

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox((LPCWSTR)e.ErrText());
		return FALSE;
	}

	return TRUE;
}

double conv(double v)
{
	CString S;
	S.Format(L"%f",v);
	S.Replace(L",",L".");
	UMMessageBox(S);
	return _tstof(S);
}

BOOL CDBHandleCalc::updTraktData(vecTransactionTraktData &vec)
{
	if (vec.empty()) return FALSE;

	try
	{
		// Put together update statement
		for (UINT i = 0;i < vec.size();i++)
		{
			m_saCommand.setCommandText(_T("UPDATE esti_trakt_data_table ")
										_T("SET tdata_spc_name = :1,")
										_T("tdata_percent = :2,")
										_T("tdata_numof = :3,")
										_T("tdata_da = :4,")
										_T("tdata_dg = :5,")
										_T("tdata_dgv = :6,")
										_T("tdata_hgv = :7,")
										_T("tdata_gy = :8,")
										_T("tdata_avg_hgt = :9,")
										_T("tdata_h25 = :10,")
										_T("tdata_m3sk_vol = :11,")
										_T("tdata_m3fub_vol = :12,")
										_T("tdata_m3ub_vol = :13,")
										_T("tdata_avg_m3sk_vol = :14,")
										_T("tdata_avg_m3fub_vol = :15,")
										_T("tdata_avg_m3ub_vol = :16,")
										_T("tdata_gcrown = :17,")
										_T("tdata_grot = :18 ")
										_T("WHERE tdata_id = :19 AND tdata_trakt_id = :20 AND tdata_data_type = :21 AND tdata_spc_id = :22"));
			m_saCommand.Param(1).setAsString() = vec[i].getSpecieName();
			m_saCommand.Param(2).setAsDouble() = vec[i].getPercent();
			m_saCommand.Param(3).setAsLong() = vec[i].getNumOf();
			m_saCommand.Param(4).setAsDouble() = vec[i].getDA();
			m_saCommand.Param(5).setAsDouble() = vec[i].getDG();
			m_saCommand.Param(6).setAsDouble() = vec[i].getDGV();
			m_saCommand.Param(7).setAsDouble() = vec[i].getHGV();
			m_saCommand.Param(8).setAsDouble() = vec[i].getGY();
			m_saCommand.Param(9).setAsDouble() = vec[i].getAvgHgt();
			m_saCommand.Param(10).setAsDouble() = vec[i].getH25();
			m_saCommand.Param(11).setAsDouble() = vec[i].getM3SK();
			m_saCommand.Param(12).setAsDouble() = vec[i].getM3FUB();
			m_saCommand.Param(13).setAsDouble() = vec[i].getM3UB();
			m_saCommand.Param(14).setAsDouble() = vec[i].getAvgM3SK();
			m_saCommand.Param(15).setAsDouble() = vec[i].getAvgM3FUB();
			m_saCommand.Param(16).setAsDouble() = vec[i].getAvgM3UB();
			m_saCommand.Param(17).setAsDouble() = vec[i].getGreenCrown();
			m_saCommand.Param(18).setAsDouble() = vec[i].getGrot();
			m_saCommand.Param(19).setAsLong() = vec[i].getTDataID();
			m_saCommand.Param(20).setAsLong() = vec[i].getTDataTraktID();
			m_saCommand.Param(21).setAsLong() = vec[i].getTDataType();
			m_saCommand.Param(22).setAsLong() = vec[i].getSpecieID();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


/////////////////////////////////////////////////////////
// Trakt assort

BOOL CDBHandleCalc::getTraktAss(vecTransactionTraktAss &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where tass_trakt_id=:1 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asShort(),
																					 m_saCommand.Field(5).asString(),
																					 m_saCommand.Field(6).asDouble(),
																					 m_saCommand.Field(7).asDouble(),
																					 m_saCommand.Field(8).asDouble(),
																					 m_saCommand.Field(9).asDouble(),
																					 m_saCommand.Field(10).asDouble(),
																					 m_saCommand.Field(11).asDouble(),
																					 m_saCommand.Field(12).asString(),
																					 m_saCommand.Field(13).asDouble()));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getTraktAss(CTransaction_trakt_ass &rec,CTransaction_trakt_ass rec1)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tass_assort_id=:1 and tass_trakt_id=:2 and tass_trakt_data_id=:3 and tass_data_type=:4 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec1.getTAssID();
		m_saCommand.Param(2).setAsLong()		= rec1.getTAssTraktID();
		m_saCommand.Param(3).setAsLong()		= rec1.getTAssTraktDataID();
		m_saCommand.Param(4).setAsShort()		= rec1.getTAssTraktDataType();

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			rec = CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
										 m_saCommand.Field(2).asLong(),
										 m_saCommand.Field(3).asLong(),
										 m_saCommand.Field(4).asShort(),
										m_saCommand.Field(5).asString(),
										 m_saCommand.Field(6).asDouble(),
										 m_saCommand.Field(7).asDouble(),
										 m_saCommand.Field(8).asDouble(),
										 m_saCommand.Field(9).asDouble(),
										 m_saCommand.Field(10).asDouble(),
										 m_saCommand.Field(11).asDouble(),
										 m_saCommand.Field(12).asString(),
									 	 m_saCommand.Field(13).asDouble());

		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getTraktAss(CTransaction_trakt_ass &rec,int trakt_id,int trakt_data_id,int data_type,int assort_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tass_assort_id=:1 and tass_trakt_id=:2 and tass_trakt_data_id=:3 and tass_data_type=:4 order by tass_assort_id,tass_trakt_id,tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= assort_id;
		m_saCommand.Param(2).setAsLong()		= trakt_id;
		m_saCommand.Param(3).setAsLong()		= trakt_data_id;
		m_saCommand.Param(4).setAsShort()		= data_type;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
	
			rec = CTransaction_trakt_ass(m_saCommand.Field(1).asLong(),
																	 m_saCommand.Field(2).asLong(),
																	 m_saCommand.Field(3).asLong(),
																	 m_saCommand.Field(4).asShort(),
																	m_saCommand.Field(5).asString(),
																	 m_saCommand.Field(6).asDouble(),
																	 m_saCommand.Field(7).asDouble(),
																	 m_saCommand.Field(8).asDouble(),
																	 m_saCommand.Field(9).asDouble(),
																	 m_saCommand.Field(10).asDouble(),
																	 m_saCommand.Field(11).asDouble(),
																	 m_saCommand.Field(12).asString(),
						 										 	 m_saCommand.Field(13).asDouble());

		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL;
	try
	{
		if (!traktAssExists(rec))
		{
			sSQL.Format(_T("insert into %s (tass_assort_id,tass_trakt_id,tass_trakt_data_id,tass_data_type,tass_assort_name,tass_price_m3fub,tass_price_m3to,")
									 _T("tass_m3fub,tass_m3to,tass_m3fub_value,tass_m3to_value,tass_kr_m3_value) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)"),TBL_TRAKT_SPC_ASS);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
			m_saCommand.Param(1).setAsLong()		= rec.getTAssID();
			m_saCommand.Param(2).setAsLong()		= rec.getTAssTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTAssTraktDataID();
			m_saCommand.Param(4).setAsShort()		= rec.getTAssTraktDataType();
			m_saCommand.Param(5).setAsString()	= rec.getTAssName();
			m_saCommand.Param(6).setAsDouble()	= rec.getPriceM3FUB();
			m_saCommand.Param(7).setAsDouble()	= rec.getPriceM3TO();
			m_saCommand.Param(8).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(9).setAsDouble()	= rec.getM3TO();
			m_saCommand.Param(10).setAsDouble()	= rec.getValueM3FUB();
			m_saCommand.Param(11).setAsDouble()	= rec.getValueM3TO();
			m_saCommand.Param(12).setAsDouble()	= rec.getKrPerM3();

			m_saCommand.Execute();
		}	// if (!traktAssExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updTraktAss(vecTransactionTraktAss &vec)
{
	try
	{
		// Put together update statements
		vecTransactionTraktAss::iterator iter = vec.begin();
		while( iter != vec.end() )
		{
			m_saCommand.setCommandText(_T("UPDATE esti_trakt_spc_assort_table ")
										_T("SET tass_price_m3fub = :1, tass_price_m3to = :2, tass_m3fub = :3, tass_m3to = :4, tass_m3fub_value = :5, tass_m3to_value = :6, tass_kr_m3_value = :7 ")
										_T("WHERE tass_assort_id = :8 AND tass_trakt_id = :9 AND tass_trakt_data_id = :10 AND tass_data_type = :11"));
			m_saCommand.Param(1).setAsDouble() = iter->getPriceM3FUB();
			m_saCommand.Param(2).setAsDouble() = iter->getPriceM3TO();
			m_saCommand.Param(3).setAsDouble() = iter->getM3FUB();
			m_saCommand.Param(4).setAsDouble() = iter->getM3TO();
			m_saCommand.Param(5).setAsDouble() = iter->getValueM3FUB();
			m_saCommand.Param(6).setAsDouble() = iter->getValueM3TO();
			m_saCommand.Param(7).setAsDouble() = iter->getKrPerM3();
			m_saCommand.Param(8).setAsLong() = iter->getTAssID();
			m_saCommand.Param(9).setAsLong() = iter->getTAssTraktID();
			m_saCommand.Param(10).setAsLong() = iter->getTAssTraktDataID();
			m_saCommand.Param(11).setAsLong() = iter->getTAssTraktDataType();
			m_saCommand.Execute();
			iter++;
		}
	}
	catch( SAException &e )
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updTraktAss(CTransaction_trakt_ass &rec)
{
	CString sSQL,S;
	try
	{
		sSQL.Format(_T("update %s set tass_assort_name=:1,tass_price_m3fub=:2,tass_price_m3to=:3,tass_m3fub=:4,tass_m3to=:5,tass_m3fub_value=:6,tass_m3to_value=:7,tass_kr_m3_value=:8,created=GETDATE() ")
								_T("where tass_assort_id=:9 and tass_trakt_id=:10 and tass_trakt_data_id=:11 and tass_data_type=:12"),TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

		m_saCommand.Param(1).setAsString()	= rec.getTAssName();
		m_saCommand.Param(2).setAsDouble()	= rec.getPriceM3FUB();
		m_saCommand.Param(3).setAsDouble()	= rec.getPriceM3TO();
		m_saCommand.Param(4).setAsDouble()	= rec.getM3FUB();
		m_saCommand.Param(5).setAsDouble()	= rec.getM3TO();
		m_saCommand.Param(6).setAsDouble()	= rec.getValueM3FUB();
		m_saCommand.Param(7).setAsDouble()	= rec.getValueM3TO();
		m_saCommand.Param(8).setAsDouble()	= rec.getKrPerM3();
		m_saCommand.Param(9).setAsLong()	= rec.getTAssID();
		m_saCommand.Param(10).setAsLong()	= rec.getTAssTraktID();
		m_saCommand.Param(11).setAsLong()	= rec.getTAssTraktDataID();
		m_saCommand.Param(12).setAsShort()	= rec.getTAssTraktDataType();

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updTraktAss_prices(int trakt_id,int trakt_data_id,int data_type,int assort_id,double exch_price,int price_in)
{
	CString sSQL;
	try
	{
		if (price_in == 0)	// M3TO
		{
			sSQL.Format(_T("update %s set tass_price_m3to=:1,tass_price_m3fub=:2,created=GETDATE() where tass_trakt_id=:3 and tass_trakt_data_id=:4 and tass_assort_id=:5 and tass_data_type=:6"),
									TBL_TRAKT_SPC_ASS);
		}
		else if (price_in == 3)	// M3FUB
		{
			sSQL.Format(_T("update %s set tass_price_m3fub=:1,tass_price_m3to=:2,created=GETDATE() where tass_trakt_id=:3 and tass_trakt_data_id=:4 and tass_assort_id=:5 and tass_data_type=:6"),
									TBL_TRAKT_SPC_ASS);
		}
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsDouble()	= exch_price;
		m_saCommand.Param(2).setAsDouble()	= 0.0;
		m_saCommand.Param(3).setAsLong()	= trakt_id;
		m_saCommand.Param(4).setAsLong()	= trakt_data_id;
		m_saCommand.Param(5).setAsLong()	= assort_id;
		m_saCommand.Param(6).setAsShort()	= data_type;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


//Gjort en ny funktion som anv�nder sig av parametrar f�r att s�tta r�tt flyttal i tabellen, 
//f�rut s� var formateringen p� ExchPrise %.f vilket gjorde att den trunkerades och inte sparade decimalerna
//Bug #2552 20111118 J�
BOOL CDBHandleCalc::updTraktAss_prices(vecExchCalcInfo &vec)
{
	CString sSQL=_T("");

	if (vec.empty()) return FALSE;
	try
	{
		sSQL.Empty();
		for (UINT i = 0;i < vec.size();i++)
		{

			if (vec[i].nPriceIn == 0)	// M3TO
				sSQL.Format(_T("update %s set tass_price_m3to=:1,tass_price_m3fub=:2,created=GETDATE() where tass_trakt_id=:3 and tass_trakt_data_id=:4 and tass_assort_id=:5 and tass_data_type=:6"),TBL_TRAKT_SPC_ASS);
			else
				sSQL.Format(_T("update %s set tass_price_m3fub=:1,tass_price_m3to=:2,created=GETDATE() where tass_trakt_id=:3 and tass_trakt_data_id=:4 and tass_assort_id=:5 and tass_data_type=:6"),TBL_TRAKT_SPC_ASS);
			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

			m_saCommand.Param(1).setAsDouble()	= vec[i].fExchPrice;
			m_saCommand.Param(2).setAsDouble()	= 0.0;
			m_saCommand.Param(3).setAsLong()	= vec[i].nTraktID;
			m_saCommand.Param(4).setAsLong()	= vec[i].nTraktDataID;
			m_saCommand.Param(5).setAsLong()	= vec[i].nAssortID;
			m_saCommand.Param(6).setAsShort()	= STMP_LEN_WITHDRAW;

			m_saCommand.Execute();

		}	// for (UINT i = 0;i < vec.size();i++)	
	}
	catch(SAException &e)
	{
		// print error message
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}
/*
BOOL CDBHandleCalc::updTraktAss_prices(vecExchCalcInfo &vec)
{
	CString sSQL,sTmp;

	if (vec.empty()) return FALSE;

	try
	{
		sSQL.Empty();
		for (UINT i = 0;i < vec.size();i++)
		{
			if (vec[i].nPriceIn == 0)	// M3TO
			{
				sTmp.Format(_T("update %s set tass_price_m3to=%.f,tass_price_m3fub=%.f,created=GETDATE() where tass_trakt_id=%d and tass_trakt_data_id=%d and tass_assort_id=%d and tass_data_type=%d;"),
										TBL_TRAKT_SPC_ASS,
										vec[i].fExchPrice,
										0.0,
										vec[i].nTraktID,
										vec[i].nTraktDataID,
										vec[i].nAssortID,
										STMP_LEN_WITHDRAW);
			}
			else if (vec[i].nPriceIn == 3)	// M3FUB
			{
				sTmp.Format(_T("update %s set tass_price_m3fub=%.f,tass_price_m3to=%.f,created=GETDATE() where tass_trakt_id=%d and tass_trakt_data_id=%d and tass_assort_id=%d and tass_data_type=%d;"),
										TBL_TRAKT_SPC_ASS,
										vec[i].fExchPrice,
										0.0,
										vec[i].nTraktID,
										vec[i].nTraktDataID,
										vec[i].nAssortID,
										STMP_LEN_WITHDRAW);

			}
			sSQL+= sTmp;
		}	// for (UINT i = 0;i < vec.size();i++)	


		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		 // print error message
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}*/

BOOL CDBHandleCalc::resetTraktAss(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set tass_m3fub=:1,tass_m3to=:2,tass_m3fub_value=:3,tass_m3to_value=:4,created=GETDATE()	where tass_trakt_id=:5"),
								TBL_TRAKT_SPC_ASS);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

		m_saCommand.Param(1).setAsDouble()	= 0.0;
		m_saCommand.Param(2).setAsDouble()	= 0.0;
		m_saCommand.Param(3).setAsDouble()	= 0.0;
		m_saCommand.Param(4).setAsDouble()	= 0.0;
		m_saCommand.Param(5).setAsLong()	= trakt_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// "R�kna upp/ned v�rdet per sortiment och tr�dslag"; 080611 p�d
BOOL CDBHandleCalc::recalcTraktAss_on_k3_per_m3(int trakt_id,int price_set_in)
{
	CString sSQL,S;
	BOOL bReturn = FALSE;
	try
	{
/*
		// Price st in m3fub; 090615 p�d
		if (price_set_in == 1)
		{
			sSQL.Format(_T("update %s set tass_m3fub_value=tass_m3fub_value+(tass_m3fub*tass_kr_m3_value),tass_m3to_value=0.0,created=GETDATE() \
											where tass_trakt_id=:1"),TBL_TRAKT_SPC_ASS);
		}
		// Price st in m3to; 090615 p�d
		else if (price_set_in == 2)
		{
			sSQL.Format(_T("update %s set tass_m3fub_value=0.0,tass_m3to_value=tass_m3to_value+(tass_m3to*tass_kr_m3_value),created=GETDATE() \
											where tass_trakt_id=:1"),TBL_TRAKT_SPC_ASS);
		}
*/
		// Update m3fub; 090915 p�d
		sSQL.Format(_T("update %s set tass_m3fub_value=tass_m3fub_value+(tass_m3fub*tass_kr_m3_value),created=GETDATE() where tass_trakt_id=:1 and tass_m3fub_value>0.0"),
										TBL_TRAKT_SPC_ASS);

		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()	= trakt_id;
		m_saCommand.Execute();

		// Update m3to; 090915 p�d
		sSQL.Format(_T("update %s set tass_m3to_value=tass_m3to_value+(tass_m3to*tass_kr_m3_value),created=GETDATE() where tass_trakt_id=:1 and tass_m3to_value>0.0"),
										TBL_TRAKT_SPC_ASS);

		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()	= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////
// Trakt transfers for assortments
BOOL CDBHandleCalc::getTraktTrans(vecTransactionTraktTrans &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		if (trakt_id == -1)
			sSQL.Format(_T("select * from %s order by ttrans_to_ass_id"),
									TBL_TRAKT_TRANS);
		else
			sSQL.Format(_T("select * from %s where ttrans_trakt_id=:1 order by ttrans_to_ass_id"),
									TBL_TRAKT_TRANS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_trakt_trans(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asLong(),
													m_saCommand.Field(6).asShort(),
													m_saCommand.Field(7).asString(),
													m_saCommand.Field(8).asString(),
													m_saCommand.Field(9).asString(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(12).asString()));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getTraktTrans(CTransaction_trakt_trans &rec1,CTransaction_trakt_trans rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where ttrans_trakt_data_id=:1 and ttrans_trakt_id=:2 and ttrans_from_ass_id=:3 and ttrans_to_ass_id=:4 and ttrans_to_spc_id=:5 and ttrans_data_type=:6 ")
								_T("order by ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id"),TBL_TRAKT_TRANS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTTransDataID();
		m_saCommand.Param(2).setAsLong()		= rec.getTTransTraktID();
		m_saCommand.Param(3).setAsLong()		= rec.getTTransFromID();
		m_saCommand.Param(4).setAsLong()		= rec.getTTransToID();
		m_saCommand.Param(5).setAsLong()		= rec.getSpecieID();
		m_saCommand.Param(6).setAsShort()		= rec.getTTransDataType();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			rec1 = CTransaction_trakt_trans(m_saCommand.Field(1).asLong(),
											m_saCommand.Field(2).asLong(),
											m_saCommand.Field(3).asLong(),
											m_saCommand.Field(4).asLong(),
											m_saCommand.Field(5).asLong(),
											m_saCommand.Field(6).asShort(),
											m_saCommand.Field(7).asString(),
											m_saCommand.Field(8).asString(),
											m_saCommand.Field(9).asString(),
											m_saCommand.Field(10).asDouble(),
											m_saCommand.Field(11).asDouble(),
											m_saCommand.Field(13).asDouble(),
											m_saCommand.Field(12).asString());

		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::addTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL;
	try
	{
		if (!traktTransExists(rec))
		{
			sSQL.Format(_T("insert into %s (ttrans_trakt_data_id,ttrans_trakt_id,ttrans_from_ass_id,ttrans_to_ass_id,ttrans_to_spc_id,ttrans_data_type,ttrans_from_name,ttrans_to_name,ttrans_spc_name,ttrans_m3fub,ttrans_percent,ttrans_trans_m3) ")
				_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)"),TBL_TRAKT_TRANS);
			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
			m_saCommand.Param(1).setAsLong()		= rec.getTTransDataID();
			m_saCommand.Param(2).setAsLong()		= rec.getTTransTraktID();
			m_saCommand.Param(3).setAsLong()		= rec.getTTransFromID();
			m_saCommand.Param(4).setAsLong()		= rec.getTTransToID();
			m_saCommand.Param(5).setAsShort()		= rec.getSpecieID();
			m_saCommand.Param(6).setAsLong()		= rec.getTTransDataType();
			m_saCommand.Param(7).setAsString()	= rec.getFromName();
			m_saCommand.Param(8).setAsString()	= rec.getToName();
			m_saCommand.Param(9).setAsString()	= rec.getSpecieName();
			m_saCommand.Param(10).setAsDouble()	= rec.getM3FUB();
			m_saCommand.Param(11).setAsDouble()	= rec.getPercent();
			m_saCommand.Param(12).setAsDouble()	= rec.getM3Trans();

			m_saCommand.Execute();
		}	// if (!traktTransExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::updTraktTrans(CTransaction_trakt_trans &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set ttrans_from_name=:1,ttrans_to_name=:2,ttrans_spc_name=:3,ttrans_m3fub=:4,ttrans_percent=:5,ttrans_trans_m3=:6,created=GETDATE() ")
								_T("where ttrans_trakt_data_id=:7 and ttrans_trakt_id=:8 and ttrans_from_ass_id=:9 and ttrans_to_ass_id=:10 and ttrans_to_spc_id=:11 and ttrans_data_type=:12"),
								TBL_TRAKT_TRANS);

		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsString()	= rec.getFromName();
		m_saCommand.Param(2).setAsString()	= rec.getToName();
		m_saCommand.Param(3).setAsString()	= rec.getSpecieName();
		m_saCommand.Param(4).setAsDouble()	= rec.getM3FUB();
		m_saCommand.Param(5).setAsDouble()	= rec.getPercent();
		m_saCommand.Param(6).setAsDouble()	= rec.getM3Trans();
		m_saCommand.Param(7).setAsLong()	= rec.getTTransDataID();
		m_saCommand.Param(8).setAsLong()	= rec.getTTransTraktID();
		m_saCommand.Param(9).setAsLong()	= rec.getTTransFromID();
		m_saCommand.Param(10).setAsLong()	= rec.getTTransToID();
		m_saCommand.Param(11).setAsLong()	= rec.getSpecieID();
		m_saCommand.Param(12).setAsShort()	= rec.getTTransDataType();

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::removeTraktTrans(int trakt_data_id,int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where ttrans_trakt_data_id=:1 and ttrans_trakt_id=:2"),
								TBL_TRAKT_TRANS);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()		= trakt_data_id;
		m_saCommand.Param(2).setAsLong()		= trakt_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////
// Trakt set specie(s), functions (volume,height,bark etc)
BOOL CDBHandleCalc::getTraktSetSpc(vecTransactionTraktSetSpc &vec,int trakt_id)
{
	CString sSQL;
	try
	{
		vec.clear();
			
		// Check value of trakt_is. If trakt_id = -1, select ALL data otherwise, select
		// only data for trakt_id; 070504 p�d
		if (trakt_id == -1)
		{
			sSQL.Format(_T("select * from %s order by tsetspc_tdata_trakt_id,tsetspc_tdata_id,tsetspc_id"),
										TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
		}
		else
		{
			sSQL.Format(_T("select * from %s where tsetspc_tdata_trakt_id=:1 order by tsetspc_tdata_trakt_id,tsetspc_tdata_id,tsetspc_id"),
										TBL_TRAKT_SET_SPC);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = trakt_id;
		}
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_trakt_set_spc(m_saCommand.Field(1).asLong(),
													 m_saCommand.Field(2).asLong(),
													 m_saCommand.Field(3).asLong(),
													 m_saCommand.Field(4).asShort(),
													 m_saCommand.Field(5).asLong(),
													 m_saCommand.Field(6).asLong(),
													 m_saCommand.Field(7).asLong(),
													 m_saCommand.Field(8).asLong(),
													 m_saCommand.Field(9).asLong(),
													 m_saCommand.Field(10).asLong(),
													 m_saCommand.Field(11).asLong(),
													 m_saCommand.Field(12).asLong(),
													 m_saCommand.Field(13).asLong(),
													 m_saCommand.Field(14).asLong(),
													 m_saCommand.Field(15).asLong(),
													 m_saCommand.Field(16).asLong(),
													 m_saCommand.Field(17).asLong(),
													 m_saCommand.Field(18).asDouble(),
													 m_saCommand.Field(19).asDouble(),
													 m_saCommand.Field(20).asDouble(),	// m3fub -> m3to
													 m_saCommand.Field(21).asDouble(),
													 m_saCommand.Field(_T("tsetspc_qdesc_index")).asLong(),
													 m_saCommand.Field(_T("tsetspc_extra_info")).asString(),
													 m_saCommand.Field(_T("tsetspc_transp_dist")).asLong(),
													 m_saCommand.Field(_T("tsetspc_transp_dist2")).asLong(),
#ifdef VER120
													 convertSADateTime(saDateTime),
													 m_saCommand.Field(_T("tsetspc_qdesc_name")).asString(),
 													 m_saCommand.Field(_T("tsetspc_grot_func_id")).asLong(),
 													 m_saCommand.Field(_T("tsetspc_grot_percent")).asDouble(),
 													 m_saCommand.Field(_T("tsetspc_grot_price")).asDouble(),
													 m_saCommand.Field(_T("tsetspc_grot_cost")).asDouble(),
													 m_saCommand.Field(_T("tsetspc_default_h25")).asDouble(),
													 m_saCommand.Field(_T("tsetspc_default_greencrown")).asDouble()
													 ));
#else
													 convertSADateTime(saDateTime)));
#endif
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Get plots for trakt; 080505 p�d
BOOL CDBHandleCalc::getPlots(int trakt_id,vecTransactionPlot &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s where tplot_trakt_id=:1"),TBL_TRAKT_PLOT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_plot(m_saCommand.Field(1).asLong(),
											m_saCommand.Field(2).asLong(),
											m_saCommand.Field(3).asString(),
											m_saCommand.Field(4).asLong(),
											m_saCommand.Field(5).asDouble(),
											m_saCommand.Field(6).asDouble(),
											m_saCommand.Field(7).asDouble(),
											m_saCommand.Field(8).asDouble(),
											m_saCommand.Field(9).asString(),
											m_saCommand.Field(10).asLong(),
											convertSADateTime(saDateTime)));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////
// Handle pricelist set for trakt; 070430 p�d

BOOL CDBHandleCalc::getTraktMiscData(int trakt_id,CTransaction_trakt_misc_data &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tprl_trakt_id=:1"),TBL_TRAKT_MISC_DATA);		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec = CTransaction_trakt_misc_data(m_saCommand.Field(1).asLong(),
												m_saCommand.Field(2).asString(),
												m_saCommand.Field(3).asLong(),
												m_saCommand.Field(4).asLongChar(),
												m_saCommand.Field(5).asString(),
												m_saCommand.Field(6).asLong(),
												m_saCommand.Field(7).asLongChar(),
												m_saCommand.Field(8).asDouble(),
												convertSADateTime(saDateTime));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Handle Sampletrees; 070510 p�d
BOOL CDBHandleCalc::getSampleTrees(int trakt_id,int tree_type,vecTransactionSampleTree &vec)
{
	CString sSQL,S;

	try
	{
		vec.clear();

		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (tree_type == SAMPLE_TREE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= SAMPLE_TREE;
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else if (tree_type == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE;
		}
		else if (tree_type == TREE_OUTSIDE_NO_SAMP)	// "Kanttr�d (Ber�knad h�jd)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}
		else if (tree_type == TREE_SAMPLE_REMAINIG)	// "Provtr�d (Kvarl�mmnat)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_SAMPLE_REMAINIG;
		}
		else if (tree_type == TREE_SAMPLE_EXTRA)	// "Provtr�d (Extra)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
			m_saCommand.Param(2).setAsLong()		= TREE_SAMPLE_EXTRA;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= trakt_id;
		}
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_sample_tree(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			m_saCommand.Field(3).asLong(),
																			m_saCommand.Field(4).asLong(),
																		  m_saCommand.Field(5).asString(),
																		  m_saCommand.Field(6).asDouble(),
																		  m_saCommand.Field(7).asDouble(),
																		  m_saCommand.Field(8).asDouble(),
																			m_saCommand.Field(9).asDouble(),
																			m_saCommand.Field(10).asDouble(),
																			m_saCommand.Field(11).asLong(),
																			m_saCommand.Field(12).asLong(),
																			m_saCommand.Field(14).asDouble(),
																			m_saCommand.Field(15).asDouble(),
																			m_saCommand.Field(16).asDouble(),
																			m_saCommand.Field(17).asDouble(),
																			m_saCommand.Field(18).asDouble(),
																			m_saCommand.Field(13).asLong(),
																			m_saCommand.Field(19).asString(),
																			convertSADateTime(saDateTime),
																			m_saCommand.Field(_T("ttree_coord")).asString(),
																			m_saCommand.Field(_T("ttree_distcable")).asLong(),
																			m_saCommand.Field(_T("ttree_category")).asLong(),
																			m_saCommand.Field(_T("ttree_topcut")).asLong()));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getSampleTree(CTransaction_sample_tree &rec1,CTransaction_sample_tree rec)
{
	CString sSQL;
	try
	{
		// Get ALL Sample trees, both "Provtr�d" and "Kanttr�d (Provtr�d)"; 071123 p�d
		if (rec.getTreeType() == SAMPLE_TREE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and (ttree_tree_type=:3 or ttree_tree_type=:4)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= SAMPLE_TREE;
			m_saCommand.Param(4).setAsLong()		= TREE_OUTSIDE;
		}
		if (rec.getTreeType() == TREE_OUTSIDE)
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and (ttree_tree_type=:3)"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else if (rec.getTreeType() == TREE_OUTSIDE_NO_SAMP)	// "Kanttr�d (Ber�knad h�jd)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}
		else if (rec.getTreeType() == TREE_SAMPLE_REMAINIG)	// "Provtr�d (Kvarl�mmnat)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_SAMPLE_REMAINIG;
		}
		else if (rec.getTreeType() == TREE_SAMPLE_EXTRA)	// "Provtr�d (Extra)"
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2 and ttree_tree_type=:3"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= TREE_SAMPLE_EXTRA;
		}
		else
		{
			sSQL.Format(_T("select * from %s where ttree_trakt_id=:1 and ttree_id=:2"),
				TBL_TRAKT_SAMPLE_TREES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		}
		
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_sample_tree(m_saCommand.Field(1).asLong(),
											m_saCommand.Field(2).asLong(),
											m_saCommand.Field(3).asLong(),
											m_saCommand.Field(4).asLong(),
											m_saCommand.Field(5).asString(),
											m_saCommand.Field(6).asDouble(),
											m_saCommand.Field(7).asDouble(),
											m_saCommand.Field(8).asDouble(),
											m_saCommand.Field(9).asDouble(),
											m_saCommand.Field(10).asDouble(),
											m_saCommand.Field(11).asLong(),
											m_saCommand.Field(12).asLong(),
											m_saCommand.Field(14).asDouble(),
											m_saCommand.Field(15).asDouble(),
											m_saCommand.Field(16).asDouble(),
											m_saCommand.Field(17).asDouble(),
											m_saCommand.Field(18).asDouble(),
											m_saCommand.Field(13).asLong(),
											m_saCommand.Field(19).asString(),
											convertSADateTime(saDateTime),
											m_saCommand.Field(_T("ttree_coord")).asString(),
											m_saCommand.Field(_T("ttree_distcable")).asLong(),
											m_saCommand.Field(_T("ttree_category")).asLong(),
											m_saCommand.Field(_T("ttree_topcut")).asLong());

		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addSampleTrees(CTransaction_sample_tree &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	CString S;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktSampleTreeExists(rec))
		{

			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (ttree_id,ttree_trakt_id,ttree_plot_id,ttree_spc_id,ttree_spc_name,ttree_dbh,ttree_hgt,ttree_gcrown,ttree_bark_thick,")
										_T("ttree_grot,ttree_age,ttree_growth,ttree_dcls_from,ttree_dcls_to,ttree_m3sk,ttree_m3fub,ttree_m3ub,ttree_tree_type,ttree_bonitet,ttree_coord,ttree_distcable,ttree_category,ttree_topcut) ")
										_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23)"),
										TBL_TRAKT_SAMPLE_TREES);

				m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3Sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3Fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3Ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
*/
				//-------------------------------------------------------------

				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDBH();
				m_saCommand.Param(7).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(11).setAsLong()		= rec.getAge();
				m_saCommand.Param(12).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(13).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(14).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(15).setAsDouble()		= rec.getM3Sk(); // fM3Sk_rounded
				m_saCommand.Param(16).setAsDouble()		= rec.getM3Fub(); // fM3Fub_rounded
				m_saCommand.Param(17).setAsDouble()		= rec.getM3Ub();	// fM3Ub_rounded
				m_saCommand.Param(18).setAsLong()		= rec.getTreeType();
				m_saCommand.Param(19).setAsString()		= rec.getBonitet();
				m_saCommand.Param(20).setAsString()		= rec.getCoord();
				m_saCommand.Param(21).setAsLong()		= rec.getDistCable();
				m_saCommand.Param(22).setAsLong()		= rec.getCategory();
				m_saCommand.Param(23).setAsLong()		= rec.getTopCut();

				m_saCommand.Execute();
			}
		}	// if (!traktSampleTreeExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::updSampleTrees(CTransaction_sample_tree &rec)
{
	CString sSQL,S;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	BOOL bReturn = FALSE;

	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{
			sSQL.Format(_T("update %s set ttree_plot_id=:1,ttree_spc_id=:2,ttree_spc_name=:3,ttree_dbh=:4,ttree_hgt=:5,ttree_gcrown=:6,ttree_bark_thick=:7,")
									_T("ttree_grot=:8,ttree_age=:9,ttree_growth=:10,ttree_dcls_from=:11,ttree_dcls_to=:12,ttree_m3sk=:13,ttree_m3fub=:14,ttree_m3ub=:15,ttree_tree_type=:16,ttree_bonitet=:17,created=GETDATE(),ttree_coord=:18,ttree_distcable=:19,ttree_category=:20,ttree_topcut=:21")
									_T("where ttree_id=:22 and ttree_trakt_id=:23"),
									TBL_TRAKT_SAMPLE_TREES);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
			_stprintf(tmp,_T("%.3f"),rec.getM3Sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3Fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3Ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
*/
			//-------------------------------------------------------------

			m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDBH();
			m_saCommand.Param(5).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
			m_saCommand.Param(6).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(7).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
			m_saCommand.Param(8).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(9).setAsLong()			= rec.getAge();
			m_saCommand.Param(10).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(11).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(12).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(13).setAsDouble()		= rec.getM3Sk(); //fM3Sk_rounded;
			m_saCommand.Param(14).setAsDouble()		= rec.getM3Fub(); //fM3Fub_rounded;
			m_saCommand.Param(15).setAsDouble()		= rec.getM3Ub(); //fM3Ub_rounded;
			m_saCommand.Param(16).setAsLong()		= rec.getTreeType();
			m_saCommand.Param(17).setAsString()		= rec.getBonitet();

			m_saCommand.Param(18).setAsString()		= rec.getCoord();
			m_saCommand.Param(19).setAsLong()		= rec.getDistCable();
			m_saCommand.Param(20).setAsLong()		= rec.getCategory();
			m_saCommand.Param(21).setAsLong()		= rec.getTopCut();

			m_saCommand.Param(22).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(23).setAsLong()		= rec.getTraktID();

			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}


BOOL CDBHandleCalc::updSampleTrees(vecTransactionSampleTree &vec)
{
	if (vec.empty()) return FALSE;

	try
	{
		// Put together update statement
		for (UINT i = 0;i < vec.size();i++)
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (vec[i].getSpcID() > 0)
			{
				m_saCommand.setCommandText(_T("UPDATE esti_trakt_sample_trees_table ")
											_T("SET ttree_hgt = :1, ttree_dcls_from = :2, ttree_dcls_to = :3, ttree_m3sk = :4, ttree_m3fub = :5, ttree_m3ub = :6 ,ttree_grot = :7 ")
											_T("WHERE ttree_id = :8 AND ttree_trakt_id = :9"));
				m_saCommand.Param(1).setAsDouble() = vec[i].getHgt();
				m_saCommand.Param(2).setAsDouble() = vec[i].getDCLS_from();
				m_saCommand.Param(3).setAsDouble() = vec[i].getDCLS_to();
				m_saCommand.Param(4).setAsDouble() = vec[i].getM3Sk();
				m_saCommand.Param(5).setAsDouble() = vec[i].getM3Fub();
				m_saCommand.Param(6).setAsDouble() = vec[i].getM3Ub();
				m_saCommand.Param(7).setAsDouble() = vec[i].getGROT();
				m_saCommand.Param(8).setAsLong() = vec[i].getTreeID();
				m_saCommand.Param(9).setAsLong() = vec[i].getTraktID();
				m_saCommand.Execute();
			}
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::delSampleTreesInTrakt(int trakt_id,int tree_type)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (tree_type == SAMPLE_TREE || tree_type == TREE_OUTSIDE)
		{
			sSQL.Format(_T("delete from %s where ttree_trakt_id=:1 and (ttree_tree_type=:2 or ttree_tree_type=:3)"),
									TBL_TRAKT_SAMPLE_TREES);
				m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
				m_saCommand.Param(1).setAsLong()		= trakt_id;
				m_saCommand.Param(2).setAsLong()		= SAMPLE_TREE;
				m_saCommand.Param(3).setAsLong()		= TREE_OUTSIDE;
		}
		else
		{
			sSQL.Format(_T("delete from %s where ttree_trakt_id=:1 and ttree_tree_type=:2"),
									TBL_TRAKT_SAMPLE_TREES);
				m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
				m_saCommand.Param(1).setAsLong()		= trakt_id;
				m_saCommand.Param(2).setAsLong()		= TREE_OUTSIDE_NO_SAMP;
		}

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandleCalc::getDCLSAllTrees(int trakt_id, vecTransactionDCLSTree &vec0, vecTransactionDCLSTree &vec1, vecTransactionDCLSTree &vec2)
{
	try
	{
		// Grab data for all dcls types in a single query
		// Pad any differences in amount of columns between tables with 0!
		m_saCommand.setCommandText( _T("SELECT *, 0 dtype FROM esti_trakt_dcls_trees_table WHERE tdcls_trakt_id = :1 ")
									_T("UNION ")
									_T("SELECT *, 0, 1 dtype FROM esti_trakt_dcls1_trees_table WHERE tdcls1_trakt_id = :1 ")
									_T("UNION ")
									_T("SELECT *, 0, 2 dtype FROM esti_trakt_dcls2_trees_table WHERE tdcls2_trakt_id = :1") );
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			CTransaction_dcls_tree rec = CTransaction_dcls_tree( m_saCommand.Field(1).asLong(),
																 m_saCommand.Field(2).asLong(),
																 m_saCommand.Field(3).asLong(),
																 m_saCommand.Field(4).asLong(),
																 m_saCommand.Field(5).asString(),
																 m_saCommand.Field(6).asDouble(),
																 m_saCommand.Field(7).asDouble(),
																 m_saCommand.Field(8).asDouble(),
																 m_saCommand.Field(9).asLong(),
																 m_saCommand.Field(10).asDouble(),
																 m_saCommand.Field(11).asDouble(),
																 m_saCommand.Field(12).asDouble(),
																 m_saCommand.Field(13).asDouble(),
																 m_saCommand.Field(14).asDouble(),
																 m_saCommand.Field(15).asDouble(),
																 m_saCommand.Field(16).asLong(),
																 m_saCommand.Field(17).asLong(),
																 m_saCommand.Field(19).asLong(),
																 convertSADateTime(m_saCommand.Field("created").asDateTime()) );

			// Determine type of dcls
			switch( m_saCommand.Field(_T("dtype")).asLong() )
			{
			case 0:
				vec0.push_back(rec);
				break;
			case 1:
				vec1.push_back(rec);
				break;
			case 2:
				vec2.push_back(rec);
				break;
			}
		}
	}
	catch( SAException &e )
	{
		UMMessageBox( e.ErrText() );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Handle DCLSrees; 070820 p�d
BOOL CDBHandleCalc::getDCLSTrees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL,S;
	int ant = 0;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1"),
			TBL_TRAKT_DCLS_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																			m_saCommand.Field(2).asLong(),
																			m_saCommand.Field(3).asLong(),
																			m_saCommand.Field(4).asLong(),
																		  m_saCommand.Field(5).asString(),
																		  m_saCommand.Field(6).asDouble(),
																		  m_saCommand.Field(7).asDouble(),
																		  m_saCommand.Field(8).asDouble(),
																		  m_saCommand.Field(9).asLong(),
																			m_saCommand.Field(10).asDouble(),
																			m_saCommand.Field(11).asDouble(),
																			m_saCommand.Field(12).asDouble(),
																			m_saCommand.Field(13).asDouble(),
																			m_saCommand.Field(14).asDouble(),
																			m_saCommand.Field(15).asDouble(),
																			m_saCommand.Field(16).asLong(),
																			m_saCommand.Field(17).asLong(),
																			m_saCommand.Field(19).asLong(),
																			convertSADateTime(saDateTime) ));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getDCLSTree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1 and tdcls_id=:2"),
				TBL_TRAKT_DCLS_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
											m_saCommand.Field(2).asLong(),
											m_saCommand.Field(3).asLong(),
											m_saCommand.Field(4).asLong(),
											m_saCommand.Field(5).asString(),
											m_saCommand.Field(6).asDouble(),
											m_saCommand.Field(7).asDouble(),
											m_saCommand.Field(8).asDouble(),
											m_saCommand.Field(9).asLong(),
											m_saCommand.Field(10).asDouble(),
											m_saCommand.Field(11).asDouble(),
											m_saCommand.Field(12).asDouble(),
											m_saCommand.Field(13).asDouble(),
											m_saCommand.Field(14).asDouble(),
											m_saCommand.Field(15).asDouble(),
											m_saCommand.Field(16).asLong(),
											m_saCommand.Field(17).asLong(),
											m_saCommand.Field(19).asLong(),
											convertSADateTime(saDateTime) );
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addDCLSTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLSTreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls_id,tdcls_trakt_id,tdcls_plot_id,tdcls_spc_id,tdcls_spc_name,tdcls_dcls_from,tdcls_dcls_to,tdcls_hgt,tdcls_numof,tdcls_gcrown,tdcls_bark_thick,")
										_T("tdcls_m3sk,tdcls_m3fub,tdcls_m3ub,tdcls_grot,tdcls_age,tdcls_growth,tdcls_numof_randtrees) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)"),
										TBL_TRAKT_DCLS_TREES);

				m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(18).setAsLong()		= rec.getNumOfRandTrees();	// Added 080307 p�d

				m_saCommand.Execute();
			}
		}	// if (!spcExist(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updDCLSTrees(CTransaction_dcls_tree &rec)
{
	CString sSQL,S;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	BOOL bReturn = FALSE;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{
			sSQL.Format(_T("update %s set tdcls_plot_id=:1,tdcls_spc_id=:2,tdcls_spc_name=:3,tdcls_dcls_from=:4,tdcls_dcls_to=:5,tdcls_hgt=:6,tdcls_numof=:7,")
									_T("tdcls_gcrown=:8,tdcls_bark_thick=:9,tdcls_m3sk=:10,tdcls_m3fub=:11,tdcls_m3ub=:12,tdcls_grot=:13,tdcls_age=:14,tdcls_growth=:15,tdcls_numof_randtrees=:16,created=GETDATE() ")
									_T("where tdcls_id=:17 and tdcls_trakt_id=:18"),TBL_TRAKT_DCLS_TREES);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
			_stprintf(tmp,_T("%.3f"),rec.getM3sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------
*/
			m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
			m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
			m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
			m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
			m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
			m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(14).setAsLong()		= rec.getAge();
			m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(16).setAsLong()		= rec.getNumOfRandTrees();	// Added 080307 p�d
			m_saCommand.Param(17).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(18).setAsLong()		= rec.getTraktID();

			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}


	return bReturn;
}



BOOL CDBHandleCalc::updDCLSTrees(vecTransactionDCLSTree &vec)
{
	if (vec.empty()) return FALSE;

	try
	{
		// Put together update statements
		for (UINT i = 0;i < vec.size();i++)
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (vec[i].getSpcID() > 0)
			{
				m_saCommand.setCommandText(_T("UPDATE esti_trakt_dcls_trees_table ")
											_T("SET tdcls_hgt = :1, tdcls_gcrown = :2, tdcls_bark_thick = :3, tdcls_m3sk = :4, tdcls_m3fub = :5, tdcls_m3ub = :6, tdcls_grot = :7, tdcls_age = :8,tdcls_growth = :9, tdcls_numof_randtrees = :10 ")
											_T("WHERE tdcls_id = :11 AND tdcls_trakt_id = :12"));
				m_saCommand.Param(1).setAsDouble() = vec[i].getHgt();
				m_saCommand.Param(2).setAsDouble() = vec[i].getGCrownPerc();
				m_saCommand.Param(3).setAsDouble() = vec[i].getBarkThick();
				m_saCommand.Param(4).setAsDouble() = vec[i].getM3sk();
				m_saCommand.Param(5).setAsDouble() = vec[i].getM3fub();
				m_saCommand.Param(6).setAsDouble() = vec[i].getM3ub();
				m_saCommand.Param(7).setAsDouble() = vec[i].getGROT();
				m_saCommand.Param(8).setAsLong() = vec[i].getAge();
				m_saCommand.Param(9).setAsLong() = vec[i].getGrowth();
				m_saCommand.Param(10).setAsLong() = vec[i].getNumOfRandTrees();
				m_saCommand.Param(11).setAsLong() = vec[i].getTreeID();
				m_saCommand.Param(12).setAsLong() = vec[i].getTraktID();
				m_saCommand.Execute();
			}
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::updDCLSTrees_m3fub(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Fub_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{

			sSQL.Format(_T("update %s set tdcls_m3fub=:1,created=GETDATE() where tdcls_id=:2 and tdcls_trakt_id=:3"),TBL_TRAKT_DCLS_TREES);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
			//-------------------------------------------------------------
			// COMMENTED OUT 2010-02-08 P�D
			// Dont' round data before save to DB-table; 100208 p�d
			//_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			//fM3Fub_rounded = _tstof(tmp);
			//-------------------------------------------------------------
			m_saCommand.Param(1).setAsDouble()	= rec.getM3fub(); //fM3Fub_rounded;
			m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(3).setAsLong()		= rec.getTraktID();

			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL  CDBHandleCalc::updDCLSTrees_m3fub(vecTransactionDCLSTree &vec,std::map<int,double>& map)
{
	CString sSQL,sTmp;
//	double fM3Fub_rounded = 0.0;
	TCHAR tmp[127];

	if (vec.empty()) return FALSE;

	try
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (vec[i].getSpcID() > 0)
			{
				//-------------------------------------------------------------
				// COMMENTED OUT 2010-02-08 P�D
				// Dont' round data before save to DB-table; 100208 p�d
				//	_stprintf(tmp,_T("%.3f"),vec[i].getM3fub());
				//	fM3Fub_rounded = _tstof(tmp);
				//-------------------------------------------------------------

				sTmp.Format(_T("update %s set tdcls_m3fub=%.f,created=GETDATE() where tdcls_id=%d and tdcls_trakt_id=%d"),
					TBL_TRAKT_DCLS_TREES,
					vec[i].getM3fub(), //fM3Fub_rounded,
					vec[i].getTreeID(),
					vec[i].getTraktID());

				map[vec[i].getSpcID()] += vec[i].getM3fub();

				sSQL += sTmp;
			}
		}
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::delDCLSTreesInTrakt(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls_trakt_id=:1"),TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::delDCLSTreesInTrakt(int dcls_id, int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls_id=:1 AND tdcls_trakt_id=:2"),TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong() = dcls_id;
		m_saCommand.Param(2).setAsLong() = trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getDCLSNumOfTrees(int trakt_id, double areal_factor, std::map<int,int> &numTrees)
{
	// Make sure target map is empty
	numTrees.clear();

	try
	{
		// Sum up num trees + num randtrees per specie for specified stand
		m_saCommand.setCommandText( _T("SELECT tdcls_spc_id, SUM(tdcls_numof + tdcls_numof_randtrees) FROM esti_trakt_dcls_trees_table WHERE tdcls_trakt_id = :1 GROUP BY tdcls_spc_id") );
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			numTrees[m_saCommand.Field(1).asLong()] = static_cast<int>((m_saCommand.Field(2).asDouble() * areal_factor) + 0.5); // Scale & round value before inserting
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getDCLSNumOfTrees(int trakt_id,int spc_id,double areal_factor,long* num_of_trees)
{
	CString sSQL;
	char szFmt[32];
	double fNumOfTrees = 0.0;
	try
	{
		sSQL.Format(_T("select sum(tdcls_numof+tdcls_numof_randtrees) as 'num_of' from %s where tdcls_trakt_id=:1 and tdcls_spc_id=:2"),
								TBL_TRAKT_DCLS_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Param(2).setAsLong()		= spc_id;
		m_saCommand.Execute();	

		while(m_saCommand.FetchNext())
		{
			fNumOfTrees = m_saCommand.Field(_T("num_of")).asLong()*areal_factor;
			sprintf_s(szFmt,16,"%.0f",fNumOfTrees);
		}
		*num_of_trees = atof(szFmt);
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////
// Handle DCLS1Trees; 071120 p�d
BOOL CDBHandleCalc::getDCLS1Trees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls1_trakt_id=:1"),
						TBL_TRAKT_DCLS1_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asLong(),
													m_saCommand.Field(17).asLong(),
													0,
													convertSADateTime(saDateTime) ));
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getDCLS1Tree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1 and tdcls_id=:2"),
				TBL_TRAKT_DCLS1_TREES);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
											m_saCommand.Field(2).asLong(),
											m_saCommand.Field(3).asLong(),
											m_saCommand.Field(4).asLong(),
											m_saCommand.Field(5).asString(),
											m_saCommand.Field(6).asDouble(),
											m_saCommand.Field(7).asDouble(),
											m_saCommand.Field(8).asDouble(),
											m_saCommand.Field(9).asLong(),
											m_saCommand.Field(10).asDouble(),
											m_saCommand.Field(11).asDouble(),
											m_saCommand.Field(12).asDouble(),
											m_saCommand.Field(13).asDouble(),
											m_saCommand.Field(14).asDouble(),
											m_saCommand.Field(15).asDouble(),
											m_saCommand.Field(16).asLong(),
											m_saCommand.Field(17).asLong(),
											0,
											convertSADateTime(saDateTime) );
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addDCLS1Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLS1TreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls1_id,tdcls1_trakt_id,tdcls1_plot_id,tdcls1_spc_id,tdcls1_spc_name,tdcls1_dcls_from,tdcls1_dcls_to,tdcls1_hgt,tdcls1_numof,tdcls1_gcrown,tdcls1_bark_thick,")
										_T("tdcls1_m3sk,tdcls1_m3fub,tdcls1_m3ub,tdcls1_grot,tdcls1_age,tdcls1_growth) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),TBL_TRAKT_DCLS1_TREES);

				m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();
				m_saCommand.Execute();
			}
		}	// if (!traktDCLS1TreeExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updDCLS1Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{

			sSQL.Format(_T("update %s set tdcls1_plot_id=:1,tdcls1_spc_id=:2,tdcls1_spc_name=:3,tdcls1_dcls_from=:4,tdcls1_dcls_to=:5,tdcls1_hgt=:6,tdcls1_numof=:7,")
									_T("tdcls1_gcrown=:8,tdcls1_bark_thick=:9,tdcls1_m3sk=:10,tdcls1_m3fub=:11,tdcls1_m3ub=:12,tdcls1_grot=:13,tdcls1_age=:14,tdcls1_growth=:15,created=GETDATE() ")
									_T("where tdcls1_id=:16 and tdcls1_trakt_id=:17"),TBL_TRAKT_DCLS1_TREES);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
			_stprintf(tmp,_T("%.3f"),rec.getM3sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------
*/
			m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
			m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
			m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
			m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
			m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
			m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(14).setAsLong()		= rec.getAge();
			m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(17).setAsLong()		= rec.getTraktID();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}


	return TRUE;
}


BOOL CDBHandleCalc::updDCLS1Trees(vecTransactionDCLSTree &vec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	CTransaction_dcls_tree rec;
	try
	{
		if (vec.empty()) return FALSE;

		sSQL.Format(_T("update %s set tdcls1_plot_id=:1,tdcls1_spc_id=:2,tdcls1_spc_name=:3,tdcls1_dcls_from=:4,tdcls1_dcls_to=:5,tdcls1_hgt=:6,tdcls1_numof=:7,")
								_T("tdcls1_gcrown=:8,tdcls1_bark_thick=:9,tdcls1_m3sk=:10,tdcls1_m3fub=:11,tdcls1_m3ub=:12,tdcls1_grot=:13,tdcls1_age=:14,tdcls1_growth=:15,created=GETDATE() ")
								_T("where tdcls1_id=:16 and tdcls1_trakt_id=:17"),TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		for (UINT i = 0;i < vec.size();i++)
		{
			rec = vec[i];
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(3).setAsString()		= rec.getSpcName();
				m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(14).setAsLong()		= rec.getAge();
				m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(17).setAsLong()		= rec.getTraktID();

				m_saCommand.Execute();
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}


	return TRUE;
}


BOOL CDBHandleCalc::delDCLS1TreesInTrakt(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls1_trakt_id=:1"),
								TBL_TRAKT_DCLS1_TREES);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()		= trakt_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Handle DCLS2Trees; 071120 p�d
BOOL CDBHandleCalc::getDCLS2Trees(int trakt_id,vecTransactionDCLSTree &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where tdcls2_trakt_id=:1"),TBL_TRAKT_DCLS2_TREES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			vec.push_back(CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
													m_saCommand.Field(2).asLong(),
													m_saCommand.Field(3).asLong(),
													m_saCommand.Field(4).asLong(),
													m_saCommand.Field(5).asString(),
													m_saCommand.Field(6).asDouble(),
													m_saCommand.Field(7).asDouble(),
													m_saCommand.Field(8).asDouble(),
													m_saCommand.Field(9).asLong(),
													m_saCommand.Field(10).asDouble(),
													m_saCommand.Field(11).asDouble(),
													m_saCommand.Field(12).asDouble(),
													m_saCommand.Field(13).asDouble(),
													m_saCommand.Field(14).asDouble(),
													m_saCommand.Field(15).asDouble(),
													m_saCommand.Field(16).asLong(),
													m_saCommand.Field(17).asLong(),
													0,
													convertSADateTime(saDateTime) ));


		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getDCLS2Tree(CTransaction_dcls_tree &rec1,CTransaction_dcls_tree rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where tdcls_trakt_id=:1 and tdcls_id=:2"),
				TBL_TRAKT_DCLS2_TREES);
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field("created").asDateTime();
			rec1 = CTransaction_dcls_tree(m_saCommand.Field(1).asLong(),
																		m_saCommand.Field(2).asLong(),
																		m_saCommand.Field(3).asLong(),
																		m_saCommand.Field(4).asLong(),
																		m_saCommand.Field(5).asString(),
																		m_saCommand.Field(6).asDouble(),
																		m_saCommand.Field(7).asDouble(),
																		m_saCommand.Field(8).asDouble(),
																		m_saCommand.Field(9).asLong(),
																		m_saCommand.Field(10).asDouble(),
																		m_saCommand.Field(11).asDouble(),
																		m_saCommand.Field(12).asDouble(),
																		m_saCommand.Field(13).asDouble(),
																		m_saCommand.Field(14).asDouble(),
																		m_saCommand.Field(15).asDouble(),
																		m_saCommand.Field(16).asLong(),
																		m_saCommand.Field(17).asLong(),
																		0,
																		convertSADateTime(saDateTime) );
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addDCLS2Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktDCLS2TreeExists(rec))
		{
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				sSQL.Format(_T("insert into %s (tdcls2_id,tdcls2_trakt_id,tdcls2_plot_id,tdcls2_spc_id,tdcls2_spc_name,tdcls2_dcls_from,tdcls2_dcls_to,tdcls2_hgt,tdcls2_numof,tdcls2_gcrown,tdcls2_bark_thick,")
										_T("tdcls2_m3sk,tdcls2_m3fub,tdcls2_m3ub,tdcls2_grot,tdcls2_age,tdcls2_growth) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)"),TBL_TRAKT_DCLS2_TREES);
				m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getTreeID();
				m_saCommand.Param(2).setAsLong()			= rec.getTraktID();
				m_saCommand.Param(3).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(4).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(5).setAsString()		= rec.getSpcName();
				m_saCommand.Param(6).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(7).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(8).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(9).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(10).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(11).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(14).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(15).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(16).setAsLong()		= rec.getAge();
				m_saCommand.Param(17).setAsLong()		= rec.getGrowth();

				m_saCommand.Execute();
			}
		}	// if (!traktDCLS2TreeExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updDCLS2Trees(CTransaction_dcls_tree &rec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	BOOL bReturn = FALSE;
	TCHAR tmp[127];
	try
	{
		// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
		if (rec.getSpcID() > 0)
		{

			sSQL.Format(_T("update %s set tdcls2_plot_id=:1,tdcls2_spc_id=:2,tdcls2_spc_name=:3,tdcls2_dcls_from=:4,tdcls2_dcls_to=:5,tdcls2_hgt=:6,tdcls2_numof=:7,")
									_T("tdcls2_gcrown=:8,tdcls2_bark_thick=:9,tdcls2_m3sk=:10,tdcls2_m3fub=:11,tdcls2_m3ub=:12,tdcls2_grot=:13,tdcls2_age=:14,tdcls2_growth=:15,created=GETDATE() ")
									_T("where tdcls2_id=:16 and tdcls2_trakt_id=:17"),TBL_TRAKT_DCLS2_TREES);
			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

			//-------------------------------------------------------------
			// Added 2007-08-21 (PL) p�d
			// Round M3Sk, M3Fub to 3 decimals
			// Round Barkthickness to 1 decimal (mm)
			// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
			_stprintf(tmp,_T("%.3f"),rec.getM3sk());
			fM3Sk_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3fub());
			fM3Fub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.3f"),rec.getM3ub());
			fM3Ub_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getHgt());
			fHgt_rounded = _tstof(tmp);
			_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
			fBark_rounded = _tstof(tmp);
			//-------------------------------------------------------------
*/
			m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
			m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
			m_saCommand.Param(3).setAsString()		= rec.getSpcName();
			m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
			m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
			m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
			m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
			m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
			m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
			m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
			m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
			m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
			m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
			m_saCommand.Param(14).setAsLong()		= rec.getAge();
			m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
			m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
			m_saCommand.Param(17).setAsLong()		= rec.getTraktID();

			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}



BOOL CDBHandleCalc::updDCLS2Trees(vecTransactionDCLSTree &vec)
{
	CString sSQL;
	//double fM3Sk_rounded = 0.0;
	//double fM3Fub_rounded = 0.0;
	//double fM3Ub_rounded = 0.0;
	//double fBark_rounded = 0.0;
	//double fHgt_rounded = 0.0;
	TCHAR tmp[127];
	CTransaction_dcls_tree rec;
	try
	{
		if (vec.empty()) return FALSE;

		sSQL.Format(_T("update %s set tdcls2_plot_id=:1,tdcls2_spc_id=:2,tdcls2_spc_name=:3,tdcls2_dcls_from=:4,tdcls2_dcls_to=:5,tdcls2_hgt=:6,tdcls2_numof=:7,")
								_T("tdcls2_gcrown=:8,tdcls2_bark_thick=:9,tdcls2_m3sk=:10,tdcls2_m3fub=:11,tdcls2_m3ub=:12,tdcls2_grot=:13,tdcls2_age=:14,tdcls2_growth=:15,created=GETDATE() ")
								_T("where tdcls2_id=:16 and tdcls2_trakt_id=:17"),TBL_TRAKT_DCLS2_TREES);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);

		// Only update if trakt exists and pricelist doesn't exist; 070507 p�d
		for (UINT i = 0;i < vec.size();i++)
		{
			rec = vec[i];
			// Check that we have a valid TreeID. I.e. TreeID > 0. If not don't add to database; 071009 p�d
			if (rec.getSpcID() > 0)
			{
				//-------------------------------------------------------------
				// Added 2007-08-21 (PL) p�d
				// Round M3Sk, M3Fub to 3 decimals
				// Round Barkthickness to 1 decimal (mm)
				// Round Height to 1 decimal (cm)
/*			COMMENTED OUT 2010-02-08 p�d
				Don't round data before adding to DB-table; 100208 p�d
				_stprintf(tmp,_T("%.3f"),rec.getM3sk());
				fM3Sk_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3fub());
				fM3Fub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.3f"),rec.getM3ub());
				fM3Ub_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getHgt());
				fHgt_rounded = _tstof(tmp);
				_stprintf(tmp,_T("%.1f"),rec.getBarkThick());
				fBark_rounded = _tstof(tmp);
				//-------------------------------------------------------------
*/
				m_saCommand.Param(1).setAsLong()			= rec.getPlotID();
				m_saCommand.Param(2).setAsLong()			= rec.getSpcID();
				m_saCommand.Param(3).setAsString()		= rec.getSpcName();
				m_saCommand.Param(4).setAsDouble()		= rec.getDCLS_from();
				m_saCommand.Param(5).setAsDouble()		= rec.getDCLS_to();
				m_saCommand.Param(6).setAsDouble()		= rec.getHgt(); //fHgt_rounded;
				m_saCommand.Param(7).setAsLong()			= rec.getNumOf();
				m_saCommand.Param(8).setAsDouble()		= rec.getGCrownPerc();
				m_saCommand.Param(9).setAsDouble()		= rec.getBarkThick(); //fBark_rounded;
				m_saCommand.Param(10).setAsDouble()		= rec.getM3sk(); //fM3Sk_rounded;
				m_saCommand.Param(11).setAsDouble()		= rec.getM3fub(); //fM3Fub_rounded;
				m_saCommand.Param(12).setAsDouble()		= rec.getM3ub(); //fM3Ub_rounded;
				m_saCommand.Param(13).setAsDouble()		= rec.getGROT();
				m_saCommand.Param(14).setAsLong()		= rec.getAge();
				m_saCommand.Param(15).setAsLong()		= rec.getGrowth();
				m_saCommand.Param(16).setAsLong()		= rec.getTreeID();
				m_saCommand.Param(17).setAsLong()		= rec.getTraktID();

				m_saCommand.Execute();
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::delDCLS2TreesInTrakt(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tdcls2_trakt_id=:1"),
								TBL_TRAKT_DCLS2_TREES);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addAssTree(CTransaction_tree_assort &rec)
{
	CString sSQL,S;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		sSQL.Format(_T("insert into %s (tassort_tree_trakt_id,tassort_tree_id,tassort_tree_dcls,tassort_spc_id,tassort_spc_name,tassort_assort_name,")
								_T("tassort_exch_volume,tassort_exch_price,tassort_price_in) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)"),TBL_TRAKT_TREES_ASSORT);

		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()		= rec.getTraktID();
		m_saCommand.Param(2).setAsLong()		= rec.getTreeID();
		m_saCommand.Param(3).setAsDouble()		= rec.getDCLS();
		m_saCommand.Param(4).setAsLong()		= rec.getSpcID();
		m_saCommand.Param(5).setAsString()		= rec.getSpcName();
		m_saCommand.Param(6).setAsString()		= rec.getAssortName();
		m_saCommand.Param(7).setAsDouble()		= rec.getExchVolume();
		m_saCommand.Param(8).setAsDouble()		= rec.getExchPrice();
		m_saCommand.Param(9).setAsLong()		= rec.getPriceIn();

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandleCalc::addAssTree(vecTransactionTreeAssort &vec)
{
	if (vec.empty()) return FALSE;

	try
	{
		// Put together insert statement
		for (UINT i = 0;i < vec.size();i++)
		{
			m_saCommand.setCommandText(_T("INSERT INTO esti_trakt_trees_assort_table ")
										_T("(tassort_tree_trakt_id, tassort_tree_id, tassort_tree_dcls, tassort_spc_id, tassort_spc_name, tassort_assort_name, tassort_exch_volume, tassort_exch_price, tassort_price_in) ")
										_T("VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9)"));
			m_saCommand.Param(1).setAsLong() = vec[i].getTraktID();
			m_saCommand.Param(2).setAsLong() = vec[i].getTreeID();
			m_saCommand.Param(3).setAsDouble() = vec[i].getDCLS();
			m_saCommand.Param(4).setAsLong() = vec[i].getSpcID();
			m_saCommand.Param(5).setAsString() = vec[i].getSpcName();
			m_saCommand.Param(6).setAsString() = vec[i].getAssortName();
			m_saCommand.Param(7).setAsDouble() = vec[i].getExchVolume();
			m_saCommand.Param(8).setAsDouble() = vec[i].getExchPrice();
			m_saCommand.Param(9).setAsLong() = vec[i].getPriceIn();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::delAssTree(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where tassort_tree_trakt_id=:1"),
								TBL_TRAKT_TREES_ASSORT);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// ==================================================================================
// "Rotpost"; 080505 p�d

BOOL CDBHandleCalc::getRotpost(CTransaction_trakt_rotpost &rec,int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("select * from %s where rot_trakt_id=:1"),TBL_TRAKT_ROTPOST);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = trakt_id;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			rec = CTransaction_trakt_rotpost(m_saCommand.Field(1).asLong(),
											 m_saCommand.Field(2).asDouble(),
											 m_saCommand.Field(3).asDouble(),
											 m_saCommand.Field(4).asDouble(),
											 m_saCommand.Field(5).asDouble(),
											 m_saCommand.Field(6).asDouble(),
											 m_saCommand.Field(7).asDouble(),
											 m_saCommand.Field(8).asDouble(),
											 m_saCommand.Field(9).asDouble(),
											 m_saCommand.Field(10).asDouble(),
											 m_saCommand.Field(11).asDouble(),
											 m_saCommand.Field(12).asDouble(),
											 m_saCommand.Field(13).asDouble(),
											 m_saCommand.Field(14).asShort(),
											 m_saCommand.Field(15).asString());
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

BOOL CDBHandleCalc::addRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	try
	{
		// Only insert if trakt exists and pricelist doesn't exist; 070507 p�d
		if (!traktRotpostExists(rec))
		{
			sSQL.Format(_T("insert into %s (rot_trakt_id,rot_post_volume,rot_post_value,rot_post_cost1,rot_post_cost2,rot_post_cost3,rot_post_cost4,")
									_T("rot_post_cost5,rot_post_cost6,rot_post_cost7,rot_post_cost8,rot_post_cost9,rot_post_netto,rot_post_origin) ")
									_T("values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)"),TBL_TRAKT_ROTPOST);

			m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
			m_saCommand.Param(1).setAsLong()		= rec.getRotTraktID();
			m_saCommand.Param(2).setAsDouble()		= rec.getVolume();
			m_saCommand.Param(3).setAsDouble()		= rec.getValue();
			m_saCommand.Param(4).setAsDouble()		= rec.getCost1();
			m_saCommand.Param(5).setAsDouble()		= rec.getCost2();
			m_saCommand.Param(6).setAsDouble()		= rec.getCost3();
			m_saCommand.Param(7).setAsDouble()		= rec.getCost4();
			m_saCommand.Param(8).setAsDouble()		= rec.getCost5();
			m_saCommand.Param(9).setAsDouble()		= rec.getCost6();
			m_saCommand.Param(10).setAsDouble()		= rec.getCost7();
			m_saCommand.Param(11).setAsDouble()		= rec.getCost8();
			m_saCommand.Param(12).setAsDouble()		= rec.getCost9();
			m_saCommand.Param(13).setAsDouble()		= rec.getNetto();
			m_saCommand.Param(14).setAsShort()		= rec.getOrigin();

			m_saCommand.Execute();
		}	// if (!traktRotpostExists(rec))
		else
		{
			return FALSE;
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::updRotpost(CTransaction_trakt_rotpost &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("update %s set rot_post_volume=:1,rot_post_value=:2,rot_post_cost1=:3,rot_post_cost2=:4,rot_post_cost3=:5,rot_post_cost4=:6,rot_post_cost5=:7,rot_post_cost6=:8,")
								_T("rot_post_cost7=:9,rot_post_cost8=:10,rot_post_cost9=:11,rot_post_netto=:12,rot_post_origin=:13,created=GETDATE() where rot_trakt_id=:14"),TBL_TRAKT_ROTPOST);

		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsDouble()		= rec.getVolume();
		m_saCommand.Param(2).setAsDouble()		= rec.getValue();
		m_saCommand.Param(3).setAsDouble()		= rec.getCost1();
		m_saCommand.Param(4).setAsDouble()		= rec.getCost2();
		m_saCommand.Param(5).setAsDouble()		= rec.getCost3();
		m_saCommand.Param(6).setAsDouble()		= rec.getCost4();
		m_saCommand.Param(7).setAsDouble()		= rec.getCost5();
		m_saCommand.Param(8).setAsDouble()		= rec.getCost6();
		m_saCommand.Param(9).setAsDouble()		= rec.getCost7();
		m_saCommand.Param(10).setAsDouble()		= rec.getCost8();
		m_saCommand.Param(11).setAsDouble()		= rec.getCost9();
		m_saCommand.Param(12).setAsDouble()		= rec.getNetto();
		m_saCommand.Param(13).setAsShort()		= rec.getOrigin();
		m_saCommand.Param(14).setAsLong()		= rec.getRotTraktID();

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

BOOL CDBHandleCalc::addRotpostSpc(vecTransaction_trakt_rotpost_spc &vec)
{
	try
	{
		for( UINT i = 0; i < vec.size(); i++ )
		{
			m_saCommand.setCommandText(_T("INSERT INTO esti_trakt_rotpost_spc_table (spcrot_id,spcrot_trakt_id,spcrot_post_spcname,spcrot_post_cost1,spcrot_post_cost2,spcrot_post_cost3,spcrot_post_cost4,")
										_T("spcrot_post_cost5,spcrot_post_cost6,spcrot_post_origin) ")
										_T("VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10)"));
			m_saCommand.Param(1).setAsLong() = vec[i].getRotSpcID_pk();
			m_saCommand.Param(2).setAsLong() = vec[i].getRotSpcTraktID_pk();
			m_saCommand.Param(3).setAsString() = vec[i].getSpcName();
			m_saCommand.Param(4).setAsDouble() = vec[i].getCost1();
			m_saCommand.Param(5).setAsDouble() = vec[i].getCost2();
			m_saCommand.Param(6).setAsDouble() = vec[i].getCost3();
			m_saCommand.Param(7).setAsDouble() = vec[i].getCost4();
			m_saCommand.Param(8).setAsDouble() = vec[i].getCost5();
			m_saCommand.Param(9).setAsDouble() = vec[i].getCost6();
			m_saCommand.Param(10).setAsLong() = vec[i].getOrigin();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
// "Rotpost Transport och Huggningskostnader per tr�dslag"; 080505 p�d
BOOL CDBHandleCalc::addRotpostSpc(CTransaction_trakt_rotpost_spc &rec)
{
	try
	{
		m_saCommand.setCommandText(_T("insert into esti_trakt_rotpost_spc_table (spcrot_id,spcrot_trakt_id,spcrot_post_spcname,spcrot_post_cost1,spcrot_post_cost2,spcrot_post_cost3,spcrot_post_cost4,")
									_T("spcrot_post_cost5,spcrot_post_cost6,spcrot_post_origin) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10)"));
		m_saCommand.Param(1).setAsLong()	= rec.getRotSpcID_pk();
		m_saCommand.Param(2).setAsLong()	= rec.getRotSpcTraktID_pk();
		m_saCommand.Param(3).setAsString()	= rec.getSpcName();
		m_saCommand.Param(4).setAsDouble()	= rec.getCost1();
		m_saCommand.Param(5).setAsDouble()	= rec.getCost2();
		m_saCommand.Param(6).setAsDouble()	= rec.getCost3();
		m_saCommand.Param(7).setAsDouble()	= rec.getCost4();
		m_saCommand.Param(8).setAsDouble()	= rec.getCost5();
		m_saCommand.Param(9).setAsDouble()	= rec.getCost6();
		m_saCommand.Param(10).setAsShort()	= rec.getOrigin();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::delRotpostSpc(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where spcrot_trakt_id=:1"),
								TBL_TRAKT_ROTPOST_SPC);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()		= trakt_id;

		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::addRotpostOtc(vecTransaction_trakt_rotpost_other &vec)
{
	try
	{
		for( UINT i = 0; i < vec.size(); i++ )
		{
			m_saCommand.setCommandText(_T("INSERT INTO esti_trakt_rotpost_other_table (otcrot_id,otcrot_trakt_id,otcrot_value,otcrot_sum_value,otcrot_text,otcrot_percent,otcrot_type) ")
										_T("VALUES (:1,:2,:3,:4,:5,:6,:7)"));
			m_saCommand.Param(1).setAsLong() = vec[i].getOtcRotID_pk();
			m_saCommand.Param(2).setAsLong() = vec[i].getOtcRotTraktID_pk();
			m_saCommand.Param(3).setAsDouble() = vec[i].getCost1();
			m_saCommand.Param(4).setAsDouble() = vec[i].getCost2();
			m_saCommand.Param(5).setAsString() = vec[i].getText();
			m_saCommand.Param(6).setAsDouble() = vec[i].getPercent();
			m_saCommand.Param(7).setAsLong() = vec[i].getType();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
// "Rotpost �vriga kostnader"; 080312
BOOL CDBHandleCalc::addRotpostOtc(CTransaction_trakt_rotpost_other &rec)
{
	CString sSQL,S;

	try
	{
		sSQL.Format(_T("insert into %s (otcrot_id,otcrot_trakt_id,otcrot_value,otcrot_sum_value,otcrot_text,otcrot_percent,otcrot_type) values(:1,:2,:3,:4,:5,:6,:7)"),
								TBL_TRAKT_ROTPOST_OTC);

		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()	= rec.getOtcRotID_pk();
		m_saCommand.Param(2).setAsLong()	= rec.getOtcRotTraktID_pk();
		m_saCommand.Param(3).setAsDouble()	= rec.getCost1();
		m_saCommand.Param(4).setAsDouble()	= rec.getCost2();
		m_saCommand.Param(5).setAsString()	= rec.getText();
		m_saCommand.Param(6).setAsDouble()	= rec.getPercent();
		m_saCommand.Param(7).setAsShort()	= rec.getType();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::delRotpostOtc(int trakt_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where otcrot_trakt_id=:1"),
								TBL_TRAKT_ROTPOST_OTC);
		m_saCommand.setCommandText((SAString)sSQL, SA_CmdSQLStmt);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////
// External database items.
// Properties is set in Forrest SUITE; 070305 p�d

// Special function combining 2 or more tables; 071024 p�d
// Changed; added b.tassort_price_in=3 because of change
// int Pricelist. Set timberprice in m3to=0 or m3fub=3; 080424 p�d
BOOL CDBHandleCalc::getExchPriceCalulatedInExchangeModule(int trakt_id,vecExchCalcInfo &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select a.tass_trakt_id,a.tass_trakt_data_id,a.tass_assort_id,b.tassort_exch_price,b.tassort_price_in from %s a,%s b where b.tassort_tree_trakt_id=a.tass_trakt_id and a.tass_trakt_id=:1 ")
								_T("and (b.tassort_price_in=0 or b.tassort_price_in=3) and b.tassort_assort_name=a.tass_assort_name and b.tassort_spc_id=a.tass_trakt_data_id"),
								TBL_TRAKT_SPC_ASS,
								TBL_TRAKT_TREES_ASSORT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(_exch_calc_info_struct(m_saCommand.Field(1).asLong(),
																					 m_saCommand.Field(2).asLong(),
																					 m_saCommand.Field(3).asLong(),
																					 m_saCommand.Field(4).asDouble(),
																					 m_saCommand.Field(5).asLong()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandleCalc::getSampletreesOutsideH25MinMax(int nTraktId, CStringArray* logText)
{
	CString sSQL;
	CString csBuf;

	try
	{
		sSQL.Format(_T("SELECT DISTINCT c.spc_id, c.spc_name, COUNT(c.spc_id) As Amount ")
			_T("FROM ")
			_T("%s b ")
			_T("LEFT JOIN %s a ")
			_T("ON a.ttree_spc_id = b.tsetspc_specie_id ")
			_T("LEFT JOIN %s c ")
			_T("ON b.tsetspc_id = c.spc_id ")
			_T("where ")
			_T("b.tsetspc_tdata_trakt_id = :1 AND a.ttree_trakt_id = :1 ")
			_T("AND (a.ttree_hgt < :2 OR a.ttree_hgt > :3) AND b.tsetspc_hgt_func_id = :4 AND (a.ttree_tree_type != 2 AND a.ttree_tree_type != 6 AND a.ttree_tree_type != 7 AND a.ttree_tree_type != 9) ")	// no need to check trees that should not have heights initialy
			_T("GROUP BY c.spc_id, c.spc_name "),
			TBL_TRAKT_SET_SPC,
			TBL_TRAKT_SAMPLE_TREES,
			TBL_SPECIES_TABLE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = nTraktId;
		m_saCommand.Param(2).setAsLong() = H25_MIN_VALUE*10.0;
		m_saCommand.Param(3).setAsLong() = H25_MAX_VALUE*10.0;
		m_saCommand.Param(4).setAsLong() = ID_HGT_H25;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			csBuf.Format(_T("%s (%d)"), m_saCommand.Field(2).asString(), m_saCommand.Field(3).asLong() );
			logText->Add(csBuf);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}



//==========================================================================================================

void getDiamClass(double dbh,double dcls,double *dcls_from,double *dcls_to)
{
	double fDBH = dbh/10.0;	// mm -> cm
	double fDCLS = 0.0;
	double fMaxDiam = 1000.0;	// 1000 cm (10.0 m)
	CString S;
	//---------------------------------------------------------
	// Try to find out the Diamterclass for each tree, and

	// Check if we have a diamterclass to work with.
	// If not set diamgerclass = 0.0; 070615 p�d
	if (dcls > 0.0 && fDBH > 0.0)
	{
		for (double fDCLS = 0.0;fDCLS < (fMaxDiam + dcls);fDCLS += dcls)
		{
			if (fDBH >= fDCLS && fDBH < (fDCLS + dcls))
			{
				*dcls_from = fDCLS;
				*dcls_to = fDCLS + dcls;
				return;
			}	// if (fDBH >= fDCLS && fDBH < fDCLS + m_recMiscData.getDiamClass())
		}	// for (double fDCLS = m_recMiscData.getDiamClass(); fDCLS < fMaxDia;fDCLS += m_recMiscData.getDiamClass())
	}
}

//==========================================================================================
// Method for calculating the "Rotpost" for Trakt
// based on data in: esti_trakt_spc_assort_table; 080505 p�d
void calcRotpostForTrakt(BOOL bConnected,
						 DB_CONNECTION_DATA m_dbConnectionData,
						 int trakt_id,
						 vecTransactionTraktAss *pTraktAss /*=NULL*/,
						 vecTransactionTraktSetSpc *pTraktSetSpc /*=NULL*/,
						 vecTransactionTraktData *pTraktData /*=NULL*/,
						 CTransaction_trakt_misc_data *pTraktMiscData /*=NULL*/)
{
	CDBHandleCalc *pDB = NULL;
	CString S;
	double fGangnVolM3 = 0.0;		// "Gangnvirkesvolym i kubikmeter m3, kan vara b�de m3fub och m3to"; 071011 p�d
	double fPulpwoodVolM3 = 0.0;	// "Volym massaaved i kubikmeter m3, kan vara b�de m3fub och m3to"; 071011 p�d
	double fGangnVolM3PerSpc = 0.0;	// "Gangnvirkesvolym i kubikmeter m3, kan vara b�de m3fub och m3to per tr�dslag"; 071011 p�d
	double fSumPrice = 0.0;			// Price for Trakt, depending on m3to or m3fub price; 071011 p�d
	double fCut1_cost = 0.0;		// "Avverkning"
	double fCut2_cost = 0.0;		// "Sk�rdare"
	double fCut3_cost = 0.0;		// "Skotare"
	double fCut4_cost = 0.0;		// "�vriga kostnader"
	double fCut5_cost = 0.0;		// "�vriga kostnader Intr. kostnadsmall"
	double fCut6_cost = 0.0;		// "�vriga kostnader Intr. kostnadsmall"
	double fCost_transp = 0.0;		// "Transportkostnad till v�g"
	double fCost_transp2 = 0.0;		// "Transportkostnad till Industri"
	double fCost_other	= 0.0;		// "Andra fasta kostnader"
	double fCost_total = 0.0;		// Total cost "ROTNETTO"
	//----------------------------------------------------------
	int nSetAs = -1;
	int nTranspTableValue = 0;
	int nTableTranspDist = 0;
	int nTranspTableIndex = -1;
	double fDGV = 0.0;
	double nDGV_table_prev = 0.0;
	double fSumM3FUB = 0.0;
	double fM3FUB_avg = 0.0;
	double fM3FUB_table_prev = 0.0;
	double fPercent = 0.0;
	double fValue = 0.0;
	double fVol = 0.0;
	int nCuttingTableValue = 0;
	double fCuttingTableValue = 0;
	int nTableCuttingDist = 0;
	int nCuttingTableIndex = -1;
	int nOtherCostCnt = 0;
	int nTrees = 0;
	int nValue=0;
	double fTranspCostSpc = 0.0;
	double fTransp2CostSpc = 0.0;
	double fTransp2CostSpcPerM3 = 0.0;
	double fMaxTransp2CostSpc = 0.0;
	double fCuttingCostSpc = 0.0;
	CString sSpecieName;
	BOOL bFound = FALSE;
	//----------------------------------------------------------

	CTransaction_costtempl_cutting recCutting;
	vecTransaction_costtempl_transport vecTransport;
	CTransaction_costtempl_transport recTransport;
	CTransaction_costtempl_other_costs recOtherCosts;
	vecTransactionTraktData vecTraktData;
	std::map<int,double> mapVolPerSpc;
	std::map<int,double> mapVolPulpwoodPerSpc;
	//----------------------------------------------------------
	CTransaction_trakt_set_spc recSetSpc;
	CObjectCostTemplate_table recObjCostTmpl;
	CTransaction_trakt_data recTraktData;
	CObjectCostTemplate_other_cost_table recOtherInfrCost;
	CObjectCostTemplate_other_cost_table recOtherCost;
	CTransaction_trakt_misc_data recTraktMiscData;

	vecObjectCostTemplate_table vecTranspTable;
	vecObjectCostTemplate_table vecCuttingDGVTable;
	vecObjectCostTemplate_table vecCuttingM3FUBTable;
	vecObjectCostTemplate_other_cost_table vecOtherCost;
	vecTransactionTraktAss vecTraktAss;			// esti_trees_assort_table
	vecTransaction_costtempl_transport vecTransport2Table;
	vecTransactionAssort vecAssortmentsPerSpc;	// XML pricelist vector
	vecTransactionSpecies vecSpecies;
	vecTransactionTraktSetSpc vecSetSpc;
	vecTransaction_trakt_rotpost_spc vecRotpostSpc;
	vecTransaction_trakt_rotpost_other vecRotpostOtc;


	int nPriceSetIn = 2;

	pDB = new CDBHandleCalc(m_dbConnectionData);
	if (pDB == NULL) return;

	//------------------------------------------------------------------------------
	// Load data from: esti_trakt_spc_assort_table
	// and from: esti_trakt_set_spc_table
	// Use values passed from doCruisingCalculations when possible; Optimization by Peter
	//if( pTraktAss )			vecTraktAss = *pTraktAss;
	//else					
	pDB->getTraktAss(vecTraktAss,trakt_id);						// Get data in table "esti_trakt_spc_assort_table" for Trakt; 070705 p�d
	if( pTraktSetSpc)		vecSetSpc = *pTraktSetSpc;
	else					pDB->getTraktSetSpc(vecSetSpc,trakt_id);					// Get data in "esti_trakt_set_spc_table" for Trakt; 080306 p�d
	if( pTraktData )		for(UINT i = 0; i < pTraktData->size(); i++) { if((*pTraktData)[i].getTDataType() == STMP_LEN_WITHDRAW) vecTraktData.push_back((*pTraktData)[i]); }
	else					pDB->getTraktData(vecTraktData,trakt_id,STMP_LEN_WITHDRAW);	// Get Trakt data, i.e. DGV etc; 080312 p�d
	if( pTraktMiscData )	recTraktMiscData = *pTraktMiscData;
	else					pDB->getTraktMiscData(trakt_id,recTraktMiscData);

	// .. and get information in pricelist (xml-file), saved; 070502 p�d
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(recTraktMiscData.getXMLPricelist()))
		{
			//---------------------------------------------------------------------------------
			// Get species in pricelist; 070430 p�d
			vecSpecies.clear();
			pPrlParser->getSpeciesInPricelistFile(vecSpecies);
			//---------------------------------------------------------------------------------
			// Get assortments species in pricelist; 070516 p�d
			vecAssortmentsPerSpc.clear();
			pPrlParser->getAssortmentPerSpecie(vecAssortmentsPerSpc);
			//---------------------------------------------------------------------------------
			pPrlParser->getHeaderPriceIn(&nPriceSetIn);
		}
		delete pPrlParser;
	}


	// Calculate Sum. "Gangnvirkesvolym", from vecTraktAss; 080312 p�d
	// Also calculate "Gagnvirkesvolym per Tr�dslag"; 080312 p�d
	if (vecTraktAss.size() > 0)
	{
		mapVolPulpwoodPerSpc.clear();
		mapVolPerSpc.clear();
		for (UINT i = 0;i  < vecTraktAss.size();i++)
		{
			CTransaction_trakt_ass recAss = vecTraktAss[i];
			
			// If theres volume in m3to (Timber), use this volume, else
			// use m3fub volume; 071011 p�d
			// Changed to ALWAYS SUM ON M3FUB VOLUME; 071129 p�d
			fGangnVolM3 += recAss.getM3FUB();

			// Try to find out if assortment is pulpwood or not; 080314 p�d
			// This is for sum. volume per specie for pulpwood, to be
			// used in "Transport till Industri"; 080314 p�d
			if (vecAssortmentsPerSpc.size() > 0)
			{
				for (UINT ii = 0;ii < vecAssortmentsPerSpc.size();ii++)
				{
					CTransaction_assort	rec = vecAssortmentsPerSpc[ii];
					if (rec.getPulpType() == 1 &&		// 1 = Pulpwood
						  recAss.getM3FUB() > 0 &&
							rec.getSpcID() == recAss.getTAssTraktDataID() &&
							rec.getAssortName().Compare(recAss.getTAssName()) == 0)
					{
						mapVolPulpwoodPerSpc[recAss.getTAssTraktDataID()] += recAss.getM3FUB();
						fPulpwoodVolM3 += recAss.getM3FUB();	// Sum. pulpwood; 080318 p�d
					}	// if (rec.getPulpType() == 1 && 
				}	// for (UINT ii = 0;ii < vecAssortmentsPerSpc.size();ii++)
			}	// if (vecAssortmentsPerSpc.size() > 0)
			mapVolPerSpc[recAss.getTAssTraktDataID()] += recAss.getM3FUB();

/*
			S.Format(_T("mapVolPerSpc[recAss.getTAssTraktDataID() %d] %f\nrecAss.getM3FUB() %f"),
				recAss.getTAssTraktDataID(),mapVolPerSpc[recAss.getTAssTraktDataID()],recAss.getM3FUB());
			UMMessageBox(S);
*/
		}	// for (UINT i = 0;i  < vecTraktAss.size();i++)
	}	// if (vecTraktAss.size() > 0)

	// Calculate Sum. "Pris", from vecTraktAss; 080312 p�d
	// This volume is the same for ALL Costtemplates; 080312 p�d
	if (vecTraktAss.size() > 0)
	{
		for (UINT i = 0;i  < vecTraktAss.size();i++)
		{
			CTransaction_trakt_ass recAss = vecTraktAss[i];
		
			// If theres price in m3to (Timber), use this value, else
			// use m3fub price; 071011 p�d
			// Added calcualtion for "kronor per kubikmeter"; 080305 p�d
/*
			if (nPriceSetIn == 1)
			{
				fSumPrice += recAss.getValueM3FUB(); // + (recAss.getM3TO()*recAss.getKrPerM3());
			}
			else if (nPriceSetIn == 2)
			{
				fSumPrice += recAss.getValueM3TO(); // + (recAss.getM3TO()*recAss.getKrPerM3());
			}
*/
//*
			if (recAss.getValueM3TO() > 0.0)
			{
				fSumPrice += recAss.getValueM3TO(); // + (recAss.getM3TO()*recAss.getKrPerM3());
		//		S.Format(L"M3TO Price %f   SUM %f",recAss.getValueM3TO(),fSumPrice);
			}	// if (recAss.getM3TO() > 0.0)
			else
			{
				fSumPrice += recAss.getValueM3FUB(); // + (recAss.getM3FUB()*recAss.getKrPerM3());
	//			S.Format(L"M3FUB Price %f   SUM %f",recAss.getValueM3FUB(),fSumPrice);
			}	// else

//			UMMessageBox(S);
//*/
		}	// for (UINT i = 0;i  < vecTraktAss.size();i++)
	}	// if (vecTraktAss.size() > 0)

	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//	KOSTNADSTYP 2
	//	Calulate "Rotpost f�r kostnadsmall (Intr�ngsv�rdering)"; 080306 p�d
	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	
	if (recTraktMiscData.getCostsTypeOf() == COST_TYPE_2)
	{
		// Load data from cost template in table:
		// esti_trakt_misc_data_table; 100104 p�d
		xmlliteCostsParser *parser = new xmlliteCostsParser();
		if (parser != NULL)
		{
			if (parser->loadStream(recTraktMiscData.getXMLCosts() ))
			{
				parser->getObjCostsTmplTransport(vecTranspTable);
				parser->getObjCostsTmplTransport2(vecTransport2Table);			
				
				parser->getCutCostSetAs(&nSetAs);	// 1 = "Huggningskostnad i kr/m3" 2 = "Huggningskostnad i tabell (dgv)"
				if (nSetAs == 1)
					parser->getObjCostsTmplCutting2(recCutting);
				else if (nSetAs == 2 || nSetAs == 3)
					parser->getObjCostsTmplCutting(vecCuttingDGVTable,vecCuttingM3FUBTable);

				parser->getObjCostsTmplOther(vecOtherCost);
			}
			delete parser;
		}

		// Remove ALL costs per specie in table for TraktID; 080312 p�d
		pDB->delRotpostSpc(trakt_id);

		fCost_transp = 0.0;		// "Summa transportkostnad till v�g"; 080312 p�d
		fCost_transp2 = 0.0;	// "Summa transportkostnad till Industri"; 080314 p�d
		fCut1_cost = 0.0;
		if (vecSetSpc.size() > 0)
		{
			for (UINT ui1 = 0;ui1 < vecSetSpc.size();ui1++)
			{
				fTranspCostSpc = 0.0;
				fTransp2CostSpc = 0.0;
				fTransp2CostSpcPerM3 = 0.0;
				fCuttingCostSpc = 0.0;
				nTranspTableValue = 0;
				bFound = FALSE;
				recSetSpc = vecSetSpc[ui1];

				//----------------------------------------------------------------------------
				// START: Calculate "Transportkostnad framk�rning till v�g per Tr�dslag"; 080306 p�d
				//----------------------------------------------------------------------------
				// We'll check the value of Transportdistance. If it's 0, there's no
				// need go on tryn' to find a value; 080306 p�d

				//if (recSetSpc.getTranspDist1() > 0) // #3873
				{
					// Try to find the transportcost for this specie; 080306 p�d
					if (vecTranspTable.size() > 0)
					{
						for (UINT ui2 = 0;ui2 < vecTranspTable.size();ui2++)
						{
							recObjCostTmpl = vecTranspTable[ui2];
							if (ui2 == 0)	// First row hold distance; 080306 p�d
							{
								// Make sure there's any data for "Transportkostnad"; 090428 p�d
								if (recObjCostTmpl.getValues_int().size() >= 1)	//�ven kontorllera avst�nd om bara ett avst�nd i tabell #4569
								{
									for (INT vi1 = recObjCostTmpl.getValues_int().size() - 1;vi1>=0;vi1--)
									{
										// Try to match the distance set for this specie to a
										// distance, set in costtable; 080306 p�d
										if (recSetSpc.getTranspDist1() >= recObjCostTmpl.getValues_int()[vi1])
										{
											nTranspTableIndex = vi1;
											bFound = TRUE;
											break;
										}	// if (recSetSpc.getTranspDist() <= recObjCostTmpl.getValues_int()[vi1])
									}	// for (UINT vi1 = 0;vi1 < recObjCostTmpl.getValues_int().size();vi1++)
								}	// if (recObjCostTmpl.getValues_int().size() > 0)
								/*else if( recObjCostTmpl.getValues_int().size() == 1 )
								{
									// Use first and only value
									nTranspTableIndex = 0;
									bFound = TRUE;
								}*/
							}
							else
							{
								if (recObjCostTmpl.getSpcID() == recSetSpc.getSpcID())
								{
									// We found a match for the distance in costtable for specie
									// No, we'll get the value for specie; 080306 p�d
									if (bFound)
									{
										// Make sure there's any data for "Transportkostnad"; 090428 p�d
										if (recObjCostTmpl.getValues_int().size() > 0)
										{
											// Make sure nTranspTableIndex is less than recObjCostTmpl.getValues_int().size(); 080312 p�d
											if (nTranspTableIndex < recObjCostTmpl.getValues_int().size())
											{
												nTranspTableValue = recObjCostTmpl.getValues_int()[nTranspTableIndex];
											}
											else
											{
												nTranspTableValue = recObjCostTmpl.getValues_int()[recObjCostTmpl.getValues_int().size()-1];
											}
										} // if (recObjCostTmpl.getValues_int().size() > 0)
									}
									// We didn't find a match for the distance in costtable for specie (i.e. value is less than min value)
									else
									{
										nTranspTableValue = 0;
									}

									fTranspCostSpc = nTranspTableValue;
									fCost_transp += fTranspCostSpc*mapVolPerSpc[recSetSpc.getSpcID()];
/*
									S.Format(_T("Spc %d\nfTranspCostSpc %f\nmapVolPerSpc[recSetSpc.getSpcID()] %f"),recSetSpc.getSpcID(),fTranspCostSpc,mapVolPerSpc[recSetSpc.getSpcID()]);
									//UMMessageBox(S);
*/
								}	// if (recObjCostTmpl.getSpcID() == recSetSpc.getSpcID())
							}	// else
						}	// for (UINT ui2 = 0;ui2 < vecTranspTable.size();ui2++)
					}	// if (vecTranspTable.size() > 0)
				}	// if (recSetSpc.getTranspDist1() > 0)
				//----------------------------------------------------------------------------
				// END: "Transportkostnad framk�rning till v�g, per Tr�dslag"; 080306 p�d
				//----------------------------------------------------------------------------
				//----------------------------------------------------------------------------
				// START: "Transportkostnad till Industri, per Tr�dslag"; 080314 p�d
				//----------------------------------------------------------------------------
				double fCalcDistance = recSetSpc.getTranspDist2();
				double fTemplDistance = 0.0;
				double fSpcM3Pulp = 0.0;
				double fCostTransp = 0.0;
				if (vecTransport2Table.size() > 0)
				{
					for (UINT ui2 = 0;ui2 < vecTransport2Table.size();ui2++)
					{
						CTransaction_costtempl_transport recTransp2 = vecTransport2Table[ui2];
						fValue = 0.0;
						// Volume pulpwood for specie; 081217 p�d
						fSpcM3Pulp = mapVolPulpwoodPerSpc[recSetSpc.getSpcID()];

						// Match Specie; 081218 p�d
						if (recTransp2.getSpcID() == recSetSpc.getSpcID())
						{
							// Calculate cost per km for transport; 081217 p�d
							fCostTransp = fCalcDistance * recTransp2.getCost();
							// Calculate Max transport reduction per specie.
							fMaxTransp2CostSpc = recTransp2.getMaxCost();

							// Check if cost for transport is less than max cost,
							// if not set cost to max cost; 081217 p�d
							if (fCostTransp > fMaxTransp2CostSpc)
								fCostTransp = fMaxTransp2CostSpc;
							// Calculate actual cost for transport to industry; 081217 p�d
							fValue = fCostTransp*fSpcM3Pulp;
/*
							S.Format(_T("2 fCostTransp %f\nfMaxTransp2CostSpc %f\nfCalcDistance %f\nrecTransp2.getCost() %f\nfValue %f"),
								fCostTransp,fMaxTransp2CostSpc,fCalcDistance,recTransp2.getCost(),fValue);
							//UMMessageBox(S);
*/
						}	// if (recTransp2.getSpcID() == recSetSpc.getSpcID())

						// Sum. cost for "Transport till Industri"; 080314 p�d
						if (fSpcM3Pulp > 0.0 && fValue > 0.0)
							fTransp2CostSpcPerM3 += fValue/fSpcM3Pulp;
						fTransp2CostSpc += fValue;
						fCost_transp2 += fValue;

					}	// for (UINT ui2 = 0;ui2 < vecTransport2Table.size();ui2++)
				}	// if (vecTransport2Table.size() > 0)
				//----------------------------------------------------------------------------
				// END: "Transportkostnad till Industri, per Tr�dslag"; 080314 p�d
				//----------------------------------------------------------------------------
				//----------------------------------------------------------------------------
				// START: "Huggningskostnad per Tr�dslag"; 080306 p�d
				//----------------------------------------------------------------------------
				if (nSetAs == 1)	// "Anv�nd kostnader per kubikmeter kr/m3"
				{
					// Calculate costs for cutting; 080314 p�d
					fCut1_cost = recCutting.getCut1() * fGangnVolM3;
					fCut2_cost = recCutting.getCut2() * fGangnVolM3;
					fCut3_cost = recCutting.getCut3() * fGangnVolM3;
					// "�vriga kostnader kr/m3, kommenterad bort 2009-09-04 p�d
					// "Anv�nds inte (P.L.)"
					fCut4_cost = 0.0; //recCutting.getCut4() * fGangnVolM3;
				}
				else if (nSetAs == 2)	// "Anv�nd tabell DGV"
				{
					fCut2_cost = 0.0;
					fCut3_cost = 0.0;
					fCut4_cost = 0.0;
					fDGV = 0.0;	// No value yet; 080312 p�d
					nDGV_table_prev = 0;

					// Check if there's any trakt data, i.e. DGV; 080312 p�d
					if (vecTraktData.size() > 0)
					{
						// Try to find DGV value for specie; 080312 p�d
						for (UINT vi1 = 0;vi1 < vecTraktData.size();vi1++)
						{
							recTraktData = vecTraktData[vi1];

							if (recTraktData.getSpecieID() == recSetSpc.getSpcID())
							{
								fDGV = recTraktData.getDGV();
								break;
							}	// if (recTraktData.getSpcID() == recSetSpc.getSpcID())
						}	// for (UINT vi1 = 0;vi1 < vecTraktData.size();vi1++)
					}	// if (vecTraktData.size() > 0)
					
					bFound = FALSE;	// Not found yet; 080312 p�d
					// Try to find the cutting-cost for this specie; 080312 p�d
					if (vecCuttingDGVTable.size() > 0)
					{

						for (UINT ui2 = 0;ui2 < vecCuttingDGVTable.size();ui2++)
						{
							recObjCostTmpl = vecCuttingDGVTable[ui2];
							if (ui2 == 0)	// First row hold DGV; 080312 p�d
							{
								if( recObjCostTmpl.getValues_int().size() > 1 )
								{
									for( INT vi1 = recObjCostTmpl.getValues_int().size() - 1; vi1 >= 0; vi1-- )
									{
										// Try to match the DGV set for this specie to a
										// DGV, set in costtable; 080306 p�d
										nValue=recObjCostTmpl.getValues_int()[vi1];

										// #HMS-86 20220504 J� Lagt in en koll om det bara finns en kolumn och ett v�rde kontrollera om dgv �r st�rre
										// eller lika med detta v�rde annars inget resultat
										if(vi1==0 && fDGV >= nValue)
										{
											nCuttingTableIndex = vi1;
											bFound = TRUE;
											break;										
										}

										// #HMS-86 20220504 J� lagt in en koll s� att det m�ste finnas ett v�rde att kontrollera mot
										// Detta f�r att undvika att s�kningen plockar nollade kolumner som kan ligga i slutet av dgv tabellen
										if( fDGV >= nValue && nValue>0)
										{
											nCuttingTableIndex = vi1;
											bFound = TRUE;
											break;
										}
										else
										{
											// #HMS-86 20220504 J�
											// Lagt in en koll om man st�r p� f�rsta kolumnen och den har v�rdet 0 s� anv�nd den
											if(vi1==0 && nValue==0)
											{
												nCuttingTableIndex = vi1;
												bFound = TRUE;
												break;
											}
										}
									}
								}
								else if( recObjCostTmpl.getValues_int().size() == 1 )
								{
									// Use first and only value
									//nCuttingTableIndex = 0;
									//bFound = TRUE;

									// #HMS-86 20220504 J� Lagt in en koll om det bara finns en kolumn och ett v�rde kontrollera om dgv �r st�rre
									// eller lika med detta v�rde annars inget resultat
									nValue=recObjCostTmpl.getValues_int()[0];
									if(fDGV >= nValue)
									{
										nCuttingTableIndex = 0;
										bFound = TRUE;
									}
									else
										bFound = FALSE;
								}
							}
							else
							{
								if (recObjCostTmpl.getSpcID() == recSetSpc.getSpcID())
								{

									// We found a match for the DGV in costtable for specie
									// No, we'll get the value for specie; 080306 p�d
									if (bFound)
									{
										// Make sure nCuttingTableIndex is less than recObjCostTmpl.getValues_int().size(); 080312 p�d
										if (nCuttingTableIndex < recObjCostTmpl.getValues_int().size())
										{
											nCuttingTableValue = recObjCostTmpl.getValues_int()[nCuttingTableIndex];
										}
										else
										{
											if (recObjCostTmpl.getValues_int().size() > 0)
												nCuttingTableValue = recObjCostTmpl.getValues_int()[recObjCostTmpl.getValues_int().size()-1];
											else
												nCuttingTableValue = 0;
										}
									}
									// We didn't find a match for the DGV in costtable for specie (i.e. value is less than min value)
									else
									{
										nCuttingTableValue = 0;
									}
									/*
									S.Format(_T("Spc %d\nDGV %f\nnCuttingTableValue %d\nnCuttingTableIndex %d\nrecObjCostTmpl.getValues_int().size() %d"),
											recObjCostTmpl.getSpcID(),fDGV,nCuttingTableValue,nCuttingTableIndex,recObjCostTmpl.getValues_int().size());
									//UMMessageBox(S);
									*/
									fCuttingCostSpc += nCuttingTableValue;
									fCut1_cost += fCuttingCostSpc*mapVolPerSpc[recSetSpc.getSpcID()];
								}	// if (recObjCostTmpl.getSpcID() == recSetSpc.getSpcID())
							}	// else
						}	// for (UINT ui2 = 0;ui2 < vecTranspTable.size();ui2++)
					}	// if (vecCuttingTable.size() > 0)
				}	// if (nSetAs == 2)
				else if (nSetAs == 3)	// "Anv�nd tabell M3FUB"; 091109 p�d
				{
					fCut2_cost = 0.0;
					fCut3_cost = 0.0;
					fCut4_cost = 0.0;
					fM3FUB_avg = 0.0;	// No value yet; 091109 p�d
					fM3FUB_table_prev = 0.0;	// No value yet; 091109 p�d

					// Calculation will be done on stand average below, not per specie
				}
				//----------------------------------------------------------------------------
				// END: "Huggningskostnad per Tr�dslag"; 080306 p�d
				//----------------------------------------------------------------------------
				// Get name of Specie, based on recSetSpc.getSpcID(); 080317 p�d
				if (vecSpecies.size() > 0)
				{
					sSpecieName.Empty();
					for (UINT ui3 = 0;ui3 < vecSpecies.size();ui3++)
					{
						if (vecSpecies[ui3].getSpcID() == recSetSpc.getSpcID())
						{
							sSpecieName = vecSpecies[ui3].getSpcName();
							break;
						}	// if (vecSpecies[ui3].getSpcID() == recSetSpc.getSpcID())
					}	// for (UINT ui3 = 0;ui3 < vecSpecies.size();ui3++)
				}	// if (vecSpecies.size() > 0)

				//-------------------------------------------------------------------------------------------
				// Add transport and cutting costs
				vecRotpostSpc.push_back(CTransaction_trakt_rotpost_spc(recSetSpc.getSpcID(),
																													trakt_id,
																													sSpecieName,
																													fTranspCostSpc,																			// "Kostnad i kr/m3"
																													fTranspCostSpc*mapVolPerSpc[recSetSpc.getSpcID()],	// "SUM Kostnad kr"
																													fCuttingCostSpc,																		// "Kostnad i kr/m3"
																													fCuttingCostSpc*mapVolPerSpc[recSetSpc.getSpcID()],	// "SUM Kostnad kr"
																													fTransp2CostSpcPerM3,
																													fTransp2CostSpc,
																													nSetAs,	// "Intr.kostnadsmall = 2"; 080318 p�d
																													_T("")));
/*
				S.Format("calcRotpostForTrakt\nSpc %d\ntrakt_id %d\nSpcName %s\nfTranspCostSpc %f\nmapVolPerSpc[recSetSpc.getSpcID()] %f\nfCuttingCostSpc %f\nmapVolPerSpc[recSetSpc.getSpcID()] %f",
					recSetSpc.getSpcID(),trakt_id,sSpecieName,fTranspCostSpc,mapVolPerSpc[recSetSpc.getSpcID()],fCuttingCostSpc,mapVolPerSpc[recSetSpc.getSpcID()]);
				//UMMessageBox(S);
*/
			}	// for (UINT ui1 = 0;ui1 < vecSetSpc.size();ui1++)

			// Commit transport and cutting costs to db
			pDB->addRotpostSpc(vecRotpostSpc);

		}	// if (vecSetSpc.size() > 0)
		//----------------------------------------------------------------------------
		// END: "Huggningskostnad per Tr�dslag"; 080312 p�d
		//----------------------------------------------------------------------------

		//----------------------------------------------------------------------------
		// START: "Huggningskostnad per Best�nd" ("Tabell M3FUB" only); 150417 Peter
		//----------------------------------------------------------------------------
		if (nSetAs == 3)
		{
			// Find fM3FUB_avg
			fVol = 0.0; nTrees = 0;
			for (int i=0; i<vecTraktData.size(); i++)
			{
				fVol += vecTraktData[i].getM3FUB();
				nTrees += vecTraktData[i].getNumOf();
			}
			fM3FUB_avg = fVol / nTrees;
			
			bFound = FALSE;	// Not found yet; 080312 p�d
			// Try to find the cutting-cost for this specie; 080312 p�d
			if (vecCuttingM3FUBTable.size() > 0)
			{

				for (UINT ui2 = 0;ui2 < vecCuttingM3FUBTable.size();ui2++)
				{
					recObjCostTmpl = vecCuttingM3FUBTable[ui2];
					// Find column i.e. m3fub break-value; 091125 p�d
					if (ui2 == 0)	// First row hold m3fub; 0 p�d
					{
						if( recObjCostTmpl.getValues_float().size() > 1 )
						{
							for( INT vi1 = recObjCostTmpl.getValues_float().size() - 1; vi1 >= 0; vi1-- )
							{
								// Try to match the M3Fub set for this specie to a
								// M3FUB, set in costtable; 091109 p�d
								if( fM3FUB_avg >= recObjCostTmpl.getValues_float()[vi1] )
								{
									nCuttingTableIndex = vi1;
									bFound = TRUE;
									break;
								}
							}
						}
						else if( recObjCostTmpl.getValues_float().size() == 1 )
						{
							// Use first and only value
							nCuttingTableIndex = 0;
							bFound = TRUE;
						}
					}
					// Find price; 091125 p�d
					else if (ui2 == 1)
					{
						// We found a match for the M3FUB in costtable for specie
						// No, we'll get the value for specie; 091109 p�d
						if (bFound)
						{
							// Make sure nCuttingTableIndex is less than recObjCostTmpl.getValues_int().size(); 091109 p�d
							if (nCuttingTableIndex < recObjCostTmpl.getValues_float().size())
							{
								fCuttingTableValue = recObjCostTmpl.getValues_float()[nCuttingTableIndex];
							}
							else
							{
								if (recObjCostTmpl.getValues_float().size() > 0)
									fCuttingTableValue = recObjCostTmpl.getValues_float()[recObjCostTmpl.getValues_float().size()-1];
								else
									fCuttingTableValue = 0.0;
							}
						}
						// We didn't find a match for the M3FUB_avg in costtable (i.e. value is less than min value)
						else
						{
							fCuttingTableValue = 0;
						}
						fCut1_cost = fCuttingTableValue*fVol;
					}
				}
			}
		}
		//----------------------------------------------------------------------------
		// END: "Huggningskostnad per Best�nd" ("Tabell M3FUB" only); 150417 Peter
		//----------------------------------------------------------------------------

		//----------------------------------------------------------------------------
		// START: "�vrig intr�ngsers�ttning"; 080312 p�d
		//----------------------------------------------------------------------------
		// Remove ALL Other costs in table for TraktID; 080312 p�d
		pDB->delRotpostOtc(trakt_id);

		nOtherCostCnt = 1;
		fCut5_cost = 0.0;
		fCut6_cost = 0.0;
		fValue = 0.0;
		CString S;
		//----------------------------------------------------------------------------
		// END: "�vrig intr�ngsers�ttning"; 080312 p�d
		//----------------------------------------------------------------------------
	
		//----------------------------------------------------------------------------
		// START: "�vriga fasta kostnader"; 080312 p�d
		//----------------------------------------------------------------------------
		if (vecOtherCost.size() > 0)
		{
			for (UINT ii1 = 0;ii1 < vecOtherCost.size();ii1++)
			{
				recOtherCost = vecOtherCost[ii1];
				fPercent = recOtherCost.getIsVAT();
				if (fPercent > 0.0)
					fValue = recOtherCost.getPrice() + recOtherCost.getPrice() * fPercent/100.0;
				else
					fValue = recOtherCost.getPrice();
		
				fCut6_cost += fValue;
				// Only add to DB, if there's something to add; 080312 p�d
				if (fCut6_cost > 0.0)
				{
					vecRotpostOtc.push_back(CTransaction_trakt_rotpost_other(nOtherCostCnt,
																			 trakt_id,
																			 recOtherCost.getPrice(),
																			 fValue,
																			 recOtherCost.getNotes(),
																			 fPercent,
																			 OTHER_COST_2,
																			 _T("")));
					nOtherCostCnt++;
				}	// if (fCut3_cost > 0.0)

			}	// for (UINT ii1 = 0;ii1 < vecOtherCost.size();ii++)

			pDB->addRotpostOtc(vecRotpostOtc);

		}	// if (vecOtherCost.size() > 0)
		//----------------------------------------------------------------------------
		// END: "�vrigs fasta kostnader"; 080312 p�d
		//----------------------------------------------------------------------------
		
		if (nSetAs == 1)
		{
			// Caluclate "ROTNETTO"; 080312 P�D
			fCost_total = fSumPrice - (fCut1_cost + fCut2_cost + fCut3_cost + fCut4_cost + fCut6_cost + fCost_transp + fCost_transp2);

		//		S.Format(_T("SetAs 1\nfGangnVolM3 %f\nfSumPrice %f\nfCut1_cost %f\nfCut2_cost %f\nfCut3_cost %f\nfCut4_cost %f\nfCost_transp %f\nfCost_transp2 %f\nfCost_total %f\nfPulpwoodVolM3 %f"),
		//			fGangnVolM3,fSumPrice,fCut1_cost,fCut2_cost,fCut3_cost,fCut4_cost,fCut6_cost,fCost_transp,fCost_transp2,fCost_total,fPulpwoodVolM3);
		}
		else if (nSetAs == 2 || nSetAs == 3)
		{
			// Caluclate "ROTNETTO"; 080312 P�D
			fCost_total = fSumPrice - (fCut1_cost + fCost_transp + fCost_transp2 + fCut5_cost + fCut6_cost);
	//			S.Format(_T("SetAs 2,3\nfGangnVolM3 %f\nfSumPrice %f\nfCut1_cost %f\nfCut2_transp %f\nfCut3_transp2 %f\nfCut5_cost %f\nfCost6_cost %f\nfCost_total %f\nfPulpwoodVolM3 %f"),
	//				fGangnVolM3,fSumPrice,fCut1_cost,fCost_transp,fCost_transp2,fCut5_cost,fCut6_cost,fCost_total,fPulpwoodVolM3);
		}

//		UMMessageBox(S);
		// Just set to 2, only that we present result, same in both settings.
		// This applies to "Resultat" in UMEstimate; 091109 p�d
		if (nSetAs == 3) nSetAs = 2;
//			//UMMessageBox(S);
		// Save rotpost data to Database; 080312 p�d
		CTransaction_trakt_rotpost recRotpost = CTransaction_trakt_rotpost(trakt_id,
																																			 fGangnVolM3,
																																			 fSumPrice,
																																			 fCut1_cost,		// "Summa Huggningskostnad"		
																																			 fCut2_cost,		// "Sk�rdare, medelpriser (kr/m3) i kostnadsmall"
																																			 fCut3_cost,		// "Skotare, medelpriser (kr/m3) i kostnadsmall"
																																			 fCut4_cost,		// "�vriga kostnader, medelpriser (kr/m3) i kostnadsmall OBS! Anv�nds inte"
																																			 fCost_transp,	// "Summa Transportkostnad till v�g"
																																			 fCut5_cost,		// "Summa �vrig intr�ngsers�ttning"
																																			 fCut6_cost,		// "Summa �vriga fasta kostnader"
																																			 fCost_transp2,	// "Summa transportkostnad till Industri"
																																			 fPulpwoodVolM3,// "Summa massaved"
																																			 fCost_total,		// "ROTNETTO"
																																			 nSetAs,				// "Type of Calculation"
																																			 _T(""));
		// Get data in table "esti_trakt_spc_assort_table" for Trakt; 070705 p�d
		if (!pDB->addRotpost(recRotpost))
		{
			pDB->updRotpost(recRotpost);
		}
	}

	if (pDB != NULL)
		delete pDB;
}


void calcTransfersByPercent(int spc_id,
													  LPCTSTR fromAssort,
													  LPCTSTR toAssort,
													  double percent,
													  CTransaction_trakt_data recTraktData,
													  vecTransactionTraktAss &vecTraktAss,
														short only_for_trans,
														bool first_time,
														double *vol_trans)
{
	double fFromM3Fub = 0.0;			// M3Fub Volume in "From" Assortment, before transfer; 070613 p�d
	double fFromM3To = 0.0;				// M3To Volume in "From" Assortment, before transfer; 070613 p�d
	double fFromM3FubPrice = 0.0;	// Price in m3fub for "From" assortment; 070613 p�d
	double fFromM3ToPrice = 0.0;	// Price in m3to for "From" assortment; 070613 p�d
	double fFromM3FubLeft = 0.0;	// Volume left in "From" Assortment, after transfer; 070613 p�d
	
	double fToM3Fub = 0.0;				// Volume in "To" Assortment, before transfer; 070613 p�d
	double fToM3To = 0.0;					// Volume in "To" Assortment, before transfer; 070613 p�d
	double fToM3FubPrice = 0.0;		// Price in m3fub for "To" assortment; 070613 p�d
	double fToM3ToPrice = 0.0;		// Price in m3to for "To" assortment; 070613 p�d
	double fToM3FubAdded = 0.0;		// Volume left in "To" Assortment, after transfer; 070613 p�d
	double fToM3ToAdded = 0.0;		// Volume left in "To" Assortment, after transfer; 070613 p�d

	double fCalcFactor = 0.0;			// "Omf�ringstal" between m3fub and m3to; 070613 p�d

	double fAddedKrPerM3 = 0.0;

	double fTranferPercent = 0.0;
	double fOldValue=0.0;
	CString sFromAssort;
	CString sToAssort;
	CString S;

	// Get data from active record in m_wndReport1 (Transfer data); 070613 p�d
	sFromAssort = fromAssort;
	sToAssort = toAssort;
	fTranferPercent = percent;

	// Find assortment in m_vecTraktAss and matches m_recTraktData; 070613 p�d
	for (UINT j = 0;j < vecTraktAss.size();j++)
	{
		CTransaction_trakt_ass traktAssData = vecTraktAss[j];
		fAddedKrPerM3 = traktAssData.getKrPerM3();

		if (recTraktData.getTDataID() == traktAssData.getTAssTraktDataID() && 
				recTraktData.getTDataID() == spc_id &&		
				recTraktData.getTDataTraktID() == traktAssData.getTAssTraktID())
		{
	
			// Try to find data for "From" assortment; 070613 p�d
			if (traktAssData.getTAssName().Compare(sFromAssort) == 0)
			{
				*vol_trans =0.0;	//Nollar returnerade v�rdet f�r �verf�rd volym  HMS-47 J� 20190704
				// Get Volume in "From" assortment; 070613 p�d
				fFromM3Fub = traktAssData.getM3FUB();
				fFromM3To = traktAssData.getM3TO();
				fFromM3FubPrice = traktAssData.getPriceM3FUB();
				fFromM3ToPrice = traktAssData.getPriceM3TO();

				// Calculate the "Omf�ringstal" between M3Fub and M3To; 070613 p�d
				if (fFromM3Fub > 0.0 && fFromM3To > 0.0)
				{
					fCalcFactor = fFromM3To / fFromM3Fub;
				}

				if (fFromM3Fub > 0.0 )	//G�r ingen �verf�ring om det inte finns n�gon fub att �verf�ra fr�n HMS-47 J� 20190704
				{
					// Calculate volume left in "From" assortment; 070613 p�d
					fFromM3FubLeft = fFromM3Fub - fFromM3Fub * (fTranferPercent/100.0);
					/*
					S.Format(L"calcTransfersByPercent FROM\nFub %f\nTo %f\nFubPrice %f\nToPrice %f\nCalcFactor %f\nTransPerc %f\nFromM3FubLeft %f",
					fFromM3Fub,fFromM3To,fFromM3FubPrice,fFromM3ToPrice,fCalcFactor,fTranferPercent,fFromM3FubLeft);
					UMMessageBox(S);
					*/
					// Update M3Fub value after transfer; 070613 p�d
					vecTraktAss[j].setM3FUB(fFromM3FubLeft);
					// Only calculate for m3to if we have m3to volume; 070907 p�d
					vecTraktAss[j].setM3TO(fFromM3FubLeft*fCalcFactor);

					//�ndrat h�r tilla tt bara kolla p� om det finns volym som skall �verf�ras, eftersomd et skall vara m�jligt trots att priset �r 0'
					//20111121 Bug #2581

					//if (fFromM3Fub > 0.0 && fFromM3FubPrice > 0.0)

					// Update Price for FUB and TO; 070613 p�d
					if(fFromM3FubPrice > 0.0)
					{
						vecTraktAss[j].setValueM3FUB(fFromM3FubLeft*fFromM3FubPrice + (fFromM3FubLeft*fAddedKrPerM3));
						*vol_trans = fFromM3Fub * fTranferPercent/100.0;
					}
					else
					{
						//#2581 20120907 J� 
						//S�tter �verf�rd volym �ven om det inte finns n�got pris satt f�r fub
						*vol_trans = fFromM3Fub * fTranferPercent/100.0;
					}
					// Only calculate for m3to if we have m3to volume; 070907 p�d
					//�ndrat h�r tilla tt bara kolla p� om det finns volym som skall �verf�ras, eftersomd et skall vara m�jligt trots att priset �r 0'
					//20111121 Bug #2581
					if (fFromM3To > 0.0 )
						//if (fFromM3To > 0.0 && fFromM3ToPrice > 0.0)
					{
						if (fFromM3ToPrice > 0.0)
						{
							vecTraktAss[j].setValueM3TO(fFromM3FubLeft*fCalcFactor*fFromM3ToPrice + (fFromM3FubLeft*fCalcFactor*fAddedKrPerM3));		
							*vol_trans = fFromM3To * fTranferPercent/100.0;
						}
						else
						{
							//#2581 20120907 J� 
							//S�tter ingen �verf�rd volym p� to om det inte finns n�got pris, d� kommer �verf�rd volym visas som fub ist�llet
							//	*vol_trans = fFromM3Fub * fTranferPercent/100.0;
						}

					}
				}
				break;
			}
		}
	}

	// Find assortment in m_vecTraktAss and matches m_recTraktData; 070613 p�d
	for (UINT j = 0;j < vecTraktAss.size();j++)
	{
		CTransaction_trakt_ass traktData = vecTraktAss[j];
		fAddedKrPerM3 = traktData.getKrPerM3();
		
		if (recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
				recTraktData.getTDataID() == spc_id &&		
				recTraktData.getTDataTraktID() == traktData.getTAssTraktID())
		{
			// Try to find data for "To" assortment; 070613 p�d
			if (traktData.getTAssName().Compare(sToAssort) == 0)
			{
				if (fFromM3Fub > 0.0 )	//G�r ingen �verf�ring om det inte finns n�gon fub att �verf�ra fr�n HMS-47 20190704
				{
					// Get Volume in "To" assortment; 070613 p�d
					fToM3Fub = vecTraktAss[j].getM3FUB();
					fToM3To = vecTraktAss[j].getM3TO();
					fToM3FubPrice = vecTraktAss[j].getPriceM3FUB();
					fToM3ToPrice = vecTraktAss[j].getPriceM3TO();
					// Get Volume in "To" assortment; 070613 p�d
					if (only_for_trans == 0)
					{
						fToM3Fub = vecTraktAss[j].getM3FUB();
						fToM3To = vecTraktAss[j].getM3TO();
						// Calculate volume that'll be added to "To" assortment; 070613 p�d
						fToM3FubAdded = fToM3Fub + (fFromM3Fub - fFromM3FubLeft);
						fToM3ToAdded = fToM3FubAdded*fCalcFactor;
						// Update Price for FUB and TO; 070613 p�d
						vecTraktAss[j].setM3FUB(fToM3FubAdded);
						// Update M3To value after transfer; 070613 p�d
						vecTraktAss[j].setM3TO(fToM3ToAdded);
					}
					else
					{
						fToM3Fub = 0.0;
						fToM3To = 0.0;
						// Calculate volume that'll be added to "To" assortment; 070613 p�d
						fToM3FubAdded = (fFromM3Fub - fFromM3FubLeft);
						fToM3ToAdded = fToM3FubAdded*fCalcFactor;
						if (first_time)
						{
							vecTraktAss[j].setM3FUB(0.0);
							// Update M3To value after transfer; 070613 p�d
							vecTraktAss[j].setM3TO(0.0);
						}
						// Update Price for FUB and TO; 070613 p�d
						vecTraktAss[j].setM3FUB(fToM3FubAdded+vecTraktAss[j].getM3FUB());

						// Update M3To value after transfer; 070613 p�d
						vecTraktAss[j].setM3TO(fToM3ToAdded+vecTraktAss[j].getM3TO());
					}

					// Only calculate for m3fub if we have m3to volume; 070907 p�d
					if (fToM3FubAdded > 0.0 && fToM3FubPrice > 0.0)
					{
						// Update M3Fub value after transfer; 070613 p�d
						//HMS-133 20241210 J� Plussa p� det gamla v�rdet med det nya v�rdet ist�llet f�r att bara spara det nya
						//vecTraktAss[j].setValueM3FUB(fToM3FubAdded*fToM3FubPrice + (fToM3FubAdded*fAddedKrPerM3));
						fOldValue=vecTraktAss[j].getValueM3FUB();
						vecTraktAss[j].setValueM3FUB(fOldValue+fToM3FubAdded*fToM3FubPrice + (fToM3FubAdded*fAddedKrPerM3));
					}
					// Only calculate for m3to if we have m3to volume; 070907 p�d
					if (fToM3ToAdded > 0.0 && fToM3ToPrice > 0.0)
					{

						fOldValue=vecTraktAss[j].getValueM3TO();
						// Update M3To value after transfer; 070613 p�d
						//HMS-133 20241210 J� Plussa p� det gamla v�rdet med det nya v�rdet ist�llet f�r att bara spara det nya
						//vecTraktAss[j].setValueM3TO(fToM3ToAdded*fToM3ToPrice + (fToM3ToAdded*fAddedKrPerM3));
						vecTraktAss[j].setValueM3TO(fOldValue+fToM3ToAdded*fToM3ToPrice + (fToM3ToAdded*fAddedKrPerM3));
					}
				}
				break;
			}	// if (transData.getToName().Compare(traktData.getTAssName()))
		}	// if (m_recTraktData.getTDataID() == traktData.getTAssTraktDataID() && 
	}	// for (UINT j = 0;j < m_vecTraktAss.size();j++)
}


//------------------------------------------------------------------------
// Retur 1 allt ok eller s� finns inga tr�dslag med n�gra funktioner angivna
// Return -1, tr�dslag med s�derbergs barkfunk finns samt best.�lder>500
// Return -2, tr�dslag med s�derbergs h�jdfunk finns samt best.�lder>500
//------------------------------------------------------------------------
int doSoderbergHgtandVolCheckonTrakt(DB_CONNECTION_DATA m_dbConnectionData,CTransaction_trakt recTrakt,CStringArray &logBark,CStringArray &logHgt)
{
	vecFuncSpc vecFBarkSpc;
	vecFuncSpc vecFHgtSpc;
	vecTransactionTraktSetSpc vecSetSpc;
	CTransaction_trakt_set_spc recSetSpc;
	CTransaction_dcls_tree recTree;
		vecTransactionDCLSTree vecTraktDCLSTrees;	
	vecTransactionDCLSTree vecTraktDCLS1Trees;
	vecTransactionDCLSTree vecTraktDCLS2Trees;		
	int nRet=1,nHgtId=0,nBarkFuncId=0;
	BOOL bSpecieFound=FALSE;
	CDBHandleCalc *pDB = NULL;
	CString sTrakt;
	
	pDB = new CDBHandleCalc(m_dbConnectionData);
	pDB->getDCLSAllTrees(recTrakt.getTraktID(), vecTraktDCLSTrees, vecTraktDCLS1Trees, vecTraktDCLS2Trees); // 'Uttag', 'Kvarl�mnat', 'Uttag i stickv�g'
	pDB->getTraktSetSpc(vecSetSpc,recTrakt.getTraktID());

		if (vecSetSpc.size() == 0)
		{
		return 1;
		}
//-----------------------------------------------------------------------------------------
	// Check if bark or height function soderbergs is used and age > 500  #4603 20151021 J�
	if(recTrakt.getTraktAge()>500)
	{
		vecFBarkSpc.clear();
		vecFHgtSpc.clear();
		// Check if specie in vecTraktSampleTrees also have functions connected to
		// it. I.e. Volume-,Bark- and Heightfunction; 070626 p�d


		for (UINT i = 0;i < vecSetSpc.size();i++)	//Loop items/species in transactiontraktsetspc
		{
			recSetSpc = vecSetSpc[i];
			bSpecieFound=FALSE;
			nHgtId=recSetSpc.getHgtFuncID();
			nBarkFuncId=recSetSpc.getBarkFuncID();
			if(nBarkFuncId== ID_BARK_SO || nHgtId== ID_HGT_SODERBERGS)		//Check if bark func s�dra or height func s�dra for that specie
			{
				if (vecTraktDCLSTrees.size() > 0 && bSpecieFound==FALSE)	//If trees
				{
					for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)	//Find spec in dcls table
					{
						recTree = vecTraktDCLSTrees[ii];
						if(recSetSpc.getSpcID()==recTree.getSpcID())		//Found spec
						{
							bSpecieFound=TRUE;
							break;
						}
					}
				}
				if (vecTraktDCLS1Trees.size() > 0 && bSpecieFound==FALSE)	//If trees
				{
					for (UINT ii = 0;ii < vecTraktDCLS1Trees.size();ii++)	//Find spec in dcls table
					{
						recTree = vecTraktDCLS1Trees[ii];
						if(recSetSpc.getSpcID()==recTree.getSpcID())		//Found spec
						{
							bSpecieFound=TRUE;
							break;
						}
					}
				}
				if (vecTraktDCLS2Trees.size() > 0 && bSpecieFound==FALSE)	//If trees
				{
					for (UINT ii = 0;ii < vecTraktDCLS2Trees.size();ii++)	//Find spec in dcls table
					{
						recTree = vecTraktDCLS2Trees[ii];
						if(recSetSpc.getSpcID()==recTree.getSpcID())		//Found spec
						{
							bSpecieFound=TRUE;
							break;
						}
					}
				}

				if(bSpecieFound==TRUE)
				{
					if(nBarkFuncId== ID_BARK_SO)
					{
						nRet=-1;
						vecFBarkSpc.push_back(_func_spc_struct(recSetSpc.getSpcID(),recTree.getSpcName()));		
					}
					if(nHgtId== ID_HGT_SODERBERGS)
					{
						nRet=-2;
						vecFHgtSpc.push_back(_func_spc_struct(recSetSpc.getSpcID(),recTree.getSpcName()));		
					}
				}
			}
		}	

		if (vecFBarkSpc.size() > 0)
		{
			//------------------------------------------------------
			/*sTrakt.Format(_T("%s - %s"),
				(recTrakt.getTraktNum()),
				(recTrakt.getTraktName()));
			if (recTrakt.getTraktName().IsEmpty())
			{
				sTrakt.Format(_T("%s"),(recTrakt.getTraktNum()));
			}
			logBark.Add((sTrakt));			
			logBark.Add(_T(""));			*/
			for (UINT iii = 0;iii < vecFBarkSpc.size();iii++)
			{
				CString sSpc;
				sSpc.Format(_T("- %s"),vecFBarkSpc[iii].sSpcName);
				logBark.Add(sSpc);
			}
			//logBark.Add(_T("------------------------------------------------------------------------"));
		}	
		if (vecFHgtSpc.size() > 0)
		{
			//------------------------------------------------------
			/*sTrakt.Format(_T("%s - %s"),
				(recTrakt.getTraktNum()),
				(recTrakt.getTraktName()));
			if (recTrakt.getTraktName().IsEmpty())
			{
				sTrakt.Format(_T("%s"),(recTrakt.getTraktNum()));
			}
			logHgt.Add((sTrakt));			
			logHgt.Add(_T(""));			*/
			for (UINT iii = 0;iii < vecFHgtSpc.size();iii++)
			{
				CString sSpc;
				sSpc.Format(_T("- %s"),vecFHgtSpc[iii].sSpcName);
				logHgt.Add(sSpc);
			}
			//logHgt.Add(_T("------------------------------------------------------------------------"));
		}	

	}

	if(pDB) delete pDB;
	//-----------------------------------------------------------------------------------------	
	return nRet;
}

int doCruisingCalculations(int spc_id,
							BOOL remove_trakt_trans,
							CStringArray &log,
							BOOL bConnected,
							DB_CONNECTION_DATA m_dbConnectionData,
							CTransaction_trakt &trakt,
							bool do_transfers,
							int object_id)
{
	CDBHandleCalc *pDB = NULL;
	CString S;
	CString sTrakt;
	BOOL bFuncCheckOK;
	BOOL bIsDCLSTrees = FALSE;
	BOOL bIsDCLS1Trees = FALSE;
	BOOL bIsDCLS2Trees = FALSE;
	BOOL bIsNonSampTrees = FALSE;
	long nNumOfAreal = 0;
	double fGYPerHa = 0.0;	// "Grundyta i m2/ha"
	double fDCLS_Set = 0.0;
	double fDCLS_From = 0.0;
	double fDCLS_To = 0.0;
	double fArealCalculationFactor = 1.0;
	double fArea = 0.0;
	double fPerHa = 0.0;
	std::map<int,double> mapSumM3Fub;
	BOOL bIsCircularTest = FALSE;
	int countDcls1 = 0, countDcls2 = 0;		// Optimeringsvariabler. Antal tr�d i kvarl�mnat resp. uttag i stickv�g.
	std::map<int, int> mapDcls0NumTrees;	// Antal tr�d + kanttr�d per tr�dslag (endast uttag). Optimering av Peter

	int nReturnValue = 1;	// All is OK

	vecTransactionPlot vecTraktPlot;
	vecTransactionSampleTree vecTraktAllSampleTrees;
	vecTransactionSampleTree vecTraktSampleTrees;
	vecTransactionSampleTree vecTraktNonSampleTrees;
	vecTransactionSampleTree vecTraktLeftOverSampleTrees;
	vecTransactionSampleTree vecTraktExtraSampleTrees;
	vecTransactionSampleTree vecTraktSumSampleTrees;	// Added 2009-11-24 p�d

	vecTransactionDCLSTree vecTraktDCLSTrees;	
	vecTransactionDCLSTree vecTraktDCLS1Trees;
	vecTransactionDCLSTree vecTraktDCLS2Trees;
	vecTransactionTraktData vecAllTraktData;	// 100120 p�d
	vecTransactionTraktData vecTraktData1;		// Uttag
	vecTransactionTraktData vecTraktData2;		// Kvarl�mmnat
	vecTransactionTraktData vecTraktData3;		// Uttag i stickv�g
	vecTransactionTraktData vecTraktData4;
	vecTransactionTraktSetSpc vecSetSpc;
	vecTransactionTraktTrans vecTraktTrans;		// Added 070925 p�d
	vecTransactionTreeAssort vecTreeAssort;		// esti_trakt_trees_assort_table
	vecTransactionTraktAss vecTraktAss;			// esti_trees_assort_table
	vecTransactionSampleTree vecTraktCalculateSampleTrees;
	vecTransactionDCLSTree vecDCLS;
	vecFuncSpc vecFSpc;
	vecTransactionAssort vecAssortmentsPrl;	// XML pricelist vector

	short nOnlyForTransfers = 0;	// 1 = Only for transfers

	BOOL bSpecieFound = FALSE;
	BOOL bSpecieFound2 = FALSE;
	CTransaction_trakt_set_spc recSetSpc;
	CTransaction_dcls_tree recTree;
	CTransaction_trakt_misc_data recTraktMiscData;

	CString sSpcMsg;

	CTransaction_trakt recTrakt = trakt;

	int nPriceSetIn = 2; // Is price in pricelist set in m3to or m3fub (def. m3to); 090615 p�d

	pDB = new CDBHandleCalc(m_dbConnectionData);

	if (pDB == NULL) return -1;

	// Get sample tree data, sort out different types; Optimized by Peter
	pDB->getSampleTrees(recTrakt.getTraktID(),TREE_ANYTYPE,vecTraktAllSampleTrees);
	for( UINT i = 0; i < vecTraktAllSampleTrees.size(); i++ )
	{
		CTransaction_sample_tree &rec = vecTraktAllSampleTrees[i];
		switch( rec.getTreeType() )
		{
		case SAMPLE_TREE:
		case TREE_OUTSIDE:
		case TREE_OUTSIDE_LEFT:
		case TREE_INSIDE_LEFT:
			vecTraktSampleTrees.push_back(rec);
			break;
		case TREE_OUTSIDE_NO_SAMP:
		case TREE_OUTSIDE_LEFT_NO_SAMP:	// Kanttr�d l�mnas (Ber�knad h�jd)
		case TREE_POS_NO_SAMP:	// Pos.tr�d (uttag)
		case TREE_INSIDE_LEFT_NO_SAMP:	// Tr�d i gatan l�mnas (Ber�knad h�jd)
			vecTraktNonSampleTrees.push_back(rec);
			break;
		case TREE_SAMPLE_REMAINIG:
			vecTraktLeftOverSampleTrees.push_back(rec);
			break;
		case TREE_SAMPLE_EXTRA:
			vecTraktExtraSampleTrees.push_back(rec);
			break;
		}
	}
	vecTraktAllSampleTrees.clear(); // This is just a temp vector, we don't need it anymore
	//---------------------------------------------------------------------------
	// Add sampletrees to vector for sum of sampletrees; 091124 p�d
	if (vecTraktSampleTrees.size() > 0)
	{
		for (UINT i = 0;i < vecTraktSampleTrees.size();i++)
			vecTraktSumSampleTrees.push_back(vecTraktSampleTrees[i]);
	}
	// Add Extra sampletrees to vector for sum of sampletrees; 091124 p�d
	if (vecTraktExtraSampleTrees.size() > 0)
	{
		for (UINT i = 0;i < vecTraktExtraSampleTrees.size();i++)
			vecTraktSumSampleTrees.push_back(vecTraktExtraSampleTrees[i]);
	}
	//---------------------------------------------------------------------------

	pDB->getDCLSAllTrees(recTrakt.getTraktID(), vecTraktDCLSTrees, vecTraktDCLS1Trees, vecTraktDCLS2Trees); // 'Uttag', 'Kvarl�mnat', 'Uttag i stickv�g'

	// Get ALL TraktData from DB, then add to each of, by type of data; 100120 p�d
	pDB->getTraktData(vecAllTraktData,recTrakt.getTraktID());
	if (vecAllTraktData.size() > 0)
	{
		// Sort out the different types of data, 'Uttag','Kvarl�mmnat' and 'Uttag i stickv�g'; 100120 p�d
		for (UINT i = 0;i < vecAllTraktData.size();i++)
		{
			if (vecAllTraktData[i].getTDataType() == STMP_LEN_WITHDRAW)
			{
				vecTraktData1.push_back(vecAllTraktData[i]);
				vecTraktData4.push_back(vecAllTraktData[i]);
			}
			else if (vecAllTraktData[i].getTDataType() == STMP_LEN_TO_BE_LEFT)
			{
				vecTraktData2.push_back(vecAllTraktData[i]);
				countDcls1 += vecAllTraktData[i].getNumOf();
			}
			else if (vecAllTraktData[i].getTDataType() == STMP_LEN_WITHDRAW_ROAD)
			{
				vecTraktData3.push_back(vecAllTraktData[i]);
				countDcls2 += vecAllTraktData[i].getNumOf();
			}
		}	// for (UINT i = 0;i < vecAllTraktData.size();i++)
	}	// if (vecAllTraktData.size() > 0)

	pDB->getPlots(recTrakt.getTraktID(),vecTraktPlot);
	pDB->getTraktSetSpc(vecSetSpc,recTrakt.getTraktID());

	pDB->getTraktMiscData(recTrakt.getTraktID(),recTraktMiscData);

	
	xmllitePricelistParser *pPrlParser = new xmllitePricelistParser();
	if (pPrlParser != NULL)
	{
		if (pPrlParser->loadStream(recTraktMiscData.getXMLPricelist()))
		{
			pPrlParser->getHeaderPriceIn(&nPriceSetIn);

			pPrlParser->getAssortmentPerSpecie(vecAssortmentsPrl);	// Added 100811 p�d
		}
		delete pPrlParser;
	}
	

	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	// Calculate Areal

	if (vecTraktPlot.size() > 0)
	{
		bIsCircularTest = FALSE;
		// If we have plot(s), check if we have data in plot radius.
		// If so, we have a "Cirkelyta/ytor"
		for (UINT i = 0;i < vecTraktPlot.size();i++)
		{
			CTransaction_plot plotData = vecTraktPlot[i];
			if (plotData.getPlotType() == 2 ||	// "Cirkelytor"
					plotData.getPlotType() == 3 ||	// "Rektangul�ra ytor"
					plotData.getPlotType() == 4)		// "Snabbtaxering"
			{
				fArea += plotData.getArea();
				bIsCircularTest = TRUE;
			}		

		}	// for (UINT i = 0;i < m_vecTraktPlot.size();i++)
	}	// if (vecTraktPlot.size() > 0)
	if (fArea > 0.0)
	{
		fArealCalculationFactor = (10000.0/fArea)*recTrakt.getTraktAreal();
		fPerHa = (10000.0/fArea);
	}	// if (fArea > 0.0)
	else
	{
		fArealCalculationFactor = 1.0;	// On calculting with this factor, do nothing, not yet anyway; 070614 p�d
		if (recTrakt.getTraktAreal() > 0.0)	// Add check for division by zero; 080117 p�d
		{
			fPerHa = (1.0/recTrakt.getTraktAreal());
		}
		else
			fPerHa = 0.0;
	}
	// Update fArealCalculationFactor in table "esti_trakt_table"; 091102 p�d
	pDB->setTrakt_Areal_Factor(recTrakt.getTraktID(),fArealCalculationFactor);

	// setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(4));		
	// Check if specie in vecTraktSampleTrees also have functions connected to
	// it. I.e. Volume-,Brak- and Heightfunction; 070626 p�d

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//	START checkFunctionsForTrees
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\	

	// Check if specie in vecTraktSampleTrees also have functions connected to
	// it. I.e. Volume-,Bark- and Heightfunction; 070626 p�d
	if (vecSetSpc.size() == 0)
	{
		bFuncCheckOK = FALSE;

		if (pDB != NULL) delete pDB;
		return -2;
	}
	vecFSpc.clear();
	if (vecTraktDCLSTrees.size() > 0)
	{
		for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)
		{
			bSpecieFound = FALSE;
			recTree = vecTraktDCLSTrees[ii];
			for (UINT i = 0;i < vecSetSpc.size();i++)
			{
				recSetSpc = vecSetSpc[i];

				// Check if we have the correct TraktID and Specie; 070626 p�d
				if (recSetSpc.getSpcID() == recTree.getSpcID() &&
						recSetSpc.getTSetspcTraktID() == recTrakt.getTraktID() &&
						recSetSpc.getHgtFuncID() > 0 &&
						recSetSpc.getVolFuncID() > 0 &&
						recSetSpc.getBarkFuncID() > 0 &&
						recSetSpc.getVolUBFuncID() > 0 &&
						recSetSpc.getM3FubToM3To() > 0.0)
						//recSetSpc.getM3SkToM3Fub() > 0.0) Not used 2009-06-23 p�d
				{
					bSpecieFound = TRUE;
					break;
				}	// if (recSetSpc.getSpcID() == recTree.getSpcID() &&
			}	// for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)

			bSpecieFound2 = TRUE;	
			if (!bSpecieFound)
			{
				bSpecieFound2 = FALSE;
				for (UINT i1 = 0;i1 < vecFSpc.size();i1++)
				{
					if (vecFSpc[i1].nSpcID == recTree.getSpcID())
					{
						bSpecieFound2 = TRUE;
						break;
					}
				}
			}	// if (!bSpecieFound)
			// Not found
			if (!bSpecieFound2)
			{
				vecFSpc.push_back(_func_spc_struct(recTree.getSpcID(),recTree.getSpcName()));
			}	// if (!bSpecieFound2)
		}	// for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)
	}	// if (vecTraktDCLSTrees.size() > 0)

	if (vecTraktDCLS1Trees.size() > 0)
	{
		for (UINT ii = 0;ii < vecTraktDCLS1Trees.size();ii++)
		{
			bSpecieFound = FALSE;
			recTree = vecTraktDCLS1Trees[ii];
			for (UINT i = 0;i < vecSetSpc.size();i++)
			{
				recSetSpc = vecSetSpc[i];

				// Check if we have the correct TraktID and Specie; 070626 p�d
				if (recSetSpc.getSpcID() == recTree.getSpcID() &&
						recSetSpc.getTSetspcTraktID() == recTrakt.getTraktID() &&
						recSetSpc.getHgtFuncID() > 0 &&
						recSetSpc.getVolFuncID() > 0 &&
						recSetSpc.getBarkFuncID() > 0 &&
						recSetSpc.getVolUBFuncID() > 0 &&
						recSetSpc.getM3FubToM3To() > 0.0)
						//recSetSpc.getM3SkToM3Fub() > 0.0) Not used 2009-06-23 p�d
				{
					bSpecieFound = TRUE;
					break;
				}	// if (recSetSpc.getSpcID() == recTree.getSpcID() &&
			}	// for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)

			bSpecieFound2 = TRUE;	
			if (!bSpecieFound)
			{
				bSpecieFound2 = FALSE;
				for (UINT i1 = 0;i1 < vecFSpc.size();i1++)
				{
					if (vecFSpc[i1].nSpcID == recTree.getSpcID())
					{
						bSpecieFound2 = TRUE;
						break;
					}
				}

				// Not found
				if (!bSpecieFound2)
				{
					vecFSpc.push_back(_func_spc_struct(recTree.getSpcID(),recTree.getSpcName()));
				}	// if (!bSpecieFound2)
			}	// if (!bSpecieFound)
		}	// for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)
	}	// if (vecTraktDCLS1Trees.size() > 0)

	if (vecTraktDCLS2Trees.size() > 0)
	{
		for (UINT ii = 0;ii < vecTraktDCLS2Trees.size();ii++)
		{
			bSpecieFound = FALSE;
			recTree = vecTraktDCLS2Trees[ii];
			for (UINT i = 0;i < vecSetSpc.size();i++)
			{
				recSetSpc = vecSetSpc[i];

				// Check if we have the correct TraktID and Specie; 070626 p�d
				if (recSetSpc.getSpcID() == recTree.getSpcID() &&
						recSetSpc.getTSetspcTraktID() == recTrakt.getTraktID() &&
						recSetSpc.getHgtFuncID() > 0 &&
						recSetSpc.getVolFuncID() > 0 &&
						recSetSpc.getBarkFuncID() > 0 &&
						recSetSpc.getVolUBFuncID() > 0 &&
						recSetSpc.getM3FubToM3To() > 0.0 )
						//recSetSpc.getM3SkToM3Fub() > 0.0) Not used 2009-06-23 p�d
				{
					bSpecieFound = TRUE;
					break;
				}	// if (recSetSpc.getSpcID() == recTree.getSpcID() &&
			}	// for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)

			bSpecieFound2 = TRUE;	
			if (!bSpecieFound)
			{
				bSpecieFound2 = FALSE;
				for (UINT i1 = 0;i1 < vecFSpc.size();i1++)
				{
					if (vecFSpc[i1].nSpcID == recTree.getSpcID())
					{
						bSpecieFound2 = TRUE;
						break;
					}
				}

				// Not found
				if (!bSpecieFound2)
				{
					vecFSpc.push_back(_func_spc_struct(recTree.getSpcID(),recTree.getSpcName()));
				}	// if (!bSpecieFound2)
			}	// if (!bSpecieFound)
		}	// for (UINT ii = 0;ii < vecTraktDCLSTrees.size();ii++)
	}	// if (vecTraktDCLS2Trees.size() > 0)

	bFuncCheckOK = TRUE;
	// Check if there's any species not havin' functions added.
	// If so create a log-file and tell user at the end of the
	// transaction; 080116 p�d
	if (vecFSpc.size() > 0)
	{
		//------------------------------------------------------
		// User information; 080117 p�d
		sTrakt.Format(_T("%s - %s"),
				(recTrakt.getTraktNum()),
				(recTrakt.getTraktName()));
		if (recTrakt.getTraktName().IsEmpty())
		{
			sTrakt.Format(_T("%s"),(recTrakt.getTraktNum()));
		}
		log.Add((sTrakt));			
		log.Add(_T(""));			
		for (UINT iii = 0;iii < vecFSpc.size();iii++)
		{
			CString sSpc;
			sSpc.Format(_T("- %s"),vecFSpc[iii].sSpcName);
			log.Add(sSpc);
		}
		log.Add(_T("------------------------------------------------------------------------"));
	
//				bFuncCheckOK = FALSE;
		nReturnValue = -3;	// There's a logfile created; 080116 p�d
	}	// if (vecFSpc.size() > 0)

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//	END checkFunctionsForTrees
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	// Make sure there's data to work with; 070521 p�d
	if (vecSetSpc.size() > 0 && bFuncCheckOK)
	{
		// Proceed, by doin' the actual calculations; 070521 p�d
		// setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(5));

		// setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(6));
		// This function's declared in PAD_HMSFuncLib	
		// Caluclate volume for "St�mplingsl�ngd - Uttag"; 071120 p�d
		if (vecTraktDCLSTrees.size() > 0)
		{
			doUCCalculate(recTrakt,recTraktMiscData,vecTraktData1,vecTraktSumSampleTrees,vecTraktDCLSTrees,vecSetSpc);		
			bIsDCLSTrees = TRUE;
		}

		// Caluclate volume for "St�mplingsl�ngd - Kvarl�mmnat"; 071120 p�d
		if (vecTraktDCLS1Trees.size() > 0)
		{
			doUCCalculate(recTrakt,recTraktMiscData,vecTraktData2,vecTraktSumSampleTrees,vecTraktDCLS1Trees,vecSetSpc,true,false);		
			bIsDCLS1Trees = TRUE;
		}

		// Caluclate volume for "St�mplingsl�ngd - Uttag i stickv�g"; 071120 p�d
		if (vecTraktDCLS2Trees.size() > 0)
		{
			doUCCalculate(recTrakt,recTraktMiscData,vecTraktData3,vecTraktSumSampleTrees,vecTraktDCLS2Trees,vecSetSpc,true,false);		
			bIsDCLS2Trees = TRUE;
		}
		//=================================================================================================
		// Caluclate volume for "non" sample trees; 071123 p�d

		vecTraktCalculateSampleTrees.clear();	// Vector to hold "ALL" sample trees; 080129 p�d
		if (vecTraktNonSampleTrees.size() > 0)
		{
			fDCLS_Set = recTraktMiscData.getDiamClass();
			// We need to do a Special case here.
			// Add trees "Kanttr�d utan h�jd" to a list
			// of vecTransactionDCLSTree type of trees
			// and use this list for calculating heights for trees ("Kanttr�d utan h�jd"); 071123 p�d
			for (UINT ii = 0;ii < vecTraktNonSampleTrees.size();ii++)
			{
				CTransaction_sample_tree data1 = vecTraktNonSampleTrees[ii];
				// Caluclate diameterclas for "Kanttr�d" diameter
				// Use same diameterclass as on other trees; 080122 p�d
				getDiamClass(data1.getDBH(),fDCLS_Set,&fDCLS_From,&fDCLS_To);
				vecDCLS.push_back(CTransaction_dcls_tree(data1.getTreeID(),
																								 data1.getTraktID(),
																								 data1.getPlotID(),
																								 data1.getSpcID(),
																								 data1.getSpcName(),
																								 fDCLS_From,
																								 fDCLS_To,
																								 0.0,	// No height yet; 080122 p�d
																								 1,	// Only one tree in diameterclass
																								 data1.getGCrownPerc(),
																								 0.0, //data1.getBarkThick(),
																								 0.0, //data1.getM3Sk(),
																								 0.0, //data1.getM3Fub(),
																								 0.0, //data1.getM3Ub(),
																								 data1.getGROT(),
																								 data1.getAge(),
																								 data1.getGrowth(),
																								 0,
																								 _T("") ));

				vecTraktCalculateSampleTrees.push_back(CTransaction_sample_tree(data1));
			}	// for (UINT ii = 0;ii < vecTraktNonSampleTrees.size();ii++)
		}


		if (vecTraktSampleTrees.size() > 0)
		{

			fDCLS_Set = recTraktMiscData.getDiamClass();
			for (UINT ii = 0;ii < vecTraktSampleTrees.size();ii++)
			{
				CTransaction_sample_tree data2 = vecTraktSampleTrees[ii];
				// TREE_OUTSIDE = "Kanttr�d Provtr�d"
				if (data2.getTreeType() == TREE_OUTSIDE ||
					  data2.getTreeType() == SAMPLE_TREE /*|| 
					  data2.getTreeType() == TREE_SAMPLE_EXTRA_OUT*/)
				{
					// Caluclate diameterclas for "Kanttr�d" diameter
					// Use same diameterclass as on other trees; 080122 p�d
					getDiamClass(data2.getDBH(),fDCLS_Set,&fDCLS_From,&fDCLS_To);
/*
					S.Format("TREE_OUTSIDE\ndata2.getDBH() %f\nfDCLS_Set %f,fDCLS_From %f\nfDCLS_To %f",
						data2.getDBH(),fDCLS_Set,fDCLS_From,fDCLS_To);
					//UMMessageBox(S);
*/
					vecDCLS.push_back(CTransaction_dcls_tree(data2.getTreeID(),
																									 data2.getTraktID(),
																									 data2.getPlotID(),
																									 data2.getSpcID(),
																									 data2.getSpcName(),
																									 fDCLS_From,
																									 fDCLS_To,
																									 data2.getHgt(),	// A sample tree'll have a height; 080122 p�d
																									 1,	// Only one tree in diameterclass
																									 data2.getGCrownPerc(),
																									 0.0, //data1.getBarkThick(),
																									 0.0, //data1.getM3Sk(),
																									 0.0, //data1.getM3Fub(),
																									 0.0, //data1.getM3Ub(),
																									 data2.getGROT(),
																									 data2.getAge(),
																									 data2.getGrowth(),
																									 -999,	// Special case -999 indicates it's SAMPLETREES, don't calc. height; 080626 p�d
																									 _T("") ));

					vecTraktCalculateSampleTrees.push_back(CTransaction_sample_tree(data2));

				}	// for (UINT ii = 0;ii < vecTraktNonSampleTrees.size();ii++)
			}
		}




		if (vecTraktLeftOverSampleTrees.size() > 0)
		{
			fDCLS_Set = recTraktMiscData.getDiamClass();
			for (UINT ii = 0;ii < vecTraktLeftOverSampleTrees.size();ii++)
			{
				CTransaction_sample_tree data3 = vecTraktLeftOverSampleTrees[ii];
				// TREE_SAMPLE_REMAINIG = "Kanttr�d (Kvarl�mmnat)"
				if (data3.getTreeType() == TREE_SAMPLE_REMAINIG)
				{
					// Caluclate diameterclas for "Kanttr�d" diameter
					// Use same diameterclass as on other trees; 080122 p�d
					getDiamClass(data3.getDBH(),fDCLS_Set,&fDCLS_From,&fDCLS_To);
					vecDCLS.push_back(CTransaction_dcls_tree(data3.getTreeID(),
																									 data3.getTraktID(),
																									 data3.getPlotID(),
																									 data3.getSpcID(),
																									 data3.getSpcName(),
																									 fDCLS_From,
																									 fDCLS_To,
																									 data3.getHgt(),	// A sample tree'll have a height; 080122 p�d
																									 1,	// Only one tree in diameterclass
																									 data3.getGCrownPerc(),
																									 0.0, //data3.getBarkThick(),
																									 0.0, //data3.getM3Sk(),
																									 0.0, //data3.getM3Fub(),
																									 0.0, //data3.getM3Ub(),
																									 data3.getGROT(),
																									 data3.getAge(),
																									 data3.getGrowth(),
																									 0,
																									 _T("") ));

				vecTraktCalculateSampleTrees.push_back(CTransaction_sample_tree(data3));

				}	// for (UINT ii = 0;ii < vecTraktNonSampleTrees.size();ii++)
			}
		}
		if (vecTraktExtraSampleTrees.size() > 0)
		{
			fDCLS_Set = recTraktMiscData.getDiamClass();
			for (UINT ii = 0;ii < vecTraktExtraSampleTrees.size();ii++)
			{
				CTransaction_sample_tree data4 = vecTraktExtraSampleTrees[ii];
				// TREE_SAMPLE_EXTRA = "Provtr�d (Extra)"
				if (data4.getTreeType() == TREE_SAMPLE_EXTRA)
				{
					// Caluclate diameterclas for "Provtr�d" diameter
					// Use same diameterclass as on other trees; 080122 p�d
					getDiamClass(data4.getDBH(),fDCLS_Set,&fDCLS_From,&fDCLS_To);
					vecDCLS.push_back(CTransaction_dcls_tree(data4.getTreeID(),
																									 data4.getTraktID(),
																									 data4.getPlotID(),
																									 data4.getSpcID(),
																									 data4.getSpcName(),
																									 fDCLS_From,
																									 fDCLS_To,
																									 data4.getHgt(),	// A sample tree'll have a height; 080122 p�d
																									 1,	// Only one tree in diameterclass
																									 data4.getGCrownPerc(),
																									 0.0, //data3.getBarkThick(),
																									 0.0, //data3.getM3Sk(),
																									 0.0, //data3.getM3Fub(),
																									 0.0, //data3.getM3Ub(),
																									 data4.getGROT(),
																									 data4.getAge(),
																									 data4.getGrowth(),
																									 -999, // Special case -999 indicates it's SAMPLETREES, don't calc. height; 100128 p�d
																									 _T("") ));

				vecTraktCalculateSampleTrees.push_back(CTransaction_sample_tree(data4));

				}	// for (UINT ii = 0;ii < vecTraktNonSampleTrees.size();ii++)
			}
		}	// if (vecTraktNonSampleTrees.size() > 0)

		//=================================================================================================
		// Do caluclation for "Sample" trees; 080129 p�d
		if (vecDCLS.size() > 0)
		{
			doUCCalculate(recTrakt,recTraktMiscData,vecTraktData4,vecTraktSumSampleTrees,vecDCLS,vecSetSpc);		
			bIsNonSampTrees = TRUE;
		}	// if (vecDCLS.size() > 0)
		//=================================================================================================
		// setStatusBarText(1,m_sarrCalcStatusMsg.GetAt(7));
		// This function's declared in PAD_HMSFuncLib	
		vecTreeAssort.clear();

		doUMExchange(recTraktMiscData,vecTraktDCLSTrees,vecTreeAssort,vecSetSpc);

//*

		// Save traktdata for "St�mplingsl�ngd - Uttag"
		// Changed 100120 p�d
		pDB->updDCLSTrees_m3fub(vecTraktDCLSTrees,mapSumM3Fub);
		pDB->getDCLSNumOfTrees(recTrakt.getTraktID(), fArealCalculationFactor, mapDcls0NumTrees);
		for (UINT i = 0; i < vecTraktData1.size();i++)
		{
			if (bIsDCLSTrees)
			{
				//--------------------------------------------------------------------------------
				// G�R AREALSUPPR�KNING/DIAMTERKLASS I vecTraktDCLSTrees; 091102 p�d
				nNumOfAreal = mapDcls0NumTrees[vecTraktData1[i].getSpecieID()]; // Get num trees from cached value. This values include randtrees; Optimization by Peter
				// OBS! Set to 1 tree minimum; 071022 p�d
				if (nNumOfAreal < 1 && vecTraktData1[i].getPercent() > 0.0)
					nNumOfAreal = 1;
				// Calulate "Grundyta/ha", from dm2 to m2; 071120 p�d
				fGYPerHa = (vecTraktData1[i].getGY())*fPerHa;
				vecTraktData1[i].setGY(fGYPerHa);						
				vecTraktData1[i].setNumOf(nNumOfAreal);	
				vecTraktData1[i].setM3SK(vecTraktData1[i].getM3SK()*fArealCalculationFactor);	
				vecTraktData1[i].setM3FUB(mapSumM3Fub[vecTraktData1[i].getSpecieID()]*fArealCalculationFactor); // vecTraktData1[i].getM3FUB()*fArealCalculationFactor);	
				if (vecTraktData1[i].getNumOf() > 0 && fArealCalculationFactor > 0.0)
					vecTraktData1[i].setAvgM3FUB((mapSumM3Fub[vecTraktData1[i].getSpecieID()]/vecTraktData1[i].getNumOf())*fArealCalculationFactor); // vecTraktData1[i].getM3FUB()*fArealCalculationFactor);	
				else if (vecTraktData1[i].getNumOf() == 0 && fArealCalculationFactor > 0.0)
					vecTraktData1[i].setAvgM3FUB(mapSumM3Fub[vecTraktData1[i].getSpecieID()]/1.0*fArealCalculationFactor); // vecTraktData1[i].getM3FUB()*fArealCalculationFactor);	
				else
					vecTraktData1[i].setAvgM3FUB(0.0); // vecTraktData1[i].getM3FUB()*fArealCalculationFactor);	
				vecTraktData1[i].setM3UB(vecTraktData1[i].getM3UB()*fArealCalculationFactor);				
				vecTraktData1[i].setGrot(vecTraktData1[i].getGrot()*fArealCalculationFactor);	
				//if (!pDB->addTraktData(vecTraktData2[i]))
				//pDB->updTraktData(vecTraktData1[i]);
			}	// if (bIsDCLSTrees)
			else
			{
				vecTraktData1[i].setAvgHgt(0.0);
				vecTraktData1[i].setAvgM3FUB(0.0);
				vecTraktData1[i].setAvgM3SK(0.0);
				vecTraktData1[i].setAvgM3UB(0.0);
				vecTraktData1[i].setGY(0.0);	
				vecTraktData1[i].setDA(0.0);
				vecTraktData1[i].setDG(0.0);
				vecTraktData1[i].setDGV(0.0);
				//vecTraktData1[i].setGreenCrown(0.0);
				//vecTraktData1[i].setH25(0.0);
				vecTraktData1[i].setHGV(0.0);
				vecTraktData1[i].setPercent(0.0);
				vecTraktData1[i].setNumOf(0);	
				vecTraktData1[i].setM3SK(0.0);	
				vecTraktData1[i].setM3FUB(0.0);	
				vecTraktData1[i].setM3UB(0.0);	
				vecTraktData1[i].setGrot(0.0);	
				//if (!pDB->addTraktData(vecTraktData2[i]))
				//pDB->updTraktData(vecTraktData1[i]);
			}
		}
		if (vecTraktData1.size() > 0)
		{
			pDB->updTraktData(vecTraktData1);
		}


		// Save traktdata for "St�mplingsl�ngd - Kvarl�mmnat"
		// Changed 090313 p�d
		for (UINT i = 0; i < vecTraktData2.size();i++)
		{
			if (bIsDCLS1Trees)
			{
				// Do "Arealsuppr�kning", before saving data; 070614 p�d			
				nNumOfAreal = vecTraktData2[i].getNumOf()*fArealCalculationFactor;
				// OBS! Set to 1 tree minimum; 071022 p�d
				if (nNumOfAreal < 1 && 
						vecTraktData2[i].getPercent() > 0.0)
					nNumOfAreal = 1;
				// Calulate "Grundyta/ha", from dm2 to m2; 071120 p�d
				fGYPerHa = (vecTraktData2[i].getGY())*fPerHa;
				vecTraktData2[i].setGY(fGYPerHa);	
				vecTraktData2[i].setNumOf(nNumOfAreal);	
				vecTraktData2[i].setM3SK(vecTraktData2[i].getM3SK()*fArealCalculationFactor);	
				vecTraktData2[i].setM3FUB(vecTraktData2[i].getM3FUB()*fArealCalculationFactor);	
				vecTraktData2[i].setM3UB(vecTraktData2[i].getM3UB()*fArealCalculationFactor);	
				//if (!pDB->addTraktData(vecTraktData2[i]))
				//pDB->updTraktData(vecTraktData2[i]);
			}	// if (bIsDCLS1Trees)
			else
			{
				vecTraktData2[i].setAvgHgt(0.0);
				vecTraktData2[i].setAvgM3FUB(0.0);
				vecTraktData2[i].setAvgM3SK(0.0);
				vecTraktData2[i].setAvgM3UB(0.0);
				vecTraktData2[i].setGY(0.0);	
				vecTraktData2[i].setDA(0.0);
				vecTraktData2[i].setDG(0.0);
				vecTraktData2[i].setDGV(0.0);
				//vecTraktData2[i].setGreenCrown(0.0);
				//vecTraktData2[i].setH25(0.0);
				vecTraktData2[i].setHGV(0.0);
				vecTraktData2[i].setPercent(0.0);
				vecTraktData2[i].setNumOf(0);	
				vecTraktData2[i].setM3SK(0.0);	
				vecTraktData2[i].setM3FUB(0.0);	
				vecTraktData2[i].setM3UB(0.0);	
				//if (!pDB->addTraktData(vecTraktData2[i]))
				//pDB->updTraktData(vecTraktData2[i]);
			}
		}
		if (vecTraktData2.size() > 0)
		{
			// Either we have calculated values or database contain calculated value and should be reset to zero; Optimization by Peter
			if( bIsDCLS1Trees || countDcls1 > 0 )
			{
				pDB->updTraktData(vecTraktData2);
			}
		}


		// Save traktdata for "St�mplingsl�ngd - Uttag i Stickv�g"
		// Changed 090313 p�d
		for (UINT i = 0; i < vecTraktData3.size();i++)
		{
			if (bIsDCLS2Trees)
			{
				// Do "Arealsuppr�kning", before saving data; 070614 p�d			
				nNumOfAreal = vecTraktData3[i].getNumOf()*fArealCalculationFactor;
				// OBS! Set to 1 tree minimum; 071022 p�d
				if (nNumOfAreal < 1 && 
						vecTraktData3[i].getPercent() > 0.0)
					nNumOfAreal = 1;
				// Calulate "Grundyta/ha", from dm2 to m2; 071120 p�d
				fGYPerHa = (vecTraktData3[i].getGY())*fPerHa;
				vecTraktData3[i].setGY(fGYPerHa);	
				vecTraktData3[i].setNumOf(nNumOfAreal);	
				vecTraktData3[i].setM3SK(vecTraktData3[i].getM3SK()*fArealCalculationFactor);	
				vecTraktData3[i].setM3FUB(vecTraktData3[i].getM3FUB()*fArealCalculationFactor);	
				vecTraktData3[i].setM3UB(vecTraktData3[i].getM3UB()*fArealCalculationFactor);	
				//if (!pDB->addTraktData(vecTraktData2[i]))
				//pDB->updTraktData(vecTraktData3[i]);
			}	// if (bIsDCLS1Trees)
			else
			{
				vecTraktData3[i].setAvgHgt(0.0);
				vecTraktData3[i].setAvgM3FUB(0.0);
				vecTraktData3[i].setAvgM3SK(0.0);
				vecTraktData3[i].setAvgM3UB(0.0);
				vecTraktData3[i].setGY(0.0);	
				vecTraktData3[i].setDA(0.0);
				vecTraktData3[i].setDG(0.0);
				vecTraktData3[i].setDGV(0.0);
				//vecTraktData3[i].setGreenCrown(0.0);
				//vecTraktData3[i].setH25(0.0);
				vecTraktData3[i].setHGV(0.0);
				vecTraktData3[i].setPercent(0.0);
				vecTraktData3[i].setNumOf(0);	
				vecTraktData3[i].setM3SK(0.0);	
				vecTraktData3[i].setM3FUB(0.0);	
				vecTraktData3[i].setM3UB(0.0);	
				//if (!pDB->addTraktData(vecTraktData2[i]))
				//pDB->updTraktData(vecTraktData3[i]);
			}
		}
		if (vecTraktData3.size() > 0)
		{
			// Either we have calculated values or database contain calculated value and should be reset to zero; Optimization by Peter
			if( bIsDCLS2Trees || countDcls2 > 0 )
			{
				pDB->updTraktData(vecTraktData3);
			}
		}


		// Update sample trees in trakt
		pDB->updSampleTrees(vecTraktSampleTrees);

		// Handle "Kanttr�d utan h�jd", these trees'll be entered in the
		// "Sample" tree database table; 071123 p�d

		//=================================================================================================
		if (bIsNonSampTrees && vecTraktCalculateSampleTrees.size() == vecDCLS.size())
		{
			// Remove sample trees in trakt, and add new trees; 070821 p�s
			for (UINT i = 0; i < vecDCLS.size();i++)
			{
				vecTraktCalculateSampleTrees[i].setHgt(vecDCLS[i].getHgt());
				vecTraktCalculateSampleTrees[i].setM3Sk(vecDCLS[i].getM3sk());
				vecTraktCalculateSampleTrees[i].setM3Fub(vecDCLS[i].getM3fub());
				vecTraktCalculateSampleTrees[i].setM3Ub(vecDCLS[i].getM3ub());
				vecTraktCalculateSampleTrees[i].setBarkThick(vecDCLS[i].getBarkThick());
				vecTraktCalculateSampleTrees[i].setDCLS_from(vecDCLS[i].getDCLS_from());
				vecTraktCalculateSampleTrees[i].setDCLS_to(vecDCLS[i].getDCLS_to());
				vecTraktCalculateSampleTrees[i].setGROT(vecDCLS[i].getGROT());
				//if (!pDB->addSampleTrees(vecTraktCalculateSampleTrees[i]))
			}
			pDB->updSampleTrees(vecTraktCalculateSampleTrees);
		}
		//=================================================================================================

		// Update dcls tables. Changed 100120 p�d
		if (bIsDCLSTrees)
		{
			pDB->updDCLSTrees(vecTraktDCLSTrees);
		}
		if (bIsDCLS1Trees)
		{
			pDB->updDCLS1Trees(vecTraktDCLS1Trees);
		}
		if (bIsDCLS2Trees)
		{
			pDB->updDCLS2Trees(vecTraktDCLS2Trees);
		}

		// Remove and re-add ALL Assortments per Specie for this Trakt; 070528 p�d
		pDB->delAssTree(recTrakt.getTraktID());
		pDB->addAssTree(vecTreeAssort);

		// We need to check what kind of Pricelist, user selected.
		// If it's a Pricelist usin' price information, we need
		// to save the calculated value of Price in m3to to
		// the "esti_trakt_spc_assort_table"; 071024 p�d
		if (recTraktMiscData.getTypeOf() == ID_RUNE_OLLAS_TYPEOF_PRICELIST_1)
		{
			vecExchCalcInfo vec;
			// Wee only need to load this data here; 071024 p�d
			if (pDB->getExchPriceCalulatedInExchangeModule(recTrakt.getTraktID(),vec))
			{
				if (vec.size() > 0)
				{
					// Changed 2010-01-20 p�d
					pDB->updTraktAss_prices(vec);
				}
			}
		}

		// Remove any empty dcls; 151518 Peter
		vecTransactionDCLSTree::iterator iter = vecTraktDCLSTrees.begin();
		while( iter != vecTraktDCLSTrees.end() )
		{
			iter = vecTraktDCLSTrees.begin();

			while( iter != vecTraktDCLSTrees.end() )
			{
				if( iter->getNumOf() == 0 && iter->getNumOfRandTrees() == 0 )
				{
					pDB->delDCLSTreesInTrakt(iter->getTreeID(), iter->getTraktID());
					vecTraktDCLSTrees.erase(iter);
					break; // Re-run loop
				}
				iter++;
			}
		}

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//	START calculateAssortFromTreesToSpc
//  Method for calculating sum price per Assortments per specie from; 070531 p�d
//  from: esti_trees_assort_table
//  to: esti_trakt_spc_assort_table
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		double fM3FubToM3To = 0.0;
		double fPriceFub = 0.0;
		double fPriceTo = 0.0;
		double fCalcPriceFub = 0.0;
		double fCalcPriceTo = 0.0;
		double fVolumeFub = 0.0;
		double fVolumeTo = 0.0;

		// Reset values in table "esti_trakt_spc_assort_table", before adding
		// new values for Trakt; 070705 p�d
		// This is becuse we need to remove assortments data for specie(s), that maybe have
		// been removed from tree-list on PageThree; 070705 p�d
		//pDB->resetTraktAss(recTrakt.getTraktID()); This call has been replaced with a reset of the local vector, see below.

		// Get resetted data in table "esti_trakt_spc_assort_table"; 070705 p�d
		pDB->getTraktAss(vecTraktAss,recTrakt.getTraktID());

		// 2012-02-06: code commented out because it resets the calculated m3to value. refs #2816
		// Update specie assortment prices (so user won't have to update pricelist for changed to take effect, bug #2714)
		/*for (UINT i = 0; i < vecTraktAss.size(); i++)
		{
			for (UINT j = 0; j < vecAssortmentsPrl.size(); j++)
			{
				if( vecAssortmentsPrl[j].getAssortName().CompareNoCase(vecTraktAss[i].getTAssName()) == 0 )
				{
					vecTraktAss[i].setPriceM3TO(vecAssortmentsPrl[j].getPriceM3to());
				}
			}
		}*/

		// Reset values. This way we don't need to bother commiting values to db prior to the calculation; Optimization by Peter
		for( UINT i = 0; i < vecTraktAss.size(); i++ )
		{
			//#3524 Nollar �ven volymer h�r annars blir dessa inte nollade om man tar bort alla diameterklasser f�r ett tr�dslag J� 20121220
			vecTraktAss[i].setM3FUB(0);
			vecTraktAss[i].setM3TO(0);
			vecTraktAss[i].setValueM3FUB(0);
			vecTraktAss[i].setValueM3TO(0);
		}

		CTransaction_trakt_ass tdata;
		if (vecTraktAss.size() > 0 && vecTreeAssort.size() > 0)
		{
			// Use m_vecTransactionTraktAss vector to check for Specie (tass_trakt_data_id)
			for (UINT i = 0;i < vecTraktAss.size();i++)
			{
				fPriceFub = 0.0;
				fPriceTo = 0.0;
				fVolumeFub = 0.0;
				fVolumeTo = 0.0;
				fCalcPriceFub = 0.0;
				fCalcPriceTo = 0.0;
				tdata = vecTraktAss[i];
				// Get info on "�verf�ringstal" for specie; 070531 p�d
				for (UINT iii = 0;iii < vecSetSpc.size();iii++)
				{
					CTransaction_trakt_set_spc tsetspc = vecSetSpc[iii];
					if (tsetspc.getSpcID() == tdata.getTAssTraktDataID())
					{
						fM3FubToM3To = tsetspc.getM3FubToM3To();
						fPriceFub = tdata.getPriceM3FUB();
						fPriceTo = tdata.getPriceM3TO();
						break;
					}	// if (tsetspc.getSpcID() == tdata.getTAssTraktDataID())
				}	// for (UINT iii = 0;iii < vecSetSpc.size();iii++)
		
				for (UINT ii = 0;ii < vecTreeAssort.size();ii++)
				{
					CTransaction_tree_assort tass = vecTreeAssort[ii];
					if (tass.getSpcID() == tdata.getTAssTraktDataID() && tass.getAssortName() == tdata.getTAssName())
					{
						// Get volumes, M3Fub and M3To; 090311 p�d
						fVolumeFub = tass.getExchVolume();
						fVolumeTo = fVolumeFub * fM3FubToM3To;
						fCalcPriceTo = 0.0;
						fCalcPriceFub = 0.0;
						// Calculate To price for assortment, based on setting of PriceIn; 090311 p�d
						if (tass.getPriceIn() == ASSORTMENT_TIMBER_TO || 
								tass.getPriceIn() == ASSORTMENT_TIMBER_FUB_1 || 
								tass.getPriceIn() == ASSORTMENT_TIMBER_FUB_3)
						{
							if (recTraktMiscData.getTypeOf() == ID_RUNE_OLLAS_TYPEOF_PRICELIST_1)
							{
								if (nPriceSetIn == 1)	// Prcie set in m3fub; 090615 p�d
								{
									// M3FUB
									// Change 090311 p�d
									if (fPriceFub > 0.0 && fVolumeFub > 0.0)
									{
										fCalcPriceFub = fPriceFub/fVolumeFub; //tass.getExchPrice();
									}
									else
										fCalcPriceFub = 0.0;
									
									fCalcPriceTo = 0.0;
						//S.Format(_T("Price set in m3fub\nfPriceFub %f\nfVolumeFub %f"),fPriceFub,fVolumeFub*fArealCalculationFactor);
						////UMMessageBox(S);

								}
								else if (nPriceSetIn == 2)	// Preice set in m3to; 090615 p�d
								{
									// M3FUB
									// Change 090311 p�d
									fCalcPriceFub = 0.0;
									
									// M3TO					
									// Change 090311 p�d
									if (fPriceTo > 0.0 && fVolumeTo > 0.0)
										fCalcPriceTo = fPriceTo/fVolumeTo;
									else
										fCalcPriceTo = 0.0;

//						S.Format(_T("A Price set in m3to\ntass.getSpcID() %d\nfPriceTo %f\nfVolumeTo %f\n\nfVolumeFub %f"),tass.getSpcID(),fPriceTo,fVolumeTo*fArealCalculationFactor,fVolumeFub*fArealCalculationFactor);
//						UMMessageBox(S);

								}
							
								vecTraktAss[i].setM3FUB(fVolumeFub*fArealCalculationFactor);	// Do "Arealsuppr�kning"
								vecTraktAss[i].setM3TO(fVolumeTo*fArealCalculationFactor);	// Do "Arealsuppr�kning"
								if (fCalcPriceFub > 0.0)
									vecTraktAss[i].setValueM3FUB(fVolumeFub*fCalcPriceFub*fArealCalculationFactor+(fVolumeFub*fArealCalculationFactor*tdata.getKrPerM3()));
								else
									vecTraktAss[i].setValueM3FUB(0.0);
								if (fCalcPriceTo > 0.0)
									vecTraktAss[i].setValueM3TO(fVolumeTo*fCalcPriceTo*fArealCalculationFactor+(fVolumeTo*fArealCalculationFactor*tdata.getKrPerM3()));	
								else
									vecTraktAss[i].setValueM3TO(0.0);	
									
								vecTraktAss[i].setPriceM3FUB(fCalcPriceFub); //*fArealCalculationFactor);
								vecTraktAss[i].setPriceM3TO(fCalcPriceTo); //*fArealCalculationFactor);


							}
							else if (recTraktMiscData.getTypeOf() == ID_RUNE_OLLAS_TYPEOF_PRICELIST_2)
							{
								// If we're usin' an avg. pricelist, we'll set type of depending on how price is set; 091007 p�d
								// Set; if both fub and to price is set, we'll set TO as default price; 091007 p�d
								if (fPriceFub > 0.0 && fPriceTo == 0.0) nPriceSetIn = 1;
								else if (fPriceFub > 0.0 && fPriceTo > 0.0) nPriceSetIn = 2;
								else if (fPriceFub == 0.0 && fPriceTo > 0.0) nPriceSetIn = 2;
						
								if (nPriceSetIn == 1)	// Prcie set in m3fub; 090615 p�d
								{
									// M3FUB
									// Change 090311 p�d
									if (fPriceFub > 0.0 && fVolumeFub > 0.0)
									{
										fCalcPriceFub = fPriceFub/fVolumeFub; //tass.getExchPrice();
									}
									else
										fCalcPriceFub = 0.0;
									
									fCalcPriceTo = 0.0;
						
//								S.Format(_T("B Price set in m3fub\ntass.getSpcID() %d\nfPriceFub %f\nfVolumeFub %f"),tass.getSpcID(),fPriceFub,fVolumeFub*fArealCalculationFactor);
//								UMMessageBox(S);

									vecTraktAss[i].setM3FUB(fVolumeFub*fArealCalculationFactor);	// Do "Arealsuppr�kning"
									vecTraktAss[i].setM3TO(0.0);	
									vecTraktAss[i].setValueM3FUB(fPriceFub*fVolumeFub*fArealCalculationFactor+(fVolumeFub*fArealCalculationFactor*tdata.getKrPerM3()));	// Do "Arealsuppr�kning"
									vecTraktAss[i].setValueM3TO(0.0);


								}
								else if (nPriceSetIn == 2)	// Preice set in m3to; 090615 p�d
								{
									// M3FUB
									// Change 090311 p�d
									fCalcPriceFub = 0.0;
									
									// M3TO					
									// Change 090311 p�d
									if (fPriceTo > 0.0 && fVolumeTo > 0.0)
									{
										fCalcPriceTo = fPriceTo/fVolumeTo;
									}
									else
										fCalcPriceTo = 0.0;

//								S.Format(_T("B Price set in m3to\ntass.getSpcID() %d\nfPriceTo %f\nfVolumeTo %f\n\nfVolumeFub %f"),tass.getSpcID(),fPriceTo,fVolumeTo*fArealCalculationFactor,fVolumeFub*fArealCalculationFactor);
//								UMMessageBox(S);
	
									vecTraktAss[i].setM3FUB(fVolumeFub*fArealCalculationFactor);	// Do "Arealsuppr�kning"
									vecTraktAss[i].setM3TO(fVolumeTo*fArealCalculationFactor);		// Do "Arealsuppr�kning"
									vecTraktAss[i].setValueM3FUB(0.0);
									vecTraktAss[i].setValueM3TO(fPriceTo*fVolumeTo*fArealCalculationFactor+(fVolumeTo*fArealCalculationFactor*tdata.getKrPerM3()));	// Do "Arealsuppr�kning"

								}

							}	// else if (recTraktMiscData.getTypeOf() == ID_RUNE_OLLAS_TYPEOF_PRICELIST_2)

						}	// if (tass.getPriceIn() == ASSORTMENT_TIMBER)
						else
						{
							//fVolumeFub = tdata.getM3FUB();
							//fVolumeTo = fVolumeFub * fM3FubToM3To;
							// Need to check if we have price set in m3fub or m3to
							if (fPriceFub > 0.0 && fPriceTo == 0.0)
							{
								vecTraktAss[i].setM3FUB(fVolumeFub*fArealCalculationFactor);	// Do "Arealsuppr�kning"
								vecTraktAss[i].setM3TO(0.0);	
								vecTraktAss[i].setValueM3FUB(fPriceFub*fVolumeFub*fArealCalculationFactor+(fVolumeFub*fArealCalculationFactor*tdata.getKrPerM3()));	// Do "Arealsuppr�kning"
								vecTraktAss[i].setValueM3TO(0.0);
							}
							else if (fPriceFub == 0.0 && fPriceTo > 0.0)
							{
								vecTraktAss[i].setM3TO(fVolumeFub*fM3FubToM3To*fArealCalculationFactor);	// Do "Arealsuppr�kning"
								vecTraktAss[i].setM3FUB(fVolumeFub*fArealCalculationFactor);	// Do "Arealsuppr�kning"
								vecTraktAss[i].setValueM3TO(fPriceTo*fVolumeFub*fM3FubToM3To*fArealCalculationFactor+(fVolumeFub*fM3FubToM3To*fArealCalculationFactor*tdata.getKrPerM3()));	// Do "Arealsuppr�kning"
								vecTraktAss[i].setValueM3FUB(0.0);	// Do "Arealsuppr�kning"
							}

						}

					}	// if (tass.getSpcID() == tdata.getTAssTraktDataID() && tass.getAssortName() == tdata.getTAssName())

				}	// for (UINT ii = 0;ii < m_vecTreeAssort.size();i++)
			}	// for (UINT i = 0;i < m_vecTraktAss.size();i++)
		} // if (m_vecTraktAss.size() > 0 && m_vecTreeAssort.size() > 0)
		else
		{
			// Reset data in vecTraktAss; 101208 p�d
			for (UINT i = 0;i < vecTraktAss.size();i++)
			{
				vecTraktAss[i].setM3FUB(0.0);
				vecTraktAss[i].setM3TO(0.0);
			}
		}

		// Save any changes to db (we always need to do this because values have been reset above)
		pDB->updTraktAss(vecTraktAss);

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//	END calculateAssortFromTreesToSpc
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		double fVolumeTrans = 0.0;
		// Remove all Transfers done for this trakt, for specie; 070614 p�d
		// Added condition "remove_trakt_trans", for removing trakt transfers; 070925 p�d
		if (remove_trakt_trans)
		{
			pDB->removeTraktTrans(spc_id /* OBS! SpcID = TraktDataID */,recTrakt.getTraktID());
		}
		// CALCULATE TRANSFERS; 070926 p�d
		// Get transfers done for this Trakt; 070926 p�d
		vecTraktTrans.clear();
		pDB->getTraktTrans(vecTraktTrans,recTrakt.getTraktID());
		if (do_transfers)
		{
			bool bFirstTime;
			int nAssortIndex = -1;
			// Make sure there's data to work with; 070925 p�d
			if (vecTraktTrans.size() > 0 && vecTraktAss.size() > 0 && vecTraktData1.size() > 0)
			{
				for (UINT i = 0;i < vecTraktData1.size();i++)
				{
					bFirstTime = true;
					for (UINT ii = 0;ii < vecTraktTrans.size();ii++)
					{
						if (vecTraktData1[i].getSpecieID() == vecTraktTrans[ii].getSpecieID())
						{
							// Check if there's percent for transfer; 070925 p�d
							if (vecTraktTrans[ii].getPercent() > 0.0)
							{
								nOnlyForTransfers = 0;
								// Try to find out if the assortment is only for Transfers; i.e. min.diam = 0.0 ; 100811 p�d
								if (vecAssortmentsPrl.size() > 0)
								{
									for (UINT iii = 0;iii < vecAssortmentsPrl.size();iii++)
									{
										CTransaction_assort rec = vecAssortmentsPrl[iii];
										if (rec.getAssortName().CompareNoCase(vecTraktTrans[ii].getToName()) == 0)
										{
											if (rec.getMinDiam() == 0.0) 
											{
												if (nAssortIndex != iii)
													bFirstTime = true;
												nOnlyForTransfers = 1;
												nAssortIndex = iii;
											}
											break;
										}
									}	// for (UINT iii = 0;iii < vecAssortmentsPrl.size();iii++)
								}	// if (vecAssortmentsPrl.size() > 0)
								
								calcTransfersByPercent(vecTraktData1[i].getSpecieID(),
																			 vecTraktTrans[ii].getFromName(),
																			 vecTraktTrans[ii].getToName(),
																			 vecTraktTrans[ii].getPercent(),
																			 vecTraktData1[i],
																			 vecTraktAss,
																			 nOnlyForTransfers,
																			 bFirstTime,
																			 &fVolumeTrans);
								bFirstTime = false;


								if (pDB)
								{
									//S.Format(L"VolumeTrans %f",fVolumeTrans);
									//UMMessageBox(S);
									vecTraktTrans[ii].setM3Trans(fVolumeTrans);	
									if (!pDB->addTraktTrans(vecTraktTrans[ii]))
										pDB->updTraktTrans(vecTraktTrans[ii]);
								}

							}	// if (vecTraktTrans[ii].getPercent() > 0.0)
						}	// if (vecTraktData[i].getSpecieID() == vecTraktTrans[ii].getSpecieID())

						// Update table "esti_trakt_trans_assort_table"; 101110 p�d
						/*
						if (pDB)
						{
							//S.Format(L"VolumeTrans %f",fVolumeTrans);
							//UMMessageBox(S);
							vecTraktTrans[ii].setM3Trans(fVolumeTrans);	
							if (!pDB->addTraktTrans(vecTraktTrans[ii]))
								pDB->updTraktTrans(vecTraktTrans[ii]);
						}*/

					}	// for (UINT ii = 0;ii < vecTraktTrans.size();ii++)

					// Readd Assortment information in m_vecTraktAss vector; 070613 p�d

					for (UINT iii = 0;iii < vecTraktAss.size();iii++)
					{
						pDB->updTraktAss(vecTraktAss[iii]);
					} // for (UINT iii = 0;iii < vecTraktAss.size();iii++)

				} // if (vecTraktTrans.size() > 0 && vecSetSpc.size() > 0)
			} // if (vecTraktTrans.size() > 0 && vecTraktAss.size() > 0 && vecTraktData.size() > 0)
		}	// if (do_transfers)
		// Do recalculation, for k3/m3 added; 080612 p�d
		// Commecnted out 2009-09-15 p�d, not sure we need to do this; 090915 p�d
		//pDB->recalcTraktAss_on_k3_per_m3(recTrakt.getTraktID(),nPriceSetIn);


		// Method for calculating "ROTNETTO"; 071011 p�d
//		calcRotpostForTrakt(bConnected, m_dbConnectionData, trakt.getTraktID(), &vecTraktAss, &vecSetSpc, &vecAllTraktData, &recTraktMiscData);
		calcRotpostForTrakt(bConnected, m_dbConnectionData, trakt.getTraktID(), &vecTraktAss, &vecSetSpc, &vecTraktData1, &recTraktMiscData);
	}	// if (vecTrees.size() > 0 && vecSetSpc.size() > 0)

	if (pDB != NULL)
		delete pDB;

	return nReturnValue;
}

/*
typedef struct _func_spc_struct
{
	int nSpcID;
	CString sSpcName;
	_func_spc_struct(void)
	{
		nSpcID = -1;
		sSpcName = _T("");
	}
	_func_spc_struct(int spc_id,LPCTSTR spc_name)
	{
		nSpcID = spc_id;
		sSpcName = (spc_name);
	}
} FUNC_SPC_STRUCT;

typedef std::vector<FUNC_SPC_STRUCT> vecFuncSpc;
*/

int CheckH25MinMax(DB_CONNECTION_DATA m_dbConnectionData, CTransaction_trakt recTrakt, CStringArray &logText)
{
	int nRet = TRUE;
	CDBHandleCalc *pDB = NULL;
	
	pDB = new CDBHandleCalc(m_dbConnectionData);
	pDB->getSampletreesOutsideH25MinMax(recTrakt.getTraktID(), &logText);
	if (logText.GetCount() > 0) nRet = FALSE;
	if( pDB ) delete pDB;

	return nRet;
}

