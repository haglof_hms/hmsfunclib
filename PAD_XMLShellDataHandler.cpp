#include "StdAfx.h"
#include "PAD_XMLShellDataHandler.h"



/////////////////////////////////////////////////////////////////////////////
// class XMLShellData

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

XMLShellData::XMLShellData()
{
	CHECK(CoInitialize(NULL));
  m_sModulePath	= _T("");
  pXMLDoc = NULL;
}

XMLShellData::XMLShellData(LPCTSTR module_path,HINSTANCE hinst)
{
	CHECK(CoInitialize(NULL));
  m_sModulePath	  = (module_path);
  pXMLDoc		  = NULL;
}

XMLShellData::~XMLShellData()
{
  CoUninitialize();
  m_sModulePath	  = _T("");
  pXMLDoc = NULL;
}

BOOL XMLShellData::load(LPCTSTR xml_fn)
{
  try
  {
		
		// Create MSXML2 DOM Document
		CHECK(pXMLDoc.CreateInstance("Msxml2.DOMDocument.3.0"));

		// Set parser property settings
		pXMLDoc->async = VARIANT_FALSE;

		// Validate during parsing
		pXMLDoc->validateOnParse = VARIANT_FALSE;

    // Load the sample XML file
    if (pXMLDoc->load(xml_fn) != VARIANT_TRUE)
	  return false;
  }
  catch(...)
  {
		UMMessageBox(_T("Error on XMLShellData::Start()"));
  }
  m_sModulePath	  = xml_fn;

	return true;
}

BOOL XMLShellData::getUserModules(CStringArray &um_list)
{
	TCHAR szNodeName[50];
	TCHAR szUserModule[50];
	// display the current node's name
	// simple for loop to get all children
	MSXML2::IXMLDOMNodeListPtr pNodeList = pXMLDoc->getElementsByTagName(USER_MODULE_TAG);
	if (pNodeList != NULL)
	{
		// Clear TCHAR items (NOT Anders Gustafsson dubugging session 070305); 070306 p�d
		// Just to make sure, due to VS2005 deprecated functions; 070306 p�d
		memset(szNodeName, 0, sizeof(szNodeName));
		memset(szUserModule, 0, sizeof(szUserModule));
		// Clear um_list; 051130 p�d
		um_list.RemoveAll();
		for (int i = 0;i < pNodeList->length;i++)
	  {
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[i];
			_tcscpy(szUserModule,pChild->text);
			_tcscpy(szNodeName,pChild->nodeName);
			if (_tcscmp(szUserModule,_T("")) != 0)
			{
				um_list.Add(szUserModule);
			}	// if (_tcscmp(szUserModule,"") != 0)
		}	// for (int i = 0;i < pNodeList->length;i++)
	} // if (pNodeList != NULL)
	
	return TRUE;
}

CString XMLShellData::str(LPCTSTR path)
{
  CString szValue = _T("");
  BSTR szElem = NULL;
  if (pXMLDoc != NULL)
  {
	MSXML2::IXMLDOMElementPtr pRootNode = pXMLDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pChild = pRootNode->selectSingleNode(path);
	if (pChild != NULL)
	{
	  szElem = _bstr_t(pChild->text);
	  szValue = szElem;
	  return szValue;
	}
  }
  return _T("n/a");

} 

BOOL XMLShellData::getReports(LPCTSTR module,int index,vecSTDReports &vec,LPCTSTR lang)
{
	CComBSTR bstrData;
	long lNumOf;
	int nSpcID = -1;
	TCHAR szModule[127];
	TCHAR szIndex[127];
	TCHAR szLang[127];
	int nIndex;

	TCHAR szStrID[127];
	int nStrID;
	TCHAR szCaption[127];
	TCHAR szFN[127];
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMElementPtr pRoot = pXMLDoc->documentElement;

	szModule[0] = '\0';
	szIndex[0] = '\0';
	szLang[0] = '\0';
	szStrID[0] = '\0';
	szCaption[0] = '\0';
	szFN[0] = '\0';

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_REPORTS));
	if (pNodeList)
	{	
		vec.clear();
		pNodeList->get_length( &lNumOf );
		for (int l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pParent = pNodeList->item[l];

			if (pParent)
			{
				pParent->get_attributes( &attributes1 );

				MSXML2::IXMLDOMNodePtr pChild_module = attributes1->getNamedItem(_bstr_t(NODE_MODULE_ATTR));
				if (pChild_module)
				{
					pChild_module->get_text( &bstrData );
					_tcscpy_s(szModule,127,_bstr_t(bstrData));
				}	// if (pChild_caption)

				MSXML2::IXMLDOMNodePtr pChild_index = attributes1->getNamedItem(_bstr_t(NODE_INDEX_ATTR));
				if (pChild_index)
				{
					pChild_index->get_text( &bstrData );
					_tcscpy_s(szIndex,127,_bstr_t(bstrData));
					nIndex = _tstoi(szIndex);
				}	// if (pChild_caption)

				MSXML2::IXMLDOMNodePtr pChild_index2 = attributes1->getNamedItem(_bstr_t(NODE_INDEX_LANG));
				if (pChild_index2)
				{
					pChild_index2->get_text( &bstrData );
					_tcscpy_s(szLang,127,_bstr_t(bstrData));
				}	// if (pChild_caption)

				if (_tcscmp(szModule,module) == 0 && (_tcscmp(szLang,lang) == 0 || _tcslen(szLang) == 0) && nIndex == index)
				{
					// If there's child nodes, loop an read attributes
					if (pParent->hasChildNodes())
					{
						for (MSXML2::IXMLDOMNodePtr pChild = pParent->firstChild;
								 NULL != pChild;
								 pChild = pChild->nextSibling)
						{
							pChild->get_attributes( &attributes2 );

							MSXML2::IXMLDOMNodePtr pChild_fn = attributes2->getNamedItem(_bstr_t(NODE_FILENAME_ATTR));
							if (pChild_fn)
							{
								pChild_fn->get_text( &bstrData );
								_tcscpy_s(szFN,127,_bstr_t(bstrData));
							}	// if (pChild_caption)

							MSXML2::IXMLDOMNodePtr pChild_strid = attributes2->getNamedItem(_bstr_t(NODE_STRID_ATTR));
							if (pChild_strid)
							{
								pChild_strid->get_text( &bstrData );
								_tcscpy_s(szStrID,127,_bstr_t(bstrData));
								nStrID = _tstoi(szStrID);
							}	// if (pChild_add_to)

							MSXML2::IXMLDOMNodePtr pChild_caption = attributes2->getNamedItem(_bstr_t(NODE_CAPTION_ATTR));
							if (pChild_caption)
							{
								pChild_caption->get_text( &bstrData );
								_tcscpy_s(szCaption,127,_bstr_t(bstrData));
							}	// if (pChild_caption)

							vec.push_back(CSTD_Reports(szFN,nStrID,szCaption));
						}
					}
				}	// if (_tcscmp(szAddTo,add_to) == 0)
			}	// if (pChild)

		}	// for (int l = 0;l < lNumOf;l++)

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

