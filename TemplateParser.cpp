#include "StdAfx.h"
#include "TemplateParser.h"
#include "pad_hms_miscfunc.h"
#include <string.h>

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


TemplateParser::TemplateParser(void)
{
	if(AfxGetApp() != NULL)
		CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

TemplateParser::~TemplateParser()
{
	pDomDoc = NULL;;	
	if(AfxGetApp() != NULL)
		CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL TemplateParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL TemplateParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL TemplateParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
void TemplateParser::setAttr(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name,LPCTSTR value)
{
	CComBSTR bstrData(value);
	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));
	if (pAttr)
	{
		pAttr->put_text( bstrData );
		pAttr = NULL;;
		::SysFreeString(bstrData);
	}	// if (pAttr)
}

CString TemplateParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[1024];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	szData[0] = '\0';
	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcsncpy_s(szData,_bstr_t(bstrData),1000);
		pAttr = NULL;;
		::SysFreeString(bstrData);
	}	// if (pAttr)

	return szData;
}

double TemplateParser::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue = 0.0;
	szData[0] = '\0';

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = localeStrToDbl(szData);
		pAttr = NULL;;
		::SysFreeString(bstrData);
	}	// if (pAttr)

	return fValue;
}

int TemplateParser::getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	int nValue;
	szData[0] = '\0';

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		nValue = _tstoi(szData);
		pAttr = NULL;;
		::SysFreeString(bstrData);
	}	// if (pAttr)

	return nValue;
}

BOOL TemplateParser::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)   
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;
	szData[0] = '\0';

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
		pAttr = NULL;;
		::SysFreeString(bstrData);
	}	// if (pAttr)

	return bValue;
}


// Public

//-------------------------------------------------------------------
// Methods for reading template header infromation
BOOL TemplateParser::getTemplateName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_NAME);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::setTemplateName(LPCTSTR data)
{
	CComBSTR bstrData(data);
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_NAME);
	if (pNode)
	{	
		pNode->put_text(bstrData);
		pNode = NULL;;
		::SysFreeString(bstrData);
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateDoneBy(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_DONE_BY);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_DATE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateNotes(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_NOTES);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		pNode = NULL;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}


BOOL TemplateParser::getTemplateExchange(int *id,LPTSTR func_name)
{
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_EXCHANGE);
	if (pNode)
	{	
		pNode->get_attributes( &attributes );
		if (attributes != NULL)
		{
			*id = getAttrInt(attributes,_T("id"));
			_tcscpy(func_name,getAttrText(attributes,_T("name")));
		}
		pNode = NULL;;
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplatePricelist(int *id,LPTSTR func_name)
{
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_PRICELIST);
	if (pNode)
	{	
		pNode->get_attributes( &attributes );
		if (attributes != NULL)
		{
			*id = getAttrInt(attributes,_T("id"));
			_tcscpy(func_name,getAttrText(attributes,_T("name")));
		}
		pNode = NULL;;
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::setTemplatePricelist(int id,LPCTSTR func_name)
{
	CString sData;
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_PRICELIST);
	if (pNode)
	{	
		pNode->get_attributes( &attributes );
		if (attributes != NULL)
		{
			sData.Format(_T("%d"),id);
			setAttr(attributes,_T("id"),sData);
			setAttr(attributes,_T("name"),func_name);
		}
		pNode = NULL;;
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplatePricelistTypeOf(int *type_of)
{
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_PRICELIST);
	if (pNode)
	{	
		pNode->get_attributes( &attributes );
		if (attributes != NULL)
		{
			*type_of = getAttrInt(attributes,_T("type_of"));
		}
		pNode = NULL;;

		return TRUE;
	}

	return FALSE;
}


BOOL TemplateParser::getTemplateDCLS(double *dcls)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_DCLS);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*dcls = localeStrToDbl(bstrData);
		
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateHgtOverSea(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_HGT_OVER_SEA);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateLatitude(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_LATITUDE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateLongitude(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_LONGITUDE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateGrowthArea(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_GROWTH_AREA);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateSI_H100(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_SI_H100);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		pNode = NULL;;
		::SysFreeString(bstrData);

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getTemplateCostsTmpl(int *id,LPTSTR costs_tmpl_name)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMNamedNodeMap  *attributes = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_COSTS_TMPL);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(costs_tmpl_name,_bstr_t(bstrData));
		pNode->get_attributes( &attributes );		
		if (attributes != NULL)
		{
			MSXML2::IXMLDOMNodePtr pChild_costs_tmpl_name = attributes->getNamedItem(_bstr_t("id"));
			if (pChild_costs_tmpl_name)
			{
				pChild_costs_tmpl_name->get_text( &bstrData );
				*id = _tstoi(_bstr_t(bstrData));
				::SysFreeString(bstrData);

			}	// if (pChild_spcid)
		}
		pNode = NULL;;

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::setTemplateCostsTmpl(int id,LPCTSTR costs_tmpl_name)
{
	CString sData;
	CComBSTR bstrData(costs_tmpl_name);
	MSXML2::IXMLDOMNamedNodeMap  *attributes = NULL;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_COSTS_TMPL);
	if (pNode)
	{	
		pNode->put_text(bstrData);
		pNode->get_attributes( &attributes );		
		if (attributes != NULL)
		{
			sData.Format(_T("%d"),id);
			setAttr(attributes,_T("id"),sData);
		}
		pNode = NULL;;

		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getIsAtCoast(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_AT_COAST);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
	
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getIsSouthEast(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_SOUTH_EAST);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getIsRegion5(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_REGION_5);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getPartOfPlot(int *value)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_PART_OF_PLOT);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*value = _tstoi(_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
		
		return TRUE;
	}

	return FALSE;
}

BOOL TemplateParser::getSIH100_Pine(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_TEMPLATE_SI_H100_PINE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		pNode = NULL;;
		::SysFreeString(bstrData);
		
		return TRUE;
	}

	return FALSE;
}


//-------------------------------------------------------------------
// Methods for reading template header infromation
BOOL TemplateParser::getSpeciesInTemplateFile(vecTransactionSpecies &vec)
{
	CComBSTR bstrData;
	long lNumOf;
	int nSpcID = -1;
	int nP30SpcID = 1;
	TCHAR szSpcName[127];
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_TEMPLATE_SPECIE));
	if (pNodeList)
	{	
		vec.clear();
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );
				MSXML2::IXMLDOMNodePtr pChild_spcid = attributes1->getNamedItem(_bstr_t("id"));
				if (pChild_spcid)
				{
					pChild_spcid->get_text( &bstrData );
					nSpcID = _tstoi(_bstr_t(bstrData));
					::SysFreeString(bstrData);

				}	// if (pChild_spcid)
				MSXML2::IXMLDOMNodePtr pChild_spcname = attributes1->getNamedItem(_bstr_t("name"));
				if (pChild_spcname)
				{
					pChild_spcname->get_text( &bstrData );
					_tcscpy(szSpcName,_bstr_t(bstrData));
				::SysFreeString(bstrData);

				}	// if (pChild_spcname)

				MSXML2::IXMLDOMNodePtr pChild_P30spcid = attributes1->getNamedItem(_bstr_t("p30specid"));
				if (pChild_P30spcid)
				{
					pChild_P30spcid->get_text( &bstrData );
					nP30SpcID = _tstoi(_bstr_t(bstrData));
					::SysFreeString(bstrData);

				}	// if (pChild_spcid)

				


				vec.push_back(CTransaction_species(l+1,nSpcID,nP30SpcID,szSpcName));
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)
		pNodeList = NULL;;

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

BOOL TemplateParser::getFunctionsForSpecie(int spc_id,vecUCFunctionList &vec,double *m3fub_m3to,double *m3sk_m3ub_const)
{
	CString S;
	UCFunctionList recFuncList;
	long lNumOfSpecies;
	int nSpcID;
	CComBSTR bstrData;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNodePtr pParentNode = NULL;
	MSXML2::IXMLDOMNodePtr pNode = NULL;
				
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_TEMPLATE_SPECIE));
	if (pSubNodeList)
	{
		vec.clear();
		pSubNodeList->get_length( &lNumOfSpecies );
		for (int l1 = 0;l1 < lNumOfSpecies;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
					pSubNode->get_attributes( &attributes1 );
					nSpcID = getAttrInt(attributes1,_T("id"));

					if (nSpcID == spc_id)
					{
						if (pSubNode->hasChildNodes())
						{
							// Collect attribute data for Height functions in Tamplate
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_FUNC_HGT));
							if (pNode != NULL)
							{
								pNode->get_attributes( &attributes2 );									
								recFuncList = UCFunctionList(getAttrInt(attributes2,NODE_TEMPLATE_FUNC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_SPC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_INDEX),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_SPC_NAME),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_TYPE),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_AREA),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_DESC));
								vec.push_back(recFuncList);
								pNode = NULL;;
							}	// if (pNode != NULL)

							// Collect attribute data for Volume functions in Tamplate
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_FUNC_VOL));
							if (pNode != NULL)
							{
											
								pNode->get_attributes( &attributes2 );
								recFuncList = UCFunctionList(getAttrInt(attributes2,NODE_TEMPLATE_FUNC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_SPC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_INDEX),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_SPC_NAME),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_TYPE),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_AREA),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_DESC));
								vec.push_back(recFuncList);
								pNode = NULL;;
							}	// if (pNode != NULL)

							// Collect attribute data for Bark functions in Tamplate
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_FUNC_BARK));
							if (pNode != NULL)
							{
											
								pNode->get_attributes( &attributes2 );
								recFuncList = UCFunctionList(getAttrInt(attributes2,NODE_TEMPLATE_FUNC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_SPC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_INDEX),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_SPC_NAME),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_TYPE),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_AREA),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_DESC));
								vec.push_back(recFuncList);
								pNode = NULL;;
							}	// if (pNode != NULL)
							// Collect attribute data for Volume functions in Tamplate
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_FUNC_VOL_UB));
							if (pNode != NULL)
							{
											
								pNode->get_attributes( &attributes2 );
								recFuncList = UCFunctionList(getAttrInt(attributes2,NODE_TEMPLATE_FUNC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_SPC_ID),
																						 getAttrInt(attributes2,NODE_TEMPLATE_FUNC_INDEX),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_SPC_NAME),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_TYPE),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_AREA),
																						 getAttrText(attributes2,NODE_TEMPLATE_FUNC_DESC));
								vec.push_back(recFuncList);
								*m3sk_m3ub_const = getAttrDouble(attributes2,NODE_TEMPLATE_FUNC_M3SK_M3UB);
								pNode = NULL;;
							}	// if (pNode != NULL)
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_FUNC_M3FUB_M3TO));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);
								*m3fub_m3to = localeStrToDbl(bstrData);
								pNode = NULL;;
							}
						}	// if (pSubNode->hasChildNodes())
					}	// if (nSpcID == spc_id)
					pSubNode = NULL;;
			}	// if (pSubNode)
		}	// for (int l1 = 0;l1 < lNumOfAssort;l1++)

		pSubNodeList = NULL;;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;

}


BOOL TemplateParser::getMiscDataForSpecie(int spc_id,int *qdesc_index,LPTSTR qdesc,double *v1,double *v2,double *v3,int *v4,int *v5,double *v6)
{
	CString S;
	CComBSTR bstrData;
	long lNumOfSpecies;
	int nSpcID;
	TCHAR szQDesc[127];
	MSXML2::IXMLDOMNamedNodeMap  *attributes = NULL;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1 = NULL;
	MSXML2::IXMLDOMNodePtr pNode = NULL;
				
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_TEMPLATE_SPECIE));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfSpecies );
		for (int l1 = 0;l1 < lNumOfSpecies;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
					pSubNode->get_attributes( &attributes );
					nSpcID = getAttrInt(attributes,_T("id"));

					if (nSpcID == spc_id)
					{
						if (pSubNode->hasChildNodes())
						{
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_QUAL_DESC));
							if (pNode != NULL)
							{
								pNode->get_attributes( &attributes1 );
								if (attributes1)
								{							
									*qdesc_index = getAttrInt(attributes1,_T("id"));
									_tcscpy(szQDesc,getAttrText(attributes1,_T("qdesc_name")));
									_tcscpy(qdesc,szQDesc);
								}
								else
								{
									*qdesc_index = -1;
									_tcscpy(qdesc,_T(""));
								}
								pNode = NULL;;
							}	// if (pNode != NULL)
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_TRANSP_DIST1));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);
								*v4 = _tstoi(_bstr_t(bstrData));
								pNode = NULL;;
								::SysFreeString(bstrData);

							}	// if (pNode != NULL)

							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_TRANSP_DIST2));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);
								*v5 = _tstoi(_bstr_t(bstrData));
								pNode = NULL;
								::SysFreeString(bstrData);

							}	// if (pNode != NULL)

							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_M3SK_TO_M3FUB));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);

								if(AfxGetApp() != NULL)
								{
									double fValue = _tstof(_bstr_t(bstrData));
									CString sValue,sDecPnt(getLocaleDecimalPoint());
									sValue.Format(L"%f",fValue);
									if (sValue.Find(L",") && sDecPnt == L".")	sValue.Replace(L",",sDecPnt);
									else if (sValue.Find(L".") && sDecPnt == L",")	sValue.Replace(L".",sDecPnt);

									*v1 = _tstof(sValue);
								}
								else
								{
									*v1 = localeStrToDbl(bstrData);
								}								pNode = NULL;
								::SysFreeString(bstrData);

							}	// if (pNode != NULL)
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_M3FUB_TO_M3TO));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);
								*v2 = localeStrToDbl(bstrData);
								pNode = NULL;
								::SysFreeString(bstrData);
							}	// if (pNode != NULL)
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_H25));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);
								*v3 = localeStrToDbl(bstrData);
								pNode = NULL;
								::SysFreeString(bstrData);

							}	// if (pNode != NULL)
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_GCROWN));
							if (pNode != NULL)
							{
								pNode->get_text(&bstrData);
								*v6 = localeStrToDbl(bstrData);
								pNode = NULL;
								::SysFreeString(bstrData);

							}	// if (pNode != NULL)
						}	// if (pSubNode->hasChildNodes())
					}	// if (nSpcID == spc_id)
					pSubNode = NULL;;
			}	// if (pSubNode)
		}	// for (int l1 = 0;l1 < lNumOfAssort;l1++)

		pSubNodeList = NULL;;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL TemplateParser::getGrotDataForSpecie(int spc_id,int *index,double *percent,double *price,double *cost)
{
	CComBSTR bstrData;
	long lNumOfSpecies;
	int nSpcID;
	MSXML2::IXMLDOMNamedNodeMap  *attributes = NULL;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1 = NULL;
	MSXML2::IXMLDOMNodePtr pNode = NULL;
				
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_TEMPLATE_SPECIE));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfSpecies );
		for (int l1 = 0;l1 < lNumOfSpecies;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
					pSubNode->get_attributes( &attributes );
					nSpcID = getAttrInt(attributes,_T("id"));

					if (nSpcID == spc_id)
					{
						if (pSubNode->hasChildNodes())
						{
							pNode = pSubNode->selectSingleNode(_bstr_t(NODE_TEMPLATE_GROT));
							if (pNode != NULL)
							{
								pNode->get_attributes( &attributes1 );
								if (attributes1)
								{							
									*index = getAttrInt(attributes1,_T("id"));
									*percent = getAttrDouble(attributes1,_T("percent"));
									*price = getAttrDouble(attributes1,_T("price"));
									*cost = getAttrDouble(attributes1,_T("cost"));
								}
								else
								{
									*index = 0;
									*percent = 0.0;
									*price = 0.0;
									*cost = 0.0;
								}
								pNode = NULL;;
							}	// if (pNode != NULL)
						}	// if (pSubNode->hasChildNodes())
					}	// if (nSpcID == spc_id)
					pSubNode = NULL;;
			}	// if (pSubNode)
		}	// for (int l1 = 0;l1 < lNumOfAssort;l1++)

		pSubNodeList = NULL;;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL TemplateParser::getTransfersForSpecie(int spc_id,vecTransactionTemplateTransfers &vec)
{
	long lNumOfSpecies;
	int nSpcID;

	vec.clear();
	MSXML2::IXMLDOMNamedNodeMap  *attributes = NULL;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1 = NULL;
	MSXML2::IXMLDOMNodePtr pChildNode = NULL;
	MSXML2::IXMLDOMNodePtr pChildNode1 = NULL;
				
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_TEMPLATE_SPECIE));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfSpecies );
		for (int l1 = 0;l1 < lNumOfSpecies;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				nSpcID = getAttrInt(attributes,_T("id"));
				if (nSpcID == spc_id)
				{
					pChildNode1 = pSubNode->selectSingleNode((NODE_TEMPLATE_TRANSFERS));
					if (pChildNode1 != NULL)
					{
						if (pChildNode1->hasChildNodes())
						{
							for (pChildNode = pChildNode1->firstChild;
									 pChildNode != NULL;
									 pChildNode = pChildNode->nextSibling)
							{
								pChildNode->get_attributes( &attributes1 );
								if (attributes1 != NULL)
								{
									vec.push_back(CTransaction_template_transfers(getAttrInt(attributes1,(NODE_TEMPLATE_TRANSFERS_FROM_ID)),
																																getAttrText(attributes1,(NODE_TEMPLATE_TRANSFERS_FROM)),
																																getAttrInt(attributes1,(NODE_TEMPLATE_TRANSFERS_TO_ID)),
																																getAttrText(attributes1,(NODE_TEMPLATE_TRANSFERS_TO)),
																																getAttrDouble(attributes1,(NODE_TEMPLATE_TRANSFERS_FUB)),
																																getAttrDouble(attributes1,(NODE_TEMPLATE_TRANSFERS_PERC)),
																																_T("")));
								}	// if (attributes1 != NULL)
							}	// for (pChildNode = pChildNode1->firstChild;pChildNode != NULL;pChildNode = pChildNode->nextSibling)
						}	// if (pChildNode1->hasChildNodes())
						pChildNode1 = NULL;;
					}	// if (pChildNode1 != NULL)
				}	// if (nSpcID == spc_id)
				pSubNode = NULL;;
			}	// if (pSubNode)
		}	// for (int l1 = 0;l1 < lNumOfAssort;l1++)

		pSubNodeList = NULL;;

		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL TemplateParser::getObjTmplP30(vecObjectTemplate_p30_table &vec,CString& name,CString& done_by,CString& notes)
{
	int nSpcID;
	CString sSpcName;
	double fPrice;
	double fPriceRel;
	long lNumOfRows = 0;
	// Make sure the vector is empty; 080213 p�d
	vec.clear();
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	// Selet type set 1 = Table 2 = Break/Factor; 090529 p�d
	MSXML2::IXMLDOMNodePtr pNode1 = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30_SET));
	if (pNode1)
	{	
		pNode1->get_attributes(&attributes);
		name = getAttrText(attributes,NODE_OBJ_TEMPLATE_P30_ATTR5);
		done_by = getAttrText(attributes,NODE_OBJ_TEMPLATE_P30_ATTR6);
		notes = getAttrText(attributes,NODE_OBJ_TEMPLATE_P30_ATTR7);
		pNode1 = NULL;
	}

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_TEMPLATE_P30_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfRows );
		for (long l1 = 0;l1 < lNumOfRows;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				nSpcID = getAttrInt(attributes,(NODE_OBJ_TEMPLATE_P30_ATTR1));
				sSpcName = getAttrText(attributes,(NODE_OBJ_TEMPLATE_P30_ATTR2));
				fPrice = getAttrDouble(attributes,(NODE_OBJ_TEMPLATE_P30_ATTR3));
				fPriceRel = getAttrDouble(attributes,(NODE_OBJ_TEMPLATE_P30_ATTR4));
				vec.push_back(CObjectTemplate_p30_table(nSpcID,sSpcName,fPrice,fPriceRel));
			}	// if (pSubNode != NULL)

		}	// for (long l1 = 0;l1 < lNumOfRows;l1++)
		pSubNodeList = NULL;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}
/*
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_WC					= _T("<obj_tmpl_p30nn/specie/*>");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_SPC_NAME		= _T("name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_SPC_ID			= _T("id");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_SPC_SI			= _T("si");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_FROM		= _T("from");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_TO			= _T("to");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_P30		= _T("p30");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_PREL		= _T("prel");
*/
BOOL TemplateParser::getObjTmplP30_nn(vecObjectTemplate_p30_nn_table &vec,CString &area_name,int *area_index)
{
	CString S1,S2;
	// Spc data ....
	int nSpcID;
	CString sSpcName;
	CString sSI;	// Abbrevation of specie T,G or B; 090416 p�d
	CString sNotes;
	// Data .....
	int nFrom;
	int nTo;
	int nP30;
	double fPriceRel;
	long lNumOfRows = 0;
	long lNumOfRows1 = 0;
	// Make sure the vector is empty; 080213 p�d
	vec.clear();
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMNamedNodeMap  *attributes2;
	MSXML2::IXMLDOMNamedNodeMap  *attributes3;
	MSXML2::IXMLDOMNodePtr pChild = NULL;

	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;
	//----------------------------------------------------------------------------------------------------
	MSXML2::IXMLDOMNodePtr pAreaChild = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_AREA));
	pAreaChild->get_attributes( &attributes2 );

	area_name = getAttrText(attributes2,(NODE_OBJ_TEMPLATE_P30NN_AREA_NAME));
	*area_index = getAttrInt(attributes2,(NODE_OBJ_TEMPLATE_P30NN_AREA_IDX));
	//----------------------------------------------------------------------------------------------------

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfRows );
		for (long l1 = 0;l1 < lNumOfRows;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				nSpcID = getAttrInt(attributes,(NODE_OBJ_TEMPLATE_P30NN_SPC_ID));
				sSpcName = getAttrText(attributes,(NODE_OBJ_TEMPLATE_P30NN_SPC_NAME));
				sSI = getAttrText(attributes,(NODE_OBJ_TEMPLATE_P30NN_SPC_SI));

				for (pChild = pSubNode->firstChild;
						 NULL != pChild;
						 pChild = pChild->nextSibling)
				{
					if (pChild != NULL)
					{
						pChild->get_attributes( &attributes1 );

						nFrom = getAttrInt(attributes1,(NODE_OBJ_TEMPLATE_P30NN_DATA_FROM));
						nTo = getAttrInt(attributes1,(NODE_OBJ_TEMPLATE_P30NN_DATA_TO));
						nP30 = getAttrInt(attributes1,(NODE_OBJ_TEMPLATE_P30NN_DATA_P30));
						fPriceRel = getAttrDouble(attributes1,(NODE_OBJ_TEMPLATE_P30NN_DATA_PREL));
						vec.push_back(CObjectTemplate_p30_nn_table(nSpcID,sSpcName,sSI,*area_index,nFrom,nTo,nP30,fPriceRel));
					}	// if (pChild != NULL)
				}	// for (pChild = pSubNode->firstChild;
				pSubNode = NULL;
			}	// if (pSubNode != NULL)

		}	// for (long l1 = 0;l1 < lNumOfRows;l1++)
		
		pSubNodeList = NULL;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL TemplateParser::getObjTmplP30_nn_notes(CString &notes)
{
	MSXML2::IXMLDOMNamedNodeMap  *attr;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;
	//----------------------------------------------------------------------------------------------------
	MSXML2::IXMLDOMNodePtr pNotesChild = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_NOTES));
	if (pNotesChild)
	{
		pNotesChild->get_attributes( &attr );
		notes = getAttrText(attr,(NODE_OBJ_TEMPLATE_P30NN_NOTES_VALUE));
	}
	else
		notes = L"";

	pNotesChild = NULL;
	//----------------------------------------------------------------------------------------------------
	return TRUE;
}

BOOL TemplateParser::getObjTmplP30_nn_name(CString &name)
{
	MSXML2::IXMLDOMNamedNodeMap  *attr;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;
	//----------------------------------------------------------------------------------------------------
	MSXML2::IXMLDOMNodePtr pNotesChild = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_NAME));
	if (pNotesChild)
	{
		pNotesChild->get_attributes( &attr );
		name = getAttrText(attr,(NODE_OBJ_TEMPLATE_P30NN_NAME_VALUE));
	}
	else
		name = L"";

	pNotesChild = NULL;
	//----------------------------------------------------------------------------------------------------
	return TRUE;
}

BOOL TemplateParser::getObjTmplP30_nn_done_by(CString &done_by)
{
	MSXML2::IXMLDOMNamedNodeMap  *attr;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;
	//----------------------------------------------------------------------------------------------------
	MSXML2::IXMLDOMNodePtr pNotesChild = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_DONE_BY));
	if (pNotesChild)
	{
		pNotesChild->get_attributes( &attr );
		done_by = getAttrText(attr,(NODE_OBJ_TEMPLATE_P30NN_DONE_BY_VALUE));
	}
	else
		done_by = L"";
	
	pNotesChild = NULL;
	//----------------------------------------------------------------------------------------------------
	return TRUE;
}

BOOL TemplateParser::getObjTmplP30_nn_related_species(mapShortAndBool &pine_map,mapShortAndBool &spruce_map)
{
	CString sTmp,S;
	int nSpcID = -1;
	long lLength = 0;
	MSXML2::IXMLDOMNamedNodeMap  *attrPine = NULL;
	MSXML2::IXMLDOMNamedNodeMap  *attrSpruce = NULL;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	// Handle Pine related species; 090609 p�d
	MSXML2::IXMLDOMNodePtr pChildPine = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_WC2));
	if (pChildPine)
	{
		pChildPine->get_attributes(&attrPine);
		if (attrPine)
		{
			attrPine->get_length(&lLength);
			for (long i = 0;i < lLength;i++)
			{
				sTmp.Format(_T("spc%d"),i+1);
				nSpcID = getAttrInt(attrPine,sTmp);
				pine_map[nSpcID] = true;
			}
		}
		pChildPine = NULL;
	}

	// Handle Spruce related species; 090609 p�d
	MSXML2::IXMLDOMNodePtr pChildSpruce = pElement->selectSingleNode(_bstr_t(NODE_OBJ_TEMPLATE_P30NN_WC3));
	if (pChildSpruce)
	{
		pChildSpruce->get_attributes(&attrSpruce);
		if (attrSpruce)
		{
			attrSpruce->get_length(&lLength);
			for (long i = 0;i < lLength;i++)
			{
				sTmp.Format(_T("spc%d"),i+1);
				nSpcID = getAttrInt(attrSpruce,sTmp);
				spruce_map[nSpcID] = true;
			}
		}
		pChildSpruce = NULL;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL TemplateParser::getObjTmplHCost(int *type_set,double *break_value,double *factor,vecObjectTemplate_hcost_table &vec,CString& name,CString& done_by)
{
	double fM3Sk;
	double fPriceM3Sk;
	double fSumPrice;
	double fBaseComp;
	long lNumOfRows = 0;
	CComBSTR bstrData;
	int nTypeSet;
	double fBreakValue;
	double fFactor;
	
	// Make sure the vector is empty; 080213 p�d
	vec.clear();
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	// Selet type set 1 = Table 2 = Break/Factor; 090529 p�d
	MSXML2::IXMLDOMNodePtr pNode1 = pElement->selectSingleNode(_bstr_t(NODE_HCOST_TMPL_TYPE_SET));
	if (pNode1)
	{	
		pNode1->get_attributes(&attributes);
		nTypeSet = getAttrInt(attributes,NODE_HCOST_TMPL_TYPE_SET_ATTR1);
		*type_set = nTypeSet;
		name = getAttrText(attributes,NODE_HCOST_TMPL_TYPE_SET_ATTR2);
		done_by = getAttrText(attributes,NODE_HCOST_TMPL_TYPE_SET_ATTR3);
		pNode1 = NULL;
	}

	// Selet break/factor; 090529 p�d
	MSXML2::IXMLDOMNodePtr pNode2 = pElement->selectSingleNode(_bstr_t(NODE_HCOST_TMPL_BREAK_FACTOR));
	if (pNode2)
	{	
		pNode2->get_attributes(&attributes);
		fBreakValue = getAttrDouble(attributes,NODE_HCOST_TMPL_BREAK_FAC_ATTR1);
		fFactor = getAttrDouble(attributes,NODE_HCOST_TMPL_BREAK_FAC_ATTR2);
		*break_value = fBreakValue;
		*factor = fFactor;
		pNode2 = NULL;
	}

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_TEMPLATE_HCOST_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfRows );
		for (long l1 = 0;l1 < lNumOfRows;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				fM3Sk = getAttrDouble(attributes,(NODE_OBJ_TEMPLATE_HCOST_ATTR1));
				fPriceM3Sk = getAttrDouble(attributes,(NODE_OBJ_TEMPLATE_HCOST_ATTR2));
				fSumPrice = getAttrDouble(attributes,(NODE_OBJ_TEMPLATE_HCOST_ATTR3));
				fBaseComp = getAttrDouble(attributes,(NODE_OBJ_TEMPLATE_HCOST_ATTR4));
				vec.push_back(CObjectTemplate_hcost_table(fM3Sk,fPriceM3Sk,fBaseComp,fSumPrice));
				pSubNode = NULL;
			}	// if (pSubNode != NULL)

		}	// for (long l1 = 0;l1 < lNumOfRows;l1++)
		pSubNodeList = NULL;
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

//-------------------------------------------------------------------
// 
BOOL TemplateParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		pRoot = NULL;
		::SysFreeString(bstrBuffer);

		return TRUE;
	}

	return FALSE;
}



JustSaveParser::JustSaveParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

JustSaveParser::~JustSaveParser()
{
	pDomDoc = NULL;
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL JustSaveParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL JustSaveParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL JustSaveParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}




//-------------------------------------------------------------------
// Parser based on xmlLite; 091221 p�d

xmlliteTemplateParser::xmlliteTemplateParser(void)
{
}

xmlliteTemplateParser::~xmlliteTemplateParser()
{
}


// Public

//-------------------------------------------------------------------
// Methods for reading template header infromation
BOOL xmlliteTemplateParser::getTemplateName(LPTSTR data)
{
	return getNodeValue(L"template_name",data);
}


BOOL xmlliteTemplateParser::getTemplateDoneBy(LPTSTR data)
{
	return getNodeValue(L"template_done_by",data);
}

BOOL xmlliteTemplateParser::getTemplateDate(LPTSTR data)
{
	return getNodeValue(L"template_date",data);
}


BOOL xmlliteTemplateParser::getTemplateExchange(int *id,LPTSTR func_name)
{
	int nAttrCounter = 0;
	int nID;
	TCHAR szFuncName[127];

	if (!gotoNodeElement(L"template_exchange")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nID); break;
					case 1 : getAttrValue(szFuncName); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
		}	
	} while(gotoNextNodeElement());

	*id = nID;
	_tcscpy(func_name,szFuncName);

	return TRUE;
}

BOOL xmlliteTemplateParser::getTemplatePricelist(int *id,LPTSTR func_name)
{
	int nAttrCounter = 0;
	int nID;
	TCHAR szFuncName[127];

	if (!gotoNodeElement(L"template_pricelist")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nID); break;
					case 1 : getAttrValue(szFuncName); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
		}	
	} while(gotoNextNodeElement());

	*id = nID;
	_tcscpy(func_name,szFuncName);

	return TRUE;
}

BOOL xmlliteTemplateParser::getTemplatePricelistTypeOf(int *type_of)
{
	int nAttrCounter = 0;
	int nTypeOf;

	if (!gotoNodeElement(L"template_pricelist")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nTypeOf); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
		}	
	} while(gotoNextNodeElement());

	*type_of = nTypeOf;

	return TRUE;
}

BOOL xmlliteTemplateParser::getTemplateDCLS(double *dcls)
{
	return getNodeValue(L"template_dcls",dcls);
}

BOOL xmlliteTemplateParser::getTemplateHgtOverSea(int *value)
{
	return getNodeValue(L"template_hgt_over_sea",value);
}

BOOL xmlliteTemplateParser::getTemplateLatitude(int *value)
{
	return getNodeValue(L"template_latitude",value);
}

BOOL xmlliteTemplateParser::getTemplateGrowthArea(LPTSTR data)
{
	return getNodeValue(L"template_growth_area",data);
}

BOOL xmlliteTemplateParser::getTemplateSI_H100(LPTSTR data)
{
	return getNodeValue(L"template_si_h100",data);
}

BOOL xmlliteTemplateParser::getTemplateCostsTmpl(int *id,LPTSTR costs_tmpl_name)
{
	if (!getNodeValue(L"template_costs_tmpl",costs_tmpl_name)) return FALSE;

	int nAttrCounter = 0;
	int nID;

	if (!gotoNodeElement(L"template_costs_tmpl")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nID); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
		}	
	} while(gotoNextNodeElement());

	*id = nID;

	return TRUE;
}

BOOL xmlliteTemplateParser::getIsAtCoast(int *value)
{
	return getNodeValue(L"template_at_coast",value);
}

BOOL xmlliteTemplateParser::getIsSouthEast(int *value)
{
	return getNodeValue(L"template_south_east",value);
}

BOOL xmlliteTemplateParser::getIsRegion5(int *value)
{
	return getNodeValue(L"template_region_5",value);
}

BOOL xmlliteTemplateParser::getPartOfPlot(int *value)
{
	return getNodeValue(L"template_part_of_plot",value);
}

BOOL xmlliteTemplateParser::getSIH100_Pine(LPTSTR data)
{
	return getNodeValue(L"template_si_h100_pine",data);
}

//-------------------------------------------------------------------
// Methods for reading template header infromation
BOOL xmlliteTemplateParser::getSpeciesInTemplateFile(vecTransactionSpecies &vec)
{
	CString sSpcName;
	int nSpcID;
	int nP30SpcID=1;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	XmlNodeType nodeType = XmlNodeType_None;
	int nCounter = 1;

	readerRewind();

	// Read off xml file
	while(getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"template_specie") == 0)	// Search for node; 091215 p�d
			{
				int nAttrCount = 0;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
						nAttrCount++;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					if (_tcscmp(pwszLocalAttrName,L"name") == 0)
					{
						sSpcName = pwszValue;
						nAttrCount++;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					if (_tcscmp(pwszLocalAttrName,L"P30specid") == 0)
					{
						nP30SpcID = _tstoi(pwszValue);
						nAttrCount++;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					else
						nP30SpcID=1;

					if (nAttrCount == 2 || nAttrCount == 3)
					{
						vec.push_back(CTransaction_species(nCounter,nSpcID,nP30SpcID,sSpcName));
						nCounter++;
					}
				}	// for (HRESULT result = pReader->MoveToFirstAttribute();S_OK == result;result = pReader->MoveToNextAttribute())
			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 
		}	// if( nodeType == XmlNodeType_Element )
	}	// while( pReader->Read(&nodeType) == S_OK )

	return TRUE;
}

BOOL xmlliteTemplateParser::getFunctionsForSpecie(int spc_id,vecUCFunctionList &vec,double *m3fub_m3to)
{
	return FALSE;
}


BOOL xmlliteTemplateParser::getMiscDataForSpecie(int spc_id,int *qdesc_index,LPTSTR qdesc,double *v1,double *v2,double *v3,int *v4,int *v5,double *v6)
{
	return FALSE;
}

BOOL xmlliteTemplateParser::getTransfersForSpecie(int spc_id,vecTransactionTemplateTransfers &vec)
{
	return FALSE;
}

BOOL xmlliteTemplateParser::getObjTmplP30(vecObjectTemplate_p30_table &vec,CString& name,CString& done_by,CString& notes)
{			
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	TCHAR szQualName[127];

	XmlNodeType nodeType = XmlNodeType_None;
	BOOL bFound = FALSE;
	int nSpcID = 0;
	CString sSpcName;
	double fP30Price,fPriceRel;
	int nCounter = 0;
	vecInt vInts;

	readerRewind();

	vec.clear();
	// Read off xml file
	while( getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"obj_p30_tmpl") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;

				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"name") == 0)
					{
						name = pwszValue;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					if (_tcscmp(pwszLocalAttrName,L"done_by") == 0)
					{
						done_by = pwszValue;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					if (_tcscmp(pwszLocalAttrName,L"note") == 0)
					{
						notes = pwszValue;
						bFound = TRUE;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 
			if (bFound)
			{
				if (_tcscmp(pwszLocalName,L"p30_data") == 0) 
				{
					vInts.clear();
					nCounter = 0;
					// Start by adding diamterclasses for specie or for ALL species if specie_id=-1; 091216 p�d
					for (HRESULT result = getReader()->MoveToFirstAttribute();
											 S_OK == result;
											 result = getReader()->MoveToNextAttribute())
					{
						getReader()->GetValue(&pwszValue, NULL);
						switch (nCounter)
						{
							case 0 : nSpcID = _tstoi(pwszValue); break;
							case 1 : sSpcName = pwszValue; break;
							case 2 : fP30Price = localeStrToDbl(pwszValue); break;
							case 3 : fPriceRel = localeStrToDbl(pwszValue); 					// Add each PricePerQuality row to vector; 100114 p�d
											vec.push_back(CObjectTemplate_p30_table(nSpcID,sSpcName,fP30Price,fPriceRel));
							break;
						};

							nCounter++;
					}
				}
			}
		}	// if( nodeType == XmlNodeType_Element )

	}
	return TRUE;
}

BOOL xmlliteTemplateParser::getObjTmplP30_nn(vecObjectTemplate_p30_nn_table &vec,CString &area_name,int *area_index)
{
	CString sSpcName,sSI;
	int nSpcID;
	int nFrom;
	int nTo;
	int nP30;
	double fPriceRel;
	short nPulpWood = 0;
	const WCHAR* pwszLocalName;
	const WCHAR* pwszLocalAttrName;
	const WCHAR* pwszValue;
	BOOL bFound = FALSE;
	int nCounter = 0;
	XmlNodeType nodeType = XmlNodeType_None;

	readerRewind();

	// Read off xml file
	while(getReader()->Read(&nodeType) == S_OK )
	{
		// Check every element
		if( nodeType == XmlNodeType_Element )
		{
			getReader()->GetLocalName(&pwszLocalName, NULL);		
			if (_tcscmp(pwszLocalName,L"Specie") == 0)	// Search for node; 091215 p�d
			{
				bFound = TRUE;

				int nAttrCount = 0;
				// Iterate through attributes and serach for item; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetLocalName(&pwszLocalAttrName, NULL);
					getReader()->GetValue(&pwszValue, NULL);
					if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					{
						nSpcID = _tstoi(pwszValue);
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					if (_tcscmp(pwszLocalAttrName,L"name") == 0)
					{
						sSpcName = pwszValue;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					else
						sSpcName = L"";
					if (_tcscmp(pwszLocalAttrName,L"si") == 0)
					{
						sSI = pwszValue;
					}	// if (_tcscmp(pwszLocalAttrName,L"id") == 0)
					else
						sSI = L"";
				}	// for (HRESULT result = getReader()->MoveToFirstAttribute();S_OK == result;result = getReader()->MoveToNextAttribute())

			}	// if (_tcscmp(pwszLocalName,L"Specie") == 0) 

			if (_tcscmp(pwszLocalName,L"data") == 0 && bFound) 
			{
				int nAttrCount = 0;
				// Iterate through values; 091215 p�d
				for (HRESULT result = getReader()->MoveToFirstAttribute();
										 S_OK == result;
										 result = getReader()->MoveToNextAttribute())
				{
					getReader()->GetValue(&pwszValue, NULL);
					switch(nCounter)
					{
						case 0 : nFrom = _tstoi(pwszValue); break;
						case 1 : nTo = _tstoi(pwszValue); break;
						case 2 : nP30 = _tstoi(pwszValue); break;
						case 3 : 
						{
							fPriceRel = localeStrToDbl(pwszValue); 
							vec.push_back(CObjectTemplate_p30_nn_table(nSpcID,sSpcName,sSI,*area_index,nFrom,nTo,nP30,fPriceRel));
							break;
						}
					};
					nCounter++;
				}
			}	// if (_tcscmp(pwszLocalName,L"Assortments") == 0 && bFound) 
		}	// if( nodeType == XmlNodeType_Element )
	}	// while(getReader()->Read(&nodeType) == S_OK )

	return TRUE;
}

BOOL xmlliteTemplateParser::getObjTmplP30_nn_notes(CString &notes)
{
	return TRUE;
}

BOOL xmlliteTemplateParser::getObjTmplP30_nn_name(CString &name)
{
	return TRUE;
}

BOOL xmlliteTemplateParser::getObjTmplP30_nn_done_by(CString &done_by)
{
	return TRUE;
}

BOOL xmlliteTemplateParser::getObjTmplP30_nn_related_species(mapShortAndBool &pine_map,mapShortAndBool &spruce_map)
{
	return FALSE;
}

BOOL xmlliteTemplateParser::getObjTmplHCost(int *type_set,double *break_value,double *factor,vecObjectTemplate_hcost_table &vec,CString& name,CString& done_by)
{
	return FALSE;
}
