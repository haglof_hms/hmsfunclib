#ifndef _REPORTFILEPARSER_H_
#define _REPORTFILEPARSER_H_

/*
class CSTD_Reports
{
	CString m_sFileName;
	CString m_sStrID;
	CString m_sCaption;
public:
	CSTD_Reports() 
	{
		m_sFileName	= _T("");
		m_sStrID	= _T("");
		m_sCaption	= _T("");
	}

	CSTD_Reports(LPTSTR fn,LPCTSTR str_id,LPCTSTR caption) 
	{
		m_sFileName	= _T(fn);
		m_sStrID	= _T(str_id);
		m_sCaption	= _T(caption);
	}

	CSTD_Reports(const CSTD_Reports &c) 
	{
		*this = c;
	}


	CString getFileName(void)		{ return m_sFileName; }
	CString getStrID(void)			{ return m_sStrID; }
	CString getCaption(void)		{ return m_sCaption; }
};

typedef std::vector<CSTD_Reports> vecSTDReports;

class ReportFileParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	
	vecSTDReports m_vecSTDReports;
public:
	ReportFileParser(void);

	virtual ~ReportFileParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);

	BOOL getReports(vecSTDReports &);	// Holds reports for Suite/Usermodule

};

*/
#endif