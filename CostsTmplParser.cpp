#include "StdAfx.h"
#include "CostsTmplParser.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


CostsTmplParser::CostsTmplParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

CostsTmplParser::~CostsTmplParser()
{
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL CostsTmplParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL CostsTmplParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL CostsTmplParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString CostsTmplParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double CostsTmplParser::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

int CostsTmplParser::getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	int nValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		nValue = _tstoi(szData);
	}	// if (pAttr)

	return nValue;
}

BOOL CostsTmplParser::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

//-------------------------------------------------------------------
// Methods for reading template header infromation
BOOL CostsTmplParser::getCostsTmplName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_COSTTMPL_NAME);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}

	return FALSE;
}

BOOL CostsTmplParser::getCostsTmplDoneBy(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_COSTTMPL_DONE_BY);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}

	return FALSE;
}

BOOL CostsTmplParser::getCostsTmplDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_COSTTMPL_DATE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}

	return FALSE;
}

BOOL CostsTmplParser::getCostsTmplCutting(CTransaction_costtempl_cutting &rec)
{
	TCHAR szData[128];
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = NULL;
	
	pNode = pRoot->selectSingleNode(NODE_COSTTMPL_CUT_1);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut1(_tstof(szData));
	}

	pNode = pRoot->selectSingleNode(NODE_COSTTMPL_CUT_2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut2(_tstof(szData));
	}

	pNode = pRoot->selectSingleNode(NODE_COSTTMPL_CUT_3);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut3(_tstof(szData));
	}

	pNode = pRoot->selectSingleNode(NODE_COSTTMPL_CUT_4);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut4(_tstof(szData));
	}

	return FALSE;
}

BOOL CostsTmplParser::getCostsTmplTransport(vecTransaction_costtempl_transport &vec)
{
	int nSpcID;
	CString sName;
	double fDistance;
	double fCost;
	double fMaxCost;
	CComBSTR bstrData;
	long lNumOf = 0;
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_COSTTMPL_TRANSPORT));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOf );
		for (int l1 = 0;l1 < lNumOf;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				nSpcID = getAttrInt(attributes,_T("id"));
				sName = getAttrText(attributes,_T("name"));
				fDistance = getAttrDouble(attributes,_T("distance"));
				fCost = getAttrDouble(attributes,_T("cost"));
				fMaxCost = getAttrDouble(attributes,_T("max_cost"));
/*
				S.Format("getCostsTmplTransport\nNumOf %ld\nSpcID %d\nSpcName %s\nDist %f\nCost %f",
					lNumOf,nSpcID,sName,fDistance,fCost);
				AfxMessageBox(S);
*/
				vec.push_back(CTransaction_costtempl_transport(nSpcID,sName,fDistance,fCost,fMaxCost));
			}	// if (pSubNode)
		}	// for (int l1 = 0;l1 < lNumOfAssort;l1++)

		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL CostsTmplParser::getCostsTmplOthers(CTransaction_costtempl_other_costs &rec)
{
	TCHAR szData1[128];
	TCHAR szData2[128];
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodePtr pNode = NULL;
	
	pNode = pRoot->selectSingleNode(NODE_COSTTMPL_OTHER_COST);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData1,_bstr_t(bstrData));
	}

	pNode = pRoot->selectSingleNode(NODE_COSTTMPL_OTHER_COST_NOTE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData2,_bstr_t(bstrData));
	}

	rec = CTransaction_costtempl_other_costs(_tstof(szData1),szData2);

	return TRUE;
}

//-------------------------------------------------------------------
// 
BOOL CostsTmplParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}

////////////////////////////////////////////////////////////////////
// ObjectCostsTmplParser

ObjectCostsTmplParser::ObjectCostsTmplParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

ObjectCostsTmplParser::~ObjectCostsTmplParser()
{
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL ObjectCostsTmplParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL ObjectCostsTmplParser::LoadFromBuffer(CString buffer)
{
	return pDomDoc->loadXML(_bstr_t(buffer));
}

BOOL ObjectCostsTmplParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString ObjectCostsTmplParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double ObjectCostsTmplParser::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

int ObjectCostsTmplParser::getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	int nValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		nValue = _tstoi(szData);
	}	// if (pAttr)

	return nValue;
}

BOOL ObjectCostsTmplParser::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

//-------------------------------------------------------------------
// Methods for reading template header infromation
BOOL ObjectCostsTmplParser::getObjCostsTmplName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_NAME);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}
	else
		_tcscpy(data,L"");

	return FALSE;
}

BOOL ObjectCostsTmplParser::setObjCostsTmplName(LPCTSTR data)
{
	CComBSTR bstrData(data);
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_NAME);
	if (pNode)
	{	
		pNode->put_text(bstrData);
		
		return TRUE;
	}

	return FALSE;
}

BOOL ObjectCostsTmplParser::getObjCostsTmplDoneBy(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_DONE_BY);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}
	else
		_tcscpy(data,L"");

	return FALSE;
}

BOOL ObjectCostsTmplParser::getObjCostsTmplDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_DATE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		
		return TRUE;
	}

	return FALSE;
}

BOOL ObjectCostsTmplParser::getCutCostSetAs(int *set_as)
{
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_CUTTING);
	if (pNode)
	{	
		pNode->get_attributes( &attributes );
		if (attributes)
		{
			*set_as = getAttrInt(attributes,_T("set"));
			return TRUE;
		}	// if (attributes)
		
	}	// if (pNode)

	return FALSE;
}


BOOL ObjectCostsTmplParser::getObjCostsTmplTransport(vecObjectCostTemplate_table &vec)
{
	int nSpcID;
	CString sName;
	CComBSTR bstrData;
	long lNumOfRows = 0;
	long lNumOfColumns = 0;
	int nColCnt;
	CString sColumn;
	CObjectCostTemplate_table table;

	// Make sure the vector is empty; 080213 p�d
	vec.clear();

	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_COSTTMPL_TRANSP_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfRows );
		for (long l1 = 0;l1 < lNumOfRows;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				lNumOfColumns = attributes->Getlength();
				nColCnt = 1;
				for (long l2 = 0;l2 < lNumOfColumns;l2++)
				{
					if (l2 == 0)
					{
						nSpcID = getAttrInt(attributes,(NODE_OBJ_COSTTMPL_ATTR1));
					}
					else if (l2 == 1)
					{
						sName = getAttrText(attributes,(NODE_OBJ_COSTTMPL_ATTR2));
						table = CObjectCostTemplate_table(nSpcID,sName);
					}
					else
					{
						sColumn.Format(_T("%s%d"),NODE_OBJ_COSTTMPL_ATTR3,nColCnt);
						table.setValue_int(getAttrInt(attributes,(sColumn)));
						nColCnt++;
					}	// else
				}	// for (long l2 = 0;l2 < lNumOfColumns;l2++)
			}	// if (pSubNode != NULL)

			vec.push_back(table);
		}	// for (long l1 = 0;l1 < lNumOfRows;l1++)
		return TRUE;
	}	// if (pSubNodeList)		
	
	return FALSE;
}

BOOL ObjectCostsTmplParser::getObjCostsTmplTransport2(vecTransaction_costtempl_transport &vec)
{
	int nSpcID;
	CString sName;
	double fDistance;
	double fCost;
	double fMaxCost;
	CComBSTR bstrData;
	long lNumOf = 0;
	vec.clear();

	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_COSTTMPL_TRANSP2_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOf );
		for (int l1 = 0;l1 < lNumOf;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				nSpcID = getAttrInt(attributes,_T("id"));
				sName = getAttrText(attributes,_T("spc"));
				fDistance = getAttrDouble(attributes,_T("distance"));
				fCost = getAttrDouble(attributes,_T("cost"));
				fMaxCost = getAttrDouble(attributes,_T("max_cost"));
				vec.push_back(CTransaction_costtempl_transport(nSpcID,sName,fDistance,fCost,fMaxCost));
			}	// if (pSubNode)
		}	// for (int l1 = 0;l1 < lNumOfAssort;l1++)

		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}


BOOL ObjectCostsTmplParser::getObjCostsTmplCutting(vecObjectCostTemplate_table &vecDGV,vecObjectCostTemplate_table &vecM3FUB)
{
	int nSpcID;
	CString sName;
	TCHAR szNodeName[128];
	CComBSTR bstrData;
	long lNumOfRows = 0;
	long lNumOfColumns = 0;
	int nColCntDGV,nColCntM3FUB;
	CString sColumn;
	CObjectCostTemplate_table table;
	BOOL bIsDGV = TRUE;

	// Make sure the vector is empty; 080213 p�d
	vecDGV.clear();
	vecM3FUB.clear();
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_COSTTMPL_CUTTING_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfRows );
		for (long l1 = 0;l1 < lNumOfRows;l1++)
		{
			// Don't read the first four items (Cutting per m3); 080325 p�d
			if (l1 > 3)
			{
				MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
				if (pSubNode != NULL)
				{
					
					//pSubNode->get_text(&bstrData);
					_tcscpy(szNodeName,pSubNode->baseName);
					CString sNodeName(szNodeName);
					bIsDGV = (sNodeName.CompareNoCase(_T("cutting_table")) == 0);

					pSubNode->get_attributes( &attributes );
					lNumOfColumns = attributes->Getlength();
					nColCntDGV = 1;
					nColCntM3FUB = 1;
					for (long l2 = 0;l2 < lNumOfColumns;l2++)
					{
						if (l2 == 0)
						{
							nSpcID = getAttrInt(attributes,(NODE_OBJ_COSTTMPL_ATTR1));
						}
						else if (l2 == 1)
						{
							sName = getAttrText(attributes,(NODE_OBJ_COSTTMPL_ATTR2));
							table = CObjectCostTemplate_table(nSpcID,sName);
						}
						else
						{
							if (bIsDGV)
							{
								sColumn.Format(_T("%s%d"),NODE_OBJ_COSTTMPL_ATTR3,nColCntDGV);
								table.setValue_int(getAttrInt(attributes,(sColumn)));
								nColCntDGV++;
							}
							else
							{
								sColumn.Format(_T("%s%d"),NODE_OBJ_COSTTMPL_ATTR3,nColCntM3FUB);
								table.setValue_float(getAttrDouble(attributes,(sColumn)));
								nColCntM3FUB++;
							}
						}	// else
					}	// for (long l2 = 0;l2 < lNumOfColumns;l2++)
				}	// if (pSubNode != NULL)
				if (bIsDGV)
					vecDGV.push_back(table);
				else
					vecM3FUB.push_back(table);
			}

		}	// for (long l1 = 0;l1 < lNumOfRows;l1++)
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

BOOL ObjectCostsTmplParser::getObjCostsTmplCutting2(CTransaction_costtempl_cutting &rec)
{
	TCHAR szData[128];
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = NULL;
	
	pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_CUT_1);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut1(_tstof(szData));
	}

	pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_CUT_2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut2(_tstof(szData));
	}

	pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_CUT_3);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut3(_tstof(szData));
	}

	pNode = pRoot->selectSingleNode(NODE_OBJ_COSTTMPL_CUT_4);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(szData,_bstr_t(bstrData));
		rec.setCut4(_tstof(szData));
	}

	return FALSE;
}

BOOL ObjectCostsTmplParser::getObjCostsTmplOther(vecObjectCostTemplate_other_cost_table &vec)
{
	double fPrice;
	CString sNote;
	double fVAT;
	long lNumOfRows = 0;
	// Make sure the vector is empty; 080213 p�d
	vec.clear();
	MSXML2::IXMLDOMNamedNodeMap  *attributes;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMElementPtr pElement = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pSubNodeList = pElement->getElementsByTagName(_bstr_t(NODE_OBJ_COSTTMPL_OTHER_WC));
	if (pSubNodeList)
	{
		pSubNodeList->get_length( &lNumOfRows );
		for (long l1 = 0;l1 < lNumOfRows;l1++)
		{
			MSXML2::IXMLDOMNodePtr pSubNode = pSubNodeList->item[l1];
			if (pSubNode != NULL)
			{
				pSubNode->get_attributes( &attributes );
				fPrice = getAttrDouble(attributes,(NODE_OBJ_COSTTMPL_OTHER_ATTR1));
				sNote = getAttrText(attributes,(NODE_OBJ_COSTTMPL_OTHER_ATTR2));
				fVAT = getAttrDouble(attributes,(NODE_OBJ_COSTTMPL_OTHER_ATTR3));
				vec.push_back(CObjectCostTemplate_other_cost_table(fPrice,sNote,fVAT));
			}	// if (pSubNode != NULL)

		}	// for (long l1 = 0;l1 < lNumOfRows;l1++)
		return TRUE;
	}	// if (pSubNodeList)		
				
	return FALSE;
}

//-------------------------------------------------------------------
// 
BOOL ObjectCostsTmplParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}

//-------------------------------------------------------------------
// Parser based on xmlLite; 091221 p�d

xmlliteCostsParser::xmlliteCostsParser(void)
{
}

xmlliteCostsParser::~xmlliteCostsParser()
{
}

// Private

BOOL xmlliteCostsParser::getDGVTable(vecObjectCostTemplate_table &vec)
{
	int nAttrCounter = 0;
	int nSpcID;
	int nValue;
	TCHAR szSpcName[127];
	CObjectCostTemplate_table table;

	vec.clear();
	if (!gotoNodeElement(L"cutting_table")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nSpcID); break;
					case 1 : getAttrValue(szSpcName); table =	CObjectCostTemplate_table(nSpcID,szSpcName); break;
					default : getAttrValue(&nValue); table.setValue_int(nValue); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
			vec.push_back(table);
		}	
	} while(gotoNextNodeElement());
	return TRUE;
}

BOOL xmlliteCostsParser::getM3FUBTable(vecObjectCostTemplate_table &vec)
{
	int nAttrCounter = 0;
	int nSpcID;
	double fValue;
	TCHAR szSpcName[127];
	CObjectCostTemplate_table table;

	vec.clear();
	if (!gotoNodeElement(L"cutting_table_2")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nSpcID); break;
					case 1 : getAttrValue(szSpcName); table =	CObjectCostTemplate_table(nSpcID,szSpcName); break;
					default : getAttrValue(&fValue); table.setValue_float(fValue); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
			vec.push_back(table);
		}	
	} while(gotoNextNodeElement());
	return TRUE;
}

// Public

BOOL xmlliteCostsParser::getObjCostsTmplName(LPTSTR data)
{
	return getNodeValue(L"obj_cost_tmpl_name",data);
}

BOOL xmlliteCostsParser::setObjCostsTmplName(LPCTSTR)
{
	return TRUE;
}

BOOL xmlliteCostsParser::getObjCostsTmplDoneBy(LPTSTR data)
{
	return getNodeValue(L"obj_cost_tmpl_done_by",data);
}

BOOL xmlliteCostsParser::getObjCostsTmplDate(LPTSTR data)
{
	return getNodeValue(L"obj_cost_tmpl_date",data);
}

BOOL xmlliteCostsParser::getCutCostSetAs(int *set_as)
{
	// Find element, read attributes; 091222 p�d
	if (!gotoNodeElement(L"obj_cost_tmpl_cutting")) return FALSE;
	return getAttr(L"set",set_as);
}
	
BOOL xmlliteCostsParser::getObjCostsTmplTransport(vecObjectCostTemplate_table &vec)
{
	int nAttrCounter = 0;
	int nSpcID;
	int nValue;
	TCHAR szSpcName[127];
	CObjectCostTemplate_table table;

	vec.clear();
	if (!gotoNodeElement(L"transp_table")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nSpcID); break;
					case 1 : getAttrValue(szSpcName); table =	CObjectCostTemplate_table(nSpcID,szSpcName); break;
					default : getAttrValue(&nValue); table.setValue_int(nValue); break;
				};
				
				nAttrCounter++;
			} while (gotoNextAttribute());
			vec.push_back(table);
		}	
	} while(gotoNextNodeElement());
	return TRUE;
}

BOOL xmlliteCostsParser::getObjCostsTmplTransport2(vecTransaction_costtempl_transport &vec)
{
	int nAttrCounter = 0;
	int nSpcID;
	TCHAR szSpcName[127];
	double fDistance;
	double fCost;
	double fMaxCost;
	CObjectCostTemplate_table table;

	vec.clear();
	if (!gotoNodeElement(L"transp2_table")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&nSpcID); break;
					case 1 : getAttrValue(szSpcName); break;
					case 2 : getAttrValue(&fDistance); break;
					case 3 : getAttrValue(&fCost); break;
					case 4 : 
						getAttrValue(&fMaxCost); 
						vec.push_back(CTransaction_costtempl_transport(nSpcID,szSpcName,fDistance,fCost,fMaxCost));
					break;
				};			
				nAttrCounter++;
			} while (gotoNextAttribute());
		}	
	} while(gotoNextNodeElement());
	return TRUE;
}

BOOL xmlliteCostsParser::getObjCostsTmplCutting(vecObjectCostTemplate_table &vecDGV,vecObjectCostTemplate_table &vecM3FUB)
{
	int nSetAs = 0;
	getCutCostSetAs(&nSetAs);

	vecDGV.clear();
	vecM3FUB.clear();
	getDGVTable(vecDGV);
	getM3FUBTable(vecM3FUB);

	return TRUE;
}

BOOL xmlliteCostsParser::getObjCostsTmplCutting2(CTransaction_costtempl_cutting &rec)
{
	double fCutting = 0.0;
	double fHarvester = 0.0;
	double fForwarder = 0.0;
	double fOtherCosts = 0.0;

	getNodeValue(L"cost_tmpl_cut_1",&fCutting);
	getNodeValue(L"cost_tmpl_cut_2",&fHarvester);
	getNodeValue(L"cost_tmpl_cut_3",&fForwarder);
	getNodeValue(L"cost_tmpl_cut_4",&fOtherCosts);

	rec = CTransaction_costtempl_cutting(fCutting,fHarvester,fForwarder,fOtherCosts);

	return TRUE;
}

BOOL xmlliteCostsParser::getObjCostsTmplOther(vecObjectCostTemplate_other_cost_table &vec)
{
	int nAttrCounter = 0;
	double fPrice;
	TCHAR szNote[255];
	double fVAT;
	CObjectCostTemplate_table table;

	vec.clear();
	if (!gotoNodeElement(L"other_table")) return FALSE;
	do
	{
		nAttrCounter = 0;
		if (gotoFirstAttribute())
		{
			do
			{
				switch (nAttrCounter)
				{
					case 0 : getAttrValue(&fPrice); break;
					case 1 : getAttrValue(szNote); break;
					case 2 : 
						getAttrValue(&fVAT); 
						vec.push_back(CObjectCostTemplate_other_cost_table(fPrice,szNote,fVAT));
					break;
				};			
				nAttrCounter++;
			} while (gotoNextAttribute());
		}	
	} while(gotoNextNodeElement());
	return TRUE;
}
