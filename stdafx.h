// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN	// Exclude rarely-used stuff from Windows headers
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components

// TODO: reference additional headers your program requires here
// XML handling
#import <msxml3.dll> rename ("value", "valueEx")//named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

//#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include <SQLAPI.h> // main SQLAPI++ header

#include <vector>
#include <map>

#include <locale.h>
#include <ctime>

#include <string.h>


// Disable use of AfxMessageBox and MessageBox; UMMessageBox should be used instead
#ifdef USE_UMMESSAGEBOX
#include "UMMessageBox.h"

#ifdef MessageBox
#undef MessageBox
#endif

#define AfxMessageBox _DISABLED_USE_UMMESSAGEBOX_INSTEAD
#define MessageBox _DISABLED_USE_UMMESSAGEBOX_INSTEAD
#endif


// Max buffer for split methods
#define EOL				0x0d
#define SEP				0x3b
#define MAX_BUFFER      1024  // 1 kb

// Define nodes in Pricelist; 060407 p�d
const LPCTSTR NODE_HEADER_NAME					= _T("//Pricelist/Header/Name");
const LPCTSTR NODE_HEADER_DONE_BY				= _T("//Pricelist/Header/Done_by");
const LPCTSTR NODE_HEADER_DATE					= _T("//Pricelist/Header/Date");
const LPCTSTR NODE_HEADER_NOTES					= _T("//Pricelist/Header/Notes");
const LPCTSTR NODE_HEADER_PRICE_IN				= _T("//Pricelist/Header/Timberprice_in");

const LPCTSTR NODE_SPECIES						= _T("Specie");
const LPCTSTR NODE_SPECIES_ASSORTMENTS			= _T("Assortments");
const LPCTSTR NODE_SPECIES_QUALDESC				= _T("QualityDesc");

const LPCTSTR NODE_REPORTS						= _T("Reports/*");
const LPCTSTR NODE_MODULE_ATTR					= _T("module");
const LPCTSTR NODE_INDEX_ATTR					= _T("index");
const LPCTSTR NODE_INDEX_LANG					= _T("lang");

const LPCTSTR NODE_ADD_TO						= _T("add_to");
const LPCTSTR NODE_FILENAME_ATTR				= _T("fn");
const LPCTSTR NODE_STRID_ATTR					= _T("strid");
const LPCTSTR NODE_CAPTION_ATTR					= _T("caption");

//-----------------------------------------------------------------------------------------------------
// Define nodes in Templates; 070808 p�d
const LPCTSTR NODE_TEMPLATE_NAME				= _T("//template/template_name");
const LPCTSTR NODE_TEMPLATE_DONE_BY				= _T("//template/template_done_by");
const LPCTSTR NODE_TEMPLATE_DATE				= _T("//template/template_date");
const LPCTSTR NODE_TEMPLATE_NOTES				= _T("//template/template_notes");

const LPCTSTR NODE_TEMPLATE_DCLS				= _T("//template/template_dcls");
const LPCTSTR NODE_TEMPLATE_HGT_OVER_SEA		= _T("//template/template_hgt_over_sea");
const LPCTSTR NODE_TEMPLATE_LATITUDE			= _T("//template/template_latitude");
const LPCTSTR NODE_TEMPLATE_LONGITUDE			= _T("//template/template_longitude");
const LPCTSTR NODE_TEMPLATE_GROWTH_AREA			= _T("//template/template_growth_area");
const LPCTSTR NODE_TEMPLATE_SI_H100				= _T("//template/template_si_h100");
const LPCTSTR NODE_TEMPLATE_CORR_FACTOR			= _T("//template/template_corr_factor");
const LPCTSTR NODE_TEMPLATE_COSTS_TMPL			= _T("//template/template_costs_tmpl");

const LPCTSTR NODE_TEMPLATE_AT_COAST			= _T("template_at_coast");
const LPCTSTR NODE_TEMPLATE_SOUTH_EAST			= _T("template_south_east");
const LPCTSTR NODE_TEMPLATE_REGION_5			= _T("template_region_5");
const LPCTSTR NODE_TEMPLATE_PART_OF_PLOT		= _T("template_part_of_plot");
const LPCTSTR NODE_TEMPLATE_SI_H100_PINE		= _T("template_si_h100_pine");

const LPCTSTR NODE_TEMPLATE_EXCHANGE			= _T("//template/template_exchange");
const LPCTSTR NODE_TEMPLATE_EXCHANGE_ID			= _T("id");		// Attibute
const LPCTSTR NODE_TEMPLATE_EXCHANGE_NAME		= _T("name");	// Attibute

const LPCTSTR NODE_TEMPLATE_PRICELIST			= _T("//template/template_pricelist");
const LPCTSTR NODE_TEMPLATE_PRICELIST_ID		= _T("id");		// Attibute
const LPCTSTR NODE_TEMPLATE_PRICELIST_NAME		= _T("name");	// Attibute

const LPCTSTR NODE_TEMPLATE_SPECIE				= _T("template_specie");
const LPCTSTR NODE_TEMPLATE_SPECIE_ID			= _T("id");		// Attibute
const LPCTSTR NODE_TEMPLATE_SPECIE_NAME			= _T("name");	// Attibute

const LPCTSTR NODE_TEMPLATE_FUNC_HGT				= _T("template_specie_hgt");
const LPCTSTR NODE_TEMPLATE_FUNC_VOL				= _T("template_specie_vol");
const LPCTSTR NODE_TEMPLATE_FUNC_BARK				= _T("template_specie_bark");
const LPCTSTR NODE_TEMPLATE_FUNC_VOL_UB			= _T("template_specie_vol_ub");
const LPCTSTR NODE_TEMPLATE_FUNC_ID					= _T("id");
const LPCTSTR NODE_TEMPLATE_FUNC_SPC_ID			= _T("spc_id");
const LPCTSTR NODE_TEMPLATE_FUNC_INDEX			= _T("index");
const LPCTSTR NODE_TEMPLATE_FUNC_SPC_NAME		= _T("spc_name");
const LPCTSTR NODE_TEMPLATE_FUNC_TYPE				= _T("func_type");
const LPCTSTR NODE_TEMPLATE_FUNC_AREA				= _T("func_area");
const LPCTSTR NODE_TEMPLATE_FUNC_DESC				= _T("func_desc");
const LPCTSTR NODE_TEMPLATE_FUNC_M3SK_M3UB	= _T("m3sk_m3ub");
const LPCTSTR NODE_TEMPLATE_FUNC_M3FUB_M3TO	= _T("template_specie_fub_to_to");

const LPCTSTR NODE_TEMPLATE_QUAL_DESC			= _T("template_specie_qdesc");

const LPCTSTR NODE_TEMPLATE_GROT					= _T("template_specie_grot");

const LPCTSTR NODE_TEMPLATE_TRANSP_DIST1		= _T("template_transp_dist1");
const LPCTSTR NODE_TEMPLATE_TRANSP_DIST2		= _T("template_transp_dist2");
const LPCTSTR NODE_TEMPLATE_M3SK_TO_M3FUB		= _T("template_specie_sk_to_fub");
const LPCTSTR NODE_TEMPLATE_M3FUB_TO_M3TO		= _T("template_specie_fub_to_to");
const LPCTSTR NODE_TEMPLATE_H25					= _T("template_specie_h25");
const LPCTSTR NODE_TEMPLATE_GCROWN				= _T("template_specie_gcrown");

const LPCTSTR NODE_TEMPLATE_TRANSFERS			= _T("template_transfers");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_ITEM		= _T("template_transfer_item");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_FROM_ID	= _T("from_id");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_FROM		= _T("from_name");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_TO_ID		= _T("to_id");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_TO		= _T("to_name");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_FUB		= _T("m3fub");
const LPCTSTR NODE_TEMPLATE_TRANSFERS_PERC		= _T("percent");

const LPCTSTR NODE_OBJ_TEMPLATE_P30_WC			= _T("p30_tmpl_data/*");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_SET			= _T("//obj_tmpl_p30/p30_tmpl_data");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR1		= _T("spcid");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR2		= _T("spc_name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR3		= _T("price");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR4		= _T("price_rel");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR5		= _T("name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR6		= _T("done_by");
const LPCTSTR NODE_OBJ_TEMPLATE_P30_ATTR7		= _T("note");

const LPCTSTR NODE_OBJ_TEMPLATE_HCOST_WC			= _T("hcost_tmpl_data/*");
const LPCTSTR NODE_HCOST_TMPL_TYPE_SET				= _T("//obj_tmpl_hcost/hcost_tmpl_type");
const LPCTSTR NODE_HCOST_TMPL_TYPE_SET_ATTR1	= _T("type_set");
const LPCTSTR NODE_HCOST_TMPL_TYPE_SET_ATTR2	= _T("name");
const LPCTSTR NODE_HCOST_TMPL_TYPE_SET_ATTR3	= _T("done_by");
const LPCTSTR NODE_HCOST_TMPL_BREAK_FACTOR		= _T("//obj_tmpl_hcost/hcost_tmpl_bf");
const LPCTSTR NODE_HCOST_TMPL_BREAK_FAC_ATTR1	= _T("break_value");
const LPCTSTR NODE_HCOST_TMPL_BREAK_FAC_ATTR2	= _T("factor");

const LPCTSTR NODE_OBJ_TEMPLATE_HCOST_ATTR1		= _T("m3sk");
const LPCTSTR NODE_OBJ_TEMPLATE_HCOST_ATTR2		= _T("price_m3sk");
const LPCTSTR NODE_OBJ_TEMPLATE_HCOST_ATTR3		= _T("sum_price");
const LPCTSTR NODE_OBJ_TEMPLATE_HCOST_ATTR4		= _T("base_comp");	// Added 2009-05-29 P�D

const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_NAME				= _T("//obj_tmpl_p30nn/name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_NAME_VALUE	= _T("name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DONE_BY			= _T("//obj_tmpl_p30nn/done_by");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DONE_BY_VALUE	= _T("name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_AREA				= _T("//obj_tmpl_p30nn/area");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_AREA_NAME		= _T("name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_AREA_IDX		= _T("index");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_NOTES				= _T("//obj_tmpl_p30nn/notes");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_NOTES_VALUE	= _T("value");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_WC					= _T("specie");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_WC1					= _T("data");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_SPC_NAME		= _T("name");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_SPC_ID			= _T("id");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_SPC_SI			= _T("si");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_FROM		= _T("from");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_TO			= _T("to");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_P30		= _T("p30");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_DATA_PREL		= _T("prel");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_WC2					= _T("related_to_pine");
const LPCTSTR NODE_OBJ_TEMPLATE_P30NN_WC3					= _T("related_to_spruce");

//-----------------------------------------------------------------------------------------------------
// Template for costs; 071008 p�d
const LPCTSTR NODE_COSTTMPL_NAME				= _T("//cost_tmpl/cost_tmpl_header/cost_tmpl_name");
const LPCTSTR NODE_COSTTMPL_DONE_BY				= _T("//cost_tmpl/cost_tmpl_header/cost_tmpl_done_by");
const LPCTSTR NODE_COSTTMPL_DATE				= _T("//cost_tmpl/cost_tmpl_header/cost_tmpl_date");

const LPCTSTR NODE_COSTTMPL_CUT_1				= _T("//cost_tmpl/cost_tmpl_cutting/cost_tmpl_cut_1");	// "Avverkning"
const LPCTSTR NODE_COSTTMPL_CUT_2				= _T("//cost_tmpl/cost_tmpl_cutting/cost_tmpl_cut_2");	// "Sk�rdare"
const LPCTSTR NODE_COSTTMPL_CUT_3				= _T("//cost_tmpl/cost_tmpl_cutting/cost_tmpl_cut_3");	// "Skotare"
const LPCTSTR NODE_COSTTMPL_CUT_4				= _T("//cost_tmpl/cost_tmpl_cutting/cost_tmpl_cut_4");	// "Andra kostnader"

const LPCTSTR NODE_COSTTMPL_TRANSPORT			= _T("cost_tmpl_transport/*");	// ""

const LPCTSTR NODE_COSTTMPL_OTHER_COST			= _T("//cost_tmpl/cost_tmpl_others/cost_tmpl_others_cost");	// ""
const LPCTSTR NODE_COSTTMPL_OTHER_COST_NOTE		= _T("//cost_tmpl/cost_tmpl_others/cost_tmpl_others_typeof");	// ""

//-----------------------------------------------------------------------------------------------------
// Template for object costs; 080211 p�d

const LPCTSTR NODE_OBJ_COSTTMPL_NAME			= _T("//obj_cost_tmpl/obj_cost_tmpl_header/obj_cost_tmpl_name");
const LPCTSTR NODE_OBJ_COSTTMPL_DONE_BY		= _T("//obj_cost_tmpl/obj_cost_tmpl_header/obj_cost_tmpl_done_by");
const LPCTSTR NODE_OBJ_COSTTMPL_DATE			= _T("//obj_cost_tmpl/obj_cost_tmpl_header/obj_cost_tmpl_date");

const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP_WC		= _T("obj_cost_tmpl_transp/*");	// ""
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP			= _T("//obj_cost_tmpl/obj_cost_tmpl_transp");

const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP2_WC		= _T("obj_cost_tmpl_transp2/*");	// ""

const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING_WC		= _T("obj_cost_tmpl_cutting/*");	// ""
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING				= _T("//obj_cost_tmpl/obj_cost_tmpl_cutting");
const LPCTSTR NODE_OBJ_COSTTMPL_ATTR1					= _T("id");	// Specie id, except on first row, where spc="-1"
const LPCTSTR NODE_OBJ_COSTTMPL_ATTR2					= _T("spc");	// Specie name
const LPCTSTR NODE_OBJ_COSTTMPL_ATTR3					= _T("col");	// Add this + a number e.g. col1="100" col2="200" etc.

const LPCTSTR NODE_OBJ_COSTTMPL_CUT_1					= _T("//obj_cost_tmpl/obj_cost_tmpl_cutting/cost_tmpl_cut_1");	// "Avverkning"
const LPCTSTR NODE_OBJ_COSTTMPL_CUT_2					= _T("//obj_cost_tmpl/obj_cost_tmpl_cutting/cost_tmpl_cut_2");	// "Sk�rdare"
const LPCTSTR NODE_OBJ_COSTTMPL_CUT_3					= _T("//obj_cost_tmpl/obj_cost_tmpl_cutting/cost_tmpl_cut_3");	// "Skotare"
const LPCTSTR NODE_OBJ_COSTTMPL_CUT_4					= _T("//obj_cost_tmpl/obj_cost_tmpl_cutting/cost_tmpl_cut_4");	// "Andra kostnader"

const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_WC			= _T("obj_cost_tmpl_other/*");
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_ATTR1		= _T("price=\"%.0f\" note=\"%s\" vat=\"%.0f\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_ATTR2		= _T("note");
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_ATTR3		= _T("vat");

// Title of HMSShell window; 070119 p�d
const LPCTSTR HMSShell_WindowTitle				= _T("Hagl�f Managment System");

//#4603 20151021 J�
#define ID_HGT_SODERBERGS			1502	// S�derbergs h�jd
#define ID_BARK_SO					2002	// S�derbergs barkfunktioner


// #4670 Konstanter f�r H25
#define H25_MIN_VALUE				5.0
#define H25_MAX_VALUE				35.0
#define ID_HGT_H25					1500

//#include "atlbase.h"
//#define A2W(lpa) ( ((LPCSTR)lpa == NULL) ? NULL : (_convert = (strlen(lpa)+1), AfxA2WHelper((LPWSTR) alloca(_convert*2), lpa, _convert) ))
//#include <Atlcom.h>
#include <afxpriv.h>
//#include <atlconv.h>

//#include <atlconv.h>
