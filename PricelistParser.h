#ifndef _PRICELISTPARSER_H_
#define _PRICELISTPARSER_H_

#include "pad_transaction_classes.h"

#include "xmlliteBase.h"

class PricelistParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	PricelistParser(void);

	virtual ~PricelistParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL getHeaderName(LPTSTR);
	BOOL setHeaderName(LPCTSTR);
	BOOL getHeaderDoneBy(LPTSTR);
	BOOL getHeaderDate(LPTSTR);
	BOOL getHeaderNotes(LPTSTR);
	BOOL getHeaderPriceIn(int *);

	BOOL getXML(CString &xml);

	BOOL getSpeciesInPricelistFile(vecTransactionSpecies &);

	BOOL getAssortmentPerSpecie(vecTransactionAssort &);

	BOOL getDCLSForSpecie(int spc_id,vecTransactionDiameterclass &);

	BOOL getPriceForSpecie(int spc_id,vecTransactionPrlData &);

	BOOL getQualDescNames(CStringArray &);

	BOOL getQualDescNameForSpecie(int spc_id,CStringArray &);
	BOOL getQualDescNameForSpecie(int spc_id,vecTransactionQualDesc &);

	BOOL getQualDescPercentForSpecie(int spc_id,vecTransactionPrlData &);
	BOOL getQualDescPercentForQualityAndSpecie(int spc_id,LPCTSTR qual_name,vecTransactionPrlData &);
};


//////////////////////////////////////////////////////////////////////////////////////////////
// xmllitePricelistParser

// This class only reads xml-files; 091215 p�d

class xmllitePricelistParser : public CxmlliteBase
{
protected:
public:
	xmllitePricelistParser(void);
	virtual ~xmllitePricelistParser();

	BOOL getHeaderName(LPTSTR);
	BOOL getHeaderDoneBy(LPTSTR);
	BOOL getHeaderDate(LPTSTR);
	BOOL getHeaderNotes(LPTSTR);
	BOOL getHeaderPriceIn(int *);

//	BOOL getXML(CString &xml);

	BOOL getSpeciesInPricelistFile(vecTransactionSpecies &);

	BOOL getAssortmentPerSpecie(vecTransactionAssort &);

	BOOL getDCLSForSpecie(int spc_id,vecTransactionDiameterclass &);

	BOOL getPriceForSpecie(int spc_id,vecTransactionPrlData &);

	BOOL getQualDescNames(CStringArray &);

	BOOL getQualDescNameForSpecie(int spc_id,CStringArray &);
	BOOL getQualDescNameForSpecie(int spc_id,vecTransactionQualDesc &);

	BOOL getQualDescPercentForSpecie(int spc_id,vecTransactionPrlData &);
	BOOL getQualDescPercentForQualityAndSpecie(int spc_id,LPCTSTR qual_name,vecTransactionPrlData &);
};


#endif