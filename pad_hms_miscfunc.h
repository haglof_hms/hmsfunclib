#ifndef _HMS_MISCFUNC_H_
#define _HMS_MISCFUNC_H_

#include "StdAfx.h"

#include <wincrypt.h>
#include <locale.h>
#include <vector>
#include <SQLAPI.h>
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "HistoryCombo.h"
#include "DiskObject.h"
#include "pad_calculation_classes.h"

#include "StdioFileEx.h"

extern HINSTANCE g_hInstanceDLL;

#define WM_USER_MSG_SUITE						(WM_USER + 2)		// Used for messages sent from a Suite/usermodule
#define WM_USER_MSG_IN_SUITE				(WM_USER + 10)		// Used for messages sent from a Suite/usermodule


#define ID_NEW_ITEM									32786
#define ID_OPEN_ITEM								32787
#define ID_SAVE_ITEM                32788
#define ID_DELETE_ITEM              32789
#define ID_PREVIEW_ITEM             32790
#define ID_EXECUTE_ITEM							WM_USER+100

// Defines for Database navigation button, in HMSShell toolbar; 060412 p�d
#define ID_DBNAVIG_START						32778
#define ID_DBNAVIG_NEXT							32779
#define ID_DBNAVIG_PREV							32780	
#define ID_DBNAVIG_END							32781
#define ID_DBNAVIG_LIST							32791

// Use this id to send/recive a request on an update, of some kind; 060303 p�d
// Can be uses, e.g. to send msg from HMSShell to a suite/usermodule, and include 
// a "lParam" for identification; 060303 p�d
// Is used on startup of HMSShell to send an update message to UMDatabAse usermodule,
// setting User data for Database server used; 060303 p�d
#define ID_UPDATE_ITEM							19999

#define BLACK				RGB(  0,  0,  0)
#define WHITE				RGB(255,255,255)
#define GRAY				RGB(192,192,192)
#define BLUE				RGB(0,0,255)
#define GREEN				RGB(0,255,0)
#define RED					RGB(255,0,0)
#define INFOBK			::GetSysColor(COLOR_INFOBK)
#define COL3DFACE		::GetSysColor(COLOR_3DFACE)

// Defines used to e.g. send message from CMyReportCtrl OnLButtonUp
// in HMSFuncLib; 061117 p�d

#define ID_WPARAM_VALUE_FROM				0xCF08	// Dummy value can be used in range up to 0xCF08 - 0xD0FC (53000 - 53500)
#define ID_WPARAM_VALUE_TO					0xD0FC	// Dummy value can be used in range up to 0xCF08 - 0xD0FC (53000 - 53500)
#define ID_DBCONN_VALUE							99001		// Dummy value

#define ID_DO_SOMETHING_IN_SHELL		0xD0FD	// 53501
#define ID_LPARAM_COMMAND1					0xD0FE	// 53502; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 070329 p�d
#define ID_LPARAM_COMMAND2					0xD0FF	// 53502; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 070410 p�d
#define ID_LPARAM_COMMAND3					0xD100	// 53503; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 070528 p�d
#define ID_LPARAM_COMMAND4					0xD101	// 53504; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 080421 p�d
#define ID_LPARAM_COMMAND5					0xD102	// 53505; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 080421 p�d
#define ID_LPARAM_COMMAND6					0xD103	// 53506; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 080421 p�d
#define ID_LPARAM_COMMAND7					0xD104	// 53507; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 080421 p�d
#define ID_LPARAM_COMMAND8					0xD105	// 53508; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 080421 p�d
#define ID_LPARAM_COMMAND9					0xD106	// 53509; this LPARAM command tells the SHELL to do setServerInfoOnStatusBar(); 080421 p�d
#define ID_LPARAM_COMMAND_DBTIMER_START		0xD107	// Start the database checking timer
#define ID_LPARAM_COMMAND_DBTIMER_STOP		0xD108	// Stop the database checking timer
#define ID_LPARAM_COMMAND_HANDLE_FILETYPE		0xD109	// Handle a specific filetype

//////////////////////////////////////////////////////////////////////////
// Adminstrator database sql-script "hardcoded"; 080701 p�d
const LPCTSTR HMS_ADMINISTRATOR_DBNAME			= _T("hms_administrator");	// Administrator database name; 060303 p�d
const LPCTSTR HMS_ADMINISTRATOR_TABLE			= _T("hms_user_db");	// Administrator database name; 060303 p�d
const LPCTSTR table_Administrator = _T("CREATE TABLE %s (")
									_T("DB_NAME VARCHAR(25) NOT NULL,")
									_T("USER_NAME VARCHAR(25),")
									_T("NOTES VARCHAR(255),")
									_T("CREATED_BY VARCHAR(20),")
									_T("CREATED datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("PRIMARY KEY (DB_NAME));");

//////////////////////////////////////////////////////////////////////////
// Structure used when changing language (update navigation tree etc.); 060810 p�d
typedef struct _SendMessageDataTransStruct
{
	int nCommand;
	char szValue[128];
} SEND_MESSAGE_DATA_TRANS_STRUCT;

//////////////////////////////////////////////////////////////////////////
// CMyReportCtrl; derived from CXTPReportControl
class CMyReportCtrl : public CXTPReportControl
{
	DECLARE_DYNCREATE(CMyReportCtrl)
//private:	
	BOOL bIsDirty;
	BOOL m_bDoValidateCol;
	BOOL m_bDoEditFirstColumn;
	BOOL m_bRunMetrics;
	CList<int,int&> m_vecDcls;
	CList<int,int&> m_vecStart;
	CList<int,int&> m_vecNumOf;
	int m_nID;
	int m_nFromColumn;
	CBrush m_hBrush;
	CBrush *m_pBrush;
 
	double m_fValidateValue;
public:
	CMyReportCtrl();
	CMyReportCtrl(const CMyReportCtrl &);

	BOOL Create(CWnd* pParentWnd,UINT nID,BOOL add_hscroll = FALSE,BOOL run_metrics = TRUE);

	CMyReportCtrl operator=(const CMyReportCtrl &c)
	{
		return c;
	}

	// Virtual method
	virtual void GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics);
	virtual void OnDraw(CDC* pDC);
	virtual void OnSelectionChanged();

	// Methods
	BOOL ClearReport(void);

	BOOL isDirty(void);

	void setIsDirty(BOOL val);

	CScrollBar *getVertScrollBar(void)
	{
		return GetScrollBarCtrl(SB_VERT);
	}

	// Do edit column method
	void setDoEditFirstColumn(BOOL val)
	{
		m_bDoEditFirstColumn = val;
	}
	
	// Validation methods
	void setValidateColumns(BOOL validate,int from_col,double validate_value)
	{
		m_bDoValidateCol = validate;
		m_nFromColumn = from_col;
		m_fValidateValue = validate_value;
	}

	BOOL isDoValidate(void)
	{
		return m_bDoValidateCol;
	}

	double getValidateValue(void)
	{
		return m_fValidateValue;
	}

	BOOL isDataValid(void);

	CXTPReportColumn* AddMyColumn(CXTPReportColumn* pColumn,int dcls,int start,int num_of)
	{
		CXTPReportControl::AddColumn(pColumn);
		m_vecDcls.AddTail(dcls);
		m_vecStart.AddTail(start);
		m_vecNumOf.AddTail(num_of);
		return pColumn;
	}

	// Column value methods
	int getFromColumn(void)
	{
		return m_nFromColumn;
	}


	// Always retrive the Last item in list
	BOOL getItemData(int idx,int *dcls,int *start = 0,int *num_of = 0)
	{
		BOOL bIsDCLS = FALSE;
		BOOL bIsStart = FALSE;
		BOOL bIsNumOf = FALSE;
		POSITION posDcls = m_vecDcls.FindIndex(idx);
		if (posDcls)
		{
			*dcls = (int)m_vecDcls.GetAt(posDcls);
			bIsDCLS = TRUE;
		}

		POSITION posStart = m_vecStart.FindIndex(idx);
		if (posStart)
		{
			*start = (int)m_vecStart.GetAt(posStart);
			bIsStart = TRUE;
		}

		POSITION posNumOf = m_vecNumOf.FindIndex(idx);
		if (posNumOf)
		{
			*num_of = (int)m_vecNumOf.GetAt(posNumOf);
			bIsNumOf = TRUE;
		}

		return (bIsDCLS && bIsStart && bIsNumOf);
	}

	// Always retrive the Last item in list
	BOOL getLastItemData(int *dcls,int *start = 0,int *num_of = 0)
	{
		BOOL bIsDCLS = FALSE;
		BOOL bIsStart = FALSE;
		BOOL bIsNumOf = FALSE;
		POSITION posDcls = m_vecDcls.GetTailPosition();
		if (posDcls)
		{
			*dcls = (int)m_vecDcls.GetAt(posDcls);
			bIsDCLS = TRUE;
		}

		POSITION posStart = m_vecStart.GetTailPosition();
		if (posStart)
		{
			*start = (int)m_vecStart.GetAt(posStart);
			bIsStart = TRUE;
		}

		POSITION posNumOf = m_vecNumOf.GetTailPosition();
		if (posNumOf)
		{
			*num_of = (int)m_vecNumOf.GetAt(posNumOf);
			bIsNumOf = TRUE;
		}

		return (bIsDCLS && bIsStart && bIsNumOf);
	}

	void delLastItemData(void)
	{
		if (m_vecDcls.GetCount() > 0)
		{
			m_vecDcls.RemoveTail();
		}
		if (m_vecStart.GetCount() > 0)
		{
			m_vecStart.RemoveTail();
		}
		if (m_vecNumOf.GetCount() > 0)
		{
			m_vecNumOf.RemoveTail();
		}
	}

	void delAllItemData(void)
	{
		if (m_vecDcls.GetCount() > 0)
		{
			m_vecDcls.RemoveAll();
		}
		if (m_vecStart.GetCount() > 0)
		{
			m_vecStart.RemoveAll();
		}
		if (m_vecNumOf.GetCount() > 0)
		{
			m_vecNumOf.RemoveAll();
		}
	}

protected:
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnChar(UINT,UINT,UINT);
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	afx_msg void OnLButtonUp(UINT,CPoint);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////////////////////
//	CMyReportView
class CMyReportView : public CXTPReportView
{
	DECLARE_DYNCREATE(CMyReportView)
	BOOL m_bIsDirty;
public:
	CMyReportView(void);

	BOOL isDirty(void)
	{
		return m_bIsDirty;
	}
	void resetIsDirty(void)
	{
		m_bIsDirty = FALSE;
	}
protected:
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnChar(UINT,UINT,UINT);
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////////////////////
//	CMyPropertyGrid
class CMyPropertyGrid : public CXTPPropertyGrid
{
	DECLARE_DYNCREATE(CMyPropertyGrid)
	BOOL m_bIsDirty;
public:
	CMyPropertyGrid(void);

	BOOL isDirty(void)
	{
		return m_bIsDirty;
	}
	void resetIsDirty(void)
	{
		m_bIsDirty = FALSE;
	}
protected:
	virtual void OnSelectionChanged(CXTPPropertyGridItem* pItem);
	virtual void OnNavigate(XTPPropertyGridUI nUIElement,BOOL bForward,CXTPPropertyGridItem* pItem);

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnChar(UINT,UINT,UINT);
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////////
// CMyTabControl; derived from CXTPTabControl; 060216 p�d

class CMyTabControl : public CXTPTabControl
{
protected:
	void OnItemClick(CXTPTabManagerItem* pItem);

public:
	CMyTabControl();

	void RedrawControl(LPCRECT lpRect,BOOL bAnimate);

	// Return the page that's active in control; 060324 p�d
	CXTPTabManagerItem *getSelectedTabPage(void);

	// Return the page set by index; 060405 p�d
	CXTPTabManagerItem *getTabPage(int idx);

	// Return number of tabs; 060405 p�d
	int getNumOfTabPages(void);
};

//////////////////////////////////////////////////////////////////////////////
// CMyTabControl; derived from CXTExcelTabCtrl; 070208 p�d

class CMyExcelTabControl : public CXTExcelTabCtrl
{
	DECLARE_DYNCREATE(CMyExcelTabControl)
public:
	CMyExcelTabControl();

	CRect getTabsRect(void)
	{
		return GetTabsRect();
	}
protected:
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnRButtonUp(UINT,CPoint);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};


/////////////////////////////////////////////////////////////////////////////
// CMyExtEdit window

class CMyExtEdit : public CXTEdit 
{
//private:
  // default colors
  enum
  {
     ENABLED_FG = RGB(0,0,0), // black
     ENABLED_BG = RGB(255,255,255), // white
     DISABLED_FG = RGB(0,0,0), // black
     DISABLED_BG = RGB(192,192,192), // light grey
  };

//  Implementation
	void RemoveLeadingZeros ( void );

//	Attributes
	LONG	m_nMax;		//	Maximum number the edit control will accept
	LONG	m_nMin;		//	Minimum number the edit control will accept
	LONG	m_nLastValidValue;	//	The last valid value entered by the 

	int m_nItemData;
	CString m_sItemData;

	TCHAR szValue[128];
	TCHAR szIdentifer[128];

	BOOL m_bIsNumeric;	// TRUE if only numeric values are allowed

	BOOL m_bIsAsH100;		// TRUE if the editbox'll set value according to H100; 070508 p�d

	BOOL m_bIsValidate;	// TRUE if validation of numeric range, set in SetRange(...)

	BOOL isNumber(TCHAR *str)
	{
		BOOL bOk = FALSE;
		for (TCHAR *p = str;p < str + _tcslen(str);p++)
		{
			if (isNumeric(*p))
			{
				bOk = TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		return bOk;
		
	}

	BOOL isNumeric(UINT value)
	{
		if ((value >= 96 && value <= 105) || value == 188 || value == 190 || value == 110)
			return TRUE;
		return FALSE;
	}

	CString setAsH100(LPCTSTR str)
	{
		TCHAR szBuffer[32];
		_tcsncpy(szBuffer,str,sizeof(szBuffer) / sizeof(TCHAR));
		// If empty string, return; 070510 p�d
		if (_tcscmp(szBuffer,_T("")) == 0)
			return _T("");

		// Check first character in szBuffer, to see which
		// Specie's selected. I.e. 1 = Pine, 2 = Spruce all rest
		// will be Birch, if not IMP (Impediment) is set; 070508 p�d
		if (_tcscmp(szBuffer,_T("IMP")) == 0 || _tcscmp(szBuffer,_T("imp")) == 0)
			return _T("IMP");

		if (szBuffer[0] == '1' || szBuffer[0] == 'T' || szBuffer[0] == 't')
			szBuffer[0] = 'T';
		else if (szBuffer[0] == '2' || szBuffer[0] == 'G' || szBuffer[0] == 'g')
			szBuffer[0] = 'G';
		else if (szBuffer[0] == '3' || szBuffer[0] == 'B' || szBuffer[0] == 'b')
			szBuffer[0] = 'B';
		else
			szBuffer[0] = '\0';
		return szBuffer;
	}

	// Background brush
  CBrush *m_pbrushDisabled;
  // Foreground brush
  CBrush *m_pbrushEnabled;

	CFont *m_fnt1;

	BOOL	m_bModified;
// Construction
public:
	CMyExtEdit();

// Implementation
	void SetRange ( LONG inLMin, LONG inLMax )
	{
		m_nMin = inLMin;
		m_nMax = inLMax;
		m_bIsValidate = TRUE;
	}

	void setItemIntData(int v)
	{
		m_nItemData = v;
	}
	int getItemIntData(void)
	{
		return m_nItemData;
	}

	void setItemStrData(LPCTSTR v)
	{
		m_sItemData = v;
	}

	CString getItemStrData(void)
	{
		return m_sItemData;
	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtEdit)
	virtual void PreSubclassWindow();
	virtual BOOL PreTranslateMessage(MSG *);
	//}}AFX_VIRTUAL

	void SetReadOnly(BOOL bReadOnly = TRUE);
	void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
	void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);

//	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
//	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetAsNumeric(void);
	void SetAsH100(void);

	virtual ~CMyExtEdit();

	double getFloat(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		return _tstof(szValue);
	}
	void setFloat(double value,int dec)
	{
		_stprintf(szValue,_T("%.*f"),dec,value);
		SetWindowText((LPTSTR)szValue);
	}

	int getInt(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		return _tstoi(szValue);
	}
	void setInt(int value)
	{
		_stprintf(szValue,_T("%d"),value);
		SetWindowText((LPCTSTR)szValue);
	}

	CString getText(void)
	{
		//�ndrat 20100317 h�mtar CString ist�llet f�r tchar storlek 128
		//Annars trunkeras texten i anteckningsf�lt till 128 tecken
		//GetWindowText((LPTSTR)szValue,128);
		if (m_bIsAsH100)
		{
			GetWindowText((LPTSTR)szValue,128);
			return setAsH100(szValue);
		}
		else
		{
			GetWindowText(m_sItemData);
			return m_sItemData;
		}
	}

	void setIdentifer(LPCTSTR id)
	{
		_tcscpy_s(szIdentifer,128,id);
	}

	LPCTSTR getIdentifer(void)
	{
		return (LPCTSTR)szIdentifer;
	}

	BOOL isDirty(void)
	{
		return m_bModified;
	}

	void setIsDirty(void)
	{
		m_bModified = TRUE;
	}

	void resetIsDirty(void)
	{
		m_bModified = FALSE;
	}

	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crFGEnabled;			// Holds the Background Color for the Text for Enabled window
	COLORREF m_crBGEnabled;			// Holds the Color for the Text for Enabled window
	COLORREF m_crFGDisabled;		// Holds the Background Color for the Text for Disabled window
	COLORREF m_crBGDisabled;		// Holds the Color for the Text for Disabled window
	//{{AFX_MSG(CMyExtEdit)
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnUpdate();
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

class CMyMaskExtEdit : public CXTMaskEdit
{
//private:
  // default colors
  enum
  {
     ENABLED_FG = RGB(0,0,0), // black
     ENABLED_BG = RGB(255,255,255), // white
     DISABLED_FG = RGB(0,0,0), // black
     DISABLED_BG = RGB(192,192,192), // light grey
  };

//  Implementation
	void RemoveLeadingZeros ( void );

//	Attributes
	LONG	m_nMax;		//	Maximum number the edit control will accept
	LONG	m_nMin;		//	Minimum number the edit control will accept
	LONG	m_nLastValidValue;	//	The last valid value entered by the 

	int m_nItemData;
	CString m_sItemData;

	TCHAR szValue[128];
	TCHAR szIdentifer[128];

	BOOL m_bIsNumeric;	// TRUE if only numeric values are allowed

	BOOL m_bIsAsH100;		// TRUE if the editbox'll set value according to H100; 070508 p�d

	BOOL m_bIsValidate;	// TRUE if validation of numeric range, set in SetRange(...)

	BOOL isNumber(TCHAR *str)
	{
		BOOL bOk = FALSE;
		for (TCHAR *p = str;p < str + _tcslen(str);p++)
		{
			if (isNumeric(*p))
			{
				bOk = TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		return bOk;
		
	}

	BOOL isNumeric(UINT value)
	{
		if ((value >= 96 && value <= 105) || value == 188 || value == 190 || value == 110)
			return TRUE;
		return FALSE;
	}

	CString setAsH100(LPCTSTR str)
	{
		TCHAR szBuffer[32];
		_tcsncpy(szBuffer,str,sizeof(szBuffer) / sizeof(TCHAR));
		// If empty string, return; 070510 p�d
		if (_tcscmp(szBuffer,_T("")) == 0)
			return _T("");

		// Check first character in szBuffer, to see which
		// Specie's selected. I.e. 1 = Pine, 2 = Spruce all rest
		// will be Birch, if not IMP (Impediment) is set; 070508 p�d
		if (_tcscmp(szBuffer,_T("IMP")) == 0 || _tcscmp(szBuffer,_T("imp")) == 0)
			return _T("IMP");

		if (szBuffer[0] == '1' || szBuffer[0] == 'T' || szBuffer[0] == 't')
			szBuffer[0] = 'T';
		else if (szBuffer[0] == '2' || szBuffer[0] == 'G' || szBuffer[0] == 'g')
			szBuffer[0] = 'G';
		else if (szBuffer[0] == '3' || szBuffer[0] == 'B' || szBuffer[0] == 'b')
			szBuffer[0] = 'B';
		else
			szBuffer[0] = '\0';
		return szBuffer;
	}

	// Background brush
  CBrush *m_pbrushDisabled;
  // Foreground brush
  CBrush *m_pbrushEnabled;

	CFont *m_fnt1;

	BOOL	m_bModified;
// Construction
public:
	CMyMaskExtEdit();

// Implementation
	void SetRange ( LONG inLMin, LONG inLMax )
	{
		m_nMin = inLMin;
		m_nMax = inLMax;
		m_bIsValidate = TRUE;
	}

	void setItemIntData(int v)
	{
		m_nItemData = v;
	}
	int getItemIntData(void)
	{
		return m_nItemData;
	}

	void setItemStrData(LPCTSTR v)
	{
		m_sItemData = v;
	}

	CString getItemStrData(void)
	{
		return m_sItemData;
	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtEdit)
	virtual void PreSubclassWindow();
	virtual BOOL PreTranslateMessage(MSG *);
	//}}AFX_VIRTUAL

	void SetReadOnly(BOOL bReadOnly = TRUE);
  void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
  void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);

//	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
//	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetAsNumeric(void);
	void SetAsH100(void);

	virtual ~CMyMaskExtEdit();

	double getFloat(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		return _tstof(szValue);
	}
	void setFloat(double value,int dec)
	{
		_stprintf(szValue,_T("%.*f"),dec,value);
		SetWindowText((LPTSTR)szValue);
	}

	int getInt(void)
	{
		GetWindowText((LPTSTR)szValue,128);
		return _tstoi(szValue);
	}
	void setInt(int value)
	{
		_stprintf(szValue,_T("%.d"),value);
		SetWindowText((LPCTSTR)szValue);
	}

	CString getText(void)
	{
		//�ndrat 20100317 h�mtar CString ist�llet f�r tchar storlek 128
		//Annars trunkeras texten i anteckningsf�lt till 128 tecken
		//GetWindowText((LPTSTR)szValue,128);
		if (m_bIsAsH100)
		{
			GetWindowText((LPTSTR)szValue,128);
			return setAsH100(szValue);
		}
		else
		{
			GetWindowText(m_sItemData);
			return m_sItemData;
		}
	}

	void setIdentifer(LPCTSTR id)
	{
		_tcscpy_s(szIdentifer,128,id);
	}

	LPCTSTR getIdentifer(void)
	{
		return (LPCTSTR)szIdentifer;
	}

	BOOL isDirty(void)
	{
		return m_bModified;
	}

	void setIsDirty(void)
	{
		m_bModified = TRUE;
	}

	void resetIsDirty(void)
	{
		m_bModified = FALSE;
	}

	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crFGEnabled;			// Holds the Background Color for the Text for Enabled window
	COLORREF m_crBGEnabled;			// Holds the Color for the Text for Enabled window
	COLORREF m_crFGDisabled;		// Holds the Background Color for the Text for Disabled window
	COLORREF m_crBGDisabled;		// Holds the Color for the Text for Disabled window
	//{{AFX_MSG(CMyExtEdit)
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnUpdate();
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtStatic window

class CMyExtStatic : public CStatic
{
// Construction
public:
	CMyExtStatic();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtStatic)
	//}}AFX_VIRTUAL

	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetLblFont(int,int,LPTSTR font_name = _T("Arial"));
	// Set font items except fontface; 071130 p�d
	void SetLblFontEx(int size = -1,int weight = FW_NORMAL,BOOL underline = FALSE,BOOL italic = FALSE);
	virtual ~CMyExtStatic();

protected:
	CString m_sText;
	CFont *m_fnt1;
	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crBkColor; // Holds the Background Color for the Text
	COLORREF m_crTextColor; // Holds the Color for the Text
	//{{AFX_MSG(CMyExtStatic)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor); // This Function Gets Called Every Time Your Window Gets Redrawn.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtTranspStatic

class CMyExtTranspStatic : public CStatic
{
	DECLARE_DYNAMIC(CMyExtTranspStatic)

public:
	CMyExtTranspStatic();
	virtual ~CMyExtTranspStatic();

protected:
	CBrush m_brBkGnd;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};

/////////////////////////////////////////////////////////////////////////////
// CMyExtCBox window

class CMyExtCBox : public CComboBox
{
// Construction
public:
	CMyExtCBox();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtStatic)
	//}}AFX_VIRTUAL

	CString getText(void)
	{
		int nIndex = GetCurSel();
		int nLength;
		CString sBuffer;
		if (nIndex != CB_ERR)
		{
			nLength = GetLBTextLen(nIndex);
			GetLBText(nIndex,sBuffer.GetBuffer(nLength));
			sBuffer.ReleaseBuffer();
			return sBuffer;
		}
		else
			return (LPTSTR)"";
	}

	void SetRO(BOOL flag)
	{
		m_bRO = flag;
		if (m_bRO)
		{
			ModifyStyle(WS_TABSTOP,0);
			EnableWindow(FALSE);
		}
		else
		{
			EnableWindow(TRUE);
			ModifyStyle(0,WS_TABSTOP);
		}

	}

	BOOL isDirty(void)
	{
		return m_bIsDirty;
	}

	void resetIsDirty(void)
	{
		m_bIsDirty = FALSE;
	}

	void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text and the Edit Box.
	void SetTextColor(COLORREF crColor); // This Function is to set the Color for the Text.
	void SetLblFont(int,int,LPTSTR font_name = _T("Arial"));
	virtual ~CMyExtCBox();

protected:
	CString m_sText;
	CFont *m_fnt1;

	CMyExtEdit m_wndEdit;
	CListBox m_wndLB;

	BOOL m_bRO;
	BOOL m_bIsDirty;
	// Generated message map functions
protected:
	CBrush m_brBkgnd; // Holds Brush Color for the Edit Box
	COLORREF m_crBkColor; // Holds the Background Color for the Text
	COLORREF m_crTextColor; // Holds the Color for the Text
	//{{AFX_MSG(CMyExtStatic)
	afx_msg BOOL OnCBoxChange();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////////////////////
// CMyExtListBox window

class CMyExtListBox : public CListBox
{
// Construction
public:
	CMyExtListBox();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyExtStatic)
	//}}AFX_VIRTUAL

	void SetLblFont(int,int,LPTSTR font_name = _T("Arial"));

	virtual ~CMyExtListBox();

protected:
	CFont *m_fnt1;
	// Generated message map functions
protected:
	//{{AFX_MSG(CMyExtStatic)
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////
// CMyComboBox window

class CMyComboBox : public CComboBox
{
// Attributes
private:
   // default colors
   enum
   {
      ENABLED_FG = RGB(0,0,0), // black
      ENABLED_BG = RGB(255,255,255), // white
      DISABLED_FG = RGB(0,0,0), // black
      DISABLED_BG = RGB(192,192,192), // light grey
   };

   // edit mode
   bool m_bEditable;

   // the actual colors
   COLORREF m_crFGEnabled;
   COLORREF m_crBGEnabled;
   COLORREF m_crFGDisabled;
   COLORREF m_crBGDisabled;

	 CFont m_font;
   // Background brush
   CBrush *m_pbrushDisabled;
   // Foreground brush
   CBrush *m_pbrushEnabled;

	 BOOL m_bIsDirty;
// Operations
public:
  void SetReadOnly(BOOL bReadOnly = true);
  void SetReadOnlyEx(BOOL bReadOnly = true);
  void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
  void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetLblFont(int size,int weight,LPTSTR font_name = _T("Arial"));
	CString getText(void);


// Overrides
// ClassWizard generated virtual function overrides
//{{AFX_VIRTUAL(CMyComboBox)
//}}AFX_VIRTUAL

	BOOL isDirty(void)
	{
		return m_bIsDirty;
	}

	void resetIsDirty(void)
	{
		m_bIsDirty = FALSE;
	}

// Implementation
public:
   // Construction
   CMyComboBox(BOOL bEditable = TRUE);
   virtual ~CMyComboBox();

   // Generated message map functions
protected:
  //{{AFX_MSG(CMyComboBox)
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
  afx_msg void OnEnable(BOOL bEnable);
	afx_msg BOOL OnCBoxChange();
	afx_msg BOOL OnCBoxDropDown();
  //}}AFX_MSG

   DECLARE_MESSAGE_MAP()
};


// Author: Robert Cremer

// A ComboBox with easy to switch between edit mode and non edit mode.
// (i. e. works like a drop down or a drop list)
// The ComboBox has real coloring, working in drop down and drop list mode.
// !!! The Box must not be a drop list (CBS_DROPDOWNLIST) !!!
// Working as a drop list can be done with a call to SetReadOnly()
// or with the CTor param false.
class CMyComboBoxHistory : public CHistoryCombo //CComboBox
{
// Attributes
private:
   // default colors
   enum
   {
      ENABLED_FG = RGB(0,0,0), // black
      ENABLED_BG = RGB(255,255,255), // white
      DISABLED_FG = RGB(0,0,0), // black
      DISABLED_BG = RGB(192,192,192), // light grey
   };

   // edit mode
   bool m_bEditable;

   // the actual colors
   COLORREF m_crFGEnabled;
   COLORREF m_crBGEnabled;
   COLORREF m_crFGDisabled;
   COLORREF m_crBGDisabled;

	 CFont m_font;
   // Background brush
   CBrush *m_pbrushDisabled;
   // Foreground brush
   CBrush *m_pbrushEnabled;

	 BOOL m_bIsDirty;
// Operations
public:
  void SetReadOnly(BOOL bReadOnly = true);
  void SetReadOnlyEx(BOOL bReadOnly = true);
  void SetEnabledColor(COLORREF crFG = ENABLED_FG, COLORREF crBG = ENABLED_BG);
  void SetDisabledColor(COLORREF crFG = DISABLED_FG, COLORREF crBG = DISABLED_BG);
	void SetLblFont(int size,int weight,LPTSTR font_name = _T("Arial"));
	CString getText(void);


// Overrides
// ClassWizard generated virtual function overrides
//{{AFX_VIRTUAL(CMyComboBoxHistory)
//}}AFX_VIRTUAL

	BOOL isDirty(void)
	{
		return m_bIsDirty;
	}

	void resetIsDirty(void)
	{
		m_bIsDirty = FALSE;
	}

// Implementation
public:
   // Construction
   CMyComboBoxHistory(BOOL bEditable = TRUE);
   virtual ~CMyComboBoxHistory();

   // Generated message map functions
protected:
  //{{AFX_MSG(CMyComboBoxHistory)
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
  afx_msg void OnEnable(BOOL bEnable);
	afx_msg BOOL OnCBoxChange();
	afx_msg BOOL OnCBoxDropDown();
  //}}AFX_MSG

   DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////
// CMyListBox

typedef struct _column
{
	int _col_width;
	BOOL _is_bold;
	BOOL _is_underline;
	BOOL _is_italic;
	BOOL _is_strike_through;
	int _align;

	_column(void)
	{
		_col_width = 0;
		_align = DT_LEFT;	// As default
		_is_bold = FALSE;
		_is_underline = FALSE;
		_is_italic = FALSE;
		_is_strike_through = FALSE;
	}
	_column(int col_width,int align,BOOL is_bold,BOOL is_underline,BOOL is_italic,BOOL is_strike_through)
	{
		_col_width = col_width;
		_align = align;	// As default
		_is_bold = is_bold;
		_is_underline = is_underline;
		_is_italic = is_italic;
		_is_strike_through = is_strike_through;
	}

} COLUMN;


typedef struct _column_color
{
	COLORREF _text_color;
	COLORREF _bkgnd_color;

	_column_color(void)
	{
		_text_color = RGB(0,0,0);	// Text black
		_bkgnd_color = ::GetSysColor(COLOR_BTNFACE);
	}
	_column_color(COLORREF text,COLORREF bkgnd)
	{
		_text_color = text;
		_bkgnd_color = bkgnd;
	}

} COLUMN_COLOR;

class CMyListBox : public CListBox
{
	BOOL m_bInitialized;
	BOOL m_bDrawFirstRow;
	CString m_sText;
	CFont m_font;
	std::vector<COLUMN> m_vecColumns;
	std::vector<COLUMN_COLOR> m_vecColumnsColor;
	COLORREF m_colBackGnd;
	COLORREF m_colText;
	int m_nColStart;

	int SplitString(const CString& input,const CString& delimiter, CStringArray& results);

public:
	CMyListBox();
	virtual ~CMyListBox();
	void Init(CWnd *parent,CRect rect,UINT id);

	void ResetContent();

	void setColumn(int column_start,int align,
								 BOOL is_bold = FALSE,BOOL is_underline = FALSE,
								 BOOL is_italic = FALSE,BOOL is_strike_through = FALSE);
	void setColumnColor(COLORREF text = RGB(0,0,0),COLORREF bkgnd = ::GetSysColor(COLOR_BTNFACE));

	void setBkColor(COLORREF col);
	void setColStart(int col_start);

protected:
	virtual	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct); 

	//{{AFX_MSG(CDBListBox)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////
// CMyXTButton
class CMyXTButton : public CXTButton
{
protected:
	virtual	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct); 
public:
	CMyXTButton();
};

//////////////////////////////////////////////////////////////////////////
// CMyHtmlView based on CXTHtmlView; 090902 p�d


typedef enum { HTM_FREE_FMT,HTM_HEAD1,HTM_HEAD2,HTM_HEAD3,HTM_HEAD4,HTM_PLAIN,HTM_BOLD,HTM_ULINE,HTM_ITALIC,HTM_BOLD_ULINE,HTM_BOLD_ULINE_ITALIC,HTM_ULINE_ITALIC } HTM_TYPES;
typedef enum { HTM_ALIGN_LEFT,HTM_ALIGN_CENTER,HTM_ALIGN_RIGHT } HTM_ALIGN;
typedef enum { HTM_COLOR_BLACK,
							 HTM_COLOR_RED,
							 HTM_COLOR_GREEN,
							 HTM_COLOR_BLUE,
							 HTM_COLOR_YELLOW,
							 HTM_COLOR_CYAN,
							 HTM_COLOR_MAGENTA,
							 HTM_COLOR_GRAY,
							 HTM_COLOR_WHITE 
							} HTM_COLORS;

typedef enum { HTM_AUTO,HTM_FIXED } HTM_COL_LAYOUT;

typedef enum { HTM_HR_SHADE,HTM_HR_NOSHADE } HTM_HR_LAYOUT;

typedef std::map<int,int> mapColWidths;

class CMyHtmlView : public CXTPOfficeBorder<CXTHtmlView>
{
protected:
	CMyHtmlView() {}           // protected constructor used by dynamic creation

	CStringArray m_sarrHTMText;
	CString m_sHTM;
	CString m_sHTML_FileName;


	void startHTM(void);
	void endHTM(void);

	void setHTM_text(LPCTSTR txt,HTM_TYPES type,short font_size=2,LPCTSTR font_name=_T(""),HTM_COLORS color=HTM_COLOR_BLACK,bool bold=false,bool italic=false,bool uline=false);

	void setHTML_linefeed(short numof=1);
	void setHTML_line(HTM_HR_LAYOUT hr_layout,short hr_size=1);

	void setHTM_table(void);
	void setHTM_start_table_column(void);
	void setHTM_table_column(LPCTSTR txt,short width,short font_size=2,LPCTSTR font_name=_T(""),HTM_COLORS color=HTM_COLOR_BLACK,HTM_ALIGN align=HTM_ALIGN_LEFT,bool bold=false,bool italic=false,bool uline=false);
	void setHTM_table_column(int num,short width,short font_size=2,LPCTSTR font_name=_T(""),HTM_COLORS color=HTM_COLOR_BLACK,HTM_ALIGN align=HTM_ALIGN_LEFT,bool bold=false,bool italic=false,bool uline=false);
	void setHTM_table_column(double num,short width,short dec,short font_size=2,LPCTSTR font_name=_T(""),HTM_COLORS color=HTM_COLOR_BLACK,HTM_ALIGN align=HTM_ALIGN_LEFT,bool bold=false,bool italic=false,bool uline=false);
	void setHTM_end_table_column(void);
	void endHTM_table(void);


// html Data
public:
	//{{AFX_DATA(CMyHtmlView)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:
	// Public contructor
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyHtmlView)
	public:
	virtual void OnInitialUpdate();
	virtual void printOutHTM();
	virtual CString getHTM(void);
	protected:
	virtual void showHTM(void);
	virtual void setHTMFileName(LPCTSTR  html_filename);  
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	// Make this class abstract and virtual. I.e. must ber derived; 090902 p�d
	virtual void setupHTM(void) = 0;
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CMyHtmlView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CMyHtmlView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////
// Struct's used e.g. in functions, goes here; 060215 p�d
typedef struct _network_users
{
	TCHAR szUserName[128];
	TCHAR szUserFullName[128];
	TCHAR szComment[255];
	DWORD	nTypeOf;				// 1 = Users, 2 = Local groups, 3 = groups
	DWORD nPrivelige;			// User privilige, e.g. 2 = Administrator

	// Constructor(s)
	_network_users(void)
	{
		_tcscpy_s(szUserName,128,(_T("")));
		_tcscpy_s(szUserFullName,128,(_T("")));
		_tcscpy_s(szComment,255,(_T("")));
		nTypeOf			= 0;
		nPrivelige	= 0;
	}

	_network_users(LPCTSTR user_name,LPCTSTR user_full_name,LPCTSTR comment,DWORD typeof,DWORD priv)
	{
		_tcscpy_s(szUserName,128,(user_name));
		_tcscpy_s(szUserFullName,128,(user_full_name));
		_tcscpy_s(szComment,255,(comment));
		nTypeOf			= typeof;
		nPrivelige	= priv;
	}

} NETWORK_USERS;

typedef std::vector<NETWORK_USERS> vecNETWORK_USERS;

//////////////////////////////////////////////////////////////////////////////////////////
// USER MESSAGE STRUCT; Used when communicaton with HMSShell.
// This stucture must be in each Suite/User module when communicating with HMSShell
// on USER_MSG_xxxx_xxxx
class _user_msg
{
//private:
	int m_nIndex;									// See ShellData <ExecItem> or <ReportItem>
/*	char m_szFunc[128];					// See ShellData <ExecItem> or <ReportItem>
	char m_szSuite[128];					// See ShellData <ExecItem> or <ReportItem>
	char m_szName[128];					// See ShellData <ExecItem> or <ReportItem>
	char m_szFileName[MAX_PATH];	// See ShellData <ExecItem> or <ReportItem>
	char m_szArgStr[MAX_PATH];				// Can containe a string of arguments, added to a
*/																// reports "variables". E.g. arg1;arg2;arg3;etc..; 070207 p�d
	TCHAR m_szFunc[128];					// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szSuite[128];					// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szName[128];					// See ShellData <ExecItem> or <ReportItem>
	TCHAR m_szFileName[MAX_PATH];	// See ShellData <ExecItem> or <ReportItem>
	// Changed size from MAX_PATH to 2048 (2kb); 2009-01-07 P�D (Enl. Anders G.)
	TCHAR m_szArgStr[2048];				// Can containe a string of arguments, added to a
																// reports "variables". E.g. arg1;arg2;arg3;etc..; 070207 p�d
	// Added 2009-01-07 P�D (Enl. Anders G.)
	void *m_pBuf;									// void* for generic use

public:
	_user_msg(void)
	{
		m_nIndex = -1;
/*		strcpy_s(m_szFunc,128,(""));
		strcpy_s(m_szSuite,128,(""));
		strcpy_s(m_szName,128,(""));
		strcpy_s(m_szFileName,MAX_PATH,(""));
		strcpy_s(m_szArgStr,MAX_PATH,(""));*/
		_tcscpy_s(m_szFunc,128,_T(""));
		_tcscpy_s(m_szSuite,128,_T(""));
		_tcscpy_s(m_szName,128,_T(""));
		_tcscpy_s(m_szFileName,MAX_PATH,_T(""));
		_tcscpy_s(m_szArgStr,MAX_PATH,_T(""));
		// Added 2009-01-07 P�D (Enl. Anders G.)
		void *m_pBuf = NULL;
	}
	_user_msg(int i1,LPCTSTR str1,LPCTSTR str2,LPCTSTR str3,LPCTSTR str4,LPCTSTR str5,void *pBuf = NULL)
//	_user_msg(int i1,LPCTSTR str1,LPCTSTR str2,LPCSTR str3,LPCSTR str4,LPCSTR str5)
	{
		m_nIndex = i1;
		/*strcpy_s(m_szFunc,128,(str1));
		strcpy_s(m_szSuite,128,(str2));
		strcpy_s(m_szName,128,(str3));
		strcpy_s(m_szFileName,MAX_PATH,(str4));
		strcpy_s(m_szArgStr,MAX_PATH,(str5));*/
		_tcscpy_s(m_szFunc,128,(str1));
		_tcscpy_s(m_szSuite,128,(str2));
		_tcscpy_s(m_szName,128,(str3));
		_tcscpy_s(m_szFileName,MAX_PATH,(str4));
		_tcscpy_s(m_szArgStr,MAX_PATH,(str5));
		// Added 2009-01-07 P�D (Enl. Anders G.)
		m_pBuf = pBuf;
	}
	_user_msg(const _user_msg &c)
	{
		*this = c;
	}

	int getIndex(void)				{ return m_nIndex; }

	/*LPCSTR getFunc(void)			{ return m_szFunc; }
	LPCSTR getSuite(void)			{ return m_szSuite; }
	LPCSTR getName(void)			{ return m_szName; }
	LPCSTR getFileName(void)	{ return m_szFileName; }
	LPCSTR getArgStr(void)		{ return m_szArgStr; }*/
	LPCTSTR getFunc(void)			{ return m_szFunc; }
	LPCTSTR getSuite(void)			{ return m_szSuite; }
	LPCTSTR getName(void)			{ return m_szName; }
	LPCTSTR getFileName(void)	{ return m_szFileName; }
	LPCTSTR getArgStr(void)		{ return m_szArgStr; }
	// Added 2009-01-07 P�D (Enl. Anders G.)
	void* getVoidBuf(void)		{ return m_pBuf; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// class _doc_identifer_msg
// USe this class to send LPARAM message between a Suite/Module to another.
// This message WPARAM = ID_WPARAM_VALUE_FROM + 0x02; 070813 p�d
class _doc_identifer_msg
{
//	char m_szSendFrom[128];		// Identifer for dcoument, e.g. Module5000 etc
//	char m_szSendTo[128];		// Identifer for dcoument, e.g. Module5000 etc
//	char m_szDirectTo[128];		// Identifer for dcoument, e.g. Module5000 etc
	TCHAR m_szSendFrom[128];		// Identifer for dcoument, e.g. Module5000 etc
	TCHAR m_szSendTo[128];		// Identifer for dcoument, e.g. Module5000 etc
	TCHAR m_szDirectTo[128];		// Identifer for dcoument, e.g. Module5000 etc
	int m_nValue1;						// Misc. integer value
	int m_nValue2;						// Misc. integer value
	int m_nValue3;						// Misc. integer value
public:
	_doc_identifer_msg(void)
	{
//		strcpy_s(m_szSendFrom,128,(""));		// Identifer for dcoument, e.g. Module5000 etc
//		strcpy_s(m_szSendTo,128,(""));		// Identifer for dcoument, e.g. Module5000 etc
//		strcpy_s(m_szDirectTo,128,(""));		// Identifer for dcoument, e.g. Module5000 etc
		_tcscpy_s(m_szSendFrom,128,_T(""));		// Identifer for dcoument, e.g. Module5000 etc
		_tcscpy_s(m_szSendTo,128,_T(""));		// Identifer for dcoument, e.g. Module5000 etc
		_tcscpy_s(m_szDirectTo,128,_T(""));		// Identifer for dcoument, e.g. Module5000 etc
		m_nValue1	= -1;						// Misc. integer value
		m_nValue2	= -1;						// Misc. integer value
		m_nValue3	= -1;						// Misc. integer value
	}
//	_doc_identifer_msg(LPCSTR send_from,LPCSTR send_to,LPCSTR direct_to,int v1,int v2,int v3)
	_doc_identifer_msg(LPCTSTR send_from,LPCTSTR send_to,LPCTSTR direct_to,int v1,int v2,int v3)
	{
//		strcpy_s(m_szSendFrom,128,(send_from));		// Identifer for dcoument, e.g. Module5000 etc
//		strcpy_s(m_szSendTo,128,(send_to));		// Identifer for dcoument, e.g. Module5000 etc
//		strcpy_s(m_szDirectTo,128,(direct_to));		// Identifer for dcoument, e.g. Module5000 etc
		_tcscpy_s(m_szSendFrom,128,(send_from));		// Identifer for dcoument, e.g. Module5000 etc
		_tcscpy_s(m_szSendTo,128,(send_to));		// Identifer for dcoument, e.g. Module5000 etc
		_tcscpy_s(m_szDirectTo,128,(direct_to));		// Identifer for dcoument, e.g. Module5000 etc
		m_nValue1	= v1;						// Misc. integer value
		m_nValue2	= v2;						// Misc. integer value
		m_nValue3	= v3;						// Misc. integer value
	}

//	LPCSTR getSendFrom(void)	{ return m_szSendFrom; }
//	LPCSTR getSendTo(void)		{ return m_szSendTo; }
//	LPCSTR getDirectTo(void)	{ return m_szDirectTo; }
	LPCTSTR getSendFrom(void)	{ return m_szSendFrom; }
	LPCTSTR getSendTo(void)		{ return m_szSendTo; }
	LPCTSTR getDirectTo(void)	{ return m_szDirectTo; }
	int getValue1(void)				{ return m_nValue1;	}
	int getValue2(void)				{ return m_nValue2;	}
	int getValue3(void)				{ return m_nValue3;	}
};

//////////////////////////////////////////////////////////////////////////////////////////
// defines for FormViews in Suites or UserModules used as ID for server engine selected; 
#define ID_MYSQL			                6000	// UMDataBase MySQL handling
#define ID_SQLSERVER		              6001	// UMDataBase Microsoft SQL Server handling

// _dbservers

typedef struct _dbservers
{
	int nID;
	CString sDBServerName;
	SAClient_t saClient;
	CString sDBLocation;
	CString sDSNName;

	_dbservers(void)
	{
		nID						= -1;
		sDBServerName = ("");
		saClient			= SA_Client_NotSpecified;
		sDBLocation		= ("");
		sDSNName			= ("");
	}

	_dbservers(int id,LPCTSTR db_name,SAClient_t client,LPCTSTR location,LPCTSTR dsn_name)
	{
		nID						= id;
		sDBServerName = (db_name);
		saClient			= client;
		sDBLocation		= (location);
		sDSNName			= (dsn_name);
	}
} DBSERVERS;

typedef std::vector<_dbservers> vecDBSERVERS;


// _dbservers

typedef struct _admin_ini_databases
{
	CString sDBServerName;
	CString sLocation;
	CString sDSNName;
	CString sDBClient;

	_admin_ini_databases(void)
	{
		sDBServerName = ("");
		sLocation			= ("");
		sDSNName			= ("");
		sDBClient			= ("");
	}

	_admin_ini_databases(LPCTSTR db_name,LPCTSTR location,LPCTSTR dsn_name,LPCTSTR db_client)
	{
		sDBServerName = (db_name);
		sLocation			= (location);
		sDSNName			= (dsn_name);
		sDBClient			= (db_client);
	}
} ADMININIDATABASES;

typedef std::vector<_admin_ini_databases> vecADMIN_INI_DATABASES;

//////////////////////////////////////////////////////////////////////////////////
// CNewsCategoriesSetupItem
class CNewsCategoriesSetupItem
{
	BOOL m_bDisplay;	// TRUE = Display category
	TCHAR m_szCategory[128];	// Name of category
public:
	CNewsCategoriesSetupItem(void)
	{
		m_bDisplay		= TRUE;
		_tcscpy_s(m_szCategory,128,_T(""));
	}

	CNewsCategoriesSetupItem(BOOL display,LPCTSTR category)
	{
		m_bDisplay		= display;
		_tcscpy_s(m_szCategory,128,category);
	}

	CNewsCategoriesSetupItem(const CNewsCategoriesSetupItem &c)
	{
		*this = c;
	}

	BOOL getDisplay(void)			{ return m_bDisplay;	}
	LPCTSTR getCategory(void)	{ return m_szCategory;	}
};

typedef std::vector<CNewsCategoriesSetupItem> vecNewsCategoriesSetup;


// This struct holds index and name of a loaded form; 051212 p�d
typedef struct _index_table
{
	int nTableIndex;
	TCHAR szLanguageFN[MAX_PATH];
	TCHAR szSuite[MAX_PATH];
	BOOL bOneInstance;

	_index_table(int n,LPCTSTR suite,LPCTSTR lang_fn,BOOL one_inst)
	{
		nTableIndex = n;
		_tcscpy_s(szLanguageFN, lang_fn);
		_tcscpy_s(szSuite, suite);
		bOneInstance = one_inst;
	}
} INDEX_TABLE;

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;

// This struct holds information on version etc; 060803 p�d
typedef struct _info_table
{
	int nStrIndex;
	int nType;
	TCHAR szLanguageFN[MAX_PATH];
	TCHAR szVersion[15];
	TCHAR szCopyright[128];
	TCHAR szCompany[128];

	_info_table(int idx, int type, LPCTSTR lang_fn, LPCTSTR version, LPCTSTR copy_right, LPCTSTR company)
	{
		nStrIndex = idx;
		nType	= type;
		_tcscpy_s(szLanguageFN,MAX_PATH,lang_fn);
		_tcscpy_s(szVersion,15,version);
		_tcscpy_s(szCopyright,128,copy_right);
		_tcscpy_s(szCompany,128,company);
	}
} INFO_TABLE;

typedef std::vector<INFO_TABLE> vecINFO_TABLE;

#define STR_LEN 255

//////////////////////////////////////////////////////////////////////////////////////////
// struct_LatLongToRT90, used in function 'SweRef99LatLongtoRT90xy'; 100317 p�d
class CLatLongToRT90_data
{
private:
  double m_fLatitude;
  double m_fLongitude;
  double m_fX;
  double m_fY;
  double m_fHgt;
public:
	CLatLongToRT90_data()
	{
		m_fLatitude = 0.0;
		m_fLongitude = 0.0;
		m_fX = 0.0;
		m_fY = 0.0;
		m_fHgt = 0.0;
	}
	CLatLongToRT90_data(double _lat,double _long,double _hgt = 0.0)
	{
		m_fLatitude = _lat;
		m_fLongitude = _long;
		m_fHgt = _hgt;	// Set to 0.0 for now; 100317 p�d
	}

	double getLat()			{ return m_fLatitude; }
	double getLong()		{ return m_fLongitude; }

	double getX()				{ return m_fX; }
	void setX(double v)	{ m_fX = v; }
	double getY()				{ return m_fY; }
	void setY(double v)	{ m_fY = v; }
		
	double getHgt()			{ return m_fHgt; }
	//double area;
  //char  zon_letter;
  //unsigned short int zon;
  //unsigned short int status;
};

//////////////////////////////////////////////////////////////////////////////////////////
// _languages
class _languages
{
public:
	_languages(void)
	{
		_tcscpy_s(szLngAbrev,10,_T(""));	// Abbrevated language name
		_tcscpy_s(szLng,STR_LEN,_T(""));	// Abbrevated language name
	}

	_languages(LPCTSTR s1,LPCTSTR s2)
	{
		_tcscpy_s(szLngAbrev,10,s1);
		_tcscpy_s(szLng,STR_LEN,s2);
	}

	_languages(_languages &c)
	{
		*this = c;
	}

	TCHAR szLngAbrev[10];
	TCHAR szLng[STR_LEN];
};



//////////////////////////////////////////////////////////////////////////////////
// CSuiteModuleInfoParser

// Handle Suite and User module info into XML file; 060803 p�d
// Process instruction on creating XML file; 060803 p�d
const LPCTSTR PI_XML_NAME						= _T("xml");
const LPCTSTR PI_VER_ENCODE						= _T("version='1.0' encoding='UTF-8'");

const LPCTSTR SUITE_MODULE_FILE_NAME			= _T("SuiteModuleConfig.xml");	
const LPCTSTR XML_EXT							= _T(".xml");
// Tags used in Suite/Module info XML-file; 060803 p�d
const LPCTSTR TAG_ROOT							= _T("data");
const LPCTSTR TAG_SUITE							= _T("suite");
const LPCTSTR TAG_USER_MODULE					= _T("user_module");

const LPCTSTR TAG_ELEM_STRID					= _T("strid");
const LPCTSTR TAG_ELEM_LANG_FILE				= _T("lang_file");
const LPCTSTR TAG_ELEM_VERSION					= _T("version");
const LPCTSTR TAG_ELEM_COPYRIGHT				= _T("copyright");
const LPCTSTR TAG_ELEM_COMPANY					= _T("company");

const LPCTSTR TAG_ROOT_WC						= _T("data/*");
const LPCTSTR TAG_SUITE_WC						= _T("suite/*");
const LPCTSTR TAG_USER_MODULE_WC				= _T("user_module/*");

// Name of module handling Calculations for Height,Volume, Bark etc; 070416 p�d
const LPCTSTR CALCULATION_MODULE				= _T("UCCalculate.dll");
// Name of functions in Calculation module; 070416 p�d
const LPCSTR GET_HEIGHT_FUNCTIONS				= ("getHeightFunctions");
const LPCSTR GET_HEIGHT_FUNCTION_LIST			= ("getHeightFunctionList");

const LPCSTR GET_VOLUME_FUNCTIONS				= ("getVolumeFunctions");
const LPCSTR GET_VOLUME_FUNCTION_LIST			= ("getVolumeFunctionList");

const LPCSTR GET_BARK_FUNCTIONS					= ("getBarkFunctions");
const LPCSTR GET_BARK_FUNCTION_LIST				= ("getBarkFunctionList");

const LPCSTR GET_VOLUME_UB_FUNCTIONS			= ("getVolumeFunctions_ub");
const LPCSTR GET_VOLUME_UB_FUNCTION_LIST		= ("getVolumeFunctionList_ub");

const LPCSTR GET_GROT_FUNCTIONS						= ("getGROTFunctionsCalc");

// LogScale LogScaleVolFunc.dll
const LPCTSTR	LOGSCALE_VOLUME_MODULE			= _T("LogScaleVolumeFunctions.dll");
const LPCSTR LOGSCALE_FUNCDESC_METHOD     = "getDLLVolFuncs";
const LPCSTR LOGSCALE_CALCULATE_METHOD    = "calculateDLLVol";
const LPCSTR LOGSCALE_CALCULATE_METHOD2   = "calculateDLLVolLogs";
const LPCSTR LOGSCALE_CALCULATE_METHOD3   = "calculateDLLVolLogsUserFunctions";

// EXPORTED FUNCTION NAMES
// Name of exported function in UCCalcualtion.dll, doin' the
// actual calculations; 070521 p�d
const LPCSTR GET_DO_CALCULTION					= "doCalculation";

const LPCSTR GET_DO_GROT_CALCULTION			= "doGROTCalc";

// Name of module handling Exchange ("Utbyte") calculations; 070430 p�d
const LPCTSTR EXCHANGE_MODULE					= _T("UMExchange.dll");
const LPCSTR GET_EXCH_FUNCTIONS					= ("getExchFunctions");

// Name of module handling UMEstimate calculations; 070430 p�d
const LPCTSTR UMEstimate_module_name					= _T("UMEstimate.dll");
const LPCTSTR UMEstimate_exported_function_calculate	= _T("remoteCalcualtion");

// Name of module handling Forrest norm ("Intr�ngsv�rdering") calculations; 071109 p�d
const LPCTSTR LANDVALUE_MODULE					= _T("UCLandValueNorm.dll");
const LPCSTR GET_LANDVALUE_FUNCTIONS			= ("getLandValueFunctions");
const LPCSTR GET_LANDVALUE_TYPEOF_INFRING		= ("getLandValueTypeOfInfring");

// Name of exported function in UMExchange.dll, doin' the
// actual calculations; 070525 p�d
const LPCSTR GET_DO_EXCHANGE					= ("doExchange");

const LPCSTR GET_DO_EXCHANGE_SIMPLE		= ("doExchangeSimple");

// Directory in my documents, holding sudbdirectories for inventory data
// Directory name = "Object name + Object id"
// E.g. My Documants\HMS\Obejctname_ObjectID
const LPCTSTR INVENTORY_DATA_MAIN_DIRECTORY		= _T("HMS\\LandValue");

//////////////////////////////////////////////////////////////////////////////////////////
// this enuerated type is used in exported function: getExportedFuncName in UMDataBase; 060317 p�d
typedef enum { 
								SPC_INS_UPD,
								SPC_REMOVE,
								SPC_GET,

								PRICELIST_INS_UPD,
								PRICELIST_REMOVE,
								PRICELIST_GET
						 } enumFuncNames;

//////////////////////////////////////////////////////////////////////////////////////////
// Name of the database holding tabels for administration; 060217 p�d
const LPCTSTR ADMIN_DB_NAME					= _T("hms_administrator");

//////////////////////////////////////////////////////////////////////////////////////////
// Registry settings; 060303 p�d
const LPCTSTR REG_ROOT						= _T("SOFTWARE\\HaglofManagmentSystem");

const LPCTSTR REG_KEY_DATABASE				= _T("Database");
const LPCTSTR REG_STR_DBLOCATION			= _T("DBLocation");
const LPCTSTR REG_STR_DBSERVER				= _T("DBServer");
const LPCTSTR REG_STR_USERNAME				= _T("UserName");
const LPCTSTR REG_STR_PSW					= _T("Password");
const LPCTSTR REG_STR_SELDB					= _T("DBSelected");
const LPCTSTR REG_DB_QUALIFIED_ITEM			= _T("IsQualified");	// Tell us if user is qualified to change database settings; 
const LPCTSTR REG_DB_CONNECTED_ITEM			= _T("IsConnected");	// Set a key on if there's a conenction to a Database server or not; 070329 p�d
const LPCTSTR REG_DB_AUTHENTICATION_ITEM	= _T("IsWindowsAuthentication");	// Set a key on if Windows Authentication  = 0 or Server Authentication = 1
const LPCTSTR REG_DB_DBBACKUP_ALLOWED		= _T("DBBackupAllowed");	// Tell if user's allowed to do backup of database; 081006 p�d
const LPCTSTR REG_DB_DBRESTORE_ALLOWED		= _T("DBRestoreAllowed");	// Tell if user's allowed to do restore of database; 081006 p�d
const LPCTSTR REG_DB_DBCOPY_ALLOWED			= _T("DBCopyAllowed");	// Tell if user's allowed to do copy of database; 081006 p�d

const LPCTSTR REG_FORREST_KEY				= _T("Forrest");
const LPCTSTR REG_SETUP_TABLES_ITEM			= _T("SetupTables");

const LPCTSTR REG_WEB_NEWS_ROOT				= _T("WEB_NEWS");
const LPCTSTR REG_WEB_NEWS_URL				= _T("URL");			// Address to web page. E.g. http://hms-haglof.se; 060801
const LPCTSTR REG_WEB_NEWS_FILEPATH			= _T("FILEPATH");	// Location of file to read on the web server; 060801 p�d
const LPCTSTR REG_WEB_NEWS_PUBDATE			= _T("PUBDATE");	// Location of file to read on the web server; 060801 p�d
const LPCTSTR REG_WEB_NEWS_PER_CAT			= _T("PERMANENT_CATEGORIES");	// String indication which categoris are 
																															// permanent (i.e. can't be unselected);
const LPCTSTR REG_WEB_CONNECT_KEY			= _T("Settings");	// 
const LPCTSTR REG_WEB_CONNECT				= _T("rss");	// DWORD 1 = WEB Connected, 0 = WEB not connected; 081202 p�d
const LPCTSTR REG_IS_NAVBAR_KEY				= _T("NavBar");	// A setting to check if user opend from NavigationBar; 090217 p�d
const LPCTSTR REG_IS_NAVBAR					= _T("from_navbar");	// A setting to check if user opend from NavigationBar; 090217 p�d
const LPCTSTR REG_RELEASE					= _T("Release");	// Release number of Installation; 090520 p�d

const LPCTSTR REG_LANG_KEY					= _T("Language");
const LPCTSTR REG_LANG_ITEM					= _T("LANGSET");		// Item in registry, for abbrevated language; 051114 p�d
const LPCTSTR LANGUAGE_FN_EXT				= _T(".xml");				// Uses ordinary xm-file

const LPCTSTR INIT_MODULE_FUNC				= _T("InitModule");				// Functionname in Module, for init; 051129 p�d


const LPCTSTR REG_LANDVALUE_KEY				= _T("UMLandValue");	// OBS! Use registry entry, also used by placement etc.
const LPCTSTR REG_LANDVALUE_ITEM			= _T("InventoryDirectory");
const LPCTSTR REG_LANDVALUE_XMLJSON			= _T("ExportxmljsonDirectory");

//////////////////////////////////////////////////////////////////////////////////////////
// Server client names, used in admin.ini [DATABASES] -> DB_CLIENTS
const LPCTSTR MySQL							= _T("MySQL");
const LPCTSTR SQLServer						= _T("SQLServer");
const LPCTSTR OracleServer					= _T("OracleServer");

///////////////////////////////////////////////////////////////////////////////
// Shortcuts Shelldata xml-filename; 060425 p�d

const LPCTSTR SHELLDATA_ShortcutsFN			= _T("Shortcuts.xml");

///////////////////////////////////////////////////////////////////////////////
// Administrtation Shelldata xml-filename; 060425 p�d

const LPCTSTR SHELLDATA_AdministrationFN	= _T("Administration.xml");

//////////////////////////////////////////////////////////////////////////////////////////
// Constants for new bulletine category setup file; 060809 p�d
const LPCTSTR SUBDIR_SHELL_DATA				= _T("HMS\\ShellData");	// Subdir. holds shell tree xml files; 051115 p�d
const LPCTSTR SUBDIR_HMS_APPDATA			= _T("HMS");						// Subdir. HMS in Documants & Settings ApplicationData; 060804 p�d
const LPCTSTR NEW_CATEGORIES_FILE_NAME		= _T("NewsCategoriesSetup.bin");	// Holds information on selected news, from user; 060809 p�d
const LPCTSTR SUBDIR_MODULES				= _T("Modules");					// All modules are in subdirs of the Suites directory; 051117 p�d
const LPCTSTR SUBDIR_LANGUAGE				= _T("Language");					// Language directory
const LPCTSTR SUBDIR_REPORTS				= _T("Reports");					// Reports directory
const LPCTSTR SUBDIR_SETUPS					= _T("Setup");					// Reports directory
const LPCTSTR SUBDIR_SCRIPTS				= _T("SQL script");

const LPCTSTR SUBDIR_SUITES					= _T("Suites");	// Directory which holds the suites

const LPCTSTR SHELLDATA_FN_WC				= _T("*.xml");			// Wildcard for xml files; 051115 p�d

const LPCTSTR DEF_LANGUAGE_ABREV			= _T("ENU");				// Set American English as default language; 080328 p�d
//////////////////////////////////////////////////////////////////////////////////////////
// Functions

BOOL fileExists(LPCTSTR fn);

CString getProgDir(void);
CString getTempDir(void);
CString getTempFilePath(void);
CString getPathToCommonAppData(void);

CString extractFileName(LPCTSTR fn);
CString extractFilePath(LPCTSTR fn);
CString extractFileNameNoExt(LPCTSTR fn);


// Get todays date and time in a string; 060111 p�d
CString getYearMonth(void);
CString getDate(void);
CString getDateEx(void);
CString getDateTime(void);
CString getDateTimeEx(void);
CString getDateTimeEx2(void);
CString getDBDateTime(void);

BOOL getTimeSeparated(LPCTSTR dt_str,int *h,int *m,int *s);
BOOL compTimes(LPCTSTR dt_str0,LPCTSTR dt_str1);

BOOL regGetBin(LPCTSTR root,LPCTSTR key, LPCTSTR item,BYTE** ppData, UINT* pBytes);
BOOL regSetBin(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPBYTE pData, UINT nBytes);

void regSetInt(LPCTSTR root,LPCTSTR key, LPCTSTR item,DWORD value);
DWORD regGetInt(LPCTSTR root,LPCTSTR key, LPCTSTR item,int def_value = -1);
DWORD regGetInt_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,int def_value = -1);

void regSetStr(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR value);
void regSetStr_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR value);
CString regGetStr(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR def_str = _T(""));
CString regGetStr_LM(LPCTSTR root,LPCTSTR key, LPCTSTR item,LPCTSTR def_str = _T(""));

CString regGetSQLServerInstance(void);

void regGetSQLServerInstances(CStringArray &arr);

CString createUserID(void);

void splitInfo(const TCHAR *str,int *items,int *len,TCHAR sep);
TCHAR* split(const TCHAR *str,int colNum,TCHAR sep);


void getWindowPlacement(LPCTSTR root,						// Root in registry
												WINDOWPLACEMENT *wp,		// Placement for window structure
												RECT def_placement			// A default value, if there's no entry in registry
												);

void setWindowPlacement(LPCTSTR root,				// Root in registry
												CWnd *wnd						// Window to save settings
												);

void LoadPlacement(CWnd *pwnd,LPCTSTR pszSection);
void SavePlacement(CWnd *pwnd,LPCTSTR pszSection);

// Network functions; 060114 p�d

BOOL isAdmin(void);

BOOL getNetworkUserList(vecNETWORK_USERS &);

BOOL getNetworkServersList(CStringArray &);

CString getUserName(void);

CString getNameOfComputer(void);

BOOL isIPAddress(CString ip_address);
BOOL isThisANetworkServer(CString);

BOOL getLocalIP(CStringArray&);

BOOL SetupCryptoClient();
BOOL EncryptString(TCHAR* szPassword,TCHAR* szEncryptPwd,TCHAR *szKey);
BOOL DecryptString(TCHAR* szEncryptPwd,TCHAR* szPassword,TCHAR *szKey);

BOOL WriteAdminIniData(LPCTSTR db_serv,LPCTSTR location,LPCTSTR user,LPCTSTR psw,LPCTSTR dsn_name,LPCTSTR client,int authentication);
BOOL GetAdminIniData(TCHAR* db_serv,TCHAR* location,TCHAR* user,TCHAR* psw,TCHAR* dsn_name,TCHAR* client,int *idx);
BOOL IsAdminIniDataSet(void);
BOOL GetAdminIniDataSelectedDB(LPTSTR db_selected);
BOOL GetAuthentication(int *authentication);

BOOL WriteUserDBToRegistry(LPCTSTR db_name);
BOOL GetUserDBInRegistry(LPTSTR db_name);

void GetSupportedServers(vecADMIN_INI_DATABASES&,vecDBSERVERS &);

void setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos = FALSE);

CString setDBServerName(SAClient_t &client,LPCTSTR db_name,LPCTSTR db_selected = _T(""));

BOOL getDBInfo(LPCTSTR schema,LPTSTR db_path,LPTSTR db_user,LPTSTR db_psw,LPTSTR dsn_name,SAClient_t *client);
BOOL getDBInfo(LPCTSTR schema,LPTSTR db_path,LPTSTR db_user,LPTSTR db_psw,SAClient_t *client,int *autentication);
BOOL getDBLocation(LPTSTR db_location);
BOOL getDBUserInfo(LPTSTR db_path,LPTSTR user_name,LPTSTR psw,LPTSTR dsn_name,LPTSTR location,LPTSTR db_name,SAClient_t *client);
BOOL getDBUserInfo(LPTSTR db_path,LPTSTR db_name,LPTSTR user_name,LPTSTR psw,SAClient_t *client,int *authentication);

BOOL getHMSHomepageURL(LPTSTR url);
BOOL getRSSLocation(LPTSTR url,LPTSTR file_path);
BOOL setRSSFileLastPubDateInReg(LPCTSTR dt_str);
BOOL getRSSFileLastPubDateInReg(LPTSTR dt_str);
BOOL getPermanentCategoriesInReg(LPTSTR dt_str);
void setConnectToWEB(DWORD);
DWORD getConnectToWEB(void);
CString getNewsCategoriesSetupPathAndFile(void);
void saveNewsCatergoriesSetupInfo(vecNewsCategoriesSetup &);
void getNewsCatergoriesSetupInfo(vecNewsCategoriesSetup &);

// Contants for version information
const LPCTSTR VER_NUMBER					= _T("FileVersion");
const LPCTSTR VER_COMPANY					= _T("CompanyName");
const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

CString getVersionInfo(HMODULE hLib, LPCTSTR csEntry);
CString getVersionInfo(CString csFilename, LPCTSTR csEntry);

CString getSuiteModuleConfigPathAndFile(void);

void trim(TCHAR *);

// Display a message dialog
BOOL messageDlg(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg,CWnd *parent = NULL);

BOOL getLangFiles(CList<_languages> &);

HWND FindWinTitle(LPCTSTR title );


HWND CreateProcessWithConditions(LPTSTR lpszCmdLine, LPCTSTR lpszWinTitle, HWND hMainWindow, LPCTSTR lpszMapFile,
                                 DWORD dwMaxWaitInMS, BOOL bIsLeadingTitle, LPTSTR lpszCurrentDirectory = _T("") );

// Queries for Operating system, memory etc; 070320 p�d

CString QueryOS();
CString QueryTotalRAM();
CString QueryFreeRAM();
CString QueryPercentUsedRAM();
BOOL isWow64();

// Set if user if qualified to change Server and Database; 070329 p�d
void setDBQualified(int value);
int getDBQualified(void);

// Set if we have a connection to a databaseserver or not; 070329 p�d
void setIsDBConSet(int);
int getIsDBConSet(void);

// Check, in registry, if we should add table data;
int getAddDataToTables(void);
void setAddDataToTables(int value);

//////////////////////////////////////////////////////////
// Misc. functions for calculations

// Calculate volume from DGV and Avg. height based on
// a "Formtal".
double getM3SkVolFromGyAndAvgHgt(double gy,double avg_hgt);

BOOL getHeightFunctions(vecUCFunctions &func_list1,vecUCFunctionList& func_list2);

BOOL getVolumeFunctions(vecUCFunctions &func_list1,vecUCFunctionList& func_list2);

BOOL getBarkFunctions(vecUCFunctions &func_list1,vecUCFunctionList& func_list2);

BOOL getVolumeFunctions_ub(vecUCFunctions &func_list1,vecUCFunctionList& func_list2);

BOOL getExchangeFunctions(vecUCFunctions &func_list1);

BOOL getForrestNormFunctions(vecUCFunctions &func_list1);
BOOL getForrestNormTypeOfInfring(vecUCFunctions &func_list1);

// Added 2010-03-12 p�d
BOOL getGROTFunctions(vecUCFunctions &func_list);

// Create a H100 (e.g. T20,G32 from ex. 120 => T20); 070508 p�d
CString setupH100Value(LPCTSTR str);

CString getLangSet();

CString getModulesDir(void);
CString getModuleFN(HINSTANCE hinst);
CString getLanguageDir(void);
CString getSuitesDir(void);
CString getReportsDir(void);
CString getSetupsDir(void);
CString getMyDocumentsDir(void);
CString getDefaultObjectInventoryDir(void);
CString getRegisterObjectInventoryDir(void);
CString getRegisterExportXmlJsonDir(void);
void setRegisterExportXmlJsonDir(LPCTSTR);
void setRegisterObjectInventoryDir(LPCTSTR);

BOOL getRegisterDBBackupAllowed(void);
void setRegisterDBBackupAllowed(int v);

BOOL getRegisterDBRestoreAllowed(void);
void setRegisterDBRestoreAllowed(int v);

BOOL getRegisterDBCopyAllowed(void);
void setRegisterDBCopyAllowed(int v);

CString getSystemLanguage(void);
CString getSystemLanguageName(void);

CString getRegisterRelease(void);

BOOL renameDirectory(LPCTSTR old_name,LPCTSTR new_name);
BOOL isDirectory(LPCTSTR dir_path);
BOOL createDirectory(LPCTSTR dir_path);

BOOL copyFile(LPCTSTR from_file,LPCTSTR to_dir);

BOOL getListOfSubDirs(LPCTSTR main_dir,CStringArray &sub_dir_list);
BOOL getListOfFilesInDirectory(LPCTSTR filter,LPCTSTR main_dir,CStringArray &file_list,int mode = EF_ONLY_FILENAMES /* EF_FULLY_QUALIFIED */);
BOOL getFileInformation(LPCTSTR file_name,BY_HANDLE_FILE_INFORMATION &file_info);
BOOL convertFileDateTime(FILETIME ft,CString &s);
BOOL removeFile(CString file_name);

CString getLanguageFN(LPCTSTR,LPCTSTR,LPCTSTR,LPCTSTR,LPCTSTR def_lngabrev);

long getFileSize(LPCTSTR szFileName );

CString formatNumber(long number);
CString formatNumber(double number,short dec = 5);

void readUNICODESelectedFile(IN const UINT nCodePage,LPCTSTR fn,CStringArray &arr);

void setFromNavBarOrOtherInReg(short);
short getFromNavBarOrOtherInReg(void);

CString makeStringFromNumeric(CString value);

void scanFileName(CString& fn);

void doEvents();

CString cleanCRLF(LPCTSTR str,LPCTSTR replace);

int CreatePublicDirectory(CString csPath);

//---------------------------------------------------------------------------------------
// Added 2010-03-17 P�D
// Ber�kna Noordkoordinat RT90 fr�n Latitud,Longitud
int SweRef99LatLongtoRT90xy(CLatLongToRT90_data *tgps);

CString getLocaleDecimalPoint(void);

double localeStrToDbl(LPCTSTR value);

void TextToHtml(CString* /*pcsSource*/);	// translate special characters to their HTML counterpart

#endif
