/*	2006-03-17
*
*		Transaction classes; classes used for communication between Main program/Sutes/User modules
*
*/

#ifndef _PAD_TRANSACTION_CLASSES_H_
#define _PAD_TRANSACTION_CLASSES_H_

#include <map>
#include <vector>

typedef std::vector<long> vecInt;
typedef std::vector<double> vecFloat;
typedef std::vector<CString> vecString;

typedef std::map<CString,CString> mapStrings;
typedef std::map<int,CString> mapString;
typedef std::map<int,double> mapDouble;
typedef std::map<int,bool> mapBoolean;

#define ID_1950_FORREST_NORM			0	// "1950 �rs skogsnorm"
#define ID_2009_FORREST_NORM			1	// "Ny version av 1950 �rs skogsnorm"
#define ID_2018_FORREST_NORM			2 // "2017 �rs skogsnorm"

#define STRING_1950_FORREST_NORM		_T("1950 �rs Skogsnorm")
#define STRING_2009_FORREST_NORM		_T("Skogsnormen ver1.1")
#define STRING_2018_FORREST_NORM		_T("2018 �rs Skogsnorm")


#define VER120

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class: CTransaction_prl_data. 

class CTransaction_prl_data
{
	int m_nSpcID;
	int m_nIdentifer;
	CString m_sQualName;
	vecInt m_vecInt;		// Holds price/percentage values (dynamic)
public:
	CTransaction_prl_data(void);
	CTransaction_prl_data(int spc_id,int identifer,LPCTSTR qual_name,vecInt &vec);
	// Copy constructor
	//CTransaction_prl_data(const CTransaction_prl_data &c);

	int getSpcID(void);
	int getIdentifer(void);
	LPCTSTR getQualName(void);
	vecInt &getInts(void);
	void setInts(int value);

};

typedef std::vector<CTransaction_prl_data> vecTransactionPrlData;

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_qual_desc; holds data for qualitydescriptions/specie; 091209 p�d

class CTransaction_qual_desc
{
	int m_nSpcID;
	CString m_sQualName;
public:
	CTransaction_qual_desc(void)
	{
		m_nSpcID = -1;
		m_sQualName = _T("");
	}
	CTransaction_qual_desc(int spc_id,LPCTSTR qual_name)
	{
		m_nSpcID = spc_id;
		m_sQualName = qual_name;
	}
	// Copy constructor
	CTransaction_qual_desc(const CTransaction_qual_desc &c)	{ *this = c; }

	int getSpcID(void) { return m_nSpcID; }
	LPCTSTR getQualName(void) { return m_sQualName; }
};

typedef std::vector<CTransaction_qual_desc> vecTransactionQualDesc;

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class. Transfers data from CDataEnterDlg
// via CPricelistsView to CPricelistsFormView. Setting values to Pricelist and Quality-
// description ReportControls (Grids); 060324 p�d

class CTransaction_diameterclass
{
	int m_nSpcID;
	int m_nStartDiam;		// Start from this diamter (mm)
	int m_nDiamClass;		// Diamter class increment in (mm)
	int m_nNumOfDCLS;		// Number of diamters in this diamterclass
public:
	CTransaction_diameterclass(void);
	CTransaction_diameterclass(int,int,int,int);
	CTransaction_diameterclass(const	CTransaction_diameterclass &);

	inline BOOL operator ==(CTransaction_diameterclass &c) const
	{
		return (m_nSpcID == c.m_nSpcID && m_nStartDiam == c.m_nStartDiam && m_nDiamClass == c.m_nDiamClass && m_nNumOfDCLS == c.m_nNumOfDCLS);
	}

	int getSpcID(void);
	int getStartDiam(void);
	int getDiamClass(void);
	int getNumOfDCLS(void);
};

typedef std::vector<CTransaction_diameterclass> vecTransactionDiameterclass;

// Species transaction classes; 060317 p�d
class CTransaction_species
{
	int m_nID;
	int m_nSpcID;
	//#HMS-94  lagt till info om vilket tr�dslag som skall anv�ndas senare d� det skall letas i p30 prislista
	int m_nP30SpcID; 
	CString m_sSpcName;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_species(void);

	CTransaction_species(int id,int spc_id = 0,int nP30spcid=1,LPCTSTR spc_name = _T(""),LPCTSTR notes = _T(""),LPCTSTR created = _T(""));
	
	// Copy constructor
	CTransaction_species(const CTransaction_species &);

	inline BOOL operator ==(CTransaction_species &c) const
	{
		return (
			m_nID == c.m_nID && 
			m_nSpcID == c.m_nSpcID && 
			m_nP30SpcID == c.m_nP30SpcID && 
			m_sSpcName == c.m_sSpcName && 
			m_sNotes == c.m_sNotes);
	}

	int getID(void);
	int getSpcID(void);
	//#HMS-94  lagt till info om vilket tr�dslag som skall anv�ndas senare d� det skall letas i p30 prislista
	int getP30SpcID(void);
	void setP30SpcID(int nId)
	{
	m_nP30SpcID=nId;
	}
	CString getSpcName(void);
	CString getNotes(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_species> vecTransactionSpecies;

typedef std::vector<int> vecIntegers;


// Assortment transaction classes; 060317 p�d
class CTransaction_assort
{
	int m_nSpcID;
	CString m_sAssortName;
	double m_fMinDiam;
	double m_fPriceM3fub;
	double m_fPriceM3to;
	int m_nPulpType;
public:
	// Default constructor
	CTransaction_assort(void);

	CTransaction_assort(int spc_id,
											LPCTSTR assort_name,
											double min_diam,
											double price_m3fub,
											double price_m3to,
											int pulp_type);

	
	// Copy constructor
	CTransaction_assort(const CTransaction_assort &);

	inline BOOL operator ==(CTransaction_assort &c) const
	{
		return (m_nSpcID == c.m_nSpcID && 
						m_sAssortName == c.m_sAssortName && 
						m_fMinDiam == c.m_fMinDiam && 
						m_fPriceM3fub == c.m_fPriceM3fub && 
						m_fPriceM3to == c.m_fPriceM3to && 
						m_nPulpType == c.m_nPulpType);
	}

	int getSpcID(void);
	CString getAssortName(void);
	double getMinDiam(void);
	double getPriceM3fub(void);
	double getPriceM3to(void);
	int getPulpType(void);
};

typedef std::vector<CTransaction_assort> vecTransactionAssort;
typedef CList<CTransaction_assort *,CTransaction_assort *> listTransactionAssort;

// Pricelist transaction classes; 060406 p�d
class CTransaction_pricelist
{
	int m_nID;
	CString m_sName;
	int m_nTypeOf;
	CString m_sPricelistFile;
	CString m_sCreatedBy;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_pricelist(void);

	CTransaction_pricelist(int id,LPCTSTR name,int type_of,LPCTSTR prl_name,
																LPCTSTR created_by,LPCTSTR created = _T(""));
	
	// Copy constructor
	CTransaction_pricelist(const CTransaction_pricelist &);

	inline BOOL operator ==(CTransaction_pricelist &c) const
	{
		return (m_nID == c.m_nID && 
						m_sName == c.m_sName &&
						m_nTypeOf == c.m_nTypeOf &&
						m_sPricelistFile == c.m_sPricelistFile &&
						m_sCreatedBy == c.m_sCreatedBy);
	}

	int getID(void);
	CString getName(void);
	int getTypeOf(void);
	CString getPricelistFile(void);
	CString getCreatedBy(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_pricelist> vecTransactionPricelist;


// Template transaction classes; 070824 p�d
class CTransaction_template
{
	int m_nID;
	CString m_sTemplateName;
	int m_nTypeOf;
	CString m_sTemplateFile;
	CString m_sTemplateNotes;
	CString m_sCreatedBy;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_template(void);

	CTransaction_template(int id,LPCTSTR tmpl_name,int type_of,LPCTSTR tmpl_xml,
															 LPCTSTR notes,LPCTSTR created_by,LPCTSTR created = _T(""));
	
	// Copy constructor
	CTransaction_template(const CTransaction_template &);

	inline BOOL operator ==(CTransaction_template &c) const
	{
		return (m_nID == c.m_nID &&
					  m_sTemplateName == c.m_sTemplateName &&
						m_nTypeOf == c.m_nTypeOf &&
						m_sTemplateFile == c.m_sTemplateFile &&
						m_sTemplateNotes == c.m_sTemplateNotes &&
						m_sCreatedBy == c.m_sCreatedBy);
	}


	int getID(void);
	CString getTemplateName(void);
	int getTypeOf(void);
	CString getTemplateFile(void);
	CString getTemplateNotes(void);
	CString getCreatedBy(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_template> vecTransactionTemplate;

class CObjectTemplate_p30_table
{
	//private:
	int m_nSpcID;
	CString m_sSpcName;
	double m_fPrice;
	double m_fPriceRel;
public:
	CObjectTemplate_p30_table(void)
	{
		m_nSpcID = -1;
		m_sSpcName = _T("");
		m_fPrice = 0.0;
		m_fPriceRel = 0.0;
	}
	CObjectTemplate_p30_table(int spcid,LPCTSTR spc_name,double price,double price_rel)
	{
		m_nSpcID = spcid;
		m_sSpcName = spc_name;
		m_fPrice = price;
		m_fPriceRel = price_rel;
	}
	CObjectTemplate_p30_table(const CObjectTemplate_p30_table &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CObjectTemplate_p30_table &c) const
	{
		return (m_nSpcID == c.m_nSpcID &&
						m_sSpcName == c.m_sSpcName &&
						m_fPrice == c.m_fPrice &&
						m_fPriceRel == c.m_fPriceRel);
	}


	int getSpcID(void)				{ return m_nSpcID; }
	CString getSpcName(void)	{ return m_sSpcName; }
	double getPrice(void)			{ return m_fPrice; }
	double getPriceRel(void)	{ return m_fPriceRel; }
};

typedef std::vector<CObjectTemplate_p30_table> vecObjectTemplate_p30_table;


class CObjectTemplate_p30_nn_table
{
	//private:
	int m_nSpcID;
	CString m_sSpcName;
	CString m_sSI;
	int m_nAreaIdx;
	int m_nFrom;
	int m_nTo;
	int m_nPrice;
	double m_fPriceRel;
public:
	CObjectTemplate_p30_nn_table(void)
	{
		m_nSpcID = -1;
		m_sSpcName = _T("");
		m_nAreaIdx = -1;
		m_nFrom = 0;
		m_nTo = 0;
		m_nPrice = 0;
		m_fPriceRel = 0.0;
	}
	CObjectTemplate_p30_nn_table(int spcid,LPCTSTR spc_name,LPCTSTR si,int area_idx,int from,int to,int price,double price_rel)
	{
		m_nSpcID = spcid;
		m_sSpcName = spc_name;
		m_sSI = si;
		m_nAreaIdx = area_idx;
		m_nFrom = from;
		m_nTo = to;
		m_nPrice = price;
		m_fPriceRel = price_rel;
	}
	CObjectTemplate_p30_nn_table(const CObjectTemplate_p30_nn_table &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CObjectTemplate_p30_nn_table &c) const
	{
		return (m_nSpcID == c.m_nSpcID &&
						m_sSpcName == c.m_sSpcName &&
						m_nAreaIdx == c.m_nAreaIdx,
						m_sSI == c.m_sSI &&
						m_nFrom == c.m_nFrom &&
						m_nTo == c.m_nTo &&
						m_nPrice == c.m_nPrice &&
						m_fPriceRel == c.m_fPriceRel);
	}


	int getSpcID(void)				{ return m_nSpcID; }
	CString getSpcName(void)	{ return m_sSpcName; }
	CString getSI(void)				{ return m_sSI; }
	int getAreaIdx(void)			{ return m_nAreaIdx; }
	int getFrom(void)					{ return m_nFrom; }
	int getTo(void)						{ return m_nTo; }
	void setTo(int v)					{ m_nTo = v; }
	int getPrice(void)				{ return m_nPrice; }
	double getPriceRel(void)	{ return m_fPriceRel; }
};

typedef std::vector<CObjectTemplate_p30_nn_table> vecObjectTemplate_p30_nn_table;


class CObjectTemplate_hcost_table
{
	//private:
	double m_fM3Sk;
	double m_fPriceM3Sk;
	double m_fBaseComp;
	double m_fSumPrice;
public:
	CObjectTemplate_hcost_table(void)
	{
		m_fM3Sk = 0.0;
		m_fPriceM3Sk = 0.0;
		m_fBaseComp = 0.0;
		m_fSumPrice = 0.0;
	}
	CObjectTemplate_hcost_table(double m3sk,double price_m3sk,double base_comp,double sum_price)
	{
		m_fM3Sk = m3sk;
		m_fPriceM3Sk = price_m3sk;
		m_fBaseComp = base_comp;
		m_fSumPrice = sum_price;
	}
	CObjectTemplate_hcost_table(const CObjectTemplate_hcost_table &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CObjectTemplate_hcost_table &c) const
	{
		return (m_fM3Sk == c.m_fM3Sk &&
						m_fPriceM3Sk == c.m_fPriceM3Sk &&
						m_fBaseComp == c.m_fBaseComp &&
						m_fSumPrice == c.m_fSumPrice);
	}


	double getM3Sk(void)				{ return m_fM3Sk; }
	double getPriceM3Sk(void)		{ return m_fPriceM3Sk; }
	double getBaseComp(void)		{ return m_fBaseComp; }
	double getSumPrice(void)		{ return m_fSumPrice; }
};

typedef std::vector<CObjectTemplate_hcost_table> vecObjectTemplate_hcost_table;


// Postnumber transaction classes; 061129 p�d
class CTransaction_postnumber
{
	int m_nID;
	CString m_sName;
	CString m_sNumber;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_postnumber(void);

	CTransaction_postnumber(int id,LPCTSTR name,
																 LPCTSTR number,
																 LPCTSTR created);
	
	// Copy constructor
	CTransaction_postnumber(const CTransaction_postnumber &);

	inline BOOL operator ==(CTransaction_postnumber &c) const
	{
		return (m_nID == c.m_nID &&
						m_sName == c.m_sName &&
						m_sNumber == c.m_sNumber);
	}

	int getID(void);
	CString getName(void);
	CString getNumber(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_postnumber> vecTransactionPostnumber;

// Category transaction classes; 061129 p�d
class CTransaction_category
{
	int m_nID;
	CString m_sCategory;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_category(void);

	CTransaction_category(int id,LPCTSTR category,
															 LPCTSTR notes,
															 LPCTSTR created);
	
	// Copy constructor
	CTransaction_category(const CTransaction_category &);

	inline BOOL operator ==(CTransaction_category &c) const
	{
		return (m_nID == c.m_nID  &&
						m_sCategory == c.m_sCategory &&
						m_sNotes == c.m_sNotes);
	}


	int getID(void);
	CString getCategory(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_category> vecTransactionCategory;

class CTransaction_sample_tree_category
{
	int m_nID;
	CString m_sCategory;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_sample_tree_category(void);

	CTransaction_sample_tree_category(int id,LPCTSTR category,
															 LPCTSTR notes,
															 LPCTSTR created);
	
	// Copy constructor
	CTransaction_sample_tree_category(const CTransaction_category &);

	inline BOOL operator ==(CTransaction_sample_tree_category &c) const
	{
		return (m_nID == c.m_nID  &&
						m_sCategory == c.m_sCategory &&
						m_sNotes == c.m_sNotes);
	}


	int getID(void) { return m_nID; }
	CString getCategory(void) { return m_sCategory; }
	CString getNotes(void) { return m_sNotes; }
	CString getCreated(void) { return m_sCreated; }
};
typedef std::vector<CTransaction_sample_tree_category> vecTransactionSampleTreeCategory;

// Contacts transaction classes; 061129 p�d
class CTransaction_contacts
{
	int m_nID;
	CString m_sPNR_ORGNR;
	CString m_sName;
	CString m_sCompany;
	CString m_sAddress;
	CString m_sCoAddress;
	CString m_sPostNum;
	CString m_sPostAddress;
	CString m_sCounty;
	CString m_sCountry;
	CString m_sPhoneWork;
	CString m_sPhoneHome;
	CString m_sFaxNumber;
	CString m_sMobile;
	CString m_sEMail;
	CString m_sWebSite;
	CString m_sNotes;
	CString m_sCreatedBy;
	CString m_sCreated;
#ifdef VER120
	CString m_sVATNumber;
	int m_nConnectID;
	int m_nTypeOf;	// Can be Person or Company; 090825 p�d
#endif

	//--------------------------
	// Added 2012-11-21 P�D
	CString m_sBankgiro;
	CString m_sPlusgiro;
	CString m_sBankkonto;
	CString m_sLevnummer;
	//--------------------------

	CString m_sOwnerShare;
public:
	// Default constructor
	CTransaction_contacts(void);

	CTransaction_contacts(int id,
												LPCTSTR pnr_orgnr,
												LPCTSTR name,
												LPCTSTR company,
												LPCTSTR address,
												LPCTSTR co_address,
												LPCTSTR post_num,
												LPCTSTR post_address,
												LPCTSTR county,
												LPCTSTR country,
												LPCTSTR phone_work,
												LPCTSTR phone_home,
												LPCTSTR fax_number,
												LPCTSTR mobile,
												LPCTSTR e_mail,
												LPCTSTR web_site,
												LPCTSTR notes,
												LPCTSTR created_by,
												LPCTSTR created,
											#ifdef VER120
												LPCTSTR vat_number,
												int connect_id,
												int type_of,
											#endif
												LPCTSTR m_sBankgiro,
												LPCTSTR m_sPlusgiro,
												LPCTSTR m_sBankkonto,
												LPCTSTR m_sLvenummer,
												LPCTSTR owner_share = _T(""));
	
	// Copy constructor
	CTransaction_contacts(const CTransaction_contacts &);

	inline BOOL operator ==(CTransaction_contacts &c) const
	{
		return (m_nID == c.m_nID &&
						m_sPNR_ORGNR == c.m_sPNR_ORGNR &&
						m_sName == c.m_sName &&
						m_sCompany == c.m_sCompany &&
						m_sAddress == c.m_sAddress &&
						m_sCoAddress == c.m_sCoAddress &&
						m_sPostNum == c.m_sPostNum &&
						m_sPostAddress == c.m_sPostAddress &&
						m_sCounty == c.m_sCounty &&
						m_sCountry == c.m_sCountry &&
						m_sPhoneWork == c.m_sPhoneWork &&
						m_sPhoneHome == c.m_sPhoneHome &&
						m_sFaxNumber == c.m_sFaxNumber &&
						m_sMobile == c.m_sMobile &&
						m_sEMail == c.m_sEMail &&
						m_sWebSite == c.m_sWebSite &&
						m_sNotes == c.m_sNotes &&
						m_sCreatedBy == c.m_sCreatedBy &&
					#ifdef VER120
						m_sVATNumber == c.m_sVATNumber &&
						m_nConnectID == c.m_nConnectID &&
						m_nTypeOf == c.m_nTypeOf &&
					#endif
						m_sBankgiro == c.m_sBankgiro &&
						m_sPlusgiro == c.m_sPlusgiro &&
						m_sBankkonto == c.m_sBankkonto &&
						m_sLevnummer == c.m_sLevnummer &&
						m_sOwnerShare == c.m_sOwnerShare);
	}


	int getID(void);
	CString getPNR_ORGNR(void);
	CString getName(void);
	CString getCompany(void);
	CString getAddress(void);
	CString getCoAddress(void);
	CString getPostNum(void);
	CString getPostAddress(void);
	CString getCounty(void);
	CString getCountry(void);
	CString getPhoneWork(void);
	CString getPhoneHome(void);
	CString getFaxNumber(void);
	CString getMobile(void);
	CString getEMail(void);
	CString getWebSite(void);
	CString getNotes(void);
	CString getCreatedBy(void);
	CString getCreated(void);
#ifdef VER120
	CString getVATNumber(void);
	int getConnectID(void);
	int getTypeOf(void);
#endif
	CString getBankgiro(void);
	CString getPlusgiro(void);
	CString getBankkonto(void);
	CString getLevnummer(void);
	CString getOwnerShare(void);
};

typedef std::vector<CTransaction_contacts> vecTransactionContacts;


// Category for contacts transaction classes; 061221 p�d
class CTransaction_categories_for_contacts
{
	int m_nContactID;
	int m_nCategoryID;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_categories_for_contacts(void);

	CTransaction_categories_for_contacts(int contact,
																			 int category,
																			 LPCTSTR created);
	
	// Copy constructor
	CTransaction_categories_for_contacts(const CTransaction_categories_for_contacts &);

	inline BOOL operator ==(CTransaction_categories_for_contacts &c) const
	{
		return (m_nContactID == c.m_nContactID &&
						m_nCategoryID == c.m_nCategoryID);
	}

	int getContactID(void);
	void setContactID(int);
	int getCategoryID(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_categories_for_contacts> vecTransactionCategoriesForContacts;

// TraktType transaction classes; 070111 p�d
class CTransaction_county_municipal_parish
{
	int m_nID;
	int m_nCountyID;
	int m_nMunicipalID;
	int m_nParishID;
	CString m_sCountyName;
	CString m_sMunicipalName;
	CString m_sParishName;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_county_municipal_parish(void);

	CTransaction_county_municipal_parish(int id,
																			 int county_id,
																			 int municipal_id,
																			 int parish_id,
																			 LPCTSTR county_name,
																			 LPCTSTR municipal_name,
																			 LPCTSTR parish_name,
																			 LPCTSTR created);
	
	// Copy constructor
	CTransaction_county_municipal_parish(const CTransaction_county_municipal_parish &);

	inline BOOL operator ==(CTransaction_county_municipal_parish &c) const
	{
		return (m_nID == c.m_nID &&
						m_nCountyID == c.m_nCountyID &&
						m_nMunicipalID == c.m_nMunicipalID &&
						m_nParishID == c.m_nParishID &&
						m_sCountyName == c.m_sCountyName &&
						m_sMunicipalName == c.m_sMunicipalName &&
						m_sParishName == c.m_sParishName);
	}

	int getID(void);
	int getCountyID(void);
	int getMunicipalID(void);
	int getParishID(void);
	CString getCountyName(void);
	CString getMunicipalName(void);
	CString getParishName(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_county_municipal_parish> vecTransatcionCMP;

//////////////////////////////////////////////////////////////////////////////////////////////////////
// TraktType transaction classes; 070111 p�d
class CTransaction_trakt_type
{
	int m_nID;
	CString m_sTraktType;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_trakt_type(void);

	CTransaction_trakt_type(int id,LPCTSTR type_name,
															 LPCTSTR notes,
															 LPCTSTR created);
	
	// Copy constructor
	CTransaction_trakt_type(const CTransaction_trakt_type &);

	inline BOOL operator ==(CTransaction_trakt_type &c) const
	{
		return (m_nID == c.m_nID &&
						m_sTraktType == c.m_sTraktType &&
						m_sNotes == c.m_sNotes);
	}

	int getID(void);
	CString getTraktType(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_trakt_type> vecTransactionTraktType;

// Origin transaction classes; 070228 p�d
class CTransaction_origin
{
	int m_nID;
	CString m_sOrigin;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_origin(void);

	CTransaction_origin(int id,LPCTSTR origin_name,
														 LPCTSTR notes,
														 LPCTSTR created);
	
	// Copy constructor
	CTransaction_origin(const CTransaction_origin &);

	inline BOOL operator ==(CTransaction_origin &c) const
	{
		return (m_nID == c.m_nID &&
						m_sOrigin == c.m_sOrigin &&
						m_sNotes == c.m_sNotes);
	}

	int getID(void);
	CString getOrigin(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_origin> vecTransactionOrigin;


// InputMethod transaction classes; 070228 p�d
class CTransaction_input_method
{
	int m_nID;
	CString m_sInputMethod;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_input_method(void);

	CTransaction_input_method(int id,LPCTSTR input_method_name,
															 LPCTSTR notes,
															 LPCTSTR created);
	
	// Copy constructor
	CTransaction_input_method(const CTransaction_input_method &);

	inline BOOL operator ==(CTransaction_input_method &c) const
	{
		return (m_nID == c.m_nID &&
						m_sInputMethod == c.m_sInputMethod &&
						m_sNotes == c.m_sNotes);
	}


	int getID(void);
	CString getInputMethod(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_input_method> vecTransactionInputMethod;



// HgtClass transaction classes; 070228 p�d
class CTransaction_hgt_class
{
	int m_nID;
	CString m_sHgtClass;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_hgt_class(void);

	CTransaction_hgt_class(int id,LPCTSTR hgt_class_name,
													   	  LPCTSTR notes,
															  LPCTSTR created);
	
	// Copy constructor
	CTransaction_hgt_class(const CTransaction_hgt_class &);

	inline BOOL operator ==(CTransaction_hgt_class &c) const
	{
		return (m_nID == c.m_nID &&
						m_sHgtClass == c.m_sHgtClass &&
						m_sNotes == c.m_sNotes);
	}

	int getID(void);
	CString getHgtClass(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_hgt_class> vecTransactionHgtClass;


// CutClass transaction classes; 070228 p�d
class CTransaction_cut_class
{
	int m_nID;
	CString m_sCutClass;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_cut_class(void);

	CTransaction_cut_class(int id,LPCTSTR hgt_class_name,
													   	  LPCTSTR notes,
															  LPCTSTR created);
	
	// Copy constructor
	CTransaction_cut_class(const CTransaction_cut_class &);

	inline BOOL operator ==(CTransaction_cut_class &c) const
	{
		return (m_nID == c.m_nID &&
						m_sCutClass == c.m_sCutClass &&
						m_sNotes == c.m_sNotes);
	}

	int getID(void);
	CString getCutClass(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_cut_class> vecTransactionCutClass;

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt; 2007-03-01 P�D
class CTransaction_trakt
{
//private:
	int m_nTraktID;
	CString m_sTraktNum;
	CString m_sTraktPNum;	// Delnummer
	CString m_sTraktName;
	CString m_sTraktType;
	CString m_sTraktDate;
	int m_nTraktPropID;	// ID for Property
	double m_fTraktAreal;
	int m_nTraktLength;
	int m_nTraktWidth;
	double m_fTraktArealConsiderd;
	double m_fTraktArealHandled;
	int m_nTraktAge;
	CString m_sTraktSIH100;
	double m_fTraktWithdraw;
	int m_nTraktHgtOverSea;
	int m_nTraktGround;
	int m_nTraktSurface;
	int m_nTraktRake;
	CString m_sTraktMapdata;
	int m_nTraktLatitude;
	int m_nTraktLongitude;
	CString m_sTraktXCoord;
	CString m_sTraktYCoord;
	CString m_sTraktOrigin;
	CString m_sTraktInpMethod;
	CString m_sTraktHgtClass;
	CString m_sTraktCutClass;
	BOOL m_nTraktSimData;
	BOOL m_nTraktNearCoast;
	BOOL m_nTraktSoutheast;
	BOOL m_nTraktRegion5;
	BOOL m_nTraktPartOfPlot;
	CString m_sTraktSIH100_Pine;
	CString m_sPropNum;
	CString m_sPropName;
	CString m_sTraktNotes;
	CString m_sTraktCreatedBy;
	CString m_sCreated;
	CString m_sTraktPoint;
	CString m_sTraktCoordinates;
	int m_nWSide;		// "Vilken sida har man taxerat (breddning ny norm)"; 080922 p�d
	// OBS! THIS DATA-MEMBER ISN'T A FILED IN THE "esti_trakt_table"; 090625 p�d
	// ADDED JUST TO BE ABLE TO TRANSEFER CruiseWidth SET IN "elv_cruise_table", TO "UMLandValueNorm" NY NORM; 090625 p�d
	double m_fCruiseWidth;	// "Vid breddning; kan s�ttas till en bredd, som avviker fr�n Objekts-bredden satt"; 090625 p�d
	short m_nOnlyForStand;	// "1 = Anv�nds endast i best�nd, ej intr�ng; 0 = Anv�nds �ven i intr�ng; Default = 0"
	BOOL m_bTillfUtnyttj;
public:
	// Default constructor
	CTransaction_trakt(void);
	
	CTransaction_trakt(int id,LPCTSTR num,LPCTSTR pnum,LPCTSTR name,LPCTSTR type,LPCTSTR date,
										 int prop_id,double areal,double areal_considerd,double areal_handled,
										 int age,LPCTSTR h100,double withdraw,int hgt_over_sea,
										 int ground,int surface,int rake,LPCTSTR mapdata,int latitude,int longitude,
										 LPCTSTR x_coord,LPCTSTR y_coord,LPCTSTR origin,LPCTSTR inp_method,LPCTSTR hgt_class,LPCTSTR cut_class,
										 BOOL sim_data,BOOL near_coast,BOOL southeast,BOOL region5,BOOL part_of_plot,LPCTSTR si_h100_pine,
										 LPCTSTR prop_num,LPCTSTR prop_name,
										 LPCTSTR notes,LPCTSTR created_by,LPCTSTR created,int wside,
										 short only_for_stand,int nTraktLength,int nTraktWidth,
										 BOOL bTillfUtnyttj,LPCTSTR cPoint,LPCTSTR cTraktCoordinates);
	// Copy constructor
	CTransaction_trakt(const CTransaction_trakt &v);

	inline BOOL operator ==(CTransaction_trakt &c) const
	{
		return (m_nTraktID == c.m_nTraktID &&
						m_sTraktNum == c.m_sTraktNum &&
						m_sTraktPNum == c.m_sTraktPNum &&
						m_sTraktName == c.m_sTraktName &&
						m_sTraktType == c.m_sTraktType &&
						m_sTraktDate == c.m_sTraktDate &&
						m_nTraktPropID == c.m_nTraktPropID &&
						m_fTraktAreal == c.m_fTraktAreal &&

						m_nTraktLength == c.m_nTraktLength && 
						m_nTraktWidth == c.m_nTraktWidth && 

						m_fTraktArealConsiderd == c.m_fTraktArealConsiderd &&
						m_fTraktArealHandled == c.m_fTraktArealHandled &&
						m_nTraktAge == c.m_nTraktAge &&
						m_sTraktSIH100 == c.m_sTraktSIH100 &&
						m_fTraktWithdraw == c.m_fTraktWithdraw &&
						m_nTraktHgtOverSea == c.m_nTraktHgtOverSea &&
						m_nTraktGround == c.m_nTraktGround &&
						m_nTraktSurface == c.m_nTraktSurface &&
						m_nTraktRake == c.m_nTraktRake &&
						m_sTraktMapdata == c.m_sTraktMapdata &&
						m_nTraktLatitude == c.m_nTraktLatitude &&
						m_nTraktLongitude == c.m_nTraktLongitude &&
						m_sTraktXCoord == c.m_sTraktXCoord &&
						m_sTraktYCoord == c.m_sTraktYCoord &&
						m_sTraktOrigin == c.m_sTraktOrigin &&
						m_sTraktInpMethod == c.m_sTraktInpMethod &&
						m_sTraktHgtClass == c.m_sTraktHgtClass &&
						m_sTraktCutClass == c.m_sTraktCutClass &&
						m_nTraktSimData == c.m_nTraktSimData &&
						m_nTraktNearCoast == c.m_nTraktNearCoast &&
						m_nTraktSoutheast == c.m_nTraktSoutheast &&
						m_nTraktRegion5 == c.m_nTraktRegion5 &&
						m_nTraktPartOfPlot == c.m_nTraktPartOfPlot &&
						m_sTraktSIH100_Pine == c.m_sTraktSIH100_Pine &&
						m_sPropNum == c.m_sPropNum &&
						m_sPropName == c.m_sPropName &&
						m_sTraktNotes == c.m_sTraktNotes &&
						m_sTraktCreatedBy == c.m_sTraktCreatedBy &&
						m_nWSide == c.m_nWSide &&
						m_nOnlyForStand == c.m_nOnlyForStand &&
						m_bTillfUtnyttj == c.m_bTillfUtnyttj &&
						m_sTraktPoint == c.m_sTraktPoint &&
						m_sTraktCoordinates == c.m_sTraktCoordinates);
	}


	int getTraktID(void);
	CString getTraktNum(void);
	CString getTraktPNum(void);	// Delnummer
	CString getTraktName(void);
	CString getTraktType(void);
	CString getTraktDate(void);
	int getTraktPropID(void);	// ID for Property
	double getTraktAreal(void);
	int getTraktLength(void);
	int getTraktWidth(void);
	double getTraktArealConsiderd(void);
	double getTraktArealHandled(void);
	int getTraktAge(void);
	CString getTraktSIH100(void);
	double getTraktWithdraw(void);
	int getTraktHgtOverSea(void);
	int getTraktGround(void);
	int getTraktSurface(void);
	int getTraktRake(void);
	CString getTraktMapdata(void);
	int getTraktLatitude(void);
	int getTraktLongitude(void);
	CString getTraktXCoord(void);
	CString getTraktYCoord(void);
	CString getTraktOrigin(void);
	CString getTraktInpMethod(void);
	CString getTraktHgtClass(void);
	CString getTraktCutClass(void);
	BOOL getTraktSimData(void);
	BOOL getTraktNearCoast(void);
	void setTraktNearCoast(int);
	BOOL getTraktSoutheast(void);
	void setTraktSoutheast(int);
	BOOL getTraktRegion5(void);
	void setTraktRegion5(int);
	BOOL getTraktPartOfPlot(void);
	void setTraktPartOfPlot(int);
	CString getTraktSIH100_Pine(void);
	void setTraktSIH100_Pine(LPCTSTR v);
	CString getTraktPropNum(void);
	CString getTraktPropName(void);
	CString getTraktNotes(void);
	CString getTraktCreatedBy(void);
	CString getCreated(void);
	CString getTraktPoint(void);
	CString getTraktCoordinates(void);
	int getWSide(void);
	void setWSide(int v);
	short getOnlyForStand(void);
	short getTillfUtnyttj(void);
	void setOnlyForStand(short v);
	// NOT IN CONSTRUCTOR; 090625 p�d
	double getCruiseWidth(void)		{ return m_fCruiseWidth; }
	void setCruiseWidth(double v)	{ m_fCruiseWidth = v; }

	BOOL getTraktTillfUtnyttj()	{return m_bTillfUtnyttj;};
};
typedef std::vector<CTransaction_trakt> vecTransactionTrakt;

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt data; 2007-03-01 P�D
class CTransaction_trakt_data
{
//private:
	int m_nTDataID;
	int m_nTDataTraktID;
	int m_nTDataType;
	int m_nSpeiceID;
	CString m_sSpecieName;
	double m_fPercent;
	long m_nNumOf;
	double m_fDA;
	double m_fDG;
	double m_fDGV;
	double m_fHGV;
	double m_fGY;
	double m_fAvgHgt;
	double m_fH25;
	double m_fGreenCrown;
	double m_fM3SK;
	double m_fM3FUB;
	double m_fM3UB;
	double m_fAvgM3SK;
	double m_fAvgM3FUB;
	double m_fAvgM3UB;
	double m_fGrot;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_trakt_data(void);
	
	CTransaction_trakt_data(int tdata_id,int trakt_id,int data_type,
													int spc_id,LPCTSTR spc_name,
													double percent,long numof,
													double da,double dg,double dgv,double hgv,
													double gy,double avg_hgt,double h25,double green_crown,
													double m3sk,double m3fub,double m3ub,
													double avg_m3sk,double avg_m3fub,double avg_m3ub,double grot,
													LPCTSTR created);
	// Copy constructor
	CTransaction_trakt_data(const CTransaction_trakt_data &v);

	inline BOOL operator ==(CTransaction_trakt_data &c) const
	{
		return (m_nTDataID == c.m_nTDataID &&
						m_nTDataTraktID == c.m_nTDataTraktID &&
						m_nTDataType == c.m_nTDataType &&
						m_nSpeiceID == c.m_nSpeiceID &&
						m_sSpecieName == c.m_sSpecieName &&
						m_fPercent == c.m_fPercent &&
						m_nNumOf == c.m_nNumOf &&
						m_fDA == c.m_fDA &&
						m_fDG == c.m_fDG &&
						m_fDGV == c.m_fDGV &&
						m_fHGV == c.m_fHGV &&
						m_fGY == c.m_fGY &&
						m_fAvgHgt == c.m_fAvgHgt &&
						m_fH25 == c.m_fH25 &&
						m_fGreenCrown == c.m_fGreenCrown &&
						m_fM3SK == c.m_fM3SK &&
						m_fM3FUB == c.m_fM3FUB &&
						m_fM3UB == c.m_fM3UB &&
						m_fAvgM3SK == c.m_fAvgM3SK &&
						m_fAvgM3FUB == c.m_fAvgM3FUB &&
						m_fAvgM3UB == c.m_fAvgM3UB &&
						m_fGrot == c.m_fGrot);
	}

	int getTDataID(void);
	void setTDataID(int v);
	int getTDataTraktID(void);
	int getTDataType(void);
	void setTDataType(int v);
	int getSpecieID(void);
	CString getSpecieName(void);
	double getPercent(void);
	void setPercent(double v);
	long getNumOf(void);
	void setNumOf(long v);
	double getDA(void);
	void setDA(double v);
	double getDG(void);
	void setDG(double v);
	double getDGV(void);
	void setDGV(double v);
	double getHGV(void);
	void setHGV(double v);
	double getGY(void);
	void setGY(double v);
	double getAvgHgt(void);
	void setAvgHgt(double v);
	double getH25(void);
	void setH25(double v);
	double getGreenCrown(void);
	void setGreenCrown(double v);
	double getM3SK(void);
	void setM3SK(double v);
	double getM3FUB(void);
	void setM3FUB(double v);
	double getM3UB(void);
	void setM3UB(double v);
	double getAvgM3SK(void);
	void setAvgM3SK(double v);
	double getAvgM3FUB(void);
	void setAvgM3FUB(double v);
	double getAvgM3UB(void);
	void setAvgM3UB(double v);
	double getGrot(void);
	void setGrot(double v);
	CString getCreated(void);

};
typedef std::vector<CTransaction_trakt_data> vecTransactionTraktData;


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_trans; 2007-06-01 p�d
// Transfers between Assortments per Specie, for Trakt
class CTransaction_trakt_trans
{
//private:
	int m_nTTransDataID;		// TrakrData id
	int m_nTTransTraktID;		// Trakt id
	int m_nTTransFromID;		// From assortment id
	int m_nTTransToID;			// To assortment id
	int m_nSpeiceID;
	int m_nTTransDataType;	// "1=Uttag etc."
	CString m_sFromName;		// Name of From assortment
	CString m_sToName;			// Name of To assortment
	CString m_sSpecieName;
	double m_fM3FUB;
	double m_fPercent;
	CString m_sCreated;
	double m_fM3Trans;			// Transferd m3; 101110 p�d
public:
	// Default constructor
	CTransaction_trakt_trans(void);
	
	CTransaction_trakt_trans(int trans_data_id,
													 int trans_trakt_id,
													 int trans_from_id,
													 int trans_to_id,
													 int trans_spc_id,
													 int trans_data_type,
													 LPCTSTR from_name,
													 LPCTSTR to_name,
													 LPCTSTR spc_name,
													 double m3sk,
													 double percent,
													 double m3trans,
													 LPCTSTR created);
	// Copy constructor
	CTransaction_trakt_trans(const CTransaction_trakt_trans &v);

	inline BOOL operator ==(CTransaction_trakt_trans &c) const
	{
		return (m_nTTransDataID == c.m_nTTransDataID &&
						m_nTTransTraktID == c.m_nTTransTraktID &&
						m_nTTransFromID == c.m_nTTransFromID &&
						m_nTTransToID == c.m_nTTransToID &&
						m_nSpeiceID == c.m_nSpeiceID &&
						m_nTTransDataType == c.m_nTTransDataType &&
						m_sFromName == c.m_sFromName &&
						m_sToName == c.m_sToName &&
						m_sSpecieName == c.m_sSpecieName &&
						m_fM3FUB == c.m_fM3FUB &&
						m_fPercent == c.m_fPercent &&
						m_fM3Trans == c.m_fM3Trans);
	}

	int getTTransDataID(void);
	int getTTransTraktID(void);
	int getTTransFromID(void);
	int getTTransToID(void);
	int getSpecieID(void);
	int getTTransDataType(void);
	CString getFromName(void);
	CString getToName(void);
	CString getSpecieName(void);
	double getM3FUB(void);
	void setM3FUB(double v);
	double getPercent(void);
	void setPercent(double v);
	double getM3Trans(void);
	void setM3Trans(double v);
	CString getCreated(void);

};
typedef std::vector<CTransaction_trakt_trans> vecTransactionTraktTrans;


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_ass; 2007-03-06 P�D
class CTransaction_trakt_ass
{
//private:
	int m_nTAssID;							// ID of assortment
	int m_nTAssTraktID;					// ID of trakt
	int m_nTAssTraktDataID;			// ID of trakt data = specie id
	int m_nTAssTraktDataType;		// Type of data "1=Uttag,2=Kvarl�mmnat,3=Uttag i Stickv�g"
	CString m_sTAssName;
	double m_fPriceM3FUB;				// Price from e.g. pricelist, avg. assortment list etc.
	double m_fPriceM3TO;				// Price from e.g. pricelist, avg. assortment list etc.
	double m_fM3FUB;						// "Utbytesvolym" m3fub
	double m_fM3TO;							// "Utbytesvolym" m3to
	double m_fValueM3FUB;				// Value of e.g. in kronor (M3FUB * PriceM3FUB)
	double m_fValueM3TO;				// Value of e.g. in kronor (M3TO * PriceM3TO)
	CString m_sCreated;
	double m_fKrPerM3;					// "Kronor per Kubikmeter"
public:
	// Default constructor
	CTransaction_trakt_ass(void);
	
	CTransaction_trakt_ass(int tass_assort_id,int tass_trakt_id,int tass_data_id,int tass_data_type,
												 LPCTSTR tass_name,
												 double tass_price_m3fub,double tass_price_m3to,
												 double tass_m3fub,double tass_m3to,
												 double tass_value_m3fub,double tass_value_m3to,
												 LPCTSTR created,double kr_per_m3);
	// Copy constructor
	CTransaction_trakt_ass(const CTransaction_trakt_ass &v);

	inline BOOL operator ==(CTransaction_trakt_ass &c) const
	{
		return (m_nTAssID == c.m_nTAssID &&
						m_nTAssTraktID == c.m_nTAssTraktID &&
						m_nTAssTraktDataID == c.m_nTAssTraktDataID &&
						m_nTAssTraktDataType == c.m_nTAssTraktDataType &&
						m_sTAssName	== c.m_sTAssName &&
						m_fPriceM3FUB == c.m_fPriceM3FUB &&
						m_fPriceM3TO == c.m_fPriceM3TO &&
						m_fM3FUB == c.m_fM3FUB &&
						m_fM3TO == c.m_fM3TO &&
						m_fValueM3FUB == c.m_fValueM3FUB &&
						m_fValueM3TO == c.m_fValueM3TO &&
						m_fKrPerM3 == c.m_fKrPerM3);
	}

	int getTAssID(void);
	void setTAssID(int v);

	int getTAssTraktID(void);
	void setTAssTraktID(int v);

	int getTAssTraktDataID(void);
	void setTAssTraktDataID(int v);

	int getTAssTraktDataType(void);
	void setTAssTraktDataType(int v);

	CString getTAssName(void);

	double getPriceM3FUB(void);
	void setPriceM3FUB(double);
	
	double getPriceM3TO(void);
	void setPriceM3TO(double);
	
	double getM3FUB(void);
	void setM3FUB(double);
	
	double getM3TO(void);
	void setM3TO(double);
	
	double getValueM3FUB(void);
	void setValueM3FUB(double);
	
	double getValueM3TO(void);
	void setValueM3TO(double);

	CString getCreated(void);

	double getKrPerM3(void);
	void setKrPerM3(double);

};
typedef std::vector<CTransaction_trakt_ass> vecTransactionTraktAss;

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_misc_data
class CTransaction_trakt_misc_data
{
	int m_nTSetTraktID;
	CString m_sName;
	int m_nTypeOf;
	CString m_sXMLPricelist;
	CString m_sCostsName;
	int m_nCostsTypeOf;
	CString m_sXMLCosts;
	double m_fDiamClass;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_trakt_misc_data(void);

	CTransaction_trakt_misc_data(int tset_trakt_id,
															 LPCTSTR name,
															 int type_of,
															 LPCTSTR xml_prl,
															 LPCTSTR costs_name,
															 int costs_type_of,
															 LPCTSTR xml_costs,
															 double dam_class,
 															 LPCTSTR created);
	
	// Copy constructor
	CTransaction_trakt_misc_data(const CTransaction_trakt_misc_data &);

	inline BOOL operator ==(CTransaction_trakt_misc_data &c) const
	{
		return (m_nTSetTraktID == c.m_nTSetTraktID &&
						m_sName == c.m_sName &&
						m_nTypeOf == c.m_nTypeOf &&
						m_sXMLPricelist == c.m_sXMLPricelist &&
						m_sCostsName == c.m_sCostsName &&
						m_nCostsTypeOf == c.m_nCostsTypeOf &&
						m_sXMLCosts == c.m_sXMLCosts &&
						m_fDiamClass == c.m_fDiamClass);
	}


	int getTSetTraktID(void);
	CString getName(void);
	int getTypeOf(void);
	CString getXMLPricelist(void);
	CString getCostsName(void);
	int getCostsTypeOf(void);
	CString getXMLCosts(void);
	double getDiamClass(void);
	CString getCreated(void);

};

typedef std::vector<CTransaction_trakt_misc_data> vecTransactionTraktMiscData;


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_trakt_set_spc
class CTransaction_trakt_set_spc
{
	int m_nTSetspcID;
	int m_nTSetspcDataID;
	int m_nTSetspcTraktID;
	int m_nTSetspcDataType;	// "1=Uttag etc"
	int m_nSpcID;		// Specie which settings applay for in Trakt and Settings. 
									// Same species as in selected Pricelist

	int m_nHgtFuncID;						// ID Of height function selected
	int m_nHgtFuncSpcID;				// ID Of specie for heightfunction to use
	int m_nHgtFuncIndex;				// Index of function used in height module

	int m_nVolFuncID;						// ID Of volume function selected
	int m_nVolFuncSpcID;				// ID Of specie for volumefunction to use
	int m_nVolFuncIndex;				// Index of function used in volume module

	int m_nBarkFuncID;					// ID Of bark function selected
	int m_nBarkFuncSpcID;				// ID Of specie for barkfunction to use
	int m_nBarkFuncIndex;				// Index of function used in bark module

	int m_nVolUBFuncID;						// ID Of volume function selected
	int m_nVolUBFuncSpcID;				// ID Of specie for volumefunction to use
	int m_nVolUBFuncIndex;				// Index of function used in volume module

	double m_fM3SkToM3Ub;				// "Omf�ringstal" from m3sk to m3fub
	double m_fM3SkToM3Fub;				// "Omf�ringstal" from m3sk to m3fub
	double m_fM3FubToM3To;				// "Omf�ringstal" from m3fub to m3to

	double m_fDiamClass;				// "Diameterklass i cm"

	int m_nSelQDescIndex;				// Index for selected qualitydesription for Pricelist selected

	CString m_sExtraInfo;				// Can hold e.g. information on "S�derbergs Kustn�ra,Syd�stra Sverige etc"

	int m_nTranspDist1;					// Transport distance, e.g. "Avst�nd f�r framk�rning Skotare"; 080306 p�d
	int m_nTranspDist2;					// Transport distance, e.g. "Avst�nd till Industri"; 080314 p�d

	CString m_sCreated;

#ifdef VER120
	CString m_sQDescName;				// Add name of selected qualitysescription; 090917 p�d
	int m_nGrotFuncID;					// ID for GROT function selected
	double m_fGrotPercent;			// Uttagsprocent
	double m_fGrotPrice;				// Pris (kr/ton)
	double m_fGrotCost;					// Kostnad (kr/ton)
	double m_fDefaultH25;				// Default H25 from template
	double m_fDefaultGreenCrown;	// #4555: Default green crown from template
#endif
public:
	// Default constructor
	CTransaction_trakt_set_spc(void);

	CTransaction_trakt_set_spc(int setspc_id,
														 int setspc_data_id,
														 int setspc_data_trakt_id,
														 int setspc_data_type,
														 int spc_id,
														 int hgt_func_id,
														 int hgt_func_spc_id,
														 int hgt_func_idx,
														 int vol_func_id,
														 int vol_func_spc_id,
														 int vol_func_idx,
														 int bark_func_id,
														 int bark_func_spc_id,
														 int bark_func_idx,
														 int vol_ub_func_id,
														 int vol_ub_func_spc_id,
														 int vol_ub_func_idx,
														 double m3sk_m3ub,
														 double m3sk_m3fub,
														 double m3fub_m3to,
														 double dia_class,
														 int sel_qdesc_index,
														 LPCTSTR extra_info,
														 int transp_dist1,
														 int transp_dist2,
#ifdef VER120
														 LPCTSTR create_date,
														 LPCTSTR qdesc_name,
														 int grot_id,
														 double grot_percent,
														 double grot_price,
														 double grot_cost,
														 double default_h25,
														 double default_greencrown);
#else
														 LPCTSTR create_date);
#endif
	
	// Copy constructor
	CTransaction_trakt_set_spc(const CTransaction_trakt_set_spc &);


	inline BOOL operator ==(CTransaction_trakt_set_spc &c) const
	{
		return (m_nTSetspcID == c.m_nTSetspcID &&
						m_nTSetspcDataID == c.m_nTSetspcDataID &&
						m_nTSetspcTraktID == c.m_nTSetspcTraktID &&
						m_nTSetspcDataType == c.m_nTSetspcDataType &&
						m_nSpcID == c.m_nSpcID &&
						m_nHgtFuncID == c.m_nHgtFuncID &&
						m_nHgtFuncSpcID == c.m_nHgtFuncSpcID &&
						m_nHgtFuncIndex == c.m_nHgtFuncIndex &&
						m_nVolFuncID == c.m_nVolFuncID &&
						m_nVolFuncSpcID == c.m_nVolFuncSpcID &&
						m_nVolFuncIndex == c.m_nVolFuncIndex &&
						m_nBarkFuncID == c.m_nBarkFuncID &&
						m_nBarkFuncSpcID == c.m_nBarkFuncSpcID &&
						m_nBarkFuncIndex == c.m_nBarkFuncIndex &&
						m_nVolUBFuncID == c.m_nVolUBFuncID &&
						m_nVolUBFuncSpcID == c.m_nVolUBFuncSpcID &&
						m_nVolUBFuncIndex == c.m_nVolUBFuncIndex &&
						m_fM3SkToM3Ub == c.m_fM3SkToM3Ub &&
						m_fM3SkToM3Fub == c.m_fM3SkToM3Fub &&
						m_fM3FubToM3To == c.m_fM3FubToM3To &&
						m_fDiamClass == c.m_fDiamClass &&
						m_nSelQDescIndex == c.m_nSelQDescIndex &&
						m_sExtraInfo == c.m_sExtraInfo &&
						m_nTranspDist1 == c.m_nTranspDist1 &&
#ifdef VER120
						m_nTranspDist2 == c.m_nTranspDist2 &&
						m_sQDescName == c.m_sQDescName &&
						m_nGrotFuncID == c.m_nGrotFuncID &&
						m_fGrotPercent == c.m_fGrotPercent &&
						m_fGrotPrice == c.m_fGrotPrice &&
						m_fGrotCost == c.m_fGrotCost &&
						m_fDefaultH25 == c.m_fDefaultH25 &&
						m_fDefaultGreenCrown == c.m_fDefaultGreenCrown); 
#else
						m_nTranspDist2 == c.m_nTranspDist2);
#endif

	}


	int getTSetspcID(void);
	int getTSetspcDataID(void);
	int getTSetspcTraktID(void);
	int getTSetspcDataType(void);
	int getSpcID(void);
	int getHgtFuncID(void);
	int getHgtFuncSpcID(void);
	int getHgtFuncIndex(void);
	int getVolFuncID(void);
	int getVolFuncSpcID(void);
	int getVolFuncIndex(void);
	int getBarkFuncID(void);
	int getBarkFuncSpcID(void);
	int getBarkFuncIndex(void);
	int getVolUBFuncID(void);
	int getVolUBFuncSpcID(void);
	int getVolUBFuncIndex(void);
	double getM3SkToM3Ub(void);
	double getM3SkToM3Fub(void);
	double getM3FubToM3To(void);
	double getDiamClass(void);
	int getSelQDescIndex(void);
	CString getExtraInfo(void);
	int getTranspDist1(void);
	int getTranspDist2(void);
	CString getCreated();
#ifdef VER120
	CString getQDescName();
	int getGrotFuncID(void);
	double getGrotPercent(void);
	double getGrotPrice(void);
	double getGrotCost(void);
	double getDefaultH25(void);
	double getDefaultGreenCrown(void);
#endif
};

typedef std::vector<CTransaction_trakt_set_spc> vecTransactionTraktSetSpc;


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_plot

class CTransaction_plot
{
//private:
	//--------------------------------------------
	// Key field
	int m_nPlotID;					// ID or plot
	int m_nTraktID;					// ID of trakt
	//--------------------------------------------
	CString m_sPlotName;		// Name of Plot
	int m_nPlotType;				// "0 = Totaltaxering, 1 = Cirkelytor, 2 = Snabbtaxering etc."
	double m_fRadius;				// "Ytradie vid cirkelytetaxering"
	double m_fLength1;			// "Rektangul�r yta L�ngd 1 
	double m_fWidth1;				// "Rektangul�r yta Bredd 1 
	double m_fArea;					// "Area f�r ytan"
	CString m_sCoord;				// " koordinat"
	int m_nNumOfTrees;			// "Antal tr�d"
	CString m_sCreated;
public:
	CTransaction_plot(void);

	CTransaction_plot(int plot_id,
										int trakt_id,
										LPCTSTR plot_name,
										int plot_type,
										double radius,
										double length1,
										double width1,
										double area,
										LPCTSTR coord,
										int numof_trees,
										LPCTSTR created);

	CTransaction_plot(const CTransaction_plot &);

	inline BOOL operator ==(CTransaction_plot &c) const
	{
		return (m_nPlotID == c.m_nPlotID &&
						m_nTraktID == c.m_nTraktID &&
						m_sPlotName == c.m_sPlotName &&
						m_nPlotType == c.m_nPlotType &&
						m_fRadius == c.m_fRadius &&
						m_fLength1 == c.m_fLength1 &&
						m_fWidth1 == c.m_fWidth1 &&
						m_fArea == c.m_fArea &&
						m_sCoord == c.m_sCoord &&
						m_nNumOfTrees == c.m_nNumOfTrees);
	}

	int getPlotID(void);
	int getTraktID(void);
	CString getPlotName(void);
	void setPlotName(LPCTSTR v);
	int getPlotType(void);
	void setPlotType(int v);
	double getRadius(void);
	void setRadius(double v);
	double getLength1(void);
	void setLength1(double v);
	double getWidth1(void);
	void setWidth1(double v);
	double getArea(void);
	void setArea(double v);
	CString getCoord(void);
	void setCoord(LPCTSTR v);
	int getNumOfTrees(void);
	void setNumOfTrees(int v);
	CString getCreated(void);
};

typedef std::vector<CTransaction_plot> vecTransactionPlot;

////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_sample_tree

class CTransaction_sample_tree
{
//private:
	//--------------------------------------------
	// Key field
	int m_nTreeID;					// Incemental number from 1 to number of trees for Trakt/Plot
	int m_nTraktID;					// ID of trakt
	//--------------------------------------------
	int m_nPlotID;					// Plot (Group)
	int m_nSpcID;						// ID of specie
	CString m_sSpcName;			// Name of specie
	double m_fDbh;					// "Br�sth�jdsdiameter i mm"
	double m_fHgt;					// "H�jd i dm"
	double m_fGCrownPerc;		// "Gr�nkrona procent"
	double m_fBarkThick;		// "Barktjocklek i mm (dubbla)"
	double m_fGROT;					// "Biomassa i kg"
	int m_nAge;							// "�lder"
	int m_nGrowth;					// "Tillv�xt"
	double m_fDCLS_from;		// "Start Diameterklass ex. 200 = 20 cm"
	double m_fDCLS_to;			// "Till Diameterklass ex. 400 = 40 cm"
	double m_fM3Sk;					// "Tr�dvolym i m3sk"
	double m_fM3Fub;				// "Tr�dvolym i m3fub"
	double m_fM3Ub;					// "Tr�dvolym i m3ub"
	int m_nTreeType;				// "0 = Ej provtr�d, 1 = Provtr�d, 2 = I gatan, 3 = Kanttr�d"
	CString m_sBonitet;			// "Jonsson bonitet"
	CString m_sCreated;
	CString m_sCoord;	// Koordinat
	int m_nDistCable;	// "Avst�nd till fas"
	int m_nCategory;	// Kategori p� tr�d (anv�ndardefinerat)
	int m_nTopCut;	// "Toppningsv�rde"
public:
	// Default constructor
	CTransaction_sample_tree(void);
	
	CTransaction_sample_tree(
int tree_id,
		int trakt_id,
		int plot_id,
		int spc_id,
		LPCTSTR spc_name,
		double dbh,
		double hgt,
		double gcrown_perc,
		double bark_thick,
		double grot,
		int age,
		int growth,
		double dcls_from,
		double dcls_to,
		double m3sk,
		double m3fub,
		double m3ub,
		int tree_type,
		LPCTSTR bonitet,
		LPCTSTR created,
		LPCTSTR coord = L"",
		int distCable = 0,
		int category = 0,
		int topcut = 0);
	
	// Copy constructor; 060320 p�d
	CTransaction_sample_tree(const CTransaction_sample_tree &c);

	inline BOOL operator ==(CTransaction_sample_tree &c) const
	{
		return (m_nTreeID == c.m_nTreeID &&
						m_nTraktID == c.m_nTraktID &&
						m_nPlotID == c.m_nPlotID &&
						m_nSpcID == c.m_nSpcID &&
						m_sSpcName == c.m_sSpcName &&
						m_fDbh == c.m_fDbh &&
						m_fHgt == c.m_fHgt &&
						m_fGCrownPerc == c.m_fGCrownPerc &&
						m_fBarkThick == c.m_fBarkThick &&
						m_fGROT == c.m_fGROT &&
						m_nAge == c.m_nAge &&
						m_nGrowth == c.m_nGrowth &&
						m_fDCLS_from == c.m_fDCLS_from &&
						m_fDCLS_to == c.m_fDCLS_to &&
						m_fM3Sk == c.m_fM3Sk &&
						m_fM3Fub == c.m_fM3Fub &&
						m_fM3Ub == c.m_fM3Ub &&
						m_nTreeType == c.m_nTreeType &&
						m_sBonitet == c.m_sBonitet);
	}


	int getTreeID(void);
	int getTraktID(void);
	int getPlotID(void);
	int getSpcID(void);
	CString getSpcName(void);
	double getDBH(void);
	void setDBH(double v);
	double getHgt(void);
	void setHgt(double v);
	double getGCrownPerc(void);
	double getBarkThick(void);
	void setBarkThick(double v);
	double getGROT(void);
	void setGROT(double v);
	int getAge(void);
	int getGrowth(void);
	double getDCLS_from(void);
	void setDCLS_from(double v);
	double getDCLS_to(void);
	void setDCLS_to(double v);
	double getM3Sk(void);
	void setM3Sk(double v);
	double getM3Fub(void);
	void setM3Fub(double v);
	double getM3Ub(void);
	void setM3Ub(double v);
	int getTreeType(void);
	CString getBonitet(void);
	CString getCoord(void);
	CString getCreated(void);
	int getDistCable(void);
	int getCategory(void);
	int getTopCut(void);
	void setTopCut(int);
	void setCoord(CString);
};

typedef std::vector<CTransaction_sample_tree> vecTransactionSampleTree;



////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_dcsl_tree

class CTransaction_dcls_tree
{
//private:
	//--------------------------------------------
	// Key field
	int m_nTreeID;					// Incemental number from 1 to number of trees for Trakt/Plot
	int m_nTraktID;					// ID of trakt
	//--------------------------------------------
	int m_nPlotID;					// Plot (Group)
	int m_nSpcID;						// ID of specie
	CString m_sSpcName;			// Name of specie
	double m_fDCLS_from;		// "Start Diameterklass ex. 200 = 20 cm"
	double m_fDCLS_to;			// "Slut Diameterklass ex. 400 = 40 cm"
	double m_fHgt;					// "H�jd i dm"
	long m_nNumOf;						// "Antal i diameterklass"
	double m_fGCrownPerc;		// "Gr�nkrona procent"
	double m_fBarkThick;		// "Barktjocklek i mm (dubbla)"
	double m_fM3Sk;					// "Volym i m3sk"
	double m_fM3Fub;				// "Volym i m3fub"
	double m_fM3Ub;					// "Volym i m3ub under bark"
	double m_fGROT;					// "Biomassa i kg"
	int m_nAge;							// "�lder"
	int m_nGrowth;					// "Tillv�xt"
	int m_nNumOfRandTrees;	// "Antal kanttr�d (Intr�ng)"
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_dcls_tree(void);
	
	CTransaction_dcls_tree(int tree_id,int trakt_id,
										int plot_id,int spc_id,LPCTSTR spc_name,
										double dcls_from,double dcls_to,double hgt,long num_of,
										double gcrown_perc,double bark_thick,
										double m3sk,double m3fub,double m3ub,double grot,
										int age,int growth,int rand_trees,LPCTSTR created);
	
	// Copy constructor; 060320 p�d
	CTransaction_dcls_tree(const CTransaction_dcls_tree &c);

	inline BOOL operator ==(CTransaction_dcls_tree &c) const
	{
		return (m_nTreeID == c.m_nTreeID &&
						m_nTraktID == c.m_nTraktID &&
						m_nPlotID == c.m_nPlotID &&
						m_nSpcID == c.m_nSpcID &&
						m_sSpcName == c.m_sSpcName &&
						m_fDCLS_from == c.m_fDCLS_from &&
						m_fDCLS_to == c.m_fDCLS_to &&
						m_fHgt == c.m_fHgt &&
						m_nNumOf == c.m_nNumOf &&
						m_fGCrownPerc == c.m_fGCrownPerc &&
						m_fBarkThick == c.m_fBarkThick &&
						m_fM3Sk == c.m_fM3Sk &&
						m_fM3Fub == c.m_fM3Fub &&
						m_fM3Ub == c.m_fM3Ub &&
						m_fGROT == c.m_fGROT &&
						m_nAge == c.m_nAge &&
						m_nGrowth == c.m_nGrowth &&
						m_nNumOfRandTrees == c.m_nNumOfRandTrees);
	}

	int getTreeID(void);
	int getTraktID(void);
	int getPlotID(void);
	int getSpcID(void);
	CString getSpcName(void);
	double getDCLS_from(void);
	void setDCLS_from(double v);
	double getDCLS_to(void);
	void setDCLS_to(double v);
	double getHgt(void);
	void setHgt(double v);
	long getNumOf(void);
	void setNumOf(int v);
	double getGCrownPerc(void);
	void setGCrownPerc(double v);
	double getBarkThick(void);
	void setBarkThick(double v);
	double getM3sk(void);
	void setM3sk(double v);
	double getM3fub(void);
	void setM3fub(double v);
	double getM3ub(void);
	void setM3ub(double v);
	double getGROT(void);
	void setGROT(double v);
	int getAge(void);
	int getGrowth(void);
	int getNumOfRandTrees(void);
	void setNumOfRandTrees(int val);
	CString getCreated(void);
};

typedef std::vector<CTransaction_dcls_tree> vecTransactionDCLSTree;


////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_tree_assort

class CTransaction_tree_assort
{
private:
	int m_nTraktID;
	int m_nTreeID;						
	double m_fDCLS;
	int m_nSpcID;
	CString m_sSpcName;
	CString m_sAssortName;
	double m_fExchVolume;		// "Utbytesber�knad volym f�r sortiment"
	double m_fExchPrice;		// "Utbytesber�knat pris f�r Sortiment och Tr�dslag per Diamterklass"
	int m_nPriceIn;					// 0 = m3fub, 1 = m3to etc
public:
	// Default constructor
	CTransaction_tree_assort(void);
	
	CTransaction_tree_assort(int trakt_id,
													 int tree_id,
													 double dcls,
													 int spc_id,
													 LPCTSTR spc_name,
													 LPCTSTR assort_name,
													 double exch_volume,
													 double exch_price,
													 int price_in);
	
	// Copy constructor; 060320 p�d
	CTransaction_tree_assort(const CTransaction_tree_assort &c);

	inline BOOL operator ==(CTransaction_tree_assort &c) const
	{
		return (m_nTraktID == c.m_nTraktID &&
						m_nTreeID == c.m_nTreeID &&
						m_fDCLS == c.m_fDCLS &&
						m_nSpcID == c.m_nSpcID &&
						m_sSpcName == c.m_sSpcName &&
						m_sAssortName == c.m_sAssortName &&
						m_fExchVolume == c.m_fExchVolume &&
						m_fExchPrice == c.m_fExchPrice &&
						m_nPriceIn == c.m_nPriceIn);
	}

	int getTraktID(void);
	int getTreeID(void);
	double getDCLS(void);
	int getSpcID(void);
	CString getSpcName(void);
	CString getAssortName(void);
	double getExchVolume(void);
	void setExchVolume(double v);
	double getExchPrice(void);
	int getPriceIn(void);
};

typedef std::vector<CTransaction_tree_assort> vecTransactionTreeAssort;



// Property transaction classes; 061129 p�d
class CTransaction_property
{
	int m_nID;
	CString m_sCountyCode;
	CString m_sMunicipalCode;
	CString m_sParishCode;
	CString m_sCountyName;
	CString m_sMunicipalName;
	CString m_sParishName;
	CString m_sPropertyNum;
	CString m_sPropertyName;
	CString m_sBlock;
	CString m_sUnit;
	double m_fAreal;
	double m_fArealMeasured;
	CString m_sCreatedBy;
	CString m_sCreated;
	CString m_sObjID;
#ifdef VER120
	CString m_sMottagarref;
	CString m_sPropFullName;
#endif
	CString m_sCoord;	//#3735

public:
	// Default constructor
	CTransaction_property(void);

	CTransaction_property(int id,
												LPCTSTR county_code,
												LPCTSTR municipal_code,
												LPCTSTR parish_code,
												LPCTSTR county_name,
												LPCTSTR municipal_name,
												LPCTSTR parish_name,
												LPCTSTR prop_num,
												LPCTSTR prop_name,
												LPCTSTR block,
												LPCTSTR unit,
												double areal,
												double areal_measured,
												LPCTSTR created_by,
												LPCTSTR created,
												LPCTSTR obj_id,
												LPCTSTR prop_full_name,
												LPCTSTR m_sMottagarref,
												LPCTSTR sCoord = _T(""));
	
	// Copy constructor
	CTransaction_property(const CTransaction_property &);

	inline BOOL operator ==(CTransaction_property &c) const
	{
		return (m_nID == c.m_nID &&
						m_sCountyCode == c.m_sCountyCode &&
						m_sMunicipalCode == c.m_sMunicipalCode &&
						m_sParishCode == c.m_sParishCode &&
						m_sCountyName == c.m_sCountyName &&
						m_sMunicipalName == c.m_sMunicipalName &&
						m_sParishName == c.m_sParishName &&
						m_sPropertyNum == c.m_sPropertyNum &&
						m_sPropertyName == c.m_sPropertyName &&
						m_sBlock == c.m_sBlock &&
						m_sUnit == c.m_sUnit &&
						m_fAreal == c.m_fAreal &&
						m_fArealMeasured == c.m_fArealMeasured &&
						m_sCreatedBy == c.m_sCreatedBy &&
						m_sObjID == c.m_sObjID && 
						m_sPropFullName == c.m_sPropFullName &&
						m_sMottagarref == c.m_sMottagarref &&
						m_sCoord == c.m_sCoord);
	}


	int getID(void);
	CString getCountyCode(void);
	CString getMunicipalCode(void);
	CString getParishCode(void);
	CString getCountyName(void);
	CString getMunicipalName(void);
	CString getParishName(void);
	CString getPropertyNum(void);
	CString getPropertyName(void);
	CString getBlock(void);
	CString getUnit(void);
	double getAreal(void);
	double getArealMeasured(void);
	CString getCreatedBy(void);
	CString getCreated(void);
	CString getObjectID(void);
	CString getPropFullName(void);
	CString getMottagarref(void);
	CString getCoord(void);
};

typedef std::vector<CTransaction_property> vecTransactionProperty;

// Property transaction classes; 070118 p�d
class CTransaction_prop_owners
{
	int m_nPropID;
	int m_nContactID;
	CString m_sShareOff;
	int m_nIsContact;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_prop_owners(void);

	CTransaction_prop_owners(int prop_id,
		                       int contact_id,
      										 LPCTSTR share_off,
													 int is_contact,
												   LPCTSTR created);
	
	// Copy constructor
	CTransaction_prop_owners(const CTransaction_prop_owners &);

	inline BOOL operator ==(CTransaction_prop_owners &c) const
	{
		return (m_nPropID == c.m_nPropID &&
						m_nContactID == c.m_nContactID &&
						m_sShareOff == c.m_sShareOff &&
						m_nIsContact == c.m_nIsContact);
	}


	int getPropID(void);
	int getContactID(void);
	CString getShareOff(void);
	int getIsContact(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_prop_owners> vecTransactionPropOwners;


////////////////////////////////////////////////////////////////////////////////////////
// This is a structure, based on data from two different
// tables; fst_prop_owners_table and fst_contacts_table
// This'll hold information on each property owner
// per property; 070126 p�d

typedef class CTransaction_prop_owners_for_property_data
{
	int nPropID;
	int nIsContact;
	CString sOwnerShare;
	CTransaction_contacts Contacts;
public:
	CTransaction_prop_owners_for_property_data(void)
	{
		nPropID = -1;
		nIsContact = 0;
		sOwnerShare = _T("");
		Contacts = CTransaction_contacts();
	}
	CTransaction_prop_owners_for_property_data(int prop_id,int is_contact,LPCTSTR owner_share,CTransaction_contacts& data)
	{
		nPropID = prop_id;
		nIsContact = is_contact;
		sOwnerShare = owner_share;
		Contacts = CTransaction_contacts(data);
	}

	inline BOOL operator ==(CTransaction_prop_owners_for_property_data &c) const
	{
		return (nPropID == c.nPropID &&
						nIsContact == c.nIsContact &&
						sOwnerShare == c.sOwnerShare &&
						Contacts == c.Contacts);
	}


	int getPropID(void)													{ return nPropID; }
	int getIsContact(void)											{ return nIsContact; }
	CString getOwnerShare(void)									{ return sOwnerShare; }
	CTransaction_contacts& getContactData(void)	{ return Contacts; }

} PROP_OWNERS_FOR_PROPERTY_DATA;

typedef std::vector<CTransaction_prop_owners_for_property_data> vecTransactionPropOwnersForPropertyData;


////////////////////////////////////////////////////////////////////////////////////////
// This class is used in Template transfers; 070828 p�d
class CTransaction_template_transfers
{
	int m_nFromAssortID;
	CString m_sFromAssort;
	int m_nToAssortID;
	CString m_sToAssort;
	double m_fM3Fub;
	double m_fPercent;
	CString m_sCreated;
public:
	CTransaction_template_transfers(void)
	{
		m_nFromAssortID = -1;
		m_sFromAssort = _T("");
		m_nToAssortID = -1;
		m_sToAssort = _T("");
		m_fM3Fub = 0.0;
		m_fPercent = 0.0;
		m_sCreated = _T("");
	}

	CTransaction_template_transfers(int from_id,LPCTSTR from_assort,int to_id,LPCTSTR to_assort,double m3fub,double percent,LPCTSTR created)
	{
		m_nFromAssortID = from_id;
		m_sFromAssort = from_assort;
		m_nToAssortID = to_id;
		m_sToAssort = to_assort;
		m_fM3Fub = m3fub;
		m_fPercent = percent;
		m_sCreated = created;
	}

	inline BOOL operator ==(CTransaction_template_transfers &c) const
	{
		return (m_nFromAssortID == c.m_nFromAssortID &&
						m_sFromAssort == c.m_sFromAssort &&
						m_nToAssortID == c.m_nToAssortID &&
						m_sToAssort == c.m_sToAssort &&
						m_fM3Fub == c.m_fM3Fub &&
						m_fPercent == c.m_fPercent);
	}


	int getFromAssortID(void)			{ return m_nFromAssortID; }
	CString getFromAssort(void)		{ return m_sFromAssort; }
	int getToAssortID(void)				{ return m_nToAssortID; }
	CString getToAssort(void)			{ return m_sToAssort; }
	double getM3Fub(void)					{ return m_fM3Fub; }
	double getPercent(void)				{ return m_fPercent; }
	CString getCreated(void)			{ return m_sCreated; }
};

typedef std::vector<CTransaction_template_transfers> vecTransactionTemplateTransfers;


////////////////////////////////////////////////////////////////////////////////////////
// Cost Template transaction class; 071008 p�d
class CTransaction_costtempl
{
	int m_nID;
	CString m_sTemplateName;
	int m_nTypeOf;
	CString m_sTemplateFile;
	CString m_sTemplateNotes;
	CString m_sCreatedBy;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_costtempl(void);

	CTransaction_costtempl(int id,LPCTSTR tmpl_name,int type_of,LPCTSTR tmpl_xml,
															 LPCTSTR notes,LPCTSTR created_by,LPCTSTR created = _T(""));
	
	// Copy constructor
	CTransaction_costtempl(const CTransaction_costtempl &);

	inline BOOL operator ==(CTransaction_costtempl &c) const
	{
		return (m_nID == c.m_nID &&
						m_sTemplateName == c.m_sTemplateName &&
						m_nTypeOf == c.m_nTypeOf &&
						m_sTemplateFile == c.m_sTemplateFile &&
						m_sTemplateNotes == c.m_sTemplateNotes &&
						m_sCreatedBy == c.m_sCreatedBy);
	}

	int getID(void);
	CString getTemplateName(void);
	int getTypeOf(void);
	CString getTemplateFile(void);
	CString getTemplateNotes(void);
	CString getCreatedBy(void);
	CString getCreated(void);
};

typedef std::vector<CTransaction_costtempl> vecTransaction_costtempl;


// Get costs for cutting; 071008 p�d
class CTransaction_costtempl_cutting
{
	double m_fCut1;
	double m_fCut2;
	double m_fCut3;
	double m_fCut4;
public:
	CTransaction_costtempl_cutting(void)
	{
		m_fCut1	= 0.0;
		m_fCut2	= 0.0;
		m_fCut3	= 0.0;
		m_fCut4	= 0.0;
	}

	CTransaction_costtempl_cutting(double c1,double c2,double c3,double c4)
	{
		m_fCut1	= c1;
		m_fCut2	= c2;
		m_fCut3	= c3;
		m_fCut4	= c4;
	}

	CTransaction_costtempl_cutting(const CTransaction_costtempl_cutting &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_costtempl_cutting &c) const
	{
		return (m_fCut1 == c.m_fCut1 &&
						m_fCut2 == c.m_fCut2 &&
						m_fCut3 == c.m_fCut3 &&
						m_fCut4 == c.m_fCut4);
	}


	double getCut1(void)			{ return m_fCut1; }
	void setCut1(double v)		{ m_fCut1 = v; }
	double getCut2(void)			{ return m_fCut2; }
	void setCut2(double v)		{ m_fCut2 = v; }
	double getCut3(void)			{ return m_fCut3; }
	void setCut3(double v)		{ m_fCut3 = v; }
	double getCut4(void)			{ return m_fCut4; }
	void setCut4(double v)		{ m_fCut4 = v; }

};


// Get costs for transport per specie; 071008 p�d
class CTransaction_costtempl_transport
{
	int m_nSpcID;
	CString m_sSpecie;
	double m_fDistance;
	double m_fCost;
	double m_fMaxCost;
public:
	CTransaction_costtempl_transport(void)
	{
		m_nSpcID = -1;
		m_sSpecie = _T("");
		m_fDistance = 0.0;
		m_fCost	= 0.0;
		m_fMaxCost = 0.0;
	}

	CTransaction_costtempl_transport(int spc_id,LPCTSTR spc_name,double distance,double cost,double max_cost)
	{
		m_nSpcID = spc_id;
		m_sSpecie = (spc_name);
		m_fDistance = distance;
		m_fCost	= cost;
		m_fMaxCost = max_cost;

	}

	CTransaction_costtempl_transport(const CTransaction_costtempl_transport &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_costtempl_transport &c) const
	{
		return (m_nSpcID == c.m_nSpcID &&
						m_sSpecie == c.m_sSpecie &&
						m_fDistance == c.m_fDistance &&
						m_fCost == c.m_fCost &&
						m_fMaxCost == c.m_fMaxCost);
	}


	int getSpcID(void)							{ return m_nSpcID; }
	CString getSpecieName(void)			{ return m_sSpecie; }
	void setSpecieName(LPCTSTR v)		{ m_sSpecie = v; }
	double getDistance(void)				{ return m_fDistance; }
	double getCost(void)						{ return m_fCost; }
	double getMaxCost(void)					{ return m_fMaxCost; }

};

typedef std::vector<CTransaction_costtempl_transport> vecTransaction_costtempl_transport;


// Get costs for other costs; 071008 p�d
class CTransaction_costtempl_other_costs
{
	double m_fCost;
	CString m_sTypeOfCost;
public:
	CTransaction_costtempl_other_costs(void)
	{
		m_fCost	= 0.0;
		m_sTypeOfCost = _T("");
	}

	CTransaction_costtempl_other_costs(double cost,LPCTSTR type_of_cost)
	{
		m_fCost	= cost;
		m_sTypeOfCost = type_of_cost;
	}

	CTransaction_costtempl_other_costs(const CTransaction_costtempl_other_costs &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_costtempl_other_costs &c) const
	{
		return (m_fCost == c.m_fCost && m_sTypeOfCost == c.m_sTypeOfCost);
	}


	double getCost(void)					{ return m_fCost; }
	CString getTypeOfCOst(void)		{ return m_sTypeOfCost; }

};

////////////////////////////////////////////////////////////////////////////////////////
// Object Cost Template transaction class; 080212 p�d
class CObjectCostTemplate_table
{
private:
	int m_nSpcID;
	CString m_sSpcName;
protected:
	vecInt m_vecValues_int;
	vecFloat m_vecValues_float;
public:
	CObjectCostTemplate_table(void)
	{
		m_nSpcID = -1;
		m_sSpcName = _T("");
		m_vecValues_int.clear();
		m_vecValues_float.clear();
	}

	CObjectCostTemplate_table(int spc_id,LPCTSTR spc_name)
	{
		m_nSpcID = spc_id;
		m_sSpcName = (spc_name);
	}
	
	inline BOOL operator ==(CObjectCostTemplate_table &c) const
	{
		return (m_nSpcID == c.m_nSpcID && m_sSpcName == c.m_sSpcName);
	}


	int getSpcID(void)								{ return m_nSpcID; }
	CString getSpcName(void)					{ return m_sSpcName; }
	void setSpcName(LPCTSTR v)				{ m_sSpcName = v; }
	vecInt &getValues_int(void)				{ return m_vecValues_int; }
	void setValue_int(int value)			{ m_vecValues_int.push_back(value); }
	vecFloat &getValues_float(void)		{ return m_vecValues_float; }
	void setValue_float(double value)	{ m_vecValues_float.push_back(value); }

};

typedef std::vector<CObjectCostTemplate_table> vecObjectCostTemplate_table;

class CObjectCostTemplate_other_cost_table
{
	//private:
	double m_fPrice;
	CString m_sNotes;
	double m_fIsVAT;
public:
	CObjectCostTemplate_other_cost_table(void)
	{
		m_fPrice = 0.0;
		m_sNotes = _T("");
		m_fIsVAT = 0.0;
	}
	CObjectCostTemplate_other_cost_table(double price,LPCTSTR text,double vat)
	{
		m_fPrice = price;
		m_sNotes = text;
		m_fIsVAT = vat;
	}
	CObjectCostTemplate_other_cost_table(const CObjectCostTemplate_other_cost_table &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CObjectCostTemplate_other_cost_table &c) const
	{
		return (m_fPrice == c.m_fPrice && m_sNotes == c.m_sNotes && m_fIsVAT == c.m_fIsVAT);
	}

	double getPrice(void)			{ return m_fPrice; }
	CString getNotes(void)		{ return m_sNotes; }
	double getIsVAT(void)			{ return m_fIsVAT; }

};

typedef std::vector<CObjectCostTemplate_other_cost_table> vecObjectCostTemplate_other_cost_table;

//////////////////////////////////////////////////////////////////////////////////////////
// transaction class: CTransaction_transport_data. 
class CTransaction_transport_data
{
	int m_nIdentifer;
	CString m_sSpcName;
	vecInt m_vecInt;					// Holds distance and cost/distance
	vecFloat m_vecFloat;		// Holds distance and cost/distance
public:
	CTransaction_transport_data(void)
	{
		m_nIdentifer = -1;
		m_sSpcName = _T("");
		m_vecInt.clear();
		m_vecFloat.clear();
	}
	CTransaction_transport_data(int identifer,LPCTSTR spc_name,vecInt &vec)
	{
		m_nIdentifer = identifer;
		m_sSpcName = (spc_name);
		m_vecInt = vec;
	}
	CTransaction_transport_data(int identifer,LPCTSTR spc_name,vecFloat &vec)
	{
		m_nIdentifer = identifer;
		m_sSpcName = (spc_name);
		m_vecFloat = vec;
	}
	// Copy constructor
	CTransaction_transport_data(const CTransaction_transport_data &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_transport_data &c) const
	{
		return (m_nIdentifer == c.m_nIdentifer && m_sSpcName == c.m_sSpcName && (m_vecInt == c.m_vecInt || m_vecFloat == c.m_vecFloat));
	}

	int getIdentifer(void)					{ return m_nIdentifer; }
	LPCTSTR getSpcName(void)				{ return m_sSpcName; }
	void setSpcName(LPCTSTR v)			{ m_sSpcName = v; }
	vecInt &getInts(void)						{ return m_vecInt; }
	void setInts(int value)					{ m_vecInt.push_back(value); }
	vecFloat &getFloats(void)				{ return m_vecFloat; }
	void setFloats(double value)		{ m_vecFloat.push_back(value); }
};

typedef std::vector<CTransaction_transport_data> vecTransaction_transport_data;

///////////////////////////////////////////////////////////////////////////////////////////
// Get trakt rotpost; 071008 p�d
class CTransaction_trakt_rotpost
{
	int m_nRotTraktID;
	double m_fVolume;		// "Gagnvirke"
	double m_fValue;		// "Virkesv�rde"
	double m_fCost1;		// "Huggningskostnad"
	double m_fCost2;		// "Sk�rdarkostnad"
	double m_fCost3;		// "Skotarkostnad"
	double m_fCost4;		// "�vrig kostnad"
	double m_fCost5;		// "Transportkostnad"
	double m_fCost6;		// "Fast kostnad"
	double m_fCost7;		
	double m_fCost8;
	double m_fCost9;
	double m_fNetto;		// "Rotnetto"
	int m_nOrigin;			// "Ursprung: 1 = "Vanlig" 2 = "Intr�ngsers�ttning" etc
	CString m_sCreated;
public:
	CTransaction_trakt_rotpost(void)
	{
		m_nRotTraktID = -1;
		m_fVolume = 0.0;	// "Gagnvirke"
		m_fValue = 0.0;		// "Virkesv�rde"
		m_fCost1 = 0.0;		// "Huggningskostnad"
		m_fCost2 = 0.0;		// "Sk�rdarkostnad"
		m_fCost3 = 0.0;		// "Skotarkostnad"
		m_fCost4 = 0.0;		// "�vrig kostnad"
		m_fCost5 = 0.0;		// "Transportkostnad"
		m_fCost6 = 0.0;		// "Fast kostnad"
		m_fCost7 = 0.0;		
		m_fCost8 = 0.0;
		m_fCost9 = 0.0;
		m_fNetto = 0.0;		// "Rotnetto"
		m_nOrigin	= 0;		// Not set
		m_sCreated = _T("");
	}

	CTransaction_trakt_rotpost(int rot_trakt_id,double volume,double value,
														 double cost1,double cost2,double cost3,double cost4,
														 double cost5,double cost6,double cost7,double cost8,
														 double cost9,double netto,int origin,LPCTSTR create_date)
	{
		m_nRotTraktID = rot_trakt_id;
		m_fVolume = volume;	// "Gagnvirke"
		m_fValue = value;		// "Virkesv�rde"
		m_fCost1 = cost1;		// "Huggningskostnad"
		m_fCost2 = cost2;		// "Sk�rdarkostnad"
		m_fCost3 = cost3;		// "Skotarkostnad"
		m_fCost4 = cost4;		// "�vrig kostnad"
		m_fCost5 = cost5;		// "Transportkostnad"
		m_fCost6 = cost6;		// "Fast kostnad"
		m_fCost7 = cost7;		
		m_fCost8 = cost8;
		m_fCost9 = cost9;
		m_fNetto = netto;		// "Rotnetto"
		m_nOrigin	= origin;		
		m_sCreated = create_date;
	}

	CTransaction_trakt_rotpost(const CTransaction_trakt_rotpost &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_trakt_rotpost &c) const
	{
		return (m_nRotTraktID == c.m_nRotTraktID &&
						m_fVolume == c.m_fVolume &&
						m_fValue == c.m_fValue &&
						m_fCost1 == c.m_fCost1 &&
						m_fCost2 == c.m_fCost2 &&
						m_fCost3 == c.m_fCost3 &&
						m_fCost4 == c.m_fCost4 &&
						m_fCost5 == c.m_fCost5 &&
						m_fCost6 == c.m_fCost6 &&
						m_fCost7 == c.m_fCost7 &&
						m_fCost8 == c.m_fCost8 &&
						m_fCost9 == c.m_fCost9 &&
						m_fNetto == c.m_fNetto &&
						m_nOrigin == c.m_nOrigin);
	}


	int getRotTraktID(void)				{ return m_nRotTraktID; }
	double getVolume(void)				{ return m_fVolume; }
	double getValue(void)					{ return m_fValue;	}
	double getCost1(void)					{ return m_fCost1; }
	double getCost2(void)					{ return m_fCost2; }
	double getCost3(void)					{ return m_fCost3; }
	double getCost4(void)					{ return m_fCost4; }
	double getCost5(void)					{ return m_fCost5; }
	double getCost6(void)					{ return m_fCost6; }
	double getCost7(void)					{ return m_fCost7; }
	double getCost8(void)					{ return m_fCost8; }
	double getCost9(void)					{ return m_fCost9; }
	double getNetto(void)					{ return m_fNetto; }
	int getOrigin(void)						{ return m_nOrigin; }
	CString getCreated(void)			{ return m_sCreated; }

};

typedef std::vector<CTransaction_trakt_rotpost> vecCTransaction_trakt_rotpost;


///////////////////////////////////////////////////////////////////////////////////////////
// Get trakt rotpost specie; 080306 p�d
class CTransaction_trakt_rotpost_spc
{
	int m_nRotSpcID_pk;			// Equals specie id
	int m_nRotSpcTraktID_pk;
	CString m_sRotSpcName;
	double m_fCost1;		// "Kostnad Transport kr/m3"
	double m_fCost2;		// "SUMMA; Kostnad Transport kr"
	double m_fCost3;		// "Kostnad Huggning kr/m3"
	double m_fCost4;		// "SUMMA; Kostnad Huggning kr"
	double m_fCost5;		// "Kostnad Transport till Industri kr/m3"
	double m_fCost6;		// "SUMMA; Kostnad Transport till Industri kr"
	int m_nOrigin;			// "Ursprung: ALLTID 2 = "Intr�ngsers�ttning"
	CString m_sCreated;
public:
	CTransaction_trakt_rotpost_spc(void)
	{
		m_nRotSpcID_pk = -1;			// Equals specie id
		m_nRotSpcTraktID_pk = -1;
		m_sRotSpcName = _T("");
		m_fCost1 = 0.0;	
		m_fCost2 = 0.0;		
		m_fCost3 = 0.0;		
		m_fCost4 = 0.0;		
		m_fCost5 = 0.0;		
		m_fCost6 = 0.0;		
		m_nOrigin	= 0;		
		m_sCreated = _T("");
	}

	CTransaction_trakt_rotpost_spc(int rot_spc_id,int rot_spc_trakt_id,
																 LPCTSTR spc_name,
																 double cost1,double cost2,double cost3,
																 double cost4,double cost5,double cost6,
																 int origin,LPCTSTR create_date)
	{
		m_nRotSpcID_pk = rot_spc_id;			// Equals specie id
		m_nRotSpcTraktID_pk = rot_spc_trakt_id;
		m_sRotSpcName = (spc_name);
		m_fCost1 = cost1;		
		m_fCost2 = cost2;		
		m_fCost3 = cost3;		
		m_fCost4 = cost4;		
		m_fCost5 = cost5;	
		m_fCost6 = cost6;
		m_nOrigin	= origin;		
		m_sCreated = (create_date);
	}

	CTransaction_trakt_rotpost_spc(const CTransaction_trakt_rotpost_spc &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_trakt_rotpost_spc &c) const
	{
		return (m_nRotSpcID_pk == c.m_nRotSpcID_pk &&
						m_nRotSpcTraktID_pk == c.m_nRotSpcTraktID_pk &&
						m_sRotSpcName == c.m_sRotSpcName &&
						m_fCost1 == c.m_fCost1 &&
						m_fCost2 == c.m_fCost2 &&
						m_fCost3 == c.m_fCost3 &&
						m_fCost4 == c.m_fCost4 &&
						m_fCost5 == c.m_fCost5 &&
						m_fCost6 == c.m_fCost6 &&
						m_nOrigin == c.m_nOrigin);
	}


	int getRotSpcID_pk(void)			{ return m_nRotSpcID_pk; }
	int getRotSpcTraktID_pk(void)	{ return m_nRotSpcTraktID_pk; }
	CString getSpcName(void)			{ return m_sRotSpcName; }
	double getCost1(void)					{ return m_fCost1; }
	double getCost2(void)					{ return m_fCost2; }
	double getCost3(void)					{ return m_fCost3; }
	double getCost4(void)					{ return m_fCost4; }
	double getCost5(void)					{ return m_fCost5; }
	double getCost6(void)					{ return m_fCost6; }
	int getOrigin(void)						{ return m_nOrigin; }
	CString getCreated(void)			{ return m_sCreated; }

};

typedef std::vector<CTransaction_trakt_rotpost_spc> vecTransaction_trakt_rotpost_spc;


///////////////////////////////////////////////////////////////////////////////////////////
// Get trakt rotpost other; 080306 p�d
class CTransaction_trakt_rotpost_other
{
	int m_nOtcRotID_pk;		
	int m_nOtcRotTraktID_pk;
	double m_fCost1;		// "�vrig kostnad (intr�ng, fast kostnad etc.)"
	double m_fCost2;		// "Summa �vrig kostnad (kan vara utr�knat fr�n ett procenttal ex. MOMS)"
	CString m_sText;		// "F�rklarande text (samma som i mallen)"
	double m_fPercent;	// "Procenttal f�r utr�kning av kostnad (ex. MOMS=25%"
	int m_nType;				// "Typ; 1 = "�vrig intr�ngsers�ttning 2 = �vriga fasta kostnader"
	CString m_sCreated;
public:
	CTransaction_trakt_rotpost_other(void)
	{
		m_nOtcRotID_pk = -1;		
		m_nOtcRotTraktID_pk = -1;
		m_fCost1 = 0.0;			// "�vrig kostnad (intr�ng, fast kostnad etc.)"
		m_fCost2 = 0.0;			// "Summa �vrig kostnad (kan vara utr�knat fr�n ett procenttal ex. MOMS)"
		m_sText = _T("");		// "F�rklarande text (samma som i mallen)"
		m_fPercent = 0.0;		// "Procenttal f�r utr�kning av kostnad (ex. MOMS=25%"
		m_nType = 0;				// "Typ; 1 = "�vrig intr�ngsers�ttning 2 = �vriga fasta kostnader"
		m_sCreated = _T("");
	}

	CTransaction_trakt_rotpost_other(int otcrot_id,int otcrot_trakt_id,
																 double cost1,double cost2,LPCTSTR text,double percent,
																 int type,LPCTSTR create_date)
	{
		m_nOtcRotID_pk = otcrot_id;		
		m_nOtcRotTraktID_pk = otcrot_trakt_id;
		m_fCost1 = cost1;										
		m_fCost2 = cost2;			
		m_sText = (text);		
		m_fPercent = percent;	
		m_nType = type;				
		m_sCreated = (create_date);
	}

	CTransaction_trakt_rotpost_other(const CTransaction_trakt_rotpost_other &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_trakt_rotpost_other &c) const
	{
		return (m_nOtcRotID_pk == c.m_nOtcRotID_pk &&		
						m_nOtcRotTraktID_pk == c.m_nOtcRotTraktID_pk &&
						m_fCost1 == c.m_fCost1 &&
						m_fCost2 == c.m_fCost2 &&
						m_sText == c.m_sText &&
						m_fPercent == c.m_fPercent &&
						m_nType == c.m_nType);
	}

	int getOtcRotID_pk(void)			{ return m_nOtcRotID_pk; }
	int getOtcRotTraktID_pk(void)	{ return m_nOtcRotTraktID_pk; }
	double getCost1(void)					{ return m_fCost1; }
	double getCost2(void)					{ return m_fCost2; }
	CString getText(void)					{ return m_sText; }
	double getPercent(void)				{ return m_fPercent; }
	int getType(void)							{ return m_nType; }
	CString getCreated(void)			{ return m_sCreated; }

};

typedef std::vector<CTransaction_trakt_rotpost_other> vecTransaction_trakt_rotpost_other;


//////////////////////////////////////////////////////////////////////////////////
// Transaction classes for "Intr�ngsv�rdering"; 080116 p�d


// CTransaction_elv_object
class CTransaction_elv_object
{
	//private:
	int m_nObjID_pk;							// Primary key
	CString m_sObjErrandNum;			// "�rendenummer"
	CString m_sObjectName;				// "Objektets namn"
	CString m_sObjIDNumber;				// "Objektets id nummer"
	CString m_sObjLittra;					// "Ledningslittra"
	CString m_sObjGrowthArea;			// "Tillv�xtomr�de 1-6"
	double m_fObjTransportDist;		// "Transportavst�nd"
	int m_nObjLatitude;						// "Breddgrad"
	int m_nObjAltitude;						// "H�jd �ver havet"
	CString m_sObjTypeOfInfring;	// "Typ av intr�ng (ex. breddning)"
	double m_fObjLength;					// "Str�ckning i (km)"
	double m_fObjPresentWidth1;		// "Nuvaranade bredd 1"
	double m_fObjPresentWidth2;		// "Nuvaranade bredd 2"
	double m_fObjAddedWidth1;			// "Tillkommande bredd 1"
	double m_fObjAddedWidth2;			// "Tillkommande bredd 2"
	double m_fObjParallelWidth;			// "Parallell bredd"
	int m_nObjTypeOfNet;					// "Typ av n�t Lokalt eller Regionalt"
	double m_fObjPriceDiv1;				// "Prisf�rdelning (I Gatan)"
	double m_fObjPriceDiv2;				// "Prisf�rdelning (Kanttr�d)"
	//---------------------------------------------------------------
	// "Frivillig uppg�relse"
	double m_fObjPriceBase;				// "Prisbasbelopp"
	double m_fObjMaxPercent;			// "Max procent p� prisbasbelopp"
	double m_fObjPercent;				// "Procent p� prisbasbelopp"
	double m_fObjPercentOfPriceBase;    // Procent av prisbasbelopp #4424 #4420 20150804 J�
	//---------------------------------------------------------------
	double m_fObjVAT;							// "MOMS"
	//---------------------------------------------------------------
	int m_nObjUseNormID;					// "ID f�r norm att anv�nda till intr�ngsers�ttning"
	CString m_sObjUseNorm;				// "Namn p� skogsnorm f�r ber�kning"
	double m_fObjTakeCareOfPerc;	// "Tillvaratagande procent"
	double m_fObjCorrFactor;			// "Korrektionsfactor"
	CString m_sObjNameOfPricelist;// "Namn p� prislista"	
	int m_nObjTypeOfPricelist;		// "Typ av prislista; 1=Estimate, 2=Medelpris"
	CString m_sObjPricelistXML;		// "Prislista i XML format"
	CString m_sObjNameOfCosts;		// "Namn p� kostnader"	
	int m_nObjTypeOfCosts;				// "Typ av kostnad; 1=Estimate, 2=Intr�ng"
	CString m_sObjCostsXML;				// "Kostnader i XML format"
	//---------------------------------------------------------------
	int m_nObjContractorID;				// "Primary key f�r uppdragsgivare i kontakter"
	//---------------------------------------------------------------
	double m_fObjDCLS;						// "Diameterklass"
	//---------------------------------------------------------------
	CString m_sObjExtraInfo;			// "Information om S�derbergs"
	//---------------------------------------------------------------
	CString m_sObjP30HCostNameP30;	// Name of Template used for P30
	int m_nObjP30HCostTypeOfP30;		// TypeOf for P30 (=1)
	CString m_sObjP30HCostXMLP30;	// XML-tamplate file for P30
	CString m_sObjP30HCostNameHC;	// Name of Template used for HCost
	int m_nObjP30HCostTypeOfHC;		// TypeOf for HCost (=2)
	CString m_sObjP30HCostXMLHC;		// XML-tamplate file for HCost
	//---------------------------------------------------------------
	int m_nIsVATIncluded;						// "Inkludera/Exkludera Moms"
	int m_nIsVoluntaryDealIncluded;	// "Inkludera/Exkludera Frivillig uppg�relse"
	int m_nIsHigherCostsIncluded;		// "Inkludera/Exkludera F�rdyrad avverkning"
	int m_nIsOtherCompensIncluded;	// "Inkludera/Exkludera Annan ers�ttning"
	int m_nIsGrotIncluded;					// "Inkludera/Exkludera Grot"
	//---------------------------------------------------------------
	CString m_sObjCreatedBy;			// "Skapad av, signatur"
	CString m_sObjNotes;					// "Noteringar"
	int m_nObjReturnAddressID;		// "ID f�r returnaddress i kontakter"
	int m_nObjLogoID;							// "ID f�r logo i kontakter"; Added 100225 p�d
	CString m_sObjDate;						// "Datum"
	CString m_sObjStartDate;			// "Startdatum f�r object" 100610 p�d
	double m_fObjRecalcBy;				// "Uppr�kning av Intr�ng (%)" 100615 p�d
	int m_nStatus;						// Status p� objektet 0=p�g�ende, 1=slutf�rd, #3385
public:
	CTransaction_elv_object(void)
	{
		m_nObjID_pk = -1;		
		m_sObjErrandNum = _T("");
		m_sObjectName = _T("");	
		m_sObjIDNumber = _T("");
		m_sObjLittra = _T("");	
		m_sObjGrowthArea = _T("");
		m_fObjTransportDist = 0.0;
		m_nObjLatitude = -1;
		m_nObjAltitude = -1;
		m_sObjTypeOfInfring = _T("");
		m_fObjPresentWidth1 = 0.0;
		m_fObjPresentWidth2 = 0.0;
		m_fObjAddedWidth1 = 0.0;	
		m_fObjAddedWidth2 = 0.0;	
		m_fObjParallelWidth = 0.0;
		m_nObjTypeOfNet = -1;			
		m_fObjPriceDiv1 = 0.0;			
		m_fObjPriceDiv2 = 0.0;			
		m_fObjPriceBase = 0.0;			
		m_fObjMaxPercent = 0.0;			
		m_fObjPercentOfPriceBase = 3.0; // Procent av prisbasbelopp #4424 #4420 20150804 J�
		m_fObjPercent = 0.0;				
		m_fObjVAT = 0.0;						
		m_nObjUseNormID = -1;
		m_sObjUseNorm = _T("");
		m_fObjTakeCareOfPerc = 0.0;
		m_fObjCorrFactor = 0.0;
		m_sObjNameOfPricelist = _T("");
		m_nObjTypeOfPricelist = -1;
		m_sObjPricelistXML = _T("");
		m_sObjNameOfCosts = _T("");
		m_nObjTypeOfCosts = -1;
		m_sObjCostsXML = _T("");
		m_nObjContractorID = -1;
		m_fObjDCLS = 0.0;
		m_sObjExtraInfo = _T("");
		m_sObjP30HCostNameP30 = _T("");		// Name of Template used for P30
		m_nObjP30HCostTypeOfP30 = -1;			// TypeOf for P30 (=1)
		m_sObjP30HCostXMLP30 = _T("");		// XML-tamplate file for P30
		m_sObjP30HCostNameHC = _T("");		// Name of Template used for HCost
		m_nObjP30HCostTypeOfHC = -1;			// TypeOf for HCost (=2)
		m_sObjP30HCostXMLHC = _T("");			// XML-tamplate file for HCost
		m_nIsVATIncluded = 1;							// "Includera/Excludera Moms"
		m_nIsVoluntaryDealIncluded = 1;		// "Includera/Excludera Frivillig uppg�relse"
		m_nIsHigherCostsIncluded = 1;			// "Inkludera/Exkludera F�rdyrad avverkning"
		m_nIsOtherCompensIncluded = 1;		// "Inkludera/Exkludera Annan ers�ttning"
		m_nIsGrotIncluded = 1;						// "Inkludera/Exkludera Grot"
		m_sObjCreatedBy = _T("");
		m_sObjNotes = _T("");
		m_nObjReturnAddressID = -1;
		m_nObjLogoID = -1;					// "ID f�rlogo, h�mtas fr�n kontakter"; 100225 p�d
		m_sObjDate = _T("");
		m_fObjLength = 0.0;					// "Str�ckning i (km)"
		m_sObjStartDate = _T("");		// "Startdatum f�r object"
		m_fObjRecalcBy = 0.0;
		m_nStatus = -1;	//-1 = ej initierad, #3385
	}

	CTransaction_elv_object(int id_pk,LPCTSTR errand_num,LPCTSTR name,LPCTSTR id_num,LPCTSTR littra,LPCTSTR growth_area,
													double transp_dist,int latitude,int altitude,LPCTSTR type_of_infring,
													double present_width1,double present_width2,double added_width1,double added_width2,double parallel_width,
													int type_of_net,double price_div1,double price_div2,double price_base,double max_percent,
													double percent,double vat,double fObjPercentOfPriceBase,int use_norm_id,LPCTSTR use_norm,double take_care_of_percent,double corr_factor,
													LPCTSTR name_of_prl,int type_of_pricelist,LPCTSTR pricelist_xml,
													LPCTSTR name_of_costs,int type_of_costs,LPCTSTR costs_xml,int contractor_id,double dcls,LPCTSTR extra_info,
													LPCTSTR p30_name,int p30_typeof,LPCTSTR p30_xml,
													LPCTSTR hcost_name,int hcost_typeof,LPCTSTR hcost_xml,
													int is_vat,int is_voluntary_deal,int is_higher_costs,int is_other_compens,
													LPCTSTR created_by,LPCTSTR notes,int ret_adress_id,int logo_id,int is_grot,LPCTSTR start_date,double recalc_by,double length = 0.0,LPCTSTR date = _T(""),int status = 0)
	{
		m_nObjID_pk = id_pk;		
		m_sObjErrandNum = (errand_num);
		m_sObjectName = (name);	
		m_sObjIDNumber = (id_num);
		m_sObjLittra = (littra);	
		m_sObjGrowthArea = (growth_area);
		m_fObjTransportDist = transp_dist;
		m_nObjLatitude = latitude;
		m_nObjAltitude = altitude;
		m_sObjTypeOfInfring = (type_of_infring);
		m_fObjPresentWidth1 = present_width1;
		m_fObjPresentWidth2 = present_width2;
		m_fObjAddedWidth1 = added_width1;	
		m_fObjAddedWidth2 = added_width2;	
		m_fObjParallelWidth = parallel_width;
		m_nObjTypeOfNet = type_of_net;			
		m_fObjPriceDiv1 = price_div1;			
		m_fObjPriceDiv2 = price_div2;			
		m_fObjPriceBase = price_base;			
		m_fObjMaxPercent = max_percent;			
		m_fObjPercent = percent;				
		m_fObjVAT = vat;	

		if(use_norm_id>=4000)
			m_nObjUseNormID = use_norm_id-4000;
		else
			m_nObjUseNormID = use_norm_id;

		m_sObjUseNorm = (use_norm);
		m_fObjTakeCareOfPerc = take_care_of_percent;
		m_fObjCorrFactor = corr_factor;
		m_sObjNameOfPricelist = (name_of_prl);
		m_nObjTypeOfPricelist = type_of_pricelist;
		m_sObjPricelistXML = (pricelist_xml);
		m_sObjNameOfCosts = (name_of_costs);
		m_nObjTypeOfCosts = type_of_costs;
		m_sObjCostsXML = (costs_xml);
		m_nObjContractorID = contractor_id;
		m_fObjDCLS = dcls;
		m_sObjExtraInfo = (extra_info);
		m_sObjP30HCostNameP30 = (p30_name);		// Name of Template used for P30
		m_nObjP30HCostTypeOfP30 = p30_typeof;		// TypeOf for P30 (=1)
		m_sObjP30HCostXMLP30 = (p30_xml);			// XML-tamplate file for P30
		m_sObjP30HCostNameHC = (hcost_name);		// Name of Template used for HCost
		m_nObjP30HCostTypeOfHC = hcost_typeof;		// TypeOf for HCost (=2)
		m_sObjP30HCostXMLHC = (hcost_xml);			// XML-tamplate file for HCost
		m_nIsVATIncluded = is_vat;
		m_nIsVoluntaryDealIncluded = is_voluntary_deal;
		m_nIsHigherCostsIncluded = is_higher_costs;			// "Inkludera/Exkludera F�rdyrad avverkning"
		m_nIsOtherCompensIncluded = is_other_compens;		// "Inkludera/Exkludera Annan ers�ttning"
		m_nIsGrotIncluded = is_grot;										// "Inkludera/Exkludera Grot"
		m_sObjCreatedBy = (created_by);
		m_sObjNotes = (notes);
		m_nObjReturnAddressID = ret_adress_id;
		m_nObjLogoID = logo_id;
		m_fObjLength = length;					// "Str�ckning i (km)"
		m_sObjDate = (date);
		m_sObjStartDate = start_date;		// "Startdatum f�r object"
		m_fObjRecalcBy = recalc_by;
		m_nStatus = status;
		m_fObjPercentOfPriceBase = fObjPercentOfPriceBase; // Procent av prisbasbelopp #4424 #4420 20150804 J�
	}

	CTransaction_elv_object(const CTransaction_elv_object &c)
	{
		*this = c;
	}

	BOOL operator ==(CTransaction_elv_object &c) const
	{
		return (m_nObjID_pk == c.m_nObjID_pk &&
						m_sObjErrandNum == c.m_sObjErrandNum &&
						m_sObjectName == c.m_sObjectName &&
						m_sObjIDNumber == c.m_sObjIDNumber &&
						m_sObjLittra == c.m_sObjLittra &&
						m_sObjGrowthArea == c.m_sObjGrowthArea &&
						m_fObjTransportDist == c.m_fObjTransportDist &&
						m_nObjLatitude == c.m_nObjLatitude &&
						m_nObjAltitude == c.m_nObjAltitude &&
						m_sObjTypeOfInfring == c.m_sObjTypeOfInfring &&
						m_fObjPresentWidth1 == c.m_fObjPresentWidth1 &&
						m_fObjPresentWidth2 == c.m_fObjPresentWidth2 &&
						m_fObjAddedWidth1 == c.m_fObjAddedWidth1 &&
						m_fObjAddedWidth2 == c.m_fObjAddedWidth2 &&
						m_fObjParallelWidth == c.m_fObjParallelWidth &&
						m_nObjTypeOfNet == c.m_nObjTypeOfNet &&
						m_fObjPriceDiv1 == c.m_fObjPriceDiv1 &&
						m_fObjPriceDiv2 == c.m_fObjPriceDiv2 &&
						m_fObjPriceBase == c.m_fObjPriceBase &&
						m_fObjMaxPercent == c.m_fObjMaxPercent &&
						m_fObjPercentOfPriceBase == c.m_fObjPercentOfPriceBase && /* Procent av prisbasbelopp #4424 #4420 20150804 J� */
						m_fObjPercent == c.m_fObjPercent &&
						m_fObjVAT == c.m_fObjVAT &&
						m_nObjUseNormID == c.m_nObjUseNormID &&
						m_sObjUseNorm == c.m_sObjUseNorm &&
						m_fObjTakeCareOfPerc == c.m_fObjTakeCareOfPerc &&
						m_fObjCorrFactor == c.m_fObjCorrFactor &&
						m_sObjNameOfPricelist == c.m_sObjNameOfPricelist &&
						m_nObjTypeOfPricelist == c.m_nObjTypeOfPricelist &&
						m_sObjPricelistXML == c.m_sObjPricelistXML &&
						m_sObjNameOfCosts == c.m_sObjNameOfCosts &&
						m_nObjTypeOfCosts == c.m_nObjTypeOfCosts &&
						m_sObjCostsXML == c.m_sObjCostsXML &&
						m_nObjContractorID == c.m_nObjContractorID &&
						m_fObjDCLS == c.m_fObjDCLS &&
						m_sObjExtraInfo == c.m_sObjExtraInfo &&
						m_sObjP30HCostNameP30 == c.m_sObjP30HCostNameP30 &&
						m_nObjP30HCostTypeOfP30 == c.m_nObjP30HCostTypeOfP30 &&
						m_sObjP30HCostXMLP30 == c.m_sObjP30HCostXMLP30 &&
						m_sObjP30HCostNameHC == c.m_sObjP30HCostNameHC &&
						m_nObjP30HCostTypeOfHC == c.m_nObjP30HCostTypeOfHC &&
						m_sObjP30HCostXMLHC == c.m_sObjP30HCostXMLHC &&
						m_nIsVATIncluded == c.m_nIsVATIncluded &&
						m_nIsVoluntaryDealIncluded == c.m_nIsVoluntaryDealIncluded &&
						m_nIsHigherCostsIncluded == c.m_nIsHigherCostsIncluded &&
						m_nIsOtherCompensIncluded == c.m_nIsOtherCompensIncluded &&
						m_nIsGrotIncluded == c.m_nIsGrotIncluded &&
						m_sObjCreatedBy == c.m_sObjCreatedBy &&
						m_sObjNotes == c.m_sObjNotes &&
						m_nObjReturnAddressID == c.m_nObjReturnAddressID &&
						m_nObjLogoID == c.m_nObjLogoID &&
						m_sObjDate == c.m_sObjDate &&
						m_fObjLength == c.m_fObjLength &&
						m_sObjStartDate == c.m_sObjStartDate &&
						m_fObjRecalcBy == c.m_fObjRecalcBy &&
						m_nStatus == c.m_nStatus);
	}

	int getObjID_pk(void)							{ return m_nObjID_pk; }
	CString getObjErrandNum(void)			{ return m_sObjErrandNum; }
	CString getObjectName(void)				{ return m_sObjectName; }
	CString getObjIDNumber(void)			{ return m_sObjIDNumber; }
	CString getObjLittra(void)				{ return m_sObjLittra; }
	CString getObjGrowthArea(void)		{ return m_sObjGrowthArea; }
	double getObjTransportDist(void)	{ return m_fObjTransportDist; }
	int getObjLatitude(void)					{ return m_nObjLatitude; }
	int getObjAltitude(void)					{ return m_nObjAltitude; }
	CString getObjTypeOfInfring(void)	{ return m_sObjTypeOfInfring; }
	void setObjTypeOfInfring(CString val)	{ m_sObjTypeOfInfring = val; }
	double getObjLength(void)					{ return m_fObjLength; }
	double getObjPresentWidth1(void)	{ return m_fObjPresentWidth1; }
	double getObjPresentWidth2(void)	{ return m_fObjPresentWidth2; }
	double getObjAddedWidth1(void)		{ return m_fObjAddedWidth1; }
	double getObjAddedWidth2(void)		{ return m_fObjAddedWidth2; }
	double getObjParallelWidth(void)	{ return m_fObjParallelWidth; }
	int getObjTypeOfNet(void)					{ return m_nObjTypeOfNet; }
	double getObjPriceDiv1(void)			{ return m_fObjPriceDiv1; }
	double getObjPriceDiv2(void)			{ return m_fObjPriceDiv2; }
	double getObjPriceBase(void)			{ return m_fObjPriceBase; }
	double getObjMaxPercent(void)			{ return m_fObjMaxPercent; }
	double getObjPercent(void)				{ return m_fObjPercent; }

	double getObjPercentOfPriceBase(void)	{ return m_fObjPercentOfPriceBase; } // Procent av prisbasbelopp #4424 #4420 20150804 J�

	double getObjVAT(void)						{ return m_fObjVAT; }
	int getObjUseNormID(void)					{ return m_nObjUseNormID; }
	CString getObjUseNorm(void)				{ return m_sObjUseNorm; }
	double getObjTakeCareOfPerc(void)	{ return m_fObjTakeCareOfPerc; }
	double getObjCorrFactor(void)			{ return m_fObjCorrFactor; }
	CString getObjNameOfPricelist(void)	{ return m_sObjNameOfPricelist; }
	int getObjTypeOfPricelist(void)		{ return m_nObjTypeOfPricelist; }
	CString getObjPricelistXML(void)	{ return m_sObjPricelistXML; }
	CString getObjNameOfCosts(void)		{ return m_sObjNameOfCosts; }
	int getObjTypeOfCosts(void)				{ return m_nObjTypeOfCosts; }
	CString getObjCostsXML(void)			{ return m_sObjCostsXML; }
	int getObjContractorID(void)			{ return m_nObjContractorID; }
	double getObjDCLS(void)						{ return m_fObjDCLS; }
	CString getObjExtraInfo(void)			{ return m_sObjExtraInfo; }
	
	CString getObjP30Name(void)				{ return m_sObjP30HCostNameP30; }
	int getObjP30TypeOf(void)					{ return m_nObjP30HCostTypeOfP30; }
	CString getObjP30XML(void)				{ return m_sObjP30HCostXMLP30; }
	CString getObjHCostName(void)			{ return m_sObjP30HCostNameHC; }
	int getObjHCostTypeOf(void)				{ return m_nObjP30HCostTypeOfHC; }
	CString getObjHCostXML(void)			{ return m_sObjP30HCostXMLHC; }

	int getObjIsVATIncluded(void)							{ return m_nIsVATIncluded; }
	int getObjIsVoluntaryDealIncluded(void)		{ return m_nIsVoluntaryDealIncluded; }
	int getObjIsHigherCostsIncluded(void)			{ return m_nIsHigherCostsIncluded; }
	int getObjIsOtherCompensIncluded(void)		{ return m_nIsOtherCompensIncluded; }
	int getObjIsGrotIncluded(void)						{ return m_nIsGrotIncluded; }

	CString getObjCreatedBy(void)			{ return m_sObjCreatedBy; }
	CString getObjNotes(void)					{ return m_sObjNotes; }
	int getObjReturnAddressID(void)		{ return m_nObjReturnAddressID; }
	int getObjLogoID(void)						{ return m_nObjLogoID; }
	CString getObjDate(void)					{ return m_sObjDate; }
	CString getObjStartDate(void)			{ return m_sObjStartDate; }
	double getObjRecalcBy(void)				{ return m_fObjRecalcBy; }
	int getObjStatus(void)				{return m_nStatus; }
};

typedef std::vector<CTransaction_elv_object> vecTransaction_elv_object;

class CTransaction_elv_p30
{
	//private:
	int m_nP30ID;
	int m_nObjID;
	int m_nSpcID;
	CString m_sSpcName;
	double m_fPrice;
	double m_fPriceRel;
public:
	CTransaction_elv_p30(void)
	{
		m_nP30ID = -1;
		m_nObjID = -1;
		m_nSpcID = -1;
		m_sSpcName = _T("");
		m_fPrice = 0.0;
		m_fPriceRel = 0.0;
	}
	CTransaction_elv_p30(int p30_id,int obj_id,int spcid,LPCTSTR spc_name,double price,double price_rel)
	{
		m_nP30ID = p30_id;
		m_nObjID = obj_id;
		m_nSpcID = spcid;
		m_sSpcName = (spc_name);
		m_fPrice = price;
		m_fPriceRel = price_rel;
	}
	CTransaction_elv_p30(const CTransaction_elv_p30 &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_p30 &c) const
	{
		return (m_nP30ID == c.m_nP30ID &&
						m_nObjID == c.m_nObjID &&
						m_nSpcID == c.m_nSpcID &&
						m_sSpcName == c.m_sSpcName &&
						m_fPrice == c.m_fPrice &&
						m_fPriceRel == c.m_fPriceRel);
	}


	int getP30ID_pk(void)						{ return m_nP30ID; }
	int getP30ObjID_pk(void)				{ return m_nObjID; }
	int getP30SpcID(void)						{ return m_nSpcID; }
	CString getP30SpecieName(void)	{ return m_sSpcName; }
	double getP30Value(void)				{ return m_fPrice; }
	double getP30PriceRel(void)			{ return m_fPriceRel; }
};

typedef std::vector<CTransaction_elv_p30> vecCTransaction_elv_p30;

// CTransaction_elv_hcost
class CTransaction_elv_hcost
{
	int m_nHCostID_pk;				// Primary key
	int m_nHCostObjectID_pk;	// Primary key
	double m_fHCostM3Sk;
	double m_fHCostBaseComp;
	double m_fHCostPrice;
	double m_fHCostSum;
public:
	CTransaction_elv_hcost(void)
	{
		m_nHCostID_pk	= -1;
		m_nHCostObjectID_pk = -1;
		m_fHCostM3Sk = 0.0;
		m_fHCostBaseComp = 0.0;
		m_fHCostPrice = 0.0;
		m_fHCostSum = 0.0;
	}

	CTransaction_elv_hcost(int hcost_id,int obj_id,double m3sk,double price,double base_comp,double sum_price)
	{
		m_nHCostID_pk	= hcost_id;
		m_nHCostObjectID_pk = obj_id;
		m_fHCostM3Sk = m3sk;
		m_fHCostBaseComp = base_comp;
		m_fHCostPrice = price;
		m_fHCostSum = sum_price;
	}

	CTransaction_elv_hcost(const CTransaction_elv_hcost &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_hcost &c) const
	{
		return (m_nHCostID_pk == c.m_nHCostID_pk &&
						m_nHCostObjectID_pk == c.m_nHCostObjectID_pk &&
						m_fHCostM3Sk == c.m_fHCostM3Sk &&
						m_fHCostBaseComp == c.m_fHCostBaseComp &&
						m_fHCostPrice == c.m_fHCostPrice &&
						m_fHCostSum == c.m_fHCostSum);
	}


	int getHCostID_pk(void)				{ return m_nHCostID_pk; }
	int getHCostObjID_pk(void)		{ return m_nHCostObjectID_pk; }
	double getHCostM3Sk(void)			{ return m_fHCostM3Sk; }
	double getHCostBaseComp(void)	{ return m_fHCostBaseComp; }
	double getHCostPrice(void)		{ return m_fHCostPrice; }
	double getHCostSUM(void)			{ return m_fHCostSum; }

};

typedef std::vector<CTransaction_elv_hcost> vecTransaction_elv_hcost;

// CTransaction_elv_properties
class CTransaction_elv_properties
{
	//private:
	int m_nPropID_pk;							// Primary key
	int m_nPropObjectID_pk;				// Primary key
	CString m_sProperyCounty;			// "L�n"
	CString m_sProperyMunicipal;	// "Kommun"
	CString m_sProperyName;				// "Namn p� fastigheten i 'fst_property_table'
	CString m_sProperyNumber;			// "Fastighetsnummer i 'fst_property_table'
	double m_fPropM3Sk;						// "Totala volymen m3sk f�r fastigheten"
	double m_fPropAreal;					// "Totala arealen f�r fastigheten"
	long m_nPropNumOfTrees;				// "Totala antalet tr�d p� fastigheten"
	int m_nPropNumOfStands;				// "Totala antalet best�nd"
	double m_fPropWoodVolume;			// "Gagnvirkesvolym m3fub"
	double m_fPropWoodValue;			// "Virkets v�rde"
	double m_fPropCostValue;			// "Kostnader"
	double m_fPropRotpost;				// "NETTO"
	double m_fPropLandValue;			// "Markv�rde"
	double m_fPropEarlyCutValue;	// "F�rtidig avverkning"
	double m_fPropStormDryValue;	// "Storm- och Torkskador"
	double m_fPropRandTressValue;	// "Kanttr�d"
	double m_fPropVoluntaryDealValue;		// "Frivillig uppg�relse"
	double m_fPropHighCutVolume;	// "F�rdyrad avverkning; volym"
	double m_fPropHighCutValue;		// "F�rdyrad avverkning; v�rde"
	double m_fPropOtherCompValue;	// "Annan ers�ttning"
	int m_nPropStatus;						// "Status"
	int m_nPropStatus2;						// "Status anv�nd provtr�d/anv�nd inte"
	CString m_sPropGroupID;				// "Gruppid, f�r att grupera fastigheter ex. mark�gare"
#ifdef VER120
	int m_nPropTypeOfAction;			// "S�tt 1 = F�rs�ljning, 0 = Egen regi"
	int m_nPropSortOrder;					// "S�tta sorteringsordning p� Fastigheter"
	double m_fPropGrotVolume;			// "Volym Grot (ton)"
	double m_fPropGrotValue;			// "V�rde Grot (kr)"
	double m_fPropGrotCost;				// "Kostnad Grot (kr)"
#endif
	CString m_sSearchID;					// Lagt till s�k id att ing� i lista �ver fastigheter; 2012-08-28 P�D
	CString m_sMottagarref;				// Mottagarreferens, lagt till fr�mst f�r Ansj� export #3348
	CString m_sPropNotifyDate;		// "Datum f�r Avisering"
	CString m_sPropCompOfferDate;	// "Datum f�r Ers�ttningserbjudande"
	CString m_sPropPayDate;				// "Datum f�r Utbetalning"
	CString m_sPropDate;					// "Datum �ndrad/skapad"
	CString m_sCoord;	//#3735 Koordinater
public:
	CTransaction_elv_properties(void)
	{
		m_nPropID_pk = -1;
		m_nPropObjectID_pk = -1;
		m_sProperyCounty = _T("");
		m_sProperyMunicipal = _T("");
		m_sProperyName = _T("");
		m_sProperyNumber = _T("");
		m_fPropM3Sk = 0.0;
		m_fPropAreal = 0.0;
		m_nPropNumOfTrees = -1;
		m_nPropNumOfStands = -1;
		m_fPropWoodVolume = 0.0;
		m_fPropWoodValue = 0.0;
		m_fPropCostValue = 0.0;
		m_fPropRotpost = 0.0;
		m_fPropLandValue = 0.0;
		m_fPropEarlyCutValue = 0.0;
		m_fPropStormDryValue = 0.0;
		m_fPropRandTressValue = 0.0;
		m_fPropVoluntaryDealValue = 0.0;
		m_fPropHighCutVolume = 0.0;
		m_fPropHighCutValue = 0.0;
		m_fPropOtherCompValue = 0.0;
		m_nPropStatus = 0;
		m_nPropStatus2 = 0;
		m_sPropGroupID = _T("");
#ifdef VER120
		m_nPropTypeOfAction = -1;
		m_nPropSortOrder = 1;
		m_fPropGrotVolume = 0.0;
		m_fPropGrotValue = 0.0;
		m_fPropGrotCost = 0.0;
#endif
		m_sSearchID = _T("");
		m_sMottagarref = _T("");
		m_sPropNotifyDate = _T("");
		m_sPropCompOfferDate = _T("");
		m_sPropPayDate = _T("");
		m_sPropDate = _T("");
		m_sCoord = _T("");
	}

	CTransaction_elv_properties(int prop_id,int prop_obj_id,
															LPCTSTR prop_county,LPCTSTR prop_municipal,LPCTSTR prop_name,LPCTSTR prop_num,
															double m3sk,double areal,long numof_trees,int numof_stands,
															double wood_volume,double wood_value,double cost_value,double netto,
															double land_value,double cut_value,double s_t_value,double rand_trees_value,
															double voluntary_deal,double high_cut_volume,double high_cut_value,double other_comp_value,
															int status,int status2,LPCTSTR group_id,
														#ifdef VER120
															int prop_type_of_action,
															int prop_sort_order,
															double grot_volume,
															double grot_value,
															double grot_cost,
														#endif
															LPCTSTR search_id,LPCTSTR mottagatrref,
															LPCTSTR notify_date = _T(""),LPCTSTR comp_offer_date = _T(""),LPCTSTR pay_date = _T(""),LPCTSTR date = _T(""),
															LPCTSTR sCoord = _T(""))
	{
		m_nPropID_pk = prop_id;
		m_nPropObjectID_pk = prop_obj_id;
		m_sProperyCounty = (prop_county);
		m_sProperyMunicipal = (prop_municipal);
		m_sProperyName = (prop_name);
		m_sProperyNumber = (prop_num);
		m_fPropM3Sk = m3sk;
		m_fPropAreal = areal;
		m_nPropNumOfTrees = numof_trees;
		m_nPropNumOfStands = numof_stands;
		m_fPropWoodVolume = wood_volume;
		m_fPropWoodValue = wood_value;
		m_fPropCostValue = cost_value;
		m_fPropRotpost = netto;
		m_fPropLandValue = land_value;
		m_fPropEarlyCutValue = cut_value;
		m_fPropStormDryValue = s_t_value;
		m_fPropRandTressValue = rand_trees_value;
		m_fPropVoluntaryDealValue = voluntary_deal;
		m_fPropHighCutVolume = high_cut_volume;
		m_fPropHighCutValue = high_cut_value;
		m_fPropOtherCompValue = other_comp_value;
		m_nPropStatus = status;
		m_nPropStatus2 = status2;
		m_sPropGroupID = (group_id);
#ifdef VER120
		m_nPropTypeOfAction = prop_type_of_action;
		m_nPropSortOrder = prop_sort_order;
		m_fPropGrotVolume = grot_volume;
		m_fPropGrotValue = grot_value;
		m_fPropGrotCost = grot_cost;
#endif
		m_sSearchID = search_id;
		m_sMottagarref = mottagatrref;
		m_sPropNotifyDate = (notify_date);
		m_sPropCompOfferDate = (comp_offer_date);
		m_sPropPayDate = (pay_date);
		m_sPropDate = (date);
		m_sCoord = sCoord;
	}

	CTransaction_elv_properties(const CTransaction_elv_properties &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_properties &c) const
	{
		return (m_nPropID_pk == c.m_nPropID_pk &&
						m_nPropObjectID_pk == c.m_nPropObjectID_pk &&
						m_sProperyCounty == c.m_sProperyCounty &&
						m_sProperyMunicipal == c.m_sProperyMunicipal &&
						m_sProperyName == c.m_sProperyName &&
						m_sProperyNumber == c.m_sProperyNumber &&
						m_fPropM3Sk == c.m_fPropM3Sk &&
						m_fPropAreal == c.m_fPropAreal &&
						m_nPropNumOfTrees == c.m_nPropNumOfTrees &&
						m_nPropNumOfStands == c.m_nPropNumOfStands &&
						m_fPropWoodVolume == c.m_fPropWoodVolume &&
						m_fPropWoodValue == c.m_fPropWoodValue &&
						m_fPropCostValue == c.m_fPropCostValue &&
						m_fPropRotpost == c.m_fPropRotpost &&
						m_fPropLandValue == c.m_fPropLandValue &&
						m_fPropEarlyCutValue == c.m_fPropEarlyCutValue &&
						m_fPropStormDryValue == c.m_fPropStormDryValue &&
						m_fPropRandTressValue == c.m_fPropRandTressValue &&
						m_fPropVoluntaryDealValue == c.m_fPropVoluntaryDealValue &&
						m_fPropHighCutVolume == c.m_fPropHighCutVolume &&
						m_fPropHighCutValue == c.m_fPropHighCutValue &&
						m_fPropOtherCompValue == c.m_fPropOtherCompValue &&
						m_nPropStatus == c.m_nPropStatus &&
						m_nPropStatus2 == c.m_nPropStatus2 &&
						m_sPropGroupID == c.m_sPropGroupID &&
				#ifdef VER120
						m_nPropTypeOfAction == c.m_nPropTypeOfAction && 
						m_nPropSortOrder == c.m_nPropSortOrder &&
						m_fPropGrotVolume == c.m_fPropGrotVolume &&
						m_fPropGrotValue == c.m_fPropGrotValue &&
						m_fPropGrotCost == c.m_fPropGrotCost &&
				#endif
						m_sSearchID == c.m_sSearchID && 
						m_sMottagarref == c.m_sMottagarref &&
						m_sPropNotifyDate == c.m_sPropNotifyDate &&
						m_sPropCompOfferDate == c.m_sPropCompOfferDate &&
						m_sPropPayDate == c.m_sPropPayDate &&
						m_sPropDate == c.m_sPropDate &&
						m_sCoord == c.m_sCoord);
	}


	int getPropID_pk(void)									{ return m_nPropID_pk; };
	int getPropObjectID_pk(void)						{ return m_nPropObjectID_pk; }
	CString getPropCounty(void)							{ return m_sProperyCounty; }
	CString getPropMunicipal(void)					{ return m_sProperyMunicipal; }
	CString getPropName(void)								{ return m_sProperyName; }
	CString getPropNumber(void)							{ return m_sProperyNumber; }
	double getPropM3Sk(void)								{ return m_fPropM3Sk; }
	double getPropAreal(void)								{ return m_fPropAreal; }
	long getPropNumOfTrees(void)						{ return m_nPropNumOfTrees; }
	int getPropNumOfStands(void)						{ return m_nPropNumOfStands; }
	double getPropWoodVolume(void)					{ return m_fPropWoodVolume; }
	double getPropWoodValue(void)						{ return m_fPropWoodValue; }
	double getPropCostValue(void)						{ return m_fPropCostValue; }
	double getPropRotpost(void)							{ return m_fPropRotpost; }
	double getPropLandValue(void)						{ return m_fPropLandValue; }
	double getPropEarlyCutValue(void)				{ return m_fPropEarlyCutValue; }
	double getPropStromDryValue(void)				{ return m_fPropStormDryValue; }
	double getPropRandTreesValue(void)			{ return m_fPropRandTressValue; }
	double getPropVoluntaryDealValue(void)	{ return 	m_fPropVoluntaryDealValue; }
	double getPropHighCutVolume(void)				{ return m_fPropHighCutVolume; }
	double getPropHighCutValue(void)				{ return m_fPropHighCutValue; }
	double getPropOtherCostValue(void)			{ return m_fPropOtherCompValue; }
	int getPropStatus(void)									{ return m_nPropStatus; }
	void setPropStatus(int v)								{ m_nPropStatus = v; }
	int getPropStatus2(void)								{ return m_nPropStatus2; }
	CString getPropGroupID(void)						{ return m_sPropGroupID; }
	void setPropGroupID(LPCTSTR group_id)		{ m_sPropGroupID = group_id; }
#ifdef VER120
	int getPropTypeOfAction(void)						{ return m_nPropTypeOfAction; }
	int getPropSortOrder(void)							{ return m_nPropSortOrder; }
	void setPropSortOrder(int v)						{ m_nPropSortOrder = v; }
	double getPropGrotVolume(void)					{ return m_fPropGrotVolume; }
	void setPropGrotVolume(double v)				{ m_fPropGrotVolume = v; }
	double getPropGrotValue(void)						{ return m_fPropGrotValue; }
	void setPropGrotValue(double v)					{ m_fPropGrotValue = v; }
	double getPropGrotCost(void)						{ return m_fPropGrotCost; }
	void setPropGrotCost(double v)					{ m_fPropGrotCost = v; }
#endif
	CString getSearchID(void)								{ return m_sSearchID; }
	CString getMottagarref(void)						{ return m_sMottagarref; }
	CString getPropNotifyDate(void)					{ return m_sPropNotifyDate; }
	CString getPropCompOfferDate(void)			{ return m_sPropCompOfferDate; }
	CString getPropPayDate(void)						{ return m_sPropPayDate; }
	CString getPropDate(void)								{ return m_sPropDate; }
	CString getCoord(void) { return m_sCoord; }

};

typedef std::vector<CTransaction_elv_properties> vecTransaction_elv_properties;


// CTransaction_elv_properties_logbook
class CTransaction_elv_properties_logbook
{
	//private:
	int m_nPropLogBookID_pk;				// Primary key
	int m_nPropLogBookObjectID_pk;	// Primary key
	CString m_sPropLogBookDate;			// Primary key
	CString m_sPropLogBookAddedBy;	// "Signatur"
	CString m_sPropLogBookNote;			// "Logboksanteckning'
	CString m_sPropDate;						// "Datum �ndrad/skapad"
	int m_nPropLogType;
public:
	CTransaction_elv_properties_logbook(void)
	{
		m_nPropLogBookID_pk = -1;
		m_nPropLogBookObjectID_pk = -1;
		m_sPropLogBookDate = _T("");
		m_sPropLogBookAddedBy = _T("");
		m_sPropLogBookNote = _T("");
		m_sPropDate = _T("");
		m_nPropLogType = -1;
	}

	CTransaction_elv_properties_logbook(int logbook_id,int logbook_obj_id,LPCTSTR logbook_date,
																			LPCTSTR logbook_added_by,LPCTSTR logbook_note,LPCTSTR /*date*/,int prop_log_type)
	{
		m_nPropLogBookID_pk = logbook_id;
		m_nPropLogBookObjectID_pk = logbook_obj_id;
		m_sPropLogBookDate = (logbook_date);
		m_sPropLogBookAddedBy = (logbook_added_by);
		m_sPropLogBookNote = (logbook_note);
		m_nPropLogType = prop_log_type;
	}

	CTransaction_elv_properties_logbook(const CTransaction_elv_properties_logbook &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_properties_logbook &c) const
	{
		return (m_nPropLogBookID_pk == c.m_nPropLogBookID_pk &&
						m_nPropLogBookObjectID_pk == c.m_nPropLogBookObjectID_pk &&
						m_sPropLogBookDate == c.m_sPropLogBookDate &&
						m_sPropLogBookAddedBy == c.m_sPropLogBookAddedBy &&
						m_sPropLogBookNote == c.m_sPropLogBookNote && 
						m_nPropLogType == c.m_nPropLogType);
	}


	int getPropLogBookID_pk()				{ return m_nPropLogBookID_pk; }
	int getPropLogBookObjectID_pk()	{ return m_nPropLogBookObjectID_pk; }
	CString getPropLogBookDate()		{ return m_sPropLogBookDate; }
	CString getPropLogBookAddedBy()	{ return m_sPropLogBookAddedBy; }
	CString getPropLogBookNote()		{ return m_sPropLogBookNote; }
	int getPropLogType()						{ return m_nPropLogType; }
};

typedef std::vector<CTransaction_elv_properties_logbook> vecTransaction_elv_properties_logbook;


// CTransaction_elv_evaluation; "V�rdebest�nd"
class CTransaction_eval_evaluation
{
	//private:
	int m_nEValID_pk;				// Primary key
	int m_nEValPropID_pk;		// Primary key
	int m_nEValObjID_pk;		// Primary key
	//-------------------------------------------------------------------
	// Misc. data
	CString m_sEValName;
	double m_nEValAreal;						// "Areal (ha)"
	int m_nEValLength;						// "L�ngd [m]" #4204 20150112 J�
	int m_nEValWidth;						// "Bredd [m]" #4204 20150112 J�
	int m_nEValLayer;						// "Skikt" #5086 20160819 PL
	int m_nEValSide;						// "Sida" #5171 20161005 PL
	int m_nEValAge;									// "�lder"
	CString m_sEValSIH100;					// "SI H100 v�de i dm"
	double m_nEValCorrFac;					// "Korrektionsfaktor"
	// "Tr�dslagsf�rdelning TGL (Tall,Gran,L�v)"
	double m_fEValPinePart;					// "Andel tall"
	double m_fEValSprucePart;				// "Andel gran"
	double m_fEValBirchPart;				// "Andel bj�rk(l�v)"
	// "Typ av skog; Skogsmark,Kalmark
	int m_nEValForrestType;					// "Typ av skog 0=Skogsmark 1=Kalmark"
	// "Typ av skog; Skogsmark,Kalmark
	int m_nEValType;								// "Typ av v�rdering 0=V�rdeing 1=NOLL-v�rdering"
	// Calculated data
	double m_fEValLandValue;				// "Markv�rde"
	double m_fEValPreCut;						// "F�rtidig avverkning"
	double m_fEValLandValue_ha;			// "Markv�rde/ha"
	double m_fEValPreCut_ha;				// "F�rtidig avverkning/ha"
	// Create date
	CString m_sDate;
	// 
	double m_fEValVolume;						// "Anv�nds f�r anv�ndardefinierad volym"
	bool m_bOverlappingLand;				// "�verlappande mark"
	bool m_bTillfUtnyttjande;						// Tillf�lligt utnyttjande


	//HMS-49 Info om f�rtidig avverkning 20191126 J�
	double m_fPreCutInfo_P30_Pine;
	double m_fPreCutInfo_P30_Spruce;
	double m_fPreCutInfo_P30_Birch;
	double m_fPreCutInfo_Andel_Pine;
	double m_fPreCutInfo_Andel_Spruce;
	double m_fPreCutInfo_Andel_Birch;
	double m_fPreCutInfo_CorrFact;
	double m_fPreCutInfo_Ers_Pine;
	double m_fPreCutInfo_Ers_Spruce;
	double m_fPreCutInfo_Ers_Birch;	
	double m_fPreCutInfo_ReducedBy;


	
	//HMS-48 Info om markv�rde 20200427 J�
	double m_fMarkInfo_ValuePine;
	double m_fMarkInfo_ValueSpruce;	
	double m_fMarkInfo_Andel_Pine;
	double m_fMarkInfo_Andel_Spruce;
	double m_fMarkInfo_P30_Pine;
	double m_fMarkInfo_P30_Spruce;
	double m_fMarkInfo_ReducedBy;

public:
	CTransaction_eval_evaluation(void)
	{
		m_nEValID_pk = -1;						
		m_nEValPropID_pk = -1;				
		m_nEValObjID_pk = -1;					
		m_sEValName = _T("");
		m_nEValAreal = 0.0;
		m_nEValLength=0;
		m_nEValWidth=0;
		m_nEValLayer=1;
		m_nEValSide=1;
		m_nEValAge = -1;			
		m_sEValSIH100 = _T("");
		m_nEValCorrFac = 0.0;
		m_fEValPinePart = 0.0;				
		m_fEValSprucePart = 0.0;			
		m_fEValBirchPart = 0.0;				

		m_nEValForrestType = 0;				
		m_nEValType = -1;				

		m_fEValLandValue = 0.0;				
		m_fEValPreCut = 0.0;					
		m_fEValLandValue_ha = 0.0;				
		m_fEValPreCut_ha = 0.0;					

		m_sDate = _T("");

		m_fEValVolume = 0.0;

		m_bOverlappingLand = false;
		m_bTillfUtnyttjande=false;

		m_fPreCutInfo_P30_Pine= 0.0;
		m_fPreCutInfo_P30_Spruce= 0.0;
		m_fPreCutInfo_P30_Birch= 0.0;
		m_fPreCutInfo_Andel_Pine= 0.0;
		m_fPreCutInfo_Andel_Spruce= 0.0;
		m_fPreCutInfo_Andel_Birch= 0.0;
		m_fPreCutInfo_CorrFact= 0.0;
		m_fPreCutInfo_Ers_Pine= 0.0;
		m_fPreCutInfo_Ers_Spruce= 0.0;
		m_fPreCutInfo_Ers_Birch= 0.0;	
		m_fPreCutInfo_ReducedBy=0.0;
		
		
		//HMS-48 Info om markv�rde 20200427 J�
		m_fMarkInfo_ValuePine=0.0;
		m_fMarkInfo_ValueSpruce=0.0;
		m_fMarkInfo_Andel_Pine=0.0;
		m_fMarkInfo_Andel_Spruce=0.0;
		m_fMarkInfo_P30_Pine=0.0;
		m_fMarkInfo_P30_Spruce=0.0;
		m_fMarkInfo_ReducedBy=0.0;
	}

	CTransaction_eval_evaluation(
		int eval_id
		,int obj_id
		,int prop_id
		,LPCTSTR name
		,double areal
		,int age
		,LPCTSTR si_h100
		,double corr_fac
		,double pine_part
		,double spruce_part
		,double birch_part
		,int forrest_type
		,int type_of
		,double land_value
		,double pre_cut
		,double land_value_ha
		,double pre_cut_ha
		,LPCTSTR date
		,double vol
		,int nEValLength
		,int nEValWidth
		,int nEValLayer
		,int nEValSide
		,bool bOverlappingLand
		,bool bTillfUtnyttjande
		,
		//HMS-49 Info om f�rtidig avverkning
		double fPreCutInfo_P30_Pine,
		double fPreCutInfo_P30_Spruce,
		double fPreCutInfo_P30_Birch,
		double fPreCutInfo_Andel_Pine,
		double fPreCutInfo_Andel_Spruce,
		double fPreCutInfo_Andel_Birch,
		double fPreCutInfo_CorrFact,
		double fPreCutInfo_Ers_Pine,
		double fPreCutInfo_Ers_Spruce,
		double fPreCutInfo_Ers_Birch,	
		double fPreCutInfo_ReducedBy,
		
		//HMS-48 Info om markv�rde 20200427 J�
		double fMarkInfo_ValuePine,
		double fMarkInfo_ValueSpruce,
		double fMarkInfo_Andel_Pine,
		double fMarkInfo_Andel_Spruce,
		double fMarkInfo_P30_Pine,
		double fMarkInfo_P30_Spruce,
		double fMarkInfo_ReducedBy
		)
	{
		m_nEValID_pk = eval_id;						
		m_nEValPropID_pk = prop_id;				
		m_nEValObjID_pk = obj_id;					
		m_sEValName = (name);
		m_nEValAreal = areal;
		m_nEValLength=nEValLength;
		m_nEValWidth=nEValWidth;
		m_nEValLayer=nEValLayer;
		m_nEValSide=nEValSide;
		m_nEValAge = age;			
		m_sEValSIH100 = (si_h100);
		m_nEValCorrFac = corr_fac;
		m_fEValPinePart = pine_part;				
		m_fEValSprucePart = spruce_part;			
		m_fEValBirchPart = birch_part;				
		m_nEValForrestType = forrest_type;				
		m_nEValType = type_of;				

		m_fEValLandValue = land_value;				
		m_fEValPreCut = pre_cut;					
		m_fEValLandValue_ha = land_value_ha;				
		m_fEValPreCut_ha = pre_cut_ha;					
		m_sDate = (date);

		m_fEValVolume = vol;

		m_bOverlappingLand = bOverlappingLand;
		m_bTillfUtnyttjande=bTillfUtnyttjande;

		//HMS-49 Info om f�rtidig avverkning
		m_fPreCutInfo_P30_Pine=fPreCutInfo_P30_Pine;
		m_fPreCutInfo_P30_Spruce=fPreCutInfo_P30_Spruce;
		m_fPreCutInfo_P30_Birch=fPreCutInfo_P30_Birch;
		m_fPreCutInfo_Andel_Pine=fPreCutInfo_Andel_Pine;
		m_fPreCutInfo_Andel_Spruce=fPreCutInfo_Andel_Spruce;
		m_fPreCutInfo_Andel_Birch=fPreCutInfo_Andel_Birch;
		m_fPreCutInfo_CorrFact=fPreCutInfo_CorrFact;
		m_fPreCutInfo_Ers_Pine=fPreCutInfo_Ers_Pine;
		m_fPreCutInfo_Ers_Spruce=fPreCutInfo_Ers_Spruce;
		m_fPreCutInfo_Ers_Birch=fPreCutInfo_Ers_Birch;	
		m_fPreCutInfo_ReducedBy=fPreCutInfo_ReducedBy;

		
		//HMS-48 Info om markv�rde 20200427 J�
		m_fMarkInfo_ValuePine=fMarkInfo_ValuePine;
		m_fMarkInfo_ValueSpruce=fMarkInfo_ValueSpruce;
		m_fMarkInfo_Andel_Pine=fMarkInfo_Andel_Pine;
		m_fMarkInfo_Andel_Spruce=fMarkInfo_Andel_Spruce;
		m_fMarkInfo_P30_Pine=fMarkInfo_P30_Pine;
		m_fMarkInfo_P30_Spruce=fMarkInfo_P30_Spruce;
		m_fMarkInfo_ReducedBy=fMarkInfo_ReducedBy;

	}

	CTransaction_eval_evaluation(const CTransaction_eval_evaluation &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_eval_evaluation &c) const
	{
		return (m_nEValID_pk == c.m_nEValID_pk &&
			m_nEValPropID_pk == c.m_nEValPropID_pk &&
			m_nEValObjID_pk == c.m_nEValObjID_pk &&
			m_sEValName == c.m_sEValName &&
			m_nEValAreal == c.m_nEValAreal &&
			m_nEValLength == c.m_nEValLength && 
			m_nEValWidth == c.m_nEValWidth && 
			m_nEValLayer == c.m_nEValLayer &&
			m_nEValSide == c.m_nEValSide &&
			m_nEValAge == c.m_nEValAge &&
			m_sEValSIH100 == c.m_sEValSIH100 &&
			m_nEValCorrFac == c.m_nEValCorrFac &&
			m_fEValPinePart == c.m_fEValPinePart &&
			m_fEValSprucePart == c.m_fEValSprucePart &&
			m_fEValBirchPart == c.m_fEValBirchPart &&
			m_nEValForrestType == c.m_nEValForrestType &&
			m_nEValType == c.m_nEValType &&
			m_fEValLandValue == c.m_fEValLandValue &&
			m_fEValPreCut == c.m_fEValPreCut &&
			m_fEValLandValue_ha == c.m_fEValLandValue_ha &&
			m_fEValPreCut_ha == c.m_fEValPreCut_ha &&
			m_fEValVolume == c.m_fEValVolume &&
			m_bOverlappingLand == c.m_bOverlappingLand &&
			m_bTillfUtnyttjande==c.m_bTillfUtnyttjande &&
			//HMS-49 Info om f�rtidig avverkning
			m_fPreCutInfo_P30_Pine==c.m_fPreCutInfo_P30_Pine &&
			m_fPreCutInfo_P30_Spruce==c.m_fPreCutInfo_P30_Spruce &&
			m_fPreCutInfo_P30_Birch==c.m_fPreCutInfo_P30_Birch &&
			m_fPreCutInfo_Andel_Pine==c.m_fPreCutInfo_Andel_Pine &&
			m_fPreCutInfo_Andel_Spruce==c.m_fPreCutInfo_Andel_Spruce &&
			m_fPreCutInfo_Andel_Birch==c.m_fPreCutInfo_Andel_Birch &&
			m_fPreCutInfo_CorrFact==c.m_fPreCutInfo_CorrFact &&
			m_fPreCutInfo_Ers_Pine==c.m_fPreCutInfo_Ers_Pine &&
			m_fPreCutInfo_Ers_Spruce==c.m_fPreCutInfo_Ers_Spruce &&
			m_fPreCutInfo_Ers_Birch==c.m_fPreCutInfo_Ers_Birch &&
			m_fPreCutInfo_ReducedBy==c.m_fPreCutInfo_ReducedBy &&
			//HMS-48 Info om markv�rde 20200427 J�
			m_fMarkInfo_ValuePine==c.m_fMarkInfo_ValuePine &&
			m_fMarkInfo_ValueSpruce==c.m_fMarkInfo_ValueSpruce &&
			m_fMarkInfo_Andel_Pine==c.m_fMarkInfo_Andel_Pine &&
			m_fMarkInfo_Andel_Spruce==c.m_fMarkInfo_Andel_Spruce &&
			m_fMarkInfo_P30_Pine==c.m_fMarkInfo_P30_Pine &&
			m_fMarkInfo_P30_Spruce==c.m_fMarkInfo_P30_Spruce &&
			m_fMarkInfo_ReducedBy==c.m_fMarkInfo_ReducedBy
			);
	}


	int getEValID_pk()								{ return m_nEValID_pk; }
	int getEValPropID_pk()						{ return m_nEValPropID_pk; }
	int getEValObjID_pk()							{ return m_nEValObjID_pk; }
	CString getEValName()							{ return m_sEValName; }
	double getEValAreal()							{ return m_nEValAreal; }

	int getEValLength() { return m_nEValLength; }
	int getEValWidth() { return m_nEValWidth; }
	int getEValLayer() { return m_nEValLayer; }
	int getEValSide() { return m_nEValSide; }

	int getEValAge()									{ return m_nEValAge; }
	CString getEValSIH100()						{ return m_sEValSIH100; }
	double getEValCorrFactor()				{ return m_nEValCorrFac; }
	void setEValCorrFactor(double v)	{ m_nEValCorrFac = v; }
	// "Tr�dslagsf�rdelning TGL (Tall,Gran,L�v)"
	double getEValPinePart()					{ return m_fEValPinePart; }
	void setEValPinePart(double v)		{ m_fEValPinePart = v; }
	double getEValSprucePart()				{ return m_fEValSprucePart; }
	void setEValSprucePart(double v)	{ m_fEValSprucePart = v; }
	double getEValBirchPart()					{ return m_fEValBirchPart; }
	void setEValBirchPart(double v)		{ m_fEValBirchPart = v; }
	// "Typ av skog; Skogsmark,Ungskog,Kalmark
	int getEValForrestType()					{ return m_nEValForrestType; }
	// "Typ av v�rdering; V�rdering,NOLL-v�rdering
	int getEValType()									{ return m_nEValType; }
	// Calcualted data
	double getEValLandValue()					{ return m_fEValLandValue; }
	void setEValLandValue(double v)		{ m_fEValLandValue = v; }
	double getEValPreCut()						{ return m_fEValPreCut; }
	void setEValPreCut(double v)			{ m_fEValPreCut = v; }

	double getEValLandValue_ha()				{ return m_fEValLandValue_ha; }
	void setEValLandValue_ha(double v)	{ m_fEValLandValue_ha = v; }
	double getEValPreCut_ha()						{ return m_fEValPreCut_ha; }
	void setEValPreCut_ha(double v)			{ m_fEValPreCut_ha = v; }
	// Create date
	CString getEValDate()								{ return m_sDate; }
	double getEValVolume()							{ return m_fEValVolume; }

	bool getEValOverlappingLand()					{ return m_bOverlappingLand; }
	void setEValOverlappingLand(bool v)				{ m_bOverlappingLand = v; }

	bool getEValTillfUtnyttjande()					{ return m_bTillfUtnyttjande; }
	void setEValTillfUtnyttjande(bool v)				{ m_bTillfUtnyttjande = v; }

	//#5029 PH 2016-07-06, Added some setters
	void setEvalId(int id) { m_nEValID_pk=id; }
	void setPropId(int id) { m_nEValPropID_pk=id; }
	void setObjectId(int id) { m_nEValObjID_pk=id; }
	void setName(CString name) { m_sEValName=name; }
	void setAreal(double areal) { m_nEValAreal=areal; }
	void setLength(double length) { m_nEValLength=length; }
	void setWidth(double width) { m_nEValWidth=width; }
	void setAge(int age) { m_nEValAge=age; }
	void setH100(CString h100) { m_sEValSIH100=h100; }
	void setVolume(double volume) { m_fEValVolume=volume; }
	void setForrestType(int forrestType) { m_nEValForrestType=forrestType; }
	void setEvalType(int evalType) { m_nEValType=evalType; }
	void setEvalLayer(int layer) { m_nEValLayer = layer; }

		//HMS-49 Info om f�rtidig avverkning
	double getPreCutInfo_P30_Pine()		{ return m_fPreCutInfo_P30_Pine;	}
	double getPreCutInfo_P30_Spruce()	{ return m_fPreCutInfo_P30_Spruce;	}
	double getPreCutInfo_P30_Birch()	{ return  m_fPreCutInfo_P30_Birch;	}
	double getPreCutInfo_Andel_Pine()	{ return  m_fPreCutInfo_Andel_Pine;	}
	double getPreCutInfo_Andel_Spruce()	{ return  m_fPreCutInfo_Andel_Spruce;	}
	double getPreCutInfo_Andel_Birch()	{ return  m_fPreCutInfo_Andel_Birch;	}
	double getPreCutInfo_CorrFact()		{ return  m_fPreCutInfo_CorrFact;	}
	double getPreCutInfo_Ers_Pine()		{ return  m_fPreCutInfo_Ers_Pine;	}
	double getPreCutInfo_Ers_Spruce()	{ return  m_fPreCutInfo_Ers_Spruce;	}
	double getPreCutInfo_Ers_Birch()	{ return  m_fPreCutInfo_Ers_Birch;	}		
	double getPreCutInfo_ReducedBy()	{return m_fPreCutInfo_ReducedBy;}

	
	//HMS-48 Info om markv�rde 20200427 J�
	double getMarkInfo_ValuePine() { return  m_fMarkInfo_ValuePine;}
	double getMarkInfo_ValueSpruce() { return m_fMarkInfo_ValueSpruce;}
	double getMarkInfo_Andel_Pine() { return  m_fMarkInfo_Andel_Pine;}
	double getMarkInfo_Andel_Spruce() { return  m_fMarkInfo_Andel_Spruce;}
	double getMarkInfo_P30_Pine() { return  m_fMarkInfo_P30_Pine;}
	double getMarkInfo_P30_Spruce() { return  m_fMarkInfo_P30_Spruce;}
	double getMarkInfo_ReducedBy() { return  m_fMarkInfo_ReducedBy;}

};

typedef std::vector<CTransaction_eval_evaluation> vecTransaction_eval_evaluation;


// CTransaction_elv_cruise_randtrees
class CTransaction_elv_cruise_randtrees
{
	//private:
	int m_nEValRandID_pk;					// Primary key
	int m_nEValRandObjectID_pk;		// Primary key
	int m_nEValRandPropID_pk;			// Primary key
	int m_nEValRandEvaluID_pk;		// Primary key
	int m_nEValRandSpcID;					// "ID-nummer tr�dslag"
	CString m_sEValRandSpcName;		// "Namn p� tr�dslag"
	double m_fEValRandVolume;			// "Totala volymen m3sk f�r kanttr�d/tr�dslag"
	double m_fEValRandValue;			// "Totala v�rdet f�r kanttr�d/tr�dslag"
	CString m_sEValRandDate;			// "Datum �ndrad/skapad"
public:
	CTransaction_elv_cruise_randtrees(void)
	{
		m_nEValRandID_pk = -1;
		m_nEValRandObjectID_pk = -1;
		m_nEValRandPropID_pk = -1;
		m_nEValRandEvaluID_pk = -1;
		m_nEValRandSpcID = -1;
		m_sEValRandSpcName = _T("");
		m_fEValRandVolume = 0.0;
		m_fEValRandValue = 0.0;
		m_sEValRandDate = _T("");
	}

	CTransaction_elv_cruise_randtrees(int randtree_id,int object_id,int prop_id,int evalu_id,
															int spc_id,LPCTSTR spc_name,
															double volume,double value,
															LPCTSTR date = _T(""))
	{
		m_nEValRandID_pk = randtree_id;
		m_nEValRandObjectID_pk = object_id;
		m_nEValRandPropID_pk = prop_id;
		m_nEValRandEvaluID_pk = evalu_id;
		m_nEValRandSpcID = spc_id;
		m_sEValRandSpcName = (spc_name);
		m_fEValRandVolume = volume;
		m_fEValRandValue = value;
		m_sEValRandDate = (date);
	}

	CTransaction_elv_cruise_randtrees(const CTransaction_elv_cruise_randtrees &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_cruise_randtrees &c) const
	{
		return (m_nEValRandID_pk == c.m_nEValRandID_pk &&
						m_nEValRandObjectID_pk == c.m_nEValRandObjectID_pk &&
						m_nEValRandPropID_pk == c.m_nEValRandPropID_pk &&
						m_nEValRandEvaluID_pk == c.m_nEValRandEvaluID_pk &&
						m_nEValRandSpcID == c.m_nEValRandSpcID &&
						m_sEValRandSpcName == c.m_sEValRandSpcName &&
						m_fEValRandVolume == c.m_fEValRandVolume &&
						m_fEValRandValue == c.m_fEValRandValue);
	}


	int getEValRandID_pk()				{ return m_nEValRandID_pk; }
	int getEValRandObjectID_pk()	{ return m_nEValRandObjectID_pk; }
	int getEValRandPropID_pk()		{ return m_nEValRandPropID_pk; }
	int getEValRandEvaluID_pk()		{ return m_nEValRandEvaluID_pk; }
	int getEValRandSpcID()				{ return m_nEValRandSpcID; }
	CString getEValRandSpcName()	{ return m_sEValRandSpcName; }
	double getEValRandVolume()		{ return m_fEValRandVolume; }
	double getEValRandValue()			{ return m_fEValRandValue; }
	CString getEValRandDate()			{ return m_sEValRandDate; }
};

typedef std::vector<CTransaction_elv_cruise_randtrees> vecTransaction_elv_cruise_randtrees;

// CTransaction_elv_cruise
class CTransaction_elv_cruise
{
	//private:
	long m_nECruID_pk;							// Primary key (Equals Trakt id)
	int m_nECruObjectID_pk;				// Primary key
	int m_nECruPropID_pk;					// Primary key
	CString m_sECruNumber;				// "Trakt-nummer"
	CString m_sECruName;					// "Namn p� Trakt"
	long m_lECruNumOfTrees;				// Number of trees in trakt
	double m_fECruAreal;					// "Areal (ha) f�r trakt"
	double m_fECruM3Sk;						// "Volym i m3sk f�r trakt"
	double m_fECruTimberVol;			// "Timmervolym"
	double m_fECruTimberValue;		// "Timmerv�rde (kr)"
	double m_fECruTimberCost;			// "Kostnad (kr)"
	double m_fECruNetto;					// "Rotnetto (kr)"
	double m_fECruStDryVol;				// "Volym storm och tork OBS! Vid Parts-breddning 'I gatan'+Kanttr�d"
	double m_fECruStDryValue;			// "V�rde storm och tork"
	double m_fECruStDrySpruceMix;	// "Graninblandning i Procent"
	double m_fECruAvgPriceFactor;	// "Medelprisfaktor, baserad p� P30-priser och Tr�dslagsf�rdelning"
	double m_fECruRandTreesVol;		// "Volym f�r Kanttr�d"
	double m_fECruRandTreesValue;	// "V�rde f�r Kanttr�d"
	int m_nECruUseSampleTrees;		// "Kontroll; anv�nd provtr�d i denna Trakt (Best�nd)"
	int m_nECruDoReduceRotpost;		// "Redusera Rotpostpris enligt Prisf�rdelning p� Objekt"
	CString m_sECruDate;					// "Datum �ndrad/skapad"
	int m_nECruType;							// "Typ av texering 0 = Taxerat  1 = Manuellt (volym etc)"; 090625 p�d
	double m_fECruWidth;					// "Bredd vid breddning, styr �vre bredd satt p� Objektet"; 090625 p�d
	BOOL m_bTillfUtnyttj;			//HMS-42 Lagt till tillf�lligt utnyttjande 190327 J�

		//HMS-50 Info om storm o tork 20200415 J�
	double m_fStormTorkInfo_SpruceMix;
	double m_fStormTorkInfo_SumM3Sk_inside;
	double m_fStormTorkInfo_AvgPriceFactor;
	double m_fStormTorkInfo_TakeCareOfPerc;
	double m_fStormTorkInfo_PineP30Price;
	double m_fStormTorkInfo_SpruceP30Price;
	double m_fStormTorkInfo_BirchP30Price;
	double m_fStormTorkInfo_CompensationLevel;
	double m_fStormTorkInfo_Andel_Pine;
	double m_fStormTorkInfo_Andel_Spruce;
	double m_fStormTorkInfo_Andel_Birch;
	double m_fStormTorkInfo_WideningFactor;

	double m_fMarkInfo_ValuePine;
	double m_fMarkInfo_ValueSpruce;
	double m_fMarkInfo_Andel_Pine;
	double m_fMarkInfo_Andel_Spruce;
	double m_fMarkInfo_P30_Pine;
	double m_fMarkInfo_P30_Spruce;
	double m_fMarkInfo_ReducedBy;


#ifdef VER120
	CString m_sECruSI;						// "St�ndortsindex; samma som p� en Trakt, f�r manuellt inl�sta taxeringar "
	CString m_sECruTGL;						// "Tr�dslagsblandning, manuellt inl�sta best�nd"
	double m_fECruGrotVolume;			// "Volym grot i ton"
	double m_fECruGrotValue;			// "V�rde grot i kr"
	double m_fECruGrotCost;				// "Kostnad grot i kr"
	int m_fECruUseForInfr;				// "Anv�nds/Anv�nds inte f�r ber�kning av Intr�ng; 1 = Ing�r EJ i Intr�ng, 0 = Ing�r i Intr�ng"
#endif


public:
	CTransaction_elv_cruise(void)
	{
		m_nECruID_pk = -1;
		m_nECruObjectID_pk = -1;
		m_nECruPropID_pk = -1;
		m_sECruNumber = _T("");
		m_sECruName = _T("");
		m_lECruNumOfTrees = 0;
		m_fECruAreal = 0.0;
		m_fECruM3Sk = 0.0;
		m_fECruTimberVol = 0.0;
		m_fECruTimberValue = 0.0;
		m_fECruTimberCost = 0.0;
		m_fECruNetto = 0.0;
		m_fECruStDryVol = 0.0;
		m_fECruStDryValue = 0.0;
		m_fECruStDrySpruceMix = 0.0;
		m_fECruAvgPriceFactor = 0.0;
		m_fECruRandTreesVol = 0.0;
		m_fECruRandTreesValue = 0.0;
		m_nECruUseSampleTrees = -1;
		m_nECruDoReduceRotpost = -1;
		m_nECruType = 0;
		m_fECruWidth = 0.0;
		m_bTillfUtnyttj=0;

		m_fStormTorkInfo_SpruceMix=0.0;
		m_fStormTorkInfo_SumM3Sk_inside=0.0;
		m_fStormTorkInfo_AvgPriceFactor=0.0;
		m_fStormTorkInfo_TakeCareOfPerc=0.0;
		m_fStormTorkInfo_PineP30Price=0.0;
		m_fStormTorkInfo_SpruceP30Price=0.0;
		m_fStormTorkInfo_BirchP30Price=0.0;
		m_fStormTorkInfo_CompensationLevel=0.0;
		m_fStormTorkInfo_Andel_Pine=0.0;
		m_fStormTorkInfo_Andel_Spruce=0.0;
		m_fStormTorkInfo_Andel_Birch=0.0;
		m_fStormTorkInfo_WideningFactor=0.0;
	
		//HMS-48 Info om markv�rde 20200427 J�
		m_fMarkInfo_ValuePine=0.0;
		m_fMarkInfo_ValueSpruce=0.0;
		m_fMarkInfo_Andel_Pine=0.0;
		m_fMarkInfo_Andel_Spruce=0.0;
		m_fMarkInfo_P30_Pine=0.0;
		m_fMarkInfo_P30_Spruce=0.0;
		m_fMarkInfo_ReducedBy=0.0;

#ifdef VER120
		m_sECruSI = _T("");
		m_sECruTGL = _T("");
		m_fECruGrotVolume = 0.0;
		m_fECruGrotValue = 0.0;
		m_fECruGrotCost = 0.0;
		m_fECruUseForInfr = -1;
#endif
		m_sECruDate = _T("");
	
	}

	CTransaction_elv_cruise(long id,int object_id,int prop_id,
													LPCTSTR number,LPCTSTR name,long numof_trees,
													double areal,double vol_m3sk,
													double timber_vol,double timber_value,double timber_cost,double netto,
													double storm_dry_vol,double storm_dry_value,double spruce_mix,double price_factor,
													double rand_trees_vol,double rand_trees_value,int use_sample_trees,int do_reduce_rotpost,
													int cruise_type,double cruise_width,
											#ifdef VER120
													LPCTSTR si,LPCTSTR ecru_tgl,double grot_volume,double grot_value,double grot_cost,
													int use_for_infr,
											#endif
													BOOL bTillfUtnyttj,
												double fStormTorkInfo_SpruceMix,
													double fStormTorkInfo_SumM3Sk_inside,
													double fStormTorkInfo_AvgPriceFactor,
													double fStormTorkInfo_TakeCareOfPerc,
													double fStormTorkInfo_PineP30Price,
													double fStormTorkInfo_SpruceP30Price,
													double fStormTorkInfo_BirchP30Price,
													double fStormTorkInfo_CompensationLevel,
													double fStormTorkInfo_Andel_Pine,
													double fStormTorkInfo_Andel_Spruce,
													double fStormTorkInfo_Andel_Birch,
													double fStormTorkInfo_WideningFactor,
													double fMarkInfo_ValuePine,
													double fMarkInfo_ValueSpruce,
													double fMarkInfo_Andel_Pine,
													double fMarkInfo_Andel_Spruce,
													double fMarkInfo_P30_Pine,
													double fMarkInfo_P30_Spruce,
													double fMarkInfo_ReducedBy,
													LPCTSTR date = _T("")
														)
	{
		m_nECruID_pk = id;
		m_nECruObjectID_pk = object_id;
		m_nECruPropID_pk = prop_id;
		m_sECruNumber = (number);
		m_sECruName = (name);
		m_lECruNumOfTrees = numof_trees;
		m_fECruAreal = areal;
		m_fECruM3Sk = vol_m3sk;
		m_fECruTimberVol = timber_vol;
		m_fECruTimberValue = timber_value;
		m_fECruTimberCost = timber_cost;
		m_fECruNetto = netto;
		m_fECruStDryVol = storm_dry_vol;
		m_fECruStDryValue = storm_dry_value;
		m_fECruStDrySpruceMix = spruce_mix;
		m_fECruAvgPriceFactor = price_factor;
		m_fECruRandTreesVol = rand_trees_vol;
		m_fECruRandTreesValue = rand_trees_value;
		m_nECruUseSampleTrees = use_sample_trees;
		m_nECruDoReduceRotpost = do_reduce_rotpost;
		m_nECruType = cruise_type;
		m_fECruWidth = cruise_width;
		m_sECruSI = si;
		m_sECruTGL = ecru_tgl,
		m_fECruGrotVolume = grot_volume;
		m_fECruGrotValue = grot_value;
		m_fECruGrotCost = grot_cost;
		m_fECruUseForInfr = use_for_infr;
		m_sECruDate = date;
		m_bTillfUtnyttj=bTillfUtnyttj;

		m_fStormTorkInfo_SpruceMix=fStormTorkInfo_SpruceMix;
		m_fStormTorkInfo_SumM3Sk_inside=fStormTorkInfo_SumM3Sk_inside;
		m_fStormTorkInfo_AvgPriceFactor=fStormTorkInfo_AvgPriceFactor;
		m_fStormTorkInfo_TakeCareOfPerc=fStormTorkInfo_TakeCareOfPerc;
		m_fStormTorkInfo_PineP30Price=fStormTorkInfo_PineP30Price;
		m_fStormTorkInfo_SpruceP30Price=fStormTorkInfo_SpruceP30Price;
		m_fStormTorkInfo_BirchP30Price=fStormTorkInfo_BirchP30Price;
		m_fStormTorkInfo_CompensationLevel=fStormTorkInfo_CompensationLevel;
		m_fStormTorkInfo_Andel_Pine=fStormTorkInfo_Andel_Pine;
		m_fStormTorkInfo_Andel_Spruce=fStormTorkInfo_Andel_Spruce;
		m_fStormTorkInfo_Andel_Birch=fStormTorkInfo_Andel_Birch;
		m_fStormTorkInfo_WideningFactor=fStormTorkInfo_WideningFactor;

		//HMS-48 Info om markv�rde 20200427 J�
		m_fMarkInfo_ValuePine=fMarkInfo_ValuePine;
		m_fMarkInfo_ValueSpruce=fMarkInfo_ValueSpruce;
		m_fMarkInfo_Andel_Pine=fMarkInfo_Andel_Pine;
		m_fMarkInfo_Andel_Spruce=fMarkInfo_Andel_Spruce;
		m_fMarkInfo_P30_Pine=fMarkInfo_P30_Pine;
		m_fMarkInfo_P30_Spruce=fMarkInfo_P30_Spruce;
		m_fMarkInfo_ReducedBy=fMarkInfo_ReducedBy;
	}

	CTransaction_elv_cruise(const CTransaction_elv_cruise &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_cruise &c) const
	{
		return (m_nECruID_pk == c.m_nECruID_pk &&
						m_nECruObjectID_pk == c.m_nECruObjectID_pk &&
						m_nECruPropID_pk == c.m_nECruPropID_pk &&
						m_sECruNumber == c.m_sECruNumber &&
						m_sECruName == c.m_sECruName &&
						m_lECruNumOfTrees == c.m_lECruNumOfTrees &&
						m_fECruAreal == c.m_fECruAreal &&
						m_fECruM3Sk == c.m_fECruM3Sk &&
						m_fECruTimberVol == c.m_fECruTimberVol &&
						m_fECruTimberValue == c.m_fECruTimberValue &&
						m_fECruTimberCost == c.m_fECruTimberCost &&
						m_fECruNetto == c.m_fECruNetto &&
						m_fECruStDryVol == c.m_fECruStDryVol &&
						m_fECruStDryValue == c.m_fECruStDryValue &&
						m_fECruStDrySpruceMix == c.m_fECruStDrySpruceMix &&
						m_fECruAvgPriceFactor == c.m_fECruAvgPriceFactor &&
						m_fECruRandTreesVol == c.m_fECruRandTreesVol &&
						m_fECruRandTreesValue == c.m_fECruRandTreesValue &&
						m_nECruUseSampleTrees == c.m_nECruUseSampleTrees &&
						m_nECruDoReduceRotpost == c.m_nECruDoReduceRotpost &&
						m_nECruType == c.m_nECruType &&
						m_bTillfUtnyttj == c.m_bTillfUtnyttj &&

						m_fStormTorkInfo_SpruceMix==c.m_fStormTorkInfo_SpruceMix &&
						m_fStormTorkInfo_SumM3Sk_inside==c.m_fStormTorkInfo_SumM3Sk_inside &&
						m_fStormTorkInfo_AvgPriceFactor==c.m_fStormTorkInfo_AvgPriceFactor &&
						m_fStormTorkInfo_TakeCareOfPerc==c.m_fStormTorkInfo_TakeCareOfPerc &&
						m_fStormTorkInfo_PineP30Price==c.m_fStormTorkInfo_PineP30Price &&
						m_fStormTorkInfo_SpruceP30Price==c.m_fStormTorkInfo_SpruceP30Price &&
						m_fStormTorkInfo_BirchP30Price==c.m_fStormTorkInfo_BirchP30Price &&
						m_fStormTorkInfo_CompensationLevel==c.m_fStormTorkInfo_CompensationLevel &&
						m_fStormTorkInfo_Andel_Pine==c.m_fStormTorkInfo_Andel_Pine &&
						m_fStormTorkInfo_Andel_Spruce==c.m_fStormTorkInfo_Andel_Spruce &&
						m_fStormTorkInfo_Andel_Birch==c.m_fStormTorkInfo_Andel_Birch &&
						m_fStormTorkInfo_WideningFactor==c.m_fStormTorkInfo_WideningFactor &&

						m_fMarkInfo_ValuePine==c.m_fMarkInfo_ValuePine &&
						m_fMarkInfo_ValueSpruce==c.m_fMarkInfo_ValueSpruce &&
						m_fMarkInfo_Andel_Pine==c.m_fMarkInfo_Andel_Pine &&
						m_fMarkInfo_Andel_Spruce==c.m_fMarkInfo_Andel_Spruce &&
						m_fMarkInfo_P30_Pine==c.m_fMarkInfo_P30_Pine &&
						m_fMarkInfo_P30_Spruce==c.m_fMarkInfo_P30_Spruce &&
						m_fMarkInfo_ReducedBy==c.m_fMarkInfo_ReducedBy &&

#ifdef VER120
						m_fECruWidth == c.m_fECruWidth &&
						m_sECruSI == c.m_sECruSI &&
						m_sECruTGL == c.m_sECruTGL &&
						m_fECruGrotVolume == c.m_fECruGrotVolume &&
						m_fECruGrotValue == c.m_fECruGrotValue &&
						m_fECruGrotCost == c.m_fECruGrotCost &&
						m_fECruUseForInfr == c.m_fECruUseForInfr);
#else
						m_fECruWidth == c.m_fECruWidth);
#endif
	}

	long getECruID_pk()								{ return m_nECruID_pk; }
	int getECruObjectID_pk()					{ return m_nECruObjectID_pk; }
	int getECruPropID_pk()						{ return m_nECruPropID_pk; }
	CString getECruNumber()						{ return m_sECruNumber; }
	CString getECruName()							{ return m_sECruName; }
	long getECruNumOfTrees()					{ return m_lECruNumOfTrees; }
	double getECruAreal()							{ return m_fECruAreal; }
	double getECruM3Sk()							{ return m_fECruM3Sk; }
	double getECruTimberVol()					{ return m_fECruTimberVol; }
	double getECruTimberValue()				{ return m_fECruTimberValue; }
	double getECruTimberCost()				{ return m_fECruTimberCost; }
	double getECruNetto()							{ return m_fECruNetto; }
	double getECruStormDryVol()				{ return m_fECruStDryVol; }
	void setECruStormDryVol(double v)	{ m_fECruStDryVol = v; }
	double getECruStormDryValue()			{ return m_fECruStDryValue; }
	void setECruStormDryValue(double v)	{ m_fECruStDryValue = v; }
	double gteECruStormDrySpruceMix()	{	return m_fECruStDrySpruceMix; }
	void setECruStormDrySpruceMix(double v)	{	m_fECruStDrySpruceMix = v; }
	double getECruAvgPriceFactor()		{ return m_fECruAvgPriceFactor; }
	double getECruRandTreesVol()			{ return m_fECruRandTreesVol; }
	double getECruRandTreesValue()		{ return m_fECruRandTreesValue; }
	int getECruUseSampleTrees()				{ return m_nECruUseSampleTrees; }
	int getECruDoReduceRotpost()			{ return m_nECruDoReduceRotpost; }
	int getECruType()									{ return m_nECruType; }
	double getECruWidth()							{ return m_fECruWidth; }
	BOOL getECruTillfUtnyttj()						{ return m_bTillfUtnyttj;}
	void setECruTillfUtnyttj(BOOL bTillfUtnyttj)						{m_bTillfUtnyttj=bTillfUtnyttj;}
#ifdef VER120
	CString getECruSI()								{ return m_sECruSI; }
	CString getECruTGL()							{ return m_sECruTGL; }
	double getECruGrotVolume()				{ return m_fECruGrotVolume; }
	double getECruGrotValue()					{ return m_fECruGrotValue; }
	double getECruGrotCost()					{ return m_fECruGrotCost; }
	int getECruUseForInfr()						{ return m_fECruUseForInfr; }
#endif
	CString getECruDate()							{ return m_sECruDate; }

	//HMS-50 Info om storm o tork 20200415 J�
	double getStormTorkInfo_SpruceMix()			{return m_fStormTorkInfo_SpruceMix;}
	double getStormTorkInfo_SumM3Sk_inside()		{return m_fStormTorkInfo_SumM3Sk_inside;}
	double getStormTorkInfo_AvgPriceFactor()		{return m_fStormTorkInfo_AvgPriceFactor;}
	double getStormTorkInfo_TakeCareOfPerc()		{return m_fStormTorkInfo_TakeCareOfPerc;}
	double getStormTorkInfo_PineP30Price()			{return m_fStormTorkInfo_PineP30Price;}
	double getStormTorkInfo_SpruceP30Price()		{return m_fStormTorkInfo_SpruceP30Price;}
	double getStormTorkInfo_BirchP30Price()			{return m_fStormTorkInfo_BirchP30Price;}
	double getStormTorkInfo_CompensationLevel()		{return m_fStormTorkInfo_CompensationLevel;}
	double getStormTorkInfo_Andel_Pine()		{return m_fStormTorkInfo_Andel_Pine;}
	double getStormTorkInfo_Andel_Spruce()		{return m_fStormTorkInfo_Andel_Spruce;}
	double getStormTorkInfo_Andel_Birch()		{return m_fStormTorkInfo_Andel_Birch;}
	double getStormTorkInfo_WideningFactor()		{return m_fStormTorkInfo_WideningFactor;}

	void setStormTorkInfo_SpruceMix(double v)			{m_fStormTorkInfo_SpruceMix=v;}
	void setStormTorkInfo_SumM3Sk_inside(double v)		{m_fStormTorkInfo_SumM3Sk_inside=v;}
	void setStormTorkInfo_AvgPriceFactor(double v)		{m_fStormTorkInfo_AvgPriceFactor=v;}
	void setStormTorkInfo_TakeCareOfPerc(double v)		{m_fStormTorkInfo_TakeCareOfPerc=v;}
	void setStormTorkInfo_PineP30Price(double v)			{m_fStormTorkInfo_PineP30Price=v;}
	void setStormTorkInfo_SpruceP30Price(double v)		{m_fStormTorkInfo_SpruceP30Price=v;}
	void setStormTorkInfo_BirchP30Price(double v)			{m_fStormTorkInfo_BirchP30Price=v;}
	void setStormTorkInfo_CompensationLevel(double v)		{m_fStormTorkInfo_CompensationLevel=v;}
	void setStormTorkInfo_Andel_Pine(double v)		{m_fStormTorkInfo_Andel_Pine=v;}
	void setStormTorkInfo_Andel_Spruce(double v)		{m_fStormTorkInfo_Andel_Spruce=v;}
	void setStormTorkInfo_Andel_Birch(double v)		{m_fStormTorkInfo_Andel_Birch=v;}
	void setStormTorkInfo_WideningFactor(double v)		{m_fStormTorkInfo_WideningFactor=v;}


	//HMS-48 Info om markv�rde 20200427 J�
	double getMarkInfo_ValuePine()		{return m_fMarkInfo_ValuePine;}
	double getMarkInfo_ValueSpruce()		{return m_fMarkInfo_ValueSpruce;}
	double getMarkInfo_Andel_Pine()		{return m_fMarkInfo_Andel_Pine;}
	double getMarkInfo_Andel_Spruce()		{return m_fMarkInfo_Andel_Spruce;}
	double getMarkInfo_P30_Pine()		{return m_fMarkInfo_P30_Pine;}
	double getMarkInfo_P30_Spruce()		{return m_fMarkInfo_P30_Spruce;}
	double getMarkInfo_ReducedBy()		{return  m_fMarkInfo_ReducedBy;}
};

typedef std::vector<CTransaction_elv_cruise> vecTransaction_elv_cruise;

// Class for sum. data from trakt(s), based on Object and the Property in a trakt
// connected to the Property in Object; 080430 p�d
class CTransaction_elv_sumdata
{
	// Primary keys
	int m_nSumEcruID_pk;
	int m_nSumEcruObjectID_pk;
	int m_nSumEcruPropID_pk;
	// SUM Information on Cruised data
	long m_lSumEcruNumOfTrees;
	double m_fSumEcruAreal;
	double m_fSumEcruM3Sk;
	double m_fSumEcruTimberValue;
	double m_fSumEcruTimberCost;
	double m_fSumEcruRotNetto;
	// SUM Information on "Skogsnorm" calculated data
	double m_fSumEcruStormDryM3sk;
	double m_fSumEcruStormDryValue;
	double m_fSumEcruRandTreesM3sk;
	double m_fSumEcruRandTreesValue;
	// DATE
	CString m_sSumEcruDate;
public:
	CTransaction_elv_sumdata(void)
	{
		m_nSumEcruID_pk = -1;
		m_nSumEcruObjectID_pk = -1;
		m_nSumEcruPropID_pk = -1;
		// SUM Information on Cruised data
		m_lSumEcruNumOfTrees = -1;
		m_fSumEcruAreal = 0.0;
		m_fSumEcruM3Sk = 0.0;
		m_fSumEcruTimberValue = 0.0;
		m_fSumEcruTimberCost = 0.0;
		m_fSumEcruRotNetto = 0.0;
		// SUM Information on "Skogsnorm" calculated data
		m_fSumEcruStormDryM3sk = 0.0;
		m_fSumEcruStormDryValue = 0.0;
		m_fSumEcruRandTreesM3sk = 0.0;
		m_fSumEcruRandTreesValue = 0.0;
		// DATE
		m_sSumEcruDate = _T("");
	}

	CTransaction_elv_sumdata(int ecru_id,int obj_id,int prop_id,
													 long num_of_trees,double areal,double m3sk,
													 double timber_value,double timber_cost,double rot_netto,
													 double storm_dry_m3sk,double storm_dry_value,
													 double rand_trees_m3sk,double rand_trees_value,
													 LPCTSTR created = _T(""))
	{
		m_nSumEcruID_pk = ecru_id;
		m_nSumEcruObjectID_pk = obj_id;
		m_nSumEcruPropID_pk = prop_id;
		// SUM Information on Cruised data
		m_lSumEcruNumOfTrees = num_of_trees;
		m_fSumEcruAreal = areal;
		m_fSumEcruM3Sk = m3sk;
		m_fSumEcruTimberValue = timber_value;
		m_fSumEcruTimberCost = timber_cost;
		m_fSumEcruRotNetto = rot_netto;
		// SUM Information on "Skogsnorm" calculated data
		m_fSumEcruStormDryM3sk = storm_dry_m3sk;
		m_fSumEcruStormDryValue = storm_dry_value;
		m_fSumEcruRandTreesM3sk = rand_trees_m3sk;
		m_fSumEcruRandTreesValue = rand_trees_value;
		// DATE
		m_sSumEcruDate = (created);
	}
	// Add data on NumOf trees,Areal and volume (m3sk)
	CTransaction_elv_sumdata(long num_of_trees,double areal,double m3sk)
	{
		// SUM Information on Cruised data
		m_lSumEcruNumOfTrees = num_of_trees;
		m_fSumEcruAreal = areal;
		m_fSumEcruM3Sk = m3sk;
	}
	// Add data on timber,rotpost
	CTransaction_elv_sumdata(double timber_value,double timber_cost,double rot_netto)
	{
		m_fSumEcruTimberValue = timber_value;
		m_fSumEcruTimberCost = timber_cost;
		m_fSumEcruRotNetto = rot_netto;
	}
	// Add data on Storm/Dry and RandTrees
	CTransaction_elv_sumdata(double storm_dry_m3sk,double storm_dry_value,
													 double rand_trees_m3sk,double rand_trees_value)
	{
		// SUM Information on "Skogsnorm" calculated data
		m_fSumEcruStormDryM3sk = storm_dry_m3sk;
		m_fSumEcruStormDryValue = storm_dry_value;
		m_fSumEcruRandTreesM3sk = rand_trees_m3sk;
		m_fSumEcruRandTreesValue = rand_trees_value;
	}


	CTransaction_elv_sumdata(const CTransaction_elv_sumdata& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_sumdata &c) const
	{
		return (m_nSumEcruID_pk == c.m_nSumEcruID_pk &&
						m_nSumEcruObjectID_pk == c.m_nSumEcruObjectID_pk &&
						m_nSumEcruPropID_pk == c.m_nSumEcruPropID_pk &&
						m_lSumEcruNumOfTrees == c.m_lSumEcruNumOfTrees &&
						m_fSumEcruAreal == c.m_fSumEcruAreal &&
						m_fSumEcruM3Sk == c.m_fSumEcruM3Sk &&
						m_fSumEcruTimberValue == c.m_fSumEcruTimberValue &&
						m_fSumEcruTimberCost == c.m_fSumEcruTimberCost &&
						m_fSumEcruRotNetto == c.m_fSumEcruRotNetto &&
						m_fSumEcruStormDryM3sk == c.m_fSumEcruStormDryM3sk &&
						m_fSumEcruStormDryValue == c.m_fSumEcruStormDryValue &&
						m_fSumEcruRandTreesM3sk == c.m_fSumEcruRandTreesM3sk &&
						m_fSumEcruRandTreesValue == c.m_fSumEcruRandTreesValue);
	}


	int getSumEcruID_pk()									{ return m_nSumEcruID_pk; }
	int getSumEcruObjID_pk()							{ return m_nSumEcruObjectID_pk; }
	int getSumEcruPropID_pk()							{ return m_nSumEcruPropID_pk;	}
		// SUM Information on Cruised data
	long getSumEcruNumOfTrees()						{ return m_lSumEcruNumOfTrees; }
	double getSumEcruAreal()							{ return m_fSumEcruAreal; }
	double getSumEcruM3Sk()								{ return m_fSumEcruM3Sk; }
	double getSumEcruTimberValue()				{ return m_fSumEcruTimberValue; }
	double getSumEcruTimberCost()					{ return m_fSumEcruTimberCost; }
	double getSumEcruRotNetto()						{ return m_fSumEcruRotNetto; }
		// SUM Information on "Skogsnorm" calculated data
	double getSumEcruStormDryM3Sk()				{ return m_fSumEcruStormDryM3sk; }
	double getSumEcruStormDryValue()			{ return m_fSumEcruStormDryValue; }
	double getSumEcruRandTreesM3Sk()			{ return m_fSumEcruRandTreesM3sk; }
	double getSumEcruRandTreesValue()			{ return m_fSumEcruRandTreesValue; }
		// DATE
	CString getSumEcruDate()							{ return m_sSumEcruDate; }

};

typedef std::vector<CTransaction_elv_sumdata> vecCTransaction_elv_sumdata;

// CTransaction_elv_properties_other_comp
class CTransaction_elv_properties_other_comp
{
	//private:
	int m_nPropOtherCompID_pk;						// Primary key
	int m_nPropOtherCompObjectID_pk;			// Primary key
	int m_nPropOtherCompPropID_pk;				// Primary key
	double m_fPropOtherCompValue_entered;	// "Ers�ttning inl�st kr"
	double m_fPropOtherCompValue_calc;		// "Ers�ttning utr�knad kr"
	CString m_sPropOtherCompNote;					// "Notering om ers�ttning"
	int m_nPropOtherCompTypeOf;						// "Typ av ers�ttning; Fast est�ttning,Intr�ngsers�ttning"
	CString m_sPropOtherCompDate;					// "Datum �ndrad/skapad"
public:
	CTransaction_elv_properties_other_comp(void)
	{
		m_nPropOtherCompID_pk = -1;
		m_nPropOtherCompObjectID_pk = -1;
		m_nPropOtherCompPropID_pk = -1;
		m_fPropOtherCompValue_entered = 0.0;
		m_fPropOtherCompValue_calc = 0.0;
		m_sPropOtherCompNote = _T("");
		m_nPropOtherCompTypeOf = -1;
		m_sPropOtherCompDate = _T("");
	}

	CTransaction_elv_properties_other_comp(int ocomp_id,int ocomp_obj_id,int ocomp_prop_id,
																				 double ocomp_value_entered,double ocomp_value_calc,
																				 LPCTSTR ocomp_note,int ocomp_typeof,LPCTSTR date = _T(""))
	{
		m_nPropOtherCompID_pk = ocomp_id;
		m_nPropOtherCompObjectID_pk = ocomp_obj_id;
		m_nPropOtherCompPropID_pk = ocomp_prop_id;
		m_fPropOtherCompValue_entered = ocomp_value_entered;
		m_fPropOtherCompValue_calc = ocomp_value_calc;
		m_sPropOtherCompNote = (ocomp_note);
		m_nPropOtherCompTypeOf = ocomp_typeof;
		m_sPropOtherCompDate = (date);
	}

	CTransaction_elv_properties_other_comp(const CTransaction_elv_properties_other_comp &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_properties_other_comp &c) const
	{
		return (m_nPropOtherCompID_pk == c.m_nPropOtherCompID_pk &&
						m_nPropOtherCompObjectID_pk == c.m_nPropOtherCompObjectID_pk &&
						m_nPropOtherCompPropID_pk == c.m_nPropOtherCompPropID_pk &&
						m_fPropOtherCompValue_entered == c.m_fPropOtherCompValue_entered &&
						m_fPropOtherCompValue_calc == c.m_fPropOtherCompValue_calc &&
						m_sPropOtherCompNote == c.m_sPropOtherCompNote &&
						m_nPropOtherCompTypeOf == c.m_nPropOtherCompTypeOf);
	}


	int getPropOtherCompID_pk()								{ return m_nPropOtherCompID_pk; }
	int getPropOtherCompObjectID_pk()					{ return m_nPropOtherCompObjectID_pk; }
	int getPropOtherCompPropID_pk()						{ return m_nPropOtherCompPropID_pk; }
	double getPropOtherCompValue_entered()		{ return m_fPropOtherCompValue_entered; }
	double getPropOtherCompValue_calc()				{ return m_fPropOtherCompValue_calc; }
	CString getPropOtherCompNote()						{ return m_sPropOtherCompNote; }
	int getPropOtherCompTypeOf()							{ return m_nPropOtherCompTypeOf; }
	CString getPropOtherCompDate()						{ return m_sPropOtherCompDate; }

	void setPropOtherCompValue_calc(double v)		{ m_fPropOtherCompValue_calc = v; }

};

typedef std::vector<CTransaction_elv_properties_other_comp> vecTransaction_elv_properties_other_comp;



// Transaction classes used in "Intr�ngsber�kning", to doUMForrestNorm
// This class'll hold data on SUM Volume in m3sk for "Tr�d i gatan" and "Kanttr�d"; 080508 p�d
class CTransaction_elv_m3sk_per_specie
{
	//private:
	int m_nSpcID;
	double m_fM3Sk_inside;	// "I gatan"
	double m_fM3Sk_randtrees;
public:
	CTransaction_elv_m3sk_per_specie(void)
	{
		m_nSpcID = -1;
		m_fM3Sk_inside = 0.0;
		m_fM3Sk_randtrees = 0.0;
	}

	CTransaction_elv_m3sk_per_specie(int spc_id,double m3sk_inside,double m3sk_randtrees)
	{
		m_nSpcID = spc_id;
		m_fM3Sk_inside = m3sk_inside;
		m_fM3Sk_randtrees = m3sk_randtrees;
	}
	CTransaction_elv_m3sk_per_specie(const CTransaction_elv_m3sk_per_specie& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_m3sk_per_specie &c) const
	{
		return (m_nSpcID == c.m_nSpcID && m_fM3Sk_inside == c.m_fM3Sk_inside && m_fM3Sk_randtrees == c.m_fM3Sk_randtrees);
	}


	int getSpcID(void)							{ return m_nSpcID; }
	double getM3SkInside(void)			{ return m_fM3Sk_inside; }
	double getM3SkRandTrees(void)		{ return m_fM3Sk_randtrees; }

};

typedef std::vector<CTransaction_elv_m3sk_per_specie> vecTransaction_elv_m3sk_per_specie;


// This class'll holds return-data from ForrestNorm: "Storm- och Torkskador"; 080509 p�d
class CTransaction_elv_return_storm_dry
{
	//private:
	double m_fStormDryValue;
	double m_fStormDryVolume;
	double m_fStormDrySpruceMix;	// "Gransinblandning"
	double m_fAvgPricefactor;			// "Medelprisfactor"

			//HMS-50 Info om storm o tork 20200415 J�
	double m_fStormTorkInfo_SpruceMix;
	double m_fStormTorkInfo_SumM3Sk_inside;
	double m_fStormTorkInfo_AvgPriceFactor;
	double m_fStormTorkInfo_TakeCareOfPerc;
	double m_fStormTorkInfo_PineP30Price;
	double m_fStormTorkInfo_SpruceP30Price;
	double m_fStormTorkInfo_BirchP30Price;
	double m_fStormTorkInfo_CompensationLevel;
	double m_fStormTorkInfo_Andel_Pine;
	double m_fStormTorkInfo_Andel_Spruce;
	double m_fStormTorkInfo_Andel_Birch;
	double m_fStormTorkInfo_WideningFactor;
public:
	CTransaction_elv_return_storm_dry(void)
	{
		m_fStormDryValue = 0.0;
		m_fStormDryVolume = 0.0;
		m_fStormDrySpruceMix = 0.0;
		m_fAvgPricefactor = 0.0;
			m_fStormTorkInfo_SpruceMix = 0.0;
	m_fStormTorkInfo_SumM3Sk_inside = 0.0;
	m_fStormTorkInfo_AvgPriceFactor = 0.0;
	m_fStormTorkInfo_TakeCareOfPerc = 0.0;
	m_fStormTorkInfo_PineP30Price = 0.0;
	m_fStormTorkInfo_SpruceP30Price = 0.0;
	m_fStormTorkInfo_BirchP30Price = 0.0;
	m_fStormTorkInfo_CompensationLevel = 0.0;
	m_fStormTorkInfo_Andel_Pine= 0.0;
	m_fStormTorkInfo_Andel_Spruce= 0.0;
	m_fStormTorkInfo_Andel_Birch= 0.0;
	m_fStormTorkInfo_WideningFactor=0.0;
	}

	CTransaction_elv_return_storm_dry(double value,double volume,double spruce_mix,double avg_price,
		double fStormTorkInfo_SpruceMix,
		double fStormTorkInfo_SumM3Sk_inside,
		double fStormTorkInfo_AvgPriceFactor,
		double fStormTorkInfo_TakeCareOfPerc,
		double fStormTorkInfo_PineP30Price,
		double fStormTorkInfo_SpruceP30Price,
		double fStormTorkInfo_BirchP30Price,
		double fStormTorkInfo_CompensationLevel,
		double fStormTorkInfo_Andel_Pine,
		double fStormTorkInfo_Andel_Spruce,
		double fStormTorkInfo_Andel_Birch,
		double fStormTorkInfo_WideningFactor
		)
	{
		m_fStormDryValue = value;
		m_fStormDryVolume = volume;
		m_fStormDrySpruceMix = spruce_mix;
		m_fAvgPricefactor = avg_price;
		m_fStormTorkInfo_SpruceMix=fStormTorkInfo_SpruceMix;
		m_fStormTorkInfo_SumM3Sk_inside=fStormTorkInfo_SumM3Sk_inside;
		m_fStormTorkInfo_AvgPriceFactor=fStormTorkInfo_AvgPriceFactor;
		m_fStormTorkInfo_TakeCareOfPerc=fStormTorkInfo_TakeCareOfPerc;
		m_fStormTorkInfo_PineP30Price=fStormTorkInfo_PineP30Price;
		m_fStormTorkInfo_SpruceP30Price=fStormTorkInfo_SpruceP30Price;
		m_fStormTorkInfo_BirchP30Price=fStormTorkInfo_BirchP30Price;
		m_fStormTorkInfo_CompensationLevel=fStormTorkInfo_CompensationLevel;
		m_fStormTorkInfo_Andel_Pine=fStormTorkInfo_Andel_Pine;
		m_fStormTorkInfo_Andel_Spruce=fStormTorkInfo_Andel_Spruce;
		m_fStormTorkInfo_Andel_Birch=fStormTorkInfo_Andel_Birch;
		m_fStormTorkInfo_WideningFactor=fStormTorkInfo_WideningFactor;
	}

	CTransaction_elv_return_storm_dry(const CTransaction_elv_return_storm_dry& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_return_storm_dry &c) const
	{
		return (m_fStormDryValue == c.m_fStormDryValue &&
			m_fStormDryVolume == c.m_fStormDryVolume &&
			m_fStormDrySpruceMix == c.m_fStormDrySpruceMix &&
			m_fAvgPricefactor == c.m_fAvgPricefactor &&

			m_fStormTorkInfo_SpruceMix==c.m_fStormTorkInfo_SpruceMix &&
			m_fStormTorkInfo_SumM3Sk_inside==c.m_fStormTorkInfo_SumM3Sk_inside &&
			m_fStormTorkInfo_AvgPriceFactor==c.m_fStormTorkInfo_AvgPriceFactor &&
			m_fStormTorkInfo_TakeCareOfPerc==c.m_fStormTorkInfo_TakeCareOfPerc &&
			m_fStormTorkInfo_PineP30Price==c.m_fStormTorkInfo_PineP30Price &&
			m_fStormTorkInfo_SpruceP30Price==c.m_fStormTorkInfo_SpruceP30Price &&
			m_fStormTorkInfo_BirchP30Price==c.m_fStormTorkInfo_BirchP30Price &&
			m_fStormTorkInfo_CompensationLevel==c.m_fStormTorkInfo_CompensationLevel &&
			m_fStormTorkInfo_Andel_Pine==c.m_fStormTorkInfo_Andel_Pine &&
			m_fStormTorkInfo_Andel_Spruce==c.m_fStormTorkInfo_Andel_Spruce &&
			m_fStormTorkInfo_Andel_Birch==c.m_fStormTorkInfo_Andel_Birch && 
			m_fStormTorkInfo_WideningFactor==c.m_fStormTorkInfo_WideningFactor
						);
	}


	double getStormDryValue()				{ return m_fStormDryValue;	}
	double getStormDryVolume()			{ return m_fStormDryVolume;	}
	double getStormDrySpruceMix()		{ return m_fStormDrySpruceMix;	}
	double getAvgPriceFactor()			{ return m_fAvgPricefactor; }
	//HMS-50 Info om storm o tork 20200415 J�
	double getStormTorkInfo_SpruceMix()			{return m_fStormTorkInfo_SpruceMix;}
	double getStormTorkInfo_SumM3Sk_inside()		{return m_fStormTorkInfo_SumM3Sk_inside;}
	double getStormTorkInfo_AvgPriceFactor()		{return m_fStormTorkInfo_AvgPriceFactor;}
	double getStormTorkInfo_TakeCareOfPerc()		{return m_fStormTorkInfo_TakeCareOfPerc;}
	double getStormTorkInfo_PineP30Price()			{return m_fStormTorkInfo_PineP30Price;}
	double getStormTorkInfo_SpruceP30Price()		{return m_fStormTorkInfo_SpruceP30Price;}
	double getStormTorkInfo_BirchP30Price()			{return m_fStormTorkInfo_BirchP30Price;}
	double getStormTorkInfo_CompensationLevel()		{return m_fStormTorkInfo_CompensationLevel;}
	double getStormTorkInfo_Andel_Pine()		{return m_fStormTorkInfo_Andel_Pine;}
	double getStormTorkInfo_Andel_Spruce()		{return m_fStormTorkInfo_Andel_Spruce;}
	double getStormTorkInfo_Andel_Birch()		{return m_fStormTorkInfo_Andel_Birch;}
	double getStormTorkInfo_WideningFactor()		{return  m_fStormTorkInfo_WideningFactor;}
};


// This class'll holds return-data from ForrestNorm: "Markv�rde och F�rtidig avverkning"; 080509 p�d
class CTransaction_elv_return_land_and_precut
{
	//private:
	double m_fLandValue;					// "Markv�rde"
	double m_fPreCutValue;				// "F�rtidig avverkning"
	double m_fLandValue_ha;				// "Markv�rde per ha"
	double m_fPreCutValue_ha;			// "F�rtidig avverkning per ha"
	double m_fCorrFactor;					// "Korrectionsfaktor - fr�n tabell i Skogsnorm, vid Taxeringsbes�nd"

	//HMS-49 Info om f�rtidig avverkning 20191126 J�
	double m_fPreCutInfo_P30_Pine;
	double m_fPreCutInfo_P30_Spruce;
	double m_fPreCutInfo_P30_Birch;
	double m_fPreCutInfo_Andel_Pine;
	double m_fPreCutInfo_Andel_Spruce;
	double m_fPreCutInfo_Andel_Birch;
	double m_fPreCutInfo_CorrFact;
	double m_fPreCutInfo_Ers_Pine;
	double m_fPreCutInfo_Ers_Spruce;
	double m_fPreCutInfo_Ers_Birch;	
	double m_fPreCutInfo_ReducedBy;	

	//HMS-48 Info om markv�rde 20200427 J�
	double m_fMarkInfo_ValuePine;
	double m_fMarkInfo_ValueSpruce;
	double m_fMarkInfo_Andel_Pine;
	double m_fMarkInfo_Andel_Spruce;
	double m_fMarkInfo_P30_Pine;
	double m_fMarkInfo_P30_Spruce;
	double m_fMarkInfo_ReducedBy;

public:
	CTransaction_elv_return_land_and_precut(void)
	{
		m_fLandValue = 0.0;
		m_fPreCutValue = 0.0;
		m_fLandValue_ha = 0.0;
		m_fPreCutValue_ha = 0.0;
		m_fCorrFactor = 0.0;

	m_fPreCutInfo_P30_Pine= 0.0;
	m_fPreCutInfo_P30_Spruce= 0.0;
	m_fPreCutInfo_P30_Birch= 0.0;
	m_fPreCutInfo_Andel_Pine= 0.0;
	m_fPreCutInfo_Andel_Spruce= 0.0;
	m_fPreCutInfo_Andel_Birch= 0.0;
	m_fPreCutInfo_CorrFact= 0.0;
	m_fPreCutInfo_Ers_Pine= 0.0;
	m_fPreCutInfo_Ers_Spruce= 0.0;
	m_fPreCutInfo_Ers_Birch= 0.0;	
	m_fPreCutInfo_ReducedBy=0.0;	

	m_fMarkInfo_ValuePine=0.0;
	m_fMarkInfo_ValueSpruce=0.0;
	m_fMarkInfo_Andel_Pine=0.0;
	m_fMarkInfo_Andel_Spruce=0.0;
	m_fMarkInfo_P30_Pine=0.0;
	m_fMarkInfo_P30_Spruce=0.0;
	m_fMarkInfo_ReducedBy=0.0;

	}

	CTransaction_elv_return_land_and_precut(double land_value,double precut_value,double land_value_ha,double precut_value_ha,double corr_fac,
		//HMS-49 Info om f�rtidig avverkning
		double fPreCutInfo_P30_Pine,
		double fPreCutInfo_P30_Spruce,
		double fPreCutInfo_P30_Birch,
		double fPreCutInfo_Andel_Pine,
		double fPreCutInfo_Andel_Spruce,
		double fPreCutInfo_Andel_Birch,
		double fPreCutInfo_CorrFact,
		double fPreCutInfo_Ers_Pine,
		double fPreCutInfo_Ers_Spruce,
		double fPreCutInfo_Ers_Birch,	
		double fPreCutInfo_ReducedBy,
			double fMarkInfo_ValuePine,
	double fMarkInfo_ValueSpruce,
	double fMarkInfo_Andel_Pine,
	double fMarkInfo_Andel_Spruce,
	double fMarkInfo_P30_Pine,
	double fMarkInfo_P30_Spruce,
	double fMarkInfo_ReducedBy
	)
	{
		m_fLandValue = land_value;
		m_fPreCutValue = precut_value;
		m_fLandValue_ha = land_value_ha;
		m_fPreCutValue_ha = precut_value_ha;
		m_fCorrFactor = corr_fac;
//HMS-49 Info om f�rtidig avverkning
	m_fPreCutInfo_P30_Pine=fPreCutInfo_P30_Pine;
	m_fPreCutInfo_P30_Spruce=fPreCutInfo_P30_Spruce;
	m_fPreCutInfo_P30_Birch=fPreCutInfo_P30_Birch;
	m_fPreCutInfo_Andel_Pine=fPreCutInfo_Andel_Pine;
	m_fPreCutInfo_Andel_Spruce=fPreCutInfo_Andel_Spruce;
	m_fPreCutInfo_Andel_Birch=fPreCutInfo_Andel_Birch;
	m_fPreCutInfo_CorrFact=fPreCutInfo_CorrFact;
	m_fPreCutInfo_Ers_Pine=fPreCutInfo_Ers_Pine;
	m_fPreCutInfo_Ers_Spruce=fPreCutInfo_Ers_Spruce;
	m_fPreCutInfo_Ers_Birch=fPreCutInfo_Ers_Birch;	
	m_fPreCutInfo_ReducedBy=fPreCutInfo_ReducedBy;

//HMS-48 Info om markv�rde 20200427 J�
	m_fMarkInfo_ValuePine=fMarkInfo_ValuePine;
	m_fMarkInfo_ValueSpruce=fMarkInfo_ValueSpruce;
	m_fMarkInfo_Andel_Pine=fMarkInfo_Andel_Pine;
	m_fMarkInfo_Andel_Spruce=fMarkInfo_Andel_Spruce;
	m_fMarkInfo_P30_Pine=fMarkInfo_P30_Pine;
	m_fMarkInfo_P30_Spruce=fMarkInfo_P30_Spruce;
	m_fMarkInfo_ReducedBy=fMarkInfo_ReducedBy;

	}

	CTransaction_elv_return_land_and_precut(const CTransaction_elv_return_land_and_precut& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_return_land_and_precut &c) const
	{
		return (m_fLandValue == c.m_fLandValue &&
			m_fPreCutValue == c.m_fPreCutValue &&
			m_fLandValue_ha == c.m_fLandValue_ha &&
			m_fPreCutValue_ha == c.m_fPreCutValue_ha &&
			m_fCorrFactor == c.m_fCorrFactor &&
			//HMS-49 Info om f�rtidig avverkning
			m_fPreCutInfo_P30_Pine==c.m_fPreCutInfo_P30_Pine &&
			m_fPreCutInfo_P30_Spruce==c.m_fPreCutInfo_P30_Spruce &&
			m_fPreCutInfo_P30_Birch==c.m_fPreCutInfo_P30_Birch &&
			m_fPreCutInfo_Andel_Pine==c.m_fPreCutInfo_Andel_Pine &&
			m_fPreCutInfo_Andel_Spruce==c.m_fPreCutInfo_Andel_Spruce &&
			m_fPreCutInfo_Andel_Birch==c.m_fPreCutInfo_Andel_Birch &&
			m_fPreCutInfo_CorrFact==c.m_fPreCutInfo_CorrFact &&
			m_fPreCutInfo_Ers_Pine==c.m_fPreCutInfo_Ers_Pine &&
			m_fPreCutInfo_Ers_Spruce==c.m_fPreCutInfo_Ers_Spruce &&
			m_fPreCutInfo_Ers_Birch==c.m_fPreCutInfo_Ers_Birch &&
			m_fPreCutInfo_ReducedBy==c.m_fPreCutInfo_ReducedBy &&
			
			m_fMarkInfo_ValuePine==c.m_fMarkInfo_ValuePine &&
			m_fMarkInfo_ValueSpruce==c.m_fMarkInfo_ValueSpruce &&
			m_fMarkInfo_Andel_Pine==c.m_fMarkInfo_Andel_Pine &&
			m_fMarkInfo_Andel_Spruce==c.m_fMarkInfo_Andel_Spruce &&
			m_fMarkInfo_P30_Pine==c.m_fMarkInfo_P30_Pine &&
			m_fMarkInfo_P30_Spruce==c.m_fMarkInfo_P30_Spruce &&
			m_fMarkInfo_ReducedBy==c.m_fMarkInfo_ReducedBy
			);
	}


	double getLandValue()				{ return m_fLandValue;	}
	double getPreCutValue()			{ return m_fPreCutValue;	}
	double getLandValue_ha()		{ return m_fLandValue_ha;	}
	double getPreCutValue_ha()	{ return m_fPreCutValue_ha;	}
	double getCorrFactor()			{ return m_fCorrFactor;	}

	//HMS-49 Info om f�rtidig avverkning
	double getPreCutInfo_P30_Pine()		{ return m_fPreCutInfo_P30_Pine;	}
	double getPreCutInfo_P30_Spruce()	{ return m_fPreCutInfo_P30_Spruce;	}
	double getPreCutInfo_P30_Birch()	{ return  m_fPreCutInfo_P30_Birch;	}
	double getPreCutInfo_Andel_Pine()	{ return  m_fPreCutInfo_Andel_Pine;	}
	double getPreCutInfo_Andel_Spruce()	{ return  m_fPreCutInfo_Andel_Spruce;	}
	double getPreCutInfo_Andel_Birch()	{ return  m_fPreCutInfo_Andel_Birch;	}
	double getPreCutInfo_CorrFact()		{ return  m_fPreCutInfo_CorrFact;	}
	double getPreCutInfo_Ers_Pine()		{ return  m_fPreCutInfo_Ers_Pine;	}
	double getPreCutInfo_Ers_Spruce()	{ return  m_fPreCutInfo_Ers_Spruce;	}
	double getPreCutInfo_Ers_Birch()	{ return  m_fPreCutInfo_Ers_Birch;	}	
	double getPreCutInfo_ReducedBy()	{ return  m_fPreCutInfo_ReducedBy; }
	
	//HMS-48 Info om markv�rde 20200427 J�
	double getMarkInfo_ValuePine()	{ return  m_fMarkInfo_ValuePine; }
	double getMarkInfo_ValueSpruce()	{ return  m_fMarkInfo_ValueSpruce; }
	double getMarkInfo_Andel_Pine()	{ return  m_fMarkInfo_Andel_Pine; }
	double getMarkInfo_Andel_Spruce()	{ return  m_fMarkInfo_Andel_Spruce; }
	double getMarkInfo_P30_Pine()	{ return  m_fMarkInfo_P30_Pine;}
	double getMarkInfo_P30_Spruce()	{ return  m_fMarkInfo_P30_Spruce;}
	double getMarkInfo_ReducedBy()	{ return  m_fMarkInfo_ReducedBy;}

};


// This class'll holds data from esti_trakt_tdcls_table "St�mplingsl�ngd"; 080526 p�d
class CTransaction_elv_slen_data
{
	//private:
	int nSpcID;
	CString sSpcName;
	double fDclsFrom;
	double fDclsTo;
	long nNumOf;
	long nNumOfRandTrees;
public:
	CTransaction_elv_slen_data(void)
	{
		nSpcID = -1;
		sSpcName = _T("");
		fDclsFrom = 0.0;
		fDclsTo = 0.0;
		nNumOf = 0;
		nNumOfRandTrees = 0;
	}

	CTransaction_elv_slen_data(int spc_id,LPCTSTR spc_name,double dcls_from,double dcls_to,long num_of,long numof_randtrees)
	{
		nSpcID = spc_id;
		sSpcName = (spc_name);
		fDclsFrom = dcls_from;
		fDclsTo = dcls_to;
		nNumOf = num_of;
		nNumOfRandTrees = numof_randtrees;
	}

	CTransaction_elv_slen_data(const CTransaction_elv_slen_data& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_slen_data &c) const
	{
		return (nSpcID == c.nSpcID &&
						sSpcName == c.sSpcName &&
						fDclsFrom == c.fDclsFrom &&
						fDclsTo == c.fDclsTo &&
						nNumOf == c.nNumOf &&
						nNumOfRandTrees == c.nNumOfRandTrees);
	}

	int getSpcID()						{ return nSpcID; }
	CString getSpcName()			{ return sSpcName; }
	double getDclsFrom()			{ return fDclsFrom; }
	double getDclsTo()				{ return fDclsTo; }
	long getNumOf()						{ return nNumOf; }
	long getNumOfRandTrees()	{ return nNumOfRandTrees; }
};

typedef std::vector<CTransaction_elv_slen_data> vecTransaction_elv_slen_data;

// This class'll holds summarized data from esti_trakt_tdcls_table "St�mplingsl�ngd"; 080526 p�d
class CTransaction_elv_slen_sum_data
{
	//private:
	int nSpcID;
	CString sSpcName;
	long nNumOf;
	long nNumOfRandTrees;
	double fM3Sk;
	double fM3Sk_randtrees;
public:
	CTransaction_elv_slen_sum_data(void)
	{
		nSpcID = -1;
		sSpcName = _T("");
		nNumOf = 0;
		nNumOfRandTrees = 0;
		fM3Sk = 0.0;
		fM3Sk_randtrees = 0.0;
	}

	CTransaction_elv_slen_sum_data(int spc_id,LPCTSTR spc_name,long num_of,long numof_randtrees,double m3sk,double m3sk_randtrees)
	{
		nSpcID = spc_id;
		sSpcName = (spc_name);
		nNumOf = num_of;
		nNumOfRandTrees = numof_randtrees;
		fM3Sk = m3sk;
		fM3Sk_randtrees = m3sk_randtrees;
	}

	CTransaction_elv_slen_sum_data(const CTransaction_elv_slen_sum_data& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_slen_sum_data &c) const
	{
		return (nSpcID == c.nSpcID &&
						sSpcName == c.sSpcName &&
						nNumOf == c.nNumOf &&
						nNumOfRandTrees == c.nNumOfRandTrees &&
						fM3Sk == c.fM3Sk &&
						fM3Sk_randtrees == c.fM3Sk_randtrees);
	}


	int getSpcID()						{ return nSpcID; }
	CString getSpcName()			{ return sSpcName; }
	long getNumOf()						{ return nNumOf; }
	long getNumOfRandTrees()	{ return nNumOfRandTrees; }
	double getM3sk()					{ return fM3Sk; }
	double getM3SkRandTrees()	{ return fM3Sk_randtrees; }
};

typedef std::vector<CTransaction_elv_slen_sum_data> vecTransaction_elv_slen_sum_data;


// This class'll hold information on Assortments for Stand(s) on an Object; 080612 p�d
class CTransaction_elv_ass_stands_obj
{
	int nAssortID;
	CString sAssortName;
	double fKrPerM3;
public:
	CTransaction_elv_ass_stands_obj(void)
	{
		nAssortID = -1;
		sAssortName = _T("");
		fKrPerM3 = 0.0;
	}
	CTransaction_elv_ass_stands_obj(int ass_id,LPCTSTR ass_name,double kr_per_m3)
	{
		nAssortID = ass_id;
		sAssortName = (ass_name);
		fKrPerM3 = kr_per_m3;
	}
	CTransaction_elv_ass_stands_obj(const CTransaction_elv_ass_stands_obj &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_ass_stands_obj &c) const
	{
		return (nAssortID == c.nAssortID &&
						sAssortName == c.sAssortName &&
						fKrPerM3 == c.fKrPerM3);
	}

	int getAssortID()					{ return nAssortID; }
	CString getAssortName()		{ return sAssortName; }
	double getKrPerM3()				{ return fKrPerM3; }
	void setKrPerM3(double v)	{ fKrPerM3 = v; }
};

typedef std::vector<CTransaction_elv_ass_stands_obj> vecTransaction_elv_ass_stands_obj;


// Simple Transaction class, holds info. on TraktID,PropID and ObjectID in "elv_cruise_table"; 080613 p�d
class CTransaction_elv_cruise_id
{
	int m_nObjID;
	int m_nPropID;
	long m_nTraktID;
public:
	CTransaction_elv_cruise_id(void)
	{
		m_nObjID = -1;
		m_nPropID = -1;
		m_nTraktID = -1;
	}
	CTransaction_elv_cruise_id(int obj_id,int prop_id,long trakt_id)
	{
		m_nObjID = obj_id;
		m_nPropID = prop_id;
		m_nTraktID = trakt_id;
	}

	CTransaction_elv_cruise_id(const CTransaction_elv_cruise_id& c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_elv_cruise_id &c) const
	{
		return (m_nObjID == c.m_nObjID &&
						m_nPropID == c.m_nPropID &&
						m_nTraktID == c.m_nTraktID);
	}


	int getObjID()		{ return m_nObjID; }
	int getPropID()		{ return m_nPropID; }
	long getTraktID()	{ return m_nTraktID; }
};

typedef std::vector<CTransaction_elv_cruise_id> vecTransaction_elv_cruise_id;


// Class for handling external documents; 080925 p�d
class CTransaction_external_docs
{
	//private:
	int m_nExtDocID_pk;
	CString m_sExtDocLinkTbl_pk;
	int m_nExtDocLinkID;
	int m_nExtDocLinkType;
	CString m_sExtDocURL;
	CString m_sExtDocNote;
	CString m_sExtDocDoneBy;
	CString m_sExtDocCreated;
public:
	CTransaction_external_docs(void)
	{
		m_nExtDocID_pk = -1;
		m_sExtDocLinkTbl_pk = _T("");
		m_nExtDocLinkID = -1;
		m_nExtDocLinkType = -1;
		m_sExtDocURL = _T("");
		m_sExtDocNote = _T("");
		m_sExtDocDoneBy = _T("");
		m_sExtDocCreated = _T("");
	}
	CTransaction_external_docs(int id,LPCTSTR tbl_name,int link_id,int link_type,LPCTSTR url,LPCTSTR note,LPCTSTR done_by,LPCTSTR created)
	{
		m_nExtDocID_pk = id;
		m_sExtDocLinkTbl_pk = (tbl_name);
		m_nExtDocLinkID = link_id;
		m_nExtDocLinkType = link_type;
		m_sExtDocURL = (url);
		m_sExtDocNote = (note);
		m_sExtDocDoneBy = (done_by);
		m_sExtDocCreated = (created);
	}
	CTransaction_external_docs(const CTransaction_external_docs &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_external_docs &c) const
	{
		return (m_nExtDocID_pk == c.m_nExtDocID_pk &&
						m_sExtDocLinkTbl_pk == c.m_sExtDocLinkTbl_pk &&
						m_nExtDocLinkID == c.m_nExtDocLinkID &&
						m_nExtDocLinkType == c.m_nExtDocLinkType &&
						m_sExtDocURL == c.m_sExtDocURL &&
						m_sExtDocNote == c.m_sExtDocNote &&
						m_sExtDocDoneBy == c.m_sExtDocDoneBy);
	}

	int getExtDocID_pk()						{ return m_nExtDocID_pk; }
	CString getExtDocLinkTbl_pk()		{ return m_sExtDocLinkTbl_pk; }
	int getExtDocLinkID()						{ return m_nExtDocLinkID; }
	int getExtDocLinkType()					{ return m_nExtDocLinkType; }
	CString getExtDocURL()					{ return m_sExtDocURL; }
	CString getExtDocNote()					{ return m_sExtDocNote; }
	CString getExtDocDoneBy()				{ return m_sExtDocDoneBy; }
	CString getExtDocCreated()			{ return m_sExtDocCreated; }


};

typedef std::vector<CTransaction_external_docs> vecTransaction_external_docs;

// Class for property status; 090914 p�d
class CTransaction_property_status
{
	//private:
	int m_nPropStatusID_pk;
	short m_nPropStatusOrder;			// Ordningsnummer
	CString m_sPropStatusName;
	short m_nPropStatusType;		// 0 = No recalculation, 1 = Can recalculate
	short m_nPropStatusRCol;		// Red - Color intensity
	short m_nPropStatusGCol;		// Green - Color intensity
	short m_nPropStatusBCol;		// Blue - Color intensity
	CString m_sPropStatusCreated;
public:
	CTransaction_property_status(void)
	{
		m_nPropStatusID_pk = -1;
		m_nPropStatusOrder = 1;
		m_sPropStatusName = _T("");
		m_nPropStatusType = 0;
		m_nPropStatusRCol = 255;
		m_nPropStatusGCol = 255;
		m_nPropStatusBCol = 255;
		m_sPropStatusCreated = _T("");
	}
	CTransaction_property_status(int id,short order_num,LPCTSTR name,short type_of,short r_col,short g_col,short b_col,LPCTSTR created)
	{
		m_nPropStatusID_pk = id;
		m_nPropStatusOrder = order_num;
		m_sPropStatusName = name;
		m_nPropStatusType = type_of;
		m_nPropStatusRCol = r_col;
		m_nPropStatusGCol = g_col;
		m_nPropStatusBCol = b_col;
		m_sPropStatusCreated = created;
	}
	CTransaction_property_status(const CTransaction_property_status &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_property_status &c) const
	{
		return (m_nPropStatusID_pk == c.m_nPropStatusID_pk &&
						m_nPropStatusOrder == c.m_nPropStatusOrder &&
						m_sPropStatusName == c.m_sPropStatusName &&
						m_nPropStatusType == c.m_nPropStatusType &&
						m_nPropStatusRCol == c.m_nPropStatusRCol &&
						m_nPropStatusGCol == c.m_nPropStatusGCol &&
						m_nPropStatusBCol == c.m_nPropStatusBCol &&
						m_sPropStatusCreated == c.m_sPropStatusCreated);
	}

	int getPropStatusID_pk()					{ return m_nPropStatusID_pk; }
	void setPropStatusID_pk(int v)					{ m_nPropStatusID_pk = v; }
	short getPropStatusOrderNum()			{ return m_nPropStatusOrder; }
	CString getPropStatusName()				{ return m_sPropStatusName; }
	short getPropStatusType()					{ return m_nPropStatusType; }
	short getPropStatusRCol()					{ return m_nPropStatusRCol; }
	void setPropStatusRCol(short v)			{ m_nPropStatusRCol = v; }
	short getPropStatusGCol()					{ return m_nPropStatusGCol; }
	void setPropStatusGCol(short v)			{ m_nPropStatusGCol = v; }
	short getPropStatusBCol()					{ return m_nPropStatusBCol; }
	void setPropStatusBCol(short v)			{ m_nPropStatusBCol = v; }
	CString getPropStatusCreated()		{ return m_sPropStatusCreated; }
};

typedef std::vector<CTransaction_property_status> vecTransaction_property_status;


// Holds data calculated in doExchangeSimple,doExchangeCalculationSimple; 100409 p�d
class CTransaction_exchange_simple
{
	int m_nTraktID;
	int m_nSpcID;
	CString m_sSpcName;
	CString m_sAssName;
	double m_fDiam;		// Diameter on bark
	double m_fBark;		// NB! Double bark here!
	double m_fDiamUb;	// Diameter under bark
	double m_fMinDiam;
	double m_fDCLSFrom;
	double m_fDCLSTo;
	double m_fExchValue;
	double m_fExchVolume;
	double m_fDeltaExchVolume;	// Actual volume per assortment
	short m_nIsPulp;		// "1 = Massaved"
public:
	CTransaction_exchange_simple()
	{
		m_nTraktID = -1;
		m_nSpcID = -1;
		m_sSpcName = _T("");
		m_sAssName = _T("");
		m_fDiam = 0.0;		// Diameter on bark
		m_fBark = 0.0;		// NB! Double bark here!
		m_fDiamUb = 0.0;	// Diameter under bark
		m_fMinDiam = 0.0;
		m_fDCLSFrom = 0.0;
		m_fDCLSTo = 0.0;
		m_fExchValue = 0.0;
		m_fExchVolume = 0.0;
		m_fDeltaExchVolume = 0.0;
		m_nIsPulp = -1;		// "1 = Massaved"
	}
	CTransaction_exchange_simple(int trakt_id,int spc_id,
															 LPCTSTR spc_name,LPCTSTR ass_name,
															 double diam,double bark,double diam_ub,double min_diam,
															 double dcls_from,double dcls_to,
															 double exch_value,double exch_volume,double delta_exch_volume,short is_pulp)
	{
		m_nTraktID = trakt_id;
		m_nSpcID = spc_id;
		m_sSpcName = spc_name;
		m_sAssName = ass_name;
		m_fDiam = diam;				// Diameter on bark
		m_fBark = bark;				// NB! Double bark here!
		m_fDiamUb = diam_ub;	// Diameter under bark
		m_fMinDiam = min_diam;
		m_fDCLSFrom = dcls_from;
		m_fDCLSTo = dcls_to;
		m_fExchValue = exch_value;
		m_fExchVolume = exch_volume;
		m_fDeltaExchVolume = delta_exch_volume;
		m_nIsPulp = is_pulp;		// "1 = Massaved"
	}
	CTransaction_exchange_simple(const 	CTransaction_exchange_simple &c)	
	{ 
		*this = c; 
	}

	int getTraktID()					{ return m_nTraktID; }
	int getSpcID()						{ return m_nSpcID; }
	CString getSpcName()			{ return m_sSpcName; }
	CString getAssName()			{ return m_sAssName; }
	void setAssName(LPCTSTR s)	{ m_sAssName = s; }
	double getDiam()					{ return m_fDiam; }
	void setDiam(double v)		{ m_fDiam = v; }
	double getBark()					{ return m_fBark; }
	void setBark(double v)		{ m_fBark = v; }
	double getDiamUb()				{ return m_fDiamUb; }
	void setDiamUb(double v)	{ m_fDiamUb = v; }
	double getMinDiam()				{ return m_fMinDiam; }
	void setMinDiam(double v)	{ m_fMinDiam = v; }
	double getDCLSFrom()			{ return m_fDCLSFrom; }
	void setDCLSFrom(double v)	{ m_fDCLSFrom = v; }
	double getDCLSTo()				{ return m_fDCLSTo; }
	void setDCLSTo(double v)	{ m_fDCLSTo = v; }
	double getExchValue()			{ return m_fExchValue; }
	void setExchValue(double v)	{ m_fExchValue = v; }
	double getExchVolume()		{ return m_fExchVolume; }
	void setExchVolume(double v)	{ m_fExchVolume = v; }
	double getDeltaExchVolume()		{ return m_fDeltaExchVolume; }
	void setDeltaExchVolume(double v)	{ m_fDeltaExchVolume = v; }
	short getIsPulp()					{ return m_nIsPulp; }
	void setIsPulp(short v)			{ m_nIsPulp = v; }

};
typedef std::vector<CTransaction_exchange_simple> vecTransaction_exchange_simple;

#endif