#ifndef _COSTSTMPLPARSER_H_
#define _COSTSTMPLPARSER_H_

#include "pad_transaction_classes.h"

#include "xmlliteBase.h"


class CostsTmplParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	CostsTmplParser(void);

	virtual ~CostsTmplParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL getCostsTmplName(LPTSTR);
	BOOL getCostsTmplDoneBy(LPTSTR);
	BOOL getCostsTmplDate(LPTSTR);
	
	BOOL getCostsTmplCutting(CTransaction_costtempl_cutting &);
	BOOL getCostsTmplTransport(vecTransaction_costtempl_transport &);
	BOOL getCostsTmplOthers(CTransaction_costtempl_other_costs &);

	BOOL getXML(CString &xml);
};



class ObjectCostsTmplParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	ObjectCostsTmplParser(void);

	virtual ~ObjectCostsTmplParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(CString);
	BOOL SaveToFile(LPCTSTR);

	BOOL getObjCostsTmplName(LPTSTR);
	BOOL setObjCostsTmplName(LPCTSTR);
	BOOL getObjCostsTmplDoneBy(LPTSTR);
	BOOL getObjCostsTmplDate(LPTSTR);

	BOOL getCutCostSetAs(int *);
	
	BOOL getObjCostsTmplTransport(vecObjectCostTemplate_table &);
	BOOL getObjCostsTmplTransport2(vecTransaction_costtempl_transport &);
	BOOL getObjCostsTmplCutting(vecObjectCostTemplate_table &,vecObjectCostTemplate_table &);
	BOOL getObjCostsTmplCutting2(CTransaction_costtempl_cutting &);
	BOOL getObjCostsTmplOther(vecObjectCostTemplate_other_cost_table &);

	BOOL getXML(CString &xml);
};



// Parser based on xmlLite; 091221 p�d

class xmlliteCostsParser : public CxmlliteBase
{
	BOOL getDGVTable(vecObjectCostTemplate_table &vec);
	BOOL getM3FUBTable(vecObjectCostTemplate_table &vec);
protected:
public:
	xmlliteCostsParser(void);
	virtual ~xmlliteCostsParser();

	BOOL getObjCostsTmplName(LPTSTR);
	BOOL setObjCostsTmplName(LPCTSTR);
	BOOL getObjCostsTmplDoneBy(LPTSTR);
	BOOL getObjCostsTmplDate(LPTSTR);

	BOOL getCutCostSetAs(int *);
	
	BOOL getObjCostsTmplTransport(vecObjectCostTemplate_table &);
	BOOL getObjCostsTmplTransport2(vecTransaction_costtempl_transport &);
	BOOL getObjCostsTmplCutting(vecObjectCostTemplate_table &,vecObjectCostTemplate_table &);
	BOOL getObjCostsTmplCutting2(CTransaction_costtempl_cutting &);
	BOOL getObjCostsTmplOther(vecObjectCostTemplate_other_cost_table &);
};

#endif