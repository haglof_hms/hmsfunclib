#pragma once

#include "Resource.h"

#include "XHTMLStatic.h"
// CMessageDlg dialog

class CMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CMessageDlg)

	CString m_sCaption;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sMsgText;
public:
	CMessageDlg(CWnd* pParent = NULL);   // standard constructor
	CMessageDlg(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg,CWnd* pParent = NULL); 
	virtual ~CMessageDlg();

// Dialog Data
	enum { IDD = IDD_FLIB_MSGDLG };

	CXHTMLStatic m_wndHTML;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
