/*
//////////////////////////////////////////////////////////////////////////
COPYRIGHT NOTICE, DISCLAIMER, and LICENSE:
//////////////////////////////////////////////////////////////////////////

CUtil : Copyright (C) 2008, Kazi Shupantha Imam (shupantha@yahoo.com)

//////////////////////////////////////////////////////////////////////////
Covered code is provided under this license on an "as is" basis, without
warranty of any kind, either expressed or implied, including, without
limitation, warranties that the covered code is free of defects,
merchantable, fit for a particular purpose or non-infringing. The entire
risk as to the quality and performance of the covered code is with you.
Should any covered code prove defective in any respect, you (not the
initial developer or any other contributor) assume the cost of any
necessary servicing, repair or correction. This disclaimer of warranty
constitutes an essential part of this license. No use of any covered code
is authorized hereunder except under this disclaimer.

Permission is hereby granted to use, copy, modify, and distribute this
source code, or portions hereof, for any purpose, including commercial
applications, freely and without fee, subject to the following
restrictions: 

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
//////////////////////////////////////////////////////////////////////////
*/

#pragma once

#ifdef WIN32
#include <windows.h>
#include "atlbase.h"
#endif

#include <sys\types.h> 
#include <sys\stat.h> 

#include <vector>
#include <string>

using std::vector;
using std::string;
using std::wstring;

#define stlString		std::wstring

#define DIRECTORY_SEPARATOR		L"\\"
#define DIRECTORY_SEPARATOR_C	L'\\'

class CUtil
{
public:
	CUtil(void);
	~CUtil(void);

public:
	//////////////////////////////////////////////////////////////////////////
	// File IO functions best suited for text files
	//////////////////////////////////////////////////////////////////////////
	static bool ReadFile(const stlString& strFilePath, stlString& strFileData);
	static bool WriteFile(const stlString& strFilePath, const stlString& strFileData);

	static long GetFileSize(const stlString& strFilePath);
	static __int64 GetFileSize64(const stlString& strFilePath);

	static bool IsFile(const stlString& strFilePath);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// String manipulation function, returns the number of successful substrings replaced
	//////////////////////////////////////////////////////////////////////////
	static long FindReplace(stlString& strSource, const stlString& strFind, const stlString& strReplace);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// String tokenizing functions
	//////////////////////////////////////////////////////////////////////////
	static long GetTokenCount(const stlString& strSource, const stlString& strDeliminator);
	static stlString GetToken(const stlString& strSource, const stlString& strDeliminator, long lTokenIndex);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Numeric to string converters
	//////////////////////////////////////////////////////////////////////////
	#define Long2String Long2StringW
	#define Double2String Double2StringW

	static string Long2StringA(unsigned long lNumber, unsigned long lPrecision);
	static string Long2StringA(long long lNumber);
	static string Double2StringA(long double dNumber, unsigned long lPrecision = 0);

	static wstring Long2StringW(unsigned long lNumber, unsigned long lPrecision);
	static wstring Long2StringW(long long lNumber);
	static wstring Double2StringW(long double dNumber, unsigned long lPrecision = 0);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// String case converters, upper <-> lower
	//////////////////////////////////////////////////////////////////////////
	static stlString ToUpper(const stlString& str);
	static stlString ToLower(const stlString& str);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Compare without case sensitivity
	//////////////////////////////////////////////////////////////////////////
	static int CompareNoCase(const stlString& str1, const stlString& str2);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// System time retrieval
	//////////////////////////////////////////////////////////////////////////
	static stlString GetSystemTime(const stlString& strDateFormat = _T("%A, %d/%m/%Y, %H:%M:%S - %Z"));
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Convert given number of seconds to hh:mm:ss and vice versa
	//////////////////////////////////////////////////////////////////////////
	static stlString GetTime(long lSeconds);
	static long GetTime(const stlString& strTime = _T("00:00:00"));
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Get commonly used directory paths
	//////////////////////////////////////////////////////////////////////////
	static stlString GetWorkingDirectory();
	static stlString GetProgramDirectory();

	static stlString GetProgramFilesDirectory();
	static stlString GetWindowsDirectory();
	static stlString GetSystemDirectory();
	
	static stlString GetMyDocumentsDirectory();
	static stlString GetMyMusicDirectory();
	static stlString GetMyPicturesDirectory();
	static stlString GetMyVideosDirectory();
	
	static stlString GetAppDataDirectory();
	static stlString GetLocalAppDataDirectory();
	
	static stlString GetDesktopDirectory();
	static stlString GetStartupDirectory();
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Copy or cut a file into another directory, creating the directory path if necessary
	//////////////////////////////////////////////////////////////////////////
	static bool CopyFile2Directory(const stlString& strSourceFilePath, const stlString& strDestinationDirectory, bool bDeleteSource = false);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Directory manipulation functions
	//////////////////////////////////////////////////////////////////////////
	// Get list of all files in the target directory
	static void GetFileList(const stlString& strTargetDirectoryPath, bool bLookInSubdirectories, vector<stlString>& vecstrFileList);

	// Create the entire directory path
	static void MakeDirectory(const stlString& strDirectoryPath);

	// Delete the entire directory path, including all files and folders within
	static void DeleteDirectory(const stlString& strTargetDirectoryPath);

	// Check whether the given path is a directory
	static bool IsDirectory(const stlString& strDirectoryPath);

	// Add "\" to the end of a directory path, if not present
	static stlString AddDirectoryEnding(const stlString& strDirectoryPath);

	// Remove "\" from the end of a directory path, if present
	static stlString RemoveDirectoryEnding(const stlString& strDirectoryPath);

	// Get the name of the directory form a given directory path: e.g. C:\Program Files\XYZ, will return XYZ
	static stlString GetDirectoryName(const stlString& strDirectoryPath);

	// Get the directory from a file path
	static stlString GetFileDirectory(const stlString& strFilePath);

	// Get the previous directory from a given directory path
	static stlString GetRootDirectory(const stlString& strDirectoryPath);

	// Get the file name including/excluding the extension from a given file path
	static stlString GetFileName(const stlString& strFilePath, bool bIncludeExtension = false);

	// Get the file extension including/excluding the "." from a given file path
	static stlString GetFileExtension(const stlString& strFilePath, bool bIncludeDot = false);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// String type converters, ANSI <-> UNICODE
	//////////////////////////////////////////////////////////////////////////
	#define GetString GetStringW

	static char Wide2Narrow(wchar_t wch);
	static wchar_t Narrow2Wide(char ch);

	static wstring GetStringW(const string& strA);
	static string GetStringA(const wstring& strW);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Registry
	//////////////////////////////////////////////////////////////////////////
	static stlString GetRegistryInfo(HKEY hKey, const stlString& strRegistryPath, const stlString& strValueName);
	static bool SetRegistryInfo(HKEY hkey, const stlString& strRegistryPath, const stlString& strValueName, const stlString& strValue);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Internet
	//////////////////////////////////////////////////////////////////////////
private:
	static LONG GetRegKey(HKEY key, LPCTSTR subkey, LPTSTR regdata);	// Only used by OpenURL()

public:
	static HINSTANCE OpenURL(LPCTSTR strURL);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Execution
	//////////////////////////////////////////////////////////////////////////
	static bool Execute(const stlString& strFilePath, const stlString& strParameters = _T(""), bool bShow = true, bool bWait = false);
	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	// Random generators
	//////////////////////////////////////////////////////////////////////////
	static stlString GenerateRandomString(long lMaxLength, bool bIncludeAlpha = true, bool bIncludeNumbers = true);
	//////////////////////////////////////////////////////////////////////////
};
