//********************************************************************************
//	This header- and sourcefiles holds commonly used calsses and structures
//  handling passing data in calculations; 070413 p�d
//********************************************************************************

#include "StdAfx.h"
#include "pad_hms_miscfunc.h"
#include "pad_calculation_classes.h"

////////////////////////////////////////////////////////////////////////////
// UCFunctionList; show ALL functions for e.g. volumes per 
// functiontype (Brandel,N�slunds etc); 070416 p�d

UCFunctionList::UCFunctionList() 
{
	m_nIdentifer = -1;
	m_nSpcID = -1;
	m_nIndex = -1;
	_tcscpy_s(m_szSpcName,64,_T(""));
	_tcscpy_s(m_szFuncType,64,_T(""));
	_tcscpy_s(m_szFuncArea,64,_T(""));
	_tcscpy_s(m_szFuncDesc,64,_T(""));
}

UCFunctionList::UCFunctionList(int id,int spc_id,int idx,LPCTSTR spc_name,LPCTSTR func_type,LPCTSTR func_area,LPCTSTR desc)
{
	m_nIdentifer = id;
	m_nSpcID = spc_id;
	m_nIndex = idx;
	_tcscpy_s(m_szSpcName,64,spc_name);
	_tcscpy_s(m_szFuncType,64,func_type);
	_tcscpy_s(m_szFuncArea,64,func_area);
	_tcscpy_s(m_szFuncDesc,64,desc);
}

int UCFunctionList::getID(void)						{ return m_nIdentifer; }
int UCFunctionList::getSpcID(void)				{ return m_nSpcID; }
int UCFunctionList::getIndex(void)				{ return m_nIndex; }
LPCTSTR UCFunctionList::getSpcName(void)		{ return m_szSpcName; }
LPCTSTR UCFunctionList::getFuncType(void)	{ return m_szFuncType; }
LPCTSTR UCFunctionList::getFuncArea(void)	{ return m_szFuncArea; }
LPCTSTR UCFunctionList::getFuncDesc(void)	{ return m_szFuncDesc; }

////////////////////////////////////////////////////////////////////////////
// UCFunctions; holds functiontypes e.g. for volume (Brandel,N�slunds etc); 070416 p�d
// This class also holds the vector to UCFunctionList (vecUCFunctionList); 070416 p�d
UCFunctions::UCFunctions(void)
{
	m_nIdentifer = -1;
	_tcscpy_s(m_szName,64,_T(""));
}
UCFunctions::UCFunctions(int id,LPCTSTR name)
{
	m_nIdentifer = id;
	_tcscpy_s(m_szName,64,name);
}

UCFunctions::UCFunctions(const UCFunctions &c)
{
	*this = c;
}

int UCFunctions::getIdentifer(void)		{ return m_nIdentifer; }
LPCTSTR UCFunctions::getName(void)				{ return m_szName; }

//////////////////////////////////////////////////////////////////////////
// Calculate Height,Bark and Volume; 070525 p�d

BOOL doUCCalculate(CTransaction_trakt rec_trakt,
				 					 CTransaction_trakt_misc_data rec_trakt_data,
									 vecTransactionTraktData &tdata_list,
									 vecTransactionSampleTree &tree_list,
									 vecTransactionDCLSTree &dcls_list,
									 vecTransactionTraktSetSpc &set_spc_list,
									 bool doCalc,bool doGrotCalc)
{
	BOOL bReturn = FALSE;
	// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(CTransaction_trakt,
																		CTransaction_trakt_misc_data,
																		vecTransactionTraktData &,
																		vecTransactionSampleTree &,
																		vecTransactionDCLSTree &,
																		vecTransactionTraktSetSpc &);
  DO_CALC doCalcProc;

	typedef CRuntimeClass *(*DO_CALC_GROT)(CTransaction_trakt,
																				 vecTransactionTraktData &,
																				 vecTransactionTraktSetSpc &,
																				 vecTransactionDCLSTree &);
  DO_CALC_GROT doCalcGrotProc;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,CALCULATION_MODULE);
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hModule, (GET_DO_CALCULTION));
		if (doCalcProc != NULL)
		{
			if (doCalc)
				doCalcProc(rec_trakt,rec_trakt_data,tdata_list,tree_list,dcls_list,set_spc_list);

			bReturn = TRUE;
		}	// if (proc != NULL)

		// Added 100312 p�d
		doCalcGrotProc = (DO_CALC_GROT)GetProcAddress((HMODULE)hModule, (GET_DO_GROT_CALCULTION));
		if (doCalcProc != NULL)
		{
			if (doGrotCalc)
				doCalcGrotProc(rec_trakt,tdata_list,set_spc_list,dcls_list);

			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}



//////////////////////////////////////////////////////////////////////////
// Calculate Exchange; 070525 p�d

BOOL doUMExchange(CTransaction_trakt_misc_data rec1,
								  vecTransactionDCLSTree &dcls_list,
								  vecTransactionTreeAssort &tree_assort,
									vecTransactionTraktSetSpc &set_spc)
{
	BOOL bReturn = FALSE;
	// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(CTransaction_trakt_misc_data,
																		vecTransactionDCLSTree &,
																		vecTransactionTreeAssort &,
																		vecTransactionTraktSetSpc &);
  DO_CALC doCalcProc;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,EXCHANGE_MODULE);
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hModule, GET_DO_EXCHANGE);
		if (doCalcProc != NULL)
		{
			doCalcProc(rec1,dcls_list,tree_assort,set_spc);

			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

//////////////////////////////////////////////////////////////////////////
// Calculate Exchange; 070525 p�d

BOOL doUMExchangeSimple(CTransaction_trakt_misc_data rec1,
												vecTransactionDCLSTree &dcls_list,
												vecTransactionTraktSetSpc &set_spc,
												vecTransactionTreeAssort &tree_assort,
												vecTransaction_exchange_simple &exch_data)
{
	BOOL bReturn = FALSE;
	// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_CALC)(CTransaction_trakt_misc_data,
																		vecTransactionDCLSTree &,
																		vecTransactionTraktSetSpc &,
																		vecTransactionTreeAssort &,
																		vecTransaction_exchange_simple &);
  DO_CALC doCalcProc;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,EXCHANGE_MODULE);
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		doCalcProc = (DO_CALC)GetProcAddress((HMODULE)hModule, GET_DO_EXCHANGE_SIMPLE);
		if (doCalcProc != NULL)
		{
			doCalcProc(rec1,dcls_list,set_spc,tree_assort,exch_data);

			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}

//////////////////////////////////////////////////////////////////////////
// Calculate fORREST NORM (Intr�ngsv�rdering); 080506 p�d

BOOL doUMForrestNorm(int calc_origin,
					 mapString& map_err_text,
					 CTransaction_elv_object rec0,
					 CTransaction_trakt rec1,
					 vecObjectTemplate_p30_table& p30,
					 vecObjectTemplate_p30_nn_table& p30_nn,
					 vecTransactionTraktData& trakt_data,
					 vecTransaction_elv_m3sk_per_specie& m3sk_in_out,
					 vecTransactionSampleTree& rand_trees,
					 CTransaction_eval_evaluation& eval_data,
					 vecTransactionTrakt& cruise_stands,
					 vecTransactionSpecies& vecSpecies,
					 // Return data ...
					 CStringArray& error_log,
					 CTransaction_elv_return_storm_dry& storm_dry,
					 CTransaction_elv_return_land_and_precut& land_and_precut,
					 mapDouble& rand_trees_volume)
{
	BOOL bReturn = FALSE;
	// Function declaration for AfxLoadLibrary, GetProcAddress; 070521 p�d
	typedef CRuntimeClass *(*DO_NORM)(int calc_origin,
		mapString& map_err_text,
		CTransaction_elv_object rec0,
		CTransaction_trakt rec1,
		vecObjectTemplate_p30_table& p30,
		vecObjectTemplate_p30_nn_table& p30_nn,
		vecTransactionTraktData& trakt_data,
		vecTransaction_elv_m3sk_per_specie& m3sk_in_out,
		vecTransactionSampleTree& rand_trees,
		CTransaction_eval_evaluation& eval_data,
		vecTransactionTrakt& cruise_stands,
		//#HMS-94 20220919 J� M�ste skicka med info om tr�dslagens inst�llda p30 tr�dslag f�r att kunna ber�kna kanttr�dsers�ttning
		vecTransactionSpecies& vecSpecies,
		// Return data ...
		CStringArray& error_log,
		CTransaction_elv_return_storm_dry& storm_dry,
		CTransaction_elv_return_land_and_precut& land_and_precut,
		mapDouble& rand_trees_volume);
  DO_NORM doCalcProc;

	CString sModule;

	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LANDVALUE_MODULE);
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		doCalcProc = (DO_NORM)GetProcAddress((HMODULE)hModule, GET_DO_CALCULTION);
		if (doCalcProc != NULL)
		{
			doCalcProc(calc_origin,
				map_err_text,
				rec0,
				rec1,
				p30,
				p30_nn,
				trakt_data,
				m3sk_in_out,
				rand_trees,
				eval_data,
				cruise_stands,
				//#HMS-94 20220919 J� M�ste skicka med info om tr�dslagens inst�llda p30 tr�dslag f�r att kunna ber�kna kanttr�dsers�ttning
				vecSpecies,
				error_log,
				storm_dry,
				land_and_precut,
				rand_trees_volume);

			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}
