#ifndef _XMLLITEBASE_H_
#define _XMLLITEBASE_H_

#include "xmllite.h"
//#include "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Include\xmllite.h"
#include <shlwapi.h>

#include <string>

class CxmlliteBase
{
	//CComPtr<IStream> m_pStream;
	CComPtr<IStream> m_pStream;
	CComPtr<IXmlReader> m_pReader;
	// Holds name of node element to iterate
	// in methods
	// - gotoFirstNodeElement
	// - gotoNextNodeElement
	CString m_sNodeElement;
	// Datamembers for xml-string into stream; 091215 p�d
	// Holds the xml-string to be entered 
	// into the stream; 091215 p�d
	std::string m_sXML;	
	// Holds the original xml data from
	// loadStream; 091217 p�d
	CString m_sXML_original;
	// Methods; 091215 p�d
	void convert_to_ascii(LPCTSTR in_str,std::string& out_str);
	std::string& find_and_replace( std::string& tInput, std::string tFind, std::string tReplace );

protected:
	HRESULT streamCopy(void);
	HRESULT streamRewind(void);
	HRESULT readerRewind(void);
	// Method; goto node element; 091222 p�d
	// Will set the reader to point to the specific element; 091222 p�d
	BOOL gotoNodeElement(LPCTSTR node_element);
	BOOL gotoNextNodeElement(void);
	// Read value from specific node element; 091222 p�d
	BOOL getNodeValue(LPCTSTR node_text,LPTSTR data);
	BOOL getNodeValue(LPCTSTR node_text,int *data);
	BOOL getNodeValue(LPCTSTR node_text,double *data);
	//-----------------------------------------------------------
	// Read specific attribute; 091222 p�d
	// By name; 091222 p�d
	BOOL getAttr(LPCTSTR attr_name,LPTSTR data);
	BOOL getAttr(LPCTSTR attr_name,int *data);
	BOOL getAttr(LPCTSTR attr_name,double *data);
	// By position; 091222 p�d
	// attr_pos starts from 0; 091222 p�d
	BOOL getAttr(int attr_pos,LPTSTR data);
	BOOL getAttr(int attr_pos,int *data);
	BOOL getAttr(int attr_pos,double *data);
	//-----------------------------------------------------------
	// Method for reading all attributes within an element; 091222 p�d
	BOOL gotoFirstAttribute(void);
	BOOL gotoNextAttribute(void);
	BOOL getAttrValue(LPTSTR data);
	BOOL getAttrValue(int *data);
	BOOL getAttrValue(double *data);

public:

	CxmlliteBase(void);
	virtual ~CxmlliteBase();

	virtual BOOL loadFile(LPCTSTR);
	virtual BOOL loadStream(LPCTSTR);

	// inlines
	inline BOOL isReader()	{ return m_pReader != NULL; }
	inline CComPtr<IXmlReader> getReader()	{ return m_pReader; }
	inline LPSTREAM getStream()	{ return m_pStream; }
	//inline CString& getXML()	{ return m_sXML_original; }

};


#endif