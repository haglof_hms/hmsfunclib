#include "StdAfx.h"
#include "ReportFileParser.h"

/*
inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


ReportFileParser::ReportFileParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

ReportFileParser::~ReportFileParser()
{
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL ReportFileParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL ReportFileParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

// Protected
CString ReportFileParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

// Public


BOOL ReportFileParser::getReports(vecSTDReports &vec)
{
	CComBSTR bstrData;
	long lNumOf;
	int nSpcID = -1;
	TCHAR szStrID[127];
	TCHAR szCaption[127];
	TCHAR szFN[127];
	MSXML2::IXMLDOMNamedNodeMap  *attributes1;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;

	MSXML2::IXMLDOMNodeListPtr pNodeList = pRoot->getElementsByTagName(_bstr_t(NODE_REPORTS));
	if (pNodeList)
	{	
		vec.clear();
		pNodeList->get_length( &lNumOf );
		for (long l = 0;l < lNumOf;l++)
		{
			MSXML2::IXMLDOMNodePtr pChild = pNodeList->item[l];

			if (pChild)
			{
				pChild->get_attributes( &attributes1 );

				MSXML2::IXMLDOMNodePtr pChild_fn = attributes1->getNamedItem(_bstr_t(NODE_FILENAME_ATTR));
				if (pChild_fn)
				{
					pChild_fn->get_text( &bstrData );
					_tcscpy_s(szFN,127,_bstr_t(bstrData));
				}	// if (pChild_caption)

				MSXML2::IXMLDOMNodePtr pChild_add_to = attributes1->getNamedItem(_bstr_t(NODE_STRID_ATTR));
				if (pChild_add_to)
				{
					pChild_add_to->get_text( &bstrData );
					_tcscpy_s(szStrID,127,_bstr_t(bstrData));
				}	// if (pChild_add_to)

				MSXML2::IXMLDOMNodePtr pChild_caption = attributes1->getNamedItem(_bstr_t(NODE_CAPTION_ATTR));
				if (pChild_caption)
				{
					pChild_caption->get_text( &bstrData );
					_tcscpy_s(szCaption,127,_bstr_t(bstrData));
				}	// if (pChild_caption)

				vec.push_back(CSTD_Reports(szFN,szStrID,szCaption));
			}	// if (pChild)

		}	// for (long l = 0;l < lNumOf;l++)

		return TRUE;
	}	// if (pNodeList)
	return FALSE;
}

*/