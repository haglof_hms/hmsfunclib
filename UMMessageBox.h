#pragma once

bool DisplayMsg();
int UMMessageBox(LPCTSTR lpszText, UINT nType = 0);
int UMMessageBox(UINT nIDPrompt, UINT nType = 0, UINT nIDHelp = (UINT)-1);
int UMMessageBox(HWND hWnd, LPCTSTR lpszText, LPCTSTR lpszCaption, UINT nType);

void LogDialogMessage(CString msg);
extern "C" void __declspec(dllexport) GetMessageLog(CString &log);
