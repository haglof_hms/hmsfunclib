#include <afxwin.h>
#include "UMMessageBox.h"

// Definition of customized message boxes.
// This file should not use precompiled headers as stdafx.h will wipe out the definition of AfxMessageBox and MessageBox.

CString g_strMessages;


bool DisplayMsg()
{
	// Display message boxes only if loaded outside of NorrServer
	const CString strNorrAppName(_T("NorrServer"));
	if( AfxGetApp() == NULL )
	{
		return false;
	}
	else if( strNorrAppName.Compare(AfxGetApp()->m_pszAppName) == 0 )
	{
		return false;
	}
	else
	{
		return true;
	}
}


int UMMessageBox(LPCTSTR lpszText, UINT nType)
{
	if( DisplayMsg() )
	{
		return AfxMessageBox(lpszText, nType);
	}
	else
	{
		g_strMessages += lpszText + CString(_T("\n"));
		return 0;
	}
}

int UMMessageBox(UINT nIDPrompt, UINT nType, UINT nIDHelp)
{
	if( DisplayMsg() )
	{
		return AfxMessageBox(nIDPrompt, nType, nIDHelp);
	}
	else
	{
		// ignore
		return 0;
	}
}

int UMMessageBox(HWND hWnd, LPCTSTR lpszText, LPCTSTR lpszCaption, UINT nType)
{
	if( DisplayMsg() )
	{
		return MessageBox(hWnd, lpszText, lpszCaption, nType);
	}
	else
	{
		g_strMessages += lpszText + CString(_T("\n"));
		return 0;
	}
}

void LogDialogMessage(CString msg)
{
	g_strMessages += msg + CString(_T("\n\n"));
}

void __declspec(dllexport) GetMessageLog(CString &log)
{
	log = g_strMessages;
}
